define(["jquery", "backbone",  "custom/Class"],
    function($, Backbone, Class) {

        return Class.extend({
            initialize: function(options) {

            },
            // Model Events - events that are triggered by a state change in the model
            // In this scenario we never want these events firing without values persisted
            // on the model...for more details see AppModel.js
            ModelEvents: {

                // Login
                UserIdChanged: "UserIdChanged",
                UserKeyChanged: "UserKeyChanged",
                UserLoginChanged: "UserLoginChanged",
                UserTypeChanged: "UserTypeChanged",
                UserFirstNameChanged: "UserFirstNameChanged",
                UserLastNameChanged: "UserLastNameChanged",
                LastCompanyMessagesViewChanged: "LastCompanyMessagesViewChanged",
                LastESAMediaViewChanged: "LastESAMediaViewChanged",
                WelcomePageClosedChanged: "WelcomePageClosedChanged",

                MyEmployeeIdChanged: "MyEmployeeIdChanged",
                MyTeamLeaderIdChanged: "MyTeamLeaderIdChanged",
                TimeZoneIdChanged: "TimeZoneIdChanged",
                TimerManualChanged: "TimerManualChanged",
                IsTeamLeaderChanged: "IsTeamLeaderChanged",

                HidePlansChanged: "HidePlansChanged",
                HideStatsTimecardChanged: "HideStatsTimecardChanged",
                HideLevel1EmployeesChanged: "HideLevel1EmployeesChanged",
                HideClientsChanged: "HideClientsChanged",
                HideCompaniesChanged: "HideCompaniesChanged",

                // Timer
                TimerStatusChanged: "TimerStatusChanged",
                UpdatedOpenTimerChanged: "UpdatedOpenTimerChanged",
                CurrentTimerIDChanged: "CurrentTimerIDChanged",
                IntTimerIDChanged: "IntTimerIDChanged",
                CurrentTimerServiceIDChanged: "CurrentTimerServiceIDChanged",
                CurrentTimerClientIDChanged: "CurrentTimerClientIDChanged",
                TimerStartTimeChanged: "TimerStartTimeChanged",
                TimerInterruptTimeChanged: "TimerInterruptTimeChanged",

                CallbackErrorMessageChanged: "CallbackErrorMessageChanged",

                // Store collapse/removal status of tiles on dashboard
                TileViewNotificationsChanged:"TileViewNotificationsChanged",
                TileViewCompanyStatsChanged:"TileViewCompanyStatsChanged",
                TileViewTeamPersonalStatsChanged:"TileViewTeamPersonalStatsChanged",
                TileViewTeamMeetingPercChanged:"TileViewTeamMeetingPercChanged",
                TileViewMessageBoardChanged:"TileViewMessageBoardChanged",
                TileViewPhoneDirectoryChanged:"TileViewPhoneDirectoryChanged",
                TileViewMyClientsChanged:"TileViewMyClientsChanged",
                ChatMessageDismissedChanged: "ChatMessageDismissedChanged",

                ShowCompTimeChanged:"ShowCompTimeChanged",
                MessageCommentDeletedIdChanged: "MessageCommentDeletedIdChanged",
                MessageCommentAddedIdChanged: "MessageCommentAddedIdChanged",

                // Filter and Other
                SelectedTeamLeaderIdChanged: "SelectedTeamLeaderIdChanged",
                SelectedClientIdChanged: "SelectedClientIdChanged",
                SelectedCompanyIdChanged: "SelectedCompanyIdChanged",
                SelectedTimerIdChanged: "SelectedTimerIdChanged",
                SelectedClientReportChanged: "SelectedClientReportChanged",
                SelectedViewChanged: "SelectedViewChanged",
                SelectedEmployeeIdChanged: "SelectedEmployeeIdChanged",
                SelectedNoteIdChanged: "SelectedNoteIdChanged",

                ResetFileList: 'ResetFileList',
                SelectedDateFilterChanged: "SelectedDateFilterChanged",
                SelectedStatusFilterChanged: "SelectedStatusFilterChanged",
                SelectedEmpStatusFilterChanged: "SelectedEmpStatusFilterChanged",

                Widget1ValueChanged: "Widget1ValueChanged",
                Widget2ValueChanged: "Widget2ValueChanged",
                Widget3ValueChanged: "Widget3ValueChanged",
                Widget4ValueChanged: "Widget4ValueChanged",

                MySumTotalTimeChanged: "MySumTotalTimeChanged",
                MySumTotalMeetingsChanged: "MySumTotalMeetingsChanged",
                MySumMeetingsTargetChanged: "MySumMeetingsTargetChanged",
                MyMtgPercentChanged: "MyMtgPercentChanged",

                TeamSumTotalTimeChanged: "TeamSumTotalTimeChanged",
                TeamSumTotalMeetingsChanged: "TeamSumTotalMeetingsChanged",
                TeamSumMeetingsTargetChanged: "TeamSumMeetingsTargetChanged",
                TeamMtgPercentChanged: "TeamMtgPercentChanged",

                ClientNameRecordsChanged: "ClientNameRecordsChanged",
                ClientAllNameRecordsChanged: "ClientAllNameRecordsChanged",
                ClientCompanyRecordsChanged: "ClientCompanyRecordsChanged",
                PlanRecordsChanged: "PlanRecordsChanged",
                EmployeeIdsNotUserType1Changed: "EmployeeIdsNotUserType1Changed",
                TeamLeaderRecordsChanged: "TeamLeaderRecordsChanged",

                EmployeeGridFilterChanged: "EmployeeGridFilterChanged",
                ClientDataGridFilterChanged: "ClientDataGridFilterChanged",
                ClientReportGridFilterChanged: "ClientReportGridFilterChanged",
                ClientContactGridFilterChanged: "ClientContactGridFilterChanged",
                ClientPasswordGridFilter: "ClientPasswordGridFilterChanged",
                ClientAssignmentGridFilterChanged: "ClientAssignmentGridFilterChanged",
                TimeCardDetailGridFilterChanged: "TimeCardDetailGridFilterChanged",
                UserGridFilterChanged: "UserGridFilterChanged",
                AdminFeedGridFilterChanged: "AdminFeedGridFilterChanged",
                AdminStaticGridFilterChanged: "AdminStaticGridFilterChanged",
                AdminBioGridFilterChanged: "AdminBioGridFilterChanged"

            }
        });
    });