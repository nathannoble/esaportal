define(["jquery", "backbone"],
    function($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            initialize: function() {
                //console.log('NoteModel:initialize');

            },
            idAttribute: 'noteID',
            // Default values for all of the Model attributes
            defaults: {

                // {
                // "clientNoteID":8855,
                // "userKey":10006,
                // "lastName":"Baumgardner",
                // "firstName":"Angela",
                // "noteTimeStamp":"2015-12-03T15:08:56.003",
                // "clientID":2189,
                // "noteText":"Called re Gimme 5 lm on vm"
                // }
                noteID: null,
                id: null,
                userKey: null,
                noteTimeStamp: null,
                noteText: null,
                lastName: null,
                firstName: null,
                isEditable: false
            },

            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function(attrs) {

            }

        });

        // Returns the Model class
        return Model;
    }
);