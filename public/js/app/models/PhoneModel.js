define(["jquery", "backbone"],
    function($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            initialize: function() {
                //console.log('NoteModel:initialize');

            },
            idAttribute: 'phoneID',
            // Default values for all of the Model attributes
            defaults: {

                employeeID: null,
                esaExtension: null,
                lastName: null,
                firstName: null,
                isEditable: false
            },

            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function(attrs) {

            }

        });

        // Returns the Model class
        return Model;
    }
);