define(["jquery", "backbone"],
    function($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            initialize: function() {
                //console.log('MessageModel:initialize');

            },
            idAttribute: 'message_id',
            // Default values for all of the Model attributes
            defaults: {

                //{
                // "messageID":1008,
                // "messageDate":"2011-10-01",
                // "messageText":"Thanks for helping make ESA a great place to work!"
                //}
                message_id: null,
                message_date: null,
                message_text: null,
                isEditable: false,
                comments:[]
            },

            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function(attrs) {

            }

        });

        // Returns the Model class
        return Model;
    }
);