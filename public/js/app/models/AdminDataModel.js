define(["jquery", "backbone"],
    function($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            initialize: function() {
                //console.log('AdminDataModel:initialize');

            },
            idAttribute: 'ID',
            // Default values for all of the Model attributes
            defaults: {
                ID: null,
                category:null,
                priority:null,
                subCategory:null,
                shortText: null,
                title: null,
                text: null,
                text2:null,
                externalLink:null,
                avFile: null,
                fileName: null,
                timeStamp:null,
                addUserImage:false,
                showAsCompanyMessage:false,
                isEditable: false,
                isFirst: false
            },

            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function(attrs) {

            }

        });

        // Returns the Model class
        return Model;
    }
);