define(["jquery", "backbone", "jquery.cookie"],
    function ($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            Application: null,

            // Default values for all of the Model attributes
            defaults: {
                // Breadcrumbs
                "breadcrumbs": [],

                // App State

                "dateFormat": null,
                // Date format - common values are "MMM dd, yyyy", "dd-MMM-yyyy", "yyyy-MM-ddTHH:mm:ss"

                // Login Info
                userKey: null,
                userId: null,
                userLogin: null,
                userType: null,
                userFirstName:null,
                userLastName:null,
                lastCompanyMessagesView:null,
                lastESAMediaView:null,
                welcomePageClosed:null,

                myEmployeeId: null,
                myTeamLeaderId: null,
                timeZoneId:null,
                timerManual:null,
                isTeamLeader: null,

                hidePlans: null,
                hideStatsTimecard: null,
                hideLevel1Employees: null,
                hideClients: null,
                hideCompanies: null,

                // Timer
                timerStatus: null,
                updatedOpenTimer: null,
                currentTimerID: null,
                intTimerID: null,
                currentTimerServiceID:null,
                currentTimerClientID: null,
                timerStartTime:null,
                timerInterruptTime:null,

                callbackErrorMessage: null,

                // Store collapse/removal status of tiles on dashboard
                tileViewNotifications:null,
                tileViewCompanyStats:null,
                tileViewTeamPersonalStats:null,
                tileViewTeamMeetingPerc:null,
                tileViewMessageBoard:null,
                tileViewPhoneDirectory:null,
                tileViewMyClients:null,
                chatMessageDismissed:null,

                showCompTime: false,
                messageCommentDeletedId: null,
                messageCommentAddedId:null,

                // Filters and Other
                resetFileList:false,
                selectedClientId: null,
                selectedClientReport: null,
                selectedView: null,
                selectedCompanyId: null, //4,
                selectedEmployeeId: null, //100
                selectedTimerId: null, //100
                selectedTeamLeaderId: null,
                selectedDateFilter: null,
                selectedStatusFilter: null,
                selectedEmpStatusFilter: null,
                selectedNoteId: null, //4,

                widget1Value: null,
                widget2Value: null,
                widget3Value: null,
                widget4Value: null,
                
                mySumTotalTime: null,
                mySumTotalMeetings: null,
                mySumMeetingsTarget: null,
                myMtgPercent: null,

                teamSumTotalTime: null,
                teamSumTotalMeetings: null,
                teamSumMeetingsTarget: null,
                teamMtgPercent: null,

                clientNameRecords: [],
                clientAllNameRecords: [],
                clientCompanyRecords: [],
                planRecords: [],
                employeeIdsNotUserType1: [],
                teamLeaderRecords: [],

                employeeGridFilter: [],
                clientDataGridFilter: [],
                clientReportGridFilter: [],
                clientContactGridFilter: [],
                clientPasswordGridFilter: [],
                clientAssignmentGridFilter: [],
                timeCardDetailGridFilter: [],
                userGridFilter: [],
                adminIndustryNewsGridFilter:[],
                adminStaticGridFilter:[],
                adminBioGridFilter:[]

            },
            initialize: function (App) {
                // List of events

                // App State
                this.bind("change:userId", this.onUserIdChanged);
                this.bind("change:userKey", this.onUserKeyChanged);
                this.bind("change:userLogin", this.onUserLoginChanged);
                this.bind("change:userType", this.onUserTypeChanged);
                this.bind("change:userFirstName", this.onUserFirstNameChanged);
                this.bind("change:userLastName", this.onUserLastNameChanged);
                this.bind("change:lastCompanyMessagesView", this.onLastCompanyMessagesViewChanged);
                this.bind("change:lastESAMediaView", this.onLastESAMediaViewChanged);
                this.bind("change:welcomePageClosed", this.onWelcomePageClosedChanged);

                this.bind("change:resetFileList", this.onResetFileList);
                this.bind("change:selectedClientId", this.onSelectedClientIdChanged);
                this.bind("change:selectedClientReport", this.onSelectedClientReportChanged);
                this.bind("change:selectedView", this.onSelectedViewChanged);
                this.bind("change:selectedCompanyId", this.onSelectedCompanyIdChanged);
                this.bind("change:selectedEmployeeId", this.onSelectedEmployeeIdChanged);
                this.bind("change:selectedTimerId", this.onSelectedTimerIdChanged);
                this.bind("change:selectedTeamLeaderId", this.onSelectedTeamLeaderIdChanged);
                this.bind("change:selectedNoteId", this.onSelectedNoteIdChanged);

                this.bind("change:myEmployeeId", this.onMyEmployeeIdChanged);
                this.bind("change:myTeamLeaderId", this.onMyTeamLeaderIdChanged);
                this.bind("change:timeZoneId", this.onTimeZoneIdChanged);
                this.bind("change:timerManual", this.onTimerManualChanged);
                this.bind("change:isTeamLeader", this.onIsTeamLeaderChanged);

                this.bind("change:hidePlans", this.onHidePlansChanged);
                this.bind("change:hideStatsTimecard", this.onHideStatsTimecardChanged);
                this.bind("change:hideLevel1Employees", this.onHideLevel1EmployeesChanged);
                this.bind("change:hideClients", this.onHideClientsChanged);
                this.bind("change:hideCompanies", this.onHideCompaniesChanged);
                this.bind("change:chatMessageDismissed", this.onChatMessageDismissedChanged);
                
                this.bind("change:timerStatus", this.onTimerStatusChanged);
                this.bind("change:updatedOpenTimer", this.onUpdatedOpenTimerChanged);

                this.bind("change:currentTimerID", this.onCurrentTimerIDChanged);
                this.bind("change:intTimerID", this.onIntTimerIDChanged);
                this.bind("change:currentTimerServiceID", this.onCurrentTimerServiceIDChanged);
                this.bind("change:currentTimerClientID", this.onCurrentTimerClientIDChanged);
                this.bind("change:timerStartTime", this.onTimerStartTimeChanged);
                this.bind("change:timerInterruptTime", this.onTimerInterruptTimeChanged);

                this.bind("change:callbackErrorMessage", this.onCallbackErrorMessageChanged);

                this.bind("change:tileViewNotifications", this.onTileViewNotificationsChanged);
                this.bind("change:tileViewCompanyStats", this.onTileViewCompanyStatsChanged);
                this.bind("change:tileViewTeamPersonalStats", this.onTileViewTeamPersonalStatsChanged);
                this.bind("change:tileViewTeamMeetingPerc", this.onTileViewTeamMeetingPercChanged);
                this.bind("change:tileViewMessageBoard", this.onTileViewMessageBoardChanged);
                this.bind("change:tileViewPhoneDirectory", this.onTileViewPhoneDirectoryChanged);

                this.bind("change:showCompTime", this.onShowCompTimeChanged);
                this.bind("change:messageCommentDeletedId", this.onMessageCommentDeletedIdChanged);
                this.bind("change:messageCommentAddedId", this.onMessageCommentAddedIdChanged);

                this.bind("change:selectedDateFilter", this.onSelectedDateFilterChanged);
                this.bind("change:selectedStatusFilter", this.onSelectedStatusFilterChanged);
                this.bind("change:selectedEmpStatusFilter", this.onSelectedEmpStatusFilterChanged);

                this.bind("change:widget1Value", this.onWidget1ValueChanged);
                this.bind("change:widget2Value", this.onWidget2ValueChanged);
                this.bind("change:widget3Value", this.onWidget3ValueChanged);
                this.bind("change:widget4Value", this.onWidget4ValueChanged);
                
                this.bind("change:mySumTotalTime", this.onMySumTotalTimeChanged);
                this.bind("change:mySumTotalMeetings", this.onMySumTotalMeetingsChanged);
                this.bind("change:mySumMeetingsTarget", this.onMySumMeetingsTargetChanged);
                this.bind("change:myMtgPercent", this.onMyMtgPercentChanged);

                this.bind("change:teamSumTotalTime", this.onTeamSumTotalTimeChanged);
                this.bind("change:teamSumTotalMeetings", this.onTeamSumTotalMeetingsChanged);
                this.bind("change:teamSumMeetingsTarget", this.onTeamSumMeetingsTargetChanged);
                this.bind("change:teamMtgPercent", this.onTeamMtgPercentChanged);

                this.bind("change:clientNameRecords", this.onClientNameRecordsChanged);
                this.bind("change:clientAllNameRecords", this.onClientAllNameRecordsChanged);
                this.bind("change:clientCompanyRecords", this.onClientCompanyRecordsChanged);
                this.bind("change:planRecords", this.onPlanRecordsChanged);
                this.bind("change:employeeIdsNotUserType1", this.onEmployeeIdsNotUserType1Changed);
                this.bind("change:teamLeaderRecords", this.onTeamLeaderRecordsChanged);

                this.bind("change:employeeGridFilter", this.onEmployeeGridFilterChanged);
                this.bind("change:clientDataGridFilter", this.onClientDataGridFilterChanged);
                this.bind("change:clientReportGridFilter", this.onClientReportGridFilterChanged);
                this.bind("change:clientContactGridFilter", this.onClientContactGridFilterChanged);
                this.bind("change:clientPasswordGridFilter", this.onClientAssignmentGridFilterChanged);
                this.bind("change:clientAssignmentGridFilter", this.onClientAssignmentGridFilterChanged);
                this.bind("change:timeCardDetailGridFilter", this.onTimeCardDetailGridFilterChanged);
                this.bind("change:userGridFilter", this.onUserGridFilterChanged);
                this.bind("change:adminIndustryNewsGridFilter", this.onAdminFeedGridFilterChanged);
                this.bind("change:adminStaticGridFilter", this.onAdminStaticGridFilterChanged);
                this.bind("change:adminBioGridFilter", this.onAdminBioGridFilterChanged);

                // Filters
                this.Application = App;

                // Restore state
                var userId = $.cookie('ESA:AppModel:userId');
                if (userId) {
                    //console.log('****** AppModel:restoreState:userId:' + userId);
                    if (userId === "null") {
                        this.set("userId", null);

                    }
                    else {
                        this.set("userId", userId);
                    }
                }

                var userKey = $.cookie('ESA:AppModel:userKey');
                if (userKey) {
                    //console.log('****** AppModel:restoreState:userKey:' + userKey);
                    if (userKey === "null") {
                        this.set("userKey", null);
                    }
                    else {
                        this.set("userKey", userKey);
                    }
                }

                var userLogin = $.cookie('ESA:AppModel:userLogin');
                if (userLogin) {
                    //console.log('****** AppModel:restoreState:userLogin:' + userLogin);
                    if (userLogin === "null") {
                        this.set("userLogin", null);
                    }
                    else {
                        this.set("userLogin", userLogin);
                    }
                }

                var userType = $.cookie('ESA:AppModel:userType');
                if (userType) {
                    //console.log('****** AppModel:restoreState:userType' + userType);
                    if (userType === "null") {
                        this.set("userType", null);
                    }
                    else {
                        this.set("userType", parseInt(userType,0));
                    }
                }

                var userFirstName = $.cookie('ESA:AppModel:userFirstName');
                if (userFirstName) {
                    //console.log('****** AppModel:restoreState:userFirstName:' + userFirstName);
                    if (userFirstName === "null") {
                        this.set("userFirstName", null);
                    }
                    else {
                        this.set("userFirstName", userFirstName);
                    }
                }

                var userLastName = $.cookie('ESA:AppModel:userLastName');
                if (userLastName) {
                    //console.log('****** AppModel:restoreState:userLastName:' + userLastName);
                    if (userLastName === "null") {
                        this.set("userLastName", null);
                    }
                    else {
                        this.set("userLastName", userLastName);
                    }
                }

                var lastCompanyMessagesView = $.cookie('ESA:AppModel:lastCompanyMessagesView');
                if (lastCompanyMessagesView) {
                    //console.log('****** AppModel:restoreState:lastCompanyMessagesView:' + lastCompanyMessagesView);
                    if (lastCompanyMessagesView === "null") {
                        this.set("lastCompanyMessagesView", null);
                    }
                    else {
                        this.set("lastCompanyMessagesView", lastCompanyMessagesView);
                    }
                }

                var lastESAMediaView = $.cookie('ESA:AppModel:lastESAMediaView');
                if (lastESAMediaView) {
                    //console.log('****** AppModel:restoreState:lastESAMediaView:' + lastESAMediaView);
                    if (lastESAMediaView === "null") {
                        this.set("lastESAMediaView", null);
                    }
                    else {
                        this.set("lastESAMediaView", lastESAMediaView);
                    }
                }

                var welcomePageClosed = $.cookie('ESA:AppModel:welcomePageClosed');
                if (welcomePageClosed) {
                    //console.log('****** AppModel:restoreState:welcomePageClosed:' + welcomePageClosed);
                    if (welcomePageClosed === "null") {
                        this.set("welcomePageClosed", null);
                    }
                    else {
                        this.set("welcomePageClosed", welcomePageClosed);
                    }
                }

                var clientId = $.cookie('ESA:AppModel:selectedClientId');
                if (clientId) {
                    if (clientId === "null") {
                        this.set("selectedClientId", null);

                    }
                    else {
                        this.set("selectedClientId", clientId);
                    }
                }

                var selectedTeamLeaderId = $.cookie('ESA:AppModel:selectedTeamLeaderId');
                if (selectedTeamLeaderId) {
                    if (selectedTeamLeaderId === "null") {
                        this.set("selectedTeamLeaderId", null);

                    }
                    else {
                        this.set("selectedTeamLeaderId", selectedTeamLeaderId);
                    }
                }


                var clientReport = $.cookie('ESA:AppModel:selectedClientReport');
                if (clientReport) {
                    if (clientReport === "null") {
                        this.set("selectedClientReport", null);

                    }
                    else {
                        this.set("selectedClientReport", clientReport);
                    }
                }

                var view = $.cookie('ESA:AppModel:selectedView');
                if (view) {
                    if (view === "null") {
                        this.set("selectedView", null);

                    }
                    else {
                        this.set("selectedView", view);
                    }
                }

                var companyId = $.cookie('ESA:AppModel:selectedCompanyId');
                if (companyId) {
                    if (companyId === "null") {
                        this.set("selectedCompanyId", null);

                    }
                    else {
                        this.set("selectedCompanyId", companyId);
                    }
                }

                var employeeId = $.cookie('ESA:AppModel:selectedEmployeeId');
                if (employeeId) {
                    if (employeeId === "null") {
                        this.set("selectedEmployeeId", null);

                    }
                    else {
                        this.set("selectedEmployeeId", employeeId);
                    }
                }

                var timerId = $.cookie('ESA:AppModel:selectedTimerId');
                if (timerId) {
                    if (timerId === "null") {
                        this.set("selectedTimerId", null);

                    }
                    else {
                        this.set("selectedTimerId", timerId);
                    }
                }

                var dateFilter = $.cookie('ESA:AppModel:selectedDateFilter');
                if (dateFilter) {
                    if (dateFilter === "null") {
                        this.set("selectedDateFilter", null);

                    }
                    else {
                        this.set("selectedDateFilter", dateFilter);
                    }
                }

                var myTeamLeaderId = $.cookie('ESA:AppModel:myTeamLeaderId');
                if (myTeamLeaderId) {
                    if (myTeamLeaderId === "null") {
                        this.set("myTeamLeaderId", null);

                    }
                    else {
                        this.set("myTeamLeaderId", myTeamLeaderId);
                    }
                }

                var isTeamLeader = $.cookie('ESA:AppModel:isTeamLeader');
                if (isTeamLeader) {
                    if (isTeamLeader === "null") {
                        this.set("isTeamLeader", null);

                    }
                    else {
                        this.set("isTeamLeader", isTeamLeader);
                    }
                }

                var hidePlans = $.cookie('ESA:AppModel:hidePlans');
                if (hidePlans) {
                    if (hidePlans === "null") {
                        this.set("hidePlans", null);

                    }
                    else {
                        this.set("hidePlans", hidePlans);
                    }
                }

                var hideStatsTimecard = $.cookie('ESA:AppModel:hideStatsTimecard');
                if (hideStatsTimecard) {
                    if (hideStatsTimecard === "null") {
                        this.set("hideStatsTimecard", null);

                    }
                    else {
                        this.set("hideStatsTimecard", hideStatsTimecard);
                    }
                }

                var hideLevel1Employees = $.cookie('ESA:AppModel:hideLevel1Employees');
                if (hideLevel1Employees) {
                    if (hideLevel1Employees === "null") {
                        this.set("hideLevel1Employees", null);

                    }
                    else {
                        this.set("hideLevel1Employees", hideLevel1Employees);
                    }
                }

                var hideClients = $.cookie('ESA:AppModel:hideClients');
                if (hideClients) {
                    if (hideClients === "null") {
                        this.set("hideClients", null);

                    }
                    else {
                        this.set("hideClients", hideClients);
                    }
                }

                var hideCompanies = $.cookie('ESA:AppModel:hideCompanies');
                if (hideCompanies) {
                    if (hideCompanies === "null") {
                        this.set("hideCompanies", null);

                    }
                    else {
                        this.set("hideCompanies", hideCompanies);
                    }
                }

                var myEmployeeId = $.cookie('ESA:AppModel:myEmployeeId');
                if (myEmployeeId) {
                    if (myEmployeeId === "null") {
                        this.set("myEmployeeId", null);

                    }
                    else {
                        this.set("myEmployeeId", myEmployeeId);
                    }
                }

                var noteId = $.cookie('ESA:AppModel:selectedNoteId');
                if (noteId) {
                    if (noteId === "null") {
                        this.set("selectedNoteId", null);

                    }
                    else {
                        this.set("selectedNoteId", noteId);
                    }
                }

                var timeZoneId = $.cookie('ESA:AppModel:timeZoneId');
                if (timeZoneId) {
                    if (timeZoneId === "null") {
                        this.set("timeZoneId", null);

                    }
                    else {
                        this.set("timeZoneId", timeZoneId);
                    }
                }

                var timerManual = $.cookie('ESA:AppModel:timerManual');
                if (timerManual) {
                    if (timerManual === "null") {
                        this.set("timerManual", null);

                    }
                    else {
                        this.set("timerManual", timerManual);
                    }
                }

                var statusFilter = $.cookie('ESA:AppModel:selectedStatusFilter');
                if (statusFilter) {
                    if (statusFilter === "null") {
                        this.set("selectedStatusFilter", null);

                    }
                    else {
                        this.set("selectedStatusFilter", statusFilter);
                    }
                }

                var empStatusFilter = $.cookie('ESA:AppModel:selectedEmpStatusFilter');
                if (empStatusFilter) {
                    if (empStatusFilter === "null") {
                        this.set("selectedEmpStatusFilter", null);

                    }
                    else {
                        this.set("selectedEmpStatusFilter", empStatusFilter);
                    }
                }

                var timerStatus = $.cookie('ESA:AppModel:timerStatus');
                if (timerStatus) {
                    if (timerStatus === "null") {
                        this.set("timerStatus", null);

                    }
                    else {
                        this.set("timerStatus", timerStatus);
                    }
                }

                var updatedOpenTimer = $.cookie('ESA:AppModel:updatedOpenTimer');
                if (updatedOpenTimer) {
                    if (updatedOpenTimer === "null") {
                        this.set("updatedOpenTimer", null);

                    }
                    else {
                        this.set("updatedOpenTimer", updatedOpenTimer);
                    }
                }

                var callbackErrorMessage = $.cookie('ESA:AppModel:callbackErrorMessage');
                if (callbackErrorMessage) {
                    if (callbackErrorMessage === "null") {
                        this.set("callbackErrorMessage", null);
                    }
                    else {
                        this.set("callbackErrorMessage", callbackErrorMessage);
                    }
                }

                var timerStartTime = $.cookie('ESA:AppModel:timerStartTime');
                if (timerStartTime) {
                    if (timerStartTime === "null") {
                        this.set("timerStartTime", null);

                    }
                    else {
                        this.set("timerStartTime", timerStartTime);
                    }
                }

                var timerInterruptTime = $.cookie('ESA:AppModel:timerInterruptTime');
                if (timerInterruptTime) {
                    if (timerInterruptTime === "null") {
                        this.set("timerInterruptTime", null);

                    }
                    else {
                        this.set("timerInterruptTime", timerInterruptTime);
                    }
                }

                var tileViewNotifications = $.cookie('ESA:AppModel:tileViewNotifications');
                if (tileViewNotifications) {
                    if (tileViewNotifications === "null") {
                        this.set("tileViewNotifications", null);

                    }
                    else {
                        this.set("tileViewNotifications", tileViewNotifications);
                    }
                }

                var tileViewCompanyStats = $.cookie('ESA:AppModel:tileViewCompanyStats');
                if (tileViewCompanyStats) {
                    if (tileViewCompanyStats === "null") {
                        this.set("tileViewCompanyStats", null);

                    }
                    else {
                        this.set("tileViewCompanyStats", tileViewCompanyStats);
                    }
                }

                var tileViewTeamPersonalStats = $.cookie('ESA:AppModel:tileViewTeamPersonalStats');
                if (tileViewTeamPersonalStats) {
                    if (tileViewTeamPersonalStats === "null") {
                        this.set("tileViewTeamPersonalStats", null);

                    }
                    else {
                        this.set("tileViewTeamPersonalStats", tileViewTeamPersonalStats);
                    }
                }

                var tileViewTeamMeetingPerc = $.cookie('ESA:AppModel:tileViewTeamMeetingPerc');
                if (tileViewTeamMeetingPerc) {
                    if (tileViewTeamMeetingPerc === "null") {
                        this.set("tileViewTeamMeetingPerc", null);

                    }
                    else {
                        this.set("tileViewTeamMeetingPerc", tileViewTeamMeetingPerc);
                    }
                }

                var tileViewMessageBoard = $.cookie('ESA:AppModel:tileViewMessageBoard');
                if (tileViewMessageBoard) {
                    if (tileViewMessageBoard === "null") {
                        this.set("tileViewMessageBoard", null);

                    }
                    else {
                        this.set("tileViewMessageBoard", tileViewMessageBoard);
                    }
                }

                var tileViewPhoneDirectory = $.cookie('ESA:AppModel:tileViewPhoneDirectory');
                if (tileViewPhoneDirectory) {
                    if (tileViewPhoneDirectory === "null") {
                        this.set("tileViewPhoneDirectory", null);

                    }
                    else {
                        this.set("tileViewPhoneDirectory", tileViewPhoneDirectory);
                    }
                }

                var tileViewMyClients = $.cookie('ESA:AppModel:tileViewMyClients');
                if (tileViewMyClients) {
                    if (tileViewMyClients === "null") {
                        this.set("tileViewMyClients", null);

                    }
                    else {
                        this.set("tileViewMyClients", tileViewMyClients);
                    }
                }

                var chatMessageDismissed = $.cookie('ESA:AppModel:chatMessageDismissed');
                if (chatMessageDismissed) {
                    if (chatMessageDismissed === "null") {
                        this.set("chatMessageDismissed", null);

                    }
                    else {
                        this.set("chatMessageDismissed", chatMessageDismissed);
                    }
                }

                var showCompTime = $.cookie('ESA:AppModel:showCompTime');
                if (showCompTime) {
                    if (showCompTime === "null") {
                        this.set("showCompTime", null);

                    }
                    else {
                        this.set("showCompTime", showCompTime);
                    }
                }

                var messageCommentDeletedId = $.cookie('ESA:AppModel:messageCommentDeletedId');
                if (messageCommentDeletedId) {
                    if (messageCommentDeletedId === "null") {
                        this.set("messageCommentDeletedId", null);

                    }
                    else {
                        this.set("messageCommentDeletedId", messageCommentDeletedId);
                    }
                }

                var messageCommentAddedId = $.cookie('ESA:AppModel:messageCommentAddedId');
                if (messageCommentAddedId) {
                    if (messageCommentAddedId === "null") {
                        this.set("messageCommentAddedId", null);

                    }
                    else {
                        this.set("messageCommentAddedId", messageCommentAddedId);
                    }
                }

                var currentTimerID = $.cookie('ESA:AppModel:currentTimerID');
                if (currentTimerID) {
                    if (currentTimerID === "null") {
                        this.set("currentTimerID", null);

                    }
                    else {
                        this.set("currentTimerID", currentTimerID);
                    }
                }

                var intTimerID = $.cookie('ESA:AppModel:intTimerID');
                if (intTimerID) {
                    if (intTimerID === "null") {
                        this.set("intTimerID", null);

                    }
                    else {
                        this.set("intTimerID", intTimerID);
                    }
                }

                var currentTimerServiceID = $.cookie('ESA:AppModel:currentTimerServiceID');
                if (currentTimerServiceID) {
                    if (currentTimerServiceID === "null") {
                        this.set("currentTimerServiceID", null);

                    }
                    else {
                        this.set("currentTimerServiceID", currentTimerServiceID);
                    }
                }

                var currentTimerClientID = $.cookie('ESA:AppModel:currentTimerClientID');
                if (currentTimerClientID) {
                    if (currentTimerClientID === "null") {
                        this.set("currentTimerClientID", null);

                    }
                    else {
                        this.set("currentTimerClientID", currentTimerClientID);
                    }
                }

                var widget1Value = $.cookie('ESA:AppModel:widget1Value');
                if (widget1Value) {
                    if (widget1Value === "null") {
                        this.set("widget1Value", null);

                    }
                    else {
                        this.set("widget1Value", widget1Value);
                    }
                }

                var widget2Value = $.cookie('ESA:AppModel:widget2Value');
                if (widget2Value) {
                    if (widget2Value === "null") {
                        this.set("widget2Value", null);

                    }
                    else {
                        this.set("widget2Value", widget2Value);
                    }
                }

                var widget3Value = $.cookie('ESA:AppModel:widget3Value');
                if (widget3Value) {
                    if (widget3Value === "null") {
                        this.set("widget33Value", null);

                    }
                    else {
                        this.set("widget3Value", widget3Value);
                    }
                }

                var widget4Value = $.cookie('ESA:AppModel:widget4Value');
                if (widget4Value) {
                    if (widget4Value === "null") {
                        this.set("widget4Value", null);

                    }
                    else {
                        this.set("widget4Value", widget4Value);
                    }
                }

                var mySumTotalTime = $.cookie('ESA:AppModel:mySumTotalTime');
                if (mySumTotalTime) {
                    if (mySumTotalTime === "null") {
                        this.set("mySumTotalTime", null);

                    }
                    else {
                        this.set("mySumTotalTime", mySumTotalTime);
                    }
                }

                var mySumTotalMeetings = $.cookie('ESA:AppModel:mySumTotalMeetings');
                if (mySumTotalMeetings) {
                    if (mySumTotalMeetings === "null") {
                        this.set("mySumTotalMeetings", null);

                    }
                    else {
                        this.set("mySumTotalMeetings", mySumTotalMeetings);
                    }
                }

                var mySumMeetingsTarget = $.cookie('ESA:AppModel:mySumMeetingsTarget');
                if (mySumMeetingsTarget) {
                    if (mySumMeetingsTarget === "null") {
                        this.set("mySumMeetingsTarget", null);

                    }
                    else {
                        this.set("mySumMeetingsTarget", mySumMeetingsTarget);
                    }
                }

                var myMtgPercent = $.cookie('ESA:AppModel:myMtgPercent');
                if (myMtgPercent) {
                    if (myMtgPercent === "null") {
                        this.set("myMtgPercent", null);

                    }
                    else {
                        this.set("myMtgPercent", myMtgPercent);
                    }
                }

                var teamSumTotalTime = $.cookie('ESA:AppModel:teamSumTotalTime');
                if (teamSumTotalTime) {
                    if (teamSumTotalTime === "null") {
                        this.set("teamSumTotalTime", null);

                    }
                    else {
                        this.set("teamSumTotalTime", teamSumTotalTime);
                    }
                }

                var teamSumTotalMeetings = $.cookie('ESA:AppModel:teamSumTotalMeetings');
                if (teamSumTotalMeetings) {
                    if (teamSumTotalMeetings === "null") {
                        this.set("teamSumTotalMeetings", null);

                    }
                    else {
                        this.set("teamSumTotalMeetings", teamSumTotalMeetings);
                    }
                }

                var teamSumMeetingsTarget = $.cookie('ESA:AppModel:teamSumMeetingsTarget');
                if (teamSumMeetingsTarget) {
                    if (teamSumMeetingsTarget === "null") {
                        this.set("teamSumMeetingsTarget", null);

                    }
                    else {
                        this.set("teamSumMeetingsTarget", teamSumMeetingsTarget);
                    }
                }

                var teamMtgPercent = $.cookie('ESA:AppModel:teamMtgPercent');
                if (teamMtgPercent) {
                    if (teamMtgPercent === "null") {
                        this.set("teamMtgPercent", null);

                    }
                    else {
                        this.set("teamMtgPercent", teamMtgPercent);
                    }
                }

            },
            //**************************************************************
            //* Model Triggers - events fired when properties on the model
            //                   change...these events generally have
            //                   state associated with them
            //**************************************************************

            onUserIdChanged: function () {

                //console.log('****** AppModel:onUserIdChanged:userId:' + this.get("userId"));
                $.cookie('ESA:AppModel:userId', this.get("userId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserIdChanged, this.get('userId'));

            },
            onUserKeyChanged: function () {

                //console.log('****** AppModel:onUserKeyChanged:userKey:' + this.get("userKey"));
                $.cookie('ESA:AppModel:userKey', this.get("userKey"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserKeyChanged, this.get('userKey'));

            },
            onUserLoginChanged: function () {

                //console.log('****** AppModel:onUserLoginChanged:userLogin:' + this.get("userLogin"));
                $.cookie('ESA:AppModel:userLogin', this.get("userLogin"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserLoginChanged, this.get('userLogin'));

            },
            onUserTypeChanged: function () {

                //console.log('****** AppModel:onUserTypeChanged:userType:' + this.get("userType"));
                $.cookie('ESA:AppModel:userType', this.get("userType"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserTypeChanged, this.get('userType'));

            },
            onUserFirstNameChanged: function () {

                //console.log('****** AppModel:onFirstNameChanged:userFirstName:' + this.get("userFirstName"));
                $.cookie('ESA:AppModel:userFirstName', this.get("userFirstName"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserFirstNameChanged, this.get('userFirstName'));

            },
            onUserLastNameChanged: function () {

                //console.log('****** AppModel:onUserLastNameChanged:userLastName:' + this.get("userLastName"));
                $.cookie('ESA:AppModel:userLastName', this.get("userLastName"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserLastNameChanged, this.get('userLastName'));

            },
            onLastCompanyMessagesViewChanged: function () {

                //console.log('****** AppModel:onLastCompanyMessagesViewChanged:lastCompanyMessagesView:' + this.get("lastCompanyMessagesView"));
                $.cookie('ESA:AppModel:lastCompanyMessagesView', this.get("lastCompanyMessagesView"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.LastCompanyMessagesViewChanged, this.get('lastCompanyMessagesView'));

            },
            onLastESAMediaViewChanged: function () {

                //console.log('****** AppModel:onLastESAMediaViewChanged:lastESAMediaView:' + this.get("lastESAMediaView"));
                $.cookie('ESA:AppModel:lastESAMediaView', this.get("lastESAMediaView"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.LastESAMediaViewChanged, this.get('lastESAMediaView'));

            },
            onWelcomePageClosedChanged: function () {

                //console.log('****** AppModel:onWelcomePageClosedChanged:welcomePageClosed:' + this.get("welcomePageClosed"));
                $.cookie('ESA:AppModel:welcomePageClosed', this.get("welcomePageClosed"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.WelcomePageClosedChanged, this.get('welcomePageClosed'));

            },

            onSelectedClientIdChanged: function () {

                //console.log('****** AppModel:onSelectedClientIdChanged:' + this.get("selectedClientId"));

                $.cookie('ESA:AppModel:selectedClientId', this.get("selectedClientId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedClientIdChanged, this.get('selectedClientId'));

            },
            onSelectedTeamLeaderIdChanged: function () {

                //console.log('****** AppModel:onSelectedTeamLeaderIdChanged:' + this.get("selectedTeamLeaderId"));

                $.cookie('ESA:AppModel:selectedTeamLeaderId', this.get("selectedTeamLeaderId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedTeamLeaderIdChanged, this.get('selectedTeamLeaderId'));

            },
            onSelectedClientReportChanged: function () {

                //console.log('****** AppModel:onSelectedClientReportChanged:' + this.get("selectedClientReport"));

                $.cookie('ESA:AppModel:selectedClientReport', this.get("selectedClientReport"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedClientReportChanged, this.get('selectedClientReport'));

            },
            onSelectedViewChanged: function () {

                //console.log('****** AppModel:onSelectedViewChanged:' + this.get("selectedView"));

                $.cookie('ESA:AppModel:selectedView', this.get("selectedView"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedViewChanged, this.get('selectedView'));

            },
            onSelectedCompanyIdChanged: function () {
                //console.log('****** AppModel:onSelectedCompanyIdChanged:' + this.get("selectedCompanyId"));

                $.cookie('ESA:AppModel:selectedCompanyId', this.get("selectedCompanyId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedCompanyIdChanged, this.get('selectedCompanyId'));

            },
            onSelectedEmployeeIdChanged: function () {
                //console.log('****** AppModel:onSelectedEmployeeIdChanged:' + this.get("selectedEmployeeId"));

                $.cookie('ESA:AppModel:selectedEmployeeId', this.get("selectedEmployeeId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedEmployeeIdChanged, this.get('selectedEmployeeId'));

            },
            onSelectedTimerIdChanged: function () {
                //console.log('****** AppModel:onSelectedTimerIdChanged:' + this.get("selectedTimerId"));

                $.cookie('ESA:AppModel:selectedTimerId', this.get("selectedTimerId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedTimerIdChanged, this.get('selectedTimerId'));

            },
            onSelectedNoteIdChanged: function () {
                //console.log('****** AppModel:onSelectedNoteIdChanged:' + this.get("selectedNoteId"));

                $.cookie('ESA:AppModel:selectedNoteId', this.get("selectedNoteId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedNoteIdChanged, this.get('selectedNoteId'));

            },
            onMyEmployeeIdChanged: function () {
                //console.log('****** AppModel:onMyEmployeeIdChanged:' + this.get("myEmployeeId"));

                $.cookie('ESA:AppModel:myEmployeeId', this.get("myEmployeeId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.MyEmployeeIdChanged, this.get('myEmployeeId'));

            },
            onMyTeamLeaderIdChanged: function () {
                //console.log('****** AppModel:onMyTeamLeaderIdChanged:' + this.get("myTeamLeaderId"));

                $.cookie('ESA:AppModel:myTeamLeaderId', this.get("myTeamLeaderId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.MyTeamLeaderIdChanged, this.get('myTeamLeaderId'));

            },
            onIsTeamLeaderChanged: function () {
                //console.log('****** AppModel:onIsTeamLeaderChanged:' + this.get("isTeamLeader"));

                $.cookie('ESA:AppModel:isTeamLeader', this.get("isTeamLeader"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.IsTeamLeaderChanged, this.get('isTeamLeader'));

            },
            onHidePlansChanged: function () {
                //console.log('****** AppModel:onHidePlansChanged:' + this.get("hidePlans"));

                $.cookie('ESA:AppModel:hidePlans', this.get("hidePlans"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.HidePlansChanged, this.get('hidePlans'));

            },
            onHideStatsTimecardChanged: function () {
                //console.log('****** AppModel:onHideStatsTimecardChanged:' + this.get("hideStatsTimecard"));

                $.cookie('ESA:AppModel:hideStatsTimecard', this.get("hideStatsTimecard"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.HideStatsTimecardChanged, this.get('hideStatsTimecard'));

            },
            onHideLevel1EmployeesChanged: function () {
                //console.log('****** AppModel:onHideLevel1EmployeesChanged:' + this.get("hideLevel1Employees"));

                $.cookie('ESA:AppModel:hideLevel1Employees', this.get("hideLevel1Employees"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.HideLevel1EmployeesChanged, this.get('hideLevel1Employees'));

            },
            onHideClientsChanged: function () {
                //console.log('****** AppModel:onHideClientsChanged:' + this.get("hideClients"));

                $.cookie('ESA:AppModel:hideClients', this.get("hideClients"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.HideClientsChanged, this.get('hideClients'));

            },
            onHideCompaniesChanged: function () {
                //console.log('****** AppModel:onHideCompaniesChanged:' + this.get("hideCompanies"));

                $.cookie('ESA:AppModel:hideCompanies', this.get("hideCompanies"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.HideCompaniesChanged, this.get('hideCompanies'));

            },
            onTimeZoneIdChanged: function () {
                //console.log('****** AppModel:onTimeZoneIdChanged:' + this.get("timeZoneId"));

                $.cookie('ESA:AppModel:timeZoneId', this.get("timeZoneId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TimeZoneIdChanged, this.get('timeZoneId'));

            },
            onTimerManualChanged: function () {
                //console.log('****** AppModel:onTimerManualChanged:' + this.get("timerManual"));

                $.cookie('ESA:AppModel:timerManual', this.get("timerManual"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TimerManualChanged, this.get('timerManual'));

            },
            onSelectedDateFilterChanged: function () {

                //console.log('****** AppModel:onSelectedDateFilterChanged:' + this.get("selectedDateFilter"));

                $.cookie('ESA:AppModel:selectedDateFilter', this.get("selectedDateFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedDateFilterChanged, this.get('selectedDateFilter'));

            },
            onSelectedStatusFilterChanged: function () {

                //console.log('****** AppModel:onSelectedStatusFilterChanged:' + this.get("selectedStatusFilter"));

                $.cookie('ESA:AppModel:selectedStatusFilter', this.get("selectedStatusFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedStatusFilterChanged, this.get('selectedStatusFilter'));

            },
            onSelectedEmpStatusFilterChanged: function () {

                //console.log('****** AppModel:onSelectedEmpStatusFilterChanged:' + this.get("selectedEmpStatusFilter"));

                $.cookie('ESA:AppModel:selectedEmpStatusFilter', this.get("selectedEmpStatusFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.SelectedEmpStatusFilterChanged, this.get('selectedEmpStatusFilter'));

            },
            onTimerStatusChanged: function () {
                //console.log('****** AppModel:onTimerStatusChanged:' + this.get("timerStatus"));

                $.cookie('ESA:AppModel:timerStatus', this.get("timerStatus"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TimerStatusChanged, this.get('timerStatus'));

            },
            onUpdatedOpenTimerChanged: function () {
                //console.log('****** AppModel:onUpdatedOpenTimerChanged:' + this.get("updatedOpenTimer"));

                $.cookie('ESA:AppModel:updatedOpenTimer', this.get("updatedOpenTimer"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UpdatedOpenTimerChanged, this.get('updatedOpenTimer'));

            },
            onCallbackErrorMessageChanged: function () {
                console.log('****** AppModel:onCallbackErrorMessageChanged:' + this.get("callbackErrorMessage"));

                $.cookie('ESA:AppModel:callbackErrorMessage', this.get("callbackErrorMessage"));

                //if (this.get('callbackErrorMessage') !== null) {
                    this.Application.vent.trigger(this.Application.Events.ModelEvents.CallbackErrorMessageChanged, this.get('callbackErrorMessage'));
                //}
            },
            onTimerStartTimeChanged: function () {
                //console.log('****** AppModel:onStartTimeChanged:' + this.get("timerStartTime"));

                $.cookie('ESA:AppModel:timerStartTime', this.get("timerStartTime"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TimerStartTimeChanged, this.get('timerStartTime'));

            },
            onTimerInterruptTimeChanged: function () {
                //console.log('****** AppModel:onTimerInterruptTimeChanged:' + this.get("timerInterruptTime"));

                $.cookie('ESA:AppModel:timerInterruptTime', this.get("timerInterruptTime"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TimerInterruptTimeChanged, this.get('timerInterruptTime'));

            },
            onTileViewNotificationsChanged: function () {
                //console.log('****** AppModel:onTileViewNotificationsChanged:' + this.get("tileViewNotifications"));

                $.cookie('ESA:AppModel:tileViewNotifications', this.get("tileViewNotifications"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TileViewNotificationsChanged, this.get('tileViewNotifications'));

            },
            onTileViewCompanyStatsChanged: function () {
                //console.log('****** AppModel:onTileViewCompanyStatsChanged:' + this.get("tileViewCompanyStats"));

                $.cookie('ESA:AppModel:tileViewCompanyStats', this.get("tileViewCompanyStats"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TileViewCompanyStatsChanged, this.get('tileViewCompanyStats'));

            },
            onTileViewTeamPersonalStatsChanged: function () {
                //console.log('****** AppModel:onTileViewTeamPersonalStatsChanged:' + this.get("tileViewTeamPersonalStats"));

                $.cookie('ESA:AppModel:tileViewTeamPersonalStats', this.get("tileViewTeamPersonalStats"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TileViewTeamPersonalStatsChanged, this.get('tileViewTeamPersonalStats'));

            },
            onTileViewTeamMeetingPercChanged: function () {
                //console.log('****** AppModel:onTileViewTeamMeetingPercChanged:' + this.get("tileViewTeamMeetingPerc"));

                $.cookie('ESA:AppModel:tileViewTeamMeetingPerc', this.get("tileViewTeamMeetingPerc"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TileViewTeamMeetingPercChanged, this.get('tileViewTeamMeetingPerc'));

            },
            onTileViewMessageBoardChanged: function () {
                //console.log('****** AppModel:onTileViewMessageBoardChanged:' + this.get("tileViewMessageBoard"));

                $.cookie('ESA:AppModel:tileViewMessageBoard', this.get("tileViewMessageBoard"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TileViewMessageBoardChanged, this.get('tileViewMessageBoard'));

            },
            onTileViewPhoneDirectoryChanged: function () {
                //console.log('****** AppModel:onTileViewPhoneDirectoryChanged:' + this.get("tileViewPhoneDirectory"));

                $.cookie('ESA:AppModel:tileViewPhoneDirectory', this.get("tileViewPhoneDirectory"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TileViewPhoneDirectoryChanged, this.get('tileViewPhoneDirectory'));

            },
            onTileViewMyClientsChanged: function () {
                //console.log('****** AppModel:onTileViewMyClientsChanged:' + this.get("tileViewMyClients"));

                $.cookie('ESA:AppModel:tileViewMyClients', this.get("tileViewMyClients"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TileViewMyClientsChanged, this.get('tileViewMyClients'));

            },
            onChatMessageDismissedChanged: function () {
                //console.log('****** AppModel:onChatMessageDismissedChanged:' + this.get("chatMessageDismissed"));

                $.cookie('ESA:AppModel:chatMessageDismissed', this.get("chatMessageDismissed"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ChatMessageDismissedChanged, this.get('chatMessageDismissed'));

            },
            onShowCompTimeChanged: function () {
                //console.log('****** AppModel:onShowCompTimeChanged:' + this.get("showCompTime"));

                $.cookie('ESA:AppModel:showCompTime', this.get("showCompTime"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ShowCompTimeChanged, this.get('showCompTime'));

            },
            onMessageCommentDeletedIdChanged: function () {
                //console.log('****** AppModel:onMessageCommentDeletedIdChanged:' + this.get("messageCommentDeletedId"));

                $.cookie('ESA:AppModel:messageCommentDeletedId', this.get("messageCommentDeletedId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.MessageCommentDeletedIdChanged, this.get('messageCommentDeletedId'));

            },
            onMessageCommentAddedIdChanged: function () {
                //console.log('****** AppModel:onMessageCommentAddedIdChanged:' + this.get("messageCommentAddedId"));

                $.cookie('ESA:AppModel:messageCommentAddedId', this.get("messageCommentAddedId"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.MessageCommentAddedIdChanged, this.get('messageCommentAddedId'));

            },
            onCurrentTimerIDChanged: function () {
                //console.log('****** AppModel:onCurrentTimerIDChanged:' + this.get("currentTimerID"));

                $.cookie('ESA:AppModel:currentTimerID', this.get("currentTimerID"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.CurrentTimerIDChanged, this.get('currentTimerID'));

            },
            onIntTimerIDChanged: function () {
                //console.log('****** AppModel:onIntTimerIDChanged:' + this.get("intTimerID"));

                $.cookie('ESA:AppModel:intTimerID', this.get("intTimerID"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.IntTimerIDChanged, this.get('intTimerID'));

            },
            onCurrentTimerServiceIDChanged: function () {
                //console.log('****** AppModel:onCurrentTimerServiceIDChanged:' + this.get("currentTimerServiceID"));

                $.cookie('ESA:AppModel:currentTimerServiceID', this.get("currentTimerServiceID"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.CurrentTimerServiceIDChanged, this.get('currentTimerServiceID'));

            },
            onCurrentTimerClientIDChanged: function () {
                //console.log('****** AppModel:onCurrentTimerClientIDChanged:' + this.get("currentTimerClientID"));

                $.cookie('ESA:AppModel:currentTimerClientID', this.get("currentTimerClientID"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.CurrentTimerClientIDChanged, this.get('currentTimerClientID'));

            },
            onResetFileList: function () {

                //console.log('****** AppModel:onResetFileList:' + this.get("resetFileList"));

                $.cookie('ESA:AppModel:resetFileList', this.get("resetFileList"));
                if (this.get('resetFileList') === true) {
                    this.Application.vent.trigger(this.Application.Events.ModelEvents.ResetFileList, this.get('resetFileList'));
                    this.set('resetFileList', false);
                    //console.log('****** AppModel:onResetFileList:afterReset:' + this.get("resetFileList"));
                }

            },
            onWidget1ValueChanged: function () {

                //console.log('****** AppModel:onWidget1ValueChanged:widget1Value:' + this.get("widget1Value"));
                $.cookie('ESA:AppModel:widget1Value', this.get("widget1Value"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.Widget1ValueChanged, this.get('widget1Value'));

            },
            onWidget2ValueChanged: function () {

                //console.log('****** AppModel:onWidget2ValueChanged:widget2Value:' + this.get("widget2Value"));
                $.cookie('ESA:AppModel:widget2Value', this.get("widget2Value"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.Widget2ValueChanged, this.get('widget2Value'));

            },
            onWidget3ValueChanged: function () {

                //console.log('****** AppModel:onWidget3ValueChanged:widget3Value:' + this.get("widget3Value"));
                $.cookie('ESA:AppModel:widget3Value', this.get("widget3Value"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.Widget3ValueChanged, this.get('widget3Value'));

            },
            onWidget4ValueChanged: function () {

                //console.log('****** AppModel:onWidget4ValueChanged:widget4Value:' + this.get("widget4Value"));
                $.cookie('ESA:AppModel:widget4Value', this.get("widget4Value"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.Widget4ValueChanged, this.get('widget4Value'));

            },
            onMySumTotalTimeChanged: function () {

                //console.log('****** AppModel:onMySumTotalTimeChanged:mySumTotalTime:' + this.get("mySumTotalTime"));
                $.cookie('ESA:AppModel:mySumTotalTime', this.get("mySumTotalTime"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.MySumTotalTimeChanged, this.get('mySumTotalTime'));

            },
            onMySumTotalMeetingsChanged: function () {

                //console.log('****** AppModel:onMySumTotalMeetingsChanged:mySumTotalMeetings:' + this.get("mySumTotalMeetings"));
                $.cookie('ESA:AppModel:mySumTotalMeetings', this.get("mySumTotalMeetings"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.MySumTotalMeetingsChanged, this.get('mySumTotalMeetings'));

            },
            onMySumMeetingsTargetChanged: function () {

                //console.log('****** AppModel:onMySumMeetingsTargetChanged:mySumMeetingsTarget:' + this.get("mySumMeetingsTarget"));
                $.cookie('ESA:AppModel:mySumMeetingsTarget', this.get("mySumMeetingsTarget"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.MySumMeetingsTargetChanged, this.get('mySumMeetingsTarget'));

            },
            onMyMtgPercentChanged: function () {

                //console.log('****** AppModel:onMyMtgPercentChanged:myMtgPercent:' + this.get("myMtgPercent"));
                $.cookie('ESA:AppModel:myMtgPercent', this.get("myMtgPercent"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.MyMtgPercentChanged, this.get('myMtgPercent'));

            },
            onTeamSumTotalTimeChanged: function () {

                //console.log('****** AppModel:onTeamSumTotalTimeChanged:teamSumTotalTime:' + this.get("teamSumTotalTime"));
                $.cookie('ESA:AppModel:teamSumTotalTime', this.get("teamSumTotalTime"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TeamSumTotalTimeChanged, this.get('teamSumTotalTime'));

            },
            onTeamSumTotalMeetingsChanged: function () {

                //console.log('****** AppModel:onTeamSumTotalMeetingsChanged:teamSumTotalMeetings:' + this.get("teamSumTotalMeetings"));
                $.cookie('ESA:AppModel:teamSumTotalMeetings', this.get("teamSumTotalMeetings"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TeamSumTotalMeetingsChanged, this.get('teamSumTotalMeetings'));

            },
            onTeamSumMeetingsTargetChanged: function () {

                //console.log('****** AppModel:onTeamSumMeetingsTargetChanged:teamSumMeetingsTarget:' + this.get("teamSumMeetingsTarget"));
                $.cookie('ESA:AppModel:teamSumMeetingsTarget', this.get("teamSumMeetingsTarget"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TeamSumMeetingsTargetChanged, this.get('teamSumMeetingsTarget'));

            },
            onTeamMtgPercentChanged: function () {

                //console.log('****** AppModel:onTeamMtgPercentChanged:teamMtgPercent:' + this.get("teamMtgPercent"));
                $.cookie('ESA:AppModel:teamMtgPercent', this.get("teamMtgPercent"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TeamMtgPercentChanged, this.get('teamMtgPercent'));

            },
            onClientNameRecordsChanged: function () {

                //console.log('****** AppModel:onClientNameRecordsChanged:clientNameRecords:' + this.get("clientNameRecords"));
                $.cookie('ESA:AppModel:clientNameRecords', this.get("clientNameRecords"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ClientNameRecordsChanged, this.get('clientNameRecords'));

            },
            onClientAllNameRecordsChanged: function () {

                //console.log('****** AppModel:onClientAllNameRecordsChanged:clientAllNameRecords:' + JSON.stringify(this.get("clientAllNameRecords")));
                $.cookie('ESA:AppModel:clientAllNameRecords', this.get("clientAllNameRecords"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ClientAllNameRecordsChanged, this.get('clientAllNameRecords'));

            },
            onClientCompanyRecordsChanged: function () {

                //console.log('****** AppModel:onClientCompanyRecordsChanged:clientCompanyRecords:' + this.get("clientCompanyRecords"));
                $.cookie('ESA:AppModel:clientCompanyRecords', this.get("clientCompanyRecords"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ClientCompanyRecordsChanged, this.get('clientCompanyRecords'));

            },
            onPlanRecordsChanged: function () {

                //console.log('****** AppModel:onPlanRecordstChanged:planRecords:' + this.get("planRecords"));
                $.cookie('ESA:AppModel:planRecords', this.get("planRecords"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.PlanRecords, this.get('planRecords'));

            },
            onEmployeeIdsNotUserType1Changed: function () {

                //console.log('****** AppModel:onEmployeeIdsNotUserType1Changed:employeeIdsNotUserType1:' + this.get("employeeIdsNotUserType1"));
                $.cookie('ESA:AppModel:employeeIdsNotUserType1', this.get("employeeIdsNotUserType1"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.EmployeeIdsNotUserType1Changed, this.get('employeeIdsNotUserType1'));

            },
            onTeamLeaderRecordsChanged: function () {

                //console.log('****** AppModel:onTeamLeaderRecordsChanged:teamLeaderRecords:' + JSON.stringify(this.get("teamLeaderRecords")));
                $.cookie('ESA:AppModel:teamLeaderRecords', this.get("teamLeaderRecords"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TeamLeaderRecordsChanged, this.get('teamLeaderRecords'));

            },
            onEmployeeGridFilterChanged: function () {

                //console.log('****** AppModel:onClientDataGridFilterChanged:employeeGridFilter:' + JSON.stringify(this.get("employeeGridFilter")));
                $.cookie('ESA:AppModel:employeeGridFilter', this.get("employeeGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.EmployeeGridFilterChanged, this.get('employeeGridFilter'));

            },
            onClientDataGridFilterChanged: function () {

                //console.log('****** AppModel:onClientDataGridFilterChanged:clientDataGridFilter:' + JSON.stringify(this.get("clientDataGridFilter")));
                $.cookie('ESA:AppModel:clientDataGridFilter', this.get("clientDataGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ClientDataGridFilterChanged, this.get('clientDataGridFilter'));

            },
            onClientReportGridFilterChanged: function () {

                //console.log('****** AppModel:onClientReportGridFilterChanged:clientReportGridFilter:' + JSON.stringify(this.get("clientReportGridFilter")));
                $.cookie('ESA:AppModel:clientReportGridFilter', this.get("clientReportGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ClientReportGridFilterChanged, this.get('clientReportGridFilter'));

            },
            onClientContactGridFilterChanged: function () {

                //console.log('****** AppModel:onClientContactGridFilterChanged:clientContactGridFilter:' + JSON.stringify(this.get("clientContactGridFilter")));
                $.cookie('ESA:AppModel:clientContactGridFilter', this.get("clientContactGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ClientContactGridFilterChanged, this.get('clientContactGridFilter'));

            },
            onClientPasswordGridFilterChanged: function () {

                //console.log('****** AppModel:onClientPasswordGridFilterChanged:clientPasswordGridFilter:' + JSON.stringify(this.get("clientPasswordGridFilter")));
                $.cookie('ESA:AppModel:clientPasswordGridFilter', this.get("clientPasswordGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ClientPasswordGridFilterChanged, this.get('clientPasswordGridFilter'));

            },
            onClientAssignmentGridFilterChanged: function () {

                //console.log('****** AppModel:onClientAssignmentGridFilterChanged:clientAssignmentGridFilter:' + JSON.stringify(this.get("clientAssignmentGridFilter")));
                $.cookie('ESA:AppModel:clientAssignmentGridFilter', this.get("clientAssignmentGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.ClientAssignmentGridFilterChanged, this.get('clientAssignmentGridFilter'));

            },
            onTimeCardDetailGridFilterChanged: function () {

                //console.log('****** AppModel:onTimeCardDetailGridFilterChanged:timeCardDetailGridFilter:' + JSON.stringify(this.get("timeCardDetailGridFilter")));
                $.cookie('ESA:AppModel:timeCardDetailGridFilter', this.get("timeCardDetailGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.TimeCardDetailGridFilterChanged, this.get('timeCardDetailGridFilter'));

            },
            onUserGridFilterChanged: function () {

                //console.log('****** AppModel:onUserGridFilterChanged:userGridFilter:' + JSON.stringify(this.get("userGridFilter")));
                $.cookie('ESA:AppModel:userGridFilter', this.get("userGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.UserGridFilterChanged, this.get('userGridFilter'));

            },
            onAdminFeedGridFilterChanged: function () {

                //console.log('****** AppModel:onAdminFeedGridFilterChanged:adminIndustryNewsGridFilter:' + JSON.stringify(this.get("adminIndustryNewsGridFilter")));
                $.cookie('ESA:AppModel:adminIndustryNewsGridFilter', this.get("adminIndustryNewsGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.AdminFeedGridFilterChanged, this.get('adminIndustryNewsGridFilter'));

            },
            onAdminStaticGridFilterChanged: function () {

                //console.log('****** AppModel:onAdminStaticGridFilterChanged:adminStaticGridFilter:' + JSON.stringify(this.get("adminStaticGridFilter")));
                $.cookie('ESA:AppModel:adminStaticGridFilter', this.get("adminStaticGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.AdminStaticGridFilterChanged, this.get('adminStaticGridFilter'));

            },
            onAdminBioGridFilterChanged: function () {

                //console.log('****** AppModel:onAdminBioGridFilterChanged:adminBioGridFilter:' + JSON.stringify(this.get("adminBioGridFilter")));
                $.cookie('ESA:AppModel:adminBioGridFilter', this.get("adminBioGridFilter"));
                this.Application.vent.trigger(this.Application.Events.ModelEvents.AdminBioGridFilterChanged, this.get('adminBioGridFilter'));

            },



        //**************************************************************
            //* Event Triggers - trigger all events through this method...
            //*                  it is refined to events that do not have
            //*                  state enforcing App Singleton and MVC rules
            //**************************************************************
            triggerEvent: function (event, model) {

                // Prevent model changes from firing events using this method...i.e.
                // ensure values are changed on the model prior to firing events
                for (var key in this.Application.Events.ModelEvents) {
                    if (key == event) {
                        throw new UserException("Invalid Event Path!");
                    }
                }

                this.Application.vent.trigger(event, model);
            },
            //**************************************************************
            //* Methods
            //**************************************************************


            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function (attrs) {
                //console.log("--------------- AppModel:validate ---------------");
            }

        });

        // Returns the Model class
        return Model;
    }
);