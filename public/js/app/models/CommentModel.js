define(["jquery", "backbone"],
    function($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            // Model Constructor
            initialize: function() {
                //console.log('CommentModel:initialize');

            },
            idAttribute: 'comment_id',
            // Default values for all of the Model attributes
            defaults: {
                comment_id: null,
                message_id: null,
                comment_text: null,
                time_stamp:null,
                user_key: null,
                first_name: null,
                last_name: null,
                fileName: null,
                reply_comment_id: null,
                addUserImage: null,
                isEditable: false
            },

            // Gets called automatically by Backbone when the set and/or save methods are called (Add your own logic)
            validate: function(attrs) {

            }

        });

        // Returns the Model class
        return Model;
    }
);