define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/aside','adminLTE','moment'],
    function(App, Backbone, Marionette, $, template ) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend( {
            events:{
                //'click #toggleBannerButton': 'onToggleBannerButtonClicked',
            },
            template: template,
            initialize: function() {
                //console.log('AsideView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                var self = this;
                self.userType = parseInt(App.model.get('userType'),0);

                var lastCompanyMessagesView = new Date(0);
                var lastCMView = App.model.get('lastCompanyMessagesView');
                if (lastCMView !== null && lastCMView !== "" && lastCMView !== undefined) {
                    lastCompanyMessagesView = new Date(lastCMView);
                }
                lastCompanyMessagesView.setHours(23);

                var lastESAMediaView = new Date(0);
                var lastEMView = App.model.get('lastESAMediaView');
                if (lastEMView !== null && lastEMView !== "" && lastEMView !== undefined) {
                    lastESAMediaView = new Date(lastEMView);
                }
                lastESAMediaView.setHours(23);

                self.today = moment().clone().format('YYYY-MM-DD');
                this.openTimerDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        transport: {
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetOpenTimers/" + self.today;
                                    return url;
                                },
                                complete: function (e) {
                                    console.log('AsideView:openTimerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:openTimerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {
                            model: {
                                id: "intTimerID",
                                fields: {

                                    intTimerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    employeeID: {editable: false, type: "number"},
                                    serviceID: {editable: false, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    clientName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: true}},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   AsideView:messageDataSource:requestStart');

                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                        },
                        change: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
                        }
                    });

                this.endOfToday = new Date();
                this.endOfToday.setHours(22);
                this.companyMessagesDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        autoSync: false,
                        serverFiltering: true,
                        serverSorting: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                complete: function (e) {
                                    console.log('AsideView:adminDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('AsideView:update');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('AsideView:adminDataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:adminDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('AsideView:create');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('AsideView:adminDataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:adminDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('AsideView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('AsideView:adminDataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:adminDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    title: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Message Title Here)"
                                    },
                                    text: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Message Text Here)"
                                    },
                                    text2: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:""
                                    },
                                    shortText: {editable: true, type: "string"},
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:""
                                    },
                                    category: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:0
                                    },
                                    subCategory: {editable: true, type: "number"},
                                    priority: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:0
                                    },
                                    showAsCompanyMessage: {editable: true, type: "boolean"},
                                    addUserImage: {editable: true, type: "boolean"},
                                    timeStamp: {
                                        editable: true,
                                        type: "date",
                                        defaultValue: function () {
                                            var offset = (new Date()).getTimezoneOffset();
                                            var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                            return now;
                                        }
                                    }
                                }
                            }
                        },
                        sort: [
                            //{field: "priority", dir: "asc"},
                            {field: "timeStamp", dir: "desc"}
                        ],
                        requestStart: function (e) {
                            //console.log('AsideView:dataSource:request start:');
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('AsideView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), false);
                        },
                        change: function (e) {
                            //console.log('AsideView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('AsideView:dataSource:error');
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), false);
                        },
                        filter: [
                            {field: "category", operator: "eq", value: 0},
                            {field: "timeStamp", operator: "gte", value: lastCompanyMessagesView},
                            {field: "timeStamp", operator: "lte", value: this.endOfToday}
                        ]
                    });

                this.esaMediaDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        autoSync: false,
                        serverFiltering: true,
                        serverSorting: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                complete: function (e) {
                                    console.log('AsideView:esaMediaDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:esaMediaDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('AsideView:update');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('AsideView:esaMediaDataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:esaMediaDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('AsideView:create');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('AsideView:esaMediaDataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:esaMediaDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('AsideView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('AsideView:esaMediaDataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'AsideView:esaMediaDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    title: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Message Title Here)"
                                    },
                                    text: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Message Text Here)"
                                    },
                                    text2: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:""
                                    },
                                    shortText: {editable: true, type: "string"},
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:""
                                    },
                                    category: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:0
                                    },
                                    subCategory: {editable: true, type: "number"},
                                    priority: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:0
                                    },
                                    showAsCompanyMessage: {editable: true, type: "boolean"},
                                    addUserImage: {editable: true, type: "boolean"},
                                    timeStamp: {
                                        editable: true,
                                        type: "date",
                                        defaultValue: function () {
                                            var offset = (new Date()).getTimezoneOffset();
                                            var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                            return now;
                                        }
                                    }
                                }
                            }
                        },
                        sort: [
                            //{field: "priority", dir: "asc"},
                            {field: "timeStamp", dir: "desc"}
                        ],
                        requestStart: function (e) {
                            //console.log('AsideView:dataSource:request start:');
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('AsideView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), false);
                        },
                        change: function (e) {
                            //console.log('AsideView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('AsideView:dataSource:error');
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), false);
                        },
                        filter: [
                            {field: "category", operator: "eq", value: 1},
                            {field: "timeStamp", operator: "gte", value: lastESAMediaView},
                            {field: "timeStamp", operator: "lte", value: this.endOfToday}
                        ]
                    });

                this.listenTo(App.vent, App.Events.ModelEvents.LastCompanyMessagesViewChanged, this.onLastCompanyMessagesViewChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.LastESAMediaViewChanged, this.onLastESAMediaViewChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.UpdatedOpenTimerChanged, this.onUpdatedOpenTimerChanged);


            },
            onShow: function(){
                console.log('AsideView:onShow');

                this.getData();

            },
            getData: function () {
                console.log('AsideView:getData');

                var self = this;

                this.$('.executive').css('display', 'none');

                this.companyMessagesDataSource.fetch(function () {
                    var messageData = this.data();
                    if (messageData.length > 0) {
                        console.log('AsideView:onShow:company messages count:' + messageData.length);
                        self.$('#lastCompanyMessagesViewCount').show();
                        self.$('#lastCompanyMessagesViewCount').html(messageData.length);
                    } else {
                        console.log('AsideView:onShow:no unseen company messages');
                        self.$('#lastCompanyMessagesViewCount').hide();
                    }
                });

                this.esaMediaDataSource.fetch(function () {
                    var emData = this.data();
                    if (emData.length > 0) {
                        console.log('AsideView:onShow:ESA Media count:' + emData.length);
                        self.$('#lastESAMediaViewCount').show();
                        self.$('#lastESAMediaViewCount').html(emData.length);
                    } else {
                        console.log('AsideView:onShow:no unseen ESA Media messages');
                        self.$('#lastESAMediaViewCount').hide();
                    }
                });

                if (self.userType === 3) {
                    this.$('.manager').css('display', 'block');
                    this.$('.executive').css('display', 'block');

                    if (self.userType === 3) {
                        this.openTimerDataSource.fetch(function () {

                            var data = this.data();
                            if (data.length > 0) {
                                console.log('AsideView:open timers count:' + data.length);
                                $('#openTimersCount').html(data.length);
                            } else {
                                console.log('AsideView:no open timers');
                                $('#openTimersCount').hide();
                            }

                        });
                    }
                } else if (self.userType === 2) {
                    //hide executive
                    this.$('.header.manager').css('display', 'block');
                    this.$('.executive').css('display', 'none');

                    // Specialized hiding for userType2
                    this.hidePlans = App.model.get('hidePlans');
                    this.hideStatsTimecard = App.model.get('hideStatsTimecard');
                    this.hideLevel1Employees = App.model.get('hideLevel1Employees');
                    this.hideClients = App.model.get('hideClients');
                    this.hideCompanies = App.model.get('hideCompanies');

                    this.$('#views').css('display', 'block');
                    if (this.hidePlans) {
                        this.$('.plans.manager').css('display', 'none');
                    } else {
                        this.$('.plans.manager').css('display', 'block');
                    }
                    if (this.hideStatsTimecard) {
                        this.$('.statsTimecard.manager').css('display', 'none');
                    } else {
                        this.$('.statsTimecard.manager').css('display', 'block');
                    }
                    if (this.hideLevel1Employees) {
                        this.$('.employees.manager').css('display', 'none');
                    } else {
                        this.$('.employees.manager').css('display', 'block');
                    }
                    if (this.hideClients) {
                        this.$('.clientReports.manager').css('display', 'none');
                    } else {
                        this.$('.clientReports.manager').css('display', 'block');
                    }
                    if (this.hideCompanies) {
                        this.$('.companies.manager').css('display', 'none');
                    } else {
                        this.$('.companies.manager').css('display', 'block');
                    }

                } else if (self.userType === 1) {
                    //hide manager and executive
                    this.$('.manager').css('display', 'none');
                    this.$('.executive').css('display', 'none');
                } else if (self.userType === 0) {
                    //hide manager and executive
                    this.$('.manager').css('display', 'none');
                    this.$('.executive').css('display', 'none');
                    this.$('.scheduler').css('display', 'none');
                } else {
                    // If userType not defined, discover it again
                }

            },
            onUpdatedOpenTimerChanged: function () {
                console.log('AsideView:onUpdatedOpenTimerChanged');
                this.getData();
            },
            onLastESAMediaViewChanged: function () {
                console.log('AsideView:onLastESAMediaViewChanged');

                var self = this;

                var lastESAMediaView = new Date(0);
                var lastView = App.model.get('lastESAMediaView');
                if (lastView !== null && lastView !== "" && lastView !== undefined) {
                    lastESAMediaView = new Date(lastView);
                }
                lastESAMediaView.setHours(23);

                this.esaMediaDataSource.filter([
                    {field: "category", operator: "eq", value: 1},
                    {field: "timeStamp", operator: "gte", value: lastESAMediaView},
                    {field: "timeStamp", operator: "lte", value: this.endOfToday}
                ]);
                this.esaMediaDataSource.fetch(function () {
                    var emData = this.data();
                    if (emData.length > 0) {
                        console.log('AsideView:onLastESAMediaViewChanged:ESA Media count:' + emData.length);
                        self.$('#lastESAMediaViewCount').show();
                        self.$('#lastESAMediaViewCount').html(emData.length);
                    } else {
                        console.log('AsideView:onLastESAMediaViewChanged:no unseen ESA Media');
                        self.$('#lastESAMediaViewCount').hide();
                    }
                });

            },
            onLastCompanyMessagesViewChanged: function () {
                console.log('AsideView:onLastCompanyMessagesViewChanged');

                var self = this;

                var lastCompanyMessagesView = new Date(0);
                var lastView = App.model.get('lastCompanyMessagesView');
                if (lastView !== null && lastView !== "" && lastView !== undefined) {
                    lastCompanyMessagesView = new Date(lastView);
                }
                lastCompanyMessagesView.setHours(23);

                this.companyMessagesDataSource.filter([
                    {field: "category", operator: "eq", value: 0},
                    {field: "timeStamp", operator: "gte", value: lastCompanyMessagesView},
                    {field: "timeStamp", operator: "lte", value: this.endOfToday}
                ]);
                this.companyMessagesDataSource.fetch(function () {
                    var messageData = this.data();
                    if (messageData.length > 0) {
                        console.log('AsideView:onLastCompanyMessagesViewChanged:company messages count:' + messageData.length);
                        self.$('#lastCompanyMessagesViewCount').show();
                        self.$('#lastCompanyMessagesViewCount').html(messageData.length);
                    } else {
                        console.log('AsideView:onLastCompanyMessagesViewChanged:no unseen company messages');
                        self.$('#lastCompanyMessagesViewCount').hide();
                    }
                });

            },
            _pageView: function (tab) {
                var path = Backbone.history.getFragment();
                var prefix = "";
                if (App.mobile) {
                    if (App.phone)
                        prefix = 'phone-';
                    else
                        prefix = 'tablet-';
                }
                ga('send', 'pageview', {page: prefix + "/" + path + "#" + tab});
            },
            onResize: function () {
                //console.log('AsideView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('AsideView:resize');

                var width = $(window).width();
            },
            remove: function () {
                //console.log('---------- AsideView:remove ----------');

                // Turn off events
                $(window).off("resize", this.onResize);
                $(document).off("webkitfullscreenchange mozfullscreenchange fullscreenchange", this.onFullScreenChange);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }

        });
    });