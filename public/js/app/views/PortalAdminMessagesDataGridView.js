define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalAdminMessagesDataGrid',
        //'hbs!templates/editPortalAdminMessagesDataPopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data', 'kendo/kendo.upload'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalAdminMessagesDataGridView:initialize');

                _.bindAll(this);

                var self = this;
                self.categories = [
                    {text: 'Company Message', value: 0},
                    {text: 'ESA Media', value: 1},
                    {text: 'Industry News', value: 2},
                    {text: 'Social Media', value: 3},
                    {text: 'Alert Message', value: 4},
                    {text: 'Company Chat', value: 5}
                ];

                self.priorities = [
                    {text: 'N/A', value: 0},
                    {text: '1', value: 1},
                    {text: '2', value: 2},
                    {text: '3', value: 3},
                    {text: '4', value: 4},
                    {text: '5', value: 5},
                    {text: '6', value: 6},
                    {text: '7', value: 7},
                    {text: '8', value: 8}
                ];
                this.adminMessagesGridFilter = App.model.get('adminMessagesGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        autoSync: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                complete: function (e) {
                                    console.log('PortalAdminMessagesDataGridView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminMessagesDataGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalAdminMessagesDataGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminMessagesDataGridView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminMessagesDataGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalAdminMessagesDataGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminMessagesDataGridView:dataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminMessagesDataGridView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalAdminMessagesDataGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminMessagesDataGridView:dataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminMessagesDataGridView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    title: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Message Title Here)"
                                    },
                                    text: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Message Text Here)"
                                    },
                                    text2: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:""
                                    },
                                    shortText: {editable: true, type: "string"},
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:""
                                    },
                                    category: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:0
                                    },
                                    subCategory: {editable: true, type: "number"},
                                    priority: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:0
                                    },
                                    showAsCompanyMessage: {editable: true, type: "boolean"},
                                    addUserImage: {editable: true, type: "boolean"},
                                    timeStamp: {
                                        editable: true,
                                        type: "date"
                                        // defaultValue: function () {
                                        //     var offset = (new Date()).getTimezoneOffset();
                                        //     var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                        //     return now;
                                        // }
                                    },
                                    showStart: {
                                        editable: true,
                                        type: "date"
                                        // defaultValue: function () {
                                        //     var offset = (new Date()).getTimezoneOffset();
                                        //     var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                        //     return now;
                                        // }
                                    },
                                    showStop: {
                                        editable: true,
                                        type: "date"
                                        // defaultValue: function () {
                                        //     var offset = (new Date()).getTimezoneOffset();
                                        //     var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                        //     return now;
                                        // }
                                    }
                                }
                            }
                        },
                        sort: [
                            {field: "timeStamp", dir: "desc"}
                        ],
                        requestStart: function (e) {
                            //console.log('PortalAdminMessagesDataGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalAdminMessagesDataGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalAdminMessagesDataGridView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalAdminMessagesDataGridView:dataSource:error');
                            kendo.ui.progress(self.$("#adminMessagesDataLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalAdminMessagesDataGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#adminMessagesData').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onRender: function () {
                //console.log('PortalAdminMessagesDataGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminMessagesDataGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#adminMessagesData").kendoGrid(
                    {
                        toolbar: ["create"],
                        editable: true, //"incell",
                        //editable: {
                        //    mode: "popup",
                        //    window: {
                        //        title: "Edit Company Messages Record"
                        //    },
                        //    template: function (e) {
                        //        var template = EditPortalAdminMessagesDataPopup();
                        //        console.log('PortalAdminMessagesDataGridView:onShow:editable:template');
                        //        return template;
                        //    }
                        //},
                        selectable: "cell",
                        navigatable: true,
                        sortable: true,
                        extra: false,
                        filterable: {

                            mode: "row"
                        },
                        resizable: true,
                        reorderable: true,
                        columns: [
                            //{
                            //    width: 25,
                            //    template: function (e) {
                            //        var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction k-grid-edit' data-id='" + e.ID + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                            //        return template;
                            //    },
                            //    filterable: false,
                            //    sortable: true
                            //},
                            {
                                width: 25,
                                template: function (e) {
                                    var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.ID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                    return template;
                                },
                                filterable: false,
                                sortable: true
                            },
                            {
                                field: "priority",
                                title: "Priority",
                                template: function (e) {
                                    var template = e.priority;
                                    if (self.priorities[e.priority])
                                        template = self.priorities[e.priority].text;
                                    return template;
                                },
                                width: 70,
                                filterable: false,
                                editor: function(container, options) {
                                    var input = $("<input/>");
                                    input.attr("name", options.field);
                                    input.appendTo(container);
                                    input.kendoDropDownList({
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        dataSource: self.priorities
                                    });
                                }
                            },
                            {
                                field: "title",
                                title: "Title",
                                width: 150,
                                filterable: false,
                                editor: function(container, options) {
                                    var input = $('<textarea type="text" class="k-input k-textbox" style="width: 140px;height: 100px" ></textarea>');
                                    input.attr("name", options.field);
                                    input.appendTo(container);
                                    input.kendoMaskedTextBox();
                                }
                            }, {
                                field: "text",
                                title: "Text",
                                width: 310,
                                filterable: false,
                                editor: function(container, options) {
                                    var input = $('<textarea type="text" class="k-input k-textbox" style="width: 300px;height: 100px" ></textarea>');
                                    input.attr("name", options.field);
                                    input.appendTo(container);
                                    input.kendoMaskedTextBox();
                                }
                            },
                            {
                                field: "fileName",
                                title: "Image File",
                                template: function (e) {
                                    var template;
                                    if (e.fileName === "" || e.fileName === null || e.fileName === undefined) {
                                        template = "<div style='text-align: center;cursor:pointer'><a>" + "Upload Image..." + "</a></div>";
                                    } else {
                                        var fileName = e.fileName;
                                        if (fileName.length > 30) {
                                            fileName = fileName.substring(0,29) + " ...";
                                        }
                                        template = "<div style='text-align: center;cursor:pointer'><a>" + fileName + "</a></div>";
                                    }
                                    return template;
                                },
                                width: 300,
                                filterable: false,
                                editor: function(container, e) {
                                    var input = $('<input name="fileName" id="fileName" type="file" aria-label="files" style="width: 300px"/>');
                                    input.appendTo(container);

                                    console.log('PortalAdminMessagesDataGridView:def:onShow:e:' + JSON.stringify(e));

                                    var fileName = e.model.fileName;
                                    if (fileName.length > 15) {
                                        fileName = fileName.substring(0,14) + " ...";
                                    }
                                    var files = [{
                                        name: fileName,
                                        size: 600,
                                        extension: ""
                                    }];

                                    input.kendoUpload({
                                        success: self.onSuccess,
                                        remove: self.onRemove,
                                        async: {
                                            withCredentials: false,
                                            saveUrl: App.config.DataServiceURL + "/rest/files/AdminUpload/CompanyMessage",
                                            removeUrl: App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/CompanyMessage"
                                        },
                                        multiple: false,
                                        files: e.model.fileName !== "" ? files : []
                                    });
                                }
                            },
                            {
                                field: "externalLink",
                                title: "Link",
                                width: 150,
                                filterable: false,
                                editor: function(container, options) {
                                    var input = $('<textarea type="text" class="k-input k-textbox" style="width: 140px;height: 100px" ></textarea>');
                                    input.attr("name", options.field);
                                    input.appendTo(container);
                                    input.kendoMaskedTextBox();
                                }
                            },
                            {
                                field: "text2",
                                title: "External Link <br/>Button Title",
                                width: 100,
                                filterable: false,
                                editor: function(container, e) {
                                    var input = $('<textarea name="text2" type="text" class="k-input k-textbox" style="width: 90px;height: 100px"></textarea>');
                                    //input.attr("name", e.field);
                                    input.appendTo(container);
                                    input.kendoMaskedTextBox();
                                }
                            },
                            {
                                field: "showAsCompanyMessage",
                                title: "Show Message on <br/>Welcome Page",
                                template: function (e) {
                                    var template;
                                    if (e.showAsCompanyMessage === false) {
                                        template = "<div style='text-align: center'>No</div>";
                                        return template;
                                    } else {
                                        template = "<div style='text-align: center'>Yes</div>";
                                        return template;
                                    }
                                },
                                width: 110,
                                filterable: false,
                                editor: function(container, options) {
                                    var input = $("<input/>");
                                    input.attr("name", options.field);
                                    input.appendTo(container);
                                    input.kendoDropDownList({
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        dataSource: [
                                            {text: 'Yes', value: 'true'},
                                            {text: 'No', value: 'false'}
                                        ]
                                    });
                                }
                            },
                            {
                                field: "timeStamp",
                                title: "Message Date",
                                width: 90,
                                format: "{0:M/d/yyyy}",
                                filterable: false
                            },
                            {
                                field: "showStart",
                                title: "Show Start Date",
                                width: 90,
                                format: "{0:M/d/yyyy}",
                                filterable: false
                            },
                            {
                                field: "showStop",
                                title: "Show Stop Date",
                                width: 90,
                                format: "{0:M/d/yyyy}",
                                filterable: false
                            }
                        ],
                        change: function (e) {
                            console.log('PortalAdminMessagesDataGridView:def:onShow:onChange'); //:e.model:' + JSON.stringify(e.model));
                        },
                        save: function (e) {
                            e.model.dirty = true;
                            e.model.title = e.model.title.trim();
                            e.model.text = e.model.text.trim();
                            e.model.text2 = e.model.text2.trim();
                            e.model.externalLink = e.model.externalLink.trim();
                            e.model.shortText = e.model.shortText.trim();
                            e.model.timeStamp.setHours(9);
                            e.model.showStart.setHours(9);
                            e.model.showStop.setHours(9);
                            console.log('PortalAdminMessagesDataGridView:def:onShow:onSave:e.model:text:' + e.model.text);
                            
                            //console.log('PortalAdminMessagesDataGridView:def:onShow:onSave:e.model:text2:' + e.model.text2);

                            console.log('PortalAdminMessagesDataGridView:def:onShow:onSave:e.model:timeStamp:' + e.model.timeStamp);
                            console.log('PortalAdminMessagesDataGridView:def:onShow:onSave:e.model:showStart:' + e.model.showStart);
                            console.log('PortalAdminMessagesDataGridView:def:onShow:onSave:e.model:showStop:' + e.model.showStop);
                        },
                        edit: function (e) {
                            console.log('PortalAdminMessagesDataGridView:onShow:edit');

                            // Disable the ID editor
                            //e.container.find("input[name='ID']").prop("disabled", true);

                            e.model.category = 0;

                            // For new records set priority
                            if (e.model.ID === 0) {
                                //$('#idGroup').hide();
                                //$('#fileGroup').hide();
                                //$('.k-window-title').html('Add Company Messages Record');
                                e.model.priority = 0;
                            }

                            self.fileName = e.model.fileName;
                            console.log('PortalAdminMessagesDataGridView:onShow:edit:fileName:' + self.fileName);

                            self.category = self.categories[e.model.category].text.replace(" ", "");
                            console.log('PortalAdminMessagesDataGridView:onShow:edit:category:' + self.category);

                            console.log('PortalAdminMessagesDataGridView:def:onShow:edit:e.model:timeStamp:' + e.model.timeStamp);
                            console.log('PortalAdminMessagesDataGridView:def:onShow:edit:e.model:showStart:' + e.model.showStart);
                            console.log('PortalAdminMessagesDataGridView:def:onShow:edit:e.model:showStop:' + e.model.showStop);

                            self.model = e.model;

                        },
                        dataBound: function (e) {
                            console.log("PortalAdminMessagesDataGridView:def:onShow:dataBound");
                           
                            //self.$('.k-grid-edit').on('click', self.editPopup);

                            // save current filter
                            var filters = null;
                            if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                                filters = e.sender.dataSource.filter().filters;
                                if (filters.length === 0) {
                                    filters.push({field: "category", operator: "eq", value: 0});
                                }
                                var adminMessagesGridFilter = [];

                                $.each(filters, function (index, value) {
                                    adminMessagesGridFilter.push(value);
                                });

                                console.log("PortalAdminMessagesDataGridView:def:onShow:dataBound:set adminMessagesGridFilter:" + JSON.stringify(adminMessagesGridFilter));
                                app.model.set('adminMessagesGridFilter', adminMessagesGridFilter);
                            }

                            $('.k-grid-add').unbind("click");
                            $('.k-grid-add').bind("click", function(){
                                console.log("PortalAdminMessagesDataGridView:def:onShow:dataBound:Add");
                                var grid = self.$("#adminMessagesData").data("kendoGrid");
                                grid.dataSource.sort({field: "timeStamp", dir: "desc"});
                            });
                        }
                    }
                );

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            onRemove: function (e) {
                if (e && e.sender) {
                    console.log('PortalAdminMessagesDataGridView:onRemove:');
                    e.sender.options.async.removeUrl = App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/" + this.category + "/" + this.fileName + "/";
                    this.model.fileName = "";
                    this.model.dirty = true;
                    var grid = $("#adminMessagesData").data("kendoGrid");
                    grid.saveChanges();
                    console.log('PortalAdminMessagesDataGridView:onRemove:removeUrl:' + e.sender.options.async.removeUrl);
                }
            },
            onSuccess: function (e) {
                //if (e && e.response) {
                console.log('PortalAdminMessagesDataGridView:onSuccess:response:' + e.response);
                console.log('PortalAdminMessagesDataGridView:onSuccess:category:' + this.category);
                console.log('PortalAdminMessagesDataGridView:onSuccess:ID:' + this.model.ID);
                if (e.response.indexOf("Exception") > -1) {
                } else if (e.response.indexOf("Deleted file") > -1) {
                } else {
                    this.fileName = e.response;
                    this.model.fileName = e.response;
                    this.model.dirty = true;
                    console.log('PortalAdminMessagesDataGridView:onSuccess:fileName changed');
                    var grid = $("#adminMessagesData").data("kendoGrid");
                    grid.saveChanges();
                }
                //else {
                //    createAutoClosingAlert($("#startError"), "<strong>File Error</strong>" + e.response);
                //}
                console.log('PortalAdminMessagesDataGridView:onSuccess:model:' + JSON.stringify(this.model));

                //}
            },
            createAutoClosingAlert: function (selector, html) {
                console.log('PortalAdminMessagesDataGridView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 50000);
            },
            getData: function () {
                //console.log('PortalAdminMessagesDataGridView:getData');

                var self = this;
                var grid = self.$("#adminMessagesData").data("kendoGrid");

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);

                    if (self.adminMessagesGridFilter !== null && self.adminMessagesGridFilter !== [] && self.adminMessagesGridFilter !== undefined) {
                        var filters = [];
                        if (self.adminMessagesGridFilter.length === 0) {
                            filters.push({field: "category", operator: "eq", value: 0});
                        }
                        $.each(self.adminMessagesGridFilter, function (index, value) {
                            filters.push(value);
                        });

                        //console.log("PortalAdminMessagesDataGridView:grid:set datasource:filter" + JSON.stringify(filters));
                        grid.dataSource.filter(filters);
                    } else {
                        grid.dataSource.filter({field: "category", operator: "eq", value: 0});
                    }
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalAdminMessagesDataGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#adminMessagesData").parent().parent().height();
                this.$("#adminMessagesData").height(parentHeight);

                parentHeight = parentHeight - 56;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                this.$("#adminMessagesData").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalAdminMessagesDataGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#adminMessagesData").data('ui-tooltip'))
                    this.$("#adminMessagesData").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });