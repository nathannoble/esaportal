define(['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/home',
        'jquery.easing', 'kendo/kendo.data'],
    function (App, Backbone, Marionette, $, Model, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #esaLogin': 'esaLoginClicked',
                'click #checkEmail': 'checkEmailClicked',
                'click #resetPassword': 'resetPasswordClicked',
                'keydown': 'keyAction'
                //'visibilitychange': 'onVisibilityChanged'
            },
            initialize: function (options) {

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.userId = App.model.get('userId');
                // this.userKey = App.model.get('userKey');
                this.userLogin = App.model.get('userLogin');
                console.log('HomeView:initialize:userId:' + this.userId);
                console.log('HomeView:initialize:userLogin:' + this.userLogin);

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "login"};
                }
                console.log('HomeView:initialize:type:' + this.options.type);

            },
            onRender: function () {
//                //console.log('---------- HomeView:onRender ----------');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.

                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
//                //console.log('---------- HomeView:onShow ----------');

                // Setup the container
                this.resize();

                console.log('HomeView:onShow');

                var self = this;

                if (this.options.type === "checkEmail") {
                    $("#loginBox").hide();
                    $("#emailBox").show();
                } else {
                    $("#loginBox").show();
                    $("#emailBox").hide();
                }

                this.userDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalUserTable?$select=userLogin,userPassword,userID,userType,userKey,firstName,lastName,loginCount,lastLogin,lastLogout,inactive,prevPasswords,lastPasswordChange,lastCompanyMessagesView,lastESAMediaView",
                                complete: function (e) {
                                    console.log('HeaderView:userDataSource:read:complete');
                                 },
                                error: function (e) {
                                    var message = 'HeaderView:userDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                                },
                                complete: function (e) {
                                    console.log('HeaderView:userDataSource:update:complete');
                                 },
                                error: function (e) {
                                    var message = 'HeaderView:userDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "userKey",
                                fields: {
                                    userLogin: {editable: false, type: "string"},
                                    userPassword: {editable: false, type: "string"},
                                    firstName: {editable: false, type: "string"},
                                    lastName: {editable: false, type: "string"},
                                    userID: {editable: false,  type: "string"},
                                    userKey: {editable: false,  type: "string"},
                                    userType: {editable: false, defaultValue: 1, type: "number"},
                                    loginCount: {editable: true, type: "number"},
                                    lastLogin: {editable: true, type: "date"},
                                    lastLogout: {editable: true, type: "date"},
                                    lastPasswordChange: {editable: true, type: "date"},
                                    prevPasswords: {editable: false, type: "string"},
                                    inactive: {editable: false, type: "boolean"},
                                    lastCompanyMessagesView: {editable: true, type: "date"},
                                    lastESAMediaView: {editable: true, type: "date"}
                                }
                            }
                        },
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            kendo.ui.progress($("#loadingEsaUser"), true);
                        },
                        requestEnd: function () {
                            kendo.ui.progress($("#loadingEsaUser"), false);
                        }
                    });

                this.employeeDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('HomeView:employeeDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'HomeView:employeeDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    teamLeaderID: {editable: false, type: "number"},
                                    userKey: {editable: true, type: "string", validation: {required: false}},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    status: {type: "string", validation: {required: false}},
                                    timerManual: {editable: true, type: "boolean"},
                                    timeZoneID: {editable: false, type: "number"},
                                    isTeamLeader: {editable: true, type: "boolean"},
                                    emailAddress: {editable: true, type: "string", validation: {required: false}},
                                    homePhone: {editable: true, type: "string", validation: {required: false}},
                                    esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                    esaTollFree: {editable: true, type: "string", validation: {required: false}},
                                    hidePlans: {editable: true, type: "boolean"},
                                    hideStatsTimecard: {editable: true, type: "boolean"},
                                    hideLevel1Employees: {editable: true, type: "boolean"},
                                    hideClients: {editable: true, type: "boolean"},
                                    hideCompanies: {editable: true, type: "boolean"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            kendo.ui.progress($("#loadingEsaUser"), true);
                        },
                        requestEnd: function () {
                            kendo.ui.progress($("#loadingEsaUser"), false);
                        }
                    });

                if (self.userId === null) {
                    //self.createAutoClosingAlert($("#startWarning"), "<strong>Attention.</strong> Enter a user name.");
                } else {
                    App.router.navigate('home', {trigger: true});
                }

            },
            keyAction: function (e) {
                //console.log('HomeView:keyAction:' + e.which);
                if (e.which === 13) {
                    // enter has been pressed
                    if (this.options.type === "checkEmail") {
                        this.checkEmailClicked();
                    } else {
                        this.esaLoginClicked();
                    }
                }
            },
            resetPasswordClicked: function (e) {

                console.log('HomeView:resetPasswordClicked');
                App.router.navigate('checkEmail', {trigger: true});
                //this.resetPasswordEmail("Enter your email address.");
            },
            checkEmailClicked: function (e) {

                var self = this;

                console.log('HomeView:checkEmailClicked');

                var email = self.$('#emailAddress').val();

                if (email !== null) {
                    self.employeeDataSource.filter(
                        {field: "emailAddress", operator: "eq", value: email},
                        {field: "emailAddress", operator: "neq", value: ''}
                    );
                    self.employeeDataSource.fetch(function () {

                        var empData = this.data();
                        var empCount = empData.length;

                        if (empCount === 0) {
                            self.createAutoClosingAlert($("#startError"), "<strong>Login Failure.</strong> Email address not found.  Please try again.");
                        }
                        else {

                            var empRecord = empData[0];

                            //var url = encodeURI(App.config.DataServiceURL + "/rest/GetResetPassword/" + empRecord.userKey + "/" + encodeURIComponent(email) + "/");
                            var url = App.config.DataServiceURL + "/rest/GetResetPassword/" + empRecord.userKey + "/" + email + "/";

                            $.ajax({
                                type: 'GET',
                                //contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                                contentType: "application/json",
                                url: url,
                                dataType: 'json',
                                success: function (response) {
                                    console.log("HomeView:resetPasswordEmail:success:response:" + response);
                                    kendo.ui.progress($("#loadingEsaUser"), false);

                                    if (response.indexOf("Password changed successfully") > -1) {
                                        self.createAutoClosingAlert($("#startSuccess"), "<strong>Success.</strong> Password reset email was sent to: " + email, 2500);
                                        self.$('#checkEmail').addClass('disabled');
                                        self.$('#checkEmail').prop('disabled', true);
                                    } else {
                                        self.createAutoClosingAlert($("#startError"), "<strong>Failed.</strong> Password reset failed.", 5000);
                                    }
                                },
                                error: function (err) {
                                    console.log("HomeView:resetPasswordEmail:Error: " + err);
                                    kendo.ui.progress($("#loadingEsaUser"), false);

                                    var errorToShow = "Reset password failed.";
                                    if (err.responseText && err.responseText.length > 0) {
                                        errorToShow = err.responseText;
                                        //parse the leading and trailing double quotes from the string
                                        errorToShow = errorToShow.substring(1, errorToShow.length - 1);
                                    }
                                    self.createAutoClosingAlert($("#startError"), "<strong>Failed.</strong> " + errorToShow, 5000);

                                }
                            });


                        }
                    });
                }

            },
            esaLoginClicked: function (e) {

                var self = this;
                var app = App;

                //console.log('HomeView:esaLoginClicked:user:' + this.esaUser);
                // odata call to see if user/pw exists
                this.esaUser = this.$('#esaUser').val();
                this.esaPassword = this.$('#esaPassword').val();

                this.userDataSource.filter([
                    {field: "userLogin", operator: "eq", value: this.esaUser.toString()},
                    {field: "userPassword", operator: "eq", value: this.esaPassword.toString()}
                ]);
                this.userDataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count === 0) {
                        //console.log('HomeView:esaLoginClicked:userIdNotFound');
                        self.createAutoClosingAlert($("#startError"), "<strong>Login Failure.</strong> User/Password combination not found.  Please try again.");
                    }
                    else {
                        //console.log('HomeView:esaLoginClicked:Set:userId');

                        var record = null;
                        var userLogin = null;
                        var userType = null;
                        var userKey = null;
                        var firstName = null;
                        var lastName = null;
                        var userId = null;
                        var inactive = false;

                        var i = 0;
                        while (userLogin === null || userLogin === undefined || userLogin === "") {
                            record = data[i];
                            userId = data[i].userID;
                            userKey = data[i].userKey;
                            userLogin = data[i].userLogin;
                            userType = data[i].userType;
                            firstName = data[i].firstName;
                            lastName = data[i].lastName;
                            inactive = data[i].inactive;
                            //lastPasswordChange = data[i].lastPasswordChange;
                            i++;
                        }

                        console.log('HomeView:esaLoginClicked:inactive:' + inactive);

                        if (inactive === true) {
                            self.createAutoClosingAlert($("#startError"), "<strong>Login Failure.</strong> This user is inactive.");
                            return;
                        }

                        var offset = (new Date()).getTimezoneOffset();
                        var lastPasswordChange = null;
                        if (record.lastPasswordChange !== null) {
                            lastPasswordChange = new Date(moment(record.lastPasswordChange).clone().subtract((offset), 'minutes'));
                        }
                        //console.log('HomeView:esaLoginClicked:lastPasswordChange:' + lastPasswordChange);

                        var lastCompanyMessagesView = null;
                        if (record.lastCompanyMessagesView !== null) {
                            lastCompanyMessagesView = new Date(moment(record.lastCompanyMessagesView).clone().subtract((offset), 'minutes'));
                            lastCompanyMessagesView.setHours(0);
                        }

                        var lastESAMediaView = null;
                        if (record.lastESAMediaView !== null) {
                            lastESAMediaView = new Date(moment(record.lastESAMediaView).clone().subtract((offset), 'minutes'));
                            lastESAMediaView.setHours(0);
                        }

                        var prevPasswords = record.prevPasswords;
                        var passwordExpirationDays = App.config.PasswordExpirationDays;
                        var datePasswordChanged = new Date(moment().clone().subtract((offset), 'minutes').subtract((passwordExpirationDays), 'days'));
                        var now = new Date(moment().clone().subtract((offset), 'minutes'));

                        if (lastPasswordChange === null || moment(lastPasswordChange).isBefore(datePasswordChanged)) {

                            // Go to password change page
                            //app.router.navigate('setPassword', {trigger: true});
                            //self.resetPasswordEmail("Your password has expired. Enter the email address where password reset link will be sent.");
                            self.createAutoClosingAlert($("#startError"), "<strong>Password Change.</strong> Your password has expired. You will now be directed to enter the email address where the password reset link will be sent.");
                            window.setTimeout(function () {
                                self.resetPasswordClicked();
                            }, 5000);
                        } else {

                            console.log('HomeView:esaLoginClicked:userLogin:' + userLogin);
                            console.log('HomeView:esaLoginClicked:lastPasswordChange:' + lastPasswordChange );

                            app.model.set('userKey', parseInt(userKey, 0));
                            app.model.set('userLogin', userLogin);
                            app.model.set('userType', parseInt(userType, 0));
                            app.model.set('userFirstName', firstName);
                            app.model.set('userLastName', lastName);
                            app.model.set('userId', userId);
                            app.model.set('lastCompanyMessagesView', lastCompanyMessagesView);
                            app.model.set('lastESAMediaView', lastESAMediaView);

                            record.set("loginCount", parseInt(record.loginCount, 0) + 1);
                            record.set("lastLogin", now);
                            record.set("lastLogout", null);
                            record.set("lastPasswordChange", lastPasswordChange);
                            record.set("prevPasswords", prevPasswords);
                            record.set("lastCompanyMessagesView", lastCompanyMessagesView);
                            record.set("lastESAMediaView", lastESAMediaView);

                            $.when(this.sync()).done(function (e) {

                                // Go to dashboard
                                app.router.navigate('home', {trigger: true});
                            });
                        }

                    }
                });
            },
            createAutoClosingAlert: function (selector, html) {
                //console.log('HomeView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 5000);
            },
            onResize: function () {
//                //console.log(' HomeView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                // Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log(' HomeView:resize');

                var height = $(window).height();
                var width = $(window).width();

                var calculatedHeight = height;
                var minHeight = 600;  // 600 for live site
                var headerHeight = 145;
                if (App.model.get('appBannerVisible') === true) {
                    calculatedHeight = height - headerHeight;
                }
                else {
                    headerHeight = 67;
                    calculatedHeight = height - headerHeight;
                }
                if (calculatedHeight < minHeight)
                    calculatedHeight = minHeight;
                this.$(".home-container").height(calculatedHeight);

                var scrollHeight = $("#esa")[0].scrollHeight;
                var clientHeight = $("#esa")[0].clientHeight;

                // If we have a scroll bar then bad the bottom row so it can come into view
                if (scrollHeight > clientHeight) {
                    this.$(".bottom-row").css('padding-bottom', '38px');
                }
                else {

                    if ((clientHeight - headerHeight) < minHeight) {
                        var paddingBottom = minHeight - (clientHeight - headerHeight);
                        this.$(".bottom-row").css('padding-bottom', paddingBottom + 'px');
                    }
                    else {
                        this.$(".bottom-row").css('padding-bottom', '');
                    }
                }

                // If we are stacked then pad the last column
                if (width < 992) {
                    this.$(".last-column").css('padding-bottom', '38px');
                }
                else {
                    this.$(".last-column").css('padding-bottom', '2px');
                }
                this.$(".home-container").height(calculatedHeight);
            },
            remove: function () {
                //console.log('-------- HomeView:remove --------');

                // Remove window events
                $(window).off('resize', this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
