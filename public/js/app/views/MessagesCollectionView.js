define(['App', 'backbone', 'marionette', 'jquery', 'models/AdminDataModel', 'hbs!templates/messagesCollection',
        'views/MessagesView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, AdminDataModel, template, MessagesView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: MessagesView,
            initialize: function (options) {

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;
                this.messagesfull = options.messagesfull;

                console.log('MessagesCollectionView:initialize:options:' + JSON.stringify(this.options));

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.messages = [];

                this.getData();

            },
            onShow: function () {
                console.log("-------- MessagesCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                var endOfDayToday = new Date();
                endOfDayToday.setHours(23);

                var filter = [
                    {field: "category", operator: "eq", value: self.options.category},
                    {field: "timeStamp", operator: "lte", value: endOfDayToday}
                ];

                this.adminDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalAdminData";
                            },
                            complete: function (e) {
                                console.log('MessagesCollectionView:adminDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'MessagesCollectionView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {editable: false, type: "number"},
                                text: {editable: true, type: "string"},
                                shortText: {editable: true, type: "string"},
                                text2: {editable: true, type: "string"},
                                title: {editable: true, type: "string"},
                                fileName: {editable: true, type: "string"},
                                externalLink: {editable: true, type: "string"},
                                category: {editable: true, type: "number"},
                                subCategory: {editable: true, type: "number"},
                                priority: {editable: true, type: "number"},
                                showAsCompanyMessage:{editable: true, type: "boolean"},
                                addUserImage:{editable: true, type: "boolean"},
                                timeStamp: {editable: true,type: "date"}
                            }
                        }
                    },
                    // pageSize: 100,
                    // page: 1,
                    serverPaging:true,
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   MessagesCollectionView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#messagesCollectionViewLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    MessagesCollectionView:messageDataSource:requestEnd');
                        //kendo.ui.progress(self.$("#messagesCollectionViewLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessagesCollectionView:messageDataSource:error');
                        kendo.ui.progress(self.$("#messagesCollectionViewLoading"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessagesCollectionView:messageDataSource:change');

                        var data = this.data();

                        //if (data.length <= 0)
                        //    self.displayNoDataOverlay();
                        //else
                        //    self.hideNoDataOverlay();
                    },
                    sort: [{field: "timeStamp", dir: "desc"},{field: "priority", dir: "asc"} ],
                    filter: filter
                });

                if (!self.messagesfull) {
                    this.adminDataSource =new kendo.data.DataSource({
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('MessagesCollectionView:userDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'MessagesCollectionView:userDataSource:2:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    text: {editable: true, type: "string"},
                                    shortText: {editable: true, type: "string"},
                                    text2: {editable: true, type: "string"},
                                    title: {editable: true, type: "string"},
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {editable: true, type: "string"},
                                    category: {editable: true, type: "number"},
                                    subCategory: {editable: true, type: "number"},
                                    priority: {editable: true, type: "number"},
                                    showAsCompanyMessage:{editable: true, type: "boolean"},
                                    addUserImage:{editable: true, type: "boolean"},
                                    timeStamp: {editable: true,type: "date"}
                                }
                            }
                        },
                        pageSize: 100,
                        page: 1,
                        serverPaging:true,
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   MessagesCollectionView:messageDataSource:requestStart');
    
                            kendo.ui.progress(self.$("#messagesCollectionViewLoading"), true);
                        },
                        requestEnd: function () {
    
                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
    
                            //console.log('    MessagesCollectionView:messageDataSource:requestEnd');
                            //kendo.ui.progress(self.$("#messagesCollectionViewLoading"), false);
    
                        },
                        error: function () {
    
                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
    
                            //console.log('   MessagesCollectionView:messageDataSource:error');
                            kendo.ui.progress(self.$("#messagesCollectionViewLoading"), false);
    
                            //self.displayNoDataOverlay();
    
                            //App.modal.show(new ErrorView());
    
                        },
                        change: function () {
    
                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
    
                            //console.log('   MessagesCollectionView:messageDataSource:change');
    
                            var data = this.data();
    
                            //if (data.length <= 0)
                            //    self.displayNoDataOverlay();
                            //else
                            //    self.hideNoDataOverlay();
                        },
                        sort: [{field: "timeStamp", dir: "desc"},{field: "priority", dir: "asc"} ],
                        filter: filter
                    });    
                }

                this.adminDataSource.fetch(function () {

                    var data = this.data();
                    $.each(data, function (index, value) {

                        self.messages.push({
                            ID:value.ID,
                            fileName:value.fileName,
                            title:value.title,
                            shortText:value.shortText,
                            text:value.text,
                            text2:value.text2,
                            category: value.category,
                            subCategory: value.subCategory,
                            externalLink: value.externalLink,
                            priority: value.priority,
                            timeStamp:value.timeStamp
                        });
                    });

                    if (!self.messagesfull) {
                        console.log('MessagesCollectionView:file:extra value');
                
                        self.messages.push({
                                ID:null,
                                fileName:"",
                                title:null,
                                shortText:null,
                                text:null,
                                text2:"Load All Messages...",
                                category: null,
                                subCategory: null,
                                externalLink: "#messagesfull",
                                priority: null,
                                timeStamp: null
                            });
                    }
                    
                    self.messages.sort(function(a,b){
                        var c = new Date(a.timeStamp);
                        var d = new Date(b.timeStamp);
                        return d-c;
                    });

                    // Populate doc collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return (model.get("ID"));
                    };

                    $.each(self.messages, function (index, value) {

                        //console.log('MessagesCollectionView:file:value:' + JSON.stringify(value));
                        var model = new AdminDataModel();

                        model.set("ID", value.ID);
                        model.set("fileName", value.fileName);
                        model.set("title", value.title);
                        model.set("shortText", value.shortText);
                        model.set("text", value.text);
                        model.set("text2", value.text2);
                        model.set("category", value.category);
                        model.set("subCategory", value.subCategory);
                        model.set("externalLink", value.externalLink);
                        model.set("priority", value.priority);
                        model.set("timeStamp", value.timeStamp);

                        self.collection.push(model);
                    });

                    self.collection.sort();
                });
            },
            hideNoDataOverlay: function () {
                //console.log('MessagesCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#fileWidgetContent').css('display', 'block');
            },
            onResize: function () {
                //console.log('MessagesCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('MessagesCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- MessagesCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
