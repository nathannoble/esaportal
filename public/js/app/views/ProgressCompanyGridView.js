define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/ProgressCompanyGrid',
        'hbs!templates/editProgressCompanyPopup','jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template,EditProgressCompanyPopup) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('ProgressCompanyGridView:initialize');

                _.bindAll(this);

                var self = this;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);
                
                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('ProgressCompanyGridView:update');
                                    return App.config.DataServiceURL + "/odata/ProgressCompany" + "(" + data.companyID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    console.log('ProgressCompanyGridView:create:data:' + JSON.stringify(data));
                                    return App.config.DataServiceURL + "/odata/ProgressCompany";
                                }
                                //complete: function (e) {
                                //    //console.log('ProgressCompanyGridView:create:complete');
                                //    if (e.responseJSON !== undefined) {
                                //        //console.log('ProgressCompanyGridView:create:complete:' + JSON.stringify(e.responseJSON));
                                //        if (e.responseJSON.value[0]) {
                                //            var clientCompany = e.responseJSON.value[0].clientCompany;
                                //            var newCompanyID = e.responseJSON.value[0].companyID;
                                //
                                //            console.log('ProgressCompanyGridView:create:complete:companyID:clientCompany:' + newCompanyID + ":"+  clientCompany);
                                //
                                //            // A duplicate has been created
                                //            if (clientCompany === '--- NONE ---') {
                                //                console.log('ProgressCompanyGridView:create:complete:duplicate');
                                //
                                //                //Delete duplicate
                                //                self.deleteDuplicate(newCompanyID);
                                //            }
                                //        } else {
                                //            var message = 'ProgressCompanyGridView:create:complete:no record';
                                //            console.log(message);
                                //        }
                                //    } else {
                                //        var message2 = 'ProgressCompanyGridView:create:complete:no record 2';
                                //        console.log(message2);
                                //    }
                                //}

                            },
                            destroy: {
                                url: function (data) {
                                    console.log('ProgressCompanyGridView:destroy:data:' + JSON.stringify(data));
                                    return App.config.DataServiceURL + "/odata/ProgressCompany" + "(" + data.companyID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyID",
                                fields: {
                                    companyID: {editable: false, type: "number"},
                                    branchID: {editable: true, type: "number"},
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    startDate: {editable: true, type: "date", validation: {required: false}},
                                    stageOfSale: {editable: true, type: "string", validation: {required: false}},
                                    contactFirstName: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    contactLastName: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    contactPhone: {editable: true, type: "string", validation: {required: false}},
                                    contactEmail: {editable: true, type: "string", validation: {required: false}},
                                    contactAddress: {editable: true, type: "string", validation: {required: false}},
                                    contactCity: {editable: true, type: "string", validation: {required: false}},
                                    contactState: {editable: true, type: "string", validation: {required: false}},
                                    contactZip: {editable: true, type: "string", validation: {required: false}},
                                    systemCRM_Name: {editable: true, type: "string", validation: {required: false}},
                                    systemCRM_URL: {editable: true, type: "string", validation: {required: false}},
                                    systemEmail_Name: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    systemEmail_URL: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    companyProfile: {editable: true, type: "string", validation: {required: false}},
                                    keyPeople: {editable: true, type: "string", validation: {required: false}},
                                    industry: {editable: true, type: "string", validation: {required: false}},
                                    product: {editable: true, type: "string", validation: {required: false}},
                                    totalClientOpportunity: {editable: true, type: "number"},
                                    actualClients: {editable: true, type: "number"},
                                    expectedCloseDate: {
                                        editable: true,
                                        type: "date",
                                        validation: {required: false}
                                    },
                                    actualCloseDate: {editable: true, type: "date", validation: {required: false}},
                                    leadDate: {editable: true, type: "date", validation: {required: false}},
                                    taskTracking: {editable: true, type: "boolean"},
                                    calendarSystemNotes: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    emailSystemNotes: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    complianceGuidelines: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    billingDetails: {editable: true, type: "string", validation: {required: false}},
                                    cspType: {editable: true, type: "string", validation: {required: false}},
                                    ratesStructure: {editable: true, type: "string", validation: {required: false}},
                                    scheduleSoftware: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    esaExclusive: {editable: true, type: "boolean"},
                                    primaryAccountMgrID: {editable: true, type: "number"}

                                }
                            }
                        },
                        sort: {field: "clientCompany", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('ProgressCompanyGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#companyLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('ProgressCompanyGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#companyLoading"), false);
                        },
                        change: function (e) {
                            //console.log('ProgressCompanyGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            console.log('ProgressCompanyGridView:dataSource:error:e' + JSON.stringify(e));
                            kendo.ui.progress(self.$("#companyLoading"), false);

                            if (e.xhr.status === 500) {
                                alert('The delete operation failed because this company record is associated with an active client.  In order to delete this company, you must first remove the company from the active client.');
                                this.cancelled = true;
                                e.xhr.statusText = "ABORT";
                                return;
                            }
                        }
                    });

            },
            //deleteDuplicate: function (companyID) {
            //    var self = this;
            //
            //    console.log('ProgressCompanyGridView:deleteDuplicate:companyID:' + companyID);
            //    console.log('ProgressCompanyGridView:deleteDuplicate:companyID:' + companyID);
            //
            //    self.dataSource.filter({field: "companyID", operator: "eq", value: companyID});
            //    self.dataSource.fetch(function () {
            //        var data = this.data();
            //        var count = data.length;
            //
            //        if (count > 0) {
            //            var record = self.dataSource.at(0);
            //            self.dataSource.remove(record);
            //            this.sync();
            //            console.log('ProgressCompanyGridView:deleteDuplicate:companyID:' + companyID + ' done');
            //        } else {
            //            console.log('ProgressCompanyGridView:deleteDuplicate:companyID:' + companyID + ' not found.');
            //        }
            //    });
            //
            //},
            onRender: function () {
                //console.log('ProgressCompanyGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log("-------- ProgressCompanyGridView:onShow --------");
                var self = this;

                //console.log('ProgressCompanyGridView:onShow:districtID:' + this.districtID);

                this.grid = this.$("#company").kendoGrid(
                    {
                    //toolbar: ["create"],
                    editable: {
                        mode: "popup",
                        window: {
                            title: "Edit Company"
                        },
                        template: function (e) {
                            var template = EditProgressCompanyPopup();
                            return template;
                        }
                    },
                    //editable: true,
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    filterable: {
                        mode: "row"
                    },
                    columns: [
                        //{
                        //    width: 25,
                        //    template: function (e) {
                        //        var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction k-grid-edit' data-id='" + e.companyID + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                        //        //fa-edit
                        //        return template;
                        //    },
                        //    filterable: false,
                        //    sortable: true
                        //},
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.companyID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "companyID",
                            title: "ID",
                            width: 100,
                            filterable: false
                        },{
                            field: "branchID",
                            title: "Branch",
                            width: 100,
                            filterable: false
                        }, {
                            field: "clientCompany",
                            title: "Client Company",
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                return template;
                            },
                            width: 250,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            },
                            sortable: true
                        }, {
                            field: "startDate",
                            //template: function (e) {
                            //    var date = new Date(e.startDate);
                            //    var month = date.getMonth() + 1;
                            //    var day = date.getDate();
                            //    var year = date.getFullYear();
                            //    var shortStartDate = month + "/" + day + "/" + year;
                            //    var template = "<div style='text-align: left'>" + shortStartDate + "</div>";
                            //    return template;
                            //},
                            format: "{0:M/d/yyyy}",
                            title: "Start Date",
                            width: 125,
                            filterable: false
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("ProgressCompanyGridView:def:onShow:dataBound");
                        self.$('.company-profile').on('click', self.viewCompanyProfile);

                    },
                    change: function (e) {
                        //console.log('ProgressCompanyGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('ProgressCompanyGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('ProgressCompanyGridView:def:onShow:onEdit');

                        // Disable the companyID editor
                        e.container.find("input[name='companyID']").prop("disabled", true);

                        // Make edit/delete invisible
                        //e.container.find(".k-edit-label:first").hide();
                        //e.container.find(".k-edit-field:first").hide();
                        //e.container.find(".k-edit-label:nth-child(4)").hide();
                        //e.container.find(".k-edit-field:nth-child(4)").hide();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            viewCompanyProfile: function (e) {
                //console.log('ProgressCompanyGridView:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value,0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressCompanyGridView:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId',companyId);

                App.router.navigate('companyProfile', {trigger: true});

            },
            getData: function () {
                console.log('ProgressCompanyGridView:getData');

                //var state = {};
                //state.page = 1;
                //state.pageSize = 40;

                var grid = this.$("#company").data("kendoGrid");

                //console.log('ProgressCompanyGridView:getData:filter:' + JSON.stringify(state.filter));

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);
                });

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressCompanyGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#company').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressCompanyGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#company').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressCompanyGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#company").parent().parent().height();
                this.$("#company").height(parentHeight);

                parentHeight = parentHeight - 78;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressCompanyGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressCompanyGridView:resize:headerHeight:' + headerHeight);
                this.$("#company").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressCompanyGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#company").data('ui-tooltip'))
                    this.$("#company").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });