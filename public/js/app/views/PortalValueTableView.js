define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalValueTable', 'views/PortalValueGridView'],
    function (App, Backbone, Marionette, $, template, PortalValueGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                valueRegion: "#value-region"
            },
            initialize: function () {

                //console.log('PortalValueTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('PortalValueTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalValueTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.valueRegion.show(new PortalValueGridView());
                }



            },
            validateData: function () {
                var self = this;

                if (App.modal.currentView === null || App.modal.currentView === undefined) {

                }
            },
            resize: function () {
                //console.log('PortalValueTableView:resize');

                if (this.valueRegion.currentView)
                    this.valueRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalValueTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });