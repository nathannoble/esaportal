define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/welcomeSocialMedia'],
    function (App, Backbone, Marionette, _, $, template ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            initialize: function () {
                //console.log('WelcomeSocialMediaView:initialize');

                _.bindAll(this);

                this.sms = [];

            },
            getData: function () {

                var self = this;

                this.adminDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalAdminData";
                            },
                            complete: function (e) {
                                console.log('WelcomeSocialMediaView:adminDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'WelcomeCollectionView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {editable: false, type: "number"},
                                text: {editable: true, type: "string"},
                                shortText: {editable: true, type: "string"},
                                text2: {editable: true, type: "string"},
                                title: {editable: true, type: "string"},
                                fileName: {editable: true, type: "string"},
                                externalLink: {editable: true, type: "string"},
                                category: {editable: true, type: "number"},
                                subCategory: {editable: true, type: "number"},
                                priority: {editable: true, type: "number"},
                                showAsCompanyMessage:{editable: true, type: "boolean"},
                                addUserImage:{editable: true, type: "boolean"},
                                timeStamp: {editable: true,type: "date"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   WelcomeSocialMediaView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    WelcomeSocialMediaView:messageDataSource:requestEnd');
                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   WelcomeSocialMediaView:messageDataSource:error');
                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   WelcomeSocialMediaView:messageDataSource:change');

                        var data = this.data();
                        
                    },
                    sort: [{field: "timeStamp", dir: "desc"} ],
                    filter: [
                        //{field: "showAsCompanyMessage", operator: "eq", value:true},
                        {field: "category", operator: "eq", value: 3}
                    ]
                });

                this.adminDataSource.fetch(function () {

                    var data = this.data();
                    $.each(data, function (index, value) {

                        self.sms.push({
                            ID:value.ID,
                            fileName:value.fileName,
                            title:value.title,
                            shortText:value.shortText,
                            text:value.text,
                            text2:value.text2,
                            subCategory: value.subCategory
                        });

                    });

                    //console.log('WelcomeSocialMediaView:getData:sms:' + JSON.stringify(self.sms));

                    if (self.sms.length > 0) {
                        self.ID = self.sms[0].ID;
                        self.fileName = self.sms[0].fileName;
                        self.title = self.sms[0].title.trim();
                        self.shortText = self.sms[0].shortText.trim();
                        self.text = self.sms[0].text.trim();
                        self.text2 = self.sms[0].text2.trim();
                        self.subCategory = self.sms[0].subCategory;

                        self.$('#title').html(self.title);
                        self.$('#image').attr('src', App.config.PortalFiles + "/SocialMedia/" + self.fileName);
                    }
                });
            },

            onRender: function () {
//                //console.log('WelcomeSocialMediaView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- WelcomeSocialMediaView:onShow --------');

                this.getData();

            },
            onResize: function () {
//                //console.log('WelcomeSocialMediaView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            createAutoClosingAlert: function (selector, html, msDuration) {
                // show the alert
                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                var dz = $("#dragDropHandler");

                //this.isUploadingBio = false;
                //App.model.set('uploadingSpreadCSV', false);

                //wait five seconds and close the alert
                window.setTimeout(function () {
                    dz.slideDown(500);
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, msDuration);
            },
            resize: function () {
//                //console.log('WelcomeSocialMediaView:resize');

            },
            remove: function () {
                //console.log('-------- WelcomeSocialMediaView:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
