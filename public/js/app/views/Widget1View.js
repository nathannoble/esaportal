define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/widget1', 'views/ScoreboardDetailsView', 'kendo/kendo.data'],
    function (App, Backbone, Marionette, $, template, ScoreboardDetailsView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #OpenDetails': 'onOpenDetailsClicked'
            },
            initialize: function (options) {
                //console.log('Widget1View:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                var app = App;

                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter);

                this.maxDate = new Date(this.year + "-12-31");

            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- Widget1View:onShow ----------');

                this.populateWidget();
            },
            onOpenDetailsClicked: function () {
//                console.log('Widget1View:onOpenDetailsClicked');


                App.modal.show(new ScoreboardDetailsView());
            },
            populateWidget: function () {
                //console.log('Widget1View:populateWdiget');

                var self = this;
                var app = App;

                // data call
                //SELECT      count(sbCount) AS clientActual
                //FROM        tblProgressClients
                //WHERE      status LIKE '4 - Active' AND sbCount = 1
                //GROUP BY  sbCount

                this.dataSource = new kendo.data.DataSource(
                    {
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                            },
                            complete: function (e) {
                                console.log('Widget1View:dataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'Widget1View:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "Id",
                            fields: {
                                Id: {editable: false, defaultValue: 0, type: "number"},
                                sbCount: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   Widget1View:requestStart');

                        kendo.ui.progress(self.$("#loadingWidget1"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    Widget1View:requestEnd');
                        kendo.ui.progress(self.$("#loadingWidget1"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   Widget1View:error');
                        kendo.ui.progress(self.$("#loadingWidget1"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   Widget1View:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: [
                        {field: "status", operator: "eq", value: '4 - Active'},
                        //{field: "companyClient", operator: "eq", value: false},
                        {field: "sbCount", operator: "eq", value: true},
                        {field: "startDate", operator: "lte", value: self.maxDate}
                    ]
                });

                var widget1Value = app.model.get('widget1Value');

                if (widget1Value === null) {
                    this.dataSource.fetch(function () {

                        //var datarecord = this.data()[0];
                        var length = this.data().length;

                        // Populate widget with data
                        self.$('#clientNum').html(length);

                        app.model.set('widget1Value', length);

                    });
                } else {
                    // Populate widget with data
                    self.$('#clientNum').html(widget1Value);
                }


            },
            displayNoDataOverlay: function () {
                //console.log('ProjectGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProjectGridView:hideNodDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('.tile-content').css('display', 'block');
            },
            onResize: function () {
//                //console.log('Widget1View:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('Widget1View:resize');

            },
            remove: function () {
                //console.log('---------- Widget1View:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off
                this.$('.tile-content').off();
                this.$('.tile-more-info').off();
                this.$('.tile-hover-tip').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });