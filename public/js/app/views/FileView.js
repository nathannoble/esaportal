define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/file','models/FileModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #fileSend': 'fileSendClicked',
                'click #fileDelete': 'fileDeleteClicked'
            },
            initialize: function () {
                //console.log('FileView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('FileView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- FileView:onShow --------');

                this.fileID = this.model.get('fileID');
                this.fileName = this.model.get('fileName');
                this.fileNameWithPath = this.model.get('fileNameWithPath');
                this.fileDate = this.model.get('fileDate');
                this.type = this.model.get('type');

                this.$('#fileName').html(this.fileName);
                this.$('#fileDate').html(this.fileDate);
                this.$('#fileView').attr('href', "../../" + App.config.PortalName + "PortalFiles/files" + this.type + "/" + this.fileName );
                this.$('#fileView').attr('title',this.fileName);
                this.$('#fileView').attr('download',this.fileName);

            },
            fileSendClicked: function (e) {
                //console.log('FileView:fileSendClicked');

                var self = this;

            },
            fileDeleteClicked: function (e) {
                //console.log('FileView:fileDeleteClicked');

                var self = this;
                kendo.ui.progress($("#loadingFileWidget"), true);
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: (App.config.DataServiceURL + "/rest/files/GetDeleteFile/" + self.type + "/" + self.fileName + "/"),
                    dataType: "json",
                    success: function(response) {
                        //console.log("FileView:fileDeleteClicked:success:response:" + response);
                        kendo.ui.progress($("#loadingFileWidget"), false);

                        if (response === true) {
                            self.createAutoClosingAlert($("#fileSuccess"), "<strong>Success.</strong> File deletion was successful.",2500);
                        } else {
                            self.createAutoClosingAlert($("#fileError"), "<strong>Failed.</strong> File not found.",5000);
                        }

                        App.model.set('resetFileList', true);
                    },
                    error: function(err) {
                        //console.log(err);
                        console.log("FileView:fileDeleteClicked:Error: " + err);
                        kendo.ui.progress($("#loadingFileWidget"), false);
                        var errorToShow = "File delete failed.";
                        if (err.responseText && err.responseText.length > 0){
                            errorToShow = err.responseText;
                            //parse the leading and trailing double quotes from the string
                            errorToShow = errorToShow.substring(1,errorToShow.length-1);
                        }
                        self.createAutoClosingAlert($("#fileError"), "<strong>Failed.</strong> " + errorToShow,5000);

                    }
                });

            },
            onResize: function () {
//                //console.log('FileView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            createAutoClosingAlert: function (selector, html, msDuration) {
                // show the alert
                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                var dz = $("#dragDropHandler");

                //this.isUploadingFile = false;
                //App.model.set('uploadingSpreadCSV', false);

                //wait five seconds and close the alert
                window.setTimeout(function () {
                    dz.slideDown(500);
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, msDuration);
            },
            resize: function () {
//                //console.log('FileView:resize');

            },
            remove: function () {
                //console.log('-------- FileView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#fileRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
