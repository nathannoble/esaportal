define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/openTimersGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {

                _.bindAll(this);

                var self = this;
                var app = App;

                this.userKey = parseInt(App.model.get('userKey'), 0); //67
                //console.log('OpenTimersGridView:initialize:userKey:' + this.userKey);

                this.userType = parseInt(App.model.get('userType'), 0);
                this.isExecutive = false;
                if (self.userType === 3) {
                    // for now, hide delete on this dialog
                    this.isExecutive = false;
                }

                self.today = moment(new Date()).format('YYYY-MM-DD'); //"2016-11-01";

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                //console.log('OpenTimersGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- OpenTimersGridView:onShow --------");
                var self = this;

                this.grid = this.$("#openTimer").kendoGrid({
                    //dataSource: self.dataSource,
                    //toolbar: ["create"],
                    editable: "popup",  //inline, popup
                    sortable: true,
                    //extra: false,
                    //selectable: "multiple",
                    resizable: true,
                    reorderable: true,
                    //groupable: true,
                    columnResize: function (e) {
                        //self.resize();
                        window.setTimeout(self.resize, 10);
                    },
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction edit-timecard' data-id='" + e.intTimerID + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                                //fa-edit
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.intTimerID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true,
                            hidden: !self.isExecutive
                        },
                        {
                            field: "intTimerID",
                            title: "Timer ID",
                            width: 100
                            //locked: true
                        },
                        {
                            title: "Client Company",
                            field: "clientCompany",
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                return template;
                            },
                            width: 200
                        },
                        {
                            title: "Client Name",
                            field: "clientName",
                            width: 150,
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" + e.clientName + "</a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                            //locked: true
                        },
                        {
                            field: "employeeName",
                            title: "Employee",
                            width: 150
                            //locked: true
                        }, {
                            field: "serviceItem",
                            title: "Service",
                            width: 150
                            //locked: true
                        },
                        {
                            field: "workDate",
                            title: "Date",
                            width: 80,
                            format: "{0:M/d/yyyy}"
                            //locked: true
                        }, {
                            field: "startTime",
                            title: "Start Time",
                            width: 80,
                            format: "{0:h:mm:ss tt}"
                        }, {
                            field: "stopTime",
                            title: "Stop Time",
                            width: 80,
                            format: "{0:h:mm:ss tt}"
                        }, {
                            field: "timeWorked",
                            title: "Time Worked",
                            width: 100
                        }, {
                            field: "tasks",
                            title: "Tasks",
                            width: 100
                        }, {
                            field: "meetings",
                            title: "Meetings",
                            width: 100
                        }, {
                            field: "timerNote",
                            title: "Note",
                            width: 100
                        }, {
                            field: "mgrNote",
                            title: "Manager Note",
                            width: 150
                        }, {
                            field: "timerAction",
                            title: "Action",
                            width: 100
                        }, {
                            field: "recordType",
                            title: "Type",
                            width: 100
                        }],
                    dataBound: function (e) {
                        //console.log("OpenTimersGridView:onShow:grid:dataBound");
                        self.$('.client-profile').on('click', self.viewClientProfile);
                        self.$('.company-profile').on('click', self.viewCompanyProfile);
                        self.$('.edit-timecard').on('click', self.editTimecard);
                    },
                    change: function (e) {
                        //console.log('OpenTimersGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('OpenTimersGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('OpenTimersGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        //e.container.find("input[name='employeeID']").prop("disabled", true);

                        // Set the focus to the Username field
                        //e.container.find("input[name='Username']").focus();

                    }
                }).data("kendoGrid");

                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {

                var self = this;

                this.restDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        transport: {
                            //type: "odata",
                            read_orig: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetOpenTimers/" + self.today;
                                    return url;
                                },
                                type: "GET",
                                dataType: "json"
                            },
                            read: function (options) {
                                //self.dataSource.cancelled = true;
                                $.ajax({
                                    type: "GET",  //PATCH?
                                    contentType: "application/json", //; charset=utf-8",
                                    url: App.config.DataServiceURL + "/rest/GetOpenTimers/" + self.today,
                                    dataType: "json",
                                    requestStart: function (e) {
                                        kendo.ui.progress(self.$("#openTimerLoading"), true);
                                    },
                                    requestEnd: function (e) {
                                        kendo.ui.progress(self.$("#openTimerLoading"), false);
                                    },
                                    success: function (result) {
                                        // notify the data source that the request succeeded
                                        options.success(result);
                                    },
                                    error: function (result) {
                                        // notify the data source that the request failed
                                        kendo.ui.progress(self.$("#openTimerLoading"), false);
                                        options.error(result);
                                    }
                                });
                            },
                            destroy: function (options) {
                                //self.dataSource.cancelled = true;
                                $.ajax({
                                    type: "DELETE",
                                    contentType: "application/json", //; charset=utf-8",
                                    url: App.config.DataServiceURL + "/odata/PortalTimer" + "(" + options.data.intTimerID + ")",  // odata/PortalTimerIntTimerID
                                    dataType: "json",
                                    requestStart: function (e) {
                                        kendo.ui.progress(self.$("#openTimerLoading"), true);
                                    },
                                    requestEnd: function (e) {
                                        //var data = this.data();
                                        kendo.ui.progress(self.$("#openTimerLoading"), false);
                                    },
                                    success: function (result) {
                                        // notify the data source that the request succeeded
                                        options.success(result);
                                    },
                                    error: function (result) {
                                        // notify the data source that the request failed
                                        kendo.ui.progress(self.$("#openTimerLoading"), false);
                                        options.error(result);
                                    }
                                });
                            }
                        },
                        schema: {
                            model: {
                                id: "intTimerID",
                                fields: {

                                    intTimerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    employeeID: {editable: false, type: "number"},
                                    serviceID: {editable: false, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    clientName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: true}},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: [{field: "employeeName", dir: "asc"}, {field: "workDate", dir: "asc"}],
                        requestStart: function (e) {
                            //console.log('OpenTimersGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#openTimerLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('OpenTimersGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#openTimerLoading"), false);
                        },
                        change: function (e) {
                            //console.log('OpenTimersGridView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('OpenTimersGridView:dataSource:error');
                            kendo.ui.progress(self.$("#openTimerLoading"), false);
                        }
                    });

                self.restDataSource.fetch(function (data) {
                    //console.log('OpenTimersGridView:getData:data 1st record:' + JSON.stringify(this.data[0]));
                    self.grid.setDataSource(self.restDataSource);
                });

            },
            editTimecard: function (e) {
                console.log('OpenTimersGridView:editTimecardIntTimerID:e:target:' + JSON.stringify(e.target));
                console.log('OpenTimersGridView:editTimecardIntTimerID:e:currentTarget:' + JSON.stringify(e.currentTarget));

                var timerId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    timerId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    timerId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('OpenTimersGridView:viewCompanyProfile:e:' + timerId);
                App.model.set('selectedTimerId', timerId);

                App.router.navigate('editTimecardIntTimerID', {trigger: true});

            },
            viewCompanyProfile: function (e) {
                //console.log('OpenTimersGridView:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('OpenTimersGridView:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId', companyId);


                App.router.navigate('companyProfile', {trigger: true});

            },
            viewClientProfile: function (e) {
                //console.log('OpenTimersGridView:viewClientProfile:e:' + e.target);

                var clientId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    clientId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    clientId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('OpenTimersGridView:viewClientProfile:e:' + clientId);
                App.model.set('selectedClientId', clientId);


                App.router.navigate('clientProfile', {trigger: true});

            },
            displayNoDataOverlay: function () {
                //console.log('OpenTimersGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#openTimer').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('OpenTimersGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#openTimer').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('OpenTimersGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#openTimer").parent().parent().height();
                this.$("#openTimer").height(parentHeight);

                parentHeight = parentHeight - 13;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('OpenTimersGridView:resize:parentHeight:' + parentHeight);
                this.$("#openTimer").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- OpenTimersGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#openTimer").data('ui-tooltip'))
                    this.$("#openTimer").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });