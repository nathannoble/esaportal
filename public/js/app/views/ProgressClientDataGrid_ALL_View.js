define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/ProgressClientDataGrid_ALL',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('ProgressClientDataGrid_ALL_View:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "All Clients"};
                }


                this.userType = parseInt(App.model.get('userType'), 0);
                //this.isExecutive = false;
                //if (self.userType === 3) {
                //    this.isExecutive = true;
                //}
                this.selectedTeamLeaderId = App.model.get('selectedTeamLeaderId');
                this.myTeamLeaderId = parseInt(App.model.get('myTeamLeaderId'), 0);

                if (this.selectedTeamLeaderId === null) {
                    this.selectedTeamLeaderId = this.myTeamLeaderId;
                } else {
                    this.selectedTeamLeaderId = parseInt(App.model.get('selectedTeamLeaderId'), 0);
                }

                //console.log('ProgressClientDataGrid_ALL_View:initialize:selectedTeamLeaderId:' + this.selectedTeamLeaderId);

                this.dateFilter = App.model.get('selectedDateFilter');

                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;
                this.startYear = app.Utilities.numberUtilities.getStartYearFromDateFilter(this.dateFilter); //2016;
                //this.month = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter); //11;
                this.startMonth = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter);
                this.endMonth = app.Utilities.numberUtilities.getEndMonthFromDateFilter(this.dateFilter);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                var filters = [];

                this.statusFilter = App.model.get('selectedStatusFilter');
                if (this.statusFilter === null || this.statusFilter === undefined || this.options.type === "My Clients") {
                    this.statusFilter = "4 - Active";
                }

                if (this.statusFilter !== "All Status") {
                    filters.push({field: "status", operator: "eq", value: self.statusFilter});
                }

                if (self.selectedTeamLeaderId !== 0 && this.options.type !== "My Clients") {
                    filters.push({field: "teamLeaderID", operator: "eq", value: self.selectedTeamLeaderId});
                }

                this.clientDataGridFilter = App.model.get('clientDataGridFilter');


                // find the right plan that corresponds to this date range
                //console.log('ProgressClientDataGrid_ALL_View:initialize:datafilter:' + this.dateFilter);
                this.startDate = new Date(app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter)); //"2016-10-01T00:00:00+0000"
                this.endDate = new Date(app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter)); //"2016-10-31T23:59:59+0000"
                this.endDate = this.endDate.addHours(23);
                this.endDate = this.endDate.addMinutes(59);

                //var planDateFilter = {
                //    logic: "and",
                //    filters: [
                //        {
                //            logic: "or",
                //            filters: [
                //                {field: "planEndDate", operator: "eq", value: null},
                //                {field: "planEndDate", operator: "gt", value: this.endDate}
                //            ]
                //        },
                //        {field: "planStartDate", operator: "lt", value: this.endDate}
                //    ]
                //};
                //
                //
                //filters.push(planDateFilter);

                this.progressClientDS = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                },
                                complete: function (e) {
                                    console.log('ProgressClientDataGrid_ALL_View:progressClientDS:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientDataGrid_ALL_View:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "clientID",
                                fields: {
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    schedulerID: {defaultValue: 0, type: "number"},
                                    status: {editable: true, type: "string", validation: {required: false}},
                                    companyID: {defaultValue: 0, type: "number"},
                                    taskTargetLow: {type: "number"},
                                    taskTargetHigh: {type: "number"},
                                    mtgTargetLowDec: {type: "number"},
                                    mtgTargetHigh: {type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: false}},
                                    companyClient: {editable: true, type: "boolean"},
                                    clientSuccessPlan: {editable: true, type: "boolean"},
                                    useAsReference: {editable: true, type: "boolean"},
                                    emailAddress: {editable: true, type: "string", validation: {required: false}},
                                    address: {editable: true, type: "string", validation: {required: false}},
                                    city: {editable: true, type: "string", validation: {required: false}},
                                    state: {editable: true, type: "string", validation: {required: false}},
                                    zipCode: {editable: true, type: "string", validation: {required: false}},
                                    mobilePhone: {editable: true, type: "string", validation: {required: false}},
                                    homePhone: {editable: true, type: "string", validation: {required: false}},
                                    sbCount: {editable: true, type: "boolean"},
                                    clientTimeZoneID: {type: "number"},
                                    industry: {editable: true, type: "string", validation: {required: false}},
                                    divisionID: {type: "number"},
                                    division: {editable: true, type: "string", validation: {required: false}},
                                    title: {editable: true, type: "string", validation: {required: false}},
                                    lfdAuthorization: {editable: true, type: "string", validation: {required: false}},
                                    dsuNumber: {editable: true, type: "string", validation: {required: false}},
                                    product: {editable: true, type: "string", validation: {required: false}},
                                    distributionChannel: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    states: {editable: true, type: "string", validation: {required: false}},
                                    lengthInTerritory: {editable: true, type: "string", validation: {required: false}},
                                    workPhone: {editable: true, type: "string", validation: {required: false}},
                                    workExtension: {editable: true, type: "string", validation: {required: false}},
                                    assistantInternal: {editable: true, type: "string", validation: {required: false}},
                                    assistantPhone: {editable: true, type: "string", validation: {required: false}},
                                    assistantExtension: {editable: true, type: "string", validation: {required: false}},
                                    assistantEmail: {editable: true, type: "string", validation: {required: false}},
                                    typeOfService: {editable: true, type: "string", validation: {required: false}},
                                    referredBy: {editable: true, type: "string", validation: {required: false}},
                                    planID: {type: "number"},
                                    adminSchedulerID: {type: "number"},
                                    adminScheduler: {editable: true, type: "string", validation: {required: false}},
                                    realTimeAccess: {editable: true, type: "boolean"},
                                    realTimeUser: {editable: true, type: "string", validation: {required: false}},
                                    realTimePassword: {editable: true, type: "string", validation: {required: false}},
                                    esaAnniversary: {editable: true, type: "date", validation: {required: true}},
                                    termDate: {editable: true, type: "date", validation: {required: true}},
                                    schedulingStartDate: {editable: true, type: "date", validation: {required: false}},
                                    lastCheckInDate: {editable: true, type: "date", validation: {required: false}},
                                    surveySentDate: {editable: true, type: "date", validation: {required: false}},
                                    additionalInformation: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    sysCrmUserName: {editable: true, type: "string", validation: {required: false}},
                                    sysCrmPassword: {editable: true, type: "string", validation: {required: false}},
                                    sysCrmNotes: {editable: true, type: "string", validation: {required: false}},
                                    sysEmailUserName: {editable: true, type: "string", validation: {required: false}},
                                    sysEmailPassword: {editable: true, type: "string", validation: {required: false}},
                                    sysEmailNotes: {editable: true, type: "string", validation: {required: false}},
                                    sysCalendar: {editable: true, type: "string", validation: {required: false}},
                                    sysCalendarUserName: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    sysCalendarPassword: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    sysCalendarNotes: {editable: true, type: "string", validation: {required: false}},
                                    dbAccountType: {editable: true, type: "string", validation: {required: false}},
                                    checkInStatus: {editable: true, type: "string", validation: {required: false}},
                                    returningClient: {editable: true, type: "boolean"},
                                    dataCallDate: { editable: true, type: "date", validation: { required: false } },
                                    dbBuildCompleted: { editable: true, type: "date", validation: { required: false } },
                                    mapCreated: { editable: true, type: "date", validation: { required: false } },
                                    mapApproved: { editable: true, type: "date", validation: { required: false } },
                                    mapLastUpdated: { editable: true, type: "date", validation: { required: false } },
                                    linkedIn: { editable: true, type: "string", validation: { required: false } },
                                    cspTarget: { editable: true, type: "string", validation: { required: false } },
                                    clientAppPassword: { editable: true, type: "string", validation: { required: false } },
                                    companyEmailMFA: { editable: true, type: "string", validation: { required: false } },
                                    tier: { editable: true, type: "string", validation: { required: false } },
                                    wholesalerRank: { editable: true, type: "string", validation: { required: false } },
                                    platform: { editable: true, type: "string", validation: { required: false } },
                                    distSegment: { editable: true, type: "string", validation: { required: false } },
                                    secondaryRank: { editable: true, type: "string", validation: { required: false } },
                                    specialDBTags: { editable: true, type: "string", validation: { required: false } },
                                    focusFirms: { editable: true, type: "string", validation: { required: false } }, 
                                    maptiveID: { editable: true, type: "string", validation: { required: false } },
                                    clientTerrMap: { editable: true, type: "string", validation: { required: false } }, 
                                    schedDirectives: { editable: true, type: "string", validation: { required: false } }, 
                                    zoneMapNotes: { editable: true, type: "string", validation: { required: false } }, 
                                    adminDept: { editable: true, type: "string", validation: { required: false } }, 
                                    specialInstForDB: { editable: true, type: "string", validation: { required: false } },
                                    mtgTypes: { editable: true, type: "string", validation: { required: false } },
                                    dialerAcctTypes: { editable: true, type: "string", validation: { required: false } },
                                    existingZoning: { editable: true, type: "string", validation: { required: false } },
                                    travelRotation: { editable: true, type: "string", validation: { required: false } }
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   ProgressClientDataGrid_ALL_View:requestStart');

                            kendo.ui.progress(self.$("#clientDataLoading"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    ProgressClientDataGrid_ALL_View:requestEnd');
                            kendo.ui.progress(self.$("#clientDataLoading"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ProgressClientDataGrid_ALL_View:error');
                            kendo.ui.progress(self.$("#clientDataLoading"), false);

                            self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        change: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ProgressClientDataGrid_ALL_View:change');

                            var data = this.data();

                            if (data.length <= 0)
                                self.displayNoDataOverlay();
                            else
                                self.hideNoDataOverlay();
                        },
                        filter: filters
                        //filter: [{field: "status", operator: "eq", value: '4 - Active'}]

                    });


                this.companyDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                complete: function (e) {
                                    console.log('ProgressClientDataGrid_ALL_View:companyDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientDataGrid_ALL_View:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyID",
                                fields: {
                                    companyID: {editable: false, type: "number"},
                                    clientCompany: {type: "string"}
                                }
                            }
                        },
                        sort: {field: "clientCompany", dir: "asc"},
                        serverSorting: true,
                        serverFiltering: true,
                        filter: [
                            {field: "clientCompany", operator: "neq", value: "--- NONE ---"}
                            //{field: "actualClients", operator: "gt", value: 0},
                            //{field: "actualClients", operator: "neq", value: null}
                        ]

                    });

                this.schedulerDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('ProgressClientDataGrid_ALL_View:schedulerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientDataGrid_ALL_View:schedulerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    status: {type: "string", validation: {required: false}},
                                    isTeamLeader: {editable: true, type: "boolean"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"}
                        ],
                        filter: {} //{field: "status", operator: "eq", value: '1 - Active'}
                    });

            },
            onRender: function () {
                //console.log('ProgressClientDataGrid_ALL_View:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientDataGrid_ALL_View:onShow --------");

                // Load up the relevant data
                if (!(this.options.type === "My Clients" && this.myEmployeeId === null)) {

                    if (this.year !== this.startYear) {
                        alert("The date filter crosses years (" + this.startYear + "-" + this.year + ").  Only data between the chosen months in the most recent year will displayed.");
                    }
                    this.getData();
                } else {
                    //console.log('ProgressClientDataGrid_ALL_View:no data');
                }

            },
            getSchedulerRecord: function (id, records) {

                var emp = $.grep(records, function (element, index) {
                    return element.employeeID == id;
                });

                if (emp.length > 0) {
                    return emp[0].name;
                } else {
                    return "";
                }

            },
            getData: function () {
                //console.log('ProgressClientDataGrid_ALL_View:getData');

                var self = this;
                var app = App;

                if (self.startMonth !== null && self.endMonth !== null) {
                    this.schedulerDataSource.fetch(function (data) {

                        var schedRecords = this.data();
                        var modifiedSchedRecords = [{
                            employeeID: 0,
                            name: '--- NONE ---'
                        }];

                        $.each(schedRecords, function (index, value) {

                            var record = value;
                            var newRecord = {
                                // From ProgressClient
                                employeeID: record.employeeID,
                                name: record.firstName + " " + record.lastName
                            };
                            modifiedSchedRecords.push(newRecord);

                        });
                        var clientRecords = [];

                        self.progressClientDS.fetch(function () {
                            console.log('ProgressClientDataGrid_ALL_View:getData:self.progressClientDS.fetch');
                            var progressClientRecords = this.data();

                            $.each(progressClientRecords, function (index, value) {
                                var planHours = value.planHours;
                                var meetingsTarget = value.mtgTargetLowDec * planHours;
                                var taskTarget = value.taskTargetLow * planHours;

                                var newRecord = {

                                    // From ProgressClient
                                    clientID: value.clientID,
                                    timeZoneName: value.timeZoneName,
                                    companyID: value.companyID,
                                    clientCompany: value.clientCompany,
                                    status: value.status,
                                    lastName: value.lastName,
                                    firstName: value.firstName,
                                    clientFullName: value.lastName + ", " + value.firstName,
                                    planName: value.planName,
                                    planHours: planHours,
                                    schedulerID: value.schedulerID,
                                    schedulerName: value.schedulerName,
                                    teamLeaderID: value.teamLeaderID,

                                    tasksTarget: taskTarget,
                                    meetingsTarget: meetingsTarget,

                                    useAsReference: value.useAsReference,
                                    customerName: value.customerName,
                                    companyClient: value.companyClient,
                                    clientSuccessPlan: value.clientSuccessPlan,
                                    emailAddress: value.emailAddress,
                                    address: value.address,
                                    city: value.city,
                                    state: value.state,
                                    zipCode: value.zipCode,
                                    mobilePhone: value.mobilePhone,
                                    homePhone: value.homePhone,
                                    sbCount: value.sbCount,
                                    clientTimeZoneID: value.clientTimeZoneID,
                                    industry: value.industry,
                                    division: value.division,
                                    title: value.title,
                                    lfdAuthorization: value.lfdAuthorization,
                                    dsuNumber: value.dsuNumber,
                                    product: value.product,
                                    distributionChannel: value.distributionChannel,
                                    states: value.states,
                                    lengthInTerritory: value.lengthInTerritory,
                                    workPhone: value.workPhone,
                                    workExtension: value.workExtension,
                                    assistantInternal: value.assistantInternal,
                                    assistantPhone: value.assistantPhone,
                                    assistantExtension: value.assistantExtension,
                                    assistantEmail: value.assistantEmail,
                                    typeOfService: value.typeOfService,
                                    referredBy: value.referredBy,
                                    planID: value.planID,
                                    adminSchedulerID: value.adminSchedulerID,
                                    adminScheduler: self.getSchedulerRecord(value.adminSchedulerID,modifiedSchedRecords),
                                    taskTargetLow: value.taskTargetLow,
                                    mtgTargetLowDec: value.mtgTargetLowDec,
                                    realTimeAccess: value.realTimeAccess,
                                    realTimeUser: value.realTimeUser,
                                    realTimePassword: value.realTimePassword,
                                    esaAnniversary: value.esaAnniversary,
                                    schedulingStartDate: value.schedulingStartDate,
                                    lastCheckInDate: value.lastCheckInDate,
                                    surveySentDate: value.surveySentDate,
                                    termDate: value.termDate,
                                    additionalInfo: value.additionalInfo,
                                    sysCrmUserName: value.sysCrmUserName,
                                    sysCrmPassword: value.sysCrmPassword,
                                    sysCrmNotes: value.sysCrmNotes,
                                    sysEmailUserName: value.sysEmailUserName,
                                    sysEmailPassword: value.sysEmailPassword,
                                    sysEmailNotes: value.sysEmailNotes,
                                    sysCalendar: value.sysCalendar,
                                    sysCalendarUserName: value.sysCalendarUserName,
                                    sysCalendarPassword: value.sysCalendarPassword,
                                    sysCalendarNotes: value.sysCalendarNotes,
                                    dbAccountType: value.dbAccountType,
                                    checkInStatus: value.checkInStatus,
                                    returningClient: value.returningClient,
                                    dataCallDate: value.dataCallDate,
                                    dbBuildCompleted:  value.dbBuildCompleted,
                                    mapCreated: value.mapCreated,
                                    mapApproved: value.mapApproved,
                                    mapLastUpdated: value.mapLastUpdated, 
                                    linkedIn: value.linkedIn,
                                    cspTarget: value.cspTarget,
                                    clientAppPassword: value.clientAppPassword,
                                    companyEmailMFA: value.companyEmailMFA,
                                    tier: value.tier,
                                    wholesalerRank: value.wholesalerRank, 
                                    platform: value.platform,
                                    distSegment: value.distSegment,
                                    secondaryRank: value.secondaryRank,
                                    specialDBTags: value.specialDBTags,
                                    focusFirms: value.focusFirms,
                                    maptiveID:  value.maptiveID,
                                    clientTerrMap: value.clientTerrMap,
                                    schedDirectives: value.schedDirectives,
                                    zoneMapNotes: value.zoneMapNotes,
                                    adminDept: value.adminDept,
                                    specialInstForDB: value.specialInstForDB,
                                    mtgTypes: value.mtgTypes,
                                    dialerAcctTypes:  value.dialerAcctTypes,
                                    existingZoning: value.existingZoning,
                                    travelRotation: value.travelRotation
                                };
                                //console.log('ProgressClientDataGrid_ALL_View:getData:clientRecord:' + JSON.stringify(newRecord));

                                clientRecords.push(newRecord);
                            });

                            var fields = [];
                            fields.push({"clientID": {type: "number"}});
                            fields.push({"clientCompany": {type: "string"}});
                            fields.push({"lastName": {type: "string"}});
                            fields.push({"firstName": {type: "string"}});
                            fields.push({"meetingsTarget": {type: "number"}});

                            var configuration = {

                                selectable: "row",
                                sortable: true,
                                extra: false,
                                resizable: true,
                                reorderable: true,
                                filterable: {
                                    mode: "row"
                                },
                                //serverAggregates: true,
                                columnResize: function (e) {
                                    //self.resize();
                                    window.setTimeout(self.resize, 10);
                                },
                                dataBound: function (e) {
                                    console.log("ProgressClientDataGrid_ALL_View:def:onShow:dataBound");
                                    self.$('.client-profile').on('click', self.viewClientProfile);
                                    self.$('.company-profile').on('click', self.viewCompanyProfile);

                                    // save current filter
                                    var filters = null;
                                    if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null) { //&& e.sender.dataSource.filter() !== []
                                        filters = e.sender.dataSource.filter().filters;

                                        var clientDataGridFilter = [];

                                        $.each(filters, function (index, value) {
                                            clientDataGridFilter.push(value);
                                        });

                                        //console.log("ProgressClientDataGrid_ALL_View:def:onShow:dataBound:set clientDataGridFilter:" + JSON.stringify(clientDataGridFilter));
                                        app.model.set('clientDataGridFilter', clientDataGridFilter);

                                    } else {
                                        app.model.set('clientDataGridFilter', []);

                                    }

                                },
                                change: function (e) {
                                    //console.log('ProgressClientDataGrid_ALL_View:def:onShow:onChange');

                                },
                                save: function (e) {
                                    //console.log('ProgressClientDataGrid_ALL_View:def:onShow:onSave');
                                },
                                edit: function (e) {
                                    //console.log('ProgressClientDataGrid_ALL_View:def:onShow:onEdit');
                                },
                                dataSource: {
                                    data: clientRecords,
                                    sort: [
                                        {field: "clientCompany", dir: "asc"}
                                        //{field: "lastName", dir: "asc"}
                                    ],
                                    schema: {
                                        model: {
                                            fields: fields
                                        }
                                    }
                                    //aggregate: aggfields
                                }
                                //aggregate: aggfields
                            };

                            configuration.toolbar = [{name: "excel"}];
                            configuration.excel = {
                                fileName: "AllClientFieldsView.xlsx",
                                allPages: true
                            };
                            configuration.columns =  [
                                {
                                    title: "Client Name",
                                    field: "clientFullName",
                                    width: 200,
                                    template: function (e) {
                                        var template = "";
                                        if (self.userType > 1) {
                                            template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" + e.clientFullName + "</a></div>";
                                        } else {
                                            template = "<div style='text-align: left'><span>" + e.clientFullName + "</span></div>";
                                        }
                                        return template;

                                    },
                                    filterable: {
                                        cell: {
                                            showOperators: false,
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    },
                                    sortable: {
                                        initialDirection: "desc"
                                    }
                                },
                                {
                                    field: "customerName",
                                    title: "Customer Name",
                                    width: 150,
                                    filterable: false
                                },
                                {
                                    field: "companyClient",
                                    title: "ESA Company Client",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "clientSuccessPlan",
                                    title: "CSP",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "emailAddress",
                                    title: "Email Address",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "sbCount",
                                    title: "Count for Scoreboard",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "timeZoneName",
                                    title: "Time Zone",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "address",
                                    title: "Home Address",
                                    width: 150,
                                    filterable: false
                                },
                                {
                                    field: "city",
                                    title: "Home City",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "state",
                                    title: "Home State",
                                    width: 50,
                                    filterable: false
                                },
                                {
                                    field: "zipCode",
                                    title: "Home Zip Code",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "mobilePhone",
                                    title: "Mobile Phone",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "homePhone",
                                    title: "Home Phone",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    title: "Client Company",
                                    field: "clientCompany",
                                    template: function (e) {
                                        var template = "";
                                        if (self.userType > 1) {
                                            template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                        } else {
                                            template = "<div style='text-align: left'><span>" + e.clientCompany + "</span></div>";
                                        }
                                        return template;
                                    },
                                    width: 200,
                                    filterable: {
                                        cell: {
                                            showOperators: false,
                                            template: self.companyFilter
                                        }
                                    },
                                    sortable: true
                                },
                                {
                                    field: "teamLeaderName",
                                    title: "Account Manager",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "industry",
                                    title: "Industry",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "division",
                                    title: "Division",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "title",
                                    title: "Title",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "lfdAuthorization",
                                    title: "LFD Authorization",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "dsuNumber",
                                    title: "ESA Dialer CID #",
                                    width: 120,
                                    filterable: false
                                },
                                {
                                    field: "useAsReference",
                                    title: "Use As Reference",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "product",
                                    title: "Product/Service",
                                    width: 150,
                                    filterable: false
                                },
                                {
                                    field: "distributionChannel",
                                    title: "Channel",
                                    width: 150,
                                    filterable: false
                                },
                                {
                                    field: "states",
                                    title: "States",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "lengthInTerritory",
                                    title: "Length in Territory",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "workPhone",
                                    title: "Work Phone",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "workExtension",
                                    title: "Work Phone Extension",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "assistantInternal",
                                    title: "Assistant-Internal",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "assistantPhone",
                                    title: "Assistant Phone",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "assistantExtension",
                                    title: "Assistant Extension",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "assistantEmail",
                                    title: "Assistant Email",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "typeOfService",
                                    title: "Type of Service",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "referredBy",
                                    title: "Referred By",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "status",
                                    title: "Status",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "planName",
                                    title: "Plan Name",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "schedulerName",
                                    template: function (e) {
                                        //var template = "<div style='text-align: left'><a class='employee-profile btn-interaction' data-id='" + e.schedulerID + "' href='#employeeProfile'>" + self.getSchedulerName(e.schedulerName) + "</a></div>";
                                        var template = "<div style='text-align: left'><a class='employee-profile btn-interaction' data-id='" + e.schedulerID + "' href='#employeeProfile'>" + e.schedulerName + "</a></div>";
                                        return template;

                                    },
                                    title: "Scheduler",
                                    width: 200,
                                    hidden: self.myClients,
                                    filterable: {
                                        cell: {
                                            showOperators: false,
                                            template: self.schedulerFilter
                                        }
                                    }
                                },
                                {
                                    field: "adminScheduler",
                                    title: "Admin Scheduler",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "tasksTarget",
                                    title: "Target Tasks",
                                    template: function (e) {
                                        var template = "<div style='text-align: left'>" + app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e.tasksTarget) + "</div>";
                                        return template;
                                    },
                                    //footerTemplate: "#= kendo.toString(sum,'n2') #",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "meetingsTarget",
                                    title: "Target Meetings",
                                    template: function (e) {
                                        var template = "<div style='text-align: left'>" + app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e.meetingsTarget) + "</div>";
                                        return template;
                                    },
                                    //footerTemplate: "#= kendo.toString(sum,'n2') #",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "personalEmail",
                                    title: "Personal Email",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "personalPhone",
                                    title: "Personal Phone",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "esaAnniversary",
                                    title: "ESA Ann",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "termDate",
                                    title: "Term Date",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "schedulingStartDate",
                                    title: "Sched Start",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "checkInStatus",
                                    title: "Check In Status",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "lastCheckInDate",
                                    title: "Last Check-In",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "surveySentDate",
                                    title: "Survey Sent",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "returningClient",
                                    title: "Returning Client",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "additionalInformation",
                                    title: "Additional Information",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "dbAccountType",
                                    title: "DB Account Type",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "sysCrmUserName",
                                    title: "Company CRM Username",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "sysCrmPassword",
                                    title: "Company CRM Password",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "sysCrmNotes",
                                    title: "Company CRM Notes",
                                    width: 200,
                                    filterable: false
                                },
                                {
                                    field: "sysEmailUserName",
                                    title: "Company Email Username",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "sysEmailPassword",
                                    title: "Company Email Password",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "sysEmailNotes",
                                    title: "Company Email Notes",
                                    width: 200,
                                    filterable: false
                                },
                                {
                                    field: "sysCalendar",
                                    title: "Client Calendar System URL",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "sysCalendarUserName",
                                    title: "Client Calendar Username",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "sysCalendarPassword",
                                    title: "Client Calendar Password",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "sysCalendarNotes",
                                    title: "Client Calendar Notes",
                                    width: 200,
                                    filterable: false
                                },
                                {
                                    field: "clientID",
                                    title: "ClientID",
                                    width: 75,
                                    filterable: false
                                },
                                {
                                    field: "dataCallDate",
                                    title: "Call Back Date",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "dbBuildCompleted",
                                    title: "DB Build Completed",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "mapCreated",
                                    title: "Map Created",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "mapApproved",
                                    title: "Map Approved",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "mapLastUpdated",
                                    title: "Map Last Updated",
                                    width: 100,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "linkedIn",
                                    title: "Linked In",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "cspTarget",
                                    title: "CSP Target",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "clientAppPassword",
                                    title: "Client App Password",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "companyEmailMFA",
                                    title: "Company Email MFA",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "tier",
                                    title: "Tier",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "wholesalerRank",
                                    title: "Wholesaler Rank",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    field: "platform",
                                    title: "Platform",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Dist Segment",
                                    field: "distSegment",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Secondary Rank",
                                    field: "secondaryRank",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Special DB Tags",
                                    field: "specialDBTags",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Focus Firms",
                                    field: "focusFirms",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Maptive ID",
                                    field: "maptiveID",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Client Terr Map",
                                    field: "clientTerrMap",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Sched Directives",
                                    field: "schedDirectives",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Zone Map Notes",
                                    field: "zoneMapNotes",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Admin Dept",
                                    field: "adminDept",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Special Inst For DB",
                                    field: "specialInstForDB",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Meeting Types",
                                    field: "mtgTypes",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Dialer Acct Types",
                                    field: "dialerAcctTypes",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Existing Zoning",
                                    field: "existingZoning",
                                    width: 100,
                                    filterable: false
                                },
                                {
                                    title: "Travel Rotation",
                                    field: "travelRotation",
                                    width: 100,
                                    filterable: false
                                }
                            ];

                            var grid = self.$("#clientData").kendoGrid(configuration).data("kendoGrid");
                            if (self.clientDataGridFilter !== null) { //&& self.clientDataGridFilter !== []
                                var filters = [];
                                $.each(self.clientDataGridFilter, function (index, value) {
                                    filters.push(value);
                                });

                                //console.log("ProgressClientDataGrid_ALL_View:grid:set datasource:filter" + JSON.stringify(filters));
                                grid.dataSource.filter(filters);
                            } else {
                                //grid.dataSource.filter({field: "status", operator: "eq", value: '4 - Active'});
                            }

                            // Resize the grid
                            setTimeout(self.resize, 0);
                        });
                    });
                }

            },
            companyFilter: function (container) {
                var self = this;

                //console.log('ProgressClientDataGrid_ALL_View:companyFilter:' + JSON.stringify(container));
                self.companyDataSource.fetch(function (data) {

                        var dataSource = [];
                        var records = this.data();

                        $.each(records, function (index, value) {
                            dataSource.push({value: value.clientCompany, text: value.clientCompany});
                        });

                        container.element.kendoDropDownList({
                            //autoBind:false,
                            dataValueField: "value",
                            dataTextField: "text",
                            valuePrimitive: true,
                            dataSource: dataSource,
                            optionLabel: "Select Value"
                            //change: self.onCompanyFilterChanged
                        });

                        // Resize the grid
                        //setTimeout(self.resize, 0);
                    });

            },
            viewCompanyProfile: function (e) {
                //console.log('ProgressClientDataGrid_ALL_View:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientDataGrid_ALL_View:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId', companyId);


                App.router.navigate('companyProfile', {trigger: true});

            },

            viewClientProfile: function (e) {
                //console.log('ProgressClientDataGrid_ALL_View:viewClientProfile:e:' + e.target);

                var clientId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    clientId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    clientId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientDataGrid_ALL_View:viewClientProfile:e:' + clientId);
                App.model.set('selectedClientId', clientId);

                App.router.navigate('clientProfile', {trigger: true});

            },

            displayNoDataOverlay: function () {
                //console.log('ProgressClientDataGrid_ALL_View:displayNoDataOverlay');

                // Hide the grid
                this.$('#clientData').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressClientDataGrid_ALL_View:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#clientData').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressClientDataGrid_ALL_View:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#clientData").parent().parent().height();
                //console.log('ProgressClientDataGrid_ALL_View:resize:parentHeight:' + parentHeight);

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                this.$("#clientData").height(parentHeight);
                parentHeight = parentHeight - 75;

                //console.log('ProgressClientDataGrid_ALL_View:resize:parentHeight:' + parentHeight);

                this.$("#clientData").find(".k-grid-content").height(parentHeight - headerHeight);


            },
            remove: function () {
                //console.log("-------- ProgressClientDataGrid_ALL_View:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#clientData").data('ui-tooltip'))
                    this.$("#clientData").tooltip('destroy');

                this.clientDataGridFilter = [];

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });