define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalTipTable', 'views/PortalTipGridView'],
    function (App, Backbone, Marionette, $, template, PortalTipGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                tipRegion: "#tip-region"
            },
            initialize: function () {

                //console.log('PortalTipTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('PortalTipTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalTipTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.tipRegion.show(new PortalTipGridView());
                }

            },
            validateData: function () {
                var self = this;

                if (App.modal.currentView === null || App.modal.currentView === undefined) {

                }
            },
            resize: function () {
                //console.log('PortalTipTableView:resize');

                if (this.tipRegion.currentView)
                    this.tipRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalTipTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });