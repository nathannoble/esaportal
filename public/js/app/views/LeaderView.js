define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/leader','models/LeaderModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            initialize: function () {
                //console.log('LeaderView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('LeaderView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- LeaderView:onShow --------');

                var app = App;
                this.employeeID = this.model.get('employeeID');
                this.employeeName = this.model.get('employeeName');
                this.mtgsPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(this.model.get('mtgsPerHour'));

                this.$('#employeeName').html(this.employeeName);
                this.$('#mtgsPerHour').html(this.mtgsPerHour + " Mtgs/Hr");

           },

            onResize: function () {
//                //console.log('LeaderView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('LeaderView:resize');

            },
            remove: function () {
                //console.log('-------- LeaderView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#leaderRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
