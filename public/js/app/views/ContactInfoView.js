define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/clientContactInfo', 'hbs!templates/companyContactInfo',
        'hbs!templates/employeeContactInfo',
        'kendo/kendo.data'],
    function (App, Backbone, Marionette, $, clientTemplate,companyTemplate,employeeTemplate) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: function () {

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Client"};
                }

                //console.log('ContactInfoView:template:option:' + this.options.type);

                if (this.options.type === "Client") {
                    return clientTemplate;
                } else if (this.options.type === "Company") {
                    return companyTemplate;
                } else if (this.options.type === "Employee") {
                    return employeeTemplate;
                }

            },
            initialize: function (options) {

                this.clientID = parseInt(App.model.get('selectedClientId'),0); //2189;
                console.log('ContactInfoView:initialize:clientID:' + this.clientID);

                this.employeeID =  parseInt(App.model.get('selectedEmployeeId'),0); //100;
                console.log('ContactInfoView:initialize:employeeID:' + this.employeeID);

                this.companyID = parseInt(App.model.get('selectedCompanyId'),0); //4;

                var app = App;

                this.dateFilter = App.model.get('selectedDateFilter');
                this.startDate = new Date(app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter)); //"2016-10-01T00:00:00+0000"
                this.endDate = new Date(app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter)); //"2016-10-31T23:59:59+0000"
                this.endDate = this.endDate.addHours(23);
                this.endDate = this.endDate.addMinutes(59);

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- ContactInfoView:onShow ----------');

                this.populateWidget();
            },
            populateWidget: function () {
//                //console.log('ContactInfoView:populateWidget');

                var self = this;
                var app = App;

                if (this.options.type === "Client") {

                    this.progressClientFilters = {
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {field: "planEndDate", operator: "eq", value: null},
                                    {field: "planEndDate", operator: "gt", value: this.endDate}
                                ]
                            },
                            {field: "planStartDate", operator: "lt", value: this.endDate},
                            {field: "clientID", operator: "eq", value: this.clientID}
                        ]
                    };

                    console.log('ContactInfoView:initialize:this.progressClientFilters:' + JSON.stringify(this.progressClientFilters));

                    this.dataSource = new kendo.data.DataSource(
                        {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressClient";
                                },
                                complete: function (e) {
                                    console.log('ContactInfoView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ContactInfoView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "clientID",
                                fields: {
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    planName: {editable: true, type: "string", validation: {required: false}},
                                    planPrice: {type: "number"},
                                    schedulerName: {editable: true, type: "string", validation: {required: false}},
                                    workPhone: {editable: true, type: "string", validation: {required: false}},
                                    workExtension: {editable: true, type: "string", validation: {required: false}},
                                    homePhone: {editable: true, type: "string", validation: {required: false}},
                                    mobilePhone: {editable: true, type: "string", validation: {required: false}},
                                    emailAddress: {editable: true, type: "string", validation: {required: false}},
                                    assistantInternal: {editable: true, type: "string", validation: {required: false}},
                                    assistantPhone: {editable: true, type: "string", validation: {required: false}},
                                    assistantEmail: {editable: true, type: "string", validation: {required: false}},
                                    additionalInformation: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: false}
                                    },
                                    planStartDate: {editable: true, type: "date", validation: {required: false}},
                                    planEndDate: {editable: true, type: "date", validation: {required: false}}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   ContactInfoView:requestStart');

                            kendo.ui.progress(self.$("#loadingContactInfo"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    ContactInfoView:requestEnd');
                            kendo.ui.progress(self.$("#loadingContactInfo"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ContactInfoView:error');
                            kendo.ui.progress(self.$("#loadingContactInfo"), false);

                            self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        change: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ContactInfoView:change');

                            var data = this.data();

                            if (data.length <= 0)
                                self.displayNoDataOverlay();
                            else
                                self.hideNoDataOverlay();
                        },
                        filter: self.progressClientFilters
                        //filter: [{field: "clientID", operator: "eq", value: self.clientID}]

                    });

                    this.dataSource.fetch(function () {

                        var record;

                        if (this.data().length === 0) {
                            self.dataSource.filter([{field: "clientID", operator: "eq", value: self.clientID}]);
                            self.dataSource.fetch(function () {
                                record = this.data()[0];
                                self.setClientProfileValues(record);
                            });
                        } else {
                            record = this.data()[0];
                            self.setClientProfileValues(record);
                        }

                    });
                } else if (this.options.type === "Company") {

                    this.dataSource = new kendo.data.DataSource(
                        {
                            //autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                    complete: function (e) {
                                        console.log('ContactInfoView:dataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ContactInfoView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ContactInfoView:update');
                                        return App.config.DataServiceURL + "/odata/ProgressCompany" + "(" + data.companyID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ContactInfoView:dataSource:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ContactInfoView:dataSource:updateerror:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('ContactInfoView:create');
                                        return App.config.DataServiceURL + "/odata/ProgressCompany";
                                    },
                                    complete: function (e) {
                                        console.log('ContactInfoView:dataSource:create:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ContactInfoView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('ContactInfoView:destroy');
                                        return App.config.DataServiceURL + "/odata/ProgressCompany" + "(" + data.companyID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ContactInfoView:dataSource:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ContactInfoView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "companyID",
                                    fields: {
                                        companyID: {editable: false, type: "number"},
                                        clientCompany: {editable: true, type: "string", validation: {required: false}},
                                        stageOfSale: {editable: true, type: "string", validation: {required: false}},
                                        contactFirstName: {editable: true, type: "string", validation: {required: false}},
                                        contactLastName: {editable: true, type: "string", validation: {required: false}},
                                        contactPhone: {editable: true, type: "string", validation: {required: false}},
                                        contactEmail: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            sort: {field: "companyID", dir: "asc"},
                            requestStart: function (e) {
                                //console.log('ContactInfoView:dataSource:request start:');
                                kendo.ui.progress(self.$("#loadingContactInfo"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('ContactInfoView:dataSource:request end:');
                                //var data = this.data();
                                kendo.ui.progress(self.$("#loadingContactInfo"), false);
                            },
                            change: function (e) {
                                //console.log('ContactInfoView:dataSource:change:');

                                var data = this.data();
                                if (data.length > 0) {

                                } else {
                                    self.displayNoDataOverlay();
                                }
                            },
                            error: function (e) {
                                //console.log('ContactInfoView:dataSource:error');
                                kendo.ui.progress(self.$("#loadingContactInfo"), false);
                            },
                            filter: [{field: "companyID", operator: "eq", value: self.companyID}]
                        });
                    this.dataSource.fetch(function () {

                        var record = this.data()[0];

                        var company = record.clientCompany;
                        var stageOfSale = record.stageOfSale;
                        var firstName = record.contactFirstName;
                        var lastName = record.contactLastName;
                        var contactEmail = record.contactEmail;
                        var contactPhone = record.contactPhone;

                        self.$('#company').html(company);
                        self.$('#stageOfSale').html(stageOfSale);
                        self.$('#primaryContact').html(firstName + " " + lastName);
                        self.$('#contactPhone').html(contactPhone);
                        self.$('#contactEmail').html(contactEmail);
                        self.$('#cspType').html(record.cspType);
                        self.$('#ratesStructure').html(record.ratesStructure);
                        self.$('#billingDetails').html(record.billingDetails);

                    });

                } else if (this.options.type === "Employee") {

                    this.dataSource = new kendo.data.DataSource(
                        {
                            //autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                    complete: function (e) {
                                        console.log('ContactInfoView:dataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ContactInfoView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ContactInfoView:update');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ContactInfoView:dataSource:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ContactInfoView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('ContactInfoView:create');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployee";
                                    },
                                    complete: function (e) {
                                        console.log('ContactInfoView:dataSource:create:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ContactInfoView:dataSource:createerror:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('ContactInfoView:destroy');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ContactInfoView:dataSource:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ContactInfoView:dataSource:destroy;error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "employeeID",
                                    fields: {
                                        employeeID: {editable: false, type: "number"},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        emailAddress: {editable: true, type: "string", validation: {required: true}},
                                        esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                        esaTollFree: {type: "string", validation: {required: false}},
                                        homePhone: {type: "string", validation: {required: false}},
                                        mobilePhone: {type: "string", validation: {required: false}},
                                        voicemailPasscode: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: true}
                                        },
                                        appPasscode: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: true}
                                        },
                                        status: {
                                            type: "string",
                                            validation: {required: false},
                                            values: [
                                                {text: '1 - Active', value: '1 - Active'},
                                                {text: '2 - Termed', value: '2 - Termed'},
                                                {text: '3 - No Hire', value: '3 - No Hire'}
                                            ]
                                        },
                                        timerStatus: {editable: true, type: "number"},
                                        additionalInfo: {type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            sort: {field: "employeeID", dir: "asc"},
                            requestStart: function (e) {
                                //console.log('ContactInfoView:dataSource:request start:');
                                kendo.ui.progress(self.$("#loadingContactInfo"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('ContactInfoView:dataSource:request end:');
                                //var data = this.data();
                                kendo.ui.progress(self.$("#loadingContactInfo"), false);
                            },
                            change: function (e) {
                                //console.log('ContactInfoView:dataSource:change:');

                                var data = this.data();
                                if (data.length > 0) {

                                } else {
                                    self.displayNoDataOverlay();
                                }
                            },
                            error: function (e) {
                                //console.log('ContactInfoView:dataSource:error');
                                kendo.ui.progress(self.$("#loadingContactInfo"), false);
                            },
                            filter: [{field: "employeeID", operator: "eq", value: self.employeeID}]
                        });

                    this.dataSource.fetch(function () {

                        var record = this.data()[0];

                        var employee = record.firstName + " " + record.lastName;
                        var esaLocalPhone = record.esaLocalPhone;
                        var esaTollFree = record.esaTollFree;
                        var homePhone = record.homePhone;
                        var mobilePhone = record.mobilePhone;

                        var email = record.emailAddress;
                        var voicemailPasscode = record.voicemailPasscode;
                        var appPasscode = record.appPasscode;
                        var status = record.status;
                        var timerStatus = record.timerStatus;
                        var additionalInfo = record.additionalInfo;

                        self.$('#employee').html(employee);
                        self.$('#esaLocalPhone').html(esaLocalPhone);
                        self.$('#esaTollFreePhone').html(esaTollFree);
                        self.$('#homePhone').html(homePhone);
                        self.$('#mobilePhone').html(mobilePhone);
                        self.$('#email').html(email);
                        self.$('#passcode').html(voicemailPasscode);
                        self.$('#appPasscode').html(appPasscode);
                        self.$('#status').html(status);
                        self.$('#timerStatus').html(timerStatus);
                        self.$('#additionalInfo').html(additionalInfo);

                    });
                }
            },
            setClientProfileValues: function (record) {
                console.log('ContactInfoView:setClientProfileValues:record:' + JSON.stringify(record));

                var self = this;
                var app = App;

                var client = record.firstName + " " + record.lastName;
                var company = record.clientCompany;
                var plan = record.planName;
                var planPrice = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(record.planPrice);
                var scheduler = record.schedulerName;
                var email = record.emailAddress;
                var workPhone = record.workPhone;
                if (record.workExtension) {
                    workPhone = record.workPhone + " " + record.workExtension;
                }
                var mobilePhone = record.mobilePhone;
                var homePhone = record.homePhone;
                var assistantInternal = record.assistantInternal;
                var assistantEmail = record.assistantEmail;
                var assistantPhone = record.assistantPhone;
                var additionalInfo = record.additionalInformation;

                self.$('#client').html(client);
                self.$('#company').html(company);
                self.$('#plan').html(plan);
                self.$('#planAmount').html("$" + planPrice);
                self.$('#scheduler').html(scheduler);
                self.$('#email').html(email);
                self.$('#workPhone').html(workPhone);
                self.$('#mobilePhone').html(mobilePhone);
                self.$('#homePhone').html(homePhone);
                self.$('#assistantInternal').html(assistantInternal);
                self.$('#assistantEmail').html(assistantEmail);
                self.$('#assistantPhone').html(assistantPhone);
                self.$('#additionalInfo').html(additionalInfo);
            },
            displayNoDataOverlay: function () {
                //console.log('ContactInfoView:displayNoDataOverlay');

                // Hide the grid
                this.$('.contact-info').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ContactInfoView:hideNodDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('.contact-info').css('display', 'block');
            },
            onResize: function () {
//                //console.log('ContactInfoView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('ContactInfoView:resize');

            },
            remove: function () {
                //console.log('---------- ContactInfoView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off
                this.$('.tile-content').off();
                this.$('.tile-more-info').off();
                this.$('.tile-hover-tip').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });