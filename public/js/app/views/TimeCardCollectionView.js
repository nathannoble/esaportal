define(['App', 'backbone', 'marionette', 'jquery', 'models/TimeCardModel', 'hbs!templates/timeCardCollection',
        'views/TimeCardEntryView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, TimeCardModel, template,
              TimeCardEntryView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: TimeCardEntryView,
            initialize: function (options) {
                //console.log('TimeCardCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.entries = [];

                var self = this;
                var app = App;

                this.myEmployeeId = App.model.get('myEmployeeId');
                //console.log('TimeCardCollectionView:initialize:myEmployeeId:' + this.myEmployeeId);
                if (this.myEmployeeId !== null && this.myEmployeeId !== undefined) {
                    this.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);

                    this.dateFilter = App.model.get('selectedDateFilter');
                    var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                    var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                    this.startDate = moment(startDate,'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                    this.endDate = moment(endDate,'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                    this.getData();
                }

            },
            onShow: function () {
                //console.log("-------- TimeCardCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                this.restDataSource = new kendo.data.DataSource(
                    {
                    autoBind: false,
                    transport: {
                        type: "odata",
                        read: {
                            url: function () {
                                var url = App.config.DataServiceURL + "/rest/GetMyTimeCardDetail/" + self.startDate + "/" + self.endDate + "/" + self.myEmployeeId;
                                return url;
                            },
                            complete: function (e) {
                                console.log('TimeCardCollectionView:restDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'TimeCardCollectionView:restDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            type: "GET",
                            dataType: "json"
                        }
                        //autoBind: false,
                    },
                    schema: {
                        //data: "value",
                        //total: function (data) {
                        //    return data['odata.count'];
                        //},
                        model: {
                            id: "employeeID",
                            fields: {

                                intTimerID: {editable: false, type: "number"},
                                companyID: {editable: false, type: "number"},
                                clientID: {editable: false, type: "number"},
                                employeeID: {editable: false, type: "number"},
                                serviceID: {editable: false, type: "number"},
                                workDate: {
                                    editable: true,
                                    type: "date"
                                    //format: "{0:yyyy-MM-ddTHH}",
                                    //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                },
                                startTime: {
                                    editable: true,
                                    type: "date"
                                    //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                },
                                stopTime: {
                                    editable: true,
                                    type: "date"
                                    //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                },
                                clientCompany: {editable: true, type: "string", validation: {required: false}},
                                clientName: {editable: true, type: "string", validation: {required: false}},
                                customerName: {editable: true, type: "string", validation: {required: true}},
                                employeeName: {editable: true, type: "string", validation: {required: false}},
                                serviceItem: {type: "string", validation: {required: false}},
                                tasks: {editable: true, type: "number"},
                                meetings: {editable: true, type: "number"},
                                timeWorked: {editable: true, type: "number"},
                                timerNote: {editable: true, type: "string", validation: {required: false}},
                                mgrNote: {editable: true, type: "string", validation: {required: false}},
                                timerAction: {editable: true, type: "string", validation: {required: false}},
                                recordType: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    group: {field: "clientName"},
                    sort: [{field: "workDate", dir: "desc"}],
                    //filter: {field: "lastName", operator: "neq", value: ""},
                    requestStart: function (e) {
                        //console.log('MyTimeCardDetailGridView:dataSource:request start:');
                        kendo.ui.progress(self.$("#timeCardDetailLoading"), true);
                    },
                    requestEnd: function (e) {
                        //console.log('MyTimeCardDetailGridView:dataSource:request end:');
                        //var data = this.data();
                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                    },
                    change: function (e) {
                        //console.log('MyTimeCardDetailGridView:dataSource:change:');

                        var data = this.data();
                        if (data.length > 0) {

                        } else {
                            self.displayNoDataOverlay();
                        }
                    },
                    error: function (e) {
                        //console.log('MyTimeCardDetailGridView:dataSource:error');
                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                    }
                });

                if (this.options.type === "Daily") {
                    self.restDataSource.fetch(function () {

                        var data = this.data();
                        var lastWorkDate = null;
                        var lastWorkDay = 0;
                        var sumTimeWorked = 0;
                        var sumPeriodTimeWorked = 0;
                        var timePeriod = "Total ";
                        var start = null;
                        var end = null;

                        $.each(data, function (index, value) {

                            //console.log('TimeCardCollectionView:entry:' + JSON.stringify(value));

                            var thisWorkDate = moment(new Date(value.workDate)).clone().format('MM/DD/YY');
                            var thisWorkDay = moment(new Date(value.workDate)).clone().date();
                            //var thisWorkMonth = moment(new Date(value.workDate)).clone().format('MMM YYYY');

                            if (thisWorkDate !== lastWorkDate && lastWorkDate !== null) {

                                self.entries.push({
                                    workDate: lastWorkDate, //value.workDate,
                                    sumTimeWorked: sumTimeWorked
                                });

                                //console.log('TimeCardCollectionView:entries:push');
                                if (thisWorkDay > lastWorkDay || (thisWorkDay < 16 && lastWorkDay > 15)) {

                                    //console.log('TimeCardCollectionView:entries:push');

                                    if (lastWorkDay > 15) {
                                        start = moment(lastWorkDate,'MM/DD/YY').startOf('month').add(15,'days').clone().format('MM/DD/YY');
                                        end = moment(lastWorkDate,'MM/DD/YY').endOf('month').clone().format('MM/DD/YY');
                                        timePeriod = start + " - " + end + " Total: ";
                                    } else {
                                        start = moment(lastWorkDate,'MM/DD/YY').startOf('month').clone().format('MM/DD/YY');
                                        end = moment(lastWorkDate,'MM/DD/YY').startOf('month').add(14,'days').clone().format('MM/DD/YY');
                                        timePeriod = start + " - " + end + " Total: ";
                                    }
                                    self.entries.push({
                                        workDate: timePeriod,
                                        sumTimeWorked: sumPeriodTimeWorked
                                    });
                                    sumPeriodTimeWorked = value.timeWorked;
                                } else {
                                    sumPeriodTimeWorked += value.timeWorked;
                                }

                                sumTimeWorked = value.timeWorked;

                            } else {
                                sumTimeWorked += value.timeWorked;
                                sumPeriodTimeWorked += value.timeWorked;
                            }

                            lastWorkDate = thisWorkDate;
                            lastWorkDay = thisWorkDay;

                            // last record
                            if (index === data.length - 1) {
                                self.entries.push({
                                    workDate: thisWorkDate,
                                    sumTimeWorked: sumTimeWorked
                                });

                                if (lastWorkDay > 15) {
                                    start = moment(lastWorkDate,'MM/DD/YY').startOf('month').add(15,'days').clone().format('MM/DD/YY');
                                    end = moment(lastWorkDate,'MM/DD/YY').endOf('month').clone().format('MM/DD/YY');
                                    timePeriod = start + " - " + end + " Total: ";
                                } else {
                                    start = moment(lastWorkDate,'MM/DD/YY').startOf('month').clone().format('MM/DD/YY');
                                    end = moment(lastWorkDate,'MM/DD/YY').startOf('month').add(14,'days').clone().format('MM/DD/YY');
                                    timePeriod = start + " - " + end + " Total: ";
                                }
                                self.entries.push({
                                    workDate: timePeriod,
                                    sumTimeWorked: sumPeriodTimeWorked
                                });
                            }

                        });

                        // Populate collection
                        self.collection.reset();
                        self.collection.comparator = function (model) {
                            return model.get("workDate");
                        };

                        //console.log('TimeCardCollectionView:all entries:' + JSON.stringify(self.entries));

                        $.each(self.entries.reverse(), function (index, value) {

                            //console.log('TimeCardCollectionView:entry:' + JSON.stringify(value));
                            var model = new TimeCardModel();

                            model.set("workDate", value.workDate);
                            model.set("sumTimeWorked", value.sumTimeWorked);

                            self.collection.push(model);
                        });

                    });
                } else {
                    // Bi-monthly summary
                    self.restDataSource.fetch(function () {

                        var data = this.data();
                        var lastWorkDate = null;
                        var lastWorkDay = 0;
                        var sumTimeWorked = 0;
                        var timePeriod = "First half of ";
                        var start = null;
                        var end = null;

                        //var today = new Date();
                        //this.day = today.getDate();

                        $.each(data, function (index, value) {

                            //console.log('TimeCardCollectionView:entry:' + JSON.stringify(value));

                            var thisWorkDate = moment(new Date(value.workDate)).clone().format('YYYY-MM-DD');
                            var thisWorkDay = moment(new Date(value.workDate)).clone().date();
                            //var thisWorkMonth = moment(new Date(value.workDate)).clone().format('MMM YYYY');

                            if (thisWorkDate !== lastWorkDate && lastWorkDate !== null) {
                                if (thisWorkDay > lastWorkDay || (thisWorkDay < 16 && lastWorkDay > 15)) {

                                    //console.log('TimeCardCollectionView:entries:push');

                                    if (lastWorkDay > 15) {
                                        start = moment(lastWorkDate,'YYYY-MM-DD').startOf('month').add(15,'days').clone().format('MM/DD/YY');
                                        end = moment(lastWorkDate,'YYYY-MM-DD').endOf('month').clone().format('MM/DD/YY');
                                        timePeriod = start + " - " + end ;
                                    } else {
                                        start = moment(lastWorkDate,'YYYY-MM-DD').startOf('month').clone().format('MM/DD/YY');
                                        end = moment(lastWorkDate,'YYYY-MM-DD').startOf('month').add(14,'days').clone().format('MM/DD/YY');
                                        timePeriod = start + " - " + end ;
                                    }
                                    self.entries.push({
                                        workDate: timePeriod,
                                        sumTimeWorked: sumTimeWorked
                                    });
                                    sumTimeWorked = value.timeWorked;
                                } else {
                                    sumTimeWorked += value.timeWorked;
                                }
                            } else {
                                sumTimeWorked += value.timeWorked;
                            }
                            lastWorkDate = thisWorkDate;
                            lastWorkDay = thisWorkDay;

                            // last record
                            if (index === data.length - 1) {
                                if (lastWorkDay > 15) {
                                    start = moment(lastWorkDate,'YYYY-MM-DD').startOf('month').add(15,'days').clone().format('MM/DD/YY');
                                    end = moment(lastWorkDate,'YYYY-MM-DD').endOf('month').clone().format('MM/DD/YY');
                                    timePeriod = start + " - " + end ;
                                } else {
                                    start = moment(lastWorkDate,'YYYY-MM-DD').startOf('month').clone().format('MM/DD/YY');
                                    end = moment(lastWorkDate,'YYYY-MM-DD').startOf('month').add(14,'days').clone().format('MM/DD/YY');
                                    timePeriod = start + " - " + end ;
                                }
                                self.entries.push({
                                    workDate: timePeriod,
                                    sumTimeWorked: sumTimeWorked
                                });
                            }

                        });

                        // Populate collection
                        self.collection.reset();
                        self.collection.comparator = function (model) {
                            return model.get("workDate");
                        };

                        //console.log('TimeCardCollectionView:all entries:' + JSON.stringify(self.entries));

                        $.each(self.entries.reverse(), function (index, value) {

                            //console.log('TimeCardCollectionView:entry:' + JSON.stringify(value));
                            var model = new TimeCardModel();

                            model.set("workDate", value.workDate);
                            model.set("sumTimeWorked", value.sumTimeWorked);

                            self.collection.push(model);
                        });

                    });
                }

            },
            hideNoDataOverlay: function () {
                //console.log('TimeCardCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#phoneWidgetContent').css('display', 'block');
            },
            displayNoDataOverlay: function () {
                //console.log('TimeCardCollectionView:displayNoDataOverlay');

                // Hide the widget content
                //this.$('#phoneWidgetContent').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message" style="height: 190px;">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {
                //console.log('TimeCardCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('TimeCardCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- TimeCardCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
