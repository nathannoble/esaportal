define(['App', 'jquery', 'hbs!templates/esaStatsLayout', 'backbone', 'marionette', 'underscore', 'options/ViewOptions',
        'views/StatisticsChartView','views/SummaryWidgetView','views/SubHeaderView'],
    function (App, $, template, Backbone, Marionette, _, ViewOptions,  StatisticsChartView,SummaryWidgetView,SubHeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template:template,
            regions:{
                subHeaderRegion: "#sub-header",
                tile1: "#tile-1",
                tile2: "#tile-2",
                tile3: "#tile-3",
                tile1a: "#tile-1a",
                tile2a: "#tile-2a",
                tile3a: "#tile-3a"
                //tile1b: "#tile-1b",
                //tile2b: "#tile-2b",
                //tile3b: "#tile-3b"

            },
            initialize: function(){
                //console.log('StatsLayoutView:initalize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedClientIdChanged, this.onSelectedClientIdChanged);
            },
            onRender: function(){
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                //console.log('StatsLayoutView:onShow');

                var self = this;
                this.userId = App.model.get('userId');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    //console.log('ESAStatsLayoutView:onShow:userId:' + this.userId);
                    this.resetLayout();
                }

                this.resetLayout();
            },
            resetLayout: function () {
                //console.log('ESAStatsLayoutView:resetDashboard');

                this.subHeaderRegion.reset();
                this.tile1.reset();
                this.tile2.reset();
                this.tile3.reset();

                this.tile1a.reset();
                this.tile2a.reset();
                this.tile3a.reset();

                //this.tile1b.reset();
                //this.tile2b.reset();
                //this.tile3b.reset();

                this.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Statistics View",
                    minorTitle: '',
                    page: "Statistics View",
                    showDateFilter: true,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: true,
                    showTeamLeaderFilter:false,
                    showPayrollProcessingDate:false
                }));

                this.tile1.show(new StatisticsChartView({type: 'Time'}));
                this.tile2.show(new StatisticsChartView({type: 'Tasks'}));
                this.tile3.show(new StatisticsChartView({type: 'Meetings'}));

                this.tile1a.show(new SummaryWidgetView({type: 'Time'}));
                this.tile2a.show(new SummaryWidgetView({type: 'Tasks'}));
                this.tile3a.show(new SummaryWidgetView({type: 'Meetings'}));

                //this.tile1b.show(new SummaryWidgetView({type: 'Time'}));
                //this.tile2b.show(new SummaryWidgetView({type: 'Tasks'}));
                //this.tile3b.show(new SummaryWidgetView({type: 'Meetings'}));

            },
            onSelectedClientIdChanged: function () {
                //console.log('StatsLayoutView:onSelectedClientIdChanged');
                this.resetLayout();
            },
            onSelectedDateFilterChanged: function () {
                //console.log('StatsLayoutView:onSelectedDateFilterChanged');
                this.resetLayout();
            },
            transitionIn: function(){
                //console.log('StatsLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function(){
                //console.log('StatsLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
                //console.log('StatsLayoutView:resize');


            },
            remove: function(){
                //console.log('---------- StatsLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });