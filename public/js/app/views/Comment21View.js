define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/comment21', 'views/Comment21ReplyView', 'models/CommentModel'],
    function (App, Backbone, Marionette, $, template,Comment21ReplyView) {
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #deleteComment': 'deleteCommentClicked',
                'click #replyToComment': 'replyToCommentClicked'
            },
            initialize: function () {
                //console.log('Comment21View:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('Comment21View:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- Comment21View:onShow --------');

                var self = this;

                this.myUserKey = App.model.get('userKey');
                this.myUserType = App.model.get('userType');

                this.messageID = this.model.get('message_id');

                // Apply localization on read rather than write of comment data (10/27/20 - NN)

                var offset = (new Date()).getTimezoneOffset();
                var date = new Date(moment(this.model.get('time_stamp')).clone().subtract((offset), 'minutes'));
                var time =   moment(date).format('h:mm a');

                this.commentID = this.model.get('comment_id');
                this.commentText = this.model.get('comment_text');
                this.timeStamp = time;
                this.userKey = this.model.get('user_key');
                this.lastName = this.model.get('last_name');
                this.firstName = this.model.get('first_name');
                this.fileName = this.model.get('fileName');
                this.replyCommentID = this.model.get('reply_comment_id');
                this.addUserImage = this.model.get('addUserImage');

                if (this.myUserType === 3 || this.myUserKey === this.userKey) {
                    this.$('#deleteComment').show();
                } else {
                    this.$('#deleteComment').hide();
                }

                this.messageCommentDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalMessagesComment";
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('Comment21View:update');
                                    return App.config.DataServiceURL + "/odata/PortalMessagesComment" + "(" + data.commentID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('Comment21View:create');
                                    return App.config.DataServiceURL + "/odata/PortalMessagesComment";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    console.log('Comment21View:destroy');
                                    App.model.set('messageCommentDeletedId', data.commentID);
                                    return App.config.DataServiceURL + "/odata/PortalMessagesComment" + "(" + data.commentID + ")";

                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "commentID",
                                fields: {
                                    commentID: {editable: false, defaultValue: 0, type: "number"},
                                    commentText: {editable: true, type: "string", validation: {required: false}},
                                    fileName: {editable: true, type: "string", validation: {required: false}},
                                    timeStamp: {editable: true, type: "date", validation: {required: true}},
                                    messageID: {type: "number"},
                                    userKey: {type: "number"},
                                    replyCommentID: {editable: false, defaultValue: 0, type: "number"}

                                }
                            }
                        },
                        sort: [{field: "messageID", dir: "desc"},{field: "commentID", dir: "asc"}],
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            console.log('   Comment21View:messageCommentDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingChatWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            console.log('    Comment21View:messageCommentDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingChatWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            console.log('   Comment21View:messageCommentDataSource:error');
                            kendo.ui.progress(self.$("#loadingChatWidget"), false);

                            //self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        filter: {field: "commentID", operator: "eq", value: this.commentID}
                    });


                this.$('#comment').html(this.commentText);
                this.$('#commentUserName').html(this.firstName + " " + this.lastName);
                this.$('#commentTimeStamp').html(this.timeStamp);

                if (this.replyCommentID === null) {
                    this.$('#replyToComment').show();
                    this.$('#boxComment').removeClass('comment-text2');

                } else {
                    this.$('#replyToComment').hide();
                    this.$('#boxComment').addClass('comment-text2');
                    //this.$('boxComment').css('background','#f4f4f4;');
                    //this.$('box-footer').css('background','#f4f4f4;');

                }

                if (this.fileName !== null) {
                    //this.$('#image').attr('src', "../../" + App.config.PortalName + "/CommentImages/" + this.fileName );
                    this.$('#commentImage').attr('src', App.config.PortalFiles + "/CommentImages/" + this.fileName);
                    this.$('#link').attr('href', App.config.PortalFiles + "/CommentImages/" + this.fileName);

                } else {
                    this.$('#link').css('display', 'none');
                }

                var url = App.config.PortalFiles + "/UserImages/" + self.userKey + ".jpg";
                $.ajax({
                    url: url,
                    type: "GET",
                    crossDomain: true,
                    success: function (response) {
                        console.log('Comment21View:user image found:done');
                        self.$('#userImage').attr('src', App.config.PortalFiles + "/UserImages/" + self.userKey + ".jpg" );
                    },
                    error: function (xhr, status) {
                        console.log('Comment21View:user image not found:fail');
                    }
                });

            },
            deleteCommentClicked: function(e){
                console.log('Comment21View:deleteCommentClicked');

                var self = this;

                if (confirm("Are you sure you want to delete?")) {
                    this.messageCommentDataSource.fetch(function () {
                        var data = this.data();
                        var count = data.length;

                        if (count > 0) {

                            var record = self.messageCommentDataSource.at(0);

                            self.messageCommentDataSource.remove(record);

                            this.sync();

                        }
                    });
                }

            },
            replyToCommentClicked: function(e){
                console.log('Comment21View:replyToCommentClicked');

                var self = this;

                App.modal.show(new Comment21ReplyView({
                    commentID: self.commentID,
                    messageID: self.messageID,
                    name: self.firstName + " " + self.lastName,
                    addUserImage: self.addUserImage
                }));
            },

            onResize: function () {
//                //console.log('Comment21View:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('Comment21View:resize');

            },
            remove: function () {
                //console.log('-------- Comment21View:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#commentRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
