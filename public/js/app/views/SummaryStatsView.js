define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/summaryStats','kendo/kendo.data'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template:template,
            initialize: function(options){
                //console.log('SummaryStatsView:initialize');
                // This view is shown on the client profile page under "This Month" stats listed for the chosen client

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Client"};
                }

                this.clientID = parseInt(App.model.get('selectedClientId'),0); //2189;

                this.dateFilter = App.model.get('selectedDateFilter');
                var app = App;
                if (this.options.type === "Client") {
                    this.startDate = new Date(moment().startOf('month'));
                    this.endDate = new Date(moment().endOf('month'));
                    //this.endDate = this.endDate.addHours(23);
                    //this.endDate = this.endDate.addMinutes(59);
                } else {
                    this.startDate = new Date(app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter)); //"2016-10-01T00:00:00+0000"
                    this.endDate = new Date(app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter)); //"2016-10-31T23:59:59+0000"
                    this.endDate = this.endDate.addHours(23);
                    this.endDate = this.endDate.addMinutes(59);
                }

                this.year = this.startDate.getFullYear();

                var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
                this.month = months[this.startDate.getMonth()];

            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- SummaryStatsView:onShow ----------');
                
                // UIUX - Events beyond css and view classes
                var self = this;

                if (this.options) {
                    if (this.options.height) {
                        this.$('.tile-content').height(this.options.height);
                    }
                }

                this.populateWidget();
            },
            populateWidget: function () {
//                //console.log('SummaryStatsView:createChart');

                var self = this;
                var app = App;

                this.progressClientDataSource = new kendo.data.DataSource(
                    {
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                            },
                            complete: function (e) {
                                console.log('SummaryStatsView:progressClientDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'SummaryStatsView:progressClientDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            //    "clientID":257,"schedulerID":0,"divisionID":null,"clientTimeZoneID":1,
                            //    "firstName":"Melanie","lastName":"Dunn","customerName":null,"companyClient":false,
                            //    "companyID":7,"planID":22,"hotList":1,"adminSchedulerID":0,"typeOfService":"1 - Scheduling",
                            //    "industry":"--- NONE ---","division":"","title":"","referredBy":"",
                            //    "lfdAuthorization":"N/A","dsuNumber":"","product":"--- NONE ---",
                            //    "distributionChannel":"--- NONE ---","states":"N CA - Reno","lengthInTerritory":"--- NONE ---",
                            //    "status":"6 - Termed","emailAddress":"melanie.dunn@genworth.com",
                            //    "workPhone":"(206) 979-1441","workExtension":"","address":"1550 Filbert Street #10",
                            //    "city":"San Fransico","state":"CA","zipCode":"94123","homePhone":"","mobilePhone":"",
                            //    "assistantInternal":"Kristi Mayhew","assistantPhone":"(804) 484-7002","assistantExtension":"",
                            //    "assistantEmail":"","realTimeAccess":false,"realTimeUser":"","realTimePassword":"",
                            //    "otherUsernames":"vOtherUserNames","esaAnniversary":"2010-06-14T00:00:00",
                            //    "termDate":"2011-01-07T00:00:00","sbCount":true,"additionalInformation":"","sysCrmUserName":"",
                            //    "sysCrmPassword":"","sysCrmNotes":"","sysEmailUserName":"","sysEmailPassword":"",
                            //    "sysEmailNotes":"","sysCalendar":"","sysCalendarUserName":"","sysCalendarPassword":"",
                            //    "sysCalendarNotes":"","taskTargetLow":15,"taskTargetHigh":20,"mtgTargetLowDec":3,"mtgTargetHigh":5
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                planHours: {type: "number"},
                                schedulingHours: {type: "number"},
                                taskTargetLow: {type: "number"},
                                taskTargetHigh: {type: "number"},
                                mtgTargetLowDec: {type: "number"},
                                mtgTargetHigh: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   SummaryStatsView:requestStart');

                        kendo.ui.progress(self.$("#loadingSummaryStats"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    SummaryStatsView:requestEnd');
                        kendo.ui.progress(self.$("#loadingSummaryStats"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryStatsView:error');
                        kendo.ui.progress(self.$("#loadingSummaryStats"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryStatsView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: [{field: "clientID", operator: "eq", value: self.clientID}]

                });

                this.progressDataSource = new kendo.data.DataSource(
                    {
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressData";
                            },
                            complete: function (e) {
                                console.log('SummaryStatsView:progressDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'SummaryStatsView:progressDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "entryID",

//"entryID":446,"clientID":257,"timeStamp":"2010-10-23T18:16:28.853","workDate":"2010-07-06T00:00:00",
//"schedulerID":0,"schedulerTime":1.25,"schedulerCalls":17,"schedulerMeetings":2,"support1ID":0,
//"support1Time":0.0,"support1Calls":0,"support1Meetings":0,"support2ID":0,"support2Time":0.0,
//"support2Calls":0,"support2Meetings":0,"compID":0,"compTime":0.0,"compCalls":0,"compMeetings":0,
//"adminTime":0.0
                            fields: {
                                entryID: {editable: false, defaultValue: 0, type: "number"},
                                workDate: {editable: true, type: "date", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: false}},
                                schedulerTime: {type: "number"},
                                support1Time: {type: "number"},
                                adminTime: {type: "number"},
                                schedulerCalls: {type: "number"},
                                support1Calls: {type: "number"},
                                schedulerMeetings: {type: "number"},
                                support1Meetings: {type: "number"},
                                compTime: {type: "number"},
                                compCalls: {type: "number"},
                                compMeetings: {type: "number"},
                                referralTime: {type: "number"},
                                referralCalls: {type: "number"},
                                referralMeetings: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   SummaryStatsView:progressDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingSummaryStats"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    SummaryStatsView:progressDataSource:requestEnd');
                        kendo.ui.progress(self.$("#loadingSummaryStats"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryStatsView:progressDataSource:error');
                        kendo.ui.progress(self.$("#loadingSummaryStats"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {
                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryStatsView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: [{field: "clientID", operator: "eq", value: self.clientID},
                        {field: "workDate", operator: "gte", value: self.startDate},
                        {field: "workDate", operator: "lte", value: self.endDate}]

                });

                this.progressDataSource.fetch(function () {

                    var progressData = this.data();

                    var timeActual = 0;
                    var tasksActual = 0;
                    var meetingsActual = 0;

                    var schedulerTime = 0;
                    var schedulerTasks = 0;
                    var schedulerMeetings = 0;

                    var adminTime = 0;

                    var compTime = 0;
                    var compMeetings = 0;
                    var compCalls = 0;

                    var referralTime = 0;
                    var referralMeetings = 0;
                    var referralCalls = 0;

                    $.each(progressData, function (index) {
                        var record = progressData[index];
                        timeActual +=  record.schedulerTime + record.support1Time + record.adminTime;
                        tasksActual += record.schedulerCalls + record.support1Calls;
                        meetingsActual += record.schedulerMeetings + record.support1Meetings;
                        
                        compTime += record.compTime;
                        compMeetings += record.compMeetings;
                        compCalls += record.compCalls;

                        referralTime += record.referralTime;
                        referralMeetings += record.referralMeetings;
                        referralCalls += record.referralCalls;

                        schedulerTime += record.schedulerTime;
                        schedulerTasks += record.schedulerCalls;
                        schedulerMeetings += record.schedulerMeetings;
                        adminTime+= record.adminTime;

                    });

                    self.progressClientDataSource.fetch(function () {

                        var progressClientRecord = this.data()[0];
                        var mtgTargetLowDec = progressClientRecord.mtgTargetLowDec;
                        var mtgTargetHigh = progressClientRecord.mtgTargetHigh;
                        var taskTargetLow = progressClientRecord.taskTargetLow;
                        var taskTargetHigh = progressClientRecord.taskTargetHigh;

                        // If scheduling hours is 0, it's an hourly plan. Planned hours should then be actual hours (NN - 8/29/18)
                        timeActual = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeActual);
                        var planHours = progressClientRecord.planHours;
                        if (progressClientRecord.schedulingHours === 0) {
                            planHours = timeActual;
                        }

                        var timeRemaining = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(planHours -timeActual);
                        var tasksRemaining = app.Utilities.numberUtilities.getFormattedNumberWithRounding(taskTargetLow*planHours - tasksActual);
                        var meetingsRemaining = app.Utilities.numberUtilities.getFormattedNumberWithRounding(Math.max(mtgTargetLowDec*planHours - meetingsActual,0));

                        schedulerTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(schedulerTime);
                        schedulerMeetings = app.Utilities.numberUtilities.getFormattedNumberWithRounding(schedulerMeetings);
                        schedulerTasks = app.Utilities.numberUtilities.getFormattedNumberWithRounding(schedulerTasks);

                        adminTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(adminTime);
                        meetingsActual = app.Utilities.numberUtilities.getFormattedNumberWithRounding(meetingsActual);
                        tasksActual = app.Utilities.numberUtilities.getFormattedNumberWithRounding(tasksActual);

                        var timeActualPlusComp =timeActual + compTime;
                        var tasksActualPlusComp = tasksActual + compCalls;
                        var meetingsActualPlusComp = meetingsActual + compMeetings;

                        // Populate widget with data
                        self.$('#monthAndYear').html(self.month + " " + self.year);

                        // Target
                        self.$('#targetTime').html(planHours);
                        self.$('#targetTasks').html(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(taskTargetLow * planHours));
                        self.$('#targetMeetings').html(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(mtgTargetLowDec * planHours));

                        // Scheduler
                        self.$('#schedulerTime').html(schedulerTime);
                        self.$('#schedulerTasks').html(schedulerTasks);
                        self.$('#schedulerMeetings').html(schedulerMeetings);

                        // Admin
                        self.$('#adminTime').html(adminTime);
                        self.$('#adminTasks').html("-");
                        self.$('#adminMeetings').html("-");

                        // Actual
                        self.$('#actualTime').html(timeActual);
                        self.$('#actualTasks').html(tasksActual);
                        self.$('#actualMeetings').html(meetingsActual);

                        // Remaining
                        self.$('#remainingTime').html(timeRemaining);
                        self.$('#remainingTasks').html(tasksRemaining);
                        self.$('#remainingMeetings').html(meetingsRemaining);

                    });
                });

            },
            displayNoDataOverlay: function () {
                //console.log('ProjectGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProjectGridView:hideNodDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('.tile-content').css('display', 'block');
            },
            onResize: function () {
//                //console.log('SummaryStatsView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('SummaryStatsView:resize');

            },
            remove: function () {
                //console.log('---------- SummaryStatsView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off
                this.$('.tile-content').off();
                this.$('.tile-more-info').off();
                this.$('.tile-hover-tip').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });