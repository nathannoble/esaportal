define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalTimeCardDetailTable',
    'views/PortalTimeCardDetailGridView','views/SubHeaderView'],
    function (App, Backbone, Marionette, $, template, PortalTimeCardDetailGridView,SubHeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                timeCardRegion: "#time-card-region"
            },
            initialize: function (options) {

                //console.log('PortalTimeCardDetailTableView:initialize');

                _.bindAll(this);

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedStatusFilterChanged, this.onSelectedStatusFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedEmpStatusFilterChanged, this.onSelectedEmpStatusFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedTeamLeaderIdChanged, this.onSelectedTeamLeaderIdChanged);

                this.options = options;

                console.log('PortalTimeCardDetailTableView:initialize:options:' + JSON.stringify(this.options));

                if (this.options === null || this.options === undefined) {
                    this.options = {
                        ignoreDateChange: false
                    };
                }


            },
            onRender: function () {

                //console.log('PortalTimeCardDetailTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalTimeCardDetailTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                this.userType = App.model.get('userType');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    if (this.userType === 2) {
                        this.hideStatsTimecard = App.model.get('hideStatsTimecard');
                        if (this.hideStatsTimecard) {
                            console.log('EmployeeProfileLayoutView:onShow:you do not have permissions to see the stats timecard.');
                            App.model.set('callbackErrorMessage',3);
                            //alert('You do not have permissions to see the stats timecard.');
                            //history.back();
                        }
                    }

                    // set date range
                    if (this.options.ignoreDateChange !== true) {
                        var start = moment();
                        var end = moment();
                        this.dateFilter = start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY');
                        App.model.set('selectedDateFilter', this.dateFilter);
                    }

                    this.subHeaderRegion.show(new SubHeaderView({
                        majorTitle: "Stats Time Card",
                        minorTitle: "", //'Stats time card data for the current date range',
                        page: "Stats Time Card",
                        showDateFilter: true,
                        showStatusFilter: true,
                        showEmpStatusFilter: true,
                        showClientReportFilter: false,
                        showViewsFilter: false,
                        showCompanyFilter: false,
                        showClientFilter: false,
                        showTeamLeaderFilter:true,
                        showPayrollProcessingDate:false
                    }));
                    this.timeCardRegion.show(new PortalTimeCardDetailGridView());
                }


            },
            onSelectedStatusFilterChanged: function () {
                this.timeCardRegion.reset();
                this.timeCardRegion.show(new PortalTimeCardDetailGridView());
            },
            onSelectedEmpStatusFilterChanged: function () {
                this.timeCardRegion.reset();
                this.timeCardRegion.show(new PortalTimeCardDetailGridView());
            },
            onSelectedTeamLeaderIdChanged: function () {
                this.timeCardRegion.reset();
                this.timeCardRegion.show(new PortalTimeCardDetailGridView());
            },
            onSelectedDateFilterChanged: function () {
                this.timeCardRegion.reset();
                this.timeCardRegion.show(new PortalTimeCardDetailGridView());
            },
            resize: function () {
                //console.log('PortalTimeCardDetailTableView:resize');

                if (this.timeCardRegion.currentView)
                    this.timeCardRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalTimeCardDetailTableView:remove --------");

                //this.saveButtonClicked();

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });