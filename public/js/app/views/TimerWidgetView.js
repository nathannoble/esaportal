define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/timerWidget', 'views/TimerOptionsView',
        'kendo/kendo.data', 'kendo/kendo.combobox'],

    function (App, Backbone, Marionette, $, template, TimerOptionsView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #startButton': 'startButtonClicked',
                'click #interruptButton': 'interruptButtonClicked',
                'click #stopButton': 'stopButtonClicked',
                'click #manualButton': 'manualButtonClicked',
                'click #resumeButton': 'resumeButtonClicked'
            },
            initialize: function (options) {
                console.log('TimerWidgetView:initialize');

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Client"};
                }

                this.myEmployeeId = App.model.get('myEmployeeId');
                if (this.myEmployeeId !== null && this.myEmployeeId !== undefined) {
                    this.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);
                }
                this.timerStatus = App.model.get('timerStatus');

                this.timerLogDataSource = new kendo.data.DataSource(
                    {
                        autoSync: true,
                        //autoBind: false,
                        //batch: false,
                        //pageSize: 10,
                        //serverPaging: true,
                        //serverSorting: true,
                        //serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalTimerLog";
                                },
                                complete: function (e) {
                                    console.log('TimeWidgetView:timerLogDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeWidgetView:timerLogDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            create: {
                                url: function (data) {
                                    //console.log('TimerWidgetView timerLogDataSourceClient:create');
                                    return App.config.DataServiceURL + "/odata/PortalTimerLog";
                                },
                                complete: function (e) {
                                    console.log('TimeWidgetView:timerLogDataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeWidgetView:timerLogDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "logID",
                                fields: {
                                    logID: {editable: false, defaultValue: 0, type: "number"},
                                    logDate: {editable: true, type: "date", validation: {required: false}},
                                    timerID: {editable: true, defaultValue: 0, type: "number"},
                                    logText: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        requestStart: function () {
                            //console.log('TimerWidgetView:timerLogDataSource:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log(' TimerWidgetView:timerLogDataSource:requestEnd');
                            //kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('TimerWidgetView:timerLogDataSource:error');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                            //App.modal.show(new ErrorView());

                        }
                    });

                console.log('TimerWidgetView:initialize:timerStatus:' + this.timerStatus);

                this.currentTimerID = App.model.get('currentTimerID');
                this.intTimerID = App.model.get('intTimerID');
                this.serviceID = App.model.get('currentTimerServiceID');
                this.clientID = App.model.get('currentTimerClientID');
                this.companyID = App.model.get('selectedCompanyId');
                this.timeZoneID = App.model.get('timeZoneId');
                this.timerManual = App.model.get('timerManual');

                var today = new Date();
                this.year = today.getFullYear();
                this.month = today.getMonth() + 1;
                this.day = today.getDate();

                // Default service recording values
                this.toStats = true;
                this.recordTasksMtgs = true;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                kendo.culture("en-US");

            },
            onTimerStatusChanged: function () {

                var self = this;

                //self.timerStatus = App.model.get('timerStatus');

                console.log('TimerWidgetView:onTimerStatusChanged:timerStatus:' + self.timerStatus);

                console.log('TimerWidgetView:onTimerStatusChanged:serviceID:' + self.serviceID);
                var serviceList = self.$("#service").kendoDropDownList({
                    dataValueField: "serviceID",
                    dataTextField: "serviceItem",
                    dataSource: self.serviceRecords,
                    value: self.serviceID,
                    change: self.onServiceIdChanged
                });

                self.onServiceIdChanged(serviceList);

                var clientDDL = self.$("#timerClientList").data("kendoDropDownList");
                var serviceDDL = self.$("#service").data("kendoDropDownList");

                if (self.timerStatus === "Open") {
                    self.$("#startGroup").hide();
                    self.$("#manualGroup").hide();
                    self.$("#interruptGroup").show();
                    self.$("#stopGroup").show();
                    self.$("#resumeGroup").hide();
                    if (clientDDL !== undefined) {
                        clientDDL.readonly();
                        serviceDDL.readonly();
                    }
                } else if (this.timerStatus === "INTERRUPTED") {
                    self.$("#startGroup").hide();
                    self.$("#manualGroup").hide();
                    self.$("#interruptGroup").hide();
                    self.$("#stopGroup").hide();
                    self.$("#resumeGroup").show();
                    if (clientDDL !== undefined) {
                        clientDDL.readonly();
                        serviceDDL.readonly();
                    }
                } else { //Stopped, Manual, nothing
                    self.$("#interruptGroup").hide();
                    self.$("#startGroup").show();
                    if (self.timerManual === true) {
                        self.$("#manualGroup").show();
                    } else {
                        self.$("#manualGroup").hide();
                    }
                    self.$("#resumeGroup").hide();
                    self.$("#stopGroup").hide();

                    if (self.timerStatus === "Finish") {
                        self.$('#startButton').addClass('disabled');
                        self.$('#manualButton').addClass('disabled');
                    }
                }
                self.$("#timerButtons").show();


            },
            getData: function () {

                var self = this;
                var app = App;

                this.serviceDataSource = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalServiceItem";
                                },
                                complete: function (e) {
                                    console.log('TimeWidgetView:serviceDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeWidgetView:serviceDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "serviceID",
                                fields: {
                                    serviceID: {editable: false, defaultValue: 0, type: "number"},
                                    ordering: {editable: false, defaultValue: 0, type: "number"},
                                    serviceItem: {editable: true, type: "string", validation: {required: false}},
                                    recordTasksMtgs: {editable: true, type: "boolean"},
                                    toStats: {editable: true, type: "boolean"},
                                    timerStatus: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //self.writeLog('TimerWidgetView:serviceDataSource:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerWidgetView:serviceDataSource:requestEnd');
                            //kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerWidgetView:serviceDataSource:error');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                            //App.modal.show(new ErrorView());

                        },
                        sort: {field: "ordering", dir: "asc"}
                    });

                this.employeeDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('TimerWidgetView:employeeDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimerWidgetView:employeeDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    title: {editable: true, type: "string", validation: {required: false}},
                                    userKey: {editable: true, type: "number"},
                                    teamLeaderID: {editable: true, type: "number"},
                                    timeZoneID: {editable: true, type: "number"},
                                    eeTimeZone: {editable: true, type: "number"},
                                    eeTimeZoneOffset: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //self.writeLog('TimerWidgetView:employeeDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerWidgetView:employeeDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerWidgetView:employeeDataSource:error');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);

                        },
                        filter: [{field: "employeeID", operator: "eq", value: self.myEmployeeId}]

                    });

                this.timerTotalDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalTimerTotalTime";
                                },
                                complete: function (e) {
                                    console.log('TimerWidgetView:timerTotalDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'MessageWidgetView:timerTotalDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "rowID",
                                fields: {
                                    rowID: {editable: false, defaultValue: 0, type: "number"},
                                    employeeID: {editable: false, defaultValue: 0, type: "number"},
                                    workDate: {editable: true, type: "date", validation: {required: false}},
                                    totalTimeWorked: {editable: false, defaultValue: 0, type: "number"},
                                    workYear: {editable: false, defaultValue: 0, type: "number"},
                                    workMonth: {editable: false, defaultValue: 0, type: "number"},
                                    workDay: {editable: false, defaultValue: 0, type: "number"}
                                }
                            }
                        },
                        requestStart: function () {
                            //self.writeLog('TimerWidgetView:timerTotalDataSource:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerWidgetView:timerTotalDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerWidgetView:timerTotalDataSource:error');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                            //App.modal.show(new ErrorView());

                        },
                        filter: [
                            {field: "workYear", operator: "eq", value: self.year},
                            {field: "workMonth", operator: "eq", value: self.month},
                            {field: "workDay", operator: "eq", value: self.day},
                            {field: "employeeID", operator: "eq", value: self.myEmployeeId}
                        ]
                    });

                this.timerDataSource = new kendo.data.DataSource(
                    {
                        batch: false,
                        pageSize: 10,
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        sync: function(e) {
                            //self.writeLog('TimerWidgetView:timerDataSource:sync');
                            //self.writeLog('TimerWidgetView:timerDataSource:sync:_data.length:' + e.sender._data.length);
                            if (e.sender._data.length === 1) {
                                //self.writeLog('TimerWidgetView:timerDataSource:sync:_data:' + JSON.stringify(e.sender._data[0]));
                                // This is after a sync of an add
                                if (e.sender._data[0].stopTime <= (new Date(0)).addDays(1)) {
                                    //self.writeLog('TimerWidgetView:addTimerRecord:add:sync done');
                                    self.timerStatus = "Open";
                                }
                            }
                        },
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalTimer",
                                complete: function (e) {
                                    console.log('TimerWidgetView:timerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'MessageWidgetView:timerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //self.writeLog('TimerWidgetView:timerDataSource:update:url');
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.timerID + ")";
                                },
                                complete: function (e) {
                                    //console.log('TimerWidgetView:timerDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                    //self.writeLog('TimerWidgetView:timerDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                },
                                error: function (e) {
                                    var message = 'TimerWidgetView:timerDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    //console.log(message);
                                    //self.writeLog(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //self.writeLog('TimerWidgetView:timerDataSource:create:url');
                                    return App.config.DataServiceURL + "/odata/PortalTimer";
                                },
                                complete: function (e) {
                                    //self.writeLog('TimerWidgetView:timerDataSource:create:complete');
                                    if (e.responseJSON !== undefined) {
                                        //self.writeLog('TimerWidgetView:timerDataSource:create:complete:' + JSON.stringify(e.responseJSON));
                                        if (e.responseJSON.value[0]) {
                                            var newTimerID = e.responseJSON.value[0].timerID;
                                            //self.writeLog('TimerWidgetView:timerDataSource:create:complete:' + newTimerID);

                                            var currentNewStartTime = e.responseJSON.value[0].startTime;
                                            if (!self.lastNewStartTime) {
                                                self.lastNewStartTime = 0;
                                            }

                                            var start2 = moment(currentNewStartTime).clone();
                                            var start = moment(self.lastNewStartTime).clone();

                                            var msDiff = parseFloat(start.diff(start2));

                                            // A duplicate has been created
                                            if (self.currentTimerID !== null && (newTimerID < self.currentTimerID || Math.abs(msDiff) < 500)) {
                                                self.writeLog('TimerWidgetView:timerDataSource:create:complete:duplicate:msDiff:' + msDiff);

                                                //Delete duplicate
                                                self.deleteDuplicate(newTimerID);
                                            } else {
                                                App.model.set('currentTimerServiceID', self.serviceID);
                                                App.model.set('currentTimerClientID', self.clientID);
                                                App.model.set('selectedCompanyId', self.companyID);
                                                self.lastNewStartTime = e.responseJSON.value[0].startTime;
                                                self.currentTimerID = newTimerID;
                                                App.model.set('currentTimerID', self.currentTimerID);
                                                self.updateTimerRecordWithTimerId();
                                                self.onTimerStatusChanged();
                                            }
                                        } else {
                                            var message = 'TimerWidgetView:timerDataSource:create:complete:no record';
                                            console.log(message);
                                            self.writeLog(message);
                                        }
                                    } else {
                                        var message2 = 'TimerWidgetView:timerDataSource:create:complete:no record 2';
                                        console.log(message2);
                                        self.writeLog(message2);
                                    }
                                },
                                error: function (e) {
                                    self.writeLog('TimerWidgetView:timerDataSource:create:error');
                                    console.log('TimerWidgetView:timerDataSource:create:error:' + JSON.stringify(e.responseJSON));
                                    self.writeLog('TimerWidgetView:timerDataSource:create:error:' + JSON.stringify(e.responseJSON));
                                }
                            },
                            destroy: {
                                url: function (data) {
                                    self.writeLog('TimerWidgetView:timerDataSource:destroy:timerID:' + data.timerID);
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.timerID + ")";
                                },
                                complete: function (e) {
                                    //console.log('TimerWidgetView:timerDataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'MessageWidgetView:timerDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    idTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    entryType: {editable: true, type: "string", validation: {required: false}},
                                    employeeID: {editable: true, type: "number"},
                                    clientID: {editable: true, type: "number"},
                                    serviceID: {editable: true, type: "number"},
                                    workDate: {editable: true, type: "string", validation: {required: false}},
                                    startTime: {
                                        editable: true,
                                        type: "date",
                                        format: "{0:s}"
                                    },
                                    stopTime: {editable: true, type: "date", validation: {required: false}},
                                    timeWorked: {editable: true, type: "number"},
                                    timeWorkedText: {editable: true, type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    intTimerID: {editable: true, type: "number"},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    editTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    editEmployeeID: {editable: true, type: "number"},
                                    tasksCalls: {editable: true, type: "number"},
                                    tasksEmails: {editable: true, type: "number"},
                                    tasksVoiceMails: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "timerID", dir: "asc"},
                        requestStart: function (e) {
                            //self.writeLog('TimerWidgetView:timerDataSource:request start:');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function (e) {
                            //self.writeLog('TimerWidgetView:timerDataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);
                        },
                        error: function (e) {
                            self.writeLog('TimerWidgetView:timerDataSource:error');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerWidgetView:timerDataSource:change');

                        }
                    });

            },
            onRender: function () {
                console.log('TimerWidgetView:onRender');
                //this.writeLog('TimerWidgetView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('---------- TimerWidgetView:onShow ---------- ');
                //this.writeLog('---------- TimerWidgetView:onShow ---------- ');
                this.$("#timerButtons").hide();
                this.getData();
                this.populateWidget();

            },
            deleteDuplicate: function (timerID) {
                var self = this;

                console.log('TimerWidgetView:deleteDuplicate:timerID:' + timerID);
                self.writeLog('TimerWidgetView:deleteDuplicate:timerID:' + timerID);

                self.timerDataSource.filter({field: "timerID", operator: "eq", value: timerID});
                self.timerDataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count > 0) {
                        var record = self.timerDataSource.at(0);
                        self.timerDataSource.remove(record);
                        this.sync();
                        self.writeLog('TimerWidgetView:deleteDuplicate:timerID:' + timerID + ' done');
                    } else {
                        self.writeLog('TimerWidgetView:deleteDuplicate:timerID:' + timerID + ' not found.');
                    }
                });

            },
            populateWidget: function () {

                var self = this;
                //self.writeLog('TimerWidgetView:populateWidget');

                if (self.timerManual === false) {
                    self.$("#manualGroup").hide();
                }

                if (this.myEmployeeId !== null) {
                    this.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);

                    self.employeeDataSource.fetch(function () {

                        var employees = this.data();
                        var showBreakServiceItem = false;
                        var state = "";

                        if (employees.length > 0) {
                            state = employees[0].homeState;
                            //console.log('TimerWidgetView:populateWidget:homeState:' + state);
                        }

                        self.serviceDataSource.fetch(function () {

                            self.serviceRecords = this.data();

                            // 9/25/20 - only show "Break" for employees in NV and CA
                            //if (state !== "NV" && state !== "CA") {
                            //    var index = self.serviceRecords.indexOf("Break");
                            //    self.serviceRecords.splice(index,1);
                            //}

                            self.onTimerStatusChanged();

                        });
                    });

                    this.timerTotalDataSource.fetch(function (data) {

                        self.$("#timeWorked").kendoMaskedTextBox();
                        self.$("#timeWorked").data("kendoMaskedTextBox").value("N/A");
                        if (this.data().length > 0) {

                            var timerRecord = this.data()[0];

                            if (timerRecord.totalTimeWorked === null) {
                                self.$("#timeWorked").data("kendoMaskedTextBox").value(0);
                            } else {
                                self.$("#timeWorked").data("kendoMaskedTextBox").value(timerRecord.totalTimeWorked);
                            }

                        }
                    });
                }
            },
            onServiceIdChanged: function (e) {

                var self = this;

                this.myEmployeeId = App.model.get('myEmployeeId');
                if (this.myEmployeeId !== null && this.myEmployeeId !== undefined) {
                    this.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);
                }

                var serviceID = null;
                if (this.serviceID !== null) {
                    serviceID = parseInt(this.serviceID, 0);
                }
                if (this._selectedValue !== undefined && this._selectedValue !== null) {
                    serviceID = parseInt(this._selectedValue, 0);
                }
                console.log('TimerWidgetView:onServiceIdChanged:serviceID:' + serviceID);
                console.log('TimerWidgetView:onServiceIdChanged:myEmployeeId:' + this.myEmployeeId);

                var filter = {
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: [{field: "clientID", operator: "eq", value: 1793}]
                        },
                        {field: "status", operator: "eq", value: '4 - Active'}
                    ]
                };

                if (serviceID === 1001 || serviceID === 1013 || serviceID === null) {
                    // Scheduling and Corporate Compliance Training only show assigned clients
                    filter.filters[0].filters.push({field: "schedulerID", operator: "eq", value: this.myEmployeeId});
                } else if (serviceID === 1011) {
                    // Support Scheduling, show non-assigned clients only
                    filter.filters[0].filters.push({field: "schedulerID", operator: "neq", value: this.myEmployeeId});
                }
                else {
                    // show all
                    filter.filters[0].filters.push({field: "schedulerID", operator: "neq", value: -9999});
                }

                self.clientNameRecords = App.model.get('clientNameRecords');

                var clientID = App.model.get('currentTimerClientID');

                if (self.clientNameRecords.length === 0) {
                    var progressClientDS = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            serverFiltering: true,
                            serverSorting: true,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                    },
                                    complete: function (e) {
                                        console.log('TimeWidgetView:progressClientDS:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'TimeWidgetView:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientID",
                                    fields: {
                                        clientID: {editable: false, defaultValue: 0, type: "number"},
                                        schedulerID: {editable: false, defaultValue: 0, type: "number"},
                                        //schedulerName: {editable: true, type: "string", validation: {required: false}},
                                        companyID: {editable: false, defaultValue: 0, type: "number"},
                                        //clientName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        clientCompany: {editable: true, type: "string", validation: {required: false}},
                                        taskTracking: {editable: true, type: "boolean"}
                                    }
                                }
                            },
                            requestStart: function () {
                                //console.log('TimerWidgetView:progressClientDS:requestStart');

                                kendo.ui.progress($("#loadingTimerWidget"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('TimerWidgetView:progressClientDS:requestEnd');
                                kendo.ui.progress($("#loadingTimerWidget"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                // console.log('TimerWidgetView:progressClientDS:error');
                                kendo.ui.progress($("#loadingTimerWidget"), false);

                            },
                            sort: [
                                {field: "lastName", dir: "asc"},
                                {field: "firstName", dir: "asc"},
                                {field: "clientCompany", dir: "asc"}
                            ],
                            filter: [{field: "status", operator: "eq", value: "4 - Active"}]

                        });

                    //console.log('TimerWidgetView:onServiceIdChanged:filters:' + JSON.stringify(filter));

                    //progressClientDS.filter(filter);
                    kendo.ui.progress($("#loadingTimerWidget"), true);
                    progressClientDS.fetch(function (data) {

                        self.clientNameRecords = this.data();

                        var modifiedRecords = [];
                        $.each(self.clientNameRecords, function (index, value) {

                            var record = value;
                            var newRecord = {
                                // From ProgressClient
                                clientID: record.clientID,
                                companyID: record.companyID,
                                name: record.lastName + ", " + record.firstName + ": " + record.clientCompany,
                                taskTracking: record.taskTracking
                            };

                            if (serviceID === 1001 || serviceID === 1013 || serviceID === null) {
                                // Scheduling and Corporate Compliance Training only show assigned clients
                                //filter.filters[0].filters.push({field: "schedulerID", operator: "eq", value: this.myEmployeeId});
                                if (record.schedulerID === self.myEmployeeId) {
                                    modifiedRecords.push(newRecord);
                                }

                            } else if (serviceID === 1011) {
                                // Support Scheduling, show non-assigned clients only
                                //filter.filters[0].filters.push({field: "schedulerID", operator: "neq", value: this.myEmployeeId});
                                if (record.schedulerID !== self.myEmployeeId) {
                                    modifiedRecords.push(newRecord);
                                }
                            }
                            else {
                                // show all
                                //filter.filters[0].filters.push({field: "schedulerID", operator: "neq", value: -9999});
                                modifiedRecords.push(newRecord);
                            }

                        });

                        kendo.ui.progress($("#loadingTimerWidget"), false);

                        //var clientID = App.model.get('currentTimerClientID');

                        //console.log('TimerWidgetView:onServiceIdChanged:clientID:' +  clientID);
                        $("#timerClientList"). kendoDropDownList({
                            dataValueField: "clientID",
                            dataTextField: "name",
                            dataSource: modifiedRecords,
                            value: clientID
                        });

                        App.model.set('clientNameRecords', self.clientNameRecords);

                    });
                } else {

                    // get client list from cache

                    var modifiedRecords = [];
                    $.each(self.clientNameRecords, function (index, value) {

                        var record = value;
                        var newRecord = {
                            // From ProgressClient
                            clientID: record.clientID,
                            companyID: record.companyID,
                            name: record.lastName + ", " + record.firstName + ": " + record.clientCompany,
                            taskTracking: record.taskTracking
                        };

                        if (serviceID === 1001 || serviceID === 1013 || serviceID === null) {
                            // Scheduling and Corporate Compliance Training only show assigned clients
                            //filter.filters[0].filters.push({field: "schedulerID", operator: "eq", value: this.myEmployeeId});
                            if (record.schedulerID === self.myEmployeeId) {
                                modifiedRecords.push(newRecord);
                            }

                        } else if (serviceID === 1011) {
                            // Support Scheduling, show non-assigned clients only
                            //filter.filters[0].filters.push({field: "schedulerID", operator: "neq", value: this.myEmployeeId});
                            if (record.schedulerID !== self.myEmployeeId) {
                                modifiedRecords.push(newRecord);
                            }
                        }
                        else {
                            // show all
                            //filter.filters[0].filters.push({field: "schedulerID", operator: "neq", value: -9999});
                            modifiedRecords.push(newRecord);
                        }

                    });

                    //var clientID = App.model.get('currentTimerClientID');

                    //console.log('TimerWidgetView:onServiceIdChanged:clientID:' +  clientID);
                    $("#timerClientList").kendoDropDownList({
                        dataValueField: "clientID",
                        dataTextField: "name",
                        dataSource: modifiedRecords,
                        value: clientID
                    });
                }


            },
            startButtonClicked: function (e) {
                var self = this;
                //self.writeLog('TimerWidgetView:startButtonClicked:Auto');
                console.log('TimerWidgetView:startButtonClicked:Auto');

                self.$('#startButton').addClass('disabled');
                self.$('#manualButton').addClass('disabled');
                if (navigator.onLine) {
                    self.doesConnectionExist("start");
                } else {
                    self.$('#startButton').removeClass('disabled');
                    self.$('#manualButton').removeClass('disabled');
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                }

            },
            interruptButtonClicked: function (e) {
                var self = this;
                //self.writeLog('TimerWidgetView:interruptButtonClicked');

                self.$('#interruptButton').addClass('disabled');
                self.$('#stopButton').addClass('disabled');
                if (navigator.onLine) {
                    self.doesConnectionExist("interrupt");
                } else {
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                    self.$('#interruptButton').removeClass('disabled');
                    self.$('#stopButton').removeClass('disabled');
                }

            },
            resumeButtonClicked: function (e) {
                var self = this;
                //self.writeLog('TimerWidgetView:resumeButtonClicked');

                self.$('#resumeButton').addClass('disabled');

                if (navigator.onLine) {
                    self.doesConnectionExist("resume");
                } else {
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                    self.$('#resumeButton').removeClass('disabled');
                }

            },
            stopButtonClicked: function (e) {
                var self = this;
                //self.writeLog('TimerWidgetView:stopButtonClicked');

                self.$('#stopButton').addClass('disabled');
                self.$('#interruptButton').addClass('disabled');
                //self.$('#startButton').addClass('disabled');
                //self.$('#manualButton').addClass('disabled');

                if (navigator.onLine) {
                    self.doesConnectionExist("stop");
                } else {
                    self.$('#stopButton').removeClass('disabled');
                    self.$('#interruptButton').removeClass('disabled');
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                }

            },
            manualButtonClicked: function (e) {
                var self = this;
                //self.writeLog('TimerWidgetView:manualButtonClicked');

                self.$('#manualButton').addClass('disabled');
                self.$('#startButton').addClass('disabled');

                if (navigator.onLine) {
                    self.doesConnectionExist("manual");
                } else {
                    self.$('#manualButton').removeClass('disabled');
                    self.$('#startButton').removeClass('disabled');
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                }

            },
            doesConnectionExist: function (type) {
                var self = this;
                console.log('TimerWidgetView:doesConnectionExist');

                $.ajax({
                    type: "GET",
                    contentType: "application/json",
                    url: App.config.DataServiceURL + "/odata/PortalValue",
                    success: function (result) {
                        console.log('TimerWidgetView:doesConnectionExist:result:' + result);
                        if (type === "stop") {
                            self.stopAction();
                        } else if (type === "manual") {
                            self.checkForOpenTimer("manual");
                        } else if (type === "resume") {
                            self.updateTimerRecord("resume");

                            self.timerStatus = "Open";
                            self.onTimerStatusChanged();

                        } else if (type === "interrupt") {
                            self.timerStatus = "INTERRUPTED";

                            self.updateTimerRecord("interrupt");
                        } else if (type === "start") {
                            self.intTimerID = null;
                            self.currentTimerID = null;

                            self.checkForOpenTimer("start");
                        }
                    },
                    error: function (result) {
                        if (type === "stop") {
                            self.$('#stopButton').removeClass('disabled');
                            self.$('#interruptButton').removeClass('disabled');
                        } else if (type === "manual") {
                            self.$('#manualButton').removeClass('disabled');
                            self.$('#startButton').removeClass('disabled');
                        } else if (type === "resume") {
                            self.$('#resumeButton').removeClass('disabled');
                        } else if (type === "interrupt") {
                            self.$('#interruptButton').removeClass('disabled');
                            self.$('#stopButton').removeClass('disabled');
                        } else if (type === "start") {
                            self.$('#startButton').removeClass('disabled');
                            self.$('#manualButton').removeClass('disabled');
                        }
                        alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                    }
                });
            },
            stopAction: function () {
                var self = this;
                console.log('TimerWidgetView:stopAction');

                var clientDDL = $("#timerClientList").data("kendoDropDownList");
                var clientDataItem = clientDDL.dataItem();
                if (clientDataItem !== undefined && clientDataItem !== null) {
                    self.clientID = clientDataItem.clientID;
                    self.clientName = clientDataItem.name;
                    self.companyID = clientDataItem.companyID;
                    self.taskTracking = clientDataItem.taskTracking;
                    //console.log('TimerWidgetView:stopButtonClicked:client record:' + JSON.stringify(clientDataItem));
                }

                var serviceDDL = $("#service").data("kendoDropDownList");
                var serviceDataItem = serviceDDL.dataItem();
                if (serviceDataItem !== undefined && serviceDataItem !== null) {
                    self.serviceID = serviceDataItem.serviceID;
                    self.serviceItem = serviceDataItem.serviceItem;
                }

                self.timerStatus = "Finish";

                App.model.set('timerStatus', self.timerStatus);

                self.updateTimerRecord("finish");
            },
            resetStatsDashboardCache: function () {

                console.log('TimerWidgetView:resetStatsDashboardCache');

                $.cookie('ESA:AppModel:mySumTotalTime', "");
                App.model.set('mySumTotalTime', null);

                $.cookie('ESA:AppModel:mySumTotalMeetings', "");
                App.model.set('mySumTotalMeetings', null);

                $.cookie('ESA:AppModel:mySumMeetingsTarget', "");
                App.model.set('mySumMeetingsTarget', null);

                $.cookie('ESA:AppModel:myMtgPercent', "");
                App.model.set('myMtgPercent', null);

                $.cookie('ESA:AppModel:teamSumTotalTime', "");
                App.model.set('teamSumTotalTime', null);

                $.cookie('ESA:AppModel:teamSumTotalMeetings', "");
                App.model.set('teamSumTotalMeetings', null);

                $.cookie('ESA:AppModel:teamSumMeetingsTarget', "");
                App.model.set('teamSumMeetingsTarget', null);

                $.cookie('ESA:AppModel:teamMtgPercent', "");
                App.model.set('teamMtgPercent', null);
            },
            checkForOpenTimer: function (callingButton) {
                var self = this;
                console.log('TimerWidgetView:checkForOpenTimer');

                this.restDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetMyOpenTimers/" + self.myEmployeeId;
                                    return url;
                                },
                                complete: function (e) {
                                    console.log('TimerView:restDataSource:update:complete');
                                 },
                                error: function (e) {
                                    var message = 'TimerView:restDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {

                            model: {
                                id: "intTimerID",
                                fields: {

                                    intTimerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    employeeID: {editable: false, type: "number"},
                                    serviceID: {editable: false, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    clientName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: true}},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: [{field: "employeeName", dir: "asc"}, {field: "workDate", dir: "asc"}],
                        requestStart: function (e) {
                            //self.writeLog('TimerWidgetView:OpenTimerRestDataSource:request start:');
                            //kendo.ui.progress(self.$("#subHeaderLoading"), true);
                        },
                        requestEnd: function (e) {
                            //self.writeLog('TimerWidgetView:OpenTimerRestDataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#subHeaderLoading"), false);
                        },
                        change: function (e) {
                            //self.writeLog('TimerWidgetView:OpenTimerRestDataSource:change:');
                        },
                        error: function (e) {
                            self.writeLog('TimerWidgetView:OpenTimerRestDataSource:error');
                            //kendo.ui.progress(self.$("#subHeaderLoading"), false);
                        }
                    });

                //kendo.ui.progress(self.$("#subHeaderLoading"), true);
                self.restDataSource.fetch(function (data) {
                    //kendo.ui.progress(self.$("#subHeaderLoading"), false);
                    if (this.data().length > 0) {
                        //self.writeLog('TimerWidgetView:OpenTimerRestDataSource:data 1st record:' + JSON.stringify(this.data()[0]));
                        // Open timer
                        //if (callingButton === "start") {
                        alert('You already have an open timer file.');
                        App.model.set('timerStatus', "Nothing2");
                        self.$('#startButton').removeClass('disabled');
                        self.$('#manualButton').removeClass('disabled');
                        //}

                    } else {
                        // No open timer
                        if (callingButton === "start") {
                            self.addTimerRecord();
                        } else if (callingButton === "manual") {

                            var serviceDDL = $("#service").data("kendoDropDownList");
                            var serviceDataItem = serviceDDL.dataItem();
                            if (serviceDataItem !== undefined && serviceDataItem !== null) {
                                self.serviceID = serviceDataItem.serviceID;
                                self.serviceItem = serviceDataItem.serviceItem;
                                //console.log('TimerWidgetView:checkForOpenTimer:serviceID:' + self.serviceID);
                            } else {
                                alert('A Service must be chosen in order to enter a manual time.');
                                self.$('#startButton').removeClass('disabled');
                                self.$('#manualButton').removeClass('disabled');
                                return;
                            }

                            var clientDDL = $("#timerClientList").data("kendoDropDownList");
                            var clientDataItem = clientDDL.dataItem();
                            if (clientDataItem !== undefined && clientDataItem !== null) {
                                self.clientID = clientDataItem.clientID;
                                self.clientName = clientDataItem.name;
                                self.companyID = clientDataItem.companyID;
                                self.taskTracking = clientDataItem.taskTracking;
                                //console.log('TimerWidgetView:checkForOpenTimer:clientID:' + self.clientID);
                                //console.log('TimerWidgetView:checkForOpenTimer:client record:' + JSON.stringify(clientDataItem));
                            } else {
                                //if (self.serviceID !== 14) {
                                    alert('A Client must be chosen in order to enter a manual time.');
                                    self.$('#startButton').removeClass('disabled');
                                    self.$('#manualButton').removeClass('disabled');
                                    return;
                                //}
                            }

                            //var today = moment(new Date()).clone().format('YYYY-MM-DD');

                            self.recordTasksMtgs = serviceDataItem.recordTasksMtgs;
                            self.toStats = serviceDataItem.toStats;

                            App.modal.show(new TimerOptionsView({
                                entryType: 'M',
                                serviceID: self.serviceID,
                                clientID: self.clientID,
                                companyID: self.companyID,
                                taskTracking: self.taskTracking,
                                serviceItem: self.serviceItem,
                                clientName: self.clientName,
                                recordTasksMtgs: self.recordTasksMtgs,
                                toStats: self.toStats
                            }));

                            self.timerStatus = "Manual";
                        }
                    }
                });
            },
            addTimerRecord: function (type) {
                var self = this;
                //var app = App;

                //console.log(('TimerWidgetView:addTimerRecord');

                var today = moment(new Date()).clone().format('YYYY-MM-DD'); //"2016-11-01";
                var offset = (new Date()).getTimezoneOffset();
                var now = new Date(moment().clone().subtract((offset), 'minutes'));

                self.startTime = moment().clone();

                var clientDDL = $("#timerClientList").data("kendoDropDownList");
                var clientDataItem = null;
                if (clientDDL !== null && clientDDL !== undefined) {
                    clientDataItem = clientDDL.dataItem();
                }

                if (clientDataItem !== undefined && clientDataItem !== null) {
                    self.clientID = clientDataItem.clientID;
                    self.clientName = clientDataItem.name;
                    self.companyID = clientDataItem.companyID;
                    self.taskTracking = clientDataItem.taskTracking;
                    clientDDL.readonly();
                    //console.log('TimerWidgetView:addTimerRecord:clientID:' + self.clientID);
                } else {
                    alert('A Client must be chosen in order to enter start the timer.');
                    self.$('#startButton').removeClass('disabled');
                    self.$('#manualButton').removeClass('disabled');
                    return;
                }

                var serviceDDL = $("#service").data("kendoDropDownList");
                var serviceDataItem = null;
                if (serviceDDL !== null && serviceDDL !== undefined) {
                    serviceDataItem = serviceDDL.dataItem();
                }
                if (serviceDataItem !== undefined && serviceDataItem !== null) {
                    self.serviceID = serviceDataItem.serviceID;
                    self.serviceItem = serviceDataItem.serviceItem;

                    self.recordTasksMtgs = serviceDataItem.recordTasksMtgs;
                    self.toStats = serviceDataItem.toStats;

                    serviceDDL.readonly();
                    //console.log('TimerWidgetView:startButtonClicked:serviceID:' + self.serviceID);
                } else {
                    alert('A Service must be chosen in order to start the timer.');
                    self.$('#startButton').removeClass('disabled');
                    self.$('#manualButton').removeClass('disabled');
                    return;
                }

                var intTimerID = null; // self.currentTimerID;
                if (self.intTimerID !== null) {
                    intTimerID = self.intTimerID;
                }

                console.log('TimerWidgetView:addTimerRecord:intTimerID:' + intTimerID);

                // Add new timer record
                self.timerDataSource.fetch(function () {

                    if (type === "resume") {
                        //self.writeLog('TimerWidgetView:addTimerRecord:resume');
                        self.timerDataSource.add({
                            idTimeStamp: now,
                            entryType: "A",
                            employeeID: self.myEmployeeId,
                            clientID: self.clientID,
                            serviceID: self.serviceID,
                            workDate: today,
                            startTime: now,
                            stopTime: new Date(0),
                            recordType: "INT",
                            timerAction: "stop",
                            intTimerID: intTimerID
                        });

                    } else {
                        //self.writeLog('TimerWidgetView:addTimerRecord:add');
                        self.timerDataSource.add({
                            idTimeStamp: now,
                            entryType: "A",
                            employeeID: self.myEmployeeId,
                            clientID: self.clientID,
                            serviceID: self.serviceID,
                            workDate: today,
                            startTime: now,
                            stopTime: new Date(0),
                            recordType: "NORM",
                            timerAction: "stop"
                        });
                    }

                    self.timerDataSource.sync();

                    //$.when(self.timerDataSource.sync()).done(function (e) {
                    //    self.writeLog('TimerWidgetView:addTimerRecord:add:sync done');
                    //    self.timerStatus = "Open";
                    //});

                });

            },
            updateTimerRecordWithTimerId: function () {

                var self = this;
                var app = App;

                //self.writeLog('TimerWidgetView:updateTimerRecordWithTimerId');

                if (self.currentTimerID !== null) {
                    self.currentTimerID = parseInt(self.currentTimerID, 0);
                    self.timerDataSource.filter({field: "timerID", operator: "eq", value: self.currentTimerID});
                    self.timerDataSource.fetch(function () {
                        var data = this.data();

                        if (data.length > 0) {

                            var timerRecord = self.timerDataSource.at(0);

                            var offset = (new Date()).getTimezoneOffset();
                            var startTime = new Date(moment(timerRecord.idTimeStamp).clone().subtract((offset), 'minutes'));

                            if (self.intTimerID === null) {
                                timerRecord.set("intTimerID", self.currentTimerID);
                                App.model.set('intTimerID', self.currentTimerID);
                            } else {
                                timerRecord.set("intTimerID", self.intTimerID);
                            }
                            timerRecord.set("startTime", startTime);
                            timerRecord.set("idTimeStamp", startTime);

                            self.timerDataSource.sync();

                            self.timerStatus = "Open";
                            App.model.set('timerInterruptTime', null);
                            App.model.set('timerStartTime', self.startTime);
                            App.model.set('timerStatus', self.timerStatus);

                        }
                    });
                } else {
                    //self.writeLog('TimerWidgetView:updateTimerRecordWithTimerID:currentTimerID is null');
                }
            },
            updateTimerRecord: function (type) {

                // This function updates timer record for interrupt, resume, and finish cases

                var self = this;
                var app = App;

                //self.writeLog('TimerWidgetView:updateTimerRecord:type:' + type);

                if (self.currentTimerID === null || self.currentTimerID === undefined) {
                    self.currentTimerID = App.model.get('currentTimerID');
                }
                //self.writeLog('TimerWidgetView:updateTimerRecord:currentTimerID:' + self.currentTimerID);

                if (self.currentTimerID !== null && self.currentTimerID !== undefined && type === "resume") {
                    //self.writeLog('TimerWidgetView:updateTimerRecord:use currentTimerID: ' + self.currentTimerID);
                    self.currentTimerID = parseInt(self.currentTimerID, 0);
                    self.timerDataSource.filter({field: "timerID", operator: "eq", value: self.currentTimerID});
                    //self.timerDataSource.sort({field: "timerID", dir: "desc"});
                } else if (self.intTimerID !== null && self.intTimerID !== undefined) {
                    //self.writeLog('TimerWidgetView:updateTimerRecord:use intTimerID');
                    self.intTimerID = parseInt(self.intTimerID, 0);
                    self.timerDataSource.filter([
                        {field: "intTimerID", operator: "eq", value: self.intTimerID},
                        {field: "timerAction", operator: "neq", value: "none"}]);
                    self.timerDataSource.sort({field: "timerID", dir: "desc"});
                }
                //else {
                //    self.timerDataSource.sort({field: "timerID", dir: "desc"});
                //}
                if (self.timerDataSource.filter !== null) {
                    //self.timerDataSource.filter({field: "timerID", operator: "eq", value: self.currentTimerID});
                    self.timerDataSource.fetch(function () {
                        var data = this.data();

                        if (data.length > 0) {

                            var timerRecord = self.timerDataSource.at(0);
                            var offset = (new Date()).getTimezoneOffset();
                            var now = new Date(moment().clone().subtract((offset), 'minutes'));
                            self.interruptTime = moment().clone().format();

                            var startTime = new Date(moment(timerRecord.startTime).clone().subtract((offset), 'minutes'));
                            var stopTime = new Date(moment(timerRecord.stopTime).clone().subtract((offset), 'minutes'));

                            if (moment(now).isBefore(startTime)) {

                                var message = 'TimerWidgetView:updateTimerRecord:now is before start time:now:startTime:' + now + ":" + startTime;
                                //console.log(message);
                                self.writeLog(message);
                            }
                            //else {

                                //self.writeLog('TimerWidgetView:updateTimerRecord:updateTimerRecord:type:' + type);

                                var stop = moment(now).clone();
                                var start = moment(startTime).clone();

                                var seconds = parseFloat(stop.diff(start, 'seconds'));
                                var minutes = parseFloat(stop.diff(start, 'minutes'));
                                var hours = parseFloat(stop.diff(start, 'hours'));

                                var timeWorkedText = "";
                                var timeWorked = 0;

                                // Round up minutes
                                if ((minutes + 1) * 60 - seconds >= 30) {
                                    minutes += 1;
                                }

                                // If less than 1 minute, make it 1 minute or 1/100th hour
                                if (minutes < 1) {
                                    timeWorked = 0.01;
                                    timeWorkedText = "0:1";
                                } else {
                                    timeWorked = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(seconds / 3600));
                                    timeWorkedText = parseInt(hours, 0).toString() + ":" + (parseInt(minutes, 0) - parseInt(hours, 0) * 60).toString();
                                }

                                if (type === "resume") {
                                    timerRecord.set("startTime", startTime);
                                    timerRecord.set("idTimeStamp", startTime);
                                    timerRecord.set("timerAction", 'none');
                                    timerRecord.set("stopTime", stopTime);
                                    timerRecord.set("recordType", 'INT-STOP');
                                    timerRecord.set("intTimerID", self.intTimerID);
                                    timerRecord.set("clientID", self.clientID);
                                    timerRecord.set("serviceID", self.serviceID);
                                    App.model.set('timerInterruptTime', null);
                                } else if (type === "interrupt") {
                                    timerRecord.set("startTime", startTime);
                                    timerRecord.set("idTimeStamp", startTime);
                                    timerRecord.set("stopTime", now);
                                    timerRecord.set("timeWorkedText", timeWorkedText);
                                    timerRecord.set("timeWorked", timeWorked);
                                    timerRecord.set("tasks", self.tasks);
                                    timerRecord.set("meetings", self.meetings);
                                    timerRecord.set("timerNote", self.timerNote);
                                    timerRecord.set("timerNote", 'INTERRUPTED');
                                    timerRecord.set("recordType", 'INT');
                                    timerRecord.set("timerAction", 'resume');
                                    App.model.set('timerInterruptTime', self.interruptTime);
                                } else if (type === "finish") {
                                    timerRecord.set("startTime", startTime);
                                    timerRecord.set("idTimeStamp", startTime);
                                    timerRecord.set("stopTime", now);
                                    timerRecord.set("timeWorkedText", timeWorkedText);
                                    timerRecord.set("timeWorked", timeWorked);
                                    timerRecord.set("timerAction", 'finish');

                                    self.intTimerID = null;
                                    self.currentTimerID = null;
                                }

                                $.when(self.timerDataSource.sync()).done(function (e) {
                                    //self.writeLog('TimerWidgetView:updateTimerRecord:done');

                                    if (type === "resume") {
                                        self.addTimerRecord("resume");
                                    }
                                    self.resetStatsDashboardCache();
                                    App.model.set('timerStatus', self.timerStatus);

                                    if (type === "finish") {
                                        App.modal.show(new TimerOptionsView({
                                            entryType: 'A',
                                            serviceID: self.serviceID,
                                            clientID: self.clientID,
                                            companyID: self.companyID,
                                            taskTracking: self.taskTracking,
                                            serviceItem: self.serviceItem,
                                            clientName: self.clientName,
                                            recordTasksMtgs: self.recordTasksMtgs,
                                            toStats: self.toStats
                                        }));

                                    }

                                });
                            //}
                        }

                    });
                } else {
                    //self.writeLog('TimerWidgetView:updateTimerRecord:currentTimerID is null');
                }

            },
            writeLog: function (text) {
                //console.log('TimerWidgetView:writeLog');

                var self = this;

                //self.timerLogDataSource.fetch(function () {

                    var offset = (new Date()).getTimezoneOffset();
                    var now = new Date(moment().clone().subtract((offset), 'minutes'));

                    var logText = "currentTimerID: " + self.currentTimerID + "\r\n";
                    logText += ":intTimerID: " + self.intTimerID + "\r\n";
                    logText += ":" + text;

                    self.timerLogDataSource.add({
                        logDate: now,
                        timerID: self.currentTimerID,
                        logText: logText
                    });

                    //this.sync();

                    //console.log('TimerOptionsView:writeLog:done');
                //});
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('TimerWidgetView:resize');

            },
            remove: function () {
                console.log('-------- TimerWidgetView:remove --------');
                //this.writeLog('-------- TimerWidgetView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });