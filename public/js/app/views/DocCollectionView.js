define(['App', 'backbone', 'marionette', 'jquery', 'models/AdminDataModel', 'hbs!templates/docCollection',
        'views/DocView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, AdminDataModel, template, DocView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: DocView,
            initialize: function (options) {

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                //console.log('DocCollectionView:initialize:type:' + this.options.type);

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.docs = [];

                var self = this;

                this.getData();

            },
            onShow: function () {
                //console.log("-------- DocCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                var filter = [
                    {field: "category", operator: "eq", value: self.options.category}
                ];

                if (self.options.category === 11) {
                    filter.push( {field: "subCategory", operator: "eq", value: self.options.subCategory});
                }

                this.adminDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalAdminData";
                            },
                            complete: function (e) {
                                console.log('DocCollectionView:adminDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'DocCollectionView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {editable: false, type: "number"},
                                text: {editable: true, type: "string"},
                                shortText: {editable: true, type: "string"},
                                text2: {editable: true, type: "string"},
                                title: {editable: true, type: "string"},
                                fileName: {editable: true, type: "string"},
                                externalLink: {editable: true, type: "string"},
                                category: {editable: true, type: "number"},
                                subCategory: {editable: true, type: "number"},
                                priority: {editable: true, type: "number"},
                                showAsCompanyMessage:{editable: true, type: "boolean"},
                                addUserImage:{editable: true, type: "boolean"},
                                timeStamp: {editable: true,type: "date"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   DocCollectionView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#docCollectionViewLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    DocCollectionView:messageDataSource:requestEnd');
                        //kendo.ui.progress(self.$("#docCollectionViewLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   DocCollectionView:messageDataSource:error');
                        kendo.ui.progress(self.$("#docCollectionViewLoading"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   DocCollectionView:messageDataSource:change');

                        var data = this.data();

                        //if (data.length <= 0)
                        //    self.displayNoDataOverlay();
                        //else
                        //    self.hideNoDataOverlay();
                    },
                    //sort: {field: "messageDate", dir: "desc"},
                    filter: filter
                });

                this.adminDataSource.fetch(function () {

                    var data = this.data();
                    $.each(data, function (index, value) {

                        self.docs.push({
                            ID:value.ID,
                            fileName:value.fileName,
                            title:value.title,
                            shortText:value.shortText,
                            text:value.text,
                            text2:value.text2,
                            subCategory: value.subCategory,
                            category: value.category
                        });
                    });

                    self.docs.sort(function(a,b){
                        var c = new Date(a.shortText);
                        var d = new Date(b.shortText);
                        return d-c;
                    });

                    // Populate doc collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return (model.get("fileName"));
                    };

                    $.each(self.docs, function (index, value) {

                        //console.log('DocCollectionView:file:value:' + JSON.stringify(value));

                        if (value.text !== "" && value.text !== null && value.text !== undefined && value.fileName !== "" && value.fileName !== null && value.fileName !== undefined) {
                            var model = new AdminDataModel();

                            model.set("ID", value.ID);
                            model.set("fileName", value.fileName);
                            model.set("title", value.title);
                            model.set("shortText", value.shortText);
                            model.set("text", value.text);
                            model.set("text2", value.text2);
                            model.set("category", value.category);
                            model.set("subCategory", value.subCategory);


                            self.collection.push(model);
                        }
                    });

                    self.collection.sort();
                });
            },
            hideNoDataOverlay: function () {
                //console.log('DocCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#fileWidgetContent').css('display', 'block');
            },
            onResize: function () {
                //console.log('DocCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('DocCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- DocCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
