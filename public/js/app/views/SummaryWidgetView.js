define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/summaryWidget',
        'kendo/kendo.data'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('SummaryWidgetView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Tasks"};
                }

                //var d = new Date();
                //this.year = d.getFullYear();

                this.clientID = parseInt(App.model.get('selectedClientId'),0); //2189;

                this.dateFilter = App.model.get('selectedDateFilter');
                var app = App;
                this.startDate = new Date(app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter)); //"2016-10-01T00:00:00+0000"
                this.endDate = new Date(app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter)); //"2016-10-31T23:59:59+0000"
                this.endDate = this.endDate.addHours(23);
                this.endDate = this.endDate.addMinutes(59);

                this.progressClientFilters = {
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: [
                                {field: "planEndDate", operator: "eq", value: null},
                                {field: "planEndDate", operator: "gt", value: this.endDate}
                            ]
                        },
                        {field: "planStartDate", operator: "lte", value: this.endDate},
                        {field: "clientID", operator: "eq", value: this.clientID}
                    ]
                };


            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- SummaryWidgetView:onShow ----------');

                // UIUX - Events beyond css and view classes
                var self = this;

                this.$('#widgetTitle').html(this.options.type);

                this.populateWidget();
            },
            populateWidget: function () {
//                //console.log('SummaryWidgetView:createChart');

                var self = this;
                var app = App;

                this.progressClientDataSource = new kendo.data.DataSource(
                    {
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClient";
                            },
                            complete: function (e) {
                                console.log('SummaryWidgetView:progressClientDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'SummaryWidgetView:progressClientDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                planHours: {type: "number"},
                                schedulingHours: {type: "number"},
                                taskTargetLow: {type: "number"},
                                taskTargetHigh: {type: "number"},
                                mtgTargetLowDec: {type: "number"},
                                mtgTargetHigh: {type: "number"},
                                planStartDate: {editable: true, type: "date", validation: {required: false}},
                                planEndDate: {editable: true, type: "date", validation: {required: false}}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   SummaryWidgetView:requestStart');

                        kendo.ui.progress(self.$("#loadingSummaryWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    SummaryWidgetView:requestEnd');
                        kendo.ui.progress(self.$("#loadingSummaryWidget"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryWidgetView:error');
                        kendo.ui.progress(self.$("#loadingSummaryWidget"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryWidgetView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: self.progressClientFilters
                    //[{field: "clientID", operator: "eq", value: self.clientID}]

                });

                this.progressDataDataSource = new kendo.data.DataSource(
                    {
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressData";
                            },
                            complete: function (e) {
                                console.log('SummaryWidgetView:progressDataDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'SummaryWidgetView:progressDataDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "entryID",
                            fields: {
                                entryID: {editable: false, defaultValue: 0, type: "number"},
                                workDate: {editable: true, type: "date", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: false}},
                                schedulerTime: {type: "number"},
                                support1Time: {type: "number"},
                                adminTime: {type: "number"},
                                schedulerCalls: {type: "number"},
                                support1Calls: {type: "number"},
                                schedulerMeetings: {type: "number"},
                                support1Meetings: {type: "number"},
                                compTime: {type: "number"},
                                compCalls: {type: "number"},
                                compMeetings: {type: "number"},
                                referralTime: {type: "number"},
                                referralCalls: {type: "number"},
                                referralMeetings: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   SummaryWidgetView:progressDataDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingSummaryWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    SummaryWidgetView:progressDataDataSource:requestEnd');
                        kendo.ui.progress(self.$("#loadingSummaryWidget"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryWidgetView:progressDataDataSource:error');
                        kendo.ui.progress(self.$("#loadingSummaryWidget"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   SummaryWidgetView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: [{field: "clientID", operator: "eq", value: self.clientID},
                        {field: "workDate", operator: "gte", value: self.startDate},
                        {field: "workDate", operator: "lte", value: self.endDate}]

                });

                this.progressDataDataSource.fetch(function () {

                    //<cfloop query="getClientStats">
                    //    <cfset vTimeActual = numberFormat((vTimeActual + #schedulerTime# + #support1Time# + #adminTime#),"99.99")>
                    //    <cfset vTimeActualNoAdmin = vTimeActualNoAdmin + #schedulerTime# + #support1Time#>
                    //    <cfset vTasksActual = vTasksActual + #schedulerCalls# + #support1Calls#>
                    //    <cfset vMeetingsActual = vMeetingsActual + #schedulerMeetings# + #support1Meetings#>
                    //    <cfset vTimeAdmin = vTimeAdmin + #adminTime#>
                    //    <cfset vSchedulerTime = vSchedulerTime + #schedulerTime#>
                    //    <cfset vSupport1Time = vSupport1Time + #support1Time#>
                    //    <cfset vCompTime = vCompTime + #compTime#>
                    //    <cfset vSchedulerCalls = vSchedulerCalls + #schedulerCalls#>
                    //    <cfset vSupport1Calls = vSupport1Calls + #support1Calls#>
                    //    <cfset vCompCalls = vCompCalls + #compCalls#>
                    //    <cfset vSchedulerMeetings = vSchedulerMeetings + #schedulerMeetings#>
                    //    <cfset vSupport1Meetings = vSupport1Meetings + #support1Meetings#>
                    //    <cfset vCompMeetings = vCompMeetings + #compMeetings#>
                    //</cfloop>

                    var progressData = this.data();
                    //var progressDataCount = this.data().length;

                    var timeActual = 0;
                    var timeActualNum = 0;

                    var timeAdmin = 0;
                    var timeScheduled = 0;
                    var timeSupport = 0;

                    var tasksActual = 0;
                    var tasksScheduled = 0;
                    var tasksSupport = 0;

                    var meetingsActual = 0;
                    var meetingsScheduled = 0;
                    var meetingsSupport = 0;

                    var compTime = 0;
                    var tasksComp = 0;
                    var meetingsComp = 0;

                    var referralTime = 0;
                    var tasksReferral = 0;
                    var meetingsReferral = 0;

                    $.each(progressData, function (index) {
                        var record = progressData[index];

                        timeActual +=  record.schedulerTime + record.support1Time + record.adminTime;
                        timeAdmin += record.adminTime;
                        timeSupport += record.support1Time;
                        timeScheduled += record.schedulerTime;

                        tasksActual += record.schedulerCalls + record.support1Calls;
                        tasksScheduled += record.schedulerCalls;
                        tasksSupport += record.support1Calls;

                        compTime += record.compTime;
                        tasksComp += record.compCalls;
                        meetingsComp += record.compMeetings;

                        meetingsActual += record.schedulerMeetings + record.support1Meetings;
                        meetingsScheduled += record.schedulerMeetings;
                        meetingsSupport += record.support1Meetings;

                        referralTime += record.referralTime;
                        tasksReferral += record.referralCalls;
                        meetingsReferral += record.referralMeetings;

                    });

                    self.progressClientDataSource.fetch(function () {

                        var progressClientRecord = this.data()[0];
                        var mtgTargetLowDec = progressClientRecord.mtgTargetLowDec;
                        var mtgTargetHigh = progressClientRecord.mtgTargetHigh;
                        var taskTargetLow = progressClientRecord.taskTargetLow;
                        var taskTargetHigh = progressClientRecord.taskTargetHigh;

                        // If scheduling hours is 0, it's an hourly plan. Planned hours should then be actual hours (NN - 8/29/18)
                        var planHours = progressClientRecord.planHours;
                        if (progressClientRecord.schedulingHours === 0) {
                            planHours = timeActual;
                        }
                        timeActualNum = timeActual;
                        timeActual = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeActual);

                        var timeActualPlusComp =app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeActualNum + compTime);
                        var tasksActualPlusComp = tasksActual + tasksComp;
                        var meetingsActualPlusComp = meetingsActual + meetingsComp;

                        var timeActualPlusReferral =app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeActualNum + referralTime);
                        var tasksActualPlusReferral = tasksActual + tasksReferral;
                        var meetingsActualPlusReferral = meetingsActual + meetingsReferral;

                        var timeRemaining = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(planHours - timeActualNum);
                        var tasksRemaining = app.Utilities.numberUtilities.getFormattedNumberWithRounding(taskTargetLow*planHours - tasksActual);
                        var meetingsRemaining = app.Utilities.numberUtilities.getFormattedNumberWithRounding(Math.max(mtgTargetLowDec*planHours - meetingsActual,0));

                        timeAdmin = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeAdmin);
                        timeSupport = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeSupport));
                        timeScheduled = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeScheduled);
                        
                        compTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(compTime);
                        referralTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(referralTime);

                        tasksActual = app.Utilities.numberUtilities.getFormattedNumberWithRounding(tasksActual);
                        tasksScheduled = app.Utilities.numberUtilities.getFormattedNumberWithRounding(tasksScheduled);
                        //tasksSupport = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithRounding(tasksSupport));

                        meetingsActual = app.Utilities.numberUtilities.getFormattedNumberWithRounding(meetingsActual);
                        meetingsScheduled = app.Utilities.numberUtilities.getFormattedNumberWithRounding(meetingsScheduled);
                        //meetingsSupport = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithRounding(meetingsSupport));

                        // Meetings, support1 meetings

                        //<!--- Avg 0-1: red text - FF0000
                        // Avg 2: yellow text - FFC002
                        // Avg 3-5: green text - 4B7101
                        // Avg 6+: purple text - 630460 --->
                        //<cfif (#vTimeActual# - #vTimeAdmin#) GT 0>
                        //    <cfset vMeetingsPerHour = max(#vMeetingsActual# / (#vTimeActual# - #vTimeAdmin#),0)>
                        //    <cfelse>
                        //    <cfset vMeetingsPerHour = 0>
                        //</cfif>

                        // Tasks, calls, support1 calls,

                        // Avg 0 -10: red text - FF0000
                        // Avg 11 -14: yellow text - ECEC00
                        // Avg 15 - 20: green text - 4B7101
                        // Avg 21+: purple text - 630460

                        var meetingsPerHour = 0;
                        var scheduledMeetingsPerHour = 0;
                        var supportMeetingsPerHour = 0;
                        var compMeetingsPerHour = 0;

                        if (timeActualNum - timeAdmin > 0) {
                            meetingsPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(meetingsActual/(timeActualNum - timeAdmin));
                        }
                        if (timeScheduled !== 0) {
                            scheduledMeetingsPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(meetingsScheduled/timeScheduled);
                        }
                        if (timeSupport !== 0) {
                            supportMeetingsPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(meetingsSupport/timeSupport);
                        } else {
                            supportMeetingsPerHour = "0.00";
                        }

                        if (compTime !== 0) {
                            compMeetingsPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(meetingsComp/compTime);
                        }

                        if (referralTime !== 0) {
                            referralMeetingsPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(meetingsReferral/referralTime);
                        }

                        var tasksPerHour = 0;
                        var scheduledTasksPerHour = 0;
                        var supportTasksPerHour = 0;
                        var compTasksPerHour = 0;

                        if (timeActualNum - timeAdmin > 0) {
                            tasksPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(tasksActual/(timeActualNum - timeAdmin));
                        }
                        if (timeScheduled !== 0) {
                            scheduledTasksPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(tasksScheduled/timeScheduled);
                        }
                        if (timeSupport !== 0) {
                            supportTasksPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(tasksSupport/timeSupport);
                        } else {
                            supportTasksPerHour = "0.00";
                        }

                        if (compTime !== 0) {
                            compTasksPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(tasksComp/compTime);
                        }

                        if (referralTime !== 0) {
                            referralTasksPerHour = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(tasksReferral/referralTime);
                        }

                        if (self.options.type === "Meetings") {
                            self.$('#monthlyNum').html(app.Utilities.numberUtilities.getFormattedNumberWithRounding(mtgTargetLowDec * planHours));
                            self.$('#actualNum').html(meetingsActual);
                            self.$('#schedNum').html(scheduledMeetingsPerHour);
                            self.$('#supportNum').html(supportMeetingsPerHour);
                            self.$('#overallNum').hide();

                            self.$('#scheduledLabel').html("Scheduler Mtgs/Hr");
                            self.$('#supportLabel').html("Support Mtgs/Hr");
                            self.$('#adminLabel').hide();

                            //console.log('SummaryWidgetView:meetingsPerHour:' + meetingsPerHour);

                            if (meetingsPerHour < 2) {
                                self.$('#actualNum').css("color","red");
                            } else if (meetingsPerHour < 3) {
                                self.$('#actualNum').css("color","#FFC002");
                            } else if (meetingsPerHour < 6) {
                                self.$('#actualNum').css("color","green");
                            } else {
                                self.$('#actualNum').css("color","purple");
                            }

                            self.$('#remainingNum').html(meetingsRemaining);

                            if (compTime > 0) {
                                self.$('.comp').show();
                                self.$('#compNum').html(meetingsComp);
                                self.$('#compActualNum').html(meetingsActualPlusComp);
                                self.$('#compRateLabel').html("Comp Mtgs/Hr");
                                self.$('#compRate').html(compMeetingsPerHour);
                            } else {
                                self.$('.comp').hide();
                            }

                            if (referralTime > 0) {
                                self.$('.referral').show();
                                self.$('#referralNum').html(meetingsReferral);
                                self.$('#referralActualNum').html(meetingsActualPlusReferral);
                                self.$('#referralRateLabel').html("Referral Mtgs/Hr");
                                self.$('#referralRate').html(referralMeetingsPerHour);
                            } else {
                                self.$('.referral').hide();
                            }

                        } else if (self.options.type === "Time") {
                            self.$('#monthlyNum').html(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(planHours));
                            self.$('#actualNum').html(timeActual);
                            self.$('#remainingNum').html(timeRemaining);

                            self.$('#schedNum').html(timeScheduled);
                            self.$('#supportNum').html(timeSupport);
                            self.$('#overallNum').html(timeAdmin);

                            self.$('#scheduledLabel').html("Scheduled");
                            self.$('#supportLabel').html("Support");
                            self.$('#adminLabel').html("Admin");

                            if (timeRemaining < 0) {
                                self.$('#remainingNum').css("color","red");
                            }

                            if (compTime > 0) {
                                self.$('.comp').show();
                                self.$('#compNum').html(compTime);
                                self.$('#compActualNum').html(timeActualPlusComp);
                                self.$('#compRateLabel').hide();
                                self.$('#compRate').hide();
                            } else {
                                self.$('.comp').hide();
                            }

                            if (referralTime > 0) {
                                self.$('.referral').show();
                                self.$('#referralNum').html(referralTime);
                                self.$('#referralActualNum').html(timeActualPlusReferral);
                                self.$('#referralRateLabel').hide();
                                self.$('#referralRate').hide();
                            } else {
                                self.$('.referral').hide();
                            }

                        } else { //if (this.options.type === "Tasks") {
                            self.$('#monthlyNum').html(app.Utilities.numberUtilities.getFormattedNumberWithRounding(taskTargetLow * planHours));
                            self.$('#actualNum').html(tasksActual);
                            self.$('#schedNum').html(scheduledTasksPerHour);
                            self.$('#supportNum').html(supportTasksPerHour);
                            self.$('#overallNum').hide();

                            self.$('#scheduledLabel').html("Scheduler Tasks/Hr");
                            self.$('#supportLabel').html("Support Tasks/Hr");
                            self.$('#adminLabel').hide();
                            //console.log('SummaryWidgetView:tasksPerHour:' + tasksPerHour);

                            if (tasksPerHour <= 10) {
                                self.$('#actualNum').css("color","red");
                            } else if (tasksPerHour < 15) {
                                self.$('#actualNum').css("color","#FFC002");
                            } else if (tasksPerHour < 21) {
                                self.$('#actualNum').css("color","green");
                            } else {
                                self.$('#actualNum').css("color","purple");
                            }

                            if (tasksRemaining < 0)
                                tasksRemaining = 0;
                            self.$('#remainingNum').html(tasksRemaining);

                            if (compTime > 0) {
                                self.$('.comp').show();
                                self.$('#compNum').html(tasksComp);
                                self.$('#compActualNum').html(tasksActualPlusComp);
                                self.$('#compRateLabel').html("Comp Tasks/Hr");
                                self.$('#compRate').html(compTasksPerHour);
                            } else {
                                self.$('.comp').hide();
                            }

                            if (referralTime > 0) {
                                self.$('.referral').show();
                                self.$('#referralNum').html(tasksReferral);
                                self.$('#referralActualNum').html(tasksActualPlusReferral);
                                self.$('#referralRateLabel').html("Referral Tasks/Hr");
                                self.$('#referralRate').html(referralTasksPerHour);
                            } else {
                                self.$('.referral').hide();
                            }
                        }
                    });
                });

            },
            displayNoDataOverlay: function () {
                //console.log('SummaryWidgetView:displayNoDataOverlay');

                // Hide the widget
                this.$('#summaryData').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message" style="height:130px;">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('SummaryWidgetView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget
                this.$('#summaryData').css('display', 'block');
            },
            mouseEnter: function () {
//                //console.log('SummaryWidgetView:mouseEnter');

                self.$('#SummaryWidgetTile').find('.tile-icon').addClass('clr-selected');
            },
            mouseLeave: function () {
//                //console.log('SummaryWidgetView:mouseLeave');

                self.$('#SummaryWidgetTile').find('.tile-icon').removeClass('clr-selected');
            },
            onResize: function () {
//                //console.log('SummaryWidgetView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('SummaryWidgetView:resize');

            },
            remove: function () {
                //console.log('---------- SummaryWidgetView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off
                this.$('.tile-content').off();
                this.$('.tile-more-info').off();
                this.$('.tile-hover-tip').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });