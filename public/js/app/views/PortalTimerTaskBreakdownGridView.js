define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalTimerTaskBreakdownGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.all.min'], //'kendo.all.min',
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalTimerTaskBreakdownGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                //this.userId = App.model.get('userId');

                this.dateFilter = App.model.get('selectedDateFilter');
                var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                this.startDate = moment(startDate,'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                this.endDate = moment(endDate,'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                var companyID = App.model.get('selectedCompanyId');

                if (!isNaN(companyID) && companyID !== null) {
                    this.companyID = parseInt(companyID, 0); //12
                } else {
                    this.companyID = 0;
                }

                //console.log('PortalTimerTaskBreakdownGridView:initialize:thisCompanyID:testCompanyID:' + this.companyID + ":" + companyID);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetPortalTimerTaskBreakdown/" + self.startDate + "/" + self.endDate + "/" + self.companyID;
                                    return url;
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {
                            //data: "value",
                            //total: function (data) {
                            //    return data['odata.count'];
                            //},
                            model: {
                                id: "timerID",
                                fields: {

                                    timerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    schedulerName: {editable: true, type: "string", validation: {required: false}},
                                    serviceID: {editable: false, type: "number"},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    tasksCalls: {editable: true, type: "number"},
                                    tasksEmails: {editable: true, type: "number"},
                                    tasksVoiceMails: {editable: true, type: "number"},
                                    status: {editable: true, type: "string", validation: {required: false}}
                                }
                            },
                            sort: [
                                {field: "companyName", dir: "asc"},
                                {field: "lastName", dir: "asc"},
                                {field: "firstName", dir: "asc"},
                                {field: "serviceItem", dir: "asc"}
                            ],
                            //filter: {field: "lastName", operator: "neq", value: ""},
                            requestStart: function (e) {
                                //console.log('PortalTimerTaskBreakdownGridView:dataSource:request start:');
                                kendo.ui.progress(self.$("#taskBreakdownLoading"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('PortalTimerTaskBreakdownGridView:dataSource:request end:');
                                //var data = this.data();
                                kendo.ui.progress(self.$("#taskBreakdownLoading"), false);
                            },
                            change: function (e) {
                                //console.log('PortalTimerTaskBreakdownGridView:dataSource:change:');

                                var data = this.data();
                                if (data.length > 0) {

                                } else {
                                    self.displayNoDataOverlay();
                                }
                            },
                            error: function (e) {
                                //console.log('PortalTimerTaskBreakdownGridView:dataSource:error');
                                kendo.ui.progress(self.$("#taskBreakdownLoading"), false);
                            }
                        }
                    });

            },
            onRender: function () {
                //console.log('PortalTimerTaskBreakdownGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalTimerTaskBreakdownGridView:onShow --------");
                var self = this;

                // Panel title
                $("#taskBreakdownTitle").html("Task Breakdown Reports");

                //FirstName  LastName  ServiceItem  Hours  Calls  Messages  Emails  Meetings

                this.grid = this.$("#taskBreakdown").kendoGrid({
                    toolbar: ["excel"],//,{name:"pdf"}
                    excel: {
                        fileName: "TaskBreakdownReport_" + self.companyID + ".xlsx",
                        allPages: true
                    },
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    filterable: {
                        mode: "row"
                    },
                    columnResize: function (e) {
                        //self.resize();
                        window.setTimeout(self.resize, 10);
                    },
                    columns: [{
                        field: "firstName",
                        title: "First Name",
                        width: 200,
                        locked: true,
                        filterable: {
                            cell: {
                                showOperators: false,
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    }, {
                        field: "lastName",
                        title: "Last Name",
                        width: 200,
                        locked: true,
                        filterable: {
                            cell: {
                                showOperators: false,
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    }, {
                        field: "clientCompany",
                        title: "Company",
                        width: 150,
                        locked: true,
                        filterable: false
                        // filterable: {
                        //     cell: {
                        //         showOperators: false,
                        //         operator: "contains",
                        //         suggestionOperator: "contains"
                        //     }
                        // }
                    }, {
                        field: "schedulerName",
                        title: "Scheduler",
                        width: 200,
                        locked: true,
                        // filterable: false
                        filterable: {
                            cell: {
                                showOperators: false,
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    },{
                        
                        field: "serviceItem",
                        title: "Service",
                        width: 200,
                        locked: true,
                        filterable: false
                    },{
                        field: "timeWorked",
                        title: "Hours",
                        width: 75,
                        filterable: false
                    }, {
                        field: "tasksCalls",
                        title: "Calls",
                        width: 75,
                        filterable: false
                    }, {
                        field: "tasksVoiceMails",
                        title: "Messages",
                        width: 75,
                        filterable: false
                    }, {
                        field: "tasksEmails",
                        title: "Emails",
                        width: 75,
                        filterable: false
                    },{
                        field: "meetings",
                        title: "Meetings",
                        width: "auto",
                        filterable: false
                    }

                    ],
                    dataBound: function (e) {
                        //console.log("PortalTimerTaskBreakdownGridView:def:onShow:dataBound");
                    },
                    change: function (e) {
                        //console.log('PortalTimerTaskBreakdownGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('PortalTimerTaskBreakdownGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('PortalTimerTaskBreakdownGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        e.container.find("input[name='employeeID']").prop("disabled", true);

                        // Set the focus to the Username field
                        //e.container.find("input[name='Username']").focus();

                    }
                }).data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PortalTimerTaskBreakdownGridView:getData');

                var app = App;
                var self = this;
                
                self.dataSource.filter([
                    {field: "status", operator: "eq", value: '4 - Active'}
                ]);

                self.dataSource.fetch(function () {
                    //console.log('PortalTimerTaskBreakdownGridView:getData:data 1st record:' + JSON.stringify(this.data[0]));

                    kendo.ui.progress(self.$("#taskBreakdownLoading"), true);

                    var taskBreakdownData = this.data();
                    var dataCount = taskBreakdownData.length;

                    //<cfquery name="getDataSummary" dbtype="query">
                    //    select       firstName AS FirstName, lastName AS LastName, serviceItem AS ServiceItem,
                    //    SUM(timeWorked) AS Hours,
                    //    SUM(tasksCalls) as Calls,
                    //    SUM(tasksVoiceMails) AS Messages,
                    //    SUM(tasksEmails) AS Emails,
                    //    SUM(meetings) AS Meetings
                    //    from        getData
                    //    group by    firstName, lastName, serviceItem
                    //    order by    lastName, firstName
                    //</cfquery>

                    var taskRecords = [];

                    var tasks = 0;
                    var meetings = 0;
                    var timeWorked = 0;
                    var tasksCalls = 0;
                    var tasksEmails = 0;
                    var tasksVoiceMails = 0;
                    var clientCompany = "";
                    var schedulerName = "";

                    if (dataCount > 0) {
                        var firstName2 = taskBreakdownData[0].firstName;
                        var lastName2 = taskBreakdownData[0].lastName;
                        //var clientCompany2 = taskBreakdownData[0].clientCompany;
                        var serviceItem2 = taskBreakdownData[0].serviceItem;
                        //var schedulerName2 = taskBreakdownData[0].schedulerName;

                        $.each(taskBreakdownData, function (index, value) {
                            var record = value; //taskBreakdownData[index];

                            var firstName = record.firstName;
                            var lastName = record.lastName;
                            //var clientCompany = record.clientCompany;
                            //var schedulerName = record.schedulerName;
                            var serviceItem = record.serviceItem;

                            if (lastName == "Comey") {
                                console.log('PortalTimerTaskBreakdownGridView:Comey');
                                var x = 1;        
                            }

                            if (firstName !== firstName2 || lastName !== lastName2 || serviceItem !== serviceItem2 ) {  //|| clientCompany !== clientCompany2 || schedulerName != schedulerName2
                                taskRecords.push({
                                    firstName: firstName2,
                                    lastName: lastName2,
                                    clientCompany: clientCompany,
                                    schedulerName:schedulerName,
                                    serviceItem: serviceItem2,
                                    tasks: tasks,
                                    meetings: meetings,
                                    timeWorked: app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeWorked),
                                    tasksCalls: tasksCalls,
                                    tasksEmails: tasksEmails,
                                    tasksVoiceMails: tasksVoiceMails
                                });

                                tasks = 0;
                                meetings = 0;
                                timeWorked = 0;
                                tasksCalls = 0;
                                tasksEmails = 0;
                                tasksVoiceMails = 0;
                            }

                            tasks += record.tasks;
                            meetings += record.meetings;
                            timeWorked += record.timeWorked;
                            tasksCalls += record.tasksCalls;
                            tasksEmails += record.tasksEmails;
                            tasksVoiceMails += record.tasksVoiceMails;

                            firstName2 = record.firstName;
                            lastName2 = record.lastName;
                            serviceItem2 = record.serviceItem;
                            schedulerName = record.schedulerName;
                            clientCompany = record.clientCompany;

                            if (index === (dataCount - 1)) {
                                taskRecords.push({
                                    firstName: firstName2,
                                    lastName: lastName2,
                                    clientCompany: clientCompany,
                                    schedulerName:schedulerName,
                                    serviceItem: serviceItem2,
                                    tasks: tasks,
                                    meetings: meetings,
                                    timeWorked: app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(timeWorked),
                                    tasksCalls: tasksCalls,
                                    tasksEmails: tasksEmails,
                                    tasksVoiceMails: tasksVoiceMails
                                });
                            }
                        });
                    }
                    self.grid.dataSource.data(taskRecords);

                    kendo.ui.progress(self.$("#taskBreakdownLoading"), false);

                });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalTimerTaskBreakdownGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#taskBreakdown').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('PortalTimerTaskBreakdownGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#taskBreakdown').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalTimerTaskBreakdownGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#taskBreakdown").parent().parent().height();
                this.$("#taskBreakdown").height(parentHeight);

                parentHeight = parentHeight - 37;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalTimerTaskBreakdownGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalTimerTaskBreakdownGridView:resize:headerHeight:' + headerHeight);
                this.$("#taskBreakdown").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalTimerTaskBreakdownGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#taskBreakdown").data('ui-tooltip'))
                    this.$("#taskBreakdown").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });