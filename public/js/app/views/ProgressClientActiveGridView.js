define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressClientActiveGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data', 'xlsx'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('ProgressClientActiveGridView:initialize');

                _.bindAll(this);

                var self = this;
                //var app = App;

                this.myEmployeeId = App.model.get('myEmployeeId');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                    //autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                            },
                            complete: function (e) {
                                console.log('ProgressClientActiveGridView:dataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'ProgressClientActiveGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                lastName: {editable: true, type: "string", validation: {required: false}},
                                firstName: {editable: true, type: "string", validation: {required: false}},
                                companyID: {editable: false, defaultValue: 0, type: "number"},
                                clientCompany: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    //serverFiltering: true,
                    //serverSorting: true,
                    requestStart: function () {
                        //console.log('   ProgressClientActiveGridView:requestStart');

                        kendo.ui.progress(self.$("#clientLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    ProgressClientActiveGridView:requestEnd');
                        kendo.ui.progress(self.$("#clientLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientActiveGridView:error');
                        kendo.ui.progress(self.$("#clientLoading"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientActiveGridView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: [{field: "status", operator: "eq", value: '4 - Active'}],
                    sort: [
                        {field: "clientCompany", dir: "asc"}, 
                        {field: "lastName", dir: "asc"},
                        {field: "firstName", dir: "asc"}
                    ]
                });

                this.companyDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                complete: function (e) {
                                    console.log('ProgressClientActiveGridView:companyDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientActiveGridView:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyID",
                                fields: {
                                    companyID: {editable: false, type: "number"},
                                    clientCompany: {type: "string"}
                                }
                            }
                        },
                        sort: {field: "clientCompany", dir: "asc"},
                        serverSorting: true,
                        serverFiltering: true,
                        filter: [
                            {field: "clientCompany", operator: "neq", value: "--- NONE ---"}
                            //{field: "actualClients", operator: "gt", value: 0},
                            //{field: "actualClients", operator: "neq", value: null}
                        ]

                    });

            },
            onRender: function () {
                //console.log('ProgressClientActiveGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientActiveGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#client").kendoGrid({
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    toolbar: [
                        {
                            name: "excel"
                        },
                        {
                            template: function (e) {
                                var template = '<a class="k-button k-button-icontext k-grid-importTimecardRecords"><span class="k-icon k-i-excel"></span>Import Timecard Records</a>';
                                return template;
                            }
                        }],
                    excel: {
                        fileName: "ActiveClients.xlsx",
                        allPages: true
                    },
                    filterable: {
                        mode: "row"
                    },
                    columns: [
                        {
                            //title: "My Employee ID",
                            field: "employeeID",
                            width: 80,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.employeeID + "</a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                            //locked: true
                        },
                        {
                            //title: "Client ID",
                            field: "clientID",
                            width: 60,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.clientID + "</a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                            //locked: true
                        },
                        {
                            //title: "Service ID",
                            field: "serviceID",
                            width: 60,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.serviceID + "</div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                            //locked: true
                        },
                        {
                            //title: "Time Worked",
                            field: "timeWorked",
                            width: 75,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.timeWorked + "</div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                            //locked: true
                        },
                        {
                            //title: "Date",
                            field: "workDate",
                            width: 100,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.workDate + "</div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                            //locked: true
                        },
                        {
                            //title: "Timer Note",
                            field: "timerNote",
                            width: 150,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.timerNote+ "</div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                            //locked: true
                        },
                        {
                            title: "Client Name",
                            field: "clientName",
                            width: 150,
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" + e.lastName + ", " + e.firstName + "</a></div>";
                                return template;
                            },
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            },
                            sortable: true
                            //locked: true
                        },
                        {
                            title: "Client Company",
                            field: "clientCompany",
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                return template;
                            },
                            width: 200,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.companyFilter
                                }
                            },
                            sortable: true
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("ProgressClientActiveGridView:def:onShow:dataBound");
                        self.$('.client-profile').on('click', self.viewClientProfile);
                        self.$('.company-profile').on('click', self.viewCompanyProfile);


                    },
                    change: function (e) {
                        //console.log('ProgressClientActiveGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('ProgressClientActiveGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('ProgressClientActiveGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        e.container.find("input[name='clientID']").prop("disabled", true);


                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('ProgressClientActiveGridView:getData');

                var grid = this.$("#client").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {

                    var records = self.dataSource.data();
                    $.each(records, function (index, value) {

                        value.employeeID = self.myEmployeeId;
                        value.serviceID = 1004;
                        value.timeWorked = 0;
                        value.workDate = moment(new Date()).clone().format('YYYY-MM-DD');
                        value.timerNote = "";

                    });

                    grid.setDataSource(self.dataSource);

                    self.$(".k-grid-importTimecardRecords").click(function (e) {
                        console.log('ProgressClientActiveGridView:getData');
                        self.openFileDialog(e);
                    });
                });

            },
            openFileDialog: function (e) {
                console.log('ProgressClientActiveGridView:openFileDialog');
                var self = this;


                // Get files
                var input = document.createElement("input");
                input.type = "file";
                input.accept = ".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel";
                //input.addEventListener("change", self.importTimecardRecords(e));

                input.onchange = function (e) {
                    console.log('ProgressClientActiveGridView:openFileDialog:Click OK');

                    if (confirm("This will load a manual timer record for all active clients with the timeWorked values you entered.  Are you sure you want to continue?")) {
                        // getting a hold of the file reference
                        var file = e.target.files[0];

                        // setting up the reader
                        var reader = new FileReader();

                        // here we tell the reader what to do when it's done reading...
                        reader.onload = function (readerEvent) {
                            var data = readerEvent.target.result; // this is the content!
                            var workbook = XLSX.read(data, {
                                type: 'binary'
                            });
                            self.importTimecardRecords(workbook);
                        };
                        reader.readAsBinaryString(file);
                    }
                };

                if ( document.createEvent ) {
                    console.log('ProgressClientActiveGridView:openFileDialog:document.createEvent');
                    var event = document.createEvent("MouseEvents");
                    event.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    input.dispatchEvent(event);

                    console.log('ProgressClientActiveGridView:openFileDialog:document.createEvent:2');
                } else {
                    console.log('ProgressClientActiveGridView:openFileDialog:non-document.createEvent');
                    var event2 = new MouseEvent("click");
                    input.dispatchEvent(event2);
                }


            },
            importTimecardRecords: function (workbook) {
                console.log('ProgressClientActiveGridView:importTimecardRecords');
                var self = this;

                var xls;
                //$.each(workbook.SheetNames, function (sheetName) {
                workbook.SheetNames.forEach(function (sheetName) {
                    xls = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    //console.log('ProgressClientActiveGridView:importTimecardRecords:sheet data:' +JSON.stringify(xls));
                    return false;
                });

                this.timerDataSource = new kendo.data.DataSource(
                    {
                        batch: false,
                        pageSize: 600,
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalTimer",
                                complete: function (e) {
                                    console.log('ProgressClientActiveGridView:timerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientActiveGridView:timerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('ProgressClientActiveGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.timerID + ")";
                                },
                                complete: function (e) {
                                    //console.log('ProgressClientActiveGridView:timerDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                },
                                error: function (e) {
                                    var message = 'ProgressClientActiveGridView:timerDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('ProgressClientActiveGridView:create:data:' + data);
                                    return App.config.DataServiceURL + "/odata/PortalTimer";
                                },
                                complete: function (e) {
                                    if (e.responseJSON.value[0]) {
                                        //var newTimerID = e.responseJSON.value[0].timerID;
                                        //console.log('ProgressClientActiveGridView:timerDataSource:create:complete:' + newTimerID);
                                    } else {
                                        var message = 'ProgressClientActiveGridView:timerDataSource:create:complete:no record';
                                        console.log(message);
                                    }
                                },
                                error: function (e) {
                                    var message = 'ProgressClientActiveGridView:timerDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    idTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    entryType: {editable: true, type: "string", validation: {required: false}},
                                    employeeID: {editable: true, type: "number"},
                                    clientID: {editable: true, type: "number"},
                                    serviceID: {editable: true, type: "number"},
                                    workDate: {editable: true, type: "string", validation: {required: false}},
                                    startTime: {
                                        editable: true,
                                        type: "date",
                                        format: "{0:s}"
                                    },
                                    //format:"{0:YYYY-MM-DD HH:mm:ss.SSS}"},
                                    //startTime: {editable: true, type: "string", validation: {required: false}},
                                    stopTime: {editable: true, type: "date", validation: {required: false}},
                                    timeWorked: {editable: true, type: "number"},
                                    timeWorkedText: {editable: true, type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    intTimerID: {editable: true, type: "number"},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    editTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    editEmployeeID: {editable: true, type: "number"},
                                    tasksCalls: {editable: true, type: "number"},
                                    tasksEmails: {editable: true, type: "number"},
                                    tasksVoiceMails: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "timerID", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('ProgressClientActiveGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#clientLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('ProgressClientActiveGridView:dataSource:request end:');
                            kendo.ui.progress(self.$("#clientLoading"), false);
                        },
                        error: function (e) {
                            //console.log('ProgressClientActiveGridView:dataSource:error');
                            kendo.ui.progress(self.$("#clientLoading"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('ProgressClientActiveGridView:change');

                        }
                    });

                var offset = (new Date()).getTimezoneOffset();
                self.now = new Date(moment().clone().subtract((offset), 'minutes'));
                var note = "Load of manual data on: " + self.now;
                kendo.ui.progress(self.$("#clientLoading"), true);
                this.timerDataSource.fetch(function () {

                    $.each(xls, function (index, value) {
                        self.timerDataSource.add({
                            idTimeStamp: self.now,
                            employeeID: value.employeeID,
                            serviceID: value.serviceID,
                            clientID: value.clientID,
                            workDate: value.workDate,
                            timeWorked: parseFloat(value.timeWorked),
                            timerNote: value.timerNote,
                            entryType: "M",
                            recordType: "NORM-STOP",
                            tasks: 0,
                            meetings: 0,
                            timerAction: "none",
                            mgrNote: note
                        });

                    });

                    $.when(self.timerDataSource.sync()).done(function (e) {
                        self.updateTimerRecordWithTimerId(note);
                    });

                });
            },
            updateTimerRecordWithTimerId: function (note) {

                var self = this;
                console.log('ProgressClientActiveGridView:updateTimerRecordWithTimerId:Manual records only:note:' + note);

                //timerID = parseInt(timerID, 0);
                self.timerDataSource.filter({field: "mgrNote", operator: "eq", value: note});
                kendo.ui.progress(self.$("#clientLoading"), true);
                self.timerDataSource.fetch(function () {
                    var data = this.data();

                    console.log('ProgressClientActiveGridView:updateTimerRecordWithTimerId:data:length:' + data.length);
                    $.each(data, function (index, value) {
                        //console.log('ProgressClientActiveGridView:updateTimerRecordWithTimerId:Manual records only:timerID:' + timerID);
                        value.set("intTimerID", value.timerID);
                        value.set("idTimeStamp", self.now);
                    });

                    $.when(this.sync()).done(function (e) {
                        self.calculateProgressDataNew();
                    });
                });

            },
            calculateProgressDataNew: function () {
                console.log('ProgressClientActiveGridView:calculateProgressDataNew');
                var self = this;

                var workStartDate = moment().clone().format('YYYY-MM-DD'); //"2016-11-01";
                var workCurrentDate = moment().clone().format('YYYY-MM-DD'); //"2016-11-15";

                this.restDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        transport: {
                            read: function (options) {
                                $.ajax({
                                    type: "GET",  //PATCH?
                                    contentType: "application/json",
                                    url: App.config.DataServiceURL + "/rest/GetCalcClientStats/all/" + workStartDate + "/" + workCurrentDate,
                                    dataType: "json",
                                    requestStart: function (e) {
                                        kendo.ui.progress(self.$("#clientLoading"), true);
                                    },
                                    requestEnd: function (e) {
                                        //kendo.ui.progress(self.$("#clientLoading"), false);
                                    },
                                    success: function (result) {
                                        //kendo.ui.progress(self.$("#clientLoading"), false);
                                        console.log('ProgressClientActiveGridView:restDataSource:success:calculateProgressData:Stats calculation has finished for all clients');
                                    },
                                    error: function (result) {
                                        // notify the data source that the request failed
                                        console.log('ProgressClientActiveGridView:restDataSource:error');
                                        kendo.ui.progress(self.$("#clientLoading"), false);
                                    }
                                });
                            }
                        }
                    });

                $.when(self.restDataSource.fetch()).done(function (e) {
                    kendo.ui.progress(self.$("#clientLoading"), false);
                });

            },
            companyFilter: function (container) {
                var self = this;

                //console.log('ProgressClientActiveGridView:companyFilter:' + JSON.stringify(container));
                self.companyDataSource.fetch(function (data) {

                    var dataSource = [];
                    var records = this.data();

                    $.each(records, function (index, value) {
                        dataSource.push({value: value.clientCompany, text: value.clientCompany});
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: dataSource,
                        optionLabel: "Select Value"
                        //change: self.onCompanyFilterChanged
                    });
                });
            },
            viewCompanyProfile: function (e) {
                //console.log('ProgressClientActiveGridView:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientActiveGridView:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId', companyId);

                App.router.navigate('companyProfile', {trigger: true});

            },
            viewClientProfile: function (e) {
                //console.log('ProgressClientActiveGridView:viewClientProfile:e:' + e.target);

                var clientId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    clientId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    clientId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientActiveGridView:viewClientProfile:e:' + clientId);
                App.model.set('selectedClientId', clientId);


                App.router.navigate('clientProfile', {trigger: true});

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressClientActiveGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#client').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressClientActiveGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#client').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressClientActiveGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#client").parent().parent().height();
                this.$("#client").height(parentHeight);

                parentHeight = parentHeight - 48;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressClientActiveGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressClientActiveGridView:resize:headerHeight:' + headerHeight);
                this.$("#client").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressClientActiveGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#client").data('ui-tooltip'))
                    this.$("#client").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });