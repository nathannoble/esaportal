define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/fileWidget', 'views/FileCollectionView',
        'kendo/kendo.data','date'],
    //'kendo/kendo.binder'
    function (App, Backbone, Marionette, $, template, FileCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                fileList: '#file-list'
            },
            events: {
                // 'click #NewFileButton': 'newFileButtonClicked',
                'dragenter #dragDropHandler': 'onDragEnter',
                'dragover #dragDropHandler': 'onDragOver',
                'drop #dragDropHandler': 'onDrop',
                'dragenter #noDropZone': 'onDragEnterView',
                'dragover #noDropZone': "onDragOverView",
                'drop #noDropZone': 'onDropView'
            },
            initialize: function (options) {

                var self = this;
                var app = App;

                this.viewModel = kendo.observable({
                    isChecked: false
                });

                //_.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.options = options;

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Client"};
                }

                if (this.options.type === "Client") {
                    this.id = parseInt(App.model.get('selectedClientId'),0); //2189;
                } else if (this.options.type === "Company") {
                    this.id = parseInt(App.model.get('selectedCompanyId'),0); //4;
                } else if (this.options.type === "Employee") {
                    this.id = parseInt(App.model.get('selectedEmployeeId'),0); //100;
                }

                //console.log('FileWidgetView:initialize:type:' + this.options.type);

                
            },
            onRender: function () {
                //console.log('FileWidgetView:initialize');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('FileWidgetView:onShow');

                this.listenTo(App.vent, App.Events.ModelEvents.ResetFileList, this.onResetFileList);

                // Setup the containers
                this.resize();

                var self = this;

                // Setup tooltips - if we're on the desktop
                if (!App.mobile) {
                    this.$('#dragDropHandler').tooltip();
                }

                this.$("#dragDropStatus").html("<p>Ready</p>");

                this.fileList.show(new FileCollectionView({type: self.options.type}));
            },
            // newFileButtonClicked: function (e) {
            //     console.log('FileWidgetView:newFileButtonClicked');
            //
            //     var self = this;
            //
            //     this.handleFileUpload(files,this.$('#dragDropHandler'));
            //
            // },
            onDragEnter: function(e){
                //console.log('FileWidgetView:onDragEnter');

                this.$('#dragDropHandler').css('border', '4px solid #61C732');
                this.$('#dragDropHandler').css('background-color', '#61C732');
                this.$('#dragDropHandler').css('color', '#B5E89E');

                e.stopPropagation();
                e.preventDefault();
            },
            onDragOver: function(e){
                //console.log('FileWidgetView:onDragOver');

                e.stopPropagation();
                e.preventDefault();
            },
            onDrop: function(e){
                //console.log('FileWidgetView:onDrop');

                this.$('#dragDropHandler').css('border', '2px dotted #0B85A1');
                this.$('#dragDropHandler').css('background-color', '#fafafa');
                this.$('#dragDropHandler').css('color', '#92AAB0');

                e.preventDefault();
                var files = e.originalEvent.dataTransfer.files;

                //We need to send dropped files to Server
                this.handleFileUpload(files,this.$('#dragDropHandler'));
            },
            onDragEnterView: function(e) {
                //console.log('FileWidgetView:onDragEnterView');

                this.$('#dragDropHandler').css('border', '2px dotted #0B85A1');
                this.$('#dragDropHandler').css('background-color', '#fafafa');
                this.$('#dragDropHandler').css('color', '#92AAB0');

                e.stopPropagation();
                e.preventDefault();
            },
            onDragOverView: function(e) {
                //console.log('FileWidgetView:onDragOverView');

                this.$('#dragDropHandler').css('border', '2px dotted #0B85A1');
                this.$('#dragDropHandler').css('background-color', '#fafafa');
                this.$('#dragDropHandler').css('color', '#92AAB0');

                e.stopPropagation();
                e.preventDefault();
            },
            onDropView: function(e) {
                //console.log('FileWidgetView:onDropView');

                this.$('#dragDropHandler').css('border', '2px dotted #0B85A1');
                this.$('#dragDropHandler').css('background-color', '#fafafa');
                this.$('#dragDropHandler').css('color', '#92AAB0');

                e.stopPropagation();
                e.preventDefault();
            },
            handleFileUpload: function(files,divObj) {
                //console.log('FileWidgetView:onDropView');
                var self = this;

                //hide the dropbox
                if (!this.isUploadingFile) {

                    //this.$('#dragDropHandler').hide();
                    this.$('#dragDropHandler').slideUp(200);

                    //this.isUploadingFile = true;
                    //App.model.set('uploadingSpreadCSV', true);
                    if (self.$(".form-control").val() !== "") {
                        setTimeout(function () {
                            self.uploadFiles(files);
                        }, 0);
                    }
                    else {
                        this.createAutoClosingAlert(self.$("#fileError"), "<strong>No file to upload.</strong> Choose file to upload.",5000);
                    }
                }
            },
            uploadFiles: function (files) {
                console.log('FileWidgetView:uploadFiles:NumberOfFiles: ' + files.length);

                kendo.ui.progress(this.$("#noDropZone"), true);
                var self = this;
                var app = App;

                // Loop through each file
                //var files = this.$('#imageFiles')[0].files;

                // IE9 case...only 1 file can be selected and can't use "FormData" object
                if (files === undefined) {

                    this.files = document.getElementById("imageFiles");
                    //var response = $('#imageFiles').contents().find('body').text();
                    this.iframe = document.createElement("frame");
                    this.iframe.setAttribute("id", "upload_iframe_myFile");
                    this.iframe.setAttribute("name", "upload_iframe_myFile");
                    this.iframe.setAttribute("width", "0");
                    this.iframe.setAttribute("height", "0");
                    this.iframe.setAttribute("border", "0");
                    //this.iframe.setAttribute("src", "javascript:false;");
                    this.iframe.style.display = "none";

                    if (this.iframe.addEventListener) {
                        this.iframe.addEventListener("load", this.fileLoaded, true);
                    }
                    else  {
                        this.iframe.attachEvent("onload", this.fileLoaded);
                    }

                    this.form = document.createElement("form");
                    this.form.setAttribute("id", "upload_form_myFile");
                    this.form.setAttribute("target", this.iframe.id);  //"upload_iframe_myFile"
                    this.form.setAttribute("action", App.config.DataServiceURL + "/rest/files/Upload");
                    this.form.setAttribute("method", "post");
                    this.form.setAttribute("enctype", "multipart/form-data");
                    this.form.setAttribute("encoding", "multipart/form-data");

                    this.form.style.display = "none";
                    this.form.appendChild(this.files);

                    this.$el.append(this.form);
                    this.$el.append(this.iframe);

                    this.form.submit();
                }
                else {

                    //console.log('FileWidgetView:uploadFiles:files: ' + JSON.stringify(files));

                    console.log(files);
                    $.each(files, function (index, value) {
                        var formData = new FormData();
                        formData.append("opmlFile", value);
                        formData.append("type", self.options.type);
                        formData.append("id", self.id);
                        $.ajax({
                            url: App.config.DataServiceURL + "/rest/files/Upload",
                            // url: "http://199.115.221.114:64859/rest/files/Upload",
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function () {
                                console.log("FileWidgetView:uploadFiles:Successfully uploaded file.");
                                kendo.ui.progress(self.$("#noDropZone"), false);
                                self.createAutoClosingAlert(self.$("#fileSuccess"), "<strong>Success.</strong> File upload was successful.",2500);

                                self.$(".form-control").val("");

                                self.fileList.show(new FileCollectionView({type: self.options.type}));
                            },
                            error: function (err) {
                                //console.log(err);
                                console.log("FileWidgetView:uploadFiles:Error: " + err);
                                kendo.ui.progress(self.$("#noDropZone"), false);
                                var errorToShow = "File upload failed.";
                                if (err.responseText && err.responseText.length > 0){
                                    errorToShow = err.responseText;
                                    //parse the leading and trailing double quotes from the string
                                    errorToShow = errorToShow.substring(1,errorToShow.length-1);
                                }
                                self.createAutoClosingAlert(self.$("#fileError"), "<strong>Failed.</strong> " + errorToShow,5000);

                            }
                        });
                    });
                }
            },
            fileLoaded: function () {
                if (this.iframe.removeEventListener) {
                    this.iframe.removeEventListener("load", this.fileLoaded, false);
                }
                else  {  //(this.iframe.detachEvent)
                    this.iframe.detachEvent("onLoad", this.fileLoaded);
                }

                response = this.getIFrameContentJSON(this.iframe);

                kendo.ui.progress(self.$("#noDropZone"), false);
                this.createAutoClosingAlert(self.$("#fileSuccess"), "<strong>Success.</strong> File upload was successful.",2500);

                this.$(".form-control").val("");

                // Delete the iframe and form
                setTimeout(this.$('#upload_form_myFile').remove(),10);
                setTimeout(this.$('#upload_iframe_myFile').remove(),10);

            },
            createAutoClosingAlert: function (selector, html, msDuration) {
                // show the alert
                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                var dz = this.$("#dragDropHandler");

                //this.isUploadingFile = false;
                //App.model.set('uploadingSpreadCSV', false);

                //wait five seconds and close the alert
                window.setTimeout(function () {
                    dz.slideDown(500);
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, msDuration);
            },
            onResetFileList: function () {

                this.fileList.show(new FileCollectionView({type: this.options.type}));

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('FileWidgetView:resize');

            },
            remove: function () {
                //console.log('-------- FileWidgetView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });