define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/phoneDirectoryWidget',
        'views/PhoneCollectionView', 'kendo/kendo.data', 'date'],
    //'kendo/kendo.binder'
    function (App, Backbone, Marionette, $, template, PhoneCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'wheel': 'onMouseWheel',
                'click #viewPhoneDirectoryButtonCollapse': 'onViewPhoneDirectoryButtonCollapseClicked'
            },
            regions: {
                phoneList: '#phone-list'
            },
            initialize: function (options) {
                //console.log('PhoneDirectoryWidgetView:initialize');

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Client"};
                }

                this.viewModel = kendo.observable({
                    isChecked: false
                });
                this.userKey = parseInt(App.model.get('userKey'), 0);  //10268;

                this.clientID = parseInt(App.model.get('selectedClientId'), 0); //2189;
                this.employeeID = parseInt(App.model.get('selectedEmployeeId'), 0); //100;
                this.companyID = parseInt(App.model.get('selectedCompanyId'), 0); //4;

                this.tileViewPhoneDirectory = App.model.get('tileViewPhoneDirectory');

                this.firstName = "First";
                this.lastName = "Last";

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                //console.log('PhoneDirectoryWidgetView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('PhoneDirectoryWidgetView:onShow');

                // Setup the containers
                this.resize();

                var self = this;

                if (this.tileViewPhoneDirectory === null) {
                    this.phoneList.show(new PhoneCollectionView({type: this.options.type}));
                } else {
                    // Change to "+"
                    self.$('#viewPhoneDirectoryIconCollapse').removeClass('fa-minus');
                    self.$('#viewPhoneDirectoryIconCollapse').addClass('fa-plus');
                    self.$('.view-phone-directory-box').addClass('collapsed-box');
                }
            },
            onViewPhoneDirectoryButtonCollapseClicked: function (e) {
                console.log('PhoneDirectoryWidgetView:onViewPhoneDirectoryButtonCollapseClicked');
                var self = this;
                var tileViewPhoneDirectory = null;
                if (e.target.classList.toString().indexOf('plus') > -1 || e.target.innerHTML.indexOf('plus') > -1) {

                    // Change to "-"
                    //self.$('#viewPhoneDirectoryIconCollapse').removeClass('fa-plus');
                    //self.$('#viewPhoneDirectoryIconCollapse').addClass('fa-minus');
                    //self.$('.view-phone-directory-box').removeClass('collapsed-box');

                    // view
                    this.phoneList.reset();
                    this.phoneList.show(new PhoneCollectionView({type: this.options.type}));

                    // expanded
                    App.model.set('tileViewPhoneDirectory', tileViewPhoneDirectory);
                } else {

                    // Change to "+"
                    //self.$('#viewPhoneDirectoryIconCollapse').removeClass('fa-minus');
                    //self.$('#viewPhoneDirectoryIconCollapse').addClass('fa-plus');
                    //self.$('.view-phone-directory-box').addClass('collapsed-box');

                    // collapsed
                    tileViewPhoneDirectory = "collapsed";
                    App.model.set('tileViewPhoneDirectory', tileViewPhoneDirectory);
                }

                this.tileViewPhoneDirectory = tileViewPhoneDirectory;
            },

            onMouseWheel: function (e) {
                //console.log('PhoneDirectoryWidgetView:onMouseWheel');
                e.preventDefault();
                var event = e.originalEvent;
                var d = event.wheelDelta || -event.detail;
                var change = ( d < 0 ? 1 : -1 ) * 32;
                $('html, body #dashboard-section').stop().animate({
                    scrollTop: "+=" + change  //: $("#elementtoScrollToID").offset().top
                }, 1);
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PhoneDirectoryWidgetView:resize');

            },
            remove: function () {
                //console.log('-------- PhoneDirectoryWidgetView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });