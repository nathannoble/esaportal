define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/welcomeESAMedia'],
    function (App, Backbone, Marionette, _, $, template ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            initialize: function () {
                //console.log('WelcomeESAMediaView:initialize');

                _.bindAll(this);

                this.news = [];

            },
            getData: function () {

                var self = this;

                this.adminDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalAdminData";
                            },
                            complete: function (e) {
                                console.log('WelcomeESAMediaView:adminDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'WelcomeESAMediaView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {editable: false, type: "number"},
                                text: {editable: true, type: "string"},
                                shortText: {editable: true, type: "string"},
                                text2: {editable: true, type: "string"},
                                title: {editable: true, type: "string"},
                                fileName: {editable: true, type: "string"},
                                externalLink: {editable: true, type: "string"},
                                category: {editable: true, type: "number"},
                                subCategory: {editable: true, type: "number"},
                                priority: {editable: true, type: "number"},
                                showAsCompanyMessage:{editable: true, type: "boolean"},
                                addUserImage:{editable: true, type: "boolean"},
                                timeStamp: {editable: true,type: "date"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   WelcomeESAMediaView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    WelcomeESAMediaView:messageDataSource:requestEnd');
                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   WelcomeESAMediaView:messageDataSource:error');
                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   WelcomeESAMediaView:messageDataSource:change');

                        var data = this.data();
                        
                    },
                    sort: [{field: "timeStamp", dir: "desc"},{field: "priority", dir: "asc"} ],
                    filter: [
                        {field: "showAsCompanyMessage", operator: "eq", value:true},
                        {field: "category", operator: "eq", value: 1}
                    ]
                });

                this.adminDataSource.fetch(function () {

                    var data = this.data();
                    $.each(data, function (index, value) {

                        self.news.push({
                            ID:value.ID,
                            fileName:value.fileName,
                            title:value.title,
                            shortText:value.shortText,
                            text:value.text,
                            text2:value.text2,
                            subCategory: value.subCategory
                        });

                    });

                    //self.news.sort(function(a,b){
                    //    var c = a.priority;
                    //    var d = b.priority;
                    //    return d-c;
                    //});

                    //console.log('WelcomeESAMediaView:getData:news:' + JSON.stringify(self.news));

                    if (self.news.length > 0) {
                        self.ID = self.news[0].ID;
                        self.fileName = self.news[0].fileName;
                        self.title = self.news[0].title.trim();
                        self.shortText = self.news[0].shortText.trim();
                        self.text = self.news[0].text.trim();
                        self.text2 = self.news[0].text2.trim();
                        self.subCategory = self.news[0].subCategory;

                        self.$('#title').html(self.title);
                        self.$('#newsImage').attr('src', App.config.PortalFiles + "/IndustryNews/" + self.fileName);
                    }
                });
            },

            onRender: function () {
//                //console.log('WelcomeESAMediaView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- WelcomeESAMediaView:onShow --------');

                //this.getData();

            },
            onResize: function () {
//                //console.log('WelcomeESAMediaView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            createAutoClosingAlert: function (selector, html, msDuration) {
                // show the alert
                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                var dz = $("#dragDropHandler");

                //this.isUploadingBio = false;
                //App.model.set('uploadingSpreadCSV', false);

                //wait five seconds and close the alert
                window.setTimeout(function () {
                    dz.slideDown(500);
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, msDuration);
            },
            resize: function () {
//                //console.log('WelcomeESAMediaView:resize');

            },
            remove: function () {
                //console.log('-------- WelcomeESAMediaView:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
