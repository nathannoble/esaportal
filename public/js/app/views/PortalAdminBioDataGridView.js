define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalAdminBioDataGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data','kendo/kendo.upload'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalAdminBioDataGridView:initialize');

                _.bindAll(this);

                var self = this;

                self.priorities = [
                    {text: '1', value: 1},
                    {text: '2', value: 2},
                    {text: '3', value: 3},
                    {text: '4', value: 4},
                    {text: '5', value: 5}
                ];

                self.subCategories = [
                    {text: 'Corporate Leadership', value: 0},
                    {text: 'Executive Team', value: 1},
                    {text: 'Senior Management', value: 2},
                    {text: 'Account Managers', value: 3},
                    {text: 'Assistant Account Managers', value: 4},
                    {text: 'Employees', value: 5}
                ];

                this.adminBioGridFilter = App.model.get('adminBioGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        autoSync: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                complete: function (e) {
                                    console.log('PortalAdminBioDataGridView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminBioDataGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalAdminBioDataGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminBioDataGridView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminBioDataGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalAdminBioDataGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminBioDataGridView:dataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminBioDataGridView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalAdminBioDataGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminBioDataGridView:dataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminBioDataGridView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    title: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Title Here)"
                                    },
                                    text: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Description Here)"
                                    },
                                    text2: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Fun Fact Here)"
                                    },
                                    shortText: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Name Here)"
                                    },
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {editable: true, type: "string"},
                                    category: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:8
                                    },
                                    subCategory: {editable: true, type: "number"},
                                    priority: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:1
                                    },
                                    showAsCompanyMessage: {editable: true, type: "boolean"},
                                    addUserImage: {editable: true, type: "boolean"},
                                    timeStamp: {
                                        editable: true,
                                        type: "date",
                                        defaultValue: function () {
                                            var offset = (new Date()).getTimezoneOffset();
                                            var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                            return now;
                                        }
                                    }
                                }
                            }
                        },
                        sort: {field: "shortText", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('PortalAdminBioDataGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#adminBioDataLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalAdminBioDataGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#adminBioDataLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalAdminBioDataGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalAdminBioDataGridView:dataSource:error');
                            kendo.ui.progress(self.$("#adminBioDataLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalAdminBioDataGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#client').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onRender: function () {
                //console.log('PortalAdminBioDataGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminBioDataGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#adminBioData").kendoGrid({

                    toolbar: ["create"],
                    editable: true,
                    selectable: "cell",
                    navigatable: true,
                    sortable: true,
                    extra: false,
                    filterable: {
                        mode: "row"
                    },
                    resizable: true,
                    reorderable: true,
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.ID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "subCategory",
                            title: "Role",
                            template: function (e) {

                                if (e.subCategory || e.subCategory === 0) {
                                    return self.subCategories[e.subCategory] ? self.subCategories[e.subCategory].text : "";
                                } else {
                                    return "";
                                }

                            },
                            width: 175,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.subCategoryFilter
                                }
                            },
                            editor: function(container, options) {
                                var input = $("<input/>");
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoDropDownList({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: [
                                        {text: 'Corporate Leadership', value: 0},
                                        {text: 'Executive Team', value: 1},
                                        {text: 'Senior Management', value: 2},
                                        {text: 'Account Managers', value: 3},
                                        {text: 'Assistant Account Managers', value: 4},
                                        {text: 'Employees', value: 5}
                                    ]
                                });
                            }
                        },
                        {
                            field: "shortText",
                            title: "Name",
                            width: 75,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },
                        {
                            field: "title",
                            title: "Title",
                            width: 150,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },
                        {
                            field: "text2",
                            title: "Fun Fact",
                            width: 310,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },

                        {
                            field: "text",
                            title: "Text",
                            width: 310,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },

                        {
                            field: "fileName",
                            title: "Photo",
                            template: function (e) {
                                var template = "<div style='text-align: center;cursor:pointer'><a>" + e.fileName + "</a></div>";
                                if (e.fileName === "" || e.fileName === null || e.fileName === undefined) {
                                    template = "<div style='text-align: center;cursor:pointer'><a>" + "Upload Image..." + "</a></div>";
                                }
                                return template;
                            },
                            width: 300,
                            filterable: false,
                            editor: function(container, e) {
                                var input = $('<input name="fileName" id="fileName" type="file" aria-label="files" style="width: 300px"/>');
                                input.appendTo(container);

                                console.log('PortalAdminSocialMediaDataGridView:def:onShow:e:' + JSON.stringify(e));

                                var files = [{
                                    name: e.model.fileName,
                                    size: 600,
                                    extension: ""
                                }];

                                input.kendoUpload({
                                    success: self.onSuccess,
                                    remove: self.onRemove,
                                    async: {
                                        withCredentials: false,
                                        saveUrl: App.config.DataServiceURL + "/rest/files/AdminUpload/EmployeeBios",
                                        removeUrl: App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/EmployeeBios"
                                    },
                                    multiple: false,
                                    files: e.model.fileName !== "" ? files : []
                                });
                            }
                        },
                        {
                            field: "showAsCompanyMessage",
                            title: "Show on <br/>Welcome Page",
                            template: function (e) {
                                var template;
                                if (e.showAsCompanyMessage === false) {
                                    template = "<div style='text-align: center'>No</div>";
                                    return template;
                                } else {
                                    template = "<div style='text-align: center'>Yes</div>";
                                    return template;
                                }
                            },
                            width: 110,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $("<input/>");
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoDropDownList({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: [
                                        {text: 'Yes', value: 'true'},
                                        {text: 'No', value: 'false'}
                                    ]
                                });
                            }
                        },
                        {

                            field: "timeStamp",
                            title: "Time Stamp",
                            width: 100,
                            format: "{0:M/d/yyyy}",
                            filterable: false,
                            editable: false
                        }

                    ],
                    change: function (e) {
                        console.log('PortalAdminBioDataGridView:def:onShow:onChange:e.model:' + JSON.stringify(e.model));
                    },
                    save: function (e) {
                        e.model.dirty = true;
                        e.model.title = e.model.title.trim();
                        e.model.text = e.model.text.trim();
                        e.model.text2 = e.model.text2.trim();
                        e.model.externalLink = e.model.externalLink.trim();
                        e.model.shortText = e.model.shortText.trim();
                        //e.model.timeStamp.setHours(0);
                        console.log('PortalAdminBioDataGridView:def:onShow:onSave:e.model:' + JSON.stringify(e.model));
                    },
                    edit: function (e) {
                        console.log('PortalAdminBioDataGridView:onShow:edit');

                        // Disable the ID editor
                        //e.container.find("input[name='ID']").prop("disabled", true);

                        // hard code category to employee bios
                        e.model.category = 8;

                        self.fileName = e.model.fileName;
                        console.log('PortalAdminBioDataGridView:onShow:edit:fileName:' + self.fileName);
                        
                        self.model = e.model;
                        
                    },
                    dataBound: function (e) {
                        console.log("PortalAdminBioDataGridView:def:onShow:dataBound");

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;

                            var adminBioGridFilter = [];

                            $.each(filters, function (index, value) {
                                adminBioGridFilter.push(value);
                            });

                            console.log("PortalAdminBioDataGridView:def:onShow:dataBound:set adminBioGridFilter:" + JSON.stringify(adminBioGridFilter));
                            app.model.set('adminBioGridFilter', adminBioGridFilter);
                        }

                        console.log("PortalAdminBioDataGridView:def:onShow:dataBound:re-sort by date");

                        $('.k-grid-add').unbind("click");
                        $('.k-grid-add').bind("click", function(){
                            console.log("PortalAdminBioDataGridView:def:onShow:dataBound:Add");
                            var grid = self.$("#adminBioData").data("kendoGrid");
                            grid.dataSource.sort({field: "timeStamp", dir: "desc"});
                        });

                    }
                });

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            onRemove:  function (e) {
                if (e && e.sender) {
                    console.log('PortalAdminBioDataGridView:onRemove:');
                    e.sender.options.async.removeUrl = App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/EmployeeBios/" + this.fileName + "/";
                    this.model.fileName = "";
                    this.model.dirty = true;
                    var grid = $("#adminBioData").data("kendoGrid");
                    grid.saveChanges();
                    console.log('PortalAdminBioDataGridView:onRemove:removeUrl:' + e.sender.options.async.removeUrl);
                }
            },
            onSuccess:  function (e) {
                //if (e && e.response) {
                    console.log('PortalAdminBioDataGridView:onSuccess:response:' + e.response);
                    //console.log('PortalAdminBioDataGridView:onSuccess:category:' + this.category);
                    console.log('PortalAdminBioDataGridView:onSuccess:ID:' + this.model.ID);
                    if (e.response.indexOf("Exception") > -1) {
                    } else if (e.response.indexOf("Deleted file") > -1) {
                    } else {
                        this.fileName = e.response;
                        this.model.fileName = e.response;
                        this.model.dirty = true;
                        console.log('PortalAdminBioDataGridView:onSuccess:fileName changed');
                        var grid = $("#adminBioData").data("kendoGrid");
                        grid.saveChanges();
                    }
                    //else {
                    //    createAutoClosingAlert($("#startError"), "<strong>File Error</strong>" + e.response);
                    //}
                    console.log('PortalAdminBioDataGridView:onSuccess:model:' + JSON.stringify(this.model));

                //}
            },
            createAutoClosingAlert:  function (selector, html)  {
                console.log('PortalAdminBioDataGridView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 50000);
            },
            subCategoryFilter: function (container) {

                //console.log('PortalAdminBioDataGridView:categoryFilter:' + JSON.stringify(container));

                container.element.kendoDropDownList({
                    dataValueField: "value",
                    dataTextField: "text",
                    valuePrimitive: true,
                    dataSource: this.subCategories,
                    optionLabel: "Select Value"
                });

            },
            getData: function () {
                //console.log('PortalAdminBioDataGridView:getData');

                //var state = {};
                //state.page = 1;
                //state.pageSize = 40;

                var grid = this.$("#adminBioData").data("kendoGrid");

                //console.log('PortalAdminBioDataView:getData:filter:' + JSON.stringify(state.filter));

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);

                    grid.dataSource.filter({field: "category", operator: "eq", value: 8});

                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalAdminBioDataGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#adminBioData").parent().parent().height();
                this.$("#adminBioData").height(parentHeight);

                parentHeight = parentHeight - 95; //65

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalAdminBioDataGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalAdminBioDataGridView:resize:headerHeight:' + headerHeight);
                this.$("#adminBioData").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalAdminBioDataGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#adminBioData").data('ui-tooltip'))
                    this.$("#adminBioData").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });