define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/header','views/WelcomeLayoutView','views/DocsLayoutView',
        'views/AsideView','views/FooterView','views/GeneralCalloutWidgetView','views/CalloutWidgetView',
        'adminLTE', 'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data','kendo/kendo.upload','moment'],
    function (App, Backbone, Marionette, $, template,WelcomeLayoutView,DocsLayoutView,AsideView,
              FooterView,GeneralCalloutWidgetView,CalloutWidgetView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            events: {
                'click #signOut': 'onSignOutClicked',
                'click #toggleMenu': 'onToggleMenuClicked',
                'click #userImageLarge': 'onUserImageClicked'
            },
            regions: {
                callbackMessageRegion: "#callback-alert"
            },
            template: template,
            initialize: function () {
                //console.log('HeaderView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                var self = this;

                self.userKey = App.model.get('userKey');
                self.userLogin = App.model.get('userLogin');
                self.userType = App.model.get('userType');
                //self.chatMessageDismissed = App.model.get('chatMessageDismissed');

                self.now = new Date();
                self.now.setHours(23);

                self.today = moment().clone().format('YYYY-MM-DD');

                this.listenTo(App.vent, App.Events.ModelEvents.CallbackErrorMessageChanged, this.onCallbackErrorMessageChanged);

                if (self.userKey) {
                    self.userKey = parseInt(self.userKey, 0);
                    this.employeeDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                    complete: function (e) {
                                        console.log('HeaderView:employeeDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'HeadertView:employeeDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "employeeID",
                                    fields: {
                                        employeeID: {editable: false, type: "number"},
                                        teamLeaderID: {editable: false, type: "number"},
                                        userKey: {editable: true, type: "string", validation: {required: false}},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        status: {type: "string", validation: {required: false}},
                                        timerManual: {editable: true, type: "boolean"},
                                        timeZoneID: {editable: false, type: "number"},
                                        isTeamLeader: {editable: true, type: "boolean"},
                                        emailAddress: {editable: true, type: "string", validation: {required: false}},
                                        homePhone: {editable: true, type: "string", validation: {required: false}},
                                        esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                        esaTollFree: {editable: true, type: "string", validation: {required: false}},
                                        hidePlans: {editable: true, type: "boolean"},
                                        hideStatsTimecard: {editable: true, type: "boolean"},
                                        hideLevel1Employees: {editable: true, type: "boolean"},
                                        hideClients: {editable: true, type: "boolean"},
                                        hideCompanies: {editable: true, type: "boolean"}
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            // sort: {field: "lastName", dir: "asc"},
                            filter: {field: "userKey", operator: "eq", value: self.userKey}
                        });

                    this.chatDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            batch: false,
                            pageSize: 10,
                            serverPaging: true,
                            serverSorting: true,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                    complete: function (e) {
                                        console.log('HeaderView:chatDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'HeadertView:chatDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ChatWidgetView:update');
                                        return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('HeaderView:chatDataSource:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'HeadertView:chatDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('ChatWidgetView:create');
                                        return App.config.DataServiceURL + "/odata/PortalAdminData";
                                    },
                                    complete: function (e) {
                                        console.log('HeaderView:chatDataSource:create:complete');
                                    },
                                    error: function (e) {
                                        var message = 'HeadertView:chatDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('ChatWidgetView:destroy');
                                        return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('HeaderView:chatDataSource:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'HeadertView:chatDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "ID",
                                    fields: {
                                        ID: {editable: false, type: "number"},
                                        text: {editable: true, type: "string"},
                                        shortText: {editable: true, type: "string"},
                                        text2: {editable: true, type: "string"},
                                        title: {editable: true, type: "string"},
                                        fileName: {editable: true, type: "string"},
                                        externalLink: {editable: true, type: "string"},
                                        category: {editable: true, type: "number"},
                                        subCategory: {editable: true, type: "number"},
                                        priority: {editable: true, type: "number"},
                                        showAsCompanyMessage:{editable: true, type: "boolean"},
                                        addUserImage:{editable: true, type: "boolean"},
                                        timeStamp: {editable: true,type: "date"}
                                    }
                                }
                            },
                            serverFiltering: true,
                            requestStart: function (e) {
                                //console.log('ChatWidgetView:dataSource:request start:');
                                kendo.ui.progress(self.$("#loadingChatWidget"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('ChatWidgetView:dataSource:request end:');
                                //var data = this.data();
                                kendo.ui.progress(self.$("#loadingChatWidget"), false);
                            },
                            change: function (e) {
                                //console.log('ChatWidgetView:dataSource:change:');

                                var data = this.data();
                                if (data.length > 0) {

                                } else {
                                    //self.displayNoDataOverlay();
                                }
                            },
                            error: function (e) {
                                //console.log('ChatWidgetView:dataSource:error');
                                kendo.ui.progress(self.$("#loadingChatWidget"), false);
                            },
                            sort: {field: "timeStamp", dir: "desc"},
                            filter: [
                                {field: "category", operator: "eq", value: 5},
                                {field: "timeStamp", operator: "lte", value: self.now}
                            ]
                        });

                    this.getData();
                }

            },
            onShow: function () {
                //console.log('HeaderView:onShow');

                var self = this;

                if (self.userKey) {

                    var offset = (new Date()).getTimezoneOffset();
                    self.then  = moment().clone().subtract((offset), 'minutes');

                    //this.timeCount = 0;
                    console.log('HeaderView:onShow:timeout:' + App.timeout);
                    if (App.timeout === null || App.timeout === undefined) {
                        //console.log('HeaderView:initialize:checkTimeout');
                        this.checkTimeout();
                    } else {
                        console.log('HeaderView:onShow:timeout already defined: ' + App.timeout);
                    }

                    var url = App.config.PortalFiles + "/UserImages/" + self.userKey + ".jpg?" + new Date().getTime();
                    $.ajax({
                        url: url,
                        type: "GET",
                        crossDomain: true,
                        success: function (response) {
                            console.log('HeaderView:user image found:done');
                            self.$('#userImage').attr('src',url );
                            self.$('#userImageLarge').attr('src', url);
                            self.$('#imageInstruction').hide();
                        },
                        error: function (xhr, status) {
                            console.log('HeaderView:user image not found:fail');
                            self.$('#imageInstruction').show();
                        }
                    });

                } else {

                    var qs = this.getQueryStringParameters();
                    console.log('HeaderView:onShow:qs:' + JSON.stringify(qs));

                    this.resetId = qs.resetId;

                    if (this.resetId === undefined || this.resetId === null) {
                        console.log('HeaderView:onShow:no user key is defined - logging out');
                        self.logout();
                    }
                }

            },
            onUserImageClicked: function (e) {

                var self = this;
                var app = App;
                console.log('HeaderView: onUserImageClicked');

                // Get files
                var input = document.createElement("input");
                input.type = "file";
                input.accept = ".jpg"; //, png, jpeg

                input.onchange = function (e) {
                    console.log('HeaderView:openFileDialog:Click OK');

                    var file = e.target.files[0];

                    var formData = new FormData();
                    formData.append("opmlFile",file);
                    kendo.ui.progress(self.$("#loadingUserImage"), true);
                    $.ajax({
                        url: app.config.DataServiceURL + "/rest/files/UserUpload/UserImages/" + self.userKey,
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function () {
                            console.log("HeaderView:uploadFiles:Successfully uploaded user image.");
                            self.$('#userImage').attr('src', app.config.PortalFiles + "/UserImages/" + self.userKey + ".jpg?" + new Date().getTime());
                            self.$('#userImageLarge').attr('src', app.config.PortalFiles + "/UserImages/" + self.userKey + ".jpg?" + new Date().getTime());
                            kendo.ui.progress(self.$("#loadingUserImage"), false);
                        },
                        error: function (err) {
                            //console.log(err);
                            console.log("HeaderView:uploadFiles:Error: " + err);
                            var errorToShow = "User Image Upload failed.";
                            if (err.responseText && err.responseText.length > 0){
                                errorToShow = err.responseText;
                                //parse the leading and trailing double quotes from the string
                                errorToShow = errorToShow.substring(1,errorToShow.length-1);
                            }
                            kendo.ui.progress(self.$("#loadingUserImage"), false);
                        }
                    });
                };

                // Open file dialog
                if ( document.createEvent ) {
                    console.log('HeaderView:openFileDialog:document.createEvent');
                    var event = document.createEvent("MouseEvents");
                    event.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    input.dispatchEvent(event);
                    console.log('HeaderView:openFileDialog:document.createEvent:2');
                } else {
                    console.log('HeaderView:openFileDialog:non-document.createEvent');
                    var event2 = new MouseEvent("click");
                    input.dispatchEvent(event2);
                }
            },
            onCallbackErrorMessageChanged: function () {
                console.log('HeaderView: onCallbackErrorMessageChanged');

                var callbackErrorMessage = App.model.get('callbackErrorMessage');

                if (callbackErrorMessage === 1) {
                    this.callbackMessageRegion.show(new GeneralCalloutWidgetView({
                        type: 'Attention',
                        message: 'You do not have permissions to see this employee profile.',
                        borderRadius: 0
                    }));
                    App.model.set('callbackErrorMessage', null);
                    App.router.navigate('employees', {trigger: true});

                } else if (callbackErrorMessage === 2) {
                    this.callbackMessageRegion.show(new GeneralCalloutWidgetView({
                        type: 'Attention',
                        message: 'You do not have permissions to see the employees list.',
                        borderRadius: 0
                    }));
                    App.model.set('callbackErrorMessage', null);
                    App.router.navigate('dashboard', {trigger: true});

                } else if (callbackErrorMessage === 3) {
                    this.callbackMessageRegion.show(new GeneralCalloutWidgetView({
                        type: 'Attention',
                        message: 'You do not have permissions to see the stats timecard.',
                        borderRadius: 0
                    }));
                    App.model.set('callbackErrorMessage', null);
                    App.router.navigate('dashboard', {trigger: true});

                } else if (callbackErrorMessage === 4) {
                    this.callbackMessageRegion.show(new GeneralCalloutWidgetView({
                        type: 'Attention',
                        message: 'You do not have permissions to see client reports.',
                        borderRadius: 0
                    }));
                    App.model.set('callbackErrorMessage', null);
                    App.router.navigate('dashboard', {trigger: true});

                } else if (callbackErrorMessage === 5) {
                    this.callbackMessageRegion.show(new GeneralCalloutWidgetView({
                        type: 'Attention',
                        message: 'You do not have permissions to see the companies list.',
                        borderRadius: 0
                    }));
                    App.model.set('callbackErrorMessage', null);
                    App.router.navigate('dashboard', {trigger: true});

                } else if (callbackErrorMessage === 6) {
                    this.callbackMessageRegion.show(new GeneralCalloutWidgetView({
                        type: 'Attention',
                        message: 'You do not have permissions to see the plans list.',
                        borderRadius: 0
                    }));
                    App.model.set('callbackErrorMessage', null);
                    App.router.navigate('dashboard', {trigger: true});
                }

            },
            getQueryStringParameters: function() {
                var url = window.location.href;
                var queryString = url.split('?')[1] || "",
                    params = {},
                    paramParts = queryString.split(/&|=/),
                    length = paramParts.length,
                    idx = 0;

                for (; idx < length; idx += 2) {
                    if (paramParts[idx] !== "") {
                        params[decodeURIComponent(paramParts[idx])] = decodeURIComponent(paramParts[idx + 1]);
                    }
                }

                return params;
            },
            getData: function () {

                var self = this;
                var app = App;

                this.employeeDataSource.fetch(function () {

                    var empRecord = self.employeeDataSource.data()[0];
                    self.employeeID = empRecord.employeeID;
                    self.teamLeaderID = empRecord.teamLeaderID;
                    self.firstName = empRecord.firstName;
                    self.lastName = empRecord.lastName;
                    self.emailAddress = empRecord.emailAddress;
                    self.homePhone = empRecord.homePhone;
                    self.timeZoneID = empRecord.timeZoneID;
                    self.timerManual = empRecord.timerManual;
                    self.userFullName = self.firstName + " " + self.lastName;
                    self.isTeamLeader = empRecord.isTeamLeader;

                    self.hidePlans = empRecord.hidePlans;
                    self.hideStatsTimecard = empRecord.hideStatsTimecard;
                    self.hideLevel1Employees = empRecord.hideLevel1Employees;
                    self.hideClients = empRecord.hideClients;
                    self.hideCompanies = empRecord.hideCompanies;

                    self.$('#userFullName').html(self.userFullName + '&nbsp;');
                    self.$('#userFullName2').html(self.userFullName);
                    self.$('#homePhone').html(self.homePhone);
                    self.$('#emailAddress').html(self.emailAddress);

                    // Populate User profile info
                    self.$('#firstLastName').html(self.userFullName);
                    self.$('#emailAddress').html("Email: " + self.emailAddress);
                    self.$('#esaLocalPhone').html("ESA Local Phone: " + empRecord.esaLocalPhone);
                    self.$('#esaTollFree').html("ESA Toll Free: " + empRecord.esaTollFree);

                    app.model.set('myTeamLeaderId', self.teamLeaderID);
                    app.model.set('timerManual', self.timerManual);
                    app.model.set('timeZoneId', self.timeZoneID);
                    app.model.set('myEmployeeId', self.employeeID);
                    app.model.set('isTeamLeader', self.isTeamLeader);

                    app.model.set('hidePlans', self.hidePlans);
                    app.model.set('hideStatsTimecard', self.hideStatsTimecard);
                    app.model.set('hideLevel1Employees', self.hideLevel1Employees);
                    app.model.set('hideClients', self.hideClients);
                    app.model.set('hideCompanies', self.hideCompanies);

                    app.asideRegion.reset();
                    app.footerRegion.reset();

                    if (self.userType === 0) {
                        app.mainRegion.reset();
                        app.mainRegion.show(new DocsLayoutView());
                    } else if (Backbone.history.getFragment() === "home") {
                        app.mainRegion.reset();
                        app.mainRegion.show(new WelcomeLayoutView());
                        self.chatDataSource.fetch(function () {

                            var data = this.data();

                            self.state = {};
                            var filters = [];

                            console.log('ChatWidgetView:chatDataSource:fetch:data.length:' + data.length);

                            $.each(data, function (index, value) {

                                self.todaysMessage = {
                                    messageID: value.ID,
                                    messageDate: value.timeStamp,
                                    messageText: value.text,
                                    messageTitle: value.title,
                                    comments:[]
                                };

                                // Open callout widget with today's message if it hasn't been closed
                                var lastDismissed = App.model.get('chatMessageDismissed');
                                if (lastDismissed !== self.today) {
                                    self.callbackMessageRegion.show(new CalloutWidgetView({
                                        type: 'New Chat Message',
                                        title: self.todaysMessage.messageTitle,
                                        message: self.todaysMessage.messageText,
                                        borderRadius: 0
                                    }));
                                }


                                return false;
                            });
                        });
                    }

                    app.asideRegion.show(new AsideView());
                    app.footerRegion.show(new FooterView());

                });

            },
            checkTimeout: function () {
                //console.log('HeaderView:checkTimeout');

                var self = this;
                var app = App;

                //log out after 12 hours of inactivity....check every 1 hour
                app.timeout = setTimeout(function () {
                    var offset = (new Date()).getTimezoneOffset();
                    if (app.activity === true) {
                        app.activity = false;
                        //self.timeCount = 0;

                        self.then  = moment().clone().subtract((offset), 'minutes');
                        self.checkTimeout();
                    } else {
                        //self.timeCount++;
                        self.now  = moment().clone().subtract((offset), 'minutes');
                        var duration = moment.duration(self.now.diff(self.then));
                        var hours = duration.asHours();
                        console.log('HeaderView:checkTimeout:hours of inactivity (after 8 hours system logs out):' + hours);

                        if (hours >= 8) {  // 8 hours, 480 minutes
                            self.logout();
                        } else {
                            self.checkTimeout();
                        }
                    }
                }, 60000);

                // 3600000 - 1 hour of ms,
                // 60000 - 1 minute of ms
            },
            onSignOutClicked: function (e) {

                //console.log('HeaderView: onSignOutClicked');

                this.timerStatus = App.model.get('timerStatus');

                if (this.timerStatus === "Open" || this.timerStatus === "INTERRUPTED") {
                    alert('You must close the open timer file before logging out.');
                } else { //if (confirm("Log out?")) {

                    this.logout();
                }

            },
            clearAppVariables: function (e) {

                //console.log('HeaderView:clearAppVariables');
                var cookies = $.cookie();
                for(var cookie in cookies) {
                    if (cookie.indexOf('ESA') > -1 ) {
                        //console.log('HeaderView:clearAppVariables:remove cookie:' + cookie);
                        $.removeCookie(cookie);
                    }
                }

                // Clear app variables
                App.model.set('userId', null);
                App.model.set('userKey', null);
                App.model.set('userType', null);
                App.model.set('currentTimerID', null);
                App.model.set('currentTimerServiceID', null);
                App.model.set('currentTimerClientID', null);
                App.model.set('selectedDateFilter', null);
                App.model.set('widget1Value', null);
                App.model.set('widget2Value', null);
                App.model.set('widget3Value', null);
                App.model.set('widget4Value', null);
                App.model.set('mySumTotalTime', null);
                App.model.set('mySumTotalMeetings', null);
                App.model.set('mySumMeetingsTarget', null);
                App.model.set('myMtgPercent', null);
                App.model.set('teamSumTotalTime', null);
                App.model.set('teamSumTotalMeetings', null);
                App.model.set('teamSumMeetingsTarget', null);
                App.model.set('teamMtgPercent', null);
                App.model.set('selectedTeamLeaderId', null);
                App.model.set('employeeGridFilter', null);
                App.model.set('userGridFilter', null);
                App.model.set('adminIndustryNewsGridFilter', null);
                App.model.set('adminStaticGridFilter', null);
                App.model.set('adminBioGridFilter', null);
                App.model.set('clientContactGridFilter', null);
                App.model.set('clientPasswordGridFilter', null);
                App.model.set('clientReportGridFilter', null);
                App.model.set('clientAssignmentGridFilter', null);
                App.model.set('timeCardDetailGridFilter', null);
                App.model.set('clientDataGridFilter', null);
                App.model.set('tileViewNotifications', null);
                App.model.set('tileViewCompanyStats', null);
                App.model.set('tileViewTeamPersonalStats', null);
                App.model.set('tileViewTeamMeetingPerc', null);
                App.model.set('tileViewMessageBoard', null);
                App.model.set('tileViewPhoneDirectory', null);
                App.model.set('tileViewMyClients', null);
                App.model.set('showCompTime', null);
                //App.model.set('showReferralTime', null);
                App.model.set('lastCompanyMessagesView', null);
                App.model.set('lastESAMediaView', null);

            },
            logout: function (e) {

                //console.log('HeaderView:logout');

                var self = this;
                var app = App;

                //console.log('HeaderView: logout');
                kendo.ui.progress($("#subHeaderLoading"), true);

                App.timeout = null;

                this.userDataSource = new kendo.data.DataSource(
                    {
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: App.config.DataServiceURL + "/odata/PortalUserTable?$select=userLogin,userPassword,userID,userType,userKey,firstName,lastName,loginCount,lastLogin,lastLogout,lastPasswordChange,prevPasswords,lastCompanyMessagesView,lastESAMediaView",
                            complete: function (e) {
                                console.log('HeaderView:userDataSource:read:complete');
                             },
                            error: function (e) {
                                var message = 'HeaderView:userDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                            },
                            complete: function (e) {
                                console.log('HeaderView:userDataSource:update:complete');
                             },
                            error: function (e) {
                                var message = 'HeaderView:userDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "userKey",
                            fields: {
                                userLogin: {editable: false, type: "string"},
                                userPassword: {editable: false, type: "string"},
                                firstName: {editable: false, type: "string"},
                                lastName: {editable: false, type: "string"},
                                userID: {editable: false, type: "string"},
                                userKey: {editable: false, type: "string"},
                                userType: {editable: false, defaultValue: 1, type: "number"},
                                loginCount: {editable: true, type: "number"},
                                lastLogin: {editable: true, type: "date"},
                                lastLogout: {editable: true, type: "date"},
                                lastPasswordChange: {editable: true, type: "date"},
                                lastCompanyMessagesView: {editable: true, type: "date"},
                                lastESAMediaView: {editable: true, type: "date"}
                            }
                        }
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //kendo.ui.progress($("#loadingESAUser"), true);
                    },
                    requestEnd: function () {
                        //kendo.ui.progress($("#loadingESAUser"), false);
                    },
                    filter: [
                        {field: "userLogin", operator: "eq", value: self.userLogin}
                    ]
                });

                this.userDataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count === 0) {
                        console.log('HeaderView:logout:userLogin not found');

                        kendo.ui.progress($("#subHeaderLoading"), false);
                        self.clearAppVariables();

                        // Go to index
                        app.router.navigate('index', {trigger: true});
                    }
                    else {
                        //console.log('HeaderView:logout');

                        var record = null;
                        var userLogin = null;

                        var i = 0;
                        while (userLogin === null || userLogin === undefined || userLogin === "") {
                            record = data[i];
                            userLogin = data[i] && data[i].userLogin;
                            i++;
                            if (i == data.length)
                                break;
                        }

                        var offset = (new Date()).getTimezoneOffset();
                        var now = new Date(moment().clone().subtract((offset), 'minutes'));
                        var lastLogin = new Date(moment(record.lastLogin).clone().subtract((offset), 'minutes'));
                        var lastPasswordChange = new Date(moment(record.lastPasswordChange).clone().subtract((offset), 'minutes'));
                        var prevPasswords  = record.prevPasswords;
                        //console.log('HeaderView:logout:lastPasswordChange:' + lastPasswordChange );
                        var lastCompanyMessagesView = new Date(moment(record.lastCompanyMessagesView).clone().subtract((offset), 'minutes'));
                        var lastESAMediaView = new Date(moment(record.lastESAMediaView).clone().subtract((offset), 'minutes'));

                        record.set("lastLogin", lastLogin);
                        record.set("lastLogout", now);
                        record.set("lastPasswordChange", lastPasswordChange);
                        record.set("prevPasswords", prevPasswords);
                        record.set("lastCompanyMessagesView", lastCompanyMessagesView);
                        record.set("lastESAMediaView", lastESAMediaView);

                        //self.userDataSource
                        $.when(this.sync()).done(function (e) {

                            kendo.ui.progress($("#subHeaderLoading"), false);

                            self.clearAppVariables();

                            // Go to index
                            app.router.navigate('index', {trigger: true});

                        });

                    }
                });
            },
            onToggleMenuClicked: function (e) {
                console.log('HeaderView:onToggleMenuClicked');

                var screenSizes = $.AdminLTE.options.screenSizes;

                e.preventDefault();

                //Enable sidebar push menu
                if ($(window).width() > (screenSizes.sm - 1)) {
                    if ($("body").hasClass('sidebar-collapse')) {
                        $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu');
                    } else {
                        $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    }
                }
                //Handle sidebar push menu for small screens
                else {
                    if ($("body").hasClass('sidebar-open')) {
                        $("body").removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    } else {
                        $("body").addClass('sidebar-open').trigger('expanded.pushMenu');
                    }
                }

            },
            onResize: function () {
                //console.log('HeaderView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('HeaderView:resize');
            },
            remove: function () {
                //console.log('---------- HeaderView:remove ----------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }

        });
    });