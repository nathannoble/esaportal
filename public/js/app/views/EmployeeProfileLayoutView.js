define(['App', 'jquery', 'hbs!templates/employeeProfileLayout', 'backbone', 'marionette', 'underscore', 'options/ViewOptions',
        'views/ContactInfoView', 'views/FileWidgetView', 'views/NoteWidgetView'],
    function (App, $, template, Backbone, Marionette, _, ViewOptions, ContactInfoView, FileWidgetView, NoteWidgetView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                tile1: "#tile-employee-contact-info",
                tile2: "#tile-employee-files",
                tile4: "#tile-employee-notes"

            },
            initialize: function (options) {
                //console.log('EmployeeProfileLayoutView:initialize');

                _.bindAll(this);

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Employee"};
                }

                // Subscribe to browser events
                $(window).on("resize", this.onResize);
            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('EmployeeProfileLayoutView:onShow');
                var self = this;
                var app = App;

                //this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);

                this.userId = App.model.get('userId');

                // For userType 2 check to see if permissions to see this employee
                this.selectedEmployeeId = App.model.get('selectedEmployeeId');
                this.userType = App.model.get('userType');
                this.hideLevel1Employees = App.model.get('hideLevel1Employees');
                this.employeeIdsNotUserType1 = App.model.get('employeeIdsNotUserType1');

                if (this.userId === null || this.userType === 1) {  // do not show page for lowest user type
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else if (this.userType === 2) {

                    console.log('EmployeeProfileLayoutView:onShow:employeeIdsNotUserType1:' + JSON.stringify(this.employeeIdsNotUserType1));
                    var employeeFound = false;
                    $.each(this.employeeIdsNotUserType1, function (index, value) {

                        if (self.selectedEmployeeId === value) {
                            employeeFound = true;
                            return false;
                        }

                    });

                    if (employeeFound || this.hideLevel1Employees) {  // these are level 2 and 3
                        console.log('EmployeeProfileLayoutView:onShow:you do not have permissions to see this employee profile.');
                        App.model.set('callbackErrorMessage',1);
                        //alert('You do not have permissions to see this employee profile.');
                    } else {
                        this.resetView();
                    }

                } else {
                    this.resetView();
                }

                //setTimeout(this.resize, 0);
            },
            resetView: function () {
                //console.log('EmployeeProfileLayoutView:resetDashboard');

                this.tile1.reset();
                this.tile2.reset();
                this.tile4.reset();

                this.tile1.show(new ContactInfoView({type: 'Employee'}));
                this.tile2.show(new FileWidgetView({type: 'Employee'}));
                this.tile4.show(new NoteWidgetView({type: 'Employee'}));

            },
            onSelectedDateFilterChanged: function () {
                this.resetView();
            },
            transitionIn: function () {
                //console.log('EmployeeProfileLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('EmployeeProfileLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('EmployeeProfileLayoutView:resize');


            },
            remove: function () {
                //console.log('---------- EmployeeProfileLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });