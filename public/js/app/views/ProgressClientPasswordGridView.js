define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressClientPasswordGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('ProgressClientPasswordGridView:initialize');

                _.bindAll(this);
                
                var self = this;

                this.clientPasswordGridFilter = App.model.get('clientPasswordGridFilter');
                
                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource({
                    //autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                            },
                            complete: function (e) {
                                console.log('ProgressClientPasswordGridView:dataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'ProgressClientPasswordGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                lastName: {editable: true, type: "string", validation: {required: false}},
                                firstName: {editable: true, type: "string", validation: {required: false}},
                                clientCompany: {editable: true, type: "string", validation: {required: false}},
                                schedulerName: {editable: true, type: "string", validation: {required: false}},
                                sysCrmUserName: {editable: true, type: "string", validation: {required: false}},
                                sysCrmPassword: {editable: true, type: "string", validation: {required: false}},
                                sysCrmNotes: {editable: true, type: "string", validation: {required: false}},
                                sysEmailUserName: {editable: true, type: "string", validation: {required: false}},
                                sysEmailPassword: {editable: true, type: "string", validation: {required: false}},
                                sysEmailNotes: {editable: true, type: "string", validation: {required: false}},
                                sysCalendarUserName: {editable: true, type: "string", validation: {required: false}},
                                sysCalendarPassword: {editable: true, type: "string", validation: {required: false}},
                                sysCalendarNotes: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    //serverFiltering: true,
                    //serverSorting: true,
                    requestStart: function () {
                        //console.log('   ProgressClientPasswordGridView:requestStart');

                        kendo.ui.progress(self.$("#clientLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    ProgressClientPasswordGridView:requestEnd');
                        kendo.ui.progress(self.$("#clientLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientPasswordGridView:error');
                        kendo.ui.progress(self.$("#clientLoading"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientPasswordGridView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    //filter: [{field: "status", operator: "eq", value: '4 - Active'}],
                    sort: [{field: "clientCompany", dir: "asc"},{field: "lastName", dir: "asc"}]
                });

                this.companyDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                complete: function (e) {
                                    console.log('ProgressClientPasswordGridView:companyDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientPasswordGridView:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyID",
                                fields: {
                                    companyID: {editable: false, type: "number"},
                                    clientCompany: {type: "string"}
                                }
                            }
                        },
                        sort: {field: "clientCompany", dir: "asc"},
                        serverSorting: true,
                        serverFiltering: true,
                        filter: [
                            {field: "clientCompany", operator: "neq", value: "--- NONE ---"}
                            //{field: "actualClients", operator: "gt", value: 0},
                            //{field: "actualClients", operator: "neq", value: null}
                        ]

                    });
            },
            onRender: function () {
                //console.log('ProgressClientPasswordGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientPasswordGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#client").kendoGrid({
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    toolbar: [
                        {
                            name: "excel"
                        }],
                    excel: {
                        fileName: "ClientPassword.xlsx",
                        allPages: true
                    },
                    filterable: {
                        mode: "row"
                    },
                    columns: [
                        {
                            title: "Client Name",
                            field: "lastName",
                            width: 175,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.lastName + ", " + e.firstName+ "</div>";
                                return template;
                            },
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            },
                            sortable: true
                        },
                        {
                            title: "Client Company",
                            field: "clientCompany",
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                return template;
                            },
                            width: 200,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.companyFilter
                                }
                            },
                            sortable: true
                        },
                        {
                            field: "schedulerName",
                            title: "Scheduler",
                            width: 150,
                            filterable: false
                        },{
                            field: "sysCRMUserName",
                            title: "CRM User Name",
                            width: 100,
                            filterable: false
                        }, {
                            field: "sysCrmPassword",
                            title: "CRM Password",
                            width: 100,
                            filterable: false
                        }, {
                            field: "sysCrmNotes",
                            title: "CRM Notes",
                            width: 150,
                            filterable: false
                        }, {
                            field: "sysEmailUserName",
                            title: "Email User Name",
                            width: 100,
                            filterable: false
                        }, {
                            field: "sysEmailPassword",
                            title: "Email Password",
                            width: 100,
                            filterable: false
                        }, {
                            field: "sysEmailNotes",
                            title: "Email Notes",
                            width: 150,
                            filterable: false
                        }, {
                            field: "sysCalendarUserName",
                            title: "Calendar User Name",
                            width: 100,
                            filterable: false
                        }, {
                            field: "sysCalendarPassword",
                            title: "Calendar Password",
                            width: 100,
                            filterable: false
                        }, {
                            field: "sysCalendarNotes",
                            title: "Calendar Notes",
                            width: 150,
                            filterable: false
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("ProgressClientPasswordGridView:def:onShow:dataBound");
                        self.$('.company-profile').on('click', self.viewCompanyProfile);

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;

                            var clientPasswordGridFilter = [];

                            $.each(filters, function (index, value) {
                                clientPasswordGridFilter.push(value);
                            });

                            //console.log("ProgressClientPasswordGridView:def:onShow:dataBound:set clientPasswordGridFilter:" + JSON.stringify(clientPasswordGridFilter));
                            app.model.set('clientPasswordGridFilter', clientPasswordGridFilter);
                        }
                    },
                    change: function (e) {
                        //console.log('ProgressClientPasswordGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('ProgressClientPasswordGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('ProgressClientPasswordGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        e.container.find("input[name='clientID']").prop("disabled", true);

                        // Set the focus to the Username field
                        //e.container.find("input[name='Username']").focus();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('ProgressClientPasswordGridView:getData');

                var grid = this.$("#client").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);
                    grid.setDataSource(self.dataSource);
                    if (self.clientPasswordGridFilter !== null && self.clientPasswordGridFilter !== []) {
                        var filters = [];
                        $.each(self.clientPasswordGridFilter, function (index, value) {
                            filters.push(value);
                        });

                        //console.log("ProgressClientPasswordGridView:grid:set datasource:filter" + JSON.stringify(filters));
                        grid.dataSource.filter(filters);
                    } else {
                        grid.dataSource.filter({field: "status", operator: "eq", value: '4 - Active'});
                    }

                    // Resize the grid
                    setTimeout(self.resize, 0);
                });
            },
            companyFilter: function (container) {
                var self = this;

                //console.log('ProgressClientPasswordGridView:companyFilter:' + JSON.stringify(container));
                self.companyDataSource.fetch(function (data) {

                    var dataSource = [];
                    var records = this.data();

                    $.each(records, function (index, value) {
                        dataSource.push({value: value.clientCompany, text: value.clientCompany});
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: dataSource,
                        optionLabel: "Select Value"
                        //change: self.onCompanyFilterChanged
                    });
                });

            },
            viewCompanyProfile: function (e) {
                //console.log('ProgressClientPasswordGridView:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                App.model.set('selectedCompanyId', companyId);
                App.router.navigate('companyProfile', {trigger: true});

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressClientPasswordGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#client').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressClientPasswordGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#client').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressClientPasswordGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#client").parent().parent().height();
                this.$("#client").height(parentHeight);

                parentHeight = parentHeight - 57; //117

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressClientPasswordGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressClientPasswordGridView:resize:headerHeight:' + headerHeight);
                this.$("#client").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressClientPasswordGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#client").data('ui-tooltip'))
                    this.$("#client").tooltip('destroy');

                this.clientPasswordGridFilter = [];

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });