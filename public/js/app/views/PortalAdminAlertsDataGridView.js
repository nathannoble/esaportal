define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalAdminAlertsDataGrid',
        //'hbs!templates/editPortalAdminAlertsDataPopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data','kendo/kendo.upload'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalAdminAlertsDataGridView:initialize');

                _.bindAll(this);

                var self = this;
                self.categories = [
                    {text: 'Company Message', value: 0},
                    {text: 'ESA Media', value: 1},
                    {text: 'Industry News', value: 2},
                    {text: 'Social Media', value: 3},
                    {text: 'Alert Message', value: 4},
                    {text: 'Company Chat', value: 5}
                ];

                self.priorities = [
                    {text: 'N/A', value: 0},
                    {text: '1', value: 1},
                    {text: '2', value: 2}
                ];

                self.subCategories = [
                    {text: 'None', value: 0},
                    {text: '1', value: 1},
                    {text: '2', value: 2}
                ];

                this.adminAlertsGridFilter = App.model.get('adminAlertsGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        autoSync: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                complete: function (e) {
                                    console.log('PortalAdminAlertsDataGridView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminAlertsDataGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalAdminAlertsDataGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminAlertsDataGridView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminAlertsDataGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalAdminAlertsDataGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminAlertsDataGridView:dataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminAlertsDataGridView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalAdminAlertsDataGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminAlertsDataGridView:dataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminAlertsDataGridView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    title: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Alert Title Here)"
                                    },
                                    text: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Alert Text Here)"
                                    },
                                    text2: {editable: true, type: "string"},
                                    shortText: {editable: true, type: "string"},
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {editable: true, type: "string"},
                                    category: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:4
                                    },
                                    subCategory: {editable: true, type: "number"},
                                    priority: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:0
                                    },
                                    showAsCompanyMessage: {editable: true, type: "boolean"},
                                    addUserImage: {editable: true, type: "boolean"},
                                    timeStamp: {
                                        editable: true,
                                        type: "date",
                                        defaultValue: function () {
                                            var offset = (new Date()).getTimezoneOffset();
                                            var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                            return now;
                                        }
                                    }
                                }
                            }
                        },
                        sort: [{field: "timeStamp", dir: "desc"},{field: "priority", dir: "asc"} ],
                        requestStart: function (e) {
                            //console.log('PortalAdminAlertsDataGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#adminAlertsDataLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalAdminAlertsDataGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#adminAlertsDataLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalAdminAlertsDataGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalAdminAlertsDataGridView:dataSource:error');
                            kendo.ui.progress(self.$("#adminAlertsDataLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalAdminAlertsDataGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#adminAlertsData').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 4)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onRender: function () {
                //console.log('PortalAdminAlertsDataGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminAlertsDataGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#adminAlertsData").kendoGrid({

                    toolbar: ["create"],
                    editable: true,
                    selectable: "cell",
                    navigatable: true,
                    sortable: true,
                    extra: false,
                    filterable: {
                        mode: "row"
                    },
                    resizable: true,
                    reorderable: true,
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.ID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "priority",
                            title: "Priority",
                            width: 70,
                            filterable: false,
                            template: function (e) {
                                var template = e.priority;
                                if (self.priorities[e.priority])
                                    template = self.priorities[e.priority].text;
                                return template;
                            },
                            editor: function(container, options) {
                                var input = $("<input/>");
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoDropDownList({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: self.priorities
                                });
                            }
                        },
                        {
                            field: "title",
                            title: "Title",
                            width: 150,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },{
                            field: "text",
                            title: "Text",
                            width: 310,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },
                        {
                            field: "showAsCompanyMessage",
                            title: "Show Alert on <br/>Welcome Page",
                            template: function (e) {
                                var template;
                                if (e.showAsCompanyMessage === false) {
                                    template = "<div style='text-align: center'>No</div>";
                                    return template;
                                } else {
                                    template = "<div style='text-align: center'>Yes</div>";
                                    return template;
                                }
                            },
                            width: 110,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $("<input/>");
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoDropDownList({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: [
                                        {text: 'Yes', value: 'true'},
                                        {text: 'No', value: 'false'}
                                    ]
                                });
                            }
                        },
                        {
                            field: "timeStamp",
                            title: "Time Stamp",
                            width: 100,
                            format: "{0:M/d/yyyy}",
                            filterable: false
                        }
                    ],
                    change: function (e) {
                        console.log('PortalAdminAlertsDataGridView:def:onShow:onChange:e.model:' + JSON.stringify(e.model));
                    },
                    save: function (e) {
                        e.model.dirty = true;
                        e.model.title = e.model.title.trim();
                        e.model.text = e.model.text.trim();
                        e.model.text2 = e.model.text2.trim();
                        e.model.externalLink = e.model.externalLink.trim();
                        e.model.shortText = e.model.shortText.trim();
                        //var offset = (new Date()).getTimezoneOffset();
                        //e.model.timeStamp.setMinutes( e.model.timeStamp.getMinutes() - offset );
                        console.log('PortalAdminAlertsDataGridView:def:onShow:onSave:e.model:' + JSON.stringify(e.model));
                    },
                    edit: function (e) {
                        console.log('PortalAdminAlertsDataGridView:onShow:edit');

                        e.model.category = 4;

                        // For new records set priority
                        if (e.model.ID === 0) {
                            //$('#idGroup').hide();
                            //$('.k-window-title').html('Add Company Alerts Record');
                            e.model.priority = 0;
                        }
                        self.category = self.categories[e.model.category].text.replace(" ","");
                        console.log('PortalAdminAlertsDataGridView:onShow:edit:category:' + self.category);

                        self.model = e.model;

                    },
                    dataBound: function (e) {
                        console.log("PortalAdminAlertsDataGridView:def:onShow:dataBound");

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;
                            if (filters.length === 0) {
                                filters.push({field: "category", operator: "eq", value: 4 });
                            }
                            var adminAlertsGridFilter = [];

                            $.each(filters, function (index, value) {
                                adminAlertsGridFilter.push(value);
                            });

                            console.log("PortalAdminAlertsDataGridView:def:onShow:dataBound:set adminAlertsGridFilter:" + JSON.stringify(adminAlertsGridFilter));
                            app.model.set('adminAlertsGridFilter', adminAlertsGridFilter);
                        }

                        console.log("PortalAdminAlertsDataGridView:def:onShow:dataBound:re-sort by date");

                        $('.k-grid-add').unbind("click");
                        $('.k-grid-add').bind("click", function(){
                            console.log("PortalAdminAlertsDataGridView:def:onShow:dataBound:Add");
                            var grid = self.$("#adminAlertsData").data("kendoGrid");
                            grid.dataSource.sort([{field: "timeStamp", dir: "desc"},{field: "priority", dir: "asc"} ]);
                        });
                    }
                });

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            createAutoClosingAlert:  function (selector, html)  {
                console.log('PortalAdminAlertsDataGridView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 50000);
            },
            getData: function () {
                //console.log('PortalAdminAlertsDataGridView:getData');

                var grid = this.$("#adminAlertsData").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);

                    if (self.adminAlertsGridFilter !== null && self.adminAlertsGridFilter !== [] && self.adminAlertsGridFilter !== undefined) {
                        var filters = [];
                        if (self.adminAlertsGridFilter.length === 0) {
                            filters.push({field: "category", operator: "eq", value: 4});
                        }
                        $.each(self.adminAlertsGridFilter, function (index, value) {
                            filters.push(value);
                        });

                        //console.log("PortalAdminAlertsDataGridView:grid:set datasource:filter" + JSON.stringify(filters));
                        grid.dataSource.filter(filters);
                    } else {
                        grid.dataSource.filter({field: "category", operator: "eq", value: 4});
                    }
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalAdminAlertsDataGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#adminAlertsData").parent().parent().height();
                this.$("#adminAlertsData").height(parentHeight);

                parentHeight = parentHeight - 56;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                this.$("#adminAlertsData").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalAdminAlertsDataGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#adminAlertsData").data('ui-tooltip'))
                    this.$("#adminAlertsData").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });