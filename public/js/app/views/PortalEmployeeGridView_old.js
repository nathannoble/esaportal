define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalEmployeeGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalEmployeeGridView:initialize');

                _.bindAll(this);

                var self = this;
                //var app = App;

                this.employeeGridFilter = App.model.get('employeeGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.teamLeaderDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    status: {type: "string", validation: {required: false}},
                                    isTeamLeader: {editable: true, type: "boolean"},
                                    teamLeaderID: {editable: false, type: "number"},
                                    teamLeaderName: {editable: false, type: "string"}
                                }
                            }
                        },
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"}
                        ],
                        filter: {field: "isTeamLeader", operator: "eq", value: true},
                        serverFiltering: true,
                        serverSorting: true
                    });

                this.clientDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: App.config.DataServiceURL + "/odata/ProgressClientTable",
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                schedulerID: {defaultValue: 0, type: "number"},
                                status: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   ProgressEmployeeView:requestStart');
                        //kendo.ui.progress(self.$("#empLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    ProgressEmployeeView:requestEnd');
                        //kendo.ui.progress(self.$("#empLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressEmployeeView:error');
                        //kendo.ui.progress(self.$("#empLoading"), false);

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                    },
                    filter: [
                        {field: "status", operator: "eq", value: '4 - Active'}
                    ]

                });

                this.dataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalEmployeeGridView:update');
                                    return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalEmployeeGridView:create');
                                    return App.config.DataServiceURL + "/odata/ProgressEmployee";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalEmployeeGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    teamLeaderID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    emailAddress: {editable: true, type: "string", validation: {required: true}},
                                    esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                    eeTypeTitle: {type: "string", validation: {required: false}},
                                    status: {
                                        type: "string",
                                        validation: {required: false},
                                        values: [
                                            {text: '1 - Active', value: '1 - Active'},
                                            {text: '2 - Termed', value: '2 - Termed'},
                                            {text: '3 - No Hire', value: '3 - No Hire'}
                                        ]
                                    },
                                    isEligibleSupport: {editable: true, type: "boolean"},
                                    maxClientLoad: {editable: true, type: "number"},
                                    currentClientLoad: {editable: true, type: "number"},
                                    hireDate: {editable: true, type: "date", validation: {required: false}},
                                    termDate: {editable: true, type: "date", validation: {required: false}},
                                    birthDate: {editable: true, type: "date", validation: {required: false}}
                                }
                            }
                        },
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"}
                        ],
                        requestStart: function (e) {
                            //console.log('PortalEmployeeGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#empLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalEmployeeGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#empLoading"), false);
                        },
                        error: function (e) {
                            //console.log('PortalEmployeeGridView:dataSource:error');
                            kendo.ui.progress(self.$("#empLoading"), false);
                        },
                        serverFiltering: true,
                        serverSorting: true
                    });

                this.teamLeaderRecords = App.model.get('teamLeaderRecords');

                if (self.teamLeaderRecords.length === 0) {
                    self.teamLeaderDataSource.fetch(function () {

                        var tlRecords = self.teamLeaderDataSource.data();
                        self.teamLeaderRecords = [];

                        var noneRecord = {
                            teamLeaderID: 0,
                            teamLeaderName: '--- NONE ---'
                        };
                        self.teamLeaderRecords.push(noneRecord);

                        $.each(tlRecords, function (index, value) {

                            var record = value;
                            var newRecord = {
                                teamLeaderID: record.employeeID,
                                teamLeaderName: record.lastName + ", " + record.firstName
                            };
                            self.teamLeaderRecords.push(newRecord);

                        });

                        App.model.set('teamLeaderRecords', self.teamLeaderRecords);
                    });
                }

            },
            onRender: function () {
                //console.log('PortalEmployeeGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalEmployeeGridView:onShow --------");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PortalEmployeeGridView:getData');

                var self = this;
                var app = App;

                self.clientDataSource.fetch(function () {

                    var clientData = this.data();

                    self.dataSource.fetch(function () {

                        var data = this.data();

                        var dataSourceRecords = [];

                        $.each(data, function (index, value) {
                            var numberOfClients = 0;
                            $.each(clientData, function (clientIndex, clientValue) {
                                if (clientValue.schedulerID === value.employeeID && value.employeeID !== 0) {
                                    numberOfClients++;
                                }
                            });

                            value.currentClientLoad = numberOfClients;
                            value.fullName = value.lastName + ", " + value.firstName;
                            dataSourceRecords.push(value);
                        });

                        var grid = self.$("#emp").kendoGrid(
                            {
                                //selectable: "row",
                                sortable: true,
                                extra: false,
                                resizable: true,
                                reorderable: true,
                                filterable: {
                                    mode: "row"
                                },
                                //serverAggregates: true,
                                columnResize: function (e) {
                                    //self.resize();
                                    window.setTimeout(self.resize, 10);
                                },
                                columns: [
                                    {
                                        field: "employeeID",
                                        title: "ID",
                                        width: 55,
                                        filterable: false
                                    }, {
                                        title: "Name",
                                        field: "lastName",
                                        template: function (e) {
                                            var template = "<div style='text-align: left'><a class='employee-profile btn-interaction' data-id='" + e.employeeID + "' href='#employeeProfile'>" + e.lastName + "</a></div>";
                                            return template;
                                        },
                                        width: 250,
                                        filterable: {
                                            cell: {
                                                showOperators: false,
                                                operator: "contains",
                                                suggestionOperator: "contains"
                                            }
                                        },
                                        sortable: true
                                    },
                                    {
                                        field: "teamLeaderID",
                                        template: function (e) {
                                            //console.log("PortalEmployeeGridView:grid:teamLeaderID:" + e.teamLeaderID);
                                            var template = "<div style='text-align: left'>" + self.getTeamLeader(e.teamLeaderID) + "</div>";
                                            return template;
                                        },
                                        title: "Account Manager",
                                        width: 200,
                                        sortable: true,
                                        filterable: {
                                            cell: {
                                                showOperators: false,
                                                template: self.teamLeaderFilter
                                            }
                                        }
                                    },
                                    {
                                        field: "emailAddress",
                                        title: "Email",
                                        width: 150,
                                        filterable: false
                                    },
                                    {
                                        field: "esaLocalPhone",
                                        title: "Local Phone",
                                        width: 100,
                                        filterable: false
                                    },
                                    {
                                        field: "status",
                                        title: "Status",
                                        width: 200,
                                        filterable: {
                                            cell: {
                                                showOperators: false,
                                                template: self.statusFilter
                                            }
                                        }
                                    }, {
                                        field: "isEligibleSupport",
                                        title: "Eligible Support",
                                        width: 65,
                                        filterable: false
                                    }, {
                                        field: "maxClientLoad",
                                        title: "Max",
                                        width: 65,
                                        filterable: false
                                    }, {
                                        field: "currentClientLoad",
                                        title: "Actual",
                                        width: 65,
                                        filterable: false
                                    }
                                ],
                                dataBound: function (e) {
                                    console.log("PortalEmployeeGridView:def:onShow:dataBound");

                                    self.$('.employee-profile').on('click', self.viewEmployeeProfile);

                                    // save current filter
                                    var filters = null;
                                    if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                                        filters = e.sender.dataSource.filter().filters;

                                        var employeeGridFilter = [];

                                        $.each(filters, function (index, value) {
                                            employeeGridFilter.push(value);
                                        });

                                        //console.log("PortalEmployeeGridView:def:onShow:dataBound:set employeeGridFilter:" + JSON.stringify(employeeGridFilter));
                                        app.model.set('employeeGridFilter', employeeGridFilter);
                                    }

                                }
                            }
                        ); //.data("kendoGrid");

                        grid.setDataSource(self.dataSource);
                        //grid.setDataSource({
                        //    autoBind: false,
                        //    //type: "odata",
                        //    schema: {
                        //        data: "value",
                        //        total: function (data) {
                        //            return data['odata.count'];
                        //        },
                        //        model: {
                        //            id: "employeeID",
                        //            fields: {
                        //                employeeID: {editable: false, type: "number"},
                        //                teamLeaderID: {editable: false, type: "number"},
                        //                firstName: {editable: true, type: "string", validation: {required: false}},
                        //                lastName: {editable: true, type: "string", validation: {required: false}},
                        //                fullName:  {editable: true, type: "string", validation: {required: false}},
                        //                emailAddress: {editable: true, type: "string", validation: {required: true}},
                        //                esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                        //                eeTypeTitle: {type: "string", validation: {required: false}},
                        //                status: {
                        //                    type: "string",
                        //                    validation: {required: false},
                        //                    values: [
                        //                        {text: '1 - Active', value: '1 - Active'},
                        //                        {text: '2 - Termed', value: '2 - Termed'},
                        //                        {text: '3 - No Hire', value: '3 - No Hire'}
                        //                    ]
                        //                },
                        //                isEligibleSupport: {editable: true, type: "boolean"},
                        //                maxClientLoad: {editable: true, type: "number"},
                        //                currentClientLoad: {editable: true, type: "number"},
                        //                hireDate: {editable: true, type: "date", validation: {required: false}},
                        //                termDate: {editable: true, type: "date", validation: {required: false}},
                        //                birthDate: {editable: true, type: "date", validation: {required: false}}
                        //            }
                        //        }
                        //    },
                        //    sort: {field: "lastName", dir: "asc"},
                        //    data: dataSourceRecords
                        //});

                        //grid.dataSource.data(dataSourceRecords);

                        if (self.employeeGridFilter !== null && self.employeeGridFilter !== []) {
                            var filters = [];
                            $.each(self.employeeGridFilter, function (index, value) {
                                filters.push(value);
                            });

                            //console.log("PortalEmployeeGridView:grid:set datasource:filter" + JSON.stringify(filters));
                            grid.dataSource.filter(filters);
                            //grid.dataSource.filter([{field: "status", operator: "eq", value: "1 - Active"}]);
                        } else {
                            grid.dataSource.filter([{field: "status", operator: "eq", value: "1 - Active"}]);
                        }
                    });

                });

            },
            teamLeaderFilter: function (container) {
                var self = this;

                //console.log('PortalEmployeeGridView:teamLeaderFilter:' + JSON.stringify(self.teamLeaderRecords));

                container.element.kendoDropDownList({
                    //autoBind:false,
                    dataValueField: "teamLeaderID",
                    dataTextField: "teamLeaderName",
                    valuePrimitive: true,
                    dataSource: self.teamLeaderRecords,
                    optionLabel: "Select Value"
                });

            },
            statusFilter: function (container) {
                var self = this;

                //console.log('PortalEmployeeGridView:statusFilter:' + JSON.stringify(container));

                var dataSource = [
                    {text: '1 - Active', value: '1 - Active'},
                    {text: '2 - Termed', value: '2 - Termed'},
                    {text: '3 - No Hire', value: '3 - No Hire'}
                ];

                container.element.kendoDropDownList({
                    //autoBind:false,
                    dataValueField: "value",
                    dataTextField: "text",
                    valuePrimitive: true,
                    dataSource: dataSource,
                    optionLabel: "Select Value"
                });

            },
            getTeamLeader: function (id) {
                //console.log('PortalEmployeeGridView:getTeamLeader:teamLeaderID:' + id);

                var self = this;

                var leaders = $.grep(self.teamLeaderRecords, function (element, index) {
                    return element.teamLeaderID == id;
                });

                if (leaders.length > 0) {
                    return leaders[0].teamLeaderName;
                } else {
                    return "--- NONE ---";
                }

            },
            viewEmployeeProfile: function (e) {
                //console.log('PortalEmployeeGridView:viewEmployeeProfile:e:' + e.target);

                var employeeId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    employeeId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    employeeId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('PortalEmployeeGridView:viewEmployeeProfile:e:' + employeeId);
                App.model.set('selectedEmployeeId', employeeId);

                App.router.navigate('employeeProfile', {trigger: true});

            },
            displayNoDataOverlay: function () {
                //console.log('PortalEmployeeGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#emp').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('PortalEmployeeGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#emp').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalEmployeeGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#emp").parent().parent().height();
                this.$("#emp").height(parentHeight);

                parentHeight = parentHeight - 50;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalEmployeeGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalEmployeeGridView:resize:headerHeight:' + headerHeight);
                this.$("#emp").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalEmployeeGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#emp").data('ui-tooltip'))
                    this.$("#emp").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });