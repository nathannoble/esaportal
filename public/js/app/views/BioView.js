define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/bio','models/AdminDataModel', 'views/BioModalView'],
    function (App, Backbone, Marionette, _, $, template,AdminDataModel, BioModalView ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #readMore': 'readMoreClicked'
            },
            initialize: function () {
                //console.log('BioView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('BioView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- BioView:onShow --------');

                this.ID = this.model.get('ID');
                this.fileName = this.model.get('fileName');
                this.title = this.model.get('title');
                this.shortText = this.model.get('shortText');
                this.text = this.model.get('text');
                this.text2 = this.model.get('text2');
                this.subCategory = this.model.get('subCategory');

                this.$('#name').html(this.shortText);
                this.$('#title').html(this.title);
                //this.$('#bioImage').attr('src', "../../" + App.config.PortalName + "/EmployeeBios/" + this.fileName );
                this.$('#bioImage').attr('src', App.config.PortalFiles + "/EmployeeBios/" + this.fileName );

            },
            readMoreClicked: function (e) {

                console.log('BioView:readMoreClicked');

                var options = {
                    ID: this.ID,
                    fileName: this.fileName,
                    title: this.title,
                    shortText:this.shortText,
                    text:this.text,
                    text2:this.text2,
                    subCategory:this.subCategory
                };

                // Open Modal dialog
                //this.bioModalRegion.show(new BioModalView(options));

                App.modal.show(new BioModalView(options));

            },
            onResize: function () {
//                //console.log('BioView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            createAutoClosingAlert: function (selector, html, msDuration) {
                // show the alert
                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                var dz = $("#dragDropHandler");

                //this.isUploadingBio = false;
                //App.model.set('uploadingSpreadCSV', false);

                //wait five seconds and close the alert
                window.setTimeout(function () {
                    dz.slideDown(500);
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, msDuration);
            },
            resize: function () {
//                //console.log('BioView:resize');

            },
            remove: function () {
                //console.log('-------- BioView:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
