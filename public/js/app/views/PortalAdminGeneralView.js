define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalAdminGeneral',
        'views/PortalAdminGeneralGridView', 'views/SubHeaderView'],
    function (App, Backbone, Marionette, $, template, PortalAdminGeneralGridView, SubHeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                adminRegion: "#admin-region",
                subHeaderRegion: "#sub-header"
            },
            initialize: function () {

                //console.log('PortalAdminGeneralView:initialize');

                _.bindAll(this);

            },
            onRender: function () {

                //console.log('PortalAdminGeneralView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminGeneralView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    this.subHeaderRegion.show(new SubHeaderView({
                        majorTitle: "Admin Links",
                        minorTitle: "",
                        page: "Admin Links",
                        showDateFilter: false,
                        showStatusFilter: false,
                        showEmpStatusFilter: false,
                        showClientReportFilter: false,
                        showViewsFilter: false,
                        showCompanyFilter: false,
                        showClientFilter: false,
                        showTeamLeaderFilter: false,
                        showPayrollProcessingDate: false
                    }));
                    this.adminRegion.show(new PortalAdminGeneralGridView());
                }

            },
            resize: function () {
                //console.log('PortalAdminGeneralView:resize');
            },
            remove: function () {
                //console.log("--------PortalAdminGeneralView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });