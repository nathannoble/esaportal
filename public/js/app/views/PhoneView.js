define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/phone','models/PhoneModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            initialize: function () {
                //console.log('PhoneView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('PhoneView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- PhoneView:onShow --------');
                
                this.employeeID = this.model.get('employeeID');
                this.esaExtension = this.model.get('esaExtension');
                this.phoneName = this.model.get('firstName') + " " + this.model.get('lastName');

                this.$('#name').html(this.phoneName);
                this.$('#phoneExt').html(this.esaExtension);

           },

            onResize: function () {
//                //console.log('PhoneView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('PhoneView:resize');

            },
            remove: function () {
                //console.log('-------- PhoneView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#phoneRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
