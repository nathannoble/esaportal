define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/dashboardSummaryWidget', 'kendo/kendo.data'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #toggleCompTime': 'onToggleCompTimeClicked'
            },
            initialize: function (options) {
                //console.log('DashboardSummaryWidgetView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "My Team Stats"};
                }

                console.log('DashboardSummaryWidgetView:initialize:type:' + this.options.type);
                this.listenTo(App.vent, App.Events.ModelEvents.MyEmployeeIdChanged, this.onMyEmployeeIdChanged);

                this.showCompTime = App.model.get('showCompTime');
                console.log('DashboardSummaryWidgetView:initialize:showCompTime:' + this.showCompTime);


            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- DashboardSummaryWidgetView:onShow ----------');

                var self = this;

                self.$("#statsTitle").html(this.options.type);
                if (this.options.type === "My Team Stats") {
                    //this.$el.css('background-color', '#5287A6');
                    this.$el.addClass("bg-blue");
                    this.$('.box-header').css("color", "white");
                    //this.$el.removeClass("bg-gray-light");
                } else {
                    this.$el.removeClass("bg-blue");
                    this.$('.box-header').css("color", "black");
                    this.$el.css('background-color', '#f4f4f4');
                    this.$('.comp-time').hide();
                }

                if (self.showCompTime === true) {
                    $('.toggleCheckBox').prop('checked', true);
                    $('.toggle').prop('title', "Show Comp Time");
                } else {
                    $('.toggleCheckBox').prop('checked', false);
                    $('.toggle').prop('title', "Don't Show Comp Time");
                }

                this.getData();
            },
            onToggleCompTimeClicked: function (e) {
                console.log('DashboardSummaryWidgetView:onToggleCompTimeClicked');

                var app = App;

                if (e.target.checked === true) {
                    $('.toggleCheckBox').prop('checked', true);
                    $('.toggle').prop('title', "Show Comp Time");
                    app.model.set('showCompTime',true);
                } else {
                    $('.toggleCheckBox').prop('checked', false);
                    $('.toggle').prop('title', "Don't Show Comp Time");
                    app.model.set('showCompTime',false);
                }

            },
            onMyEmployeeIdChanged: function () {

                this.getData();
            },
            getData: function () {
                //console.log('DashboardSummaryWidgetView:getData');

                var self = this;
                var app = App;

                this.leaderID = App.model.get('myTeamLeaderId'); //67;
                this.employeeID = App.model.get('myEmployeeId'); //67;
                this.isTeamLeader = App.model.get('isTeamLeader'); //67;

                if (this.employeeID !== null && this.employeeID !== undefined) {
                    this.employeeID = parseInt(App.model.get('myEmployeeId'), 0);
                }
                if (this.leaderID !== null && this.leaderID !== undefined) {
                    if (this.isTeamLeader === true || this.isTeamLeader === "true") {
                        this.leaderID = parseInt(App.model.get('myEmployeeId'), 0);
                    } else {
                        this.leaderID = parseInt(App.model.get('myTeamLeaderId'), 0);
                    }
                }
                console.log('DashboardSummaryWidgetView:getData:employeeID:teamLeaderID:' + this.employeeID + ":" + this.leaderID);

                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;
                this.month = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter); //11;

                var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                this.startDate = moment(startDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                this.endDate = moment(endDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                // Team Leaders

                //    SELECT   employeeID, firstName, Lastname
                //FROM      tblProgressEmployees
                //WHERE     isTeamLeader = 1
                //AND employeeID <> 74 <!---Do not show Lanise--->

                this.employeeDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('DashboardSummaryWidgetView:employeeDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'DashboardSummaryWidgetView:employeeDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    emailAddress: {editable: true, type: "string", validation: {required: true}},
                                    esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                    eeTypeTitle: {type: "string", validation: {required: false}},
                                    status: {
                                        type: "string",
                                        validation: {required: false},
                                        values: [
                                            {text: '1 - Active', value: '1 - Active'},
                                            {text: '2 - Termed', value: '2 - Termed'},
                                            {text: '3 - No Hire', value: '3 - No Hire'}
                                        ]
                                    },
                                    isTeamLeader: {editable: true, type: "boolean"},
                                    isEligibleSupport: {editable: true, type: "boolean"},
                                    maxClientLoad: {editable: true, type: "number"},
                                    currentClientLoad: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        sort: {field: "employeeID", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('DashboardSummaryWidgetView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('DashboardSummaryWidgetView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);
                        },
                        error: function (e) {
                            //console.log('PortalEmployeeGridView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);
                        },
                        change: self.onEmployeeDataSourceChange
                    });

                this.progressClientDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                },
                                complete: function (e) {
                                    console.log('DashboardSummaryWidgetView:progressClientDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'DashboardSummaryWidgetView:progressClientDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "clientID",
                                fields: {
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    status: {editable: true, type: "string", validation: {required: false}},
                                    planHours: {type: "number"},
                                    schedulingHours: {type: "number"},
                                    taskTargetLow: {type: "number"},
                                    taskTargetHigh: {type: "number"},
                                    mtgTargetLowDec: {type: "number"},
                                    mtgTargetHigh: {type: "number"},
                                    teamLeaderID: {editable: false, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        //sort: {field: "clientID", dir: "asc"},
                        requestStart: function () {
                            //console.log('   DashboardSummaryWidgetView:requestStart');

                            kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    DashboardSummaryWidgetView:requestEnd');
                            //kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   DashboardSummaryWidgetView:error');
                            kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);

                            //self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        change: self.onProgressClientDataSourceChange
                    });

                if (this.options.type === "My Stats") {
                    this.restDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            transport: {
                                type: "odata",
                                read: {
                                    url: function () {
                                        var url = App.config.DataServiceURL + "/rest/GetMyTimeCardDetail/" + self.startDate + "/" + self.endDate + "/" + self.employeeID;
                                        return url;
                                    },
                                    complete: function (e) {
                                        console.log('DashboardSummaryWidgetView:restDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'DashboardSummaryWidgetView:restDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    type: "GET",
                                    dataType: "json"
                                }
                                //autoBind: false,
                            },
                            schema: {
                                //data: "value",
                                //total: function (data) {
                                //    return data['odata.count'];
                                //},
                                model: {
                                    id: "employeeID",
                                    fields: {

                                        intTimerID: {editable: false, type: "number"},
                                        companyID: {editable: false, type: "number"},
                                        clientID: {editable: false, type: "number"},
                                        employeeID: {editable: false, type: "number"},
                                        serviceID: {editable: false, type: "number"},
                                        workDate: {
                                            editable: true,
                                            type: "date"
                                            //format: "{0:yyyy-MM-ddTHH}",
                                            //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                        },
                                        startTime: {
                                            editable: true,
                                            type: "date"
                                            //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                        },
                                        stopTime: {
                                            editable: true,
                                            type: "date"
                                            //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                        },
                                        clientCompany: {editable: true, type: "string", validation: {required: false}},
                                        clientName: {editable: true, type: "string", validation: {required: false}},
                                        customerName: {editable: true, type: "string", validation: {required: true}},
                                        employeeName: {editable: true, type: "string", validation: {required: false}},
                                        serviceItem: {type: "string", validation: {required: false}},
                                        tasks: {editable: true, type: "number"},
                                        meetings: {editable: true, type: "number"},
                                        timeWorked: {editable: true, type: "number"},
                                        timerNote: {editable: true, type: "string", validation: {required: false}},
                                        mgrNote: {editable: true, type: "string", validation: {required: false}},
                                        timerAction: {editable: true, type: "string", validation: {required: false}},
                                        recordType: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            //group: { field: "clientName" },
                            sort: [
                                {field: "clientName", dir: "asc"},
                                {field: "workDate", dir: "asc"}
                            ],
                            //filter: {field: "lastName", operator: "neq", value: ""},
                            requestStart: function (e) {
                                //console.log('DashboardSummaryWidgetView:dataSource:request start:');
                                kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('DashboardSummaryWidgetView:dataSource:request end:');
                                //var data = this.data();
                                //kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);
                            },
                            change: function (e) {
                                //console.log('DashboardSummaryWidgetView:dataSource:change:');

                                var data = this.data();
                                if (data.length > 0) {

                                } else {
                                    //self.displayNoDataOverlay();
                                }
                            },
                            error: function (e) {
                                //console.log('DashboardSummaryWidgetView:dataSource:error');
                                kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);
                            }
                        });
                } else {
                    this.restDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            transport: {
                                type: "odata",
                                read: {
                                    url: function () {
                                        var url = App.config.DataServiceURL + "/rest/GetTeamTimeCardDetail/" + self.startDate + "/" + self.endDate + "/" + self.leaderID;
                                        return url;
                                    },
                                    complete: function (e) {
                                        console.log('DashboardSummaryWidgetView:restDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'DashboardSummaryWidgetView:restDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    type: "GET",
                                    dataType: "json"
                                }
                                //autoBind: false,
                            },
                            schema: {
                                //data: "value",
                                //total: function (data) {
                                //    return data['odata.count'];
                                //},
                                model: {
                                    id: "employeeID",
                                    fields: {

                                        intTimerID: {editable: false, type: "number"},
                                        companyID: {editable: false, type: "number"},
                                        clientID: {editable: false, type: "number"},
                                        employeeID: {editable: false, type: "number"},
                                        serviceID: {editable: false, type: "number"},
                                        workDate: {
                                            editable: true,
                                            type: "date"
                                            //format: "{0:yyyy-MM-ddTHH}",
                                            //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                        },
                                        startTime: {
                                            editable: true,
                                            type: "date"
                                            //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                        },
                                        stopTime: {
                                            editable: true,
                                            type: "date"
                                            //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                        },
                                        clientCompany: {editable: true, type: "string", validation: {required: false}},
                                        clientName: {editable: true, type: "string", validation: {required: false}},
                                        customerName: {editable: true, type: "string", validation: {required: true}},
                                        employeeName: {editable: true, type: "string", validation: {required: false}},
                                        serviceItem: {type: "string", validation: {required: false}},
                                        tasks: {editable: true, type: "number"},
                                        meetings: {editable: true, type: "number"},
                                        timeWorked: {editable: true, type: "number"},
                                        timerNote: {editable: true, type: "string", validation: {required: false}},
                                        mgrNote: {editable: true, type: "string", validation: {required: false}},
                                        timerAction: {editable: true, type: "string", validation: {required: false}},
                                        recordType: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            //group: { field: "clientName" },
                            sort: [
                                {field: "clientName", dir: "asc"},
                                {field: "workDate", dir: "asc"}
                            ],
                            //filter: {field: "lastName", operator: "neq", value: ""},
                            requestStart: function (e) {
                                //console.log('DashboardSummaryWidgetView:dataSource:request start:');
                                kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('DashboardSummaryWidgetView:dataSource:request end:');
                                //var data = this.data();
                                //kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);
                            },
                            change: function (e) {
                                //console.log('DashboardSummaryWidgetView:dataSource:change:');

                                var data = this.data();
                                if (data.length > 0) {

                                } else {
                                    //self.displayNoDataOverlay();
                                }
                            },
                            error: function (e) {
                                //console.log('DashboardSummaryWidgetView:dataSource:error');
                                kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);
                            }
                        });
                }

                if (this.options.type === "My Stats") {
                    if (self.employeeID !== null && self.employeeID !== undefined) {
                        var mySumTotalTime = app.model.get('mySumTotalTime');
                        if (true) { //mySumTotalTime === null) {
                            self.restDataSource.fetch(function (data) {
                                var empState = {};
                                var empFilters = [];
                                empFilters.push({field: "employeeID", operator: "eq", value: self.employeeID});
                                empState.filter = empFilters;
                                self.employeeDataSource.query(empState);
                            });
                        } else {

                            var mySumTotalMeetings = app.model.get('mySumTotalMeetings');
                            var mySumMeetingsTarget = app.model.get('mySumMeetingsTarget');
                            var myMtgPercent = app.model.get('myMtgPercent');

                            var myMtgAverage = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(mySumTotalMeetings / mySumTotalTime));
                            if ($.isNumeric(myMtgAverage) !== true) {
                                console.log('   DashboardSummaryWidgetView:error:myMtgAverage:mySumTotalTime:' + myMtgAverage + ":" + mySumTotalTime);
                                myMtgAverage = 0;
                            }

                            self.$('#mtgAverage').html(myMtgAverage);
                            self.$('#actualMtg').html(mySumTotalMeetings);
                            self.$('#targetMtg').html(mySumMeetingsTarget);
                            self.$('#percentMtg').html(myMtgPercent);

                        }
                    }
                } else {
                    if (self.leaderID !== null && self.leaderID !== undefined) {
                        var teamSumTotalTime = app.model.get('teamSumTotalTime');
                        if (true) { //teamSumTotalTime === null) {
                            self.restDataSource.fetch(function (data) {
                                var empState = {};
                                var empFilters = [];
                                empFilters.push({field: "employeeID", operator: "eq", value: self.leaderID});
                                empState.filter = empFilters;
                                self.employeeDataSource.query(empState);
                            });
                        } else {

                            var teamSumTotalMeetings = app.model.get('teamSumTotalMeetings');
                            var teamSumMeetingsTarget = app.model.get('teamSumMeetingsTarget');
                            var teamMtgPercent = app.model.get('teamMtgPercent');

                            var teamMtgAverage = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(teamSumTotalMeetings / teamSumTotalTime));
                            if ($.isNumeric(teamMtgAverage) !== true) {
                                teamMtgAverage = 0;
                            }
                            self.$('#mtgAverage').html(teamMtgAverage);
                            self.$('#actualMtg').html(teamSumTotalMeetings);
                            self.$('#targetMtg').html(teamSumMeetingsTarget);
                            self.$('#percentMtg').html(teamMtgPercent);

                        }
                    }
                }
            },
            onEmployeeDataSourceChange: function (e) {
                var self = this;
                var empDataRecord = self.employeeDataSource.data()[0];

                //console.log('DashboardSummaryWidgetView:onEmployeeDataSourceChange:leaderID:' + empDataRecord.leaderID);

                var state = {};
                var filters = [];
                if (self.options.type === "My Team Stats") {
                    filters.push({field: "teamLeaderID", operator: "eq", value: empDataRecord.employeeID});
                } else {
                    filters.push({field: "schedulerID", operator: "eq", value: empDataRecord.employeeID});
                }
                filters.push({field: "status", operator: "contains", value: "4"});

                state.filter = {
                    logic: "and",
                    filters: filters
                };
                self.progressClientDataSource.query(state);

            },
            onProgressClientDataSourceChange: function (e) {

                var self = this;
                var data = this.progressClientDataSource.data();

                //console.log('DashboardSummaryWidgetView:onProgressClientDataSourceChange:data length:' + data.length);
                //console.log('DashboardSummaryWidgetView:onProgressClientDataSourceChange:1st record:' + JSON.stringify(data[0]));
                //console.log('DashboardSummaryWidgetView:onProgressClientDataSourceChange:2nd record:' + JSON.stringify(data[1]));

                var clientRecords = [];

                //<cfquery name="getTargets" dbtype="query" maxrows="1">
                //    SELECT   SUM(mtgTargetLowDec  * planHours) AS meetingsTarget
                //    FROM     getTeamClients
                //</cfquery>

                $.each(data, function (index, value) {
                    var record = value;

                    //if (record.status.indexOf("4 - Active") > -1) {
                    //console.log('DashboardSummaryWidgetView:onProgressClientDataSourceChange:clientData record:' + JSON.stringify(record));
                    clientRecords.push({
                        clientID: record.clientID,
                        mtgTarget: record.mtgTargetLowDec,
                        planHours: record.planHours,
                        schedulingHours: record.schedulingHours,
                        teamLeaderID: record.teamLeaderID,
                        status: record.status
                    });
                    //}

                });

                self.parseDataSourceResults(clientRecords);
            },
            parseDataSourceResults: function (clientRecords) {

                var self = this;
                var app = App;

                var data = this.restDataSource.data();

                //console.log('DashboardSummaryWidgetView:parseDataSourceResults:datarecord:' + JSON.stringify(datarecord));
                //console.log('DashboardSummaryWidgetView:parseDataSourceResults:clientRecords.length:' + clientRecords.length);

                // ProgressClientData records
                var sumTotalMeetings = 0;
                //var sumTotalMeetingsWithComp = 0;
                var sumMeetingsTarget = 0;
                var sumPlanHours = 0;
                var sumTotalTime = 0;
                var sumCompTime = 0;
                //var sumTotalTimeWithComp = 0;
                //var sumAdminTime = 0;
                //var sumCompTime = 0;
                var sumSchedulerTime = 0;


                // Get(My|Team)TimeCardDetail records
                //{
                // "workDate":"2018-05-15","startTime":null,"stopTime":null,"timeWorked":3.30,"clientID":2625,
                // "clientName":"z, zLeadership","customerName":"zLeadership","companyID":103,"clientCompany":"ESA",
                // "employeeID":403,"employeeName":"Elias, Brandi","serviceID":1005,
                // "serviceItem":"Administration - To Office    ","entryType":"M",
                // "intTimerID":668825,"idCount":1,"tasks":0,"meetings":0,"recordType":"NORM-STOP","timerAction":"none",
                // "timerNote":"","mgrNote":null,"editedByName":null
                // }

                $.each(data, function (clientDataIndex, value) {
                    if (value.serviceID === 1001) {
                        sumSchedulerTime += value.timeWorked;
                    } else {
                        sumCompTime += value.timeWorked;
                    }
                });

                // ProgressClient records
                var thisClientHours = 0;

                $.each(clientRecords, function (clientIndex, clientValue) {
                    var clientRecord = clientValue;

                    thisClientHours = 0;

                    $.each(data, function (clientDataIndex, value) {

                        var clientID = value.clientID;

                        if (clientRecord.clientID === clientID && ((self.options.type === "My Team Stats" && value.clientTeamLeaderID === self.leaderID && self.leaderID !== 0) || (self.options.type === "My Stats" && self.leaderID !== 0))) {

                            // Choose to show comp time or not
                            if (self.showCompTime === true) {
                                if (value.serviceID === 1001 || value.serviceID === 1011 || value.serviceID === 1008) {
                                    sumTotalMeetings += value.meetings;
                                }
                                if (clientRecord.schedulingHours === 0) {
                                    thisClientHours += value.timeWorked;
                                }
                            } else {

                                if (value.serviceID === 1001 || value.serviceID === 1011) {
                                    sumTotalMeetings += value.meetings;
                                }
                                if (clientRecord.schedulingHours === 0 && value.serviceID !== 1008) {
                                    thisClientHours += value.timeWorked;
                                }
                            }
                        }
                    });

                    if (clientRecord.schedulingHours === 0) {
                        sumMeetingsTarget += clientRecord.mtgTarget * thisClientHours;
                        sumPlanHours += thisClientHours;
                        //console.log('DashboardSummaryWidgetView:parseDataSourceResults:clientID:' + clientRecord.clientID);
                        //console.log('DashboardSummaryWidgetView:parseDataSourceResults:sumMeetingsTarget:' + sumMeetingsTarget);
                        //console.log('DashboardSummaryWidgetView:parseDataSourceResults:thisClientHours:planHoursTotal' + thisClientHours + ":" + sumPlanHours);
                        //console.log('DashboardSummaryWidgetView:parseDataSourceResults:hourly plan');

                    } else {
                        sumMeetingsTarget += clientRecord.mtgTarget * clientRecord.planHours;
                        sumPlanHours += clientRecord.planHours;
                        //console.log('DashboardSummaryWidgetView:parseDataSourceResults:clientID:' + clientRecord.clientID);
                        //console.log('DashboardSummaryWidgetView:parseDataSourceResults:sumMeetingsTarget:' + sumMeetingsTarget);
                        //console.log('DashboardSummaryWidgetView:parseDataSourceResults:clientRecord.planHours:planHoursTotal' + clientRecord.planHours + ":" + sumPlanHours);
                        //console.log('DashboardSummaryWidgetView:parseDataSourceResults:non-hourly plan');
                    }
                });

                sumMeetingsTarget = app.Utilities.numberUtilities.getFormattedNumberWithRoundingNoComma(sumMeetingsTarget);

                //console.log('DashboardSummaryWidgetView:parseDataSourceResults:sumTotalMeetings:' + sumTotalMeetings);
                //console.log('DashboardSummaryWidgetView:parseDataSourceResults:summtgTargetLowDec:' + sumMeetingsTarget);
                //console.log('DashboardSummaryWidgetView:parseDataSourceResults:sumPlanHours:' + sumPlanHours);

                var mtgPercent = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalMeetings / sumMeetingsTarget * 100));
                if ($.isNumeric(mtgPercent) !== true) {
                    mtgPercent = 0;
                }

                //console.log('DashboardSummaryWidgetView:parseDataSourceResults:sumTotalMeetings:sumMtgTarget' + sumTotalMeetings + ":" + sumMeetingsTarget);

                var mtgAverage = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalMeetings / (sumSchedulerTime)));
                if (self.showCompTime === true) {
                    mtgAverage = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalMeetings / (sumSchedulerTime+sumCompTime)));
                }

                if ($.isNumeric(mtgAverage) !== true) {
                    //console.log('DashboardSummaryWidgetView:error:mtgAverage:sumTotalMeetings:' + sumTotalMeetings + "/" + sumSchedulerTime);
                    mtgAverage = 0;
                }

                kendo.ui.progress(self.$("#loadingDashboardSummaryWidget"), false);

                self.$('#mtgAverage').html(mtgAverage);
                self.$('#actualMtg').html(sumTotalMeetings);
                self.$('#targetMtg').html(sumMeetingsTarget);
                self.$('#percentMtg').html(mtgPercent);

                if (self.options.type === "My Stats") {
                    app.model.set('mySumTotalTime', sumSchedulerTime);
                    app.model.set('mySumTotalMeetings', sumTotalMeetings);
                    app.model.set('mySumMeetingsTarget', sumMeetingsTarget);
                    app.model.set('myMtgPercent', mtgPercent);
                } else {
                    app.model.set('teamSumTotalTime', sumSchedulerTime);
                    app.model.set('teamSumTotalMeetings', sumTotalMeetings);
                    app.model.set('teamSumMeetingsTarget', sumMeetingsTarget);
                    app.model.set('teamMtgPercent', mtgPercent);
                }

            },
            hideNoDataOverlay: function () {
                //console.log('DashboardSummaryWidgetView:hideNodDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the tile
                this.$('.tile-manage').css('display', 'block');
            },
            mouseEnter: function () {
//                //console.log('DashboardSummaryWidgetView:mouseEnter');

                self.$('#DashboardSummaryWidgetTile').find('.tile-icon').addClass('clr-selected');
            },
            mouseLeave: function () {
//                //console.log('DashboardSummaryWidgetView:mouseLeave');

                self.$('#DashboardSummaryWidgetTile').find('.tile-icon').removeClass('clr-selected');
            },
            onResize: function () {
//                //console.log('DashboardSummaryWidgetView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('DashboardSummaryWidgetView:resize');

            },
            remove: function () {
                //console.log('---------- DashboardSummaryWidgetView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });