define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/ProgressCompanyTable', 'views/ProgressCompanyGridView'],
    function (App, Backbone, Marionette, $, template, ProgressCompanyGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                companyRegion1: "#company-region-1"
            },
            initialize: function () {

                //console.log('companyCompanyTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('ProgressCompanyTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressCompanyTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                this.userType = App.model.get('userType');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    if (this.userType === 2) {
                        this.hideCompanies = App.model.get('hideCompanies');
                        if (this.hideCompanies) {
                            console.log('EmployeeProfileLayoutView:onShow:you do not have permissions to see the companies list.');
                            App.model.set('callbackErrorMessage',5);
                            //alert('You do not have permissions to see the companies list.');
                            //history.back();
                        }
                    }

                    this.companyRegion1.show(new ProgressCompanyGridView());
                }


            },
            resize: function () {
                //console.log('ProgressCompanyTableView:resize');

                if (this.companyRegion1.currentView)
                    this.companyRegion1.currentView.resize();

            },
            remove: function () {
                //console.log("--------ProgressCompanyTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });