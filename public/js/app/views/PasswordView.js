define(['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/password',
        'jquery.easing', 'kendo/kendo.data'],
    function (App, Backbone, Marionette, $, Model, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #esaLogin': 'esaLoginClicked',
                'keydown': 'keyAction',
                'visibilitychange': 'onVisibilityChanged'
            },
            initialize: function (options) {

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.userId = App.model.get('userId');
                // this.userKey = App.model.get('userKey');
                this.userLogin = App.model.get('userLogin');
                console.log('PasswordView:initialize:userId:' + this.userId);
                console.log('PasswordView:initialize:userLogin:' + this.userLogin);

            },
            onRender: function () {
//                //console.log('---------- PasswordView:onRender ----------');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.

                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
//                //console.log('---------- PasswordView:onShow ----------');

                var self = this;

                $('#esaNewPassword').empty();
                $('#esaConfirmPassword').empty();

                $('#esaNewPassword').attr("autocomplete", "off");
                $('#esaConfirmPassword').attr("autocomplete", "off");

                $('#esaLogin').attr("title", "Password must:  \n" +
                    "Have a minimum of 8 characters. \n" +
                    "Have at least 1 lower and 1 upper case letter. \n" +
                    "Have at least 1 number. \n" +
                    "Have at least 1 special character. \n" +
                    "Be different from 12 previous passwords.");

                var qs = this.getQueryStringParameters();
                console.log('PasswordView:onShow:qs:' + JSON.stringify(qs));

                this.resetId = qs.resetId;

                if (this.resetId === undefined || this.resetId === null) {
                    self.createAutoClosingAlert($("#startError"), "<strong>Password Change Failure.</strong> Password reset code is incorrect.");
                } else {
                    // odata call to see if user/pw exists
                    this.userDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/PortalUserTable?$select=userLogin,userPassword,userID,userType,userKey,firstName,lastName,loginCount,lastLogin,lastLogout,inactive,lastPasswordChange,prevPasswords",
                                    complete: function (e) {
                                        console.log('PasswordView:userDataSource:read:complete');
                                     },
                                    error: function (e) {
                                        var message = 'PasswordView:userDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                                    },
                                    complete: function (e) {
                                        console.log('PasswordView:userDataSource:update:complete');
                                     },
                                    error: function (e) {
                                        var message = 'PasswordView:userDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "userKey",
                                    fields: {
                                        userLogin: {editable: false, type: "string"},
                                        userPassword: {editable: true, type: "string"},
                                        firstName: {editable: false, type: "string"},
                                        lastName: {editable: false, type: "string"},
                                        userID: {editable: false, type: "string"},
                                        userKey: {editable: false, type: "string"},
                                        userType: {editable: false, defaultValue: 1, type: "number"},
                                        loginCount: {editable: true, type: "number"},
                                        lastLogin: {editable: true, type: "date"},
                                        lastLogout: {editable: true, type: "date"},
                                        lastPasswordChange: {editable: true, type: "date"},
                                        prevPasswords: {editable: false, type: "string"},
                                        passwordCode: {editable: false, type: "string"},
                                        inactive: {editable: false, type: "boolean"}
                                    }
                                }
                            },
                            serverPaging: true,
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                kendo.ui.progress($("#loadingEsaUser"), true);
                            },
                            requestEnd: function () {
                                kendo.ui.progress($("#loadingEsaUser"), false);
                            },
                            filter: [
                                {field: "passwordCode", operator: "eq", value: this.resetId.toString()}
                            ]
                        });

                    this.userDataSource.fetch(function () {
                        var data = this.data();
                        var count = data.length;

                        if (count === 0) {
                            console.log('PasswordView:onShow:userPassword not found');
                            self.createAutoClosingAlert($("#startError"), "<strong>Password Change Failure.</strong> Password reset code is incorrect.");
                        }
                    });
                }


                // Setup the container
                this.resize();

            },
            keyAction: function (e) {
                //console.log('PasswordView:keyAction:' + e.which);
                if (e.which === 13) {
                    // enter has been pressed
                    this.esaLoginClicked();
                }

            },
            getQueryStringParameters: function() {
                var url = window.location.href;
                var queryString = url.split('?')[1] || "",
                    params = {},
                    paramParts = queryString.split(/&|=/),
                    length = paramParts.length,
                    idx = 0;

                for (; idx < length; idx += 2) {
                    if (paramParts[idx] !== "") {
                        params[decodeURIComponent(paramParts[idx])] = decodeURIComponent(paramParts[idx + 1]);
                    }
                }

                return params;
            },
            esaLoginClicked: function (e) {

                var self = this;
                var app = App;

                this.esaUser = this.$('#esaUser').val();
                this.esaOldPassword = this.resetId; //this.$('#esaOldPassword').val();
                this.esaNewPassword = this.$('#esaNewPassword').val();
                this.esaConfirmPassword = this.$('#esaConfirmPassword').val();

                this.userDataSource2 = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalUserTable?$select=userLogin,userPassword,userID,userType,userKey,firstName,lastName,loginCount,lastLogin,lastLogout,inactive,lastPasswordChange,prevPasswords,lastCompanyMessagesView,lastESAMediaView",
                                complete: function (e) {
                                    console.log('PasswordView:userDataSource2:read:complete');
                                 },
                                error: function (e) {
                                    var message = 'PasswordView:userDataSource2:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                                },
                                complete: function (e) {
                                    console.log('PasswordView:userDataSource2:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PasswordView:userDataSource2:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "userKey",
                                fields: {
                                    userLogin: {editable: false, type: "string"},
                                    userPassword: {editable: true, type: "string"},
                                    firstName: {editable: false, type: "string"},
                                    lastName: {editable: false, type: "string"},
                                    userID: {editable: false,  type: "string"},
                                    userKey: {editable: false,  type: "string"},
                                    userType: {editable: false, defaultValue: 1, type: "number"},
                                    loginCount: {editable: true, type: "number"},
                                    lastLogin: {editable: true, type: "date"},
                                    lastLogout: {editable: true, type: "date"},
                                    lastPasswordChange: {editable: true, type: "date"},
                                    inactive: {editable: false, type: "boolean"},
                                    prevPasswords: {editable: false, type: "string"},
                                    lastCompanyMessagesView: {editable: true, type: "date", validation: {required: false}},
                                    lastESAMediaView: {editable: true, type: "date", validation: {required: false}}
                                }
                            }
                        },
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            kendo.ui.progress($("#loadingEsaUser"), true);
                        },
                        requestEnd: function () {
                            kendo.ui.progress($("#loadingEsaUser"), false);
                        },
                        filter: [
                            {field: "userLogin", operator: "eq", value: this.esaUser.toString()},
                            {field: "passwordCode", operator: "eq", value: this.esaOldPassword.toString()}
                        ]
                    });
                this.userDataSource2.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count === 0) {
                        console.log('PasswordView:esaLoginClicked:userIdNotFound');
                        self.createAutoClosingAlert($("#startError"), "<strong>Password Change Failure.</strong> User or Reset Code not found.  Please try again.");
                    } else if (self.esaConfirmPassword !== self.esaNewPassword) {
                        console.log("PasswordView:esaLoginClicked:new passwords don't match");
                        self.createAutoClosingAlert($("#startError"), "<strong>Password Change Failure.</strong> New passwords don't match each other.  Please try again.");
                    }
                    //else if (self.esaConfirmPassword === self.esaOldPassword) {
                    //    console.log("PasswordView:esaLoginClicked:old and new passwords cannot be the same.");
                    //    self.createAutoClosingAlert($("#startError"), "<strong>Password Change Failure.</strong> The new password is the same as the old password.  Please try again.");
                    //}
                    else {
                        console.log('PasswordView:esaLoginClicked:Set:userId:new password:' + self.esaNewPassword);

                        // Validation
                        //(1) minimum of 8 characters
                        //(2) at least 1 upper case, 1 lower case
                        //(3) at least 1 number
                        //(4) at least 1 special character
                        var passw = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
                        var lengthVal = self.esaNewPassword.match(/^.{8,}$/);
                        var lcVal= self.esaNewPassword.match(/^(?=.*[a-z])/);
                        var ucVal = self.esaNewPassword.match(/^(?=.*[A-Z])/);
                        var numVal = self.esaNewPassword.match(/^(?=.*\d)/);
                        var scVal = self.esaNewPassword.match(/^(?=.*[!@#$%^&*])/);

                        if(lengthVal === null) {
                            console.log("PasswordView:esaLoginClicked:new password failed length validation.");
                            self.createAutoClosingAlert($("#startError"), "<strong>Failure. Password must:</strong><br>" +
                            "<strong>Have a minimum of 8 characters.</strong> <br>" +
                            "Have at least 1 lower and 1 upper case letter. <br>" +
                            "Have at least 1 number. <br>" +
                            "Have at least 1 special character  (commas are not permitted). <br>" +
                            "Be different from 12 previous passwords.");
                        } else if(lcVal === null) {
                            console.log("PasswordView:esaLoginClicked:new password failed lower case validation.");
                            self.createAutoClosingAlert($("#startError"),"<strong>Failure. Password must:</strong><br>" +
                                "Have a minimum of 8 characters. <br>" +
                                "<strong>Have at least 1 lower and 1 upper case letter. </strong><br>" +
                                "Have at least 1 number. <br>" +
                                "Have at least 1 special character  (commas are not permitted). <br>" +
                                "Be different from 12 previous passwords.");
                        }  else if(ucVal === null) {
                            console.log("PasswordView:esaLoginClicked:new password failed upper case validation.");
                            self.createAutoClosingAlert($("#startError"), "<strong>Failure. Password must:</strong><br>" +
                                "Have a minimum of 8 characters. <br>" +
                                "<strong>Have at least 1 lower and 1 upper case letter. </strong><br>" +
                                "Have at least 1 number. <br>" +
                                "Have at least 1 special character  (commas are not permitted). <br>" +
                                "Be different from 12 previous passwords.");
                        }  else if(numVal === null) {
                            console.log("PasswordView:esaLoginClicked:new password failed number validation.");
                            self.createAutoClosingAlert($("#startError"), "<strong>Failure. Password must:</strong><br>" +
                                "Have a minimum of 8 characters. <br>" +
                                "Have at least 1 lower and 1 upper case letter. <br>" +
                                "<strong>Have at least 1 number.</strong> <br>" +
                                "Have at least 1 special character  (commas are not permitted). <br>" +
                                "Be different from 12 previous passwords.");
                        }  else if(scVal === null || self.esaNewPassword.indexOf(',') > -1) {
                            console.log("PasswordView:esaLoginClicked:new password failed special character validation.");
                            self.createAutoClosingAlert($("#startError"), "<strong>Failure. Password must:</strong><br>" +
                                "Have a minimum of 8 characters. <br>" +
                                "Have at least 1 lower and 1 upper case letter. <br>" +
                                "Have at least 1 number. <br>" +
                                "<strong>Have at least 1 special character (commas are not permitted).</strong> <br>" +
                                "Be different from 12 previous passwords.");
                        } else {
                            var record = null;
                            var userLogin = null;
                            var userType = null;
                            var userKey = null;
                            var firstName = null;
                            var lastName = null;
                            var userId = null;
                            var inactive = false;
                            var lastPasswordChange = null;
                            var prevPasswords = null;
                            var lastCompanyMessagesView = null;
                            var lastESAMediaView = null;

                            var i = 0;
                            while (userLogin === null || userLogin === undefined || userLogin === "") {
                                record = data[i];
                                userId = data[i].userID;
                                userKey = data[i].userKey;
                                userLogin = data[i].userLogin;
                                userType = data[i].userType;
                                firstName = data[i].firstName;
                                lastName = data[i].lastName;
                                inactive = data[i].inactive;
                                lastPasswordChange = data[i].lastPasswordChange;
                                prevPasswords = data[i].prevPasswords.split(",");
                                lastCompanyMessagesView = data[i].lastCompanyMessagesView;
                                lastESAMediaView = data[i].lastESAMediaView;

                                i++;
                            }

                            console.log('PasswordView:esaLoginClicked:inactive:' + inactive);
                            console.log('PasswordView:esaLoginClicked:lastPasswordChange:' + lastPasswordChange);

                           if (prevPasswords.slice(0,11).indexOf(self.esaNewPassword) > -1) {
                                console.log("PasswordView:esaLoginClicked:new password failed previous 12 passwords validation.");
                                self.createAutoClosingAlert($("#startError"), "<strong>Failure. Password must:</strong><br>" +
                                    "Have a minimum of 8 characters. <br>" +
                                    "Have at least 1 lower and 1 upper case letter. <br>" +
                                    "Have at least 1 number. <br>" +
                                    "Have at least 1 special character (commas are not permitted).<br>" +
                                    "<strong>Be different from 12 previous passwords.</strong>");
                               return;
                            }

                            if (inactive === true) {
                                self.createAutoClosingAlert($("#startError"), "<strong>Login Failure.</strong> This user is inactive.");
                                return;
                            }

                            var offset = (new Date()).getTimezoneOffset();
                            var now = new Date(moment().clone().subtract((offset), 'minutes'));

                            console.log('PasswordView:esaLoginClicked:userLogin:' + userLogin);

                            //alert("Your password was successfully changed.");
                            self.createAutoClosingAlert($("#startSuccess"), "<strong>Success.</strong> Your password was successfully changed.  In a few seconds, you will be logged in to the system.");

                            app.model.set('userKey', parseInt(userKey, 0));
                            app.model.set('userLogin', userLogin);
                            app.model.set('userType', parseInt(userType, 0));
                            app.model.set('userFirstName', firstName);
                            app.model.set('userLastName', lastName);
                            app.model.set('userId', userId);

                            record.set("loginCount", parseInt(record.loginCount, 0) + 1);
                            record.set("lastLogin", now);
                            record.set("lastLogout", null);
                            record.set("userPassword", self.esaNewPassword);
                            record.set("lastPasswordChange", now);
                            record.set("prevPasswords", prevPasswords);
                            record.set("passwordCode", null);

                            if (lastCompanyMessagesView !== null) {
                                lastCompanyMessagesView = new Date(moment(lastCompanyMessagesView).clone().subtract((offset), 'minutes'));
                                lastCompanyMessagesView.setHours(0);
                            }

                            if (lastESAMediaView !== null) {
                                lastESAMediaView = new Date(moment(lastESAMediaView).clone().subtract((offset), 'minutes'));
                                lastESAMediaView.setHours(0);
                            }

                            app.model.set('lastCompanyMessagesView', lastCompanyMessagesView);
                            app.model.set('lastESAMediaView', lastESAMediaView);

                            record.set("lastCompanyMessagesView", lastCompanyMessagesView);
                            record.set("lastESAMediaView", lastESAMediaView);

                            $.when(this.sync()).done(function (e) {

                                window.setTimeout(function () {
                                    // Go to dashboard
                                    app.router.navigate('home', {trigger: true});
                                }, 5000);

                            });
                        }

                    }
                });
            },
            createAutoClosingAlert: function (selector, html) {
                //console.log('PasswordView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 5000);
            },
            onVisibilityChanged: function () {
                //console.log('PasswordView:onVisibilityChanged');

            },
            onResize: function () {
//                //console.log(' PasswordView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                // Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log(' PasswordView:resize');

                var height = $(window).height();
                var width = $(window).width();

                var calculatedHeight = height;
                var minHeight = 600;  // 600 for live site
                var headerHeight = 145;
                if (App.model.get('appBannerVisible') === true) {
                    calculatedHeight = height - headerHeight;
                }
                else {
                    headerHeight = 67;
                    calculatedHeight = height - headerHeight;
                }
                if (calculatedHeight < minHeight)
                    calculatedHeight = minHeight;
                this.$(".home-container").height(calculatedHeight);

                var scrollHeight = $("#esa")[0].scrollHeight;
                var clientHeight = $("#esa")[0].clientHeight;

                // If we have a scroll bar then bad the bottom row so it can come into view
                if (scrollHeight > clientHeight) {
                    this.$(".bottom-row").css('padding-bottom', '38px');
                }
                else {

                    if ((clientHeight - headerHeight) < minHeight) {
                        var paddingBottom = minHeight - (clientHeight - headerHeight);
                        this.$(".bottom-row").css('padding-bottom', paddingBottom + 'px');
                    }
                    else {
                        this.$(".bottom-row").css('padding-bottom', '');
                    }
                }

                // If we are stacked then pad the last column
                if (width < 992) {
                    this.$(".last-column").css('padding-bottom', '38px');
                }
                else {
                    this.$(".last-column").css('padding-bottom', '2px');
                }
                this.$(".home-container").height(calculatedHeight);
            },
            remove: function () {
                //console.log('-------- PasswordView:remove --------');

                // Remove window events
                $(window).off('resize', this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
