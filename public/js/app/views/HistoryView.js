define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/history', 'models/HistoryModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.Layout.extend({
            template: template,
           
            initialize: function () {
                //console.log('HistoryView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('HistoryView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- HistoryView:onShow --------');

                this.schedulerHistoryID = this.model.get('schedulerHistoryID');
                this.schedulerChangeTimeStamp = new Date(this.model.get('schedulerChangeTimeStamp')).toDateString() + " " + new Date(this.model.get('schedulerChangeTimeStamp')).toLocaleTimeString();
                this.userName = this.model.get('firstName') + " " + this.model.get('lastName');

                // this.$('#historyText').html(this.historyText);
                this.$('#historyUserName').html(this.userName);
                this.$('#schedulerChangeTimeStamp').html(this.schedulerChangeTimeStamp);
            },
            onResize: function () {
               //console.log('HistoryView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('HistoryView:resize');

            },
            remove: function () {
                //console.log('-------- HistoryView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#historyRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
