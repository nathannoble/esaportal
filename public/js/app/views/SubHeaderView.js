define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/subHeader', 'views/TimerCalloutWidgetView',
        'daterangepicker', 'kendo/kendo.combobox'], //'adminLTE'
    function (App, Backbone, Marionette, $, template, TimerCalloutWidgetView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #clientProfile': 'onClientProfileClicked',
                'click #calcStats': 'onCalcStatsClicked'
            },
            regions: {
                timerAlertRegion: "#timer-alert"
            },
            initialize: function (options) {
                console.log('SubHeaderView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dateFilter = App.model.get('selectedDateFilter');

                this.statusFilter = App.model.get('selectedStatusFilter');
                if (this.statusFilter === null || this.statusFilter === undefined || this.statusFilter === "") {
                    this.statusFilter = "4 - Active";
                }

                this.empStatusFilter = App.model.get('selectedEmpStatusFilter');
                if (this.empStatusFilter === null || this.empStatusFilter === undefined || this.empStatusFilter === "") {
                    this.empStatusFilter = "1 - Active";
                }

                this.selectedClientReport = App.model.get('selectedClientReport');
                if (this.selectedClientReport === null || this.selectedClientReport === undefined || this.selectedClientReport === "") {
                    this.selectedClientReport = "Client Contact Info";
                }

                this.selectedView = App.model.get('selectedView');
                if (this.selectedView === null || this.selectedView === undefined || this.selectedView === "") {
                    this.selectedView = "Default";
                }

                this.selectedCompanyId = App.model.get('selectedCompanyId'); //12;
                this.selectedClientId = App.model.get('selectedClientId');

                this.myEmployeeId = App.model.get('myEmployeeId');
                this.userType = parseInt(App.model.get('userType'), 0);
                this.hideClients = App.model.get('hideClients');
                this.selectedTeamLeaderId = App.model.get('selectedTeamLeaderId');
                this.myTeamLeaderId = App.model.get('myTeamLeaderId');

                if (this.selectedTeamLeaderId === null) {
                    this.selectedTeamLeaderId = this.myTeamLeaderId;
                }

                App.model.set('timerStatus', "Nothing");

                this.listenTo(App.vent, App.Events.ModelEvents.TimerStatusChanged, this.onTimerStatusChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.MyEmployeeIdChanged, this.onMyEmployeeIdChanged);

                this.options = options;

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('SubHeaderView:onShow');

                var self = this;
                var app = App;

                if (self.options !== null && self.options !== undefined) {
                    self.$('#majorTitle').html(self.options.majorTitle + "<small>" + self.options.minorTitle + "</small>");
                    //self.$('#minorTitle').html(self.options.minorTitle);
                    self.$('#page').html(self.options.page);

                    if (self.options.showDateFilter === true) {
                        self.$('#dateRangeTooltip').show();
                    } else {
                        self.$('#dateRangeTooltip').hide();
                    }

                    if (self.options.showStatusFilter === true) {
                        self.$('#statusFilterTooltip').show();
                        //self.$('#statusFilterTitle').html("<b>Client Status: </b>" + self.statusFilter + '&nbsp;');
                        self.$('#statusFilterTooltip').attr("title", "Client Status: " + self.statusFilter);

                        var statuses = [
                            {value: 'All Status'},
                            {value: '1 - Prospect (Cold)'},
                            {value: '2 - Pending'},
                            {value: '3 - Onboarding'},
                            {value: '4 - Active'},
                            {value: '5 - On Hold'},
                            {value: '6 - Termed'}
                        ];

                        self.$("#statusFilterList").kendoDropDownList({
                            dataTextField: "value",
                            dataValueField: "value",
                            dataSource: statuses,
                            value: self.statusFilter,
                            change: self.onStatusFilterListChanged
                        });
                    } else {
                        self.$('#statusFilterTooltip').hide();
                    }

                    if (self.options.showEmpStatusFilter === true) {
                        self.$('#empStatusFilterTooltip').show();
                        self.$('#empStatusFilterTooltip').attr("title", "Emp Status: " + self.empStatusFilter);

                        var empStatuses = [
                            {value: 'All Status'},
                            {value: '1 - Active'},
                            {value: '2 - Termed'},
                            {value: '3 - No Hire'}
                        ];

                        self.$("#empStatusFilterList").kendoDropDownList({
                            dataTextField: "value",
                            dataValueField: "value",
                            dataSource: empStatuses,
                            value: self.empStatusFilter,
                            change: self.onEmpStatusFilterListChanged
                        });

                        if (self.userType === 1) {  // Hide employee status selection for user level 1 (was level 2 NN 5/18/23)
                            self.$('#empStatusFilterTooltip').hide();
                        }
                    } else {
                        self.$('#empStatusFilterTooltip').hide();
                    }


                    if (self.options.showClientReportFilter === true) {
                        self.$('#clientReportFilterTooltip').show();
                        self.$('#clientReportFilterTooltip').attr("title", "Client Report: " + self.selectedClientReport);

                        var reports = [
                            {value: 'Client Contact Info'},
                            {value: 'Client Passwords'},
                            {value: 'Client Report'},
                            {value: 'Client Report with All'},
                            {value: 'Client Assignments'},
                            {value: 'Task Breakdown Reports'}
                        ];

                        if (self.userType === 3) {
                            reports.push({value: 'Active Client List'});
                        }

                        self.$("#clientReportList").kendoDropDownList({
                            dataTextField: "value",
                            dataValueField: "value",
                            dataSource: reports,
                            value: self.selectedClientReport,
                            change: self.onClientOptionListClicked
                        });

                    } else {
                        self.$('#clientReportFilterTooltip').hide();
                    }

                    if (self.options.showViewsFilter === true) {
                        self.$('#ViewsFilterTooltip').show();
                        self.$('#ViewsFilterTooltip').attr("title", "Client Report: " + self.selectedViews);

                        var views = [
                            {value: 'Default'},
                            {value: 'Client Mailing Address'},
                            {value: 'Sales'},
                            {value: 'Marketing'},
                            {value: 'Management'},
                            {value: 'Onboarding'},
                            {value: 'EOM Report'},
                            {value: 'All Client Fields'}
                        ];

                        self.$("#viewsList").kendoDropDownList({
                            dataTextField: "value",
                            dataValueField: "value",
                            dataSource: views,
                            value: self.selectedView,
                            change: self.onViewOptionListClicked
                        });

                    } else {
                        self.$('#viewsFilterTooltip').hide();
                    }

                    if (self.options.showPayrollProcessingDate === true) {
                        self.$('#payrollDateTooltip').show();

                    } else {
                        self.$('#payrollDateTooltip').hide();
                    }

                    if (self.options.showCompanyFilter === true) {
                        self.$('#clientCompanyTooltip').show();

                    } else {
                        self.$('#clientCompanyTooltip').hide();
                    }

                    if (self.options.showClientFilter === true) {
                        self.$('#clientTooltip').show();
                        if (self.userType === 1) {
                            self.$('#clientProfile').css("color", "#444");
                            self.$('#clientProfile').css("cursor", "default");
                        } else {
                            self.$('#clientProfile').css("color", "#6893AB");
                            self.$('#clientProfile').css("cursor", "pointer");
                        }
                    } else {
                        self.$('#clientTooltip').hide();
                    }

                    if (self.options.showTeamLeaderFilter === true) {
                        self.$('#teamLeaderTooltip').show();
                        if (self.myEmployeeId === 44 || self.myEmployeeId === 75 || self.myEmployeeId === 260) {   // Carol or Mitch or Nathan
                            // || self.userType === 3) {
                            self.$('#calcStatsTooltip').show();
                        } else {
                            self.$('#calcStatsTooltip').hide();
                        }
                        if (self.userType === 2 && self.hideClients === true) {  // Hide team leader selection for user level 2 where hideClients is true
                            self.$('#teamLeaderTooltip').hide();
                            self.$('#calcStatsTooltip').hide();
                        }
                    } else {
                        self.$('#teamLeaderTooltip').hide();
                        self.$('#calcStatsTooltip').hide();
                    }
                }

                this.resize();

                // set date range
                var start = moment().startOf('month');
                var end = moment().endOf('month');

                if (this.dateFilter !== null) {
                    start = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                    end = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                    start = moment(start, 'MM/DD/YYYY').clone().format('MM/DD/YYYY');
                    end = moment(end, 'MM/DD/YYYY').clone().format('MM/DD/YYYY');
                }
                self.$('#dateRange').daterangepicker({
                    "autoApply": true,
                    "ranges": {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                        'Second Half Last Month': [moment().subtract(1, 'month').startOf('month').add(15, 'days'), moment().subtract(1, 'month').endOf('month')],
                        'First Half This Month': [moment().startOf('month'), moment().startOf('month').add(14, 'days')],
                        'Second Half This Month': [moment().startOf('month').add(15, 'days'), moment().endOf('month')],
                        //'Last 15 Days': [moment().subtract(14, 'days'), moment()],
                        '2 Months Ago': [moment().subtract(2, 'month').startOf('month'), moment().subtract(2, 'month').endOf('month')],
                        '3 Months Ago': [moment().subtract(3, 'month').startOf('month'), moment().subtract(3, 'month').endOf('month')]
                    },
                    //"linkedCalendars": false,
                    "showCustomRangeLabel": "false",
                    "startDate": start, //"03/02/2017",
                    "endDate": end //"03/08/2017"
                }, function (start, end, label) {
                    //console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
                    self.DateListClickedCallback(start, end);
                });

                if (this.dateFilter === null) {
                    this.dateFilter = start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY');
                    App.model.set('widget1Value', null);
                    App.model.set('widget2Value', null);
                    App.model.set('widget3Value', null);
                    App.model.set('widget4Value', null);
                    App.model.set('mySumTotalTime', null);
                    App.model.set('mySumTotalMeetings', null);
                    App.model.set('mySumMeetingsTarget', null);
                    App.model.set('myMtgPercent', null);
                    App.model.set('teamSumTotalTime', null);
                    App.model.set('teamSumTotalMeetings', null);
                    App.model.set('teamSumMeetingsTarget', null);
                    App.model.set('teamMtgPercent', null);

                    App.model.set('selectedDateFilter', this.dateFilter);
                }
                this.$('#dateRangeText').html("<b>Date Range</b>: " + this.dateFilter + '&nbsp;');
                this.$('#dateRangeTooltip').attr("title", "Date Range: " + this.dateFilter);

                self.$('#dateRangeText').tooltip();
                self.$('#statusFilterTitle').tooltip();
                self.$('#empStatusFilterTitle').tooltip();

                self.getData();

            },
            getData: function () {
                //console.log('SubHeaderView: getData');
                var self = this;
                var app = App;

                if (this.myEmployeeId !== null) {
                    this.restDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            transport: {
                                type: "odata",
                                read: {
                                    url: function () {
                                        var url = App.config.DataServiceURL + "/rest/GetMyOpenTimers/" + self.myEmployeeId;
                                        return url;
                                    },
                                    complete: function (e) {
                                        console.log('SubHeaderView:restDataSource:update:complete');
                                     },
                                    error: function (e) {
                                        var message = 'SubHeaderView:restDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    type: "GET",
                                    dataType: "json"
                                }
                            },
                            schema: {

                                model: {
                                    id: "intTimerID",
                                    fields: {

                                        intTimerID: {editable: false, type: "number"},
                                        companyID: {editable: false, type: "number"},
                                        clientID: {editable: false, type: "number"},
                                        employeeID: {editable: false, type: "number"},
                                        serviceID: {editable: false, type: "number"},
                                        workDate: {
                                            editable: true,
                                            type: "date"
                                            //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                            //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                        },
                                        startTime: {
                                            editable: true,
                                            type: "date"
                                        },
                                        stopTime: {
                                            editable: true,
                                            type: "date"
                                        },
                                        clientCompany: {editable: true, type: "string", validation: {required: false}},
                                        clientName: {editable: true, type: "string", validation: {required: false}},
                                        customerName: {editable: true, type: "string", validation: {required: true}},
                                        employeeName: {editable: true, type: "string", validation: {required: false}},
                                        serviceItem: {type: "string", validation: {required: false}},
                                        tasks: {editable: true, type: "number"},
                                        meetings: {editable: true, type: "number"},
                                        timeWorked: {editable: true, type: "number"},
                                        timerNote: {editable: true, type: "string", validation: {required: false}},
                                        mgrNote: {editable: true, type: "string", validation: {required: false}},
                                        timerAction: {editable: true, type: "string", validation: {required: false}},
                                        recordType: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            sort: [{field: "employeeName", dir: "asc"}, {field: "workDate", dir: "asc"}],
                            requestStart: function (e) {
                                //console.log('SubHeaderView:dataSource:request start:');
                                //kendo.ui.progress(self.$("#subHeaderLoading"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('SubHeaderView:dataSource:request end:');
                                //var data = this.data();
                                //kendo.ui.progress(self.$("#subHeaderLoading"), false);
                            },
                            change: function (e) {
                                //console.log('SubHeaderView:dataSource:change:');
                            },
                            error: function (e) {
                                //console.log('SubHeaderView:dataSource:error');
                                kendo.ui.progress(self.$("#subHeaderLoading"), false);
                            }
                        });

                    self.restDataSource.fetch(function (data) {
                        kendo.ui.progress(self.$("#subHeaderLoading"), false);
                        if (this.data().length > 0) {
                            console.log('SubHeaderView:getData:data 1st record:' + JSON.stringify(this.data()[0]));
                            //console.log('SubHeaderView:getData:data:all records:' + JSON.stringify(this.data()));

                            var record = this.data()[0];
                            self.timerStatus = "Stopped";
                            if (record.timerAction === "resume") {
                                if (self.timerStatus === "INTERRUPTED") {
                                    self.onTimerStatusChanged();
                                } else {
                                    self.timerStatus = "INTERRUPTED";
                                }
                            } else {
                                if (self.timerStatus === "Open") {
                                    self.onTimerStatusChanged();
                                } else {
                                    self.timerStatus = "Open";
                                }
                            }

                            app.model.set('intTimerID', record.intTimerID);

                            if (this.data().length === 1 && record.recordType.indexOf("INT") < 0) {
                                app.model.set('currentTimerID', record.intTimerID);
                            } else {
                                app.model.set('currentTimerID', null);
                            }

                            var offset = (new Date()).getTimezoneOffset();
                            var startTime = new Date(moment(record.startTime).clone()); //.add((offset), 'minutes'));

                            // TODO
                            // If an admin (e.g. Carol) is in PST (1) editing a timer file from someone in another time zone (2, 3, 4)
                            // need to account for that employee's time zone in the start time that they created

                            var now = new Date(moment().clone()); //.subtract((offset), 'minutes'));

                            if (moment(now).isAfter(startTime)) {
                                app.model.set('timerStartTime', startTime);
                            } else {
                                // do nothing
                                console.log('SubHeaderView:inherited start time:now:startTime:' + now + ":" + startTime);

                            }

                            app.model.set('currentTimerServiceID', record.serviceID);
                            app.model.set('currentTimerClientID', record.clientID);
                            app.model.set('timerStatus', self.timerStatus);


                        } else {
                            self.timerStatus = "Stopped";
                            app.model.set('currentTimerID', null);
                            app.model.set('intTimerID', null);
                            app.model.set('timerStatus', self.timerStatus);
                        }
                    });

                    // Get employee records for userType 2
                    if (self.userType === 2) {

                        self.employeeIdsNotUserType1 = app.model.get('employeeIdsNotUserType1');

                        if (self.employeeIdsNotUserType1.length === 0) {
                            var schedulerFilter = [
                                    {field: "status", operator: "eq", value: '1 - Active'},
                                    {field: "userType", operator: "neq", value: 1}
                                    //{field: "teamLeaderID", operator: "eq", value: self.myTeamLeaderId}
                            ];
                            this.schedulerDataSource = new kendo.data.DataSource(
                                {
                                    //autoBind: false,
                                    type: "odata",
                                    transport: {
                                        read: {
                                            url: App.config.DataServiceURL + "/odata/PortalEmployee",
                                            complete: function (e) {
                                                console.log('SubHeaderView:progressClientDS:read:complete');
                                            },
                                            error: function (e) {
                                                var message = 'SubHeaderView:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                                console.log(message);
                                            },
                                            dataType: "json"
                                        }
                                    },
                                    schema: {
                                        data: "value",
                                        total: function (data) {
                                            return data['odata.count'];
                                        },
                                        model: {
                                            id: "employeeID",
                                            fields: {
                                                employeeID: {editable: false, type: "number"},
                                                //userKey: {editable: false, type: "number"},
                                                //firstName: {
                                                //    editable: true,
                                                //    type: "string",
                                                //    validation: {required: false}
                                                //},
                                                //lastName: {
                                                //    editable: true,
                                                //    type: "string",
                                                //    validation: {required: false}
                                                //},
                                                //status: {type: "string", validation: {required: false}},
                                                isTeamLeader: {editable: true, type: "boolean"}
                                            }
                                        }
                                    },
                                    sort: [
                                        {field: "firstName", dir: "asc"},
                                        {field: "lastName", dir: "asc"}
                                    ],
                                    filter: schedulerFilter,
                                    serverSorting: true,
                                    serverFiltering: true
                                });


                            self.schedulerDataSource.fetch(function (data) {

                                var records = this.data();
                                self.employeeIdsNotUserType1 = [];

                                // Add to employeeIdsNotUserType1 app model array
                                $.each(records, function (index, value) {
                                    self.employeeIdsNotUserType1.push(value.employeeID);
                                });

                                console.log('SubHeaderView:onShow:self.myTeamLeaderId:' + self.myTeamLeaderId);
                                console.log('SubHeaderView:onShow:self.employeeIdsNotUserType1:' + JSON.stringify(self.employeeIdsNotUserType1));
                                app.model.set('employeeIdsNotUserType1', self.employeeIdsNotUserType1);

                            });
                        }
                    }
                }

                if (self.options.showCompanyFilter === true) {
                    self.companyDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                    complete: function (e) {
                                        console.log('SubHeaderView:companyDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'SubHeaderView:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "companyID",
                                    fields: {

                                        companyID: {editable: false, type: "number"},
                                        branchID: {editable: false, type: "number"},
                                        clientCompany: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        }
                                    }
                                }
                            },
                            sort: {field: "clientCompany", dir: "asc"},
                            requestStart: function (e) {
                                //console.log('SubHeaderGridView:dataSource:request start:');
                                //kendo.ui.progress(self.$("#companyLoading"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('SubHeaderGridView:dataSource:request end:');
                                //kendo.ui.progress(self.$("#companyLoading"), false);
                            },
                            error: function (e) {
                                //console.log('SubHeaderGridView:dataSource:error');
                                //kendo.ui.progress(self.$("#companyLoading"), false);
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            filter: [
                                {field: "clientCompany", operator: "neq", value: "--- NONE ---"}
                            ]
                        });

                    self.companyDataSource.fetch(function (data) {

                        var coRecords = self.companyDataSource.data();
                        self.companyRecords = [];

                        $.each(coRecords, function (index, value) {

                            var record = value;
                            var newRecord = {
                                companyID: record.companyID,
                                clientCompany: record.clientCompany
                            };
                            self.companyRecords.push(newRecord);

                        });

                        var allRecord = {
                            companyID: 0,
                            clientCompany: "All Companies"
                        };

                        self.companyRecords.unshift(allRecord);

                        self.$("#companyList").kendoDropDownList({
                            dataValueField: "companyID",
                            dataTextField: "clientCompany",
                            dataSource: self.companyRecords,
                            value: self.selectedCompanyId,
                            change: self.onCompanyIdChanged
                        });

                        var company = $("#companyList").data("kendoDropDownList").text();
                        self.$('#clientCompanyTooltip').attr("title", "Company: " + company);

                    });
                }

                if (self.options.showClientFilter === true) {

                    self.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);
                    //self.userType = parseInt(App.model.get('userType'),0);
                    self.clientNameRecords = App.model.get('clientNameRecords');

                    self.progressClientDS = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                    },
                                    complete: function (e) {
                                        console.log('SubHeaderView:progressClientDS:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'SubHeaderView:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientID",
                                    fields: {
                                        clientID: {editable: false, defaultValue: 0, type: "number"},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        clientCompany: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function (e) {
                                //console.log('SubHeaderGridView:dataSource:request start:');
                                //kendo.ui.progress(self.$("#companyLoading"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('SubHeaderGridView:dataSource:request end:');
                                //kendo.ui.progress(self.$("#companyLoading"), false);
                            },
                            error: function (e) {
                                //console.log('SubHeaderGridView:dataSource:error');
                                //kendo.ui.progress(self.$("#companyLoading"), false);
                            },
                            filter: [
                                {field: "status", operator: "eq", value: '4 - Active'}
                            ],
                            sort: [
                                {field: "lastName", dir: "asc"},
                                {field: "firstName", dir: "asc"},
                                {field: "clientCompany", dir: "asc"}
                            ]
                        });

                    if (self.clientNameRecords.length === 0) {
                        self.progressClientDS.fetch(function () {

                            console.log('SubHeaderGridView:onShow:showClientFilter:progressClientDS.fetch():selectedClientId:' + self.selectedClientId);

                            var records = self.progressClientDS.data();
                            var modifiedRecords = [];

                            $.each(records, function (index, value) {

                                var record = value;

                                var newRecord = {
                                    clientID: record.clientID,
                                    name: record.lastName + ", " + record.firstName + ": " + record.clientCompany //record.firstName + " " + record.lastName
                                };

                                if (self.userType === 1) {
                                    if (record.schedulerID === self.myEmployeeId) {
                                        modifiedRecords.push(newRecord);
                                    }
                                } else {
                                    modifiedRecords.push(newRecord);
                                }

                            });

                            self.$("#clientList").kendoDropDownList({
                                dataTextField: "name",
                                dataValueField: "clientID",
                                dataSource: modifiedRecords,
                                value: self.selectedClientId,
                                change: self.onClientIdChanged
                            });

                            var client = self.$("#clientList").data("kendoDropDownList").text();
                            self.$('#clientTooltip').attr("title", "Client: " + client);

                        });
                    } else {

                        var records = self.clientNameRecords;
                        var modifiedRecords = [];

                        $.each(records, function (index, value) {

                            var record = value;
                            var newRecord = {
                                clientID: record.clientID,
                                name: record.lastName + ", " + record.firstName + ": " + record.clientCompany //record.firstName + " " + record.lastName
                            };

                            if (self.userType === 1) {
                                if (record.schedulerID === self.myEmployeeId) {
                                    modifiedRecords.push(newRecord);
                                }
                            } else {
                                modifiedRecords.push(newRecord);
                            }
                        });

                        self.$("#clientList").kendoDropDownList({
                            dataTextField: "name",
                            dataValueField: "clientID",
                            dataSource: modifiedRecords,
                            value: self.selectedClientId,
                            change: self.onClientIdChanged
                        });

                        var client = self.$("#clientList").data("kendoDropDownList").text();
                        self.$('#clientTooltip').attr("title", "Client: " + client);
                    }
                }

                if (self.options.showPayrollProcessingDate === true) {
                    self.adminDataSource = new kendo.data.DataSource({
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalAdmin";
                                },
                                complete: function (e) {
                                    console.log('SubHeaderView:adminDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'SubHeaderView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    return App.config.DataServiceURL + "/odata/PortalAdmin" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('SubHeaderView:adminDataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'SubHeaderView:adminDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, defaultValue: 0, type: "number"},
                                    payrollLockDate: {editable: true, type: "date", validation: {required: false}}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function (e) {
                            //console.log('SubHeaderGridView:dataSource:request start:');
                            //kendo.ui.progress(self.$("#companyLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('SubHeaderGridView:dataSource:request end:');
                            //kendo.ui.progress(self.$("#companyLoading"), false);
                        },
                        error: function (e) {
                            //console.log('SubHeaderGridView:dataSource:error');
                            //kendo.ui.progress(self.$("#companyLoading"), false);
                        }
                    });

                    self.adminDataSource.fetch(function () {

                        //console.log('SubHeaderGridView:onShow:adminDataSource');

                        var records = this.data();

                        if (records.length > 0) {

                            var record = records[0];
                            var payrollLockDate = record.payrollLockDate;
                            var date = moment(payrollLockDate).clone().format('M/D/YYYY');

                            self.$('#payrollDateTooltip').attr("title", "Payroll Lock Date: " + date);

                            self.$("#payrollDate").kendoDatePicker({
                                value: payrollLockDate,
                                change: self.onPayrollProcessingDateClicked
                            });
                        }
                    });
                }

                if (self.options.showTeamLeaderFilter === true) {
                    self.teamLeaderDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                    complete: function (e) {
                                        console.log('SubHeaderView:teamLeaderDataSource:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'SubHeaderView:teamLeaderDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "employeeID",
                                    fields: {
                                        employeeID: {editable: false, type: "number"},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        status: {type: "string", validation: {required: false}},
                                        isTeamLeader: {editable: true, type: "boolean"},
                                        teamLeaderID: {editable: false, type: "number"},
                                        teamLeaderName: {editable: false, type: "string"}
                                    }
                                }
                            },
                            sort: [
                                {field: "lastName", dir: "asc"},
                                {field: "firstName", dir: "asc"}
                            ],
                            filter: {field: "isTeamLeader", operator: "eq", value: true},
                            serverFiltering: true,
                            serverSorting: true
                        });

                    self.teamLeaderRecords = App.model.get('teamLeaderRecords');

                    if (self.teamLeaderRecords.length === 0) {

                        self.teamLeaderDataSource.fetch(function () {

                            var tlRecords = self.teamLeaderDataSource.data();
                            self.teamLeaderRecords = [];

                            $.each(tlRecords, function (index, value) {

                                var record = value;
                                var newRecord = {
                                    teamLeaderID: record.employeeID,
                                    teamLeaderName: record.lastName + ", " + record.firstName
                                };
                                self.teamLeaderRecords.push(newRecord);

                            });

                            //var teamLeaderRecords = self.teamLeaderRecords;
                            var allRecord = {
                                teamLeaderID: 0,
                                teamLeaderName: "All Employees"
                            };
                            self.teamLeaderRecords.unshift(allRecord);

                            self.$("#teamLeaderList").kendoDropDownList({
                                dataTextField: "teamLeaderName",
                                dataValueField: "teamLeaderID",
                                dataSource: self.teamLeaderRecords,
                                value: self.selectedTeamLeaderId,
                                change: self.onTeamLeaderIdChanged
                            });

                            var teamLeader = self.$("#teamLeaderList").data("kendoDropDownList").text();
                            self.$('#teamLeaderTooltip').attr("title", "Account Manager: " + teamLeader);

                            App.model.set('teamLeaderRecords', self.teamLeaderRecords);
                        });
                    } else {
                        //console.log('PortalEmployeeGridView:getData:cached teamLeaderRecords');

                        self.$("#teamLeaderList").kendoDropDownList({
                            dataTextField: "teamLeaderName",
                            dataValueField: "teamLeaderID",
                            dataSource: self.teamLeaderRecords,
                            value: self.selectedTeamLeaderId,
                            change: self.onTeamLeaderIdChanged
                        });

                        var teamLeader = self.$("#teamLeaderList").data("kendoDropDownList").text();
                        self.$('#teamLeaderTooltip').attr("title", "Account Manager: " + teamLeader);

                    }


                }
            },
            onMyEmployeeIdChanged: function () {
                //console.log('SubHeaderView: onMyEmployeeIdChanged');

                this.myEmployeeId = App.model.get('myEmployeeId');
                this.getData();
            },
            onTimerStatusChanged: function () {
                //console.log('SubHeaderView: onTimerStatusChanged');
                this.timerStatus = App.model.get('timerStatus');
                console.log('SubHeaderView: onTimerStatusChanged:timerStatus' + this.timerStatus);

                this.timerAlertRegion.reset();
                if (this.timerStatus === "Open") {
                    this.timerAlertRegion.show(new TimerCalloutWidgetView({
                        type: 'Attention',
                        message: 'Timer File is currently open.',
                        borderRadius: 0
                    }));
                } else if (this.timerStatus === "INTERRUPTED") {
                    this.timerAlertRegion.show(new TimerCalloutWidgetView({
                        type: 'Attention',
                        message: 'Timer File is currently open but interrupted.',
                        borderRadius: 0
                    }));
                } else if (this.timerStatus === "Finish") {
                    this.timerAlertRegion.show(new TimerCalloutWidgetView({
                        type: 'Attention',
                        message: 'Timer File is currently open but being reviewed.',
                        borderRadius: 0
                    }));
                }
            },
            DateListClickedCallback: function (start, end) {
                //console.log('SubHeaderView: onDateListClickedCallback:start:end:' + start + ":" + end);

                var self = this;

                this.dateFilter = start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY');
                this.$('#dateRangeText').html("<b>Date Range</b>: " + this.dateFilter + '&nbsp;');
                this.$('#dateRangeTooltip').attr("title", "Date Range: " + this.dateFilter);

                App.model.set('widget1Value', null);
                App.model.set('widget2Value', null);
                App.model.set('widget3Value', null);
                App.model.set('widget4Value', null);
                App.model.set('mySumTotalTime', null);
                App.model.set('mySumTotalMeetings', null);
                App.model.set('mySumMeetingsTarget', null);
                App.model.set('myMtgPercent', null);
                App.model.set('teamSumTotalTime', null);
                App.model.set('teamSumTotalMeetings', null);
                App.model.set('teamSumMeetingsTarget', null);
                App.model.set('teamMtgPercent', null);

                App.model.set('selectedDateFilter', this.dateFilter);

            },
            onCompanyIdChanged: function () {
                //console.log('SubHeaderView: onCompanyIdChanged');

                var self = this;

                var selectedCompanyId = self.$("#companyList").val();

                if (!isNaN(selectedCompanyId)) {
                    self.selectedCompanyId = parseInt(self.$("#companyList").val(), 0);
                    App.model.set('selectedCompanyId', self.selectedCompanyId);

                    var company = self.$("#companyList").data("kendoDropDownList").text();
                    self.$('#clientCompanyTooltip').attr("title", "Company: " + company);

                }

                //console.log('SubHeaderView:onCompanyIdChanged:testCompanyID:actualCompanyID:' + selectedCompanyId + ":" + self.selectedCompanyId);
            },
            onClientIdChanged: function () {
                //console.log('SubHeaderView: onClientIdChanged');

                var self = this;

                self.selectedClientId = parseInt(self.$("#clientList").val(), 0);
                App.model.set('selectedClientId', self.selectedClientId);
            },
            onStatusFilterListChanged: function (e) {

                var self = this;

                //console.log('SubHeaderView: onStatusListChanged:value:' + self.$("#statusFilterList").val());

                self.statusFilter = self.$("#statusFilterList").val(); //e.target.text;
                App.model.set("clientAllNameRecords", []);
                App.model.set('selectedStatusFilter', self.statusFilter);

                self.$('#statusFilterTooltip').attr("title", "Client: " + self.statusFilter);
            },
            onEmpStatusFilterListChanged: function (e) {

                //console.log('SubHeaderView: onEmpStatusListChanged:value:' + self.$("#empStatusFilterList").val());
                var self = this;
                App.model.set('timeCardDetailGridFilter', []);

                App.model.set("clientAllNameRecords", []);

                self.empStatusFilter = self.$("#empStatusFilterList").val(); //e.target.text;
                App.model.set('selectedEmpStatusFilter', self.empStatusFilter);

                self.$('#empStatusFilterTooltip').attr("title", "Emp: " + self.empStatusFilter);

                if (self.empStatusFilter !== "All Status" && self.empStatusFilter !== "1 - Active" &&  self.userType === 3)  {
                    self.$("#teamLeaderList").data("kendoDropDownList").value(0);
                    App.model.set('selectedTeamLeaderId', 0);
                }
            },
            onTeamLeaderIdChanged: function () {
                //console.log('SubHeaderView: onTeamLeaderIdChanged');

                var self = this;
                self.selectedTeamLeaderId = parseInt(self.$("#teamLeaderList").val(), 0);
                //if (self.selectedTeamLeaderId === 0) {
                    App.model.set('timeCardDetailGridFilter', []);
                //}
                App.model.set('selectedTeamLeaderId', self.selectedTeamLeaderId);
            },
            onPayrollProcessingDateClicked: function (e) {

                //console.log('SubHeaderView: onPayrollProcessingDateClicked');

                var self = this;

                var date = self.$("#payrollDate").val();
                date = moment(date).clone().format('M/D/YYYY');

                if (confirm("Are you sure you want to update the payroll lock date to: " + date + "?")) {
                    self.adminDataSource.fetch(function () {
                        var records = this.data();
                        if (records.length > 0) {
                            var record = records[0];
                            record.set("payrollLockDate", date);
                            self.adminDataSource.sync();
                            self.$('#payrollDateTooltip').attr("title", "Payroll Lock Date: " + date);
                        }
                    });
                }
            },
            onClientOptionListClicked: function (e) {

                //console.log('SubHeaderView: onClientOptionListClicked');

                var self = this;

                self.selectedClientReport = self.$("#clientReportList").val(); //e.target.text;
                App.model.set('selectedClientReport', self.selectedClientReport);
                self.$('#clientReportFilterTitle').html("<b>Client Report: </b>" + self.selectedClientReport + '&nbsp;');
                self.$('#clientReportFilterTooltip').attr("title", "Client Report: " + self.selectedClientReport);
            },
            onViewOptionListClicked: function (e) {

                //console.log('SubHeaderView: onViewOptionListClicked');

                var self = this;

                self.selectedView = self.$("#viewsList").val(); //e.target.text;
                App.model.set('selectedView', self.selectedView);
                self.$('#viewsFilterTitle').html("<b>Client Report: </b>" + self.selectedView + '&nbsp;');
                self.$('#viewsFilterTooltip').attr("title", "View: " + self.selectedView);
            },
            onClientProfileClicked: function () {
                //console.log('SubHeaderView:onClientProfileClicked:' + e.target);

                //App.model.set('selectedClientId', clientId);

                var self = this;
                if (self.userType !== 1) {
                    App.router.navigate('editClient', {trigger: true});
                }

            },
            onCalcStatsClicked: function () {
                //console.log('SubHeaderView:onCalcStatsClicked:' + e.target);

                var self = this;
                var app = App;

                var message = "This will recalculate the statistics for all clients for the selected date range.  Are you sure you want to continue?";
                if (confirm(message) === true) {

                    this.serviceDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/PortalServiceItem";
                                    },
                                    complete: function (e) {
                                        console.log('SubHeaderView:serviceDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'SubHeaderView:serviceDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "serviceID",
                                    fields: {
                                        serviceID: {editable: true, type: "number"},
                                        ordering: {editable: false, type: "number"},
                                        serviceItem: {editable: true, type: "string", validation: {required: false}},
                                        recordTasksMtgs: {editable: true, type: "boolean"},
                                        toStats: {editable: true, type: "boolean"},
                                        timerStatus: {editable: true, type: "number"}
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                //console.log('   Widget1View:requestStart');

                                //kendo.ui.progress(self.$("#subHeaderLoading"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    Widget1View:requestEnd');
                                //kendo.ui.progress(self.$("#subHeaderLoading"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   Widget1View:error');
                                kendo.ui.progress(self.$("#subHeaderLoading"), false);

                                //App.modal.show(new ErrorView());

                            },
                            sort: {field: "ordering", dir: "asc"}
                        });

                    this.clientDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            batch: false,
                            serverSorting: true,
                            serverFiltering: true,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                    },
                                    complete: function (e) {
                                        console.log('SubHeaderView:clientDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'SubHeaderView:clientDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientID",
                                    fields: {
                                        clientID: {editable: false, defaultValue: 0, type: "number"},
                                        schedulerID: {editable: false, defaultValue: 0, type: "number"},
                                        //schedulerName: {editable: true, type: "string", validation: {required: false}},
                                        companyID: {editable: false, defaultValue: 0, type: "number"},
                                        //clientName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        clientCompany: {editable: true, type: "string", validation: {required: false}}
                                        //status: {editable: true, type: "string", validation: {required: false}},
                                        //planHours: {type: "number"},
                                        //taskTargetLow: {type: "number"},
                                        //taskTargetHigh: {type: "number"},
                                        //mtgTargetLowDec: {type: "number"},
                                        //mtgTargetHigh: {type: "number"}
                                    }
                                }
                            },
                            requestStart: function () {
                                //console.log('   ProgressClientDataGridView:requestStart');

                                kendo.ui.progress($("#subHeaderLoading"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressClientDataGridView:requestEnd');
                                kendo.ui.progress($("#subHeaderLoading"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressClientDataGridView:error');
                                kendo.ui.progress($("#subHeaderLoading"), false);

                            },
                            sort: [
                                {field: "lastName", dir: "asc"},
                                {field: "firstName", dir: "asc"},
                                {field: "clientCompany", dir: "asc"}
                            ],
                            filter: {field: "status", operator: "eq", value: '4 - Active'}
                            //filter: [{field: "lastName", operator: "eq", value: "Clark"}]
                            //filter: [{field: "clientID", operator: "eq", value: 2238}]
                            // 2238 Helgerson, 6/18
                            // 472 sample client (ZTest, Zebulun)

                        });

                    self.serviceDataSource.fetch(function () {

                        self.serviceRecords = this.data();

                        // Get list of clients to work on

                        self.clientAllNameRecords = app.model.get('clientAllNameRecords');
                        //console.log('SubHeaderView:onCalcStatsClicked:clientAllNameRecords:' + JSON.stringify(self.clientAllNameRecords));
                        if (true) { //self.clientAllNameRecords.length === 0) {

                            self.clientDataSource.fetch(function (data) {

                                self.clientAllNameRecords = this.data();
                                //console.log('SubHeaderView:onCalcStatsClicked:clientAllNameRecords:2:' + JSON.stringify(self.clientAllNameRecords));
                                app.model.set('clientAllNameRecords', self.clientAllNameRecords);
                                self.calculateProgressData();

                            });
                        } else {
                            self.calculateProgressData();
                        }
                    });
                }
            },
            calculateProgressDataNew: function () {
                //console.log('TimerOptionsView:calculateProgressData');
                var self = this;
                var app = App;

                var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                var workStartDate = moment(startDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                var workCurrentDate = moment(endDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                this.restDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        transport: {
                            read: function (options) {
                                //self.dataSource.cancelled = true;
                                $.ajax({
                                    type: "GET",  //PATCH?
                                    contentType: "application/json", //; charset=utf-8",
                                    url: App.config.DataServiceURL + "/rest/GetCalcClientStats/all/" + workStartDate + "/" + workCurrentDate,
                                    dataType: "json",
                                    requestStart: function (e) {
                                        //kendo.ui.progress(self.$("#openTimerLoading"), true);
                                    },
                                    requestEnd: function (e) {
                                        //kendo.ui.progress(self.$("#openTimerLoading"), false);
                                    },
                                    success: function (result) {
                                        kendo.ui.progress(self.$("#subHeaderLoading"), false);
                                        console.log('SubHeaderView:restDataSource:success:calculateProgressData:Stats calculation has finished for all clients');
                                        self.resetStatsDashboardCache();
                                    },
                                    error: function (result) {
                                        // notify the data source that the request failed
                                        console.log('SubHeaderView:restDataSource:error');
                                        kendo.ui.progress(self.$("#subHeaderLoading"), false);
                                    }
                                });
                            }
                        }
                    });

                kendo.ui.progress(self.$("#subHeaderLoading"), true);
                self.restDataSource.fetch();

            },
            calculateProgressData: function () {
                console.log('SubHeaderView:calculateProgressDataOrig');

                var self = this;
                var app = App;

                var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                this.startDate = moment(startDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                this.endDate = moment(endDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                var offset = (new Date()).getTimezoneOffset();

                var workDateValue = this.startDate;

                // Get range of dates and loop through them
                var dates = [];
                var i = 1;
                while (workDateValue <= this.endDate) {
                    dates.push(workDateValue);
                    workDateValue = moment(startDate, 'MM/DD/YYYY').add('days', i).clone().format('YYYY-MM-DD');
                    i++;
                    if (i > 100) {
                        return;
                    }
                }

                var workDateStart = new Date(moment(self.startDate + "T00:00:00+0000").clone());
                var workDateEnd = new Date(moment(self.endDate + "T23:59:59+0000").clone());
                var clientID = 3053;

                self.timerClientTotalDataSource = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalTimerTotalClientTime";
                                },
                                complete: function (e) {
                                    console.log('SubHeaderView:timerClientTotalDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'SubHeaderView:timerClientTotalDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "rowID",
                                fields: {
                                    rowID: {editable: false, defaultValue: 0, type: "number"},
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date",
                                        validation: {required: false}
                                    },
                                    totalTimeWorked: {editable: false, defaultValue: 0, type: "number"},
                                    totalTasks: {editable: false, defaultValue: 0, type: "number"},
                                    totalMeetings: {editable: false, defaultValue: 0, type: "number"},
                                    workYear: {editable: false, defaultValue: 0, type: "number"},
                                    workMonth: {editable: false, defaultValue: 0, type: "number"},
                                    workDay: {editable: false, defaultValue: 0, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   SubHeaderView:requestStart');

                            kendo.ui.progress(self.$("#subHeaderLoading"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    SubHeaderView:requestEnd');
                            //kendo.ui.progress(self.$("#subHeaderLoading"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            kendo.ui.progress(self.$("#subHeaderLoading"), false);


                        },
                        //filter: [
                        //    {field: "workYear", operator: "eq", value: self.year},
                        //    {field: "workMonth", operator: "eq", value: self.month},
                        //    {field: "workDay", operator: "eq", value: self.day},
                        //    {field: "clientID", operator: "eq", value: clientID}
                        //]
                        filter: [{field: "workDate", operator: "gte", value: self.startDate},
                            {field: "workDate", operator: "lte", value: self.endDate},
                            {field: "clientID", operator: "eq", value: clientID}]
                    });

                self.progressDataSource = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressData";
                                },
                                complete: function (e) {
                                    console.log('SubHeaderView:progressDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'SubHeaderViewprogressDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('SubHeaderView:update');
                                    return App.config.DataServiceURL + "/odata/ProgressData" + "(" + data.entryID + ")";
                                },
                                complete: function (e) {
                                    //console.log('SubHeaderView:progressDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                },
                                error: function (e) {
                                    console.log('SubHeaderView:progressDataSource:update:error:' + JSON.stringify(e.responseJSON));
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('SubHeaderView:create:data:' + data);
                                    return App.config.DataServiceURL + "/odata/ProgressData";
                                },
                                complete: function (e) {
                                    console.log('SubHeaderView:progressDataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'SubHeaderViewprogressDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "entryID",
                                fields: {
                                    entryID: {editable: false, defaultValue: 0, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date",
                                        validation: {required: false}
                                    },
                                    timeStamp: {
                                        editable: true,
                                        type: "date",
                                        validation: {required: false}
                                    },
                                    schedulerTime: {type: "number"},
                                    support1Time: {type: "number"},
                                    adminTime: {type: "number"},
                                    schedulerCalls: {type: "number"},
                                    support1Calls: {type: "number"},
                                    schedulerMeetings: {type: "number"},
                                    support1Meetings: {type: "number"},
                                    compTime: {type: "number"},
                                    compCalls: {type: "number"},
                                    compMeetings: {type: "number"},
                                    referralTime: {type: "number"},
                                    referralCalls: {type: "number"},
                                    referralMeetings: {type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   SubHeaderView:progressDataSource:requestStart');

                            kendo.ui.progress(self.$("#subHeaderLoading"), true);
                        },
                        requestEnd: function () {

                            //console.log('    SubHeaderView:progressDataSource:requestEnd');
                            kendo.ui.progress(self.$("#subHeaderLoading"), false);

                        },
                        error: function () {

                            //console.log('   SubHeaderView:progressDataSource:error');
                            kendo.ui.progress(self.$("#subHeaderLoading"), false);

                        },
                        filter: [
                            {field: "clientID", operator: "eq", value: clientID},
                            {field: "workDate", operator: "gte", value: workDateStart},
                            {field: "workDate", operator: "lte", value: workDateEnd}]
                    });

                self.timerClientTotalDataSource.fetch(function ()
                {

                    var timerClientTotalData = this.data();

                    self.progressDataSource.fetch(function ()
                    {

                        var progressData = this.data();

                        // Loop through clients
                        $.each(self.clientAllNameRecords, function (clientIndex, clientValue) {

                            var clientID = clientValue.clientID;

                            console.log('SubHeaderView:calculateProgressData:clientID:number of days to process:' + clientID + ":" + (i - 1));
                            self.timerThisClientRecords = [];
                            var j = 1;

                            // Loop through date range
                            $.each(dates, function (index, value) {

                                var date = value;
                                console.log('SubHeaderView:calculateProgressData:timerData:clientID:date:' + clientID + ":" + date);

                                // Existing timer records
                                var timerClientDateRecords = $.grep(timerClientTotalData, function (element, index) {
                                    return (moment(element.workDate).clone().format('YYYY-MM-DD') === date && element.clientID === clientID);
                                });

                                var sumSchedulerTime = 0;
                                var sumSchedulerCalls = 0;
                                var sumSchedulerMeetings = 0;

                                var sumSupport1Time = 0;
                                var sumSupport1Calls = 0;
                                var sumSupport1Meetings = 0;

                                var sumCompTime = 0;
                                var sumCompCalls = 0;
                                var sumCompMeetings = 0;

                                var sumReferralTime = 0;
                                var sumReferralCalls = 0;
                                var sumReferralMeetings = 0;

                                var sumAdminTime = 0;

                                if (timerClientDateRecords.length > 0) {

                                    // Loop through timer records and create array of summed timer records for this client
                                    $.each(timerClientDateRecords, function (index, value) {
                                        var record = value;

                                        var totalTimeWorked = record.totalTimeWorked; //app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(record.totalTimeWorked);

                                        var serviceRecord = self.getServiceRecord(record.serviceID);
                                        //serviceRecord.toStats === false &&
                                        if (serviceRecord.recordTasksMtgs === false && record.serviceID !== 1013 && record.serviceID !== 1001) {
                                            sumAdminTime += totalTimeWorked;
                                        } else {

                                            if (record.serviceID === 1011) {  //Support Scheduling
                                                sumSupport1Time += totalTimeWorked;
                                                sumSupport1Calls += record.totalTasks;
                                                sumSupport1Meetings += record.totalMeetings;
                                            } else if (record.serviceID === 1008) { // Comp Time
                                                sumCompTime += totalTimeWorked;
                                                sumCompCalls += record.totalTasks;
                                                sumCompMeetings += record.totalMeetings;
                                            } else if (record.serviceID === 1014) { // Referral Time
                                                sumReferralTime += totalTimeWorked;
                                                sumReferralCalls += record.totalTasks;
                                                sumReferralMeetings += record.totalMeetings;
                                            } else { // Scheduling (1001) and other
                                                sumSchedulerTime += totalTimeWorked;
                                                sumSchedulerCalls += record.totalTasks;
                                                sumSchedulerMeetings += record.totalMeetings;
                                            }
                                        }
                                    });
                                }

                                sumSchedulerTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumSchedulerTime);
                                sumSupport1Time = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumSupport1Time);
                                sumCompTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumCompTime);
                                sumReferralTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumReferralTime);
                                sumAdminTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumAdminTime);

                                var newRecord = {
                                    clientID: clientID,
                                    workDate: date,
                                    schedulerTime: parseFloat(sumSchedulerTime),
                                    schedulerCalls: sumSchedulerCalls,
                                    schedulerMeetings: sumSchedulerMeetings,
                                    support1Time: parseFloat(sumSupport1Time),
                                    support1Calls: sumSupport1Calls,
                                    support1Meetings: sumSupport1Meetings,
                                    compTime: parseFloat(sumCompTime),
                                    compCalls: sumCompCalls,
                                    compMeetings: sumCompMeetings,
                                    referralTime: parseFloat(sumReferralTime),
                                    referralCalls: sumReferralCalls,
                                    referralMeetings: sumReferralMeetings,
                                    adminTime: parseFloat(sumAdminTime)
                                };

                                self.timerThisClientRecords.push(newRecord);
                            });

                            // move to populate ProgressData
                            var now = new Date(moment().clone().subtract((offset), 'minutes'));

                            $.each(dates, function (index, value) {

                                var date2 = value;

                                var timerRecord = $.grep(self.timerThisClientRecords, function (element, index) {
                                    return element.workDate == date2;
                                });

                                var progressRecord = $.grep(progressData, function (element, index) {
                                    return moment(element.workDate).clone().format('YYYY-MM-DD') == date2 && element.clientID === clientID;
                                });

                                if (timerRecord.length > 0) {
                                    timerRecord = timerRecord[0];
                                } else {
                                    return false;
                                }

                                if (progressRecord.length > 0) {

                                    var progressDataRecord = progressRecord[0];

                                    var workDate = new Date(moment(progressDataRecord.workDate).clone().subtract((offset), 'minutes'));
                                    var timeStamp = new Date(moment(progressDataRecord.timeStamp).clone().subtract((offset), 'minutes'));

                                    progressDataRecord.set("workDate", workDate);
                                    progressDataRecord.set("timeStamp", timeStamp);

                                    progressDataRecord.set("schedulerTime", timerRecord.schedulerTime);
                                    progressDataRecord.set("schedulerCalls", timerRecord.schedulerCalls);
                                    progressDataRecord.set("schedulerMeetings", timerRecord.schedulerMeetings);

                                    progressDataRecord.set("support1Time", timerRecord.support1Time);
                                    progressDataRecord.set("support1Calls", timerRecord.support1Calls);
                                    progressDataRecord.set("support1Meetings", timerRecord.support1Meetings);

                                    progressDataRecord.set("compTime", timerRecord.compTime);
                                    progressDataRecord.set("compCalls", timerRecord.compCalls);
                                    progressDataRecord.set("compMeetings", timerRecord.compMeetings);

                                    progressDataRecord.set("referralTime", timerRecord.referralTime);
                                    progressDataRecord.set("referralCalls", timerRecord.referralCalls);
                                    progressDataRecord.set("referralMeetings", timerRecord.referralMeetings);

                                    progressDataRecord.set("adminTime", timerRecord.adminTime);
                                    //this.sync();
                                    console.log('SubHeaderView:calculateProgressData:existing record:clientID:workDateValue:schedulerTime:support1Time:compTime:referralTime:adminTime:' + clientID + ":" + date2 + ":" + timerRecord.schedulerTime + ":" + timerRecord.support1Time + ":" + timerRecord.compTime + ":" + timerRecord.referralTime + ":" + timerRecord.adminTime);

                                } else {
                                    // Create new progressData record

                                    var progressDataRecord2 = {
                                        clientID: clientID,
                                        timeStamp: now,
                                        workDate: date2,
                                        schedulerID: 0,
                                        schedulerTime: 0,
                                        schedulerCalls: 0,
                                        schedulerMeetings: 0,
                                        support1ID: 0,
                                        support1Time: 0,
                                        support1Calls: 0,
                                        support1Meetings: 0,
                                        support2ID: 0,
                                        support2Time: 0,
                                        support2Calls: 0,
                                        support2Meetings: 0,
                                        compID: 0,
                                        compTime: 0,
                                        compCalls: 0,
                                        compMeetings: 0,
                                        referralID: 0,
                                        referralTime: 0,
                                        referralCalls: 0,
                                        referralMeetings: 0,
                                        adminTime: 0
                                    };

                                    if (timerRecord.schedulerTime > 0 || timerRecord.schedulerCalls > 0 || timerRecord.schedulerMeetings > 0 ||
                                        timerRecord.support1Time > 0 || timerRecord.support1Calls > 0 || timerRecord.support1Meetings > 0 ||
                                        timerRecord.compTime > 0 || timerRecord.compCalls > 0 || timerRecord.compMeetings > 0 || 
                                        timerRecord.referralTime > 0 || timerRecord.referralCalls > 0 || timerRecord.referralMeetings > 0 || 
                                        timerRecord.adminTime > 0) {

                                        progressDataRecord2.schedulerTime = timerRecord.schedulerTime;
                                        progressDataRecord2.schedulerCalls = timerRecord.schedulerCalls;
                                        progressDataRecord2.schedulerMeetings = timerRecord.schedulerMeetings;

                                        progressDataRecord2.support1Time = timerRecord.support1Time;
                                        progressDataRecord2.support1Calls = timerRecord.support1Calls;
                                        progressDataRecord2.support1Meetings = timerRecord.support1Meetings;

                                        progressDataRecord2.compTime = timerRecord.compTime;
                                        progressDataRecord2.compCalls = timerRecord.compCalls;
                                        progressDataRecord2.compMeetings = timerRecord.compMeetings;

                                        progressDataRecord2.referralTime = timerRecord.referralTime;
                                        progressDataRecord2.referralCalls = timerRecord.referralCalls;
                                        progressDataRecord2.referralMeetings = timerRecord.referralMeetings;

                                        progressDataRecord2.adminTime = timerRecord.adminTime;

                                        self.progressDataSource.add(progressDataRecord2);
                                        //this.sync();
                                        console.log('SubHeaderView:calculateProgressData:new record:clientID:workDateValue:schedulerTime:support1Time:compTime:referralTime:adminTime:' + clientID + ":" + date2 + ":" + timerRecord.schedulerTime + ":" + timerRecord.support1Time + ":" + timerRecord.compTime + ":" + timerRecord.referralTime + ":" + timerRecord.adminTime);
                                    } else {
                                        console.log('SubHeaderView:calculateProgressData:no time for this date:clientID:workDateValue:' + clientID + ":" + date2);
                                    }
                                }
                            });
                            //$.when(self.progressDataSource.sync()).done(function (e) {
                                console.log('SubHeaderView:calculateProgressData:Stats calculation has finished:clientID:' + clientID);
                            //});
                        });

                        //Finished updating stats
                        $.when(self.progressDataSource.sync()).done(function (e) {
                            console.log('SubHeaderView:calculateProgressDataOrig:Stats calculation has finished for all clients');
                            self.resetStatsDashboardCache();
                        });

                    });
                });
            },
            getServiceRecord: function (serviceID) {
                //console.log('SubHeaderView:getServiceRecord:serviceID:' + serviceID);

                var self = this;

                var service = $.grep(self.serviceRecords, function (element, index) {
                    return element.serviceID == serviceID;
                });

                if (service.length > 0) {
                    return service[0];
                } else {
                    return null;
                }

            },
            resetStatsDashboardCache: function () {

                //console.log('SubHeaderView:resetStatsDashboardCache');

                $.cookie('ESA:AppModel:mySumTotalTime', "");
                App.model.set('mySumTotalTime', null);

                $.cookie('ESA:AppModel:mySumTotalMeetings', "");
                App.model.set('mySumTotalMeetings', null);

                $.cookie('ESA:AppModel:mySumMeetingsTarget', "");
                App.model.set('mySumMeetingsTarget', null);

                $.cookie('ESA:AppModel:myMtgPercent', "");
                App.model.set('myMtgPercent', null);

                $.cookie('ESA:AppModel:teamSumTotalTime', "");
                App.model.set('teamSumTotalTime', null);

                $.cookie('ESA:AppModel:teamSumTotalMeetings', "");
                App.model.set('teamSumTotalMeetings', null);

                $.cookie('ESA:AppModel:teamSumMeetingsTarget', "");
                App.model.set('teamSumMeetingsTarget', null);

                $.cookie('ESA:AppModel:teamMtgPercent', "");
                App.model.set('teamMtgPercent', null);
            },
            onResize: function () {
                //console.log('SubHeaderView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('SubHeaderView:resize');
            },
            remove: function () {
                //console.log('---------- SubHeaderView:remove ----------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }

        });
    });