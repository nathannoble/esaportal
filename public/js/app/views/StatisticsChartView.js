define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/statisticsChart', 'kendo/kendo.data', 'chartjs','chartjs-plugin-datalabels','jquery.dateFormat'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {

                this.clientID = parseInt(App.model.get('selectedClientId'),0); //2189;
                //console.log('StatisticsChartView:initialize:selectedClientId:' + this.clientID);

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;
                this.metric5 = null;

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Tasks"};
                }

                if (this.options.type === "Meetings") {
                    //schedulerMeetings, supportMeetings, compMeetings
                    this.metric1 = "Scheduler Meetings";
                    this.metric2 = "Support Meetings";
                    this.metric3 = "Comp Meetings";
                    this.metric4 = "Referral Meetings";

                } else if (this.options.type === "Time") {
                    //schedulerTime, adminTime, supporTime, compTime
                    this.metric1 = "Scheduler Time";
                    this.metric2 = "Support Time";
                    this.metric3 = "Comp Time";
                    this.metric4 = "Referral Time";
                    this.metric5 = "Admin Time";

                } else { //if (this.options.type === "Tasks") {
                    //schedulerTasks, supportTasks, compTasks
                    this.metric1 = "Scheduler Tasks";
                    this.metric2 = "Support Tasks";
                    this.metric3 = "Comp Tasks";
                    this.metric4 = "Referral Tasks";
                }

                //WHERE (clientID = 2189 AND calDate BETWEEN {ts '2016-09-01 00:00:00'} and {ts '2016-09-30 23:59:59'})
                //                       OR (clientID IS NULL AND calDate BETWEEN {ts '2016-09-01 00:00:00'} and {ts '2016-09-30 23:59:59'})

                this.dateFilter = App.model.get('selectedDateFilter');
                var app = App;
                this.startDate = new Date(app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter)); //"2016-10-01T00:00:00+0000"
                this.endDate = new Date(app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter)); //"2016-10-31T23:59:59+0000"
                this.endDate = this.endDate.addHours(23);
                this.endDate = this.endDate.addMinutes(59);
            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- StatisticsChartView:onShow ----------');

                // Create chart
                this.createChart();

                // UIUX - Events beyond css and view classes
                var self = this;
                this.$('#statisticsChartTitle').html(this.options.type);
            },

            createChart: function () {
//                //console.log('StatisticsChartView:createChart');

                var self = this;

                this.dataSource = new kendo.data.DataSource(
                    {
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressStatData";
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "entryID",
                            fields: {
                                entryID: {editable: false, defaultValue: 0, type: "number"},
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                caldate: {
                                    editable: true,
                                    type: "date"
                                    //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                    //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                },
                                workingday: {type: "string"},
                                nyseHoliday: {type: "string"},
                                workDate: {editable: true, type: "date", validation: {required: false}},
                                varDay: {type: "number"},
                                adminTime: {type: "number"},
                                schedulerAndSupportTime: {type: "number"},
                                totalTime: {type: "number"},
                                totalTasks: {type: "number"},
                                totalMeetings: {type: "number"},
                                compTime: {type: "number"},
                                compCalls: {type: "number"},
                                compMeetings: {type: "number"},
                                referralTime: {type: "number"},
                                referralCalls: {type: "number"},
                                referralMeetings: {type: "number"},
                                schedulerTime: {type: "number"},
                                schedulerMeetings: {type: "number"},
                                schedulerTasks: {type: "number"},
                                supportTime: {type: "number"},
                                supportTasks: {type: "number"},
                                supportMeetings: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   StatisticsChartView:requestStart');

                        kendo.ui.progress(self.$("#loadingChart"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    StatisticsChartView:requestEnd');
                        kendo.ui.progress(self.$("#loadingChart"), false);

                    },
                    error: function (e) {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   StatisticsChartView:error:' + JSON.stringify(e));
                        kendo.ui.progress(self.$("#loadingChart"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   StatisticsChartView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    //SELECT clientID, calDate, workingday, nyseHoliday, varDay, schedulerAndSupportTime, adminTime, totalTasks,
                    //       totalMeetings, compTime, compCalls, compMeetings, schedulerTime, schedulerTasks, schedulerMeetings,
                    //       supportTime, supportTasks, supportMeetings
                    //FROM vProgressStatData WHERE (clientID = 2189 AND calDate BETWEEN {ts '2016-09-01 00:00:00'} and {ts '2016-09-30 23:59:59'})
                    //                       OR (clientID IS NULL AND calDate BETWEEN {ts '2016-09-01 00:00:00'} and {ts '2016-09-30 23:59:59'})

                    filter: [{field: "clientID", operator: "eq", value: self.clientID},
                        {field: "caldate", operator: "gte", value: self.startDate},
                        {field: "caldate", operator: "lte", value: self.endDate}],
                    sort: {field: "caldate", dir: "asc"}
                });

                this.dataSource.fetch(function () {

                    var datarecord = this.data()[0];
                    //console.log('StatisticsChartView:createChart:dataSource:fetch:data' + JSON.stringify(datarecord));

                    var dataSeries1 = [];
                    var dataSeries2 = [];
                    var dataSeries3 = [];
                    var dataSeries4 = [];
                    var dataSeries5 = [];

                    $.each(this.data(), function (index, value) {
                        var record = value;

                        var date = new Date(record.caldate); // The 0 there is the key, which sets the date to the epoch
                        //console.log('StatisticsChartView:createChart:dates:' + record.caldate + ":" + date);

                        var newrec1, newrec2, newrec3, newrec4, newrec5;

                        if (self.options.type === "Meetings") {
                            //schedulerMeetings, supportMeetings, compMeetings, referralMeetings
                            newrec1 = {
                                x: date,
                                y: record.schedulerMeetings
                            };
                            newrec2 = {
                                x: date,
                                y: record.supportMeetings
                            };
                            newrec3 = {
                                x: date,
                                y: record.compMeetings
                            };
                            newrec4 = {
                                x: date,
                                y: record.referralMeetings
                            };


                        } else if (self.options.type === "Time") {
                            //schedulerTime, adminTime, supportTime, compTime, referralTime
                            newrec1 = {
                                x: date,
                                y: record.schedulerTime
                            };
                            newrec2 = {
                                x: date,
                                y: record.supportTime
                            };
                            newrec3 = {
                                x: date,
                                y: record.compTime
                            };
                            newrec4 = {
                                x: date,
                                y: record.referralTime
                            };
                            newrec5 = {
                                x: date,
                                y: record.adminTime
                            };

                            dataSeries5.push(newrec5);

                        } else {//if (this.options.type === "Tasks") {
                            //schedulerTasks, supportTasks, compCalls, referralCalls
                            newrec1 = {
                                x: date,
                                y: record.schedulerTasks
                            };
                            newrec2 = {
                                x: date,
                                y: record.supportTasks
                            };
                            newrec3 = {
                                x: date,
                                y: record.compCalls
                            };
                            newrec4 = {
                                x: date,
                                y: record.referralCalls
                            };
                        }

                        dataSeries1.push(newrec1);
                        dataSeries2.push(newrec2);
                        dataSeries3.push(newrec3);
                        dataSeries4.push(newrec4);
                    });

                    var chartOptions = {
                        plugins: {
                            datalabels: {
                                align: 'end',
                                anchor: 'end',
                                color: 'transparent',
                                formatter: function(value, context) {
                                    //console.log('StatisticsChartView:createChart:' + context);
                                    return "";
                                },
                                font: {
                                    family: "'Source Sans Pro','Helvetica Neue', Helvetica, Arial, sans-serif",
                                    weight: 'bold',
                                    size: 14
                                }
                            }
                        },
                        legend: {
                            display: true,
                            position: 'right'
                        },
                        scales: {
                            xAxes: [{
                                //height: 120,
                                display: true,
                                type: 'time',
                                unit: 'day',
                                time: {
                                    unit: 'day',
                                    //minUnit:'day',
                                    tooltipFormat:'MM-DD-YY',
                                    unitStepSize:2,
                                    displayFormats: {
                                        //quarter: 'MM-DD-YY'
                                        day: 'DD'
                                    },
                                    max:self.endDate,
                                    min:self.startDate
                                },
                                ticks: {
                                    //autoSkip: false,
                                    maxRotation: 0,
                                    minRotation: 0
                                },
                                position: 'bottom'
                            }]
                        },
                        maintainAspectRatio: false,
                        responsive: true
                    };

                    // Create scatter chart
                    var ctx = self.$("#statisticsChart").get(0).getContext("2d");

                    var chartData = {
                        datasets: [{
                            labels: [],
                            label: self.metric1,
                            backgroundColor: "rgba(210, 214, 222,0.4)",
                            borderColor: "rgba(210, 214, 222,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            fillColor: "rgb(210, 214, 222)",
                            strokeColor: "rgb(210, 214, 222)",
                            pointColor: "rgb(210, 214, 222)",
                            pointStrokeColor: "#c1c7d1",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgb(220,220,220)",
                            data: dataSeries1
                        }, {
                            label: self.metric2,
                            lineTension: 0.1,
                            backgroundColor: "rgba(160, 208, 224,0.4)",
                            borderColor: "rgba(160, 208, 224,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            fillColor: "rgba(160, 208, 224,0.9)",
                            strokeColor: "rgba(160, 208, 224,0.8)",
                            pointColor: "#3b8bba",
                            pointStrokeColor: "rgba(160, 208, 224,1)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(160, 208, 224,1)",
                            data: dataSeries2
                        }, {
                            label: self.metric3,
                            //fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(60, 141, 188,0.4)",
                            borderColor: "rgba(60, 141, 188,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            fillColor: "rgba(60, 141, 188,0.9)",
                            strokeColor: "rgba(60, 141, 188,0.8)",
                            pointColor: "#3C8DBC", //"#3b8bba",
                            pointStrokeColor: "rgba(60, 141, 188,1)",
                            pointHighlightFill: "#3C8DBC",
                            pointHighlightStroke: "rgba(60, 141, 188,1)",
                            pointBorderColor: "rgba(60, 141, 188,1)",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(60, 141, 188,1)",
                            pointHoverBorderColor: "rgba(60, 141, 188,1)",
                            pointHoverBorderWidth: 2,
                            //pointRadius: 1,
                            pointHitRadius: 10,
                            //spanGaps: false,
                            data: dataSeries3
                        },{
                            label: self.metric4,
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(230, 230, 250,0.4)",
                            borderColor: "rgba(230, 230, 250,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            fillColor: "rgba(230, 230, 250,0.9)",
                            strokeColor: "rgba(230, 230, 250,0.8)",
                            pointColor: "rgb(230, 230, 250)",
                            pointStrokeColor: "rgba(230, 230, 250,1)",
                            pointHighlightFill: "#3C8DBC",
                            pointHighlightStroke: "rgba(230, 230, 250,1)",
                            pointBorderColor: "rgba(0, 0, 0,1)",
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(230, 230, 250,1)",
                            pointHoverBorderColor: "rgba(0, 0, 0,1)",
                            pointHoverBorderWidth: 2,
                            //pointRadius: 5,
                            pointHitRadius: 10,
                            spanGaps: false,
                            data: dataSeries4
                        }]
                    };

                    if (self.options.type === "Time") {
                        chartData.datasets.push({
                            label: self.metric5,
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(75,192,192,0.4)",
                            borderColor: "rgba(75,192,192,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: "rgba(75,192,192,1)",
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(75,192,192,1)",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            spanGaps: false,
                            data: dataSeries5
                        });
                    }

                    self.scatterChart = new Chart(ctx, {
                        type: 'line',
                        data: chartData,
                        options: chartOptions
                    });

                    //self.scatterChart.chart.height(120);
                    self.scatterChart.resize();
                });


            },
            displayNoDataOverlay: function () {
                //console.log('ProjectGridView:displayNoDataOverlay');

                // Hide the chart
                this.$('#statisticsChart').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message" style="height:130px;">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProjectGridView:hideNodDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the chart
                this.$('#statisticsChart').css('display', 'block');
            },
            toggleMoreInfo: function () {
//                //console.log('TileChangeOrdersView:toggleMoreInfo');

                if (this.$('.tile-hover-tip').hasClass('tile-hover-tip-displayed')) {
                    this.$('.tile-hover-tip').removeClass('tile-hover-tip-displayed');
                    this.$('.tile-more-info').removeClass('tile-more-info-rotated');
                }
                else {
                    this.$('.tile-hover-tip').addClass('tile-hover-tip-displayed');
                    this.$('.tile-more-info').addClass('tile-more-info-rotated');
                }
            },
            mouseEnter: function () {
//                //console.log('StatisticsChartView:mouseEnter');

                self.$('#statisticsChartTile').find('.tile-icon').addClass('clr-selected');
            },
            mouseLeave: function () {
//                //console.log('StatisticsChartView:mouseLeave');

                self.$('#statisticsChartTile').find('.tile-icon').removeClass('clr-selected');
            },
            onResize: function () {
//                //console.log('StatisticsChartView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('StatisticsChartView:resize');

                //this.scatterChart.resize();
                //this.$('#statisticsChart').height(this.$el.parent().height() - 15);

            },
            remove: function () {
                //console.log('---------- StatisticsChartView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off

                // Remove view events setup in events configuration
                this.undelegateEvents();

                // Destroy chart
                //this.scatterChart.clear();
                //this.$('#statisticsChart').data('kendoChart').destroy();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });