define(['App', 'jquery', 'hbs!templates/socialMediaLayout', 'backbone', 'marionette',
        'views/SocialMediaCollectionView', 'views/SubHeaderView'],
    function (App, $, template, Backbone, Marionette, SocialMediaCollectionView,SubHeaderView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                socialmedia: '#social-media'
            },
            initialize: function () {
                //console.log('SocialMediaLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('SocialMediaLayoutView:onShow');

                this.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Social Media",
                    minorTitle: "Help us promote our online presence",
                    page: "Social Media",
                    showDateFilter: false,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));

                this.socialmedia.show(new SocialMediaCollectionView({category: 3}));

            },
            transitionIn: function () {
                console.log('SocialMediaLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('SocialMediaLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('SocialMediaLayoutView:resize');
                var self = this;
            },
            remove: function () {
                //console.log('---------- SocialMediaLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });