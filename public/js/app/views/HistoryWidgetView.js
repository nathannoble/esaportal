define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/historyWidget', 'views/HistoryCollectionView',
        'kendo/kendo.data', 'date'],
    //'kendo/kendo.binder'
    function (App, Backbone, Marionette, $, template, HistoryCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                historyList: '#history-list'
            },
            
            initialize: function (options) {
                //console.log('HistoryWidgetView:initialize');

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Client"};
                }

                this.viewModel = kendo.observable({
                    isChecked: false
                });
                this.userKey = parseInt(App.model.get('userKey'), 0);  //10268;

                this.clientID = parseInt(App.model.get('selectedClientId'), 0); //2189;
                
                this.firstName = "First";
                this.lastName = "Last";

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                //console.log('HistoryWidgetView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('HistoryWidgetView:onShow');

                // Setup the containers
                this.resize();

                var self = this;

                this.historyList.show(new HistoryCollectionView({type: this.options.type}));
            },
           
            displayNoDataOverlay: function () {
                //console.log('HistoryWidgetView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('HistoryWidgetView:resize');

            },
            remove: function () {
                //console.log('-------- HistoryWidgetView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });