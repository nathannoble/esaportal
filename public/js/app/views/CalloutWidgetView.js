define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/calloutWidget', 'kendo/kendo.data', 'kendo/kendo.combobox'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            events: {
                'click #alertAction': 'onAlertActionClicked'
            },
            template: template,
            initialize: function (options) {
                console.log('CalloutWidgetView:initialize:options:' + options);

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Tips"};
                }

                this.userType = parseInt(App.model.get('userType'), 0);
                this.today = moment().clone().format('YYYY-MM-DD');
                this.listenTo(App.vent, App.Events.ModelEvents.WelcomePageClosedChanged, this.onWelcomePageClosedChanged);


            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('---------- CalloutWidgetView:onShow ----------');

                this.populateWidget();
            },
            populateWidget: function () {
                console.log('CalloutWidgetView:populateWidget');

                var self = this;

                self.timerStatus = App.model.get('timerStatus');
                self.$('#closeAlert').hide();
                self.$('#alertAction').hide();
                self.$('#alertTextGroup').show();
                self.$('#alertText2').hide();
                //self.$('#alertText3').hide();

                if (self.options.type === "Value") {

                    this.valueDataSource = new kendo.data.DataSource({
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalValue";
                                },
                                complete: function (e) {
                                    console.log('CalloutWidgetView:valueDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'CalloutWidgetView:valueDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    return App.config.DataServiceURL + "/odata/PortalValue" + "(" + data.valueID + ")";
                                },
                                complete: function (e) {
                                    console.log('CalloutWidgetView:valueDataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'CalloutWidgetView:valueDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "valueID",
                                fields: {
                                    valueID: {editable: false, defaultValue: 0, type: "number"},
                                    lastDisplayed: {type: "string"},
                                    valueTitle: {type: "string"},
                                    valueText: {type: "string"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   CalloutWidgetView:valueDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingCalloutWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    CalloutWidgetView:valueDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingCalloutWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   CalloutWidgetView:valueDataSource:error');
                            kendo.ui.progress(self.$("#loadingCalloutWidget"), false);


                        },
                        filter: [{field: "lastDisplayed", operator: "eq", value: self.today}],
                        sort: {field: "lastDisplayed", dir: "asc"}
                    });

                    this.valueDataSource.fetch(function () {

                        var data = this.data();
                        var header = "";
                        var text = "";
                        self.$('.alert').addClass('bg-blue');

                        if (data.length > 0) {
                            var record = data[0];
                            header = record.valueTitle;
                            text = record.valueText;
                            self.$('#alertText').html(text);
                            self.$('#alertHeader').html(header);
                        } else {
                            self.valueDataSource.filter([]);

                            self.valueDataSource.fetch(function () {
                                var data2 = this.data();
                                var record = data2[0];
                                header = record.valueTitle;
                                text = record.valueText;
                                self.$('#alertText').html(text);
                                self.$('#alertHeader').html(header);

                                //console.log('CalloutWidgetView:valueDataSource:valueText:' + text);
                                //console.log('CalloutWidgetView:valueDataSource:lastDisplayed:' + record.lastDisplayed);

                                if (record.lastDisplayed !== self.today) {
                                    record.set("lastDisplayed", self.today);
                                    self.valueDataSource.sync();
                                }
                            });
                        }
                    });
                } else if (self.options.type === "Tips"){

                    this.tipDataSource = new kendo.data.DataSource({
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalTip";
                                },
                                complete: function (e) {
                                    console.log('CalloutWidgetView:tipDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'CalloutWidgetView:tipDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    return App.config.DataServiceURL + "/odata/PortalTip" + "(" + data.tipID + ")";
                                },
                                complete: function (e) {
                                    console.log('CalloutWidgetView:tipDataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'CalloutWidgetView:tipDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            //<cfquery name="getTipToday" datasource="#APPLICATION.dataSource#">
                            //    SELECT    tipID, tipText, lastDisplayed
                            //FROM      tblPortalTips
                            //WHERE     lastDisplayed LIKE '#vToday#'
                            //</cfquery>

                            model: {
                                id: "tipID",
                                fields: {
                                    tipID: {editable: false, defaultValue: 0, type: "number"},
                                    lastDisplayed: {type: "string"},
                                    tipTitle: {type: "string"},
                                    tipText: {type: "string"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   CalloutWidgetView:valueDataSource:requestStart');
                            kendo.ui.progress(self.$("#loadingCalloutWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    CalloutWidgetView:valueDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingCalloutWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   CalloutWidgetView:valueDataSource:error');
                            kendo.ui.progress(self.$("#loadingCalloutWidget"), false);
                        },
                        filter: [{field: "lastDisplayed", operator: "eq", value: self.today}],
                        sort: {field: "lastDisplayed", dir: "asc"}
                    });

                    this.tipDataSource.fetch(function ()
                    {

                        var header = "Tips and Tricks";
                        self.$('.alert').addClass('bg-blue');
                        self.$('#alertHeader').html(header);

                        var data = this.data();

                        var text = "";

                        if (data.length > 0) {
                            var record = data[0];
                            text = record.tipText;
                            self.$('#alertText').html(text);
                        } else {
                            self.tipDataSource.filter([]);

                            self.tipDataSource.fetch(function () {
                                var data2 = this.data();
                                var record = data2[0];

                                text = record.tipText;
                                self.$('#alertText').html(text);

                                //console.log('CalloutWidgetView:tipDataSource:tipText:' + text);
                                //console.log('CalloutWidgetView:tipDataSource:lastDisplayed:' + record.lastDisplayed);
                                //console.log('CalloutWidgetView:tipDataSource:today:' + self.today);

                                if (record.lastDisplayed !== self.today) {
                                    record.set("lastDisplayed", self.today);
                                    self.tipDataSource.sync();
                                }
                            });
                        }

                    });
                } else if (self.options.type === "Message"){

                    this.messageDataSource = new kendo.data.DataSource({
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalMessage";
                                },
                                complete: function (e) {
                                    console.log('CalloutWidgetView:messageDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'CalloutWidgetView:messageDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "messageID",
                                //{
                                // "messageID":1008,
                                // "messageDate":"2011-10-01",
                                // "messageText":"Thanks for helping make ESA a great place to work!"
                                //}
                                fields: {
                                    messageID: {editable: false, defaultValue: 0, type: "number"},
                                    messageText: {editable: true, type: "string", validation: {required: false}},
                                    messageDate: {editable: true, type: "date", validation: {required: true}}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   CalloutWidgetView:messageDataSource:requestStart');

                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                        },
                        change: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
                        },
                        filter: [
                            {field: "messageDate", operator: "eq", value: self.today}
                        ],
                        sort: {field: "messageDate", dir: "desc"}
                    });

                    this.messageDataSource.fetch(function () {

                        var data = this.data();
                        if (data.length > 0) {
                            $('#alert-message').removeClass("hidden");
                            var text = data[0].messageText;
                            self.$('.alert').addClass('alert-warning');
                            self.$('#alertHeader').html("Today's Message");
                            self.$('#alertText').html(text);
                        } else {
                            //console.log('CalloutWidgetView:populateWidget:no message:');
                            self.remove();
                        }

                    });

                } else if (self.options.type === "Open Timer") {
                    // If previously open timers exist...notification for users of type 3

                    this.openTimerDataSource = new kendo.data.DataSource(
                        {
                        autoBind: false,
                        //type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetOpenTimers/" + self.today;
                                    return url;
                                },
                                complete: function (e) {
                                    console.log('CalloutWidgetView:openTimerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'CalloutWidgetView:openTimerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {
                            model: {
                                id: "intTimerID",
                                fields: {

                                    intTimerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    employeeID: {editable: false, type: "number"},
                                    serviceID: {editable: false, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    clientName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: true}},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   CalloutWidgetView:messageDataSource:requestStart');

                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                        },
                        change: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;
                        }
                    });

                    if (self.userType === 3) {
                        this.openTimerDataSource.fetch(function () {

                            var data = this.data();
                            if (data.length > 0) {
                                $('#alert-open-timer').removeClass("hidden");
                                self.$('.alert').addClass('alert-danger');
                                self.$('.alert').addClass('alert-dismissible');
                                self.$('#closeAlert').show();
                                self.$('#alertHeader').html("Open Timers");
                                self.$('#alertText').html("There are " + data.length + " open timers prior today.  Visit the <a href='#openTimers'>Open Timers Prior to Today</a> page to edit and resolve.");
                            } else {
                                //console.log('CalloutWidgetView:populateWidget:no message:');
                                self.remove();
                            }

                        });
                    }
                } else if (self.options.type === "New Chat Message"){

                    $('#alert-message').removeClass("hidden");
                    self.$('.alert').addClass('alert-info');
                    self.$('.alert').addClass('alert-dismissible');
                    self.$('#alertAction').show();
                    self.$('#alertHeader').html("New Company Chat Message");
                    self.$('#alertText2').show();
                    self.$('#alertText2').html(self.options.title + ": " + self.options.message);
                    self.$('#alertTextGroup').hide();

                }

                if (self.options.borderRadius !== undefined) {
                    self.$('.alert').css('border-radius', self.options.borderRadius);
                }
            },
            onWelcomePageClosedChanged: function (e) {
                console.log('CalloutWidgetView:onWelcomePageClosedChanged');

                this.close();

            },
            onAlertActionClicked: function (e) {
                console.log('CalloutWidgetView:onAlertActionClicked');
                //var scrollingElement = (document.scrollingElement || document.body);
                //scrollingElement.scrollTop = scrollingElement.scrollHeight;

                this.close();

                $([document.getElementById("dashboard-section"), document.body]).animate({
                    scrollTop: $("#welcome-chat").offset().top - 100
                }, 1000);

                //var element = document.getElementById("dashboard-section");
                //element.scrollTop = element.scrollHeight - element.clientHeight;
                App.model.set('chatMessageDismissed', this.today);


            },
            onResize: function () {
//                //console.log('CalloutWidgetView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('CalloutWidgetView:resize');

            },
            remove: function () {
                console.log('---------- CalloutWidgetView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
                //this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });