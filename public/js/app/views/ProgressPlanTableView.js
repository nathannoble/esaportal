define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/ProgressPlanTable', 'views/ProgressPlanGridView'],
    function (App, Backbone, Marionette, $, template, ProgressPlanGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                planRegion1: "#plan-region-1"
            },
            events: {
                'click #SaveButton': 'saveButtonClicked'
            },
            initialize: function () {

                //console.log('ProgressPlanTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('ProgressPlanTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressPlanTableView:onShow --------");

                this.userId = App.model.get('userId');
                this.userType = App.model.get('userType');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    if (this.userType === 2) {
                        this.hidePlans = App.model.get('hidePlans');
                        if (this.hidePlans) {
                            console.log('EmployeeProfileLayoutView:onShow:you do not have permissions to see the plans list.');
                            App.model.set('callbackErrorMessage',6);
                            //alert('You do not have permissions to see the plans list.');
                            //history.back();
                        }
                    }
                    this.planRegion1.show(new ProgressPlanGridView());
                }
            },

            saveButtonClicked: function (e) {
                //console.log('ProgressPlanTableView:saveButtonClicked');

                var self = this;

            },
            resize: function () {
                //console.log('ProgressPlanTableView:resize');

                if (this.planRegion1.currentView)
                    this.planRegion1.currentView.resize();

            },
            remove: function () {
                //console.log("--------ProgressPlanTableView:remove --------");

                //this.saveButtonClicked();

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });