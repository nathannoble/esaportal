define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalTimeCardDetailGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalTimeCardDetailGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "manyTimerRecords"};
                }

                this.timeCardDetailGridFilter = App.model.get('timeCardDetailGridFilter');

                this.userType = parseInt(App.model.get('userType'), 0);
                this.isExecutive = false;
                if (self.userType === 3) {
                    this.isExecutive = true;
                }

                this.dateFilter = App.model.get('selectedDateFilter');
                var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                this.startDate = moment(startDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                this.endDate = moment(endDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                this.employeeIDs = [];
                this.selectedTeamLeaderId = App.model.get('selectedTeamLeaderId');
                this.myTeamLeaderId = parseInt(App.model.get('myTeamLeaderId'), 0);

                if (this.selectedTeamLeaderId === null) {
                    this.selectedTeamLeaderId = this.myTeamLeaderId;
                } else {
                    this.selectedTeamLeaderId = parseInt(App.model.get('selectedTeamLeaderId'), 0);
                }

                this.filters = [];
                this.statusFilter = App.model.get('selectedStatusFilter');
                if (this.statusFilter === null || this.statusFilter === undefined || this.options.type === "My Clients") {
                    this.statusFilter = "4 - Active";
                }

                if (this.statusFilter !== "All Status") {
                    this.filters.push({field: "status", operator: "eq", value: self.statusFilter});
                }

                this.empFilters = [];
                this.empStatusFilter = App.model.get('selectedEmpStatusFilter');
                if (this.empStatusFilter === null || this.empStatusFilter === undefined || this.options.type === "My Clients") {
                    this.empStatusFilter = "1 - Active";
                }

                if (this.empStatusFilter !== "All Status") {
                    //this.filters.push({field: "schedStatus", operator: "eq", value: self.empStatusFilter});
                    this.empFilters.push({field: "status", operator: "eq", value: self.empStatusFilter});
                }
                //console.log('PortalTimeCardDetailGridView:initialize:this.selectedTeamLeaderId:' + this.selectedTeamLeaderId);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);


            },
            onRender: function () {
                //console.log('PortalTimeCardDetailGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalTimeCardDetailGridView:onShow --------");
                var self = this;
                var app = App;

                this.clientDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        batch: false,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                },
                                complete: function (e) {
                                    console.log('PortalTimeCardDetailGridView:clientDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalTimeCardDetailGridView:clientDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "clientID",
                                fields: {
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    schedulerID: {editable: false, defaultValue: 0, type: "number"},
                                    //schedulerName: {editable: true, type: "string", validation: {required: false}},
                                    companyID: {editable: false, defaultValue: 0, type: "number"},
                                    //clientName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    clientCompany: {editable: true, type: "string", validation: {required: false}}
                                    //schedStatus: {editable: false, type: "string", validation: {required: false}}
                                    //status: {editable: false, type: "string", validation: {required: false}},
                                    //planHours: {type: "number"},
                                    //taskTargetLow: {type: "number"},
                                    //taskTargetHigh: {type: "number"},
                                    //mtgTargetLowDec: {type: "number"},
                                    //mtgTargetHigh: {type: "number"}
                                }
                            }
                        },
                        requestStart: function () {
                            //console.log('   ProgressClientDataGridView:requestStart');

                            kendo.ui.progress($("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    ProgressClientDataGridView:requestEnd');
                            kendo.ui.progress($("#loadingTimerWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ProgressClientDataGridView:error');
                            kendo.ui.progress($("#loadingTimerWidget"), false);

                        },
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"},
                            {field: "clientCompany", dir: "asc"}
                        ],
                        filter: self.filters //[{field: "status", operator: "eq", value: "4 - Active"}]

                    });

                this.companyDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                complete: function (e) {
                                    console.log('PortalTimeCardDetailGridView:companyDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalTimeCardDetailGridView:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyID",
                                fields: {
                                    companyID: {editable: false, type: "number"},
                                    clientCompany: {type: "string"}
                                }
                            }
                        },
                        sort: {field: "clientCompany", dir: "asc"},
                        serverSorting: true,
                        serverFiltering: true,
                        //serverPaging: true,
                        filter: [
                            {field: "clientCompany", operator: "neq", value: "--- NONE ---"}
                            //{field: "actualClients", operator: "gt", value: 0},
                            //{field: "actualClients", operator: "neq", value: null}
                        ]

                    });

                //var schedulerFilters = [];
                //schedulerFilters.push({field: "status", operator: "eq", value: '1 - Active'});
                if (self.selectedTeamLeaderId !== 0) {
                    self.empFilters.push({field: "teamLeaderID", operator: "eq", value: self.selectedTeamLeaderId});
                }
                this.schedulerDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('PortalTimeCardDetailGridView:schedulerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalTimeCardDetailGridView:schedulerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    status: {type: "string", validation: {required: false}},
                                    isTeamLeader: {editable: true, type: "boolean"},
                                    teamLeaderID: {editable: false, type: "number"}
                                }
                            }
                        },
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"}
                        ],
                        filter: self.empFilters,
                        serverSorting: true,
                        serverFiltering: true
                    });

                this.serviceDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalServiceItem";
                                },
                                complete: function (e) {
                                    console.log('PortalTimeCardDetailGridView:serviceDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalTimeCardDetailGridView:serviceDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "serviceID",
                                fields: {
                                    serviceID: {editable: false, defaultValue: 0, type: "number"},
                                    //ordering: {editable: false, defaultValue: 0, type: "number"},
                                    serviceItem: {editable: true, type: "string", validation: {required: false}}
                                    //recordTasksMtgs: {editable: true, type: "boolean"},
                                    //toStats: {editable: true, type: "boolean"},
                                    //timerStatus: {editable: true, type: "number"}
                                }
                            }
                        },
                        //serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        sort: {field: "ordering", dir: "asc"}
                    });

                // Load up the relevant data
                this.getData();

            },
            getData: function () {
                //console.log('PortalTimeCardDetailGridView:getData');

                var self = this;
                var app = App;

                var restArgs = self.startDate + "/" + self.endDate + "/stopTime";
                if (self.options.type === "oneTimerRecord") {
                    var timerID = parseInt(app.model.get('selectedTimerId'), 0);
                    restArgs = timerID;
                }

                this.dataSource = new kendo.data.DataSource(
                    {
                        transport: {
                            read: function (options) {
                                //self.dataSource.cancelled = true;
                                $.ajax({
                                    type: "GET",  //PATCH?
                                    contentType: "application/json", //; charset=utf-8",
                                    url: App.config.DataServiceURL + "/rest/GetTimeCardDetailByIntTimerID/" + restArgs,
                                    dataType: "json",
                                    requestStart: function (e) {
                                        kendo.ui.progress(self.$("#timeCardDetailLoading"), true);
                                    },
                                    requestEnd: function (e) {
                                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                                    },
                                    success: function (result) {
                                        // notify the data source that the request succeeded
                                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                                        options.success(result);
                                    },
                                    error: function (result) {
                                        // notify the data source that the request failed
                                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                                        options.error(result);
                                    }
                                });
                            },
                            destroy: function (options) {
                                //self.dataSource.cancelled = true;
                                $.ajax({
                                    type: "DELETE",
                                    contentType: "application/json", //; charset=utf-8",
                                    url: App.config.DataServiceURL + "/odata/PortalTimer" + "(" + options.data.intTimerID + ")",
                                    dataType: "json",
                                    requestStart: function (e) {
                                        //kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                                        kendo.ui.progress(self.$("#timeCardDetailLoading"), true);
                                    },
                                    requestEnd: function (e) {
                                        //var data = this.data();
                                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                                    },
                                    success: function (result) {
                                        // notify the data source that the request succeeded
                                        options.success(result);

                                        //self.populateProgressData();


                                    },
                                    error: function (result) {
                                        // notify the data source that the request failed
                                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);

                                        options.error(result);
                                    }
                                });
                            }
                        },
                        schema: {
                            //data: "value",
                            total: function (data) {
                                return data.length;
                            },
                            model: {
                                id: "intTimerID",
                                fields: {
                                    //timerID: {editable: false, type: "number"},
                                    intTimerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    employeeID: {editable: false, type: "number"},
                                    serviceID: {editable: false, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    clientName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: true}},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}},
                                    editedByName: {editable: true, type: "string", validation: {required: false}}

                                }
                            }
                        },
                        aggregate: [
                            {field: "timeWorked", aggregate: "sum"},
                            {field: "tasks", aggregate: "sum"},
                            {field: "meetings", aggregate: "sum"}
                        ],
                        sort: [
                            {field: "workDate", dir: "asc"},
                            {field: "employeeName", dir: "asc"},
                            {field: "startTime", dir: "asc"}
                        ],
                        requestStart: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:request start:');
                            //kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                            kendo.ui.progress(self.$("#timeCardDetailLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:change:');
                            //kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:error');
                            kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                        }
                    });

                if (self.options.type === "oneTimerRecord") {
                    this.dataSource.aggregate([]);
                }

                this.dataSource.pageSize(100);
                this.dataSource.page(1);
                this.dataSource.fetch(function (data)
                {
                    self.schedulerDataSource.fetch(function (data) {

                        self.schedulers = [];
                        self.employeeIDs = [];

                        var schedulerRecords = this.data();

                        $.each(schedulerRecords, function (index, value) {
                            self.schedulers.push({
                                value: value.lastName + ", " + value.firstName,
                                text: value.lastName + ", " + value.firstName
                            });
                            if (self.selectedTeamLeaderId !== 0 || self.empStatusFilter !== "All Status") {
                                self.employeeIDs.push(value.employeeID);
                            }
                        });

                        self.config =
                        {
                            toolbar: ["excel"],
                            excel: {
                                fileName: "TimeCardReport.xlsx",
                                allPages: true
                            },
                            //autoBind: false,
                            selectable: "row",
                            sortable: true,
                            extra: false,
                            filterable: {
                                mode: "row"
                            },
                            serverAggregates: true,
                            aggregate: [{field: "timeWorked", aggregate: "sum"}],
                            //pageable: {
                            //    input: true,
                            //    refresh:true,
                            //    pageSize: 10
                            //    page:1
                            //},
                            pageable: true,
                            resizable: true,
                            reorderable: true,
                            columnResize: function (e) {
                                window.setTimeout(self.resize, 10);
                            },
                            columns: [
                                {
                                    width: 25,
                                    template: function (e) {
                                        var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction edit-timecard' data-id='" + e.intTimerID + "' timer-action='" + e.timerAction + "' record-type='" + e.recordType + "' work-date='" + e.workDate + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                                        return template;
                                    },
                                    filterable: false,
                                    sortable: true
                                },
                                {
                                    field: "intTimerID",
                                    title: "ID",
                                    width: 60,
                                    filterable: false
                                },
                                //{
                                //    width: 25,
                                //    template: function (e) {
                                //        var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.intTimerID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                //        return template;
                                //    },
                                //    filterable: false,
                                //    sortable: true
                                //    //hidden:!self.isExecutive
                                //},
                                {
                                    title: "Client Company",
                                    field: "clientCompany",
                                    template: function (e) {
                                        var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                        return template;
                                    },
                                    width: 200,
                                    filterable: {
                                        cell: {
                                            showOperators: false,
                                            template: self.companyFilter
                                        }
                                    }
                                },
                                {
                                    title: "Client Name",
                                    field: "clientName",
                                    width: 200,
                                    template: function (e) {
                                        var template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" + e.clientName + "</a></div>";
                                        return template;
                                    },
                                    sortable: true,
                                    filterable: {
                                        cell: {
                                            showOperators: false,
                                            template: self.clientFilter
                                        }
                                    }
                                },
                                {
                                    field: "employeeName",
                                    title: "Employee",
                                    width: 200,
                                    filterable: {
                                        cell: {
                                            showOperators: false,
                                            template: self.schedulerFilter
                                        }
                                    }
                                }, {
                                    field: "serviceItem",
                                    title: "Service",
                                    width: 200,
                                    filterable: {
                                        cell: {
                                            showOperators: false,
                                            template: self.serviceFilter
                                        }
                                    }
                                }, {
                                    field: "workDate",
                                    title: "Date",
                                    width: 80,
                                    format: "{0:M/d/yyyy}",
                                    filterable: false
                                },
                                {
                                    field: "startTime",
                                    title: "Start Time",
                                    width: 80,
                                    template: function (e) {
                                        var template = "<div style='text-align: left'>" + self.formatTime(e.startTime) + "</div>";
                                        return template;
                                    },
                                    format: "{0:h:mm:ss tt}",
                                    filterable: false
                                },
                                {
                                    field: "stopTime",
                                    title: "Stop Time",
                                    width: 80,
                                    template: function (e) {
                                        var template = "<div style='text-align: left'>" + self.formatTime(e.stopTime) + "</div>";
                                        return template;
                                    },
                                    format: "{0:h:mm:ss tt}",
                                    filterable: false
                                },

                                {
                                    field: "timeWorked",
                                    title: "Time Worked",
                                    width: 100,
                                    filterable: false,
                                    footerTemplate: "#= kendo.toString(sum,'n2') #"
                                }, {
                                    field: "tasks",
                                    title: "Tasks",
                                    width: 100,
                                    filterable: false,
                                    footerTemplate: "#= kendo.toString(sum,'n2') #"
                                }, {
                                    field: "meetings",
                                    title: "Meetings",
                                    width: 100,
                                    filterable: false,
                                    footerTemplate: "#= kendo.toString(sum,'n2') #"
                                }, {
                                    field: "timerNote",
                                    title: "Note",
                                    width: 200,
                                    filterable: false
                                }, {
                                    field: "mgrNote",
                                    title: "Manager Note",
                                    width: 150,
                                    filterable: false
                                }, {
                                    field: "timerAction",
                                    title: "Action",
                                    width: 100,
                                    filterable: false
                                }, {
                                    field: "recordType",
                                    title: "Type",
                                    width: 100,
                                    filterable: false
                                }, {
                                    field: "editedByName",
                                    title: "Edited By",
                                    width: 100,
                                    filterable: false
                                }
                            ],
                            dataBound: function (e) {
                                //console.log("PortalTimeCardDetailGridView:def:onShow:dataBound");
                                //kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                                self.$('.client-profile').on('click', self.viewClientProfile);
                                self.$('.company-profile').on('click', self.viewCompanyProfile);
                                self.$('.edit-timecard').on('click', self.editTimecard);

                                // save current filter
                                var filters = null;
                                if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                                    filters = e.sender.dataSource.filter().filters;

                                    var timeCardDetailGridFilter = [];

                                    $.each(filters, function (index, value) {
                                        timeCardDetailGridFilter.push(value);
                                    });

                                    //console.log("PortalTimeCardDetailGridView:def:onShow:dataBound:set timeCardDetailGridFilter:" + JSON.stringify(timeCardDetailGridFilter));
                                    app.model.set('timeCardDetailGridFilter', timeCardDetailGridFilter);
                                }
                                setTimeout(self.resize, 0);
                            },
                            remove: function (e) {
                                //console.log('PortalTimeCardDetailGridView:def:onShow:onRemove');
                                if (self.isExecutive !== true) {
                                    alert('Executive privileges are required to delete a timecard record.');
                                    this.cancelChanges();
                                    return;
                                }

                                // Get serviceID, clientID, and workDate from deleted row
                                self.serviceID = e.model.serviceID;
                                self.clientID = e.model.clientID;
                                self.workDate = e.model.workDate;

                            },
                            dataSource: self.dataSource
                        };

                        if (self.options.type === "oneTimerRecord")
                        {

                            self.config =
                            {
                                //selectable: "row",
                                //editable: "popup",
                                //sortable: true,
                                extra: false,
                                filterable: false,
                                serverAggregates: false,
                                //aggregate: [{field: "timeWorked", aggregate: "sum"}],
                                resizable: true,
                                //reorderable: true,
                                columnResize: function (e) {
                                    window.setTimeout(self.resize, 10);
                                },
                                columns: [
                                    {
                                        title: "Client Company",
                                        field: "clientCompany",
                                        template: function (e) {
                                            var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                            return template;
                                        },
                                        width: 150
                                    },
                                    {
                                        title: "Client Name",
                                        field: "clientName",
                                        width: 150,
                                        template: function (e) {
                                            var template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" + e.clientName + "</a></div>";
                                            return template;
                                        }
                                    },
                                    {
                                        field: "employeeName",
                                        title: "Employee",
                                        width: 150
                                    }, {
                                        field: "serviceItem",
                                        title: "Service",
                                        width: 150
                                    }, {
                                        field: "workDate",
                                        title: "Date",
                                        width: 80,
                                        format: "{0:M/d/yyyy}",
                                        filterable: false
                                    }, {
                                        field: "startTime",
                                        title: "Start Time",
                                        //template: function (e) {
                                        //    var template = "<div style='text-align: left'>" + self.formatTime(e.startTime) + "</div>";
                                        //    return template;
                                        //},
                                        width: 80,
                                        format: "{0:h:mm:ss tt}",
                                        filterable: false
                                    }, {
                                        field: "stopTime",
                                        title: "Stop Time",
                                        width: 80,
                                        format: "{0:h:mm:ss tt}",
                                        filterable: false
                                    }, {
                                        field: "timeWorked",
                                        title: "Time Worked",
                                        width: 100,
                                        filterable: false
                                        //footerTemplate: "#= kendo.toString(sum,'n2') #"
                                    },
                                    {
                                        field: "timerNote",
                                        title: "Note",
                                        width: 200,
                                        filterable: false
                                    }, {
                                        field: "mgrNote",
                                        title: "Manager Note",
                                        width: 150,
                                        filterable: false
                                    }
                                ],
                                dataBound: function (e) {
                                    //console.log("PortalTimeCardDetailGridView:def:onShow:dataBound");
                                    self.$('.client-profile').on('click', self.viewClientProfile);
                                    self.$('.company-profile').on('click', self.viewCompanyProfile);
                                    self.$('.edit-timecard').on('click', self.editTimecard);

                                },
                                change: function (e) {
                                    //console.log('PortalTimeCardDetailGridView:def:onShow:onChange');

                                },
                                dataSource: self.dataSource
                                //filter:[{field: "timerNote", operator: "neq", value: "INTERRUPTED"}]
                            };

                        }

                        if (self.options.type === "oneTimerRecord") {
                            var length = self.dataSource.data().length;
                            //console.log('PortalTimeCardDetailGridView:getData:data:length:' + length);

                            if (length === 1) {
                                $("#timerGroup").hide();
                                return;
                            }
                        }
                        var grid = self.$("#timeCardDetail").kendoGrid(self.config).data("kendoGrid");

                        if (grid !== null && grid !== undefined)
                        {
                            //grid.dataSource = self.dataSource;
                            //grid.setDataSource(self.dataSource);
                            var filters = [];

                            //console.log("PortalTimeCardDetailGridView:self.timeCardDetailGridFilter:" + JSON.stringify(self.timeCardDetailGridFilter));

                            if (self.options.type === "oneTimerRecord") {

                                var timerID = parseInt(app.model.get('selectedTimerId'), 0);
                                filters.push({field: "intTimerID", operator: "eq", value: timerID});
                                grid.dataSource.filter(filters);

                            } else if (self.timeCardDetailGridFilter !== null && self.timeCardDetailGridFilter !== []) {

                                //console.log("PortalTimeCardDetailGridView:grid:set datasource:employeeIDs:" + JSON.stringify(self.employeeIDs));

                                if (self.employeeIDs.length > 0) {
                                    filters = {
                                        logic: "and",
                                        filters: [
                                            {
                                                logic: "or",
                                                filters: []
                                            }
                                        ]
                                    };
                                    $.each(self.employeeIDs, function (index, value) {
                                        filters.filters[0].filters.push({
                                            field: "employeeID",
                                            operator: "eq",
                                            value: value
                                        });
                                    });
                                } else if (self.selectedTeamLeaderId !== 0 || self.empStatusFilter !== "All Status") {
                                    filters = {
                                        logic: "and",
                                        filters: []
                                    };
                                    filters.filters.push({
                                        field: "employeeID",
                                        operator: "eq",
                                        value: 0
                                    });
                                } else {
                                    filters = {
                                        logic: "and",
                                        filters: []
                                    };
                                }

                                $.each(self.timeCardDetailGridFilter, function (index, value) {
                                    filters.filters.push(value);
                                });

                                //console.log("PortalTimeCardDetailGridView:grid:set datasource:filter" + JSON.stringify(filters));
                                grid.dataSource.filter(filters);
                            } else {
                                //grid.dataSource.filter([{field: "timerNote", operator: "neq", value: "INTERRUPTED"}]);
                            }

                            //console.log("PortalTimeCardDetailGridView:grid:set datasource:filter:" + JSON.stringify(filters));

                            // Resize the grid
                            setTimeout(self.resize, 0);
                        }

                    });
                });

            },
            formatTime: function (time) {
                var self = this;

                //console.log('PortalTimeCardDetailGridView: formatTime: ' + time);

                if (time === 0 || time === null || time === undefined || time.toString().indexOf("Mon Jan 01 1900 00:00:00") > -1) {
                    return "--";
                } else {
                    // format: "{0:h:mm:ss tt}",
                    return kendo.toString(time, 'T'); //h:mm:ss tt
                }
            },
            companyFilter: function (container) {
                var self = this;

                //console.log('PortalTimeCardDetailGridView:companyFilter:' + JSON.stringify(container));

                self.companyDataSource.fetch(function (data) {

                    var dataSource = [];
                    var records = this.data();

                    $.each(records, function (index, value) {
                        dataSource.push({value: value.clientCompany, text: value.clientCompany});
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: dataSource,
                        optionLabel: "Select Value"
                        //change: self.onCompanyFilterChanged
                    });
                });
            },
            clientFilter: function (container) {
                var self = this;

                //console.log('PortalTimeCardDetailGridView:clientFilter:' + JSON.stringify(container));

                // For the moment get client records uniquely for this page since they involve inactive clients
                self.clientAllNameRecords = App.model.get('clientAllNameRecords');

                if (self.clientAllNameRecords.length === 0) {

                    self.clientDataSource.fetch(function (data) {

                        var modifiedRecords = [];
                        self.clientAllNameRecords = this.data();

                        $.each(self.clientAllNameRecords, function (index, value) {
                            modifiedRecords.push({
                                value: value.lastName + ", " + value.firstName,
                                text: value.lastName + ", " + value.firstName
                            });
                        });

                        container.element.kendoDropDownList({
                            //autoBind:false,
                            dataValueField: "value",
                            dataTextField: "text",
                            valuePrimitive: true,
                            dataSource: modifiedRecords,
                            optionLabel: "Select Value"
                        });

                        App.model.set('clientAllNameRecords', self.clientAllNameRecords);
                    });
                } else {
                    var modifiedRecords = [];

                    $.each(self.clientAllNameRecords, function (index, value) {
                        modifiedRecords.push({
                            value: value.lastName + ", " + value.firstName,
                            text: value.lastName + ", " + value.firstName
                        });
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: modifiedRecords,
                        optionLabel: "Select Value"
                    });
                }
            },
            serviceFilter: function (container) {
                var self = this;

                //console.log('PortalTimeCardDetailGridView:serviceFilter:' + JSON.stringify(container));

                self.serviceDataSource.fetch(function (data) {

                    var dataSource = [];
                    var records = this.data();

                    $.each(records, function (index, value) {
                        dataSource.push({value: value.serviceItem, text: value.serviceItem});
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: dataSource,
                        optionLabel: "Select Value"
                    });
                });
            },
            schedulerFilter: function (container) {
                var self = this;

                //console.log('PortalTimeCardDetailGridView:schedulerFilter:' + JSON.stringify(container));

                container.element.kendoDropDownList({
                    //autoBind:false,
                    dataValueField: "value",
                    dataTextField: "text",
                    valuePrimitive: true,
                    dataSource: self.schedulers,
                    optionLabel: "Select Value"
                });

            },
            editTimecard: function (e) {
                //console.log('PortalTimeCardDetailGridView:editTimecard:e:' + e.target);

                var self = this;
                var app = App;

                var intTimerId = 0;
                var timerAction = "";
                var recordType = "";
                var workDateString = null;
                if (!isNaN(e.currentTarget.attributes[1].value)) {  // Chrome
                    intTimerId = parseInt(e.currentTarget.attributes[1].value, 0);
                    timerAction = e.currentTarget.attributes[2].value;
                    recordType = e.currentTarget.attributes[3].value;
                    workDateString = e.currentTarget.attributes[4].value;
                } else if (!isNaN(e.currentTarget.attributes[4].value)) {  // IE
                    intTimerId = parseInt(e.currentTarget.attributes[4].value, 0);
                    timerAction = e.currentTarget.attributes[3].value;
                    recordType = e.currentTarget.attributes[2].value;
                    workDateString = e.currentTarget.attributes[1].value;
                } else if (e.target.attributes[2]) {
                    intTimerId = parseInt(e.target.attributes[2].value, 0);
                    timerAction = e.currentTarget.attributes[3].value;
                    recordType = e.currentTarget.attributes[4].value;
                    workDateString = e.currentTarget.attributes[5].value;
                }

                var now = moment().clone().format('YYYY-MM-DD');
                var workDate = moment(workDateString).clone().format('YYYY-MM-DD');

                if ((timerAction.indexOf("none") < 0 || (recordType.indexOf("NORM") >= 0 && recordType.indexOf("STOP") < 0)) && moment(now).isSame(workDate)) {
                    alert("Timer files that are open today cannot be edited.");
                } else {

                    //console.log('PortalTimeCardDetailGridView:viewCompanyProfile:e:' + timerId);
                    $.when(app.model.set('selectedTimerId', intTimerId)).done(function (e) {
                        app.router.navigate('editTimecard', {trigger: true});
                    });
                }

            },
            viewCompanyProfile: function (e) {
                //console.log('PortalTimeCardDetailGridView:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('PortalTimeCardDetailGridView:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId', companyId);


                App.router.navigate('companyProfile', {trigger: true});

            },
            viewClientProfile: function (e) {
                //console.log('PortalTimeCardDetailGridView:viewClientProfile:e:' + e.target);

                var clientId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    clientId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    clientId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('PortalTimeCardDetailGridView:viewClientProfile:e:' + clientId);
                App.model.set('selectedClientId', clientId);


                App.router.navigate('clientProfile', {trigger: true});

            },
            resetStatsDashboardCache: function () {

                //console.log('PortalTimeCardDetailGridView:resetStatsDashboardCache');

                $.cookie('ESA:AppModel:mySumTotalTime', "");
                App.model.set('mySumTotalTime', null);

                $.cookie('ESA:AppModel:mySumTotalMeetings', "");
                App.model.set('mySumTotalMeetings', null);

                $.cookie('ESA:AppModel:mySumMeetingsTarget', "");
                App.model.set('mySumMeetingsTarget', null);

                $.cookie('ESA:AppModel:myMtgPercent', "");
                App.model.set('myMtgPercent', null);

                $.cookie('ESA:AppModel:teamSumTotalTime', "");
                App.model.set('teamSumTotalTime', null);

                $.cookie('ESA:AppModel:teamSumTotalMeetings', "");
                App.model.set('teamSumTotalMeetings', null);

                $.cookie('ESA:AppModel:teamSumMeetingsTarget', "");
                App.model.set('teamSumMeetingsTarget', null);

                $.cookie('ESA:AppModel:teamMtgPercent', "");
                App.model.set('teamMtgPercent', null);
            },
            displayNoDataOverlay: function () {
                //console.log('PortalTimeCardDetailGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#timeCardDetail').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('PortalTimeCardDetailGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#timeCardDetail').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalTimeCardDetailGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var self = this;

                var parentHeight = this.$("#timeCardDetail").parent().parent().height();
                this.$("#timeCardDetail").height(parentHeight);


                if (self.options.type === "oneTimerRecord") {
                    parentHeight = parentHeight - 5;
                } else {
                    parentHeight = parentHeight - 129; //104
                }

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalTimeCardDetailGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalTimeCardDetailGridView:resize:headerHeight:' + headerHeight);
                this.$("#timeCardDetail").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalTimeCardDetailGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#timeCardDetail").data('ui-tooltip'))
                    this.$("#timeCardDetail").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });