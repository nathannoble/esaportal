define(['App', 'jquery', 'hbs!templates/industryNewsLayout', 'backbone', 'marionette',
        'views/NewsCollectionView', 'views/SubHeaderView'],
    function (App, $, template, Backbone, Marionette, NewsCollectionView,SubHeaderView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                news: '#news'
            },
            initialize: function () {
                //console.log('IndustryNewsLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('IndustryNewsLayoutView:onShow');

                this.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Industry News",
                    minorTitle: "Breaking developments our clients are thinking about",
                    page: "Industry News",
                    showDateFilter: false,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));

                this.news.show(new NewsCollectionView({category: 2}));

            },
            transitionIn: function () {
                console.log('IndustryNewsLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('IndustryNewsLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('IndustryNewsLayoutView:resize');
                var self = this;
            },
            remove: function () {
                //console.log('---------- IndustryNewsLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });