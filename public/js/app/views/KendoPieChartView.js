define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/kendoPieChart', 'kendo/kendo.data', 'kendo/kendo.dataviz.chart'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template:template,
            initialize: function(options){
                //console.log('KendoPieChartView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;


            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- KendoPieChartView:onShow ----------');
                
                // Create Pie chart
                this.createChart();

                // UIUX - Events beyond css and view classes
                var self = this;
                this.$('.tile-content').hover(function(){
                    self.$('.tile-icon').toggleClass('tile-icon-hover');
                    self.$('.tile-data').toggleClass('tile-background-hover');
                });

                this.$('.tile-hover-tip').hover(function(){
                    self.$('.tile-icon').toggleClass('tile-icon-hover');
                    self.$('.tile-data').toggleClass('tile-background-hover');
                });

                this.$('.tile-more-info').click(function(){
                    self.toggleMoreInfo();
                    return false;
                });

                this.$('.tile-hover-tip').click(function(){
                    self.toggleMoreInfo();
                    return false;
                });

                if (this.options) {
                    if (this.options.height) {
                        this.$('.tile-content').height(this.options.height);
                    }
                }
            },

            createChart: function(){
//                //console.log('KendoPieChartView:createChart');

                var self = this;
                var app = App;

                // data call
                this.dataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                //return App.config.DataServiceURL + "/odata/DashboardInput/";
                                return "http://dekalbconsentdecree.azurewebsites.net/services/odata/DashboardInput/";
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "Id",
                            fields: {
                                Id: {editable: false, defaultValue: 0, type: "number"},
                                DesignProjects: {type: "number"},
                                ConstructionProjects: {type: "number"},
                                CompleteProjects: {type: "number"},
                                CashFlowStartYear: {type: "string"},
                                CashFlowPrior: {type: "number"},
                                CashFlowYear1: {type: "number"},
                                CashFlowYear2: {type: "number"},
                                CashFlowYear3: {type: "number"},
                                CashFlowYear4: {type: "number"},
                                CashFlowYear5: {type: "number"},
                                CashFlowYear6: {type: "number"},
                                CashFlowYear7: {type: "number"},
                                CashFlowYear8: {type: "number"},
                                CashFlowYear9: {type: "number"},
                                CashFlowYear10: {type: "number"},
                                CashFlowYear11: {type: "number"},
                                ContractAwardsName1: {type: "string"},
                                ContractAwardsValue1: {type: "number"},
                                ContractAwardsName2: {type: "string"},
                                ContractAwardsValue2: {type: "number"},
                                ContractAwardsName3: {type: "string"},
                                ContractAwardsValue3: {type: "number"},
                                ContractAwardsName4: {type: "string"},
                                ContractAwardsValue4: {type: "number"},
                                AssignmentStatusName1: {type: "string"},
                                AssignmentStatusValue1: {type: "number"},
                                AssignmentStatusName2: {type: "string"},
                                AssignmentStatusValue2: {type: "number"},
                                AssignmentStatusName3: {type: "string"},
                                AssignmentStatusValue3: {type: "number"},
                                AssignmentStatusName4: {type: "string"},
                                AssignmentStatusValue4: {type: "number"},
                                AssignmentStatusName5: {type: "string"},
                                AssignmentStatusValue5: {type: "number"},
                                AssignmentStatusName6: {type: "string"},
                                AssignmentStatusValue6: {type: "number"},
                                ContractExpenditureName1: {type: "string"},
                                ContractExpenditureValue1: {type: "number"},
                                ContractExpenditureName2: {type: "string"},
                                ContractExpenditureValue2: {type: "number"},
                                ContractExpenditureName3: {type: "string"},
                                ContractExpenditureValue3: {type: "number"},
                                ContractExpenditureName4: {type: "string"},
                                ContractExpenditureValue4: {type: "number"},
                                CIPJobsName1: {type: "string"},
                                CIPJobsValue1: {type: "number"},
                                CIPJobsName2: {type: "string"},
                                CIPJobsValue2: {type: "number"},
                                CIPJobsName3: {type: "string"},
                                CIPJobsValue3: {type: "number"},
                                CIPJobsName4: {type: "string"},
                                CIPJobsValue4: {type: "number"},
                                CIPExpendituresName1: {type: "string"},
                                CIPExpendituresValue1: {type: "number"},
                                CIPExpendituresName2: {type: "string"},
                                CIPExpendituresValue2: {type: "number"},
                                CIPExpendituresName3: {type: "string"},
                                CIPExpendituresValue3: {type: "number"},
                                CIPExpendituresName4: {type: "string"},
                                CIPExpendituresValue4: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   KendoPieChartView:requestStart');

                        kendo.ui.progress(self.$("#loadingPieChart"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    KendoPieChartView:requestEnd');
                        kendo.ui.progress(self.$("#loadingPieChart"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   KendoPieChartView:error');
                        kendo.ui.progress(self.$("#loadingPieChart"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   KendoPieChartView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    }
                });

                this.dataSource.fetch(function () {

                    var datarecord = this.data()[0];

                    var interpData = [
                        {
                            label: datarecord.CIPExpendituresName1,
                            value: datarecord.CIPExpendituresValue1,
                            color: "lightblue", //App.standards.getColor_mag_1(),#2d84c8
                            borderColor: "lightblue" //App.standards.getColor_mag_1()
                        },
                        {
                            label: datarecord.CIPExpendituresName2,
                            value: datarecord.CIPExpendituresValue2,
                            color: "blue",
                            borderColor: "blue"
                        }
                    ];

                    // Pie chart
                    self.$('#pieChart').kendoChart({

                        chartArea: {
                            //margin: 25,
                            opacity: 0

                        },
                        plotArea: {
                            //margin: 15,
                            opacity: 0
                        },
                        dataSource: {data: interpData},
                        dataBound: function (e) {
                            //console.log('KendoPieChartView:dataBound');
                            //window.setTimeout(self.resize, 10);
                        },
                        legend: {
                            visible: false,
                            position: "bottom",
                            labels: {
                                margin: {
                                    left: -10,
                                    right: -10
                                },
                                font: "16px sans-serif",
                                line:2
                            }
                        },
                        seriesDefaults: {
                            type: "pie",  //pie
                            labels: {
                                visible: true,
                                position: "insideEnd", //insideEnd
                                //align: "column",
                                template: function (e) {
                                    //console.log('TileProgramContractAwardsView:createChart:e' + JSON.stringify(e));
                                    return e.category;
                                },
                                background: "transparent"
                                //template: "#= category #"
                                //position: "insideEnd",  //"insideEnd"
                                //font: "12px sans-serif"
                            }
                        },
                        series: [
                            {
                                //startAngle: -45,
                                categoryField: "label",
                                field: "value",
                                border: {
                                    visible: false,
                                    width: 0.25,
                                    dashType: "solid",
                                    color: "black",
                                    opacity: 0
                                },
                                overlay: {
                                    gradient: "none" //roundedBevel, sharpBevel, none
                                }
                            }
                        ],

                        tooltip: {
                            visible: !App.mobile,
                            shared: false,
                            background: "white",
                            border: {
                                width: 1,
                                color: "black"
                            },
                            template: function (e) {
                                return e.category + ": " + app.Utilities.numberUtilities.getFormattedFinancialNumber(e.value);
                            }
                        }
                    });

                    self.resize();

                });


            },
            displayNoDataOverlay: function () {
                //console.log('ProjectGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProjectGridView:hideNodDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('.tile-content').css('display', 'block');
            },
            toggleMoreInfo: function(){
//                //console.log('TileChangeOrdersView:toggleMoreInfo');

                if (this.$('.tile-hover-tip').hasClass('tile-hover-tip-displayed')){
                    this.$('.tile-hover-tip').removeClass('tile-hover-tip-displayed');
                    this.$('.tile-more-info').removeClass('tile-more-info-rotated');
                }
                else{
                    this.$('.tile-hover-tip').addClass('tile-hover-tip-displayed');
                    this.$('.tile-more-info').addClass('tile-more-info-rotated');
                }
            },
            mouseEnter: function(){
//                //console.log('KendoPieChartView:mouseEnter');

                self.$('#pieChartTile').find('.tile-icon').addClass('clr-selected');
            },
            mouseLeave: function () {
//                //console.log('KendoPieChartView:mouseLeave');

                self.$('#pieChartTile').find('.tile-icon').removeClass('clr-selected');
            },
            onResize: function () {
//                //console.log('KendoPieChartView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('KendoPieChartView:resize');

                this.$('#pieChart').height(this.$el.parent().height()-15);
                if (this.$('#pieChart').data('kendoChart'))
                    this.$('#pieChart').data('kendoChart').refresh();
            },
            remove: function () {
                //console.log('---------- KendoPieChartView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off
                this.$('.tile-content').off();
                this.$('.tile-more-info').off();
                this.$('.tile-hover-tip').off();

                // Clear tooltips
                if (this.$('#pieChart').data('ui-tooltip'))
                    this.$('#pieChart').tooltip('destroy');


                // Remove view events setup in events configuration
                this.undelegateEvents();

                // Destroy chart
                this.$('#pieChart').data('kendoChart').destroy();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });