define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressWorkingDayGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('ProgressWorkingDayGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;
                this.month = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter); //11;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressWorkingDay",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "workDayCount",
                                //{
                                //    "vMonthNum":1,"vMonthName":"January","vYear":2010,"workDayCount":19
                                //},
                                fields: {
                                    vMonthNum: {editable: true, type: "number"},
                                    vMonthName: {editable: true, type: "string", validation: {required: false}},
                                    vYear: {editable: true, type: "string", validation: {required: true}},
                                    workDayCount: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "vMonthNum", dir: "asc"},
                        filter: {field: "vYear", operator: "eq", value: self.year},
                        requestStart: function (e) {
                            //console.log('ProgressWorkingDayGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#workingDayLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('ProgressWorkingDayGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#workingDayLoading"), false);
                        },
                        change: function (e) {
                            //console.log('ProgressWorkingDayGridView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('ProgressWorkingDayGridView:dataSource:error');
                            kendo.ui.progress(self.$("#workingDayLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalWorkingDataGridView:displayNoDataOverlay');

                this.$el.css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No field selected</div></div></div>').appendTo(this.$el.parent());

            },
            onRender: function () {
                //console.log('ProgressWorkingDayGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressWorkingDayGridView:onShow --------");
                var self = this;

                // Panel title
                //$("#workingDayTitle").html("Employees");

                //console.log('ProgressWorkingDayGridView:onShow:districtID:' + this.districtID);
                //if (this.employeeID === null) {
                //    // Go back to home page
                //    window.location.href = "";
                //}

                this.grid = this.$("#workingDay").kendoGrid({

                    //toolbar: ["create"],
                    //editable: "popup",  //inline, popup
                    sortable: true,
                    extra: false,
                    //selectable: "multiple",
                    resizable: true,
                    //reorderable: true,
                    //filterable: true,
                    //scrollable: false,
                    //selectable: "row",
                    //pageable: true,
                    //groupable: true,
                    columns: [
                        {
                            field: "vYear",
                            title: "Year",
                            width: 70
                        }, {
                            field: "vMonthName",
                            title: "Month",
                            width: 100
                            //locked: true
                        }, {
                            field: "workDayCount",
                            title: "Working Days",
                            width: 70
                            //locked: true
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("ProgressWorkingDayGridView:def:onShow:dataBound");
                    },
                    change: function (e) {
                        //console.log('ProgressWorkingDayGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('ProgressWorkingDayGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('ProgressWorkingDayGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        e.container.find("input[name='employeeID']").prop("disabled", true);

                        // Set the focus to the Username field
                        //e.container.find("input[name='Username']").focus();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('ProgressWorkingDayGridView:getData');

                var grid = this.$("#workingDay").data("kendoGrid");

                var self = this;

                //this.dataSource.fetch();

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressWorkingDayGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#workingDay").parent().parent().height();
                this.$("#workingDay").height(parentHeight);

                parentHeight = parentHeight - 15;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressWorkingDayGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressWorkingDayGridView:resize:headerHeight:' + headerHeight);
                this.$("#workingDay").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressWorkingDayGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#workingDay").data('ui-tooltip'))
                    this.$("#workingDay").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });