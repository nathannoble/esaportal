define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalMessageGrid','hbs!templates/editMessagePopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template,EditMessagePopup) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalMessageGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalMessage",
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalMessageGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalMessage" + "(" + data.messageID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalMessageGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalMessage";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalMessageGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalMessage" + "(" + data.messageID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "messageID",
                                fields: {
                                    messageID: {editable: false, type: "number"},
                                    messageText: {editable: true, type: "string", validation: {required: true}},
                                    messageDate: {editable: true, type: "string", validation: {required: true}}
                                }
                            }
                        },
                        sort: {field: "messageDate", dir: "desc"},
                        requestStart: function (e) {
                            //console.log('PortalMessageGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#messageLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalMessageGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#messageLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalMessageGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalMessageGridView:dataSource:error');
                            kendo.ui.progress(self.$("#messageLoading"), false);
                        }
                    });

            },
            onRender: function () {
                //console.log('PortalMessageGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalMessageGridView:onShow --------");
                var self = this;

                this.grid = this.$("#message").kendoGrid({
                    toolbar: ["create"],
                    //editable: "popup",
                    editable: {
                        mode: "popup",
                        window: {
                            title: "Edit Message"
                        },
                        template: function (e) {
                            var template = EditMessagePopup();
                            return template;
                        }
                    },
                    sortable: true,
                    extra: false,
                    //selectable: "multiple",
                    resizable: true,
                    reorderable: true,
                    groupable: true,
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction k-grid-edit' data-id='" + e.messageID + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                                //fa-edit
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.messageID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "messageID",
                            title: "ID",
                            width: 100
                        }, {
                            field: "messageText",
                            title: "Message Text",
                            width: 500
                        },
                        {
                            field: "messageDate",
                            title: "Message Date",
                            width: 100,
                            format: "{0:M/d/yyyy}"
                        }

                    ],
                    change: function (e) {
                        //console.log('PortalMessageGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('PortalMessageGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('PortalMessageGridView:def:onShow:onEdit');

                        // Disable the messageID editor
                        e.container.find("input[name='messageID']").prop("disabled", true);

                        // Set the focus to the Username field
                        //e.container.find("input[name='Username']").focus();

                        // Make edit/delete invisible
                        //e.container.find(".k-edit-label:first").hide();
                        //e.container.find(".k-edit-field:first").hide();
                        //e.container.find(".k-edit-label:nth-child(4)").hide();
                        //e.container.find(".k-edit-field:nth-child(4)").hide();
                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PortalMessageGridView:getData');

                var grid = this.$("#message").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalMessageGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#message").parent().parent().height();
                this.$("#message").height(parentHeight);

                parentHeight = parentHeight - 65; //78

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalMessageGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalMessageGridView:resize:headerHeight:' + headerHeight);
                this.$("#message").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalMessageGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });