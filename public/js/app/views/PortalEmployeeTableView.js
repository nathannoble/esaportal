define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalEmployeeTable', 'views/PortalEmployeeGridView'],
    function (App, Backbone, Marionette, $, template, PortalEmployeeGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                empRegion1: "#emp-region-1"
            },
            initialize: function () {

                //console.log('PortalEmployeeTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('PortalEmployeeTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalEmployeeTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');

                // For userType 2 check to see if permissions to see this employee
                this.userType = App.model.get('userType');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    if (this.userType === 2) {
                        this.hideLevel1Employees = App.model.get('hideLevel1Employees');
                        if (this.hideLevel1Employees) {
                            console.log('EmployeeProfileLayoutView:onShow:you do not have permissions to see the employees list.');
                            App.model.set('callbackErrorMessage',2);
                            //alert('You do not have permissions to see the employees list.');
                            //history.back();
                        }
                    }
                    this.empRegion1.show(new PortalEmployeeGridView());
                }

            },
            resize: function () {
                //console.log('PortalEmployeeTableView:resize');

                if (this.empRegion1.currentView)
                    this.empRegion1.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalEmployeeTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });