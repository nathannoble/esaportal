define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/companyMessages','models/AdminDataModel'],
    function (App, Backbone, Marionette, _, $, template,AdminDataModel ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            initialize: function () {
                //console.log('CompanyMessagesView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
                //$(window).on('resize', this.onResize);

            },
            onRender: function () {
//                //console.log('CompanyMessagesView:onRender');

                // get rid of that pesky wrapping-div

                //this.$el = this.$el.children();
                this.$el = this.$('.item');//this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- CompanyMessagesView:onShow --------');

                var self = this;

                this.ID = this.model.get('ID');
                this.fileName = this.model.get('fileName');
                this.title = this.model.get('title');
                this.shortText = this.model.get('shortText');
                this.text = this.model.get('text');
                this.text2 = this.model.get('text2');
                this.externalLink = this.model.get('externalLink');
                this.priority = this.model.get('priority');
                this.timeStamp = this.model.get('timeStamp');
                this.category = this.model.get('category');
                this.isFirst = this.model.get('isFirst');

                var text = this.text.trim();
                if (text.length > 200) {
                    text = text.substring(0,199) + " ...";
                    text += '<a style="color:white;font-weight: bold" href="#messages">Read More</a>';
                }
                this.$('#text').html(text);

                if (this.text2 !== null && this.text2 !== "") {
                    this.$('#text2').html(this.text2);
                }

                this.$('#title').html(this.title);
                this.$('#timeStamp').html("Date posted: " + moment(this.timeStamp).clone().format('MM/DD/YYYY'));

                if (self.isFirst && $('.item').length === 1) {
                    $('.item').addClass('active');
                }

                var url = App.config.PortalFiles + "/CompanyMessage/" + this.fileName;
                $.ajax({
                    url: url,
                    type: "GET",
                    crossDomain: true,
                    success: function (response) {
                        console.log('CompanyMessagesView:image found:done');
                        //self.$('#image').attr('src', "../../" + App.config.PortalName + "/CompanyMessage/" + this.fileName );
                        self.$('#image').attr('src', url );

                    },
                    error: function (xhr, status) {
                        console.log('CompanyMessagesView:image not found:fail');

                    }
                });

                if (this.externalLink !== null && this.externalLink !== "") {
                    this.$('#externalLink').attr('href', this.externalLink);
                } else {
                    this.$('#externalLink').hide();
                }

                //console.log('CompanyMessagesView:onShow:model:' + JSON.stringify(this.model));

            },
            onResize: function () {
//                //console.log('CompanyMessagesView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('CompanyMessagesView:resize');

            },
            remove: function () {
                //console.log('-------- CompanyMessagesView:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
