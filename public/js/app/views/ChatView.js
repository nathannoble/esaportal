define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/chat','models/AdminDataModel'],
    function (App, Backbone, Marionette, _, $, template,AdminDataModel ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            //events: {
            //    'click #readMore': 'readMoreClicked'
            //},
            initialize: function () {
                //console.log('ChatView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);

            },
            onRender: function () {
//                //console.log('ChatView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- ChatView:onShow --------');

                this.ID = this.model.get('ID');
                this.fileName = this.model.get('fileName');
                this.title = this.model.get('title');
                this.shortText = this.model.get('shortText');
                this.text = this.model.get('text');
                this.text2 = this.model.get('text2');
                this.priority = this.model.get('priority');
                this.timeStamp = this.model.get('timeStamp');

                this.category = this.model.get('category');

                this.$('#text').html(this.text);
                //this.$('#text2').html(this.text2);
                this.$('#title').html(this.title);
                this.$('#timeStamp').html("Date posted: " + moment(this.timeStamp).clone().format('MM/DD/YYYY'));
                //this.$('#image').attr('src', "../../" + App.config.PortalName + "/CompanyChat/" + this.fileName );
                this.$('#image').attr('src', App.config.PortalFiles + "/CompanyChat/" + this.fileName );

            },
            getCategoryText: function (category) {

                console.log("ChatView:getCategoryText");

                var text = "";
                if (category || category === 0) {
                    $.each(this.categories, function (index, cat) {
                        if (cat.value === category) {
                            text = cat.text;
                            return false;
                        }
                    });
                }
                return text;

            },
            onResize: function () {
//                //console.log('ChatView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            createAutoClosingAlert: function (selector, html, msDuration) {
                // show the alert
                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                var dz = $("#dragDropHandler");

                //wait five seconds and close the alert
                window.setTimeout(function () {
                    dz.slideDown(500);
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, msDuration);
            },
            resize: function () {
//                //console.log('ChatView:resize');

            },
            remove: function () {
                //console.log('-------- ChatView:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
