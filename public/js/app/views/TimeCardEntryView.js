define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/timeCardEntry','models/TimeCardModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            initialize: function () {
                //console.log('TimeCardEntryView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('TimeCardEntryView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- TimeCardEntryView:onShow --------');

                var app = App;
                this.workDate = this.model.get('workDate');
                this.sumTimeWorked = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(this.model.get('sumTimeWorked'));

                if (this.workDate.indexOf("Total") !== -1) {
                    this.$('#date').html("<b>" + this.workDate + "</b>");
                    this.$('#hours').html("<b>" + this.sumTimeWorked + " hours</b>");
                    this.$('.divider').show();
                    this.$('.divider').css("border-width","2px");
                    this.$('.divider').css("border-color","black");
                } else {
                    this.$('#total').hide();
                    this.$('#date').html(this.workDate);
                    this.$('#hours').html(this.sumTimeWorked + " hours");
                }

           },

            onResize: function () {
//                //console.log('TimeCardEntryView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('TimeCardEntryView:resize');

            },
            remove: function () {
                //console.log('-------- TimeCardEntryView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
//                this.$('#leaderRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
