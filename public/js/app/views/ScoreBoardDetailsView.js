define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/scoreboardDetails','views/ScoreBoardDetailCollectionView'],
    function (App, Backbone, Marionette, $, template,ScoreBoardDetailCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template:template,
            regions: {
                scoreBoardList: '#scoreboard-list'
            },
            events: {
                'click #CloseButton': 'onCloseClicked'
            },
            initialize: function(options){
                //console.log('ScoreboardDetailsView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;


                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                this.widget1Value =  App.model.get('widget1Value');
                this.widget2Value =  App.model.get('widget2Value');
                this.widget3Value =  App.model.get('widget3Value');
                this.widget4Value =  App.model.get('widget4Value');

                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;

            },
            onRender: function(){
//                console.log('ScoreboardDetailsView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                //console.log('---------- ScoreboardDetailsView:onShow ----------');

                // Setup the containers
                this.resize();

                var self = this;

                this.$('#widget1Value').html(this.widget1Value);
                this.$('#widget2Value').html(this.widget2Value);
                this.$('#widget2YearLabel').html(this.year + " Target");
                this.$('#widget3Value').html(this.widget3Value);
                this.$('#widget4Value').html(this.widget4Value);

                this.scoreBoardList.show(new ScoreBoardDetailCollectionView());

            },
            onCloseClicked: function(){
//                console.log('ScoreboardDetailsView:onCloseClicked');

                var self = this;
                this.close();
            },
            onResize: function(){
//                console.log('ScoreboardDetailsView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
//                console.log('ScoreboardDetailsView:resize');

            },
            remove: function(){
                //console.log('---------- ScoreboardDetailsView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                $('click').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });