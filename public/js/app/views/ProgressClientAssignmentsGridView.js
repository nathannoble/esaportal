define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressClientAssignmentsGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('ProgressClientAssignmentsGridView:initialize');

                _.bindAll(this);
                
                var self = this;

                this.clientAssignmentGridFilter = App.model.get('clientAssignmentGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.schedulerDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/SelectScheduler",
                                complete: function (e) {
                                    console.log('ProgressClientAssignmentsGridView:schedulerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientAssignmentsGridView:schedulerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "schedulerID",
                                fields: {
                                    schedulerID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    schedulerName: {editable: true, type: "string", validation: {required: true}},
                                    status: {
                                        type: "string",
                                        validation: {required: false},
                                        values: [
                                            {text: '1 - Active', value: '1 - Active'},
                                            {text: '2 - Termed', value: '2 - Termed'},
                                            {text: '3 - No Hire', value: '3 - No Hire'}
                                        ]
                                    },
                                    maxClientLoad: {editable: true, type: "number"},
                                    currentClientLoad: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"}
                        ],
                        requestStart: function (e) {
                            //console.log('BarChartView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingChart"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('BarChartView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#loadingChart"), false);
                        },
                        error: function (e) {
                            //console.log('PortalEmployeeGridView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingChart"), false);
                        }
                        //filter: [{field: "schedulerID", operator: "neq", value: 0}]
                    });
                this.clientDataSource = new kendo.data.DataSource({
                    //autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientTable";
                            },
                            complete: function (e) {
                                console.log('ProgressClientAssignmentsGridView:clientDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'ProgressClientAssignmentsGridView:clientDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                lastName: {editable: true, type: "string", validation: {required: false}},
                                firstName: {editable: true, type: "string", validation: {required: false}},
                                clientCompany: {editable: true, type: "string", validation: {required: false}},
                                schedulerName: {editable: true, type: "string", validation: {required: false}},
                                states: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    serverFiltering: true,
                    //serverSorting: true,
                    requestStart: function () {
                        //console.log('   ProgressClientAssignmentsGridView:requestStart');

                        kendo.ui.progress(self.$("#clientLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    ProgressClientAssignmentsGridView:requestEnd');
                        kendo.ui.progress(self.$("#clientLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientAssignmentsGridView:error');
                        kendo.ui.progress(self.$("#clientLoading"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientAssignmentsGridView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: [{field: "status", operator: "eq", value: '4 - Active'}],
                    sort: [
                        {field: "clientCompany", dir: "asc"}, 
                        {field: "lastName", dir: "asc"},
                        {field: "firstName", dir: "asc"}
                    ]
                });

            },
            onRender: function () {
                //console.log('ProgressClientAssignmentsGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientAssignmentsGridView:onShow --------");
                
                var self = this;
                var app = App;

                this.grid = this.$("#client").kendoGrid({
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    toolbar: [
                        {
                            name: "excel"
                        }],
                    excel: {
                        fileName: "ClientAssignments.xlsx",
                        allPages: true
                    },
                    excelExport: function(e) {
                        var rows = e.workbook.sheets[0].rows;

                        for (var ri = 0; ri < rows.length; ri++) {
                            var row = rows[ri];

                            var ci = 0;
                            var cell;
                            if (ri === 0) {
                                for (ci = 1; ci < row.cells.length; ci++) { //don't format first column
                                    cell = row.cells[ci];
                                    if (cell.value) {
                                        // Use jQuery.fn.text to remove the HTML and get only the text
                                        cell.value = $(cell.value).text();
                                        // Set the alignment
                                        cell.hAlign = "right";
                                    }
                                }
                            } else {
                                for (ci = 3; ci < row.cells.length; ci++) { //don't format first 3 columns
                                    cell = row.cells[ci];
                                    if (cell.value) {
                                        // Use jQuery.fn.text to remove the HTML and get only the text
                                        cell.value = $(cell.value).text();
                                        // Set the alignment
                                        cell.hAlign = "right";
                                    }
                                }
                            }
                        }
                    },
                    filterable: {
                        mode: "row"
                    },
                    columns: [
                        //{
                        //    field: "schedulerName",
                        //    title: "Scheduler",
                        //    width: 150
                        //},
                        {
                            field: "schedulerName",
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='employee-profile btn-interaction' data-id='" + e.schedulerID + "' href='#employeeProfile'>" + e.schedulerName + "</a></div>";
                                return template;
                            },
                            title: "Scheduler",
                            width: 150,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.schedulerFilter
                                }
                            }
                        },
                        {
                            field: "maxClientLoad",
                            template: function (e) {
                                var template = "<div style='text-align: center'>" + e.maxClientLoad + "</div>";
                                return template;
                            },
                            title: "<div style='text-align: center'>Max</div>",
                            width: 60,
                            filterable: false
                        },
                        {
                            field: "actualClientLoad",
                            template: function (e) {
                                var template;
                                if (e.actualClientLoad > e.maxClientLoad) {
                                    template = "<div style='text-align: center;'>" + e.actualClientLoad + " <i class='fa fa-exclamation-triangle' style='color: darkgray'></i></div>";

                                } else {
                                    template = "<div style='text-align: center'>" + e.actualClientLoad + " <i class='fa fa-exclamation-triangle' style='color: transparent'></i></div>";
                                }

                                return template;
                            },
                            title: "<div style='text-align: center'>Actual</div>",
                            width: 60,
                            filterable: false
                        },
                        {
                            field: "clientTerritory",
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.clientTerritory + "</div>";
                                return template;
                            },
                            title: "Clients, Territories",
                            width: 500,
                            filterable: false
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("ProgressClientAssignmentsGridView:def:onShow:dataBound");
                        self.$('.employee-profile').on('click', self.viewEmployeeProfile);

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;

                            var clientAssignmentGridFilter = [];

                            $.each(filters, function (index, value) {
                                clientAssignmentGridFilter.push(value);
                            });

                            //console.log("ProgressClientAssignmentsGridView:def:onShow:dataBound:set clientAssignmentGridFilter:" + JSON.stringify(clientAssignmentGridFilter));
                            app.model.set('clientAssignmentGridFilter', clientAssignmentGridFilter);
                        }
                    },
                    change: function (e) {
                        //console.log('ProgressClientAssignmentsGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('ProgressClientAssignmentsGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('ProgressClientAssignmentsGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        e.container.find("input[name='schedulerID']").prop("disabled", true);

                        // Set the focus to the Username field
                        //e.container.find("input[name='Username']").focus();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('ProgressClientAssignmentsGridView:getData');

                var grid = this.$("#client").data("kendoGrid");

                var self = this;

                this.clientDataSource.fetch(function (data) {

                    var clientRecords = this.data();
                    var gridRecords = [];

                    self.scheduleRecords = [];

                    self.schedulerDataSource.fetch(function () {

                        var schedulerRecords = this.data();

                        $.each(schedulerRecords, function (index, value) {


                            self.scheduleRecords.push({
                                value: value.firstName + " " + value.lastName,
                                text: value.firstName + " " + value.lastName
                            });

                            var record = value;
                            var actualClientLoad = 0;
                            var clientTerritory = "";

                            $.each(clientRecords, function (clientIndex, clientValue) {

                                var schedulerID = clientValue.schedulerID;
                                if (record.schedulerID === schedulerID && record.employeeID !== 0) {
                                    //console.log('ProgressClientAssignmentsGridView:clientRecord:' + JSON.stringify(value));
                                    //console.log('ProgressClientDataAssignmentsGridView:clientDataRecord:' + JSON.stringify(clientValue));

                                   clientTerritory += "<b>" + clientValue.lastName + ", " + clientValue.firstName + "</b> " + clientValue.states + "<br>";
                                   actualClientLoad++;

                                }
                            });

                            var newRecord = {

                                // From SelectSchedulers
                                schedulerID: record.schedulerID,
                                schedulerName: record.schedulerName,
                                lastName:record.lastName,
                                // teamLeaderID: record.teamLeaderID,
                                maxClientLoad:record.maxClientLoad,

                                // Summed from ProgressClient
                                actualClientLoad: actualClientLoad,
                                clientTerritory:clientTerritory

                            };

                            gridRecords.push(newRecord);
                        });

                        var dataSource = new kendo.data.DataSource({
                            schema: {
                                model: {
                                    id: "schedulerID",
                                    fields: {
                                        schedulerID: {editable: false, defaultValue: 0, type: "number"},
                                        schedulerName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        maxClientLoad: {editable: true, type: "number", validation: {required: false}},
                                        actualClientLoad: {editable: false, type: "number", validation: {required: false}},
                                        clientTerritory: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            data: gridRecords,
                            sort: [{field: "lastName", dir: "asc"}]
                        });

                        grid.setDataSource(dataSource);
                        if (self.clientAssignmentGridFilter !== null && self.clientAssignmentGridFilter !== []) {
                            var filters = [];
                            $.each(self.clientAssignmentGridFilter, function (index, value) {
                                filters.push(value);
                            });

                            //console.log("ProgressClientAssignmentsGridView:grid:set datasource:filter" + JSON.stringify(filters));
                            grid.dataSource.filter(filters);
                        } else {
                            grid.dataSource.filter({field: "status", operator: "eq", value: '4 - Active'});
                        }

                        // Resize the grid
                        setTimeout(self.resize, 0);
                    });


                });

            },
            schedulerFilter: function (container) {
                var self = this;

                //console.log('ProgressClientAssignmentGridView:planFilter:' + JSON.stringify(container));

                container.element.kendoDropDownList({
                    //autoBind:false,
                    dataValueField: "value",
                    dataTextField: "text",
                    valuePrimitive: true,
                    dataSource: self.scheduleRecords,
                    optionLabel: "Select Value"
                });

            },
            viewEmployeeProfile: function (e) {
                //console.log('ProgressClientAssignmentsGridView:viewEmployeeProfile:e:' + e.target);

                var employeeId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    employeeId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    employeeId = parseInt(e.target.attributes[2].value, 0);
                }

                App.model.set('selectedEmployeeId', employeeId);


                App.router.navigate('employeeProfile', {trigger: true});

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressClientAssignmentsGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#client').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressClientAssignmentsGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#client').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressClientAssignmentsGridView:resize');

                // this.formatCells();

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#client").parent().parent().height();
                this.$("#client").height(parentHeight);

                parentHeight = parentHeight - 57; //117

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressClientAssignmentsGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressClientAssignmentsGridView:resize:headerHeight:' + headerHeight);
                this.$("#client").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressClientAssignmentsGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#client").data('ui-tooltip'))
                    this.$("#client").tooltip('destroy');

                this.clientAssignmentGridFilter = [];

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });