define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalAdminSocialMediaDataTable', 'views/PortalAdminSocialMediaDataGridView'],
    function (App, Backbone, Marionette, $, template, PortalAdminSocialMediaDataGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                adminDataRegion: "#admin-data-region"
            },
            initialize: function () {

                //console.log('PortalAdminSocialMediaDataTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('PortalAdminSocialMediaDataTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminSocialMediaDataTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.adminDataRegion.show(new PortalAdminSocialMediaDataGridView());
                }



            },
            validateData: function () {
                var self = this;

                if (App.modal.currentView === null || App.modal.currentView === undefined) {

                }
            },
            resize: function () {
                //console.log('PortalAdminSocialMediaDataTableView:resize');

                if (this.adminDataRegion.currentView)
                    this.adminDataRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalAdminSocialMediaDataTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });