define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/doc','models/AdminDataModel'],
    function (App, Backbone, Marionette, _, $, template,AdminDataModel ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            //events: {
            //    'click #readMore': 'readMoreClicked'
            //},
            initialize: function () {
                //console.log('DocView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);

                var self = this;

                self.categories = [
                    {text: 'ESA Forms', value: 9},
                    {text: 'Federal Rights and Postings', value: 10},
                    {text: 'State Rights and Postings', value: 11}
                ];

                // Arizona, California, Colorado, Florida, Georgia, Idaho, Indiana,
                // Kansas, Massachusetts, Michigan, Missouri, Nevada, New Jersey,
                // New York, North Carolina, Ohio, Pennsylvania, South Carolina,
                // Texas
                // Added Wisconsin NN 5/18/23
                self.subCategories = [
                    {text: 'Arizona', value: 0},
                    {text: 'California', value: 1},
                    {text: 'Colorado', value: 2},
                    {text: 'Florida', value: 3},
                    {text: 'Georgia', value: 4},
                    {text: 'Idaho', value: 5},
                    {text: 'Kansas', value: 6},
                    {text: 'Massachusetts', value: 7},
                    {text: 'Michigan', value: 8},
                    {text: 'Missouri', value: 9},
                    {text: 'Nevada', value: 10},
                    {text: 'New Jersey', value: 11},
                    {text: 'New York', value: 12},
                    {text: 'North Carolina', value: 13},
                    {text: 'Ohio', value: 14},
                    {text: 'Pennsylvania', value: 15},
                    {text: 'South Carolina', value: 16},
                    {text: 'Texas', value: 17},
                    {text: 'Wisconsin', value: 18}
                ];


            },
            onRender: function () {
//                //console.log('DocView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- DocView:onShow --------');

                this.ID = this.model.get('ID');
                this.fileName = this.model.get('fileName');
                this.title = this.model.get('title');
                this.shortText = this.model.get('shortText');
                this.text = this.model.get('text');
                this.text2 = this.model.get('text2');

                this.category = this.model.get('category');
                this.categoryText = this.getCategoryText(this.category).replace(/ /g,"");

                this.subCategory = this.model.get('subCategory');
                if (this.category === 11) {
                    this.subCategoryText = this.subCategories[this.subCategory] ? this.subCategories[this.subCategory].text : "";
                }

                var title = this.title.trim();
                if (title.length > 20) {
                    title = title.substring(0,19) + " ...";
                }

                this.$('#fileView').html('<i class="fa fa-file-o"></i>' + title);
                this.$('#fileView').attr('title',this.fileName);
                this.$('#fileView').attr('download',this.fileName);
                //this.$('#fileName').attr('href', "../../" + App.config.PortalName + this.categoryText + "/" + this.fileName );
                this.$('#fileView').attr('href', App.config.PortalFiles + "/" + this.categoryText + "/" + this.fileName );

            },
            getCategoryText: function (category) {

                console.log("DocView:getCategoryText");

                var text = "";
                if (category || category === 0) {
                    $.each(this.categories, function (index, cat) {
                        if (cat.value === category) {
                            text = cat.text;
                            return false;
                        }
                    });
                }
                return text;

            },
            onResize: function () {
//                //console.log('DocView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            createAutoClosingAlert: function (selector, html, msDuration) {
                // show the alert
                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                var dz = $("#dragDropHandler");

                //this.isUploadingDoc = false;
                //App.model.set('uploadingSpreadCSV', false);

                //wait five seconds and close the alert
                window.setTimeout(function () {
                    dz.slideDown(500);
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, msDuration);
            },
            resize: function () {
//                //console.log('DocView:resize');

            },
            remove: function () {
                //console.log('-------- DocView:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
