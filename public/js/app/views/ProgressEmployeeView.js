define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressEmployee', 'jquery.cookie',
        'kendo/kendo.data', 'kendo/kendo.combobox', 'kendo/kendo.datepicker', 'jquery.cookie'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #SaveButton': 'saveButtonClicked',
                'click #UserButton': 'userButtonClicked'
            },
            initialize: function (options) {
                //console.log('ProgressEmployeeView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Edit"};
                }

                this.userId = App.model.get('userId');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.changedToTermed = false;

                    this.userType = parseInt(App.model.get('userType'), 0);
                    this.userKey = parseInt(App.model.get('userKey'), 0);
                    if (this.options.type === "New Employee") {
                        this.employeeID = null;
                    } else {
                        this.employeeID = parseInt(App.model.get('selectedEmployeeId'), 0); //2189;
                    }
                    // Subscribe to browser events
                    $(window).on("resize", this.onResize);

                    this.employeeDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                    complete: function (e) {
                                        console.log('ProgressEmployeeView:employeeDataSourcee:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:employeeDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ProgressEmployeeGridView:update');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressEmployeeView:employeeDataSourcee:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:employeeDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('ProgressEmployeeGridView:create');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployee";
                                        //return "http://localhost:64859/odata/ProgressEmployee";
                                    },
                                    complete: function (e) {

                                        if (e.responseJSON.value[0]) {
                                            var newID = e.responseJSON.value[0].employeeID;
                                            //console.log('ProgressEmployeeGridView:create:complete:' + newID);
                                            App.model.set('selectedEmployeeId', newID);
                                        }
                                        App.router.navigate('employeeProfile', {trigger: true});

                                    },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:employeeDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('ProgressEmployeeGridView:destroy');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressEmployeeView:employeeDataSource:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:employeeDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "employeeID",
                                    fields: {
                                        employeeID: {editable: false, type: "number"},

                                        eeTypeTitle: {type: "string", validation: {required: false}},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        teamName: {editable: true, type: "string", validation: {required: false}},

                                        hidePlans: {editable: true, type: "boolean"},
                                        hideStatsTimecard: {editable: true, type: "boolean"},
                                        hideLevel1Employees: {editable: true, type: "boolean"},
                                        hideClients: {editable: true, type: "boolean"},
                                        hideCompanies: {editable: true, type: "boolean"},

                                        title: {editable: true, type: "string", validation: {required: false}},
                                        emailAddress: {editable: true, type: "string", validation: {required: true}},
                                        alternateEmail: {editable: true, type: "string", validation: {required: false}},
                                        esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                        esaExtension: {editable: true, type: "string", validation: {required: false}},
                                        esaTollFree: {editable: true, type: "string", validation: {required: false}},
                                        nonEsaBusinessPhone: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        workFax: {editable: true, type: "string", validation: {required: false}},
                                        mobilePhone: {editable: true, type: "string", validation: {required: false}},
                                        homePhone: {editable: true, type: "string", validation: {required: false}},
                                        emergencyPhone: {editable: true, type: "string", validation: {required: false}},
                                        homeStreet: {editable: true, type: "string", validation: {required: false}},
                                        homeCity: {editable: true, type: "string", validation: {required: false}},
                                        homeState: {editable: true, type: "string", validation: {required: false}},
                                        homeZip: {editable: true, type: "string", validation: {required: false}},
                                        voicemailPasscode: {type: "string", validation: {required: false}},
                                        xxx_esaUsername: {type: "string", validation: {required: false}},
                                        xxx_esaPassword: {type: "string", validation: {required: false}},
                                        additionalInfo: {type: "string", validation: {required: false}},
                                        status: {type: "string", validation: {required: false}},
                                        employeeType: {type: "string", validation: {required: false}},
                                        photoFile: {type: "string", validation: {required: false}},

                                        isTeamLeader: {editable: true, type: "boolean"},
                                        isEligibleSupport: {editable: true, type: "boolean"},
                                        timerManual: {editable: true, type: "boolean"},

                                        userKey: {editable: true, type: "number"},
                                        eeTypeID: {editable: true, type: "number"},
                                        teamLeaderID: {editable: true, type: "number"},
                                        timeZoneID: {editable: true, type: "number"},
                                        eeTimeZone: {editable: true, type: "number"},
                                        eeTimeZoneOffset: {editable: true, type: "number"},
                                        maxClientLoad: {editable: true, type: "number"},
                                        clientsLost: {editable: true, type: "number"},
                                        currentClientLoad: {editable: true, type: "number"},
                                        timerStatus: {editable: true, type: "number"},
                                        salary: {editable: true, type: "number"},

                                        hireDate: {editable: true, type: "date", validation: {required: false}},
                                        termDate: {editable: true, type: "date", validation: {required: false}},
                                        birthDate: {editable: true, type: "date", validation: {required: false}},

                                        appPasscode: {type: "string", validation: {required: false}}

                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                //console.log('   ProgressEmployeeView:requestStart');

                                kendo.ui.progress(self.$("#loadingEmployeeInfo"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressEmployeeView:requestEnd');
                                kendo.ui.progress(self.$("#loadingEmployeeInfo"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressEmployeeView:error');
                                kendo.ui.progress(self.$("#loadingEmployeeInfo"), false);


                            },
                            change: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;
                            },
                            filter: [{field: "employeeID", operator: "eq", value: self.employeeID}]

                        });

                    this.teamLeaderDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                    complete: function (e) {
                                        console.log('ProgressEmployeeView:teamLeaderDataSourcee:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:teamLeaderDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "employeeID",
                                    fields: {
                                        employeeID: {editable: false, type: "number"},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        status: {type: "string", validation: {required: false}},
                                        isTeamLeader: {editable: true, type: "boolean"},
                                        teamLeaderID: {editable: false, type: "number"}
                                    }
                                }
                            },
                            sort: [
                                {field: "lastName", dir: "asc"},
                                {field: "firstName", dir: "asc"}
                            ],
                            filter: {field: "isTeamLeader", operator: "eq", value: true},
                            serverFiltering: true,
                            serverSorting: true
                        });

                    this.clientDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressClientTable",
                                    complete: function (e) {
                                        console.log('ProgressEmployeeView:clientDataSourcee:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:clienteDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientID",
                                    fields: {
                                        clientID: {editable: false, defaultValue: 0, type: "number"},
                                        schedulerID: {defaultValue: 0, type: "number"},
                                        status: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                //console.log('   ProgressEmployeeView:requestStart');

                                kendo.ui.progress(self.$("#loadingClientInfo"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressEmployeeView:requestEnd');
                                //kendo.ui.progress(self.$("#loadingClientInfo"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressEmployeeView:error');
                                //kendo.ui.progress(self.$("#loadingClientInfo"), false);

                            },
                            change: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                            },
                            //<cfquery name="getClients" datasource="#APPLICATION.dataSource#">
                            //    SELECT		count(clientID) AS clientCount <!---status, schedulerID, lastName, firstName, states--->
                            //    FROM			tblProgressClients
                            //    WHERE		schedulerID <> 0 AND status LIKE '4%' AND schedulerID = <cfqueryparam value="#URL.employeeID#" cfsqltype="cf_sql_integer">
                            //</cfquery>
                            filter: [
                                {field: "schedulerID", operator: "eq", value: self.employeeID},
                                {field: "schedulerID", operator: "neq", value: 0},
                                {field: "status", operator: "eq", value: '4 - Active'}
                            ]

                        });

                    this.userDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/PortalUserTable",
                                    complete: function (e) {
                                        console.log('ProgressEmployeeView:userDataSource:read:complete');
                                     },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:userDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ProgressEmployeeView:update');
                                        return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressEmployeeView:userDataSource:update:complete');
                                     },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:userDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('ProgressEmployeeView:create');
                                        return App.config.DataServiceURL + "/odata/PortalUserTable";
                                    },
                                    complete: function (e) {

                                        if (e.responseJSON.value[0]) {
                                            var userKey = e.responseJSON.value[0].userKey;
                                            //console.log('ProgressEmployeeView:userDataSource:create:complete:userKey:' + userKey);
                                            self.updateEmployeeRecordWithUserKey(userKey);

                                        } else {
                                            //console.log('ProgressEmployeeView:userDataSource:create:complete:no record');
                                        }
                                    },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:userDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('ProgressEmployeeView:destroy');
                                        return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressEmployeeView:userDataSource:destroy:complete');
                                     },
                                    error: function (e) {
                                        var message = 'ProgressEmployeeView:userDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "userKey",
                                    fields: {
                                        userKey: {editable: false, type: "number"},
                                        userID: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: true},
                                            defaultValue: function () {
                                                return app.Utilities.numberUtilities.createGUID();
                                            }
                                        },
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        userLogin: {editable: true, type: "string", validation: {required: false}},
                                        userPassword: {editable: true, type: "string", validation: {required: false}},
                                        lastLogin: {editable: false, type: "date", validation: {required: false}},
                                        lastLogout: {editable: false, type: "date", validation: {required: false}},
                                        loginCount: {editable: false, type: "number"},
                                        userType: {
                                            type: "number",
                                            validation: {required: true},
                                            defaultValue: 1
                                        },
                                        inactive: {
                                            editable: true,
                                            type: "boolean"
                                        },
                                        lastPasswordChange: {editable: false, type: "date"}
                                    }
                                }
                            },
                            serverFiltering: true,
                            sort: [
                                {field: "lastName", dir: "asc"},
                                {field: "firstName", dir: "asc"}
                            ],
                            requestStart: function (e) {
                                //console.log('ProgressEmployeeView:dataSource:request start:');
                                kendo.ui.progress(self.$("#loadingEmployeeInfo"), true);
                            },
                            requestEnd: function (e) {
                                //console.log('ProgressEmployeeView:dataSource:request end:');
                                //var data = this.data();
                                kendo.ui.progress(self.$("#loadingEmployeeInfo"), false);
                            },
                            error: function (e) {
                                //console.log('ProgressEmployeeView:dataSource:error');
                                kendo.ui.progress(self.$("#loadingEmployeeInfo"), false);
                            }

                        });


                }
            },
            onRender: function () {
                //console.log('ProgressEmployeeView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressEmployeeView:onShow --------");
                var self = this;

                if (this.options.type === "New Employee") {
                    self.$('#SaveButton').html("Add New Employee");
                    self.$('#UserButton').addClass('disabled');
                } else {
                    self.$('#SaveButton').html("Update Employee");
                    self.$('#UserButton').removeClass('disabled');
                }

                self.$('#UserButton').hide();
                self.$("#teamNameGroup").hide();
                self.$("#sideBarGroup").hide();

                this.employeeDataSourceMax = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('ProgressEmployeeView:employeeDataSourceMax:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressEmployeeView:employeeDataSourceMax:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    userKey: {editable: true, type: "number"}
                                }
                            }
                        },
                        //serverFiltering: true,
                        //serverSorting: true,
                        aggregate: [
                            {field: "userKey", aggregate: "max"}
                        ]
                    });

                self.getData();

                self.$('#SaveButton').addClass('disabled');

            },
            getData: function () {

                var self = this;
                var app = App;

                self.$(".k-textbox").kendoMaskedTextBox();
                self.$(".k-numerictextbox").kendoNumericTextBox();

                self.employeeDataSource.fetch(function () {

                    var data = this.data();
                    var count = data.length;
                    var empRecord;

                    //console.log('ProgressEmployeeView:dataSource:fetch:count:' + count);
                    if (count === 0 || self.options.type === "New Employee") {

                        var guid = app.Utilities.numberUtilities.createGUID();

                        // Create new record
                        empRecord = {
                            userID: guid,
                            userKey: 0,
                            firstName: "",
                            lastName: "",
                            status: '1 - Active',
                            hireDate: null,
                            termDate: null,
                            birthDate: null,
                            timerManual: false,
                            timeZoneID: 1,
                            timerStatus: 1,
                            isTeamLeader: false,
                            hidePlans: false,
                            hideStatsTimecard: false,
                            hideLevel1Employees: false,
                            hideClients: false,
                            hideCompanies: false,
                            isEligibleSupport: false,
                            currentClientLoad: 0,
                            clientsLost:0,
                            maxClientLoad: 0,
                            salary: 0
                        };

                        self.newRecord = empRecord;

                        //console.log('ProgressEmployeeView:dataSource:new employee record:' + JSON.stringify(empRecord));

                    } else {

                        empRecord = data[0];
                        //console.log('ProgressEmployeeView:dataSource:record:' + JSON.stringify(empRecord));
                    }
                    self.userDataSource.filter({field: "userKey", operator: "eq", value: empRecord.userKey});
                    self.userDataSource.fetch(function () {

                        var userData = this.data();

                        if (userData.length > 0) {
                            self.selectedUserType =  userData[0].userType;
                        }

                        // populateDialog after emp userType is known
                        self.populateDialog(empRecord);

                        self.teamLeaderDataSource.fetch(function () {

                        var tlRecords = self.teamLeaderDataSource.data();
                        var modifiedRecords = [];
                        var noneRecord = {
                            employeeID: 0,
                            name: '--- NONE ---'
                        };
                        modifiedRecords.push(noneRecord);

                        $.each(tlRecords, function (index, value) {

                            var record = value;
                            var newRecord = {
                                employeeID: record.employeeID,
                                name: record.firstName + " " + record.lastName
                            };
                            modifiedRecords.push(newRecord);

                        });

                        self.$("#teamLeaderID").kendoDropDownList({
                            dataTextField: "name",
                            dataValueField: "employeeID",
                            dataSource: modifiedRecords,
                            value: empRecord.teamLeaderID

                        });

                        self.clientDataSource.fetch(function () {

                            var data = this.data();
                            var clientCount = data.length;

                            if (self.$("#currentClientLoad").data("kendoMaskedTextBox") !== undefined && self.$("#currentClientLoad").data("kendoMaskedTextBox") !== null) {
                                self.$("#currentClientLoad").data("kendoMaskedTextBox").value(clientCount);
                            }

                            // userDataSource
                            var count = userData.length;

                            console.log('ProgressEmployeeView:dataSource:user record:' + JSON.stringify(userData));

                            self.userTypes = [
                                {text: 'Pre-Hire', value: 0},
                                {text: 'Employee', value: 1},
                                {text: 'Manager', value: 2},
                                {text: 'Executive', value: 3}
                            ];

                            self.userStatuses = [
                                {text: 'Active', value: "false"},
                                {text: 'Inactive', value: "true"}
                            ];

                            if (count > 0) {

                                var userRecord = userData[0];
                                // associated user record exists
                                self.$('#UserButton').html("Update User");
                                if (self.$("#userKey").data("kendoMaskedTextBox") !== undefined && self.$("#userKey").data("kendoMaskedTextBox") !== null) {
                                    self.$("#userKey").data("kendoMaskedTextBox").value(userRecord.userKey);
                                    self.$("#userFirstName").data("kendoMaskedTextBox").value(userRecord.firstName);
                                    self.$("#userLastName").data("kendoMaskedTextBox").value(userRecord.lastName);
                                    self.$("#xxx_esaUsername").data("kendoMaskedTextBox").value(userRecord.userLogin);
                                    self.$("#xxx_esaPassword").data("kendoMaskedTextBox").value(userRecord.userPassword);

                                    self.$("#userType").kendoDropDownList({
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        dataSource: self.userTypes,
                                        value: userRecord.userType
                                    });

                                    console.log('ProgressEmployeeView:userRecord:userType:' + self.selectedUserType);

                                    self.$("#userStatus").kendoDropDownList({
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        dataSource: self.userStatuses,
                                        //valuePrimitive: true,
                                        value: userRecord.inactive
                                    });

                                    //self.$("#userStatus").val(userRecord.inactive);

                                    self.$("#lastLogin").data("kendoMaskedTextBox").value(userRecord.lastLogin);
                                    self.$("#loginCount").data("kendoMaskedTextBox").value(userRecord.loginCount);
                                    self.$("#lastPasswordChange").data("kendoMaskedTextBox").value(userRecord.lastPasswordChange);

                                    self.$('#userKey').prop("disabled", true).addClass("k-state-disabled");
                                    self.$('#userFirstName').prop("disabled", true).addClass("k-state-disabled");
                                    self.$('#userLastName').prop("disabled", true).addClass("k-state-disabled");
                                    self.$('#xxx_esaUsername').prop("disabled", true).addClass("k-state-disabled");
                                    self.$('#xxx_esaPassword').prop("disabled", true).addClass("k-state-disabled");

                                    var userType = self.$("#userType").data("kendoDropDownList");
                                    userType.enable(false);

                                    var userStatus = self.$("#userStatus").data("kendoDropDownList");
                                    userStatus.enable(false);

                                    self.$('#lastLogin').prop("disabled", true).addClass("k-state-disabled");
                                    self.$('#loginCount').prop("disabled", true).addClass("k-state-disabled");
                                    self.$('#lastPasswordChange').prop("disabled", true).addClass("k-state-disabled");
                                }

                            } else {
                                // No associated user record exists
                                self.$('#UserButton').html("Create User");
                                self.$('#UserButton').show();
                                if (self.$("#userKey").data("kendoMaskedTextBox") !== undefined && self.$("#userKey").data("kendoMaskedTextBox") !== null) {
                                    self.$("#userKey").data("kendoMaskedTextBox").value(self.$("#employeeUserKey").val());

                                    self.$("#userFirstName").data("kendoMaskedTextBox").value("N/A");
                                    self.$("#userFirstName").data("kendoMaskedTextBox").value("N/A");
                                    self.$("#userLastName").data("kendoMaskedTextBox").value("N/A");
                                    self.$("#xxx_esaUsername").data("kendoMaskedTextBox").value("N/A");
                                    self.$("#xxx_esaPassword").data("kendoMaskedTextBox").value("N/A");

                                    self.$("#userType").kendoDropDownList({
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        dataSource: self.userTypes
                                    });

                                    self.$("#userStatus").kendoDropDownList({
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        dataSource: self.userStatuses
                                    });

                                    self.$("#lastLogin").data("kendoMaskedTextBox").value("N/A");
                                    self.$("#loginCount").data("kendoMaskedTextBox").value("N/A");
                                    self.$('#lastPasswordChange').data("kendoMaskedTextBox").value("N/A");
                                }
                            }

                            // Don't allow any edits to happen until the data is completely loaded into the form
                            self.$('#SaveButton').removeClass('disabled');
                            });
                        });
                    });
                });

            },
            populateDialog: function (empRecord) {
                console.log('ProgressEmployeeView:populateDialog:empRecord:' + JSON.stringify(empRecord));

                var self = this;

                // Dates
                $("#hireDate").kendoDatePicker({
                    value: empRecord.hireDate
                });
                $("#termDate").kendoDatePicker({
                    value: empRecord.termDate
                });
                $("#birthDate").kendoDatePicker({
                    value: empRecord.birthDate
                });

                $(".k-textbox").kendoMaskedTextBox();
                $(".k-numerictextbox").kendoNumericTextBox();

                if ($("#employeeID").data("kendoMaskedTextBox") !== undefined && self.$("#employeeID").data("kendoMaskedTextBox") !== null) {
                    $("#employeeID").data("kendoMaskedTextBox").value(empRecord.employeeID);
                    $("#employeeUserKey").data("kendoMaskedTextBox").value(empRecord.userKey);
                    $("#firstName").data("kendoMaskedTextBox").value(empRecord.firstName);
                    $("#lastName").data("kendoMaskedTextBox").value(empRecord.lastName);
                    if (empRecord.isTeamLeader) { //|| parseInt(self.$("#userType").val(), 0) === 3
                        $("#teamNameGroup").show();
                        $("#teamName").data("kendoMaskedTextBox").value(empRecord.teamName);
                    } else {
                        $("#teamNameGroup").hide();
                    }

                    $("#title").data("kendoMaskedTextBox").value(empRecord.title);
                    $("#emailAddressField").data("kendoMaskedTextBox").value(empRecord.emailAddress);
                    $("#alternateEmail").data("kendoMaskedTextBox").value(empRecord.alternateEmail);

                    $("#esaLocalPhoneField").data("kendoMaskedTextBox").value(empRecord.esaLocalPhone);
                    //$("#nonEsaBusinessPhone").data("kendoMaskedTextBox").value(empRecord.nonEsaBusinessPhone);
                    $("#mobilePhone").data("kendoMaskedTextBox").value(empRecord.mobilePhone);
                    $("#homePhone").data("kendoMaskedTextBox").value(empRecord.homePhone);

                    $("#emergencyPhone").data("kendoMaskedTextBox").value(empRecord.emergencyPhone);
                    $("#homeStreet").data("kendoMaskedTextBox").value(empRecord.homeStreet);
                    $("#homeCity").data("kendoMaskedTextBox").value(empRecord.homeCity);
                    $("#homeState").data("kendoMaskedTextBox").value(empRecord.homeState);
                    $("#homeZip").data("kendoMaskedTextBox").value(empRecord.homeZip);

                    $("#voicemailPasscode").data("kendoMaskedTextBox").value(empRecord.voicemailPasscode);

                    if (empRecord.appPasscode === null || empRecord.appPasscode === undefined )
                        empRecord.appPasscode = "";
                    $("#appPasscode").data("kendoMaskedTextBox").value(empRecord.appPasscode);

                    $("#additionalInfo").data("kendoMaskedTextBox").value(empRecord.additionalInfo);

                    $("#maxClientLoad").data("kendoMaskedTextBox").value(empRecord.maxClientLoad);

                    $("#clientsLost").data("kendoMaskedTextBox").value(empRecord.clientsLost);
                    //$("#currentClientLoad").data("kendoMaskedTextBox").value(empRecord.currentClientLoad);
                    //$("#salary").data("kendoMaskedTextBox").value(empRecord.salary);

                    $("#esaExtension").data("kendoMaskedTextBox").value(empRecord.esaExtension);
                    $("#esaTollFreeField").data("kendoMaskedTextBox").value(empRecord.esaTollFree);

                    var yesNoList = [
                        {text: 'Yes', value: "true"},
                        {text: 'No', value: "false"}
                    ];

                    if (self.userType === 3 && self.selectedUserType === 2) {
                        $("#sideBarGroup").show();
                        $("#hidePlans").kendoDropDownList({
                            dataValueField: "value",
                            dataTextField: "text",
                            dataSource: yesNoList,
                            value: empRecord.hidePlans.toString()
                        });
                        $("#hideStatsTimecard").kendoDropDownList({
                            dataValueField: "value",
                            dataTextField: "text",
                            dataSource: yesNoList,
                            value: empRecord.hideStatsTimecard.toString()
                        });
                        $("#hideLevel1Employees").kendoDropDownList({
                            dataValueField: "value",
                            dataTextField: "text",
                            dataSource: yesNoList,
                            value: empRecord.hideLevel1Employees.toString()
                        });
                        $("#hideClients").kendoDropDownList({
                            dataValueField: "value",
                            dataTextField: "text",
                            dataSource: yesNoList,
                            value: empRecord.hideClients.toString()
                        });
                        $("#hideCompanies").kendoDropDownList({
                            dataValueField: "value",
                            dataTextField: "text",
                            dataSource: yesNoList,
                            value: empRecord.hideCompanies.toString()
                        });
                    } else {
                        $("#sideBarGroup").hide();
                    }

                    $("#timerManual").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: empRecord.timerManual.toString()
                    });

                    $("#isTeamLeader").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: empRecord.isTeamLeader.toString()
                    });

                    $("#isEligibleSupport").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: empRecord.isEligibleSupport.toString()
                    });

                    var timeZoneList = [
                        {text: 'US/Pacific', value: 1},
                        {text: 'US/Mountain', value: 2},
                        {text: 'US/Central', value: 3},
                        {text: 'US/Eastern', value: 4},
                        {text: 'US/Atlantic', value: 5}
                    ];

                    $("#timeZoneID").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: timeZoneList,
                        value: empRecord.timeZoneID
                    });

                    var types = [
                        {text: 'Admin', value: 1},
                        {text: 'Scheduler', value: 2}
                    ];

                    $("#eeTypeID").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: types,
                        value: empRecord.eeTypeID
                    });

                    var timerStatuses = [
                        {text: '1 - Normal', value: 1},
                        {text: '2 - Support', value: 2},
                        {text: '3 - Management', value: 3},
                        {text: '4 - Admin', value: 4}
                    ];

                    $("#timerStatus").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: timerStatuses,
                        value: empRecord.timerStatus
                    });

                    var statuses = [
                        {value: '1 - Active'},
                        {value: '2 - Termed'},
                        {value: '3 - No Hire'}
                    ];

                    $("#status").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: statuses,
                        value: empRecord.status,
                        change: self.onStatusChanged
                    });

                    var typesOfService = [
                        {value: '1 - Scheduling'},
                        {value: '2 - Admin'},
                        {value: '3 - Sched + Admin'},
                        {value: '4 - Activity Net'},
                        {value: '5 - Activity Net + Sched'}
                    ];

                    $("#typeOfService").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: typesOfService,
                        value: empRecord.typeOfService
                    });
                }
            },
            onStatusChanged: function (e) {
                var status = e.sender._selectedValue;
                var self = this;
                console.log('ProgressEmployeeView:onStatusChanged:status:' + status);

                if (status === '2 - Termed') {
                    self.changedToTermed = true;
                    $("#teamLeaderID").data("kendoDropDownList").value(0);
                }

            },
            userButtonClicked: function (e) {
                //console.log('ProgressEmployeeView:userButtonClicked');

                var self = this;
                var app = App;

                var name = self.$('#UserButton').html();

                if (name === "Create User") {

                    self.userDataSource.fetch(function () {

                        // Create new record
                        var newRecord = {
                            //userID: guid,
                            //userKey: self.$("#employeeUserKey").val(),
                            firstName: self.$("#userFirstName").val(),
                            lastName: self.$("#userLastName").val(),
                            userLogin: self.$("#xxx_esaUsername").val(),
                            userPassword: self.$("#xxx_esaPassword").val(),
                            userType: parseInt(self.$("#userType").val(), 0),
                            inactive: (self.$("#userStatus").val() === true),
                            lastLogin: null,
                            lastLogout: null,
                            loginCount: 0,
                            lastPasswordChange: null
                        };

                        self.userDataSource.add(newRecord);
                        self.userDataSource.sync();
                        self.$('#UserButton').hide();

                    });
                } else {

                    self.userDataSource.fetch(function () {

                        var data = this.data();
                        var count = data.length;

                        if (count > 0) {

                            var userRecord = data[0];

                            // Update user
                            userRecord.set("firstName", self.$("#userFirstName").val());
                            userRecord.set("lastName", self.$("#userLastName").val());
                            userRecord.set("userLogin", self.$("#xxx_esaUsername").val());
                            userRecord.set("userPassword", self.$("#xxx_esaPassword").val());
                            //userRecord.set("userType", self.$("#userType").val());
                            userRecord.set("inactive", self.$("#userStatus").val());
                            self.userDataSource.sync();
                        }
                    });
                }

            },
            updateEmployeeRecordWithUserKey: function (userKey) {
                //console.log('ProgressEmployeeView:updateEmployeeRecordWithUserKey');
                this.employeeDataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;
                    if (count > 0) {
                        var empRecord = data[0];
                        empRecord.set("userKey", userKey);
                        this.sync();
                        App.router.navigate('employeeProfile', {trigger: true});
                    }
                });

            },
            saveButtonClicked: function (e) {
                //console.log('ProgressEmployeeView:saveButtonClicked');

                var self = this;

                var record;

                var birthDate = self.$("#birthDate").val();
                if (birthDate !== null && birthDate !== "") {
                    birthDate = new Date(birthDate).toISOString();
                } else {
                    birthDate = null;
                }

                var hireDate = self.$("#hireDate").val();
                if (hireDate !== null && hireDate !== "") {
                    hireDate = new Date(hireDate).toISOString();
                } else {
                    hireDate = null;
                }

                var termDate = self.$("#termDate").val();
                if (termDate !== null && termDate !== "") {
                    termDate = new Date(termDate).toISOString();
                } else {
                    termDate = null;
                }
                //console.log('ProgressEmployeeView:saveButtonClicked:termDate:' + termDate);

                if (this.options.type === "New Employee") {

                    record = empRecord = {
                        division: null,
                        eeTimeZone: null,
                        eeTimeZoneOffset: 0,
                        employeeType: "x_ESASCHED",
                        photoFile: "x_ESASCHED",

                        //userKey: self.maxUserKey + 3,
                        firstName: self.$("#firstName").val(),
                        lastName: self.$("#lastName").val(),
                        teamName: self.$("#teamName").val(),

                        eeTypeID: self.$("#eeTypeID").val(),
                        status: self.$("#status").val(),
                        timerStatus: self.$("#timerStatus").val(),
                        timerManual: (self.$("#timerManual").val() === "true"),
                        isEligibleSupport: (self.$("#isEligibleSupport").val() === "true"),
                        isTeamLeader: (self.$("#isTeamLeader").val() === "true"),

                        hidePlans: (self.$("#hidePlans").val() === "true"),
                        hideStatsTimecard: (self.$("#hideStatsTimecard").val() === "true"),
                        hideLevel1Employees: (self.$("#hideLevel1Employees").val() === "true"),
                        hideClients: (self.$("#hideClients").val() === "true"),
                        hideCompanies: (self.$("#hideCompanies").val() === "true"),

                        timeZoneID: self.$("#timeZoneID").val(),

                        emailAddress: self.$("#emailAddressField").val(),
                        alternateEmail: self.$("#alternateEmail").val(),
                        homeStreet: self.$("#homeStreet").val(),
                        homeCity: self.$("#homeCity").val(),
                        homeState: self.$("#homeState").val(),
                        homeZip: self.$("#homeZip").val(),
                        mobilePhone: self.$("#mobilePhone").val(),
                        homePhone: self.$("#homePhone").val(),
                        //workFax: self.$("#workFax").val(),
                        emergencyPhone: self.$("#emergencyPhone").val(),
                        teamLeaderID: self.$("#teamLeaderID").val(),
                        title: self.$("#title").val(),

                        birthDate: birthDate,
                        hireDate: hireDate,
                        termDate: termDate,

                        additionalInfo: self.$("#additionalInfo").val(),
                        voicemailPasscode: self.$("#voicemailPasscode").val(),
                        appPasscode: self.$("#appPasscode").val(),
                        maxClientLoad: parseInt(self.$("#maxClientLoad").val(), 0),
                        clientsLost: parseInt(self.$("#clientsLost").val(), 0),
                        currentClientLoad: parseInt(self.$("#currentClientLoad").val(), 0),
                        //salary:parseFloat(self.$("#salary").val()),
                        esaLocalPhone: self.$("#esaLocalPhoneField").val(),
                        esaExtension: self.$("#esaExtension").val(),
                        esaTollFree: self.$("#esaTollFreeField").val(),
                        //nonEsaBusinessPhone:self.$("#nonEsaBusinessPhone").val(),
                        xxx_esaUsername: self.$("#xxx_esaUsername").val(),
                        xxx_esaPassword: self.$("#xxx_esaPassword").val()
                    };

                    self.employeeDataSource.add(record);
                    self.employeeDataSource.sync();

                    App.model.set('teamLeaderRecords', []);


                } else {
                    this.employeeDataSource.fetch(function () {
                        var data = this.data();
                        var count = data.length;

                        if (count > 0) {

                            record = self.employeeDataSource.at(0);

                            record.set("firstName", self.$("#firstName").val());
                            record.set("lastName", self.$("#lastName").val());
                            record.set("teamName", self.$("#teamName").val());

                            record.set("eeTypeID", self.$("#eeTypeID").val());
                            record.set("status", self.$("#status").val());
                            record.set("timerStatus", self.$("#timerStatus").val());
                            record.set("emailAddress", self.$("#emailAddressField").val());
                            record.set("alternateEmail", self.$("#alternateEmail").val());
                            record.set("homeStreet", self.$("#homeStreet").val());
                            record.set("homeCity", self.$("#homeCity").val());
                            record.set("homeState", self.$("#homeState").val());
                            record.set("homeZip", self.$("#homeZip").val());
                            record.set("mobilePhone", self.$("#mobilePhone").val());
                            record.set("homePhone", self.$("#homePhone").val());
                            //record.set("workFax", self.$("#workFax").val());
                            record.set("emergencyPhone", self.$("#emergencyPhone").val());
                            record.set("teamLeaderID", self.$("#teamLeaderID").val());
                            record.set("title", self.$("#title").val());
                            record.set("birthDate", self.$("#birthDate").val());
                            record.set("hireDate", self.$("#hireDate").val());
                            record.set("termDate", self.$("#termDate").val());
                            record.set("additionalInfo", self.$("#additionalInfo").val());
                            record.set("voicemailPasscode", self.$("#voicemailPasscode").val());
                            record.set("appPasscode", self.$("#appPasscode").val());
                            record.set("maxClientLoad", self.$("#maxClientLoad").val());
                            record.set("clientsLost", self.$("#clientsLost").val());
                            record.set("currentClientLoad", self.$("#currentClientLoad").val());
                            //record.set("salary", self.$("#salary").val());
                            record.set("esaLocalPhone", self.$("#esaLocalPhoneField").val());
                            record.set("esaExtension", self.$("#esaExtension").val());
                            record.set("esaTollFree", self.$("#esaTollFreeField").val());
                            //record.set("nonEsaBusinessPhone", self.$("#nonEsaBusinessPhone").val());
                            record.set("xxx_esaUsername", self.$("#xxx_esaUsername").val());
                            record.set("xxx_esaPassword", self.$("#xxx_esaPassword").val());

                            record.set("timeZoneID", self.$("#timeZoneID").val());
                            record.set("timerManual", (self.$("#timerManual").val() === "true"));
                            record.set("isEligibleSupport", (self.$("#isEligibleSupport").val() === "true"));
                            record.set("isTeamLeader", (self.$("#isTeamLeader").val() === "true"));

                            record.set("hidePlans", (self.$("#hidePlans").val() === "true"));
                            record.set("hideStatsTimecard", (self.$("#hideStatsTimecard").val() === "true"));
                            record.set("hideLevel1Employees", (self.$("#hideLevel1Employees").val() === "true"));
                            record.set("hideClients", (self.$("#hideClients").val() === "true"));
                            record.set("hideCompanies", (self.$("#hideCompanies").val() === "true"));

                                //console.log('ProgressEmployeeView:employeeDataSource:save:recordToSave:' + JSON.stringify(record));

                            $.when(this.sync()).done(function (e) {

                                if (self.changedToTermed === true) {
                                    self.userDataSource.fetch(function () {

                                        var data = this.data();
                                        var count = data.length;

                                        if (count > 0) {
                                            console.log('ProgressEmployeeView:employeeDataSource:save:setting user to inactive because Employee is termed.');

                                            var userRecord = data[0];

                                            // Update user to inactive
                                            userRecord.set("inactive", true);
                                            self.userDataSource.sync();
                                        }

                                    });
                                }
                                //console.log('ProgressEmployeeView:employeeDataSource:add new sync done:' + record.employeeID);
                                App.model.set('selectedEmployeeId', record.employeeID);
                                App.model.set('teamLeaderRecords', []);

                                App.router.navigate('employeeProfile', {trigger: true});

                            });

                        }
                    });
                }

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressEmployeeView:displayNoDataOverlay');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressEmployeeView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressEmployeeView:resize');

            },
            remove: function () {
                //console.log("-------- ProgressEmployeeView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);


                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });