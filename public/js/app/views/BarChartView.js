define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/barChart', 'kendo/kendo.data', 'chartjs', 'chartjs-plugin-datalabels',
        'jquery.dateFormat'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('BarChartView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                this.dataSeriesLabels = [];
                this.dataSeriesLabels2 = [];
                this.dataSeriesIDs = [];
                this.dataSeries = [];

                var self = this;
                var app = App;

                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;
                this.month = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter); //11;

                var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                this.startDate = moment(startDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                this.endDate = moment(endDate, 'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                this.showCompTime = App.model.get('showCompTime');
            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- BarChartView:onShow ----------');

                var self = this;

                // Create chart
                this.getData();

                this.$('#barChartTitle').html(this.options.type);

                if (self.showCompTime === true) {
                    $('.toggleCheckBox').prop('checked', true);
                    $('.toggle').prop('title', "Show Comp Time");
                } else {
                    $('.toggleCheckBox').prop('checked', false);
                    $('.toggle').prop('title', "Don't Show Comp Time");
                }
            },
            getData: function () {
                //console.log('BarChartView:getData');

                var self = this;
                var app = App;

                // Team Leaders

                //SELECT employeeID, firstName, Lastname
                //FROM tblProgressEmployees
                //WHERE isTeamLeader = 1
                //AND employeeID <> 74

                this.employeeDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                dataType: "json"
                            },
                            complete: function (e) {
                                console.log('BarChartView:employeeDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'TimeCardView:employeeDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    teamName: {editable: true, type: "string", validation: {required: false}},
                                    emailAddress: {editable: true, type: "string", validation: {required: true}},
                                    esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                    eeTypeTitle: {type: "string", validation: {required: false}},
                                    status: {
                                        type: "string",
                                        validation: {required: false},
                                        values: [
                                            {text: '1 - Active', value: '1 - Active'},
                                            {text: '2 - Termed', value: '2 - Termed'},
                                            {text: '3 - No Hire', value: '3 - No Hire'}
                                        ]
                                    },
                                    isTeamLeader: {editable: true, type: "boolean"},
                                    isEligibleSupport: {editable: true, type: "boolean"},
                                    maxClientLoad: {editable: true, type: "number"},
                                    currentClientLoad: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        sort: {field: "employeeID", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('BarChartView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingChart"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('BarChartView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#loadingChart"), false);
                        },
                        error: function (e) {
                            //console.log('PortalEmployeeGridView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingChart"), false);
                        },
                        change: self.onEmployeeDataSourceChange
                    });

                this.progressClientDS = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                },
                                complete: function (e) {
                                    console.log('BarChartView:progressClientDS:read:complete');
                                },
                                error: function (e) {
                                    var message = 'BarChartView:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "clientID",
                                fields: {
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    status: {editable: true, type: "string", validation: {required: false}},
                                    planHours: {type: "number"},
                                    schedulingHours: {type: "number"},
                                    taskTargetLow: {type: "number"},
                                    taskTargetHigh: {type: "number"},
                                    mtgTargetLowDec: {type: "number"},
                                    mtgTargetHigh: {type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   SummaryWidgetView:requestStart');

                            kendo.ui.progress(self.$("#loadingChart"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    SummaryWidgetView:requestEnd');
                            //kendo.ui.progress(self.$("#loadingChart"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   SummaryWidgetView:error');
                            kendo.ui.progress(self.$("#loadingChart"), false);

                            self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        change: self.onProgressClientDSChange
                    });

                // *** data call - vProgressClientData ***

                // getTeamClientData
                // SELECT SUM(TotalMeetings) AS MeetingsTotal, SUM(TotalMeetingsWithComp)
                // AS MeetingsTotalWithComp FROM getClientData
                // WHERE clientID IN(341,403,432,443,486,1145,1774,1808,1845,1894,1897,1916,1931,1950,1953,1980,2011,2017,2066,2067,2068,2074,2087,2100,2122,2123,2126,2133,2134,2148,2209,2239,2249,2307,2312,2317,2355,2376,2379,2380,2382,2384,2393,2394,2402,2406,2479,2487,2488,2494,2563)


                //SELECT clientId, sumTotalTime, sumAdminTime, SumTotalTasks, sumTotalMeetings,
                //    sumCompTime, sumTotalTasksWithComp, sumTotalMeetingsWithComp
                //FROM vProgressClientData
                //WHERE vYear = #thisYear# AND vMonth = #thisMonth#
                //ORDER BY clientID

                //"clientID":381,"vYear":2010,"vMonth":7,"sumTotalTime":1.23,"sumTotalTasks":20,
                //"sumTotalMeetings":1,"sumAdminTime":0.0,"sumCompTime":0.0,"sumCompCalls":0,"sumCompMeetings":0,
                //"sumTotalTimeWithComp":1.23,"sumTotalTasksWithComp":20,"sumTotalMeetingsWithComp":1,"entryID":146

                this.restDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetTeamTimeCardMeetingsByTeamLeader/" + self.startDate + "/" + self.endDate;
                                    return url;
                                },
                                complete: function (e) {
                                    console.log('BarChartView:restDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'BarChartView:restDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                type: "GET",
                                dataType: "json"
                            }
                            //autoBind: false,
                        },
                        schema: {
                            //data: "value",
                            //total: function (data) {
                            //    return data['odata.count'];
                            //},
                            model: {
                                id: "clientTeamLeaderID",
                                fields: {
                                    clientTeamLeaderID: {editable: false, type: "number"},
                                    //intTimerID: {editable: false, type: "number"},
                                    //companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    //employeeID: {editable: false, type: "number"},
                                    //serviceID: {editable: false, type: "number"},
                                    //workDate: {
                                    //    editable: true,
                                    //    type: "date"
                                    //    //format: "{0:yyyy-MM-ddTHH}",
                                    //    //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    //},
                                    //startTime: {
                                    //    editable: true,
                                    //    type: "date"
                                    //    //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    //},
                                    //stopTime: {
                                    //    editable: true,
                                    //    type: "date"
                                    //    //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    //},
                                    //clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    //clientName: {editable: true, type: "string", validation: {required: false}},
                                    //customerName: {editable: true, type: "string", validation: {required: true}},
                                    //employeeName: {editable: true, type: "string", validation: {required: false}},
                                    //serviceItem: {type: "string", validation: {required: false}},
                                    //tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"}
                                    //timerNote: {editable: true, type: "string", validation: {required: false}},
                                    //mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    //timerAction: {editable: true, type: "string", validation: {required: false}},
                                    //recordType: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        //group: { field: "clientName" },
                        //sort: [
                        //    {field: "clientName", dir: "asc"},
                        //    {field: "workDate", dir: "asc"}
                        //],
                        //filter: {field: "lastName", operator: "neq", value: ""},
                        requestStart: function (e) {
                            //console.log('MyTimeCardDetailGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingChart"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('MyTimeCardDetailGridView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#loadingChart"), false);
                        },
                        change: function (e) {
                            //console.log('MyTimeCardDetailGridView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('MyTimeCardDetailGridView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingChart"), false);
                        }
                    });

                self.restDataSource.fetch(function (data) {

                    var empState = {};
                    var empFilters = [];
                    empFilters.push({field: "isTeamLeader", operator: "eq", value: true});
                    empFilters.push({field: "employeeID", operator: "neq", value: 74});
                    empState.filter = empFilters;
                    self.employeeDataSource.query(empState);
                });


            },
            onEmployeeDataSourceChange: function (e) {
                var self = this;

                var empData = self.employeeDataSource.data();
                self.leaderCount = empData.length;

                //console.log('BarChartView:onEmployeeDataSourceChange:leaderCount:' + self.leaderCount);
                // For each leader returned by employeeDataSource
                var state = {};
                var filters = [];

                $.each(empData, function (index, value) {

                    //console.log('BarChartView:employeeDataSource:employeeID:firstName' + this.employeeID + ":" + this.firstName);
                    //console.log('BarChartView:employeeDataSource:employeeID:teamName' + this.employeeID + ":" + this.teamName);

                    if (this.teamName !== null && this.teamName !== undefined) {
                        self.dataSeriesLabels.push("Team " + this.teamName);
                    } else {
                        self.dataSeriesLabels.push("Team " + this.firstName);
                    }
                    self.dataSeriesIDs.push(this.employeeID);

                    filters.push({field: "teamLeaderID", operator: "eq", value: this.employeeID});

                });
                //filters.push({field: "status", operator: "contains", value: "4"});

                //console.log('BarChartView:employeeDataSource:filters:' + JSON.stringify(filters));

                state.filter = {
                    logic: "or",
                    filters: filters
                };
                if (empData.length > 0) {

                    self.progressClientDS.query(state);
                } else {
                    kendo.ui.progress(self.$("#loadingChart"), false);
                }

            },
            onProgressClientDSChange: function (e) {
                //console.log('BarChartView:onProgressClientDSChange');

                var self = this;

                var data = this.progressClientDS.data();

                var clientRecords = [];

                //var clientIDs = [];
                //var teamLeaderID = 0;
                //var state = {};
                //var filters = [];

                //<cfquery name="getTargets" dbtype="query" maxrows="1">
                //    SELECT SUM(mtgTargetLowDec  * planHours) AS meetingsTarget
                //    FROM getTeamClients
                //</cfquery>

                $.each(data, function (index, value) {
                    var record = value;
                    if (record.status.indexOf("4") > -1) {
                        //console.log('BarChartView:onprogressClientDSChange:clientData record:' + JSON.stringify(record));
                        clientRecords.push({
                            clientID: record.clientID,
                            mtgTargetLowDec: record.mtgTargetLowDec,
                            planHours: record.planHours,
                            schedulingHours: record.schedulingHours,
                            teamLeaderID: record.teamLeaderID,
                            status: record.status
                        });
                    }

                });

                self.parseDataSourceResults(clientRecords);
            },
            parseDataSourceResults: function (clientRecords) {

                var self = this;
                var app = App;

                //var data = this.progressClientDataDataSource.data();
                var data = this.restDataSource.data();

                console.log('BarChartView:parseDataSourceResults:data.length:' + data.length);
                if (data.length === 0) {
                    kendo.ui.progress(self.$("#loadingChart"), false);
                    $("#barChart").hide();
                    console.log('BarChartView:parseDataSourceResults:no data records');
                    return;
                }

                // ProgressClientData records
                $.each(self.dataSeriesIDs, function (index3, value3) {


                    var sumTotalMeetings = 0;
                    //var sumTotalMeetingsWithComp = 0;
                    var sumMeetingsTarget = 0;
                    var sumPlanHours = 0;
                    //var sumCompTime = 0;

                    // Get total meetings for the current timeframe for this Team Leader
                    $.each(data, function (clientDataIndex, value) {
                        //console.log('BarChartView:parseDataSourceResults:data:value:' + JSON.stringify(value));
                        if (value.teamLeaderID === value3 ) {
                            //sumTotalMeetings += value.meetings;
                        }
                    });

                    // Get total planned meetings for current timeframe for this Team Leader

                    var thisClientHours = 0;

                    // ProgressClient records
                    $.each(clientRecords, function (index2, clientValue) {
                        var clientRecord = clientValue;

                        //if (clientRecord.schedulingHours === 0) {
                        thisClientHours = 0;
                        //}

                        if (clientRecord.teamLeaderID === value3) {

                            // GetTeamTimeCardDetail records
                            $.each(data, function (index, value) {

                                if (clientRecord.clientID === value.clientID && clientRecord.teamLeaderID === value.clientTeamLeaderID) {

                                    // Choose to show comp time or not (1008)
                                    if (self.showCompTime === true) {

                                        if (value.serviceID === 1001 || value.serviceID === 1011 || value.serviceID === 1008) {
                                            sumTotalMeetings += value.meetings;
                                        }
                                        if (clientRecord.schedulingHours === 0) {
                                            thisClientHours += value.timeWorked;
                                        }
                                    } else {

                                        if (value.serviceID === 1001 || value.serviceID === 1011) {
                                            sumTotalMeetings += value.meetings; //sumTotalMeetings;
                                        }
                                        if (clientRecord.schedulingHours === 0 && value.serviceID !== 1008) { //&& value.serviceID === 1001
                                            thisClientHours += value.timeWorked;
                                        }
                                    }
                                }
                            });

                            if (clientRecord.schedulingHours === 0) {
                                sumMeetingsTarget += clientRecord.mtgTargetLowDec * thisClientHours;
                                sumPlanHours += thisClientHours;
                            } else {
                                sumMeetingsTarget += clientRecord.mtgTargetLowDec * clientRecord.planHours;
                                sumPlanHours += clientRecord.planHours;
                            }
                            //console.log('BarChartView:parseDataSourceResults:clientRecord.schedulingHours:clientRecord.planHours:thisClientHours:' + clientRecord.schedulingHours + ":"  + planHours + ":" + thisClientHours);
                            //console.log('BarChartView:parseDataSourceResults:clientID:summtgTargetLowDec:' + clientRecord.clientID + ":" + sumMeetingsTarget);

                        }
                    });

                    //console.log('BarChartView:parseDataSourceResults:sumTotalMeetings:' + sumTotalMeetings);
                    //console.log('BarChartView:parseDataSourceResults:summtgTargetLowDec:' + sumMeetingsTarget);
                    //console.log('BarChartView:parseDataSourceResults:sumPlanHours:' + sumPlanHours);

                    var mtgPercent = 0;

                    if (sumMeetingsTarget > 0) {
                        mtgPercent = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalMeetings / sumMeetingsTarget * 100));
                    }

                    //console.log('BarChartView:parseDataSourceResults:mtgPercent:' + mtgPercent);
                    self.dataSeries.push(mtgPercent);
                    self.dataSeriesLabels2.push(mtgPercent + "%");
                    //console.log('BarChartView:parseDataSourceResults:self.dataSeries.length:' + self.dataSeries.length + ":leaderCount:" + self.leaderCount + ":total/sumMtgTarget:" + sumTotalMeetings + "/" + sumMeetingsTarget);
                });


                //if (self.dataSeries.length === self.leaderCount) {
                kendo.ui.progress(self.$("#loadingChart"), false);
                self.createChart();
                //}
            },
            createChart: function () {

                var self = this;

                //console.log('BarChartView:createChart:data:labels:' + JSON.stringify(self.dataSeries) + JSON.stringify(self.dataSeriesLabels) );

                var chartOptions = {
                    plugins: {
                        datalabels: {
                            align: 'end',
                            anchor: 'end',
                            color: 'gray',
                            formatter: function (value, context) {
                                //console.log('BarChartView:createChart:' + context);
                                return context.dataset.labels[context.dataIndex];
                            },
                            font: {
                                family: "'Source Sans Pro','Helvetica Neue', Helvetica, Arial, sans-serif",
                                weight: 'bold',
                                size: 14
                            }
                        }
                    },
                    layout: {
                        padding: 24
                    },
                    tooltips: {
                        enabled: false
                    },
                    legend: {
                        display: false,
                        position: 'right'
                    },
                    scales: {
                        yAxes: [{
                            height: 120,
                            display: true
                        }],
                        xAxes: [{
                            height: 120,
                            display: true,
                            position: 'bottom'
                        }]
                    },
                    maintainAspectRatio: false,
                    responsive: true
                };

                // Create chart
                var ctx = self.$("#barChart").get(0).getContext("2d");

                var chartData = {
                    datasets: [
                        {
                            labels: self.dataSeriesLabels2,
                            //label: "Team Meeting Percentage (%)",
                            lineTension: 0.1,
                            backgroundColor: "rgba(160, 208, 224,1)",
                            borderColor: "rgba(160, 208, 224,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            fillColor: "rgba(160, 208, 224,1)",
                            strokeColor: "rgba(160, 208, 224,0.8)",
                            pointColor: "#3b8bba",
                            pointStrokeColor: "rgba(160, 208, 224,1)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(160, 208, 224,1)",
                            data: self.dataSeries
                            //datalabels: {
                            //    align: 'end'
                            //    anchor: 'end'
                            //}
                        }
                    ],
                    labels: self.dataSeriesLabels
                };

                self.barChart = new Chart(ctx, {
                    type: 'bar',
                    data: chartData,
                    options: chartOptions
                });

                self.barChart.resize();
            },
            displayNoDataOverlay: function () {
                //console.log('ProjectGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProjectGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('.tile-content').css('display', 'block');
            },
            onResize: function () {
                //console.log('BarChartView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('BarChartView:resize');

                //this.scatterChart.resize();
                //this.$('#barChart').height(this.$el.parent().height() - 15);

            },
            remove: function () {
                //console.log('---------- BarChartView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                // Destroy chart
                //this.scatterChart.clear();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });