define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/myTimeCardDetailLayout',
    'views/MyTimeCardDetailGridView','views/SubHeaderView','views/TimeCardSummaryWidgetView'],
    function (App, Backbone, Marionette, $, template, MyTimeCardDetailGridView,SubHeaderView,TimeCardSummaryWidgetView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                timeCardDetails: "#timecard-details",
                timeCardSummary: "#timecard-summary",
                timeCardSummaryBiMonthly: "#timecard-summary-bimonthly"

            },
            initialize: function () {

                //console.log('MyTimeCardDetailLayoutView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.MyEmployeeIdChanged, this.onMyEmployeeIdChanged);

            },
            onRender: function () {

                //console.log('MyTimeCardDetailLayoutView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- MyTimeCardDetailLayoutView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    this.subHeaderRegion.show(new SubHeaderView({
                        majorTitle: "My Time Card",
                        minorTitle: "", //'My time card data for the current date range',
                        page: "My Time Card",
                        showDateFilter: true,
                        showStatusFilter: false,
                        showEmpStatusFilter: false,
                        showClientReportFilter: false,
                        showViewsFilter: false,
                        showCompanyFilter: false,
                        showClientFilter: false,
                        showTeamLeaderFilter:false,
                        showPayrollProcessingDate:false
                    }));
                    this.timeCardSummary.show(new TimeCardSummaryWidgetView({
                        type: "Daily"
                    }));
                    this.timeCardSummaryBiMonthly.show(new TimeCardSummaryWidgetView({
                        type: "Bi-monthly"
                    }));
                    this.timeCardDetails.show(new MyTimeCardDetailGridView());

                }
            },
            resetLayout: function () {
                this.timeCardDetails.reset();
                this.timeCardDetails.show(new MyTimeCardDetailGridView());

                this.timeCardSummary.reset();
                this.timeCardSummary.show(new TimeCardSummaryWidgetView({
                    type: "Daily"
                }));

                this.timeCardSummaryBiMonthly.reset();
                this.timeCardSummaryBiMonthly.show(new TimeCardSummaryWidgetView({
                    type: "Bi-monthly"
                }));

            },
            onMyEmployeeIdChanged: function () {

                this.resetLayout();
},
            onSelectedDateFilterChanged: function () {

                this.resetLayout();
            },

            resize: function () {
                //console.log('MyTimeCardDetailLayoutView:resize');

                //if (this.timeCardDetails.currentView)
                //    this.timeCardDetails.currentView.resize();

            },
            remove: function () {
                //console.log("--------MyTimeCardDetailLayoutView:remove --------");

                //this.saveButtonClicked();

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });