define(['App', 'jquery', 'hbs!templates/docsLayout', 'backbone', 'marionette',
        'views/DocCollectionView', 'views/SubHeaderView'],
    function (App, $, template, Backbone, Marionette, DocCollectionView,SubHeaderView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                esaForms: '#esa-forms',
                federalRightsPostings: '#federal-rights-postings',
                stateRightsPostings0: '#state-rights-postings0',
                stateRightsPostings1: '#state-rights-postings1',
                stateRightsPostings2: '#state-rights-postings2',
                stateRightsPostings3: '#state-rights-postings3',
                stateRightsPostings4: '#state-rights-postings4',
                stateRightsPostings5: '#state-rights-postings5',
                stateRightsPostings6: '#state-rights-postings6',
                stateRightsPostings7: '#state-rights-postings7',
                stateRightsPostings8: '#state-rights-postings8',
                stateRightsPostings9: '#state-rights-postings9',
                stateRightsPostings10: '#state-rights-postings10',
                stateRightsPostings11: '#state-rights-postings11',
                stateRightsPostings12: '#state-rights-postings12',
                stateRightsPostings13: '#state-rights-postings13',
                stateRightsPostings14: '#state-rights-postings14',
                stateRightsPostings15: '#state-rights-postings15',
                stateRightsPostings16: '#state-rights-postings16',
                stateRightsPostings17: '#state-rights-postings17',
                stateRightsPostings18: '#state-rights-postings18'
            },
            initialize: function () {
                //console.log('DocsLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('DocsLayoutView:onShow');

                this.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "HR Forms & Docs",
                    minorTitle: "",
                    page: "HR Forms & Docs",
                    showDateFilter: false,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));

                this.esaForms.show(new DocCollectionView({category: 9}));
                this.federalRightsPostings.show(new DocCollectionView({category: 10}));
                this.stateRightsPostings0.show(new DocCollectionView({category: 11,subCategory:0}));
                this.stateRightsPostings1.show(new DocCollectionView({category: 11,subCategory:1}));
                this.stateRightsPostings2.show(new DocCollectionView({category: 11,subCategory:2}));
                this.stateRightsPostings3.show(new DocCollectionView({category: 11,subCategory:3}));
                this.stateRightsPostings4.show(new DocCollectionView({category: 11,subCategory:4}));
                this.stateRightsPostings5.show(new DocCollectionView({category: 11,subCategory:5}));
                this.stateRightsPostings6.show(new DocCollectionView({category: 11,subCategory:6}));
                this.stateRightsPostings7.show(new DocCollectionView({category: 11,subCategory:7}));
                this.stateRightsPostings8.show(new DocCollectionView({category: 11,subCategory:8}));
                this.stateRightsPostings9.show(new DocCollectionView({category: 11,subCategory:9}));
                this.stateRightsPostings10.show(new DocCollectionView({category: 11,subCategory:10}));
                this.stateRightsPostings11.show(new DocCollectionView({category: 11,subCategory:11}));
                this.stateRightsPostings12.show(new DocCollectionView({category: 11,subCategory:12}));
                this.stateRightsPostings13.show(new DocCollectionView({category: 11,subCategory:13}));
                this.stateRightsPostings14.show(new DocCollectionView({category: 11,subCategory:14}));
                this.stateRightsPostings15.show(new DocCollectionView({category: 11,subCategory:15}));
                this.stateRightsPostings16.show(new DocCollectionView({category: 11,subCategory:16}));
                this.stateRightsPostings17.show(new DocCollectionView({category: 11,subCategory:17}));
                this.stateRightsPostings18.show(new DocCollectionView({category: 11,subCategory:18}));

            },
            transitionIn: function () {
                console.log('DocsLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('DocsLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('DocsLayoutView:resize');
                var self = this;
            },
            remove: function () {
                //console.log('---------- DocsLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });