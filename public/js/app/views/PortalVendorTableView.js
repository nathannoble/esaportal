define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalVendorTable', 'views/PortalVendorGridView'],
    function (App, Backbone, Marionette, $, template, PortalVendorGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                vendorRegion: "#vendor-region"
            },
            initialize: function () {

                //console.log('PortalVendorTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('PortalVendorTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalVendorTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    this.vendorRegion.show(new PortalVendorGridView());
                }

            },
            validateData: function () {
                var self = this;

                if (App.modal.currentView === null || App.modal.currentView === undefined) {

                }
            },
            resize: function () {
                //console.log('PortalVendorTableView:resize');

                if (this.vendorRegion.currentView)
                    this.vendorRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalVendorTableView:remove --------");

                //this.saveButtonClicked();

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });