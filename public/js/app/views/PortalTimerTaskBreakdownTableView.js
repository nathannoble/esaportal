define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalTimerTaskBreakdownTable', 'views/portalTimerTaskBreakdownGridView'],
    function (App, Backbone, Marionette, $, template, PortalTimerTaskBreakdownGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                taskBreakdownRegion: "#task-breakdown-region"
            },
            initialize: function () {

                //console.log('PortalTimerTaskBreakdownTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('PortalTimerTaskBreakdownTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalTimerTaskBreakdownTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.taskBreakdownRegion.show(new PortalTimerTaskBreakdownGridView());
                }

            },
            validateData: function () {
                var self = this;

                if (App.modal.currentView === null || App.modal.currentView === undefined) {

                }
            },
            saveButtonClicked: function (e) {
                //console.log('PortalTimerTaskBreakdownTableView:saveButtonClicked');

                var self = this;

            },
            nextButtonClicked: function (e) {
                //console.log('PortalTimerTaskBreakdownGridView:nextButtonClicked');

                var self = this;
            },
            resize: function () {
                //console.log('PortalTimerTaskBreakdownTableView:resize');

                if (this.taskBreakdownRegion.currentView)
                    this.taskBreakdownRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalTimerTaskBreakdownTableView:remove --------");

                //this.saveButtonClicked();

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });