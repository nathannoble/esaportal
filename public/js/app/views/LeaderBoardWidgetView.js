define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/leaderBoardWidget',
    'views/LeaderCollectionView', 'kendo/kendo.data','date'],
    function (App, Backbone, Marionette, $, template, LeaderCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                leaderList: '#leader-list'
            },
            initialize: function (options) {
                //console.log('LeaderBoardWidgetView:initialize');

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "This Month"};
                }

                this.viewModel = kendo.observable({
                    isChecked: false
                });

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                //console.log('LeaderBoardWidgetView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('LeaderBoardWidgetView:onShow');

                // Setup the containers
                this.resize();

                var self = this;

                if (this.options.type === "This Month") {
                    this.$('#leaderBoardTitle').html("Month's Top 5 ");
                } else {
                    this.$('#leaderBoardTitle').html("Today's Top 5 ");
                }

                this.leaderList.show(new LeaderCollectionView({type: this.options.type}));
            },

            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('LeaderBoardWidgetView:resize');

            },
            remove: function () {
                //console.log('-------- LeaderBoardWidgetView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });