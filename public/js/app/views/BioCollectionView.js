define(['App', 'backbone', 'marionette', 'jquery', 'models/AdminDataModel', 'hbs!templates/bioCollection',
        'views/BioView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, AdminDataModel, template, BioView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: BioView,
            initialize: function (options) {

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                //console.log('BioCollectionView:initialize:type:' + this.options.type);

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.bios = [];

                var self = this;

                self.subCategories = [
                    {text: 'corporate-leadership', value: 0},
                    {text: 'executive-team', value: 1},
                    {text: 'senior-management', value: 2},
                    {text: 'account-managers', value: 3},
                    {text: 'assistant-account-managers', value: 4},
                    {text: 'employees', value: 5}
                ];

                this.getData();

            },
            onShow: function () {
                //console.log("-------- BioCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                this.adminDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalAdminData";
                            },
                            complete: function (e) {
                                console.log('BioCollectionView:adminDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'BioCollectionView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {editable: false, type: "number"},
                                text: {editable: true, type: "string"},
                                shortText: {editable: true, type: "string"},
                                text2: {editable: true, type: "string"},
                                title: {editable: true, type: "string"},
                                fileName: {editable: true, type: "string"},
                                externalLink: {editable: true, type: "string"},
                                category: {editable: true, type: "number"},
                                subCategory: {editable: true, type: "number"},
                                priority: {editable: true, type: "number"},
                                showAsCompanyMessage:{editable: true, type: "boolean"},
                                addUserImage:{editable: true, type: "boolean"},
                                timeStamp: {editable: true,type: "date"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   BioCollectionView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#bioCollectionViewLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    BioCollectionView:messageDataSource:requestEnd');
                        //kendo.ui.progress(self.$("#bioCollectionViewLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   BioCollectionView:messageDataSource:error');
                        kendo.ui.progress(self.$("#bioCollectionViewLoading"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   BioCollectionView:messageDataSource:change');

                        var data = this.data();

                        //if (data.length <= 0)
                        //    self.displayNoDataOverlay();
                        //else
                        //    self.hideNoDataOverlay();
                    },
                    //sort: {field: "messageDate", dir: "desc"},
                    filter: [
                        {field: "subCategory", operator: "eq", value: self.options.subCategory},
                        {field: "category", operator: "eq", value: 8}
                    ]
                });

                this.adminDataSource.fetch(function () {

                    var data = this.data();

                    if (data.length === 0) {
                        var label = self.subCategories[self.options.subCategory].text;
                        $("." + label).hide();
                    } else {
                        $.each(data, function (index, value) {

                            self.bios.push({
                                ID: value.ID,
                                fileName: value.fileName,
                                title: value.title,
                                shortText: value.shortText,
                                text: value.text,
                                text2: value.text2,
                                subCategory: value.subCategory
                            });
                        });

                        self.bios.sort(function (a, b) {
                            var c = a.shortText;
                            var d = b.shortText;
                            return d - c;
                        });

                        // Populate bio collection
                        self.collection.reset();
                        self.collection.comparator = function (model) {
                            return (model.get("subCategory"));
                        };

                        $.each(self.bios, function (index, value) {

                            //console.log('BioCollectionView:file:value:' + JSON.stringify(value));
                            var model = new AdminDataModel();

                            model.set("ID", value.ID);
                            model.set("fileName", value.fileName);
                            model.set("title", value.title);
                            model.set("shortText", value.shortText);
                            model.set("text", value.text);
                            model.set("text2", value.text2);
                            model.set("subCategory", self.options.type);

                            self.collection.push(model);
                        });
                    }

                    //self.collection.sort();
                });
            },
            hideNoDataOverlay: function () {
                //console.log('BioCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#fileWidgetContent').css('display', 'block');
            },
            onResize: function () {
                //console.log('BioCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('BioCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- BioCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
