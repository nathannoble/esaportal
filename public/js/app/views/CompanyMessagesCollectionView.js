define(['App', 'backbone', 'marionette', 'jquery', 'models/AdminDataModel', 'hbs!templates/companyMessagesCollection',
        'views/CompanyMessagesView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, AdminDataModel, template, CompanyMessagesView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: CompanyMessagesView,
            initialize: function (options) {

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                //console.log('CompanyMessagesCollectionView:initialize:type:' + this.options.type);

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.messages = [];

                this.getData();

            },
            onRender: function () {
                console.log('CompanyMessagesCollectionView:onRender');

                this.$el = $('#company-messages');//this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- CompanyMessagesCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                var offset = (new Date()).getTimezoneOffset();
                var anyTimeToday = new Date();
                anyTimeToday.setHours(23);
                //var endOfDayYesterday = new Date(moment(new Date()).clone().format('YYYY-MM-DD'));

                var endOfDayToday = new Date();
                endOfDayToday.setHours(23);
                //var endOfDayToday = new Date(moment(new Date()).clone().add(1, 'days').format('YYYY-MM-DD'));

                var filter = {
                    logic: "and",
                    filters: [
                        {field: "showStart", operator: "neq", value: null},
                        {field: "showStart", operator: "lte", value: anyTimeToday},
                        {field: "showStop", operator: "neq", value:null},
                        {field: "showStop", operator: "gte", value: endOfDayToday},
                        {field: "category", operator: "eq", value: self.options.category},
                        {field: "showAsCompanyMessage", operator: "eq", value: true}
                    ]
                };

                this.adminDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalAdminData";
                            },
                            complete: function (e) {
                                console.log('CompanyMessagesCollectionView:adminDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'CompanyMessagesCollectionView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {editable: false, type: "number"},
                                text: {editable: true, type: "string"},
                                shortText: {editable: true, type: "string"},
                                text2: {editable: true, type: "string"},
                                title: {editable: true, type: "string"},
                                fileName: {editable: true, type: "string"},
                                externalLink: {editable: true, type: "string"},
                                category: {editable: true, type: "number"},
                                subCategory: {editable: true, type: "number"},
                                priority: {editable: true, type: "number"},
                                showAsCompanyMessage:{editable: true, type: "boolean"},
                                addUserImage:{editable: true, type: "boolean"},
                                timeStamp: {editable: true,type: "date"},
                                showStop: {editable: true,type: "date"},
                                showStart: {editable: true,type: "date"}
                            }
                        }
                    },
                    pageSize: 100,
                    page: 1,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   CompanyMessagesCollectionView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#companyMessagesCollectionViewLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    CompanyMessagesCollectionView:messageDataSource:requestEnd');
                        //kendo.ui.progress(self.$("#companyMessagesCollectionViewLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   CompanyMessagesCollectionView:messageDataSource:error');
                        kendo.ui.progress(self.$("#companyMessagesCollectionViewLoading"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   CompanyMessagesCollectionView:messageDataSource:change');

                        var data = this.data();

                        //if (data.length <= 0)
                        //    self.displayNoDataOverlay();
                        //else
                        //    self.hideNoDataOverlay();
                    },
                    sort: [
                        {field: "timeStamp", dir: "desc"},
                        {field: "priority", dir: "asc"}
                    ],
                    filter: filter
                });

                this.adminDataSource.fetch(function () {

                    var data = this.data();
                    $.each(data, function (index, value) {

                        self.messages.push({
                            ID:value.ID,
                            fileName:value.fileName,
                            title:value.title,
                            shortText:value.shortText,
                            text:value.text,
                            text2:value.text2,
                            category: value.category,
                            subCategory: value.subCategory,
                            externalLink: value.externalLink,
                            priority: value.priority,
                            timeStamp:value.timeStamp,
                            isFirst: index = 0
                        });
                    });

                    self.messages.sort(function(a,b){
                        var c = new Date(a.timeStamp);
                        var d = new Date(b.timeStamp);
                        return d-c;
                    });

                    // Populate doc collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return (model.get("ID"));
                    };

                    // Clear indicators
                    $('#company-indicators li').remove();

                    $.each(self.messages, function (index, value) {

                        //console.log('CompanyMessagesCollectionView:file:value:' + JSON.stringify(value));
                        var model = new AdminDataModel();

                        model.set("ID", value.ID);
                        model.set("fileName", value.fileName);
                        model.set("title", value.title);
                        model.set("shortText", value.shortText);
                        model.set("text", value.text);
                        model.set("text2", value.text2);
                        model.set("category", value.category);
                        model.set("subCategory", value.subCategory);
                        model.set("externalLink", value.externalLink);
                        model.set("priority", value.priority);
                        model.set("timeStamp", value.timeStamp);
                        if (index === 0) {
                            model.set("isFirst", true);
                            $("#company-indicators").append('<li data-target="#carousel-example-generic" data-slide-to="'+index+'" class="active"></li>');
                        } else {
                            model.set("isFirst", false);
                            $("#company-indicators").append('<li data-target="#carousel-example-generic" data-slide-to="'+index+'" class=""></li>');
                        }

                        self.collection.push(model);
                    });

                    self.collection.sort();

                    window.setTimeout(function () {
                        console.log('CompanyMessagesCollectionView:carousel right-click');
                        if (document.getElementById("carouselRightButton")) {
                            document.getElementById("carouselRightButton").click();
                        }
                    }, 8000);

                });
            },
            hideNoDataOverlay: function () {
                //console.log('CompanyMessagesCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#fileWidgetContent').css('display', 'block');
            },
            onResize: function () {
                //console.log('CompanyMessagesCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('CompanyMessagesCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- CompanyMessagesCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
