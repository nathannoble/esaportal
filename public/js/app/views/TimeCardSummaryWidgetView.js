define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/timeCardSummaryWidget',
    'views/TimeCardCollectionView', 'kendo/kendo.data','date'],
    function (App, Backbone, Marionette, $, template, TimeCardCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                timeCardList: '#timecard-list'
            },
            initialize: function (options) {
                //console.log('TimeCardSummaryView:initialize');

                var self = this;
                var app = App;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.options = options;

            },
            onRender: function () {
                //console.log('TimeCardSummaryView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('TimeCardSummaryView:onShow');

                // Setup the containers
                this.resize();

                var self = this;

                this.timeCardList.show(new TimeCardCollectionView(this.options));
            },

            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('TimeCardSummaryView:resize');

            },
            remove: function () {
                //console.log('-------- TimeCardSummaryView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });