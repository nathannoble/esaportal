define(['App', 'backbone', 'marionette', 'jquery',
        'hbs!templates/timerOptions', 'kendo/kendo.data', 'kendo/kendo.combobox'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #CloseButton': 'onCloseClicked',
                'click #SaveButton': 'onSaveClicked'
            },
            initialize: function (options) {
                console.log('TimerOptionsView:initialize');
                //this.writeLog('TimerOptionsView:initialize');

                this.timerLogDataSource = new kendo.data.DataSource(
                    {
                        autoSync: true,
                        //autoBind: false,
                        //batch: false,
                        //pageSize: 10,
                        //serverPaging: true,
                        //serverSorting: true,
                        //serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalTimerLog";
                                },
                                complete: function (e) {
                                    console.log('TimeOptionsView:timerLogDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:timerLogDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            create: {
                                url: function (data) {
                                    //console.log('TimerOptionsView timerLogDataSourceClient:create');
                                    return App.config.DataServiceURL + "/odata/PortalTimerLog";
                                },
                                complete: function (e) {
                                    console.log('TimeOptionsView:timerLogDataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:timerLogDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "logID",
                                fields: {
                                    logID: {editable: false, defaultValue: 0, type: "number"},
                                    logDate: {editable: true, type: "date", validation: {required: false}},
                                    timerID: {editable: true, defaultValue: 0, type: "number"},
                                    logText: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        requestStart: function () {
                            //console.log('  TimerOptionsView:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   TimerOptionsView:requestEnd');
                            //kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        error: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            console.log('TimerOptionsView:writeLog:timerLogDataSource:error:e:' + JSON.stringify(e));
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                            //App.modal.show(new ErrorView());

                        }
                    });


                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                if (this.options === null || this.options === undefined) {
                    // need timerID to edit - throw error?
                }

                this.timeWorked = 0;
                this.entryType = this.options.entryType;
                this.serviceItem = this.options.serviceItem;
                this.clientName = this.options.clientName;
                this.companyID = parseInt(this.options.companyID, 0);
                this.taskTracking = this.options.taskTracking;
                this.recordTasksMtgs = this.options.recordTasksMtgs;
                this.toStats = this.options.toStats;
                this.intTimerID = App.model.get('intTimerID');
                this.currentTimerID = App.model.get('currentTimerID');   //this.options.currentTimerID;
                if (this.entryType === "M") {
                    this.serviceID = this.options.serviceID;
                    this.clientID = this.options.clientID;
                } else {
                    this.serviceID = App.model.get('currentTimerServiceID'); //this.options.serviceID;
                    this.clientID = App.model.get('currentTimerClientID');  //this.options.clientID;
                }
                this.myEmployeeId = App.model.get('myEmployeeId');
                this.timeZoneID = App.model.get('timeZoneId');

                this.hideCallsMeetings = false;

                this.getData();

            },
            onRender: function () {
                console.log('TimerOptionsView:onRender');
                //this.writeLog('TimerOptionsView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('---------- TimerOptionsView:onShow ----------');
                //this.writeLog('---------- TimerOptionsView:onShow ----------');

                var self = this;

                self.$(".k-textbox").kendoMaskedTextBox();
                self.$(".summary-fields").show();
                //self.$("#serviceItem").data("kendoMaskedTextBox").value(self.serviceItem);
                //self.$("#clientName").data("kendoMaskedTextBox").value(self.clientName);

                self.$("#workDate").kendoDatePicker({
                    value: new Date()
                    // TODO implement disableDates when updated version of kendo is functional
                    //disableDates: function (date) {
                    //    if (date <= (new Date())) {
                    //        return true;
                    //    } else {
                    //        return false;
                    //    }
                    //}
                });
                self.$("#timeWorked").kendoMaskedTextBox();
                var timeWorked = self.$("#timeWorked").data("kendoMaskedTextBox");

                if (self.entryType === "M") {

                    timeWorked.enable(true);

                } else {
                    self.$('#SaveButton').addClass('disabled');

                    var workDate = self.$("#workDate").data("kendoDatePicker");
                    workDate.readonly();
                    timeWorked.readonly();
                }

                // For LFD show extra fields
                console.log('TimerOptionsView:onShow:this.companyID:' + this.companyID);
                if (self.taskTracking === true) { //self.companyID === 12 ||
                    self.$(".lincoln-fields").show();
                } else {
                    self.$(".lincoln-fields").hide();
                }

            },
            getData: function () {

                var self = this;
                var app = App;

                self.serviceDataSource = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalServiceItem";
                                },
                                complete: function (e) {
                                    console.log('TimeOptionsView:serviceDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:serviceDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "serviceID",
                                fields: {
                                    serviceID: {editable: false, defaultValue: 0, type: "number"},
                                    ordering: {editable: false, defaultValue: 0, type: "number"},
                                    serviceItem: {editable: true, type: "string", validation: {required: false}},
                                    recordTasksMtgs: {editable: true, type: "boolean"},
                                    toStats: {editable: true, type: "boolean"},
                                    timerStatus: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('  TimerOptionsView:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   TimerOptionsView:requestEnd');
                            //kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('  TimerOptionsView:error');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                            //App.modal.show(new ErrorView());

                        },
                        sort: {field: "ordering", dir: "asc"}
                    });

                self.serviceDataSource.fetch(function () {

                    self.serviceRecords = this.data();

                    self.initializeServiceAndClient();

                });

                self.timerSummaryDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        //type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/rest/GetTimeCardDetailForOneIntTimerID/" + self.intTimerID;
                                },
                                complete: function (e) {
                                    console.log('TimeOptionsView:timerSummaryDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:timerSummaryDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, defaultValue: 0, type: "number"},
                                    intTimerID: {editable: false, defaultValue: 0, type: "number"},
                                    timeWorked: {type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function (e) {
                            //console.log('TimeCardView:dataSource:request start:');
                            //kendo.ui.progress(self.$("#companyLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('TimeCardView:dataSource:request end:');
                            //kendo.ui.progress(self.$("#companyLoading"), false);
                        },
                        error: function (e) {
                            //console.log('TimeCardView:dataSource:error');
                            //kendo.ui.progress(self.$("#companyLoading"), false);
                        }
                        //filter: [{field: "status", operator: "eq", value: '4 - Active'}],
                        //sort: [{field: "lastName", dir: "asc"}]

                    });

                self.timerDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        batch: false,
                        pageSize: 10,
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalTimer",
                                complete: function (e) {
                                    console.log('TimeOptionsView:timerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:timerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('TimerOptionsView::timerDataSource:update:url');
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.timerID + ")";
                                },
                                complete: function (e) {
                                    //console.log('TimerOptionsView:timerDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                    //self.writeLog('TimerOptionsView:timerDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                },
                                error: function (e) {
                                    console.log('TimerOptionsView:timerDataSource:update:error:' + JSON.stringify(e.responseJSON));
                                    self.writeLog('TimerOptionsView:timerDataSource:update:error:' + JSON.stringify(e.responseJSON));
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('TimerOptionsView:create:data:' + data);
                                    return App.config.DataServiceURL + "/odata/PortalTimer";
                                },
                                complete: function (e) {
                                    //self.writeLog('TimerOptionsView:timerDataSource:create:complete');
                                    if (e.responseJSON !== undefined) {
                                        //self.writeLog('TimerOptionsView:timerDataSource:create:complete:' + JSON.stringify(e.responseJSON));
                                        if (e.responseJSON.value[0]) {
                                            var newTimerID = e.responseJSON.value[0].timerID;
                                            //self.writeLog('TimerOptionsView:timerDataSource:create:complete:' + newTimerID);

                                            var currentNewIdTimeStamp = e.responseJSON.value[0].idTimeStamp;
                                            if (!self.lastNewIdTimeStamp) {
                                                self.lastNewIdTimeStamp = 0;
                                            }

                                            var start2 = moment(currentNewIdTimeStamp).clone();
                                            var start = moment(self.lastNewIdTimeStamp).clone();

                                            var msDiff = parseFloat(start.diff(start2));

                                            // A duplicate has been created
                                            if (self.currentTimerID !== null && (newTimerID < self.currentTimerID || Math.abs(msDiff) < 500)) {
                                                self.writeLog('TimerOptionsView:timerDataSource:create:complete:duplicate:msDiff:' + msDiff);

                                                //Delete duplicate
                                                self.deleteDuplicate(newTimerID);
                                            } else {
                                                self.lastNewIdTimeStamp = e.responseJSON.value[0].idTimeStamp;
                                                self.currentTimerID = newTimerID;
                                                //self.serviceID =  e.responseJSON.value[0].serviceID;
                                                //self.clientID = e.responseJSON.value[0].clientID;

                                                if (self.entryType === "M") {
                                                    self.updateTimerRecordWithTimerId();
                                                }
                                                //self.writeLog('TimerOptionsView:timerDataSource:create:complete:newTimerID:' + newTimerID + ":" + JSON.stringify(e.responseJSON));
                                            }
                                        } else {
                                            //self.writeLog('TimerOptionsView:timerDataSource:create:complete:no newTimerID:' + JSON.stringify(e.responseJSON));
                                        }
                                    } else {
                                        console.log('TimerOptionsView:timerDataSource:create:error');
                                        //self.writeLog("TimerOptionsView:timerDataSource:create:error:else");
                                    }
                                },
                                error: function (e) {
                                    console.log('TimerOptionsView:timerDataSource:create:error:' + JSON.stringify(e.responseJSON));
                                    self.writeLog('TimerOptionsView:timerDataSource:create:error:' + JSON.stringify(e.responseJSON));
                                }
                            },
                            destroy: {
                                url: function (data) {
                                    self.writeLog('TimerOptionsView:timerDataSource:destroy:timerID:' + data.timerID);
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.timerID + ")";
                                },
                                complete: function (e) {
                                    console.log('TimeOptionsView:timerDataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:timerDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    idTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    entryType: {editable: true, type: "string", validation: {required: false}},
                                    employeeID: {editable: true, type: "number"},
                                    clientID: {editable: true, type: "number"},
                                    serviceID: {editable: true, type: "number"},
                                    workDate: {editable: true, type: "string", validation: {required: false}},
                                    startTime: {editable: true, type: "date", validation: {required: false}},
                                    stopTime: {editable: true, type: "date", validation: {required: false}},
                                    timeWorked: {editable: true, type: "number"},
                                    timeWorkedText: {editable: true, type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    intTimerID: {editable: true, type: "number"},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    editTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    editEmployeeID: {editable: true, type: "number"},
                                    tasksCalls: {editable: true, type: "number"},
                                    tasksEmails: {editable: true, type: "number"},
                                    tasksVoiceMails: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "timerID", dir: "asc"},
                        requestStart: function (e) {
                            //self.writeLog('TimerOptionsView:timerDataSource:request start:');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function (e) {
                            //self.writeLog('TimerOptionsView:timerDataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);
                        },
                        error: function (e) {
                            self.writeLog('TimerOptionsView:timerDataSource:error');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerOptionsView:timerDataSource:change');

                        }
                    });

                self.timerIntTimerIdDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        batch: false,
                        pageSize: 10,
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalTimer",
                                complete: function (e) {
                                    console.log('TimeOptionsView:timerIntTimerIdDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:timerIntTimerIdDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('TimerOptionsView:update');
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.timerID + ")";
                                },
                                complete: function (e) {
                                    //console.log('TimerOptionsView:timerDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                    //self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                },
                                error: function (e) {
                                    console.log('TimerOptionsView:timerIntTimerIdDataSource:update:error:' + JSON.stringify(e.responseJSON));
                                    self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:update:error:' + JSON.stringify(e.responseJSON));

                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('TimerOptionsView:create:data:' + data);
                                    return App.config.DataServiceURL + "/odata/PortalTimer";
                                },
                                complete: function (e) {

                                    if (e.responseJSON !== undefined) {
                                        if (e.responseJSON.value[0]) {
                                            var newTimerID = e.responseJSON.value[0].timerID;
                                            //console.log('TimerOptionsView:timerDataSource:create:complete:' + newTimerID);
                                            self.currentTimerID = newTimerID;
                                            //self.serviceID =  e.responseJSON.value[0].serviceID;
                                            //self.clientID = e.responseJSON.value[0].clientID;

                                            if (self.entryType === "M") {
                                                self.updateTimerRecordWithTimerId();
                                            }
                                        }
                                        //self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:create:complete:' + JSON.stringify(e.responseJSON));
                                    } else {
                                        console.log('TimerOptionsView:timerIntTimerIdDataSource:create:complete:error:else');
                                        //self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:create:complete:error:else');

                                    }
                                },
                                error: function (e) {
                                    console.log('TimerOptionsView:timerIntTimerIdDataSource:create:error:' + JSON.stringify(e.responseJSON));
                                    self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:create:error:' + JSON.stringify(e.responseJSON));

                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    idTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    entryType: {editable: true, type: "string", validation: {required: false}},
                                    employeeID: {editable: true, type: "number"},
                                    clientID: {editable: true, type: "number"},
                                    serviceID: {editable: true, type: "number"},
                                    workDate: {editable: true, type: "string", validation: {required: false}},
                                    startTime: {editable: true, type: "date", validation: {required: false}},
                                    stopTime: {editable: true, type: "date", validation: {required: false}},
                                    timeWorked: {editable: true, type: "number"},
                                    timeWorkedText: {editable: true, type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    intTimerID: {editable: true, type: "number"},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    editTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    editEmployeeID: {editable: true, type: "number"},
                                    tasksCalls: {editable: true, type: "number"},
                                    tasksEmails: {editable: true, type: "number"},
                                    tasksVoiceMails: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "timerID", dir: "asc"},
                        requestStart: function (e) {
                            //self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:request start:');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function (e) {
                            //self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);
                        },
                        error: function (e) {
                            self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:error');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //self.writeLog('TimerOptionsView:timerIntTimerIdDataSource:change');

                        }
                    });

                // Stop time from running as soon as "Stop" is clicked
                if (self.currentTimerID === null || self.currentTimerID === undefined) {
                    self.currentTimerID = App.model.get('currentTimerID');
                }
                //self.writeLog('TimerOptionsView:updateTimerRecord:currentTimerID:' + self.currentTimerID);

                if (self.currentTimerID !== null && self.currentTimerID !== undefined) {
                    self.currentTimerID = parseInt(self.currentTimerID, 0);
                    self.timerDataSource.filter({field: "timerID", operator: "eq", value: self.currentTimerID});
                } else if (self.intTimerID !== null && self.intTimerID !== undefined) {
                    self.intTimerID = parseInt(self.intTimerID, 0);
                    self.timerDataSource.filter({field: "intTimerID", operator: "eq", value: self.intTimerID});
                    self.timerDataSource.sort({field: "timerID", dir: "desc"});
                }

                //self.writeLog('TimerOptionsView:updateTimerRecord:filter:' + self.timerDataSource.filter);

                if (self.timerDataSource.filter !== null && self.entryType === "A") {

                    //self.timerDataSource.filter({field: "timerID", operator: "eq", value: self.currentTimerID});
                    self.timerDataSource.fetch(function () {
                        var data = this.data();

                        if (data.length > 0) {

                            var timerRecord = data[0];
                            var offset = (new Date()).getTimezoneOffset();
                            self.now = new Date(moment().clone().subtract((offset), 'minutes'));

                            self.startTime = new Date(moment(timerRecord.startTime).clone().subtract((offset), 'minutes'));
                            self.stopTime = new Date(moment(timerRecord.stopTime).clone().subtract((offset), 'minutes'));

                            if (moment(self.now).isBefore(self.startTime)) {

                                var message = 'TimerOptionsView:getData:start time was before end time...this should never happen.';
                                console.log(message);
                                self.writeLog(message);

                            } else {

                                var stop = moment(self.now).clone();
                                var start = moment(self.startTime).clone();

                                var seconds = parseFloat(stop.diff(start, 'seconds'));
                                var minutes = parseFloat(stop.diff(start, 'minutes'));
                                var hours = parseFloat(stop.diff(start, 'hours'));

                                self.timeWorkedText = "";
                                self.timeWorked = 0;

                                // Round up minutes
                                if ((minutes + 1) * 60 - seconds >= 30) {
                                    minutes += 1;
                                }

                                // If less than 1 minute, make it 1 minute or 1/100th hour
                                if (minutes < 1) {
                                    self.timeWorked = 0.01;
                                    self.timeWorkedText = "0:1";
                                } else {
                                    self.timeWorked = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(seconds / 3600));
                                    self.timeWorkedText = parseInt(hours, 0).toString() + ":" + (parseInt(minutes, 0) - parseInt(hours, 0) * 60).toString();
                                }

                                var totalTimeWorked = 0;

                                self.timerSummaryDataSource.fetch(function () {
                                    var summaryData = this.data();
                                    var summaryCount = summaryData.length;
                                    var summaryRecordTimeWorked = 0;

                                    //totalTimeWorked = self.timeWorked;
                                    //console.log('TimerOptionsView:getData:self.timeWorked:' + self.timeWorked);

                                    if (summaryCount > 0) {
                                        summaryRecord = summaryData[0];
                                        if (summaryRecord.timeWorked !== null && summaryRecord.timeWorked !== undefined) {
                                            if (summaryRecord.timeWorked > 0) { //} && summaryRecord.timerAction === "none") {
                                                summaryRecordTimeWorked = summaryRecord.timeWorked;
                                                totalTimeWorked += summaryRecord.timeWorked;
                                            }
                                        }
                                    }
                                    //console.log('TimerOptionsView:getData:summaryRecord.timeWorked:' + summaryRecordTimeWorked);

                                    totalTimeWorked = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(totalTimeWorked);
                                    //console.log('TimerOptionsView:getData:totalTimeWorked:' + totalTimeWorked);

                                    self.$("#timeWorked").data("kendoMaskedTextBox").value(totalTimeWorked);
                                    self.$('#SaveButton').removeClass('disabled');
                                });
                            }
                        }

                    });
                } else if (self.timerDataSource.filter === null && self.entryType === "A") {
                    var message = "Warning (1).  Link to the current timer file was lost and the current timer file might not have have been saved.  Refresh your browser to see if a timer file is still open.";
                    alert(message);
                    self.writeLog('TimerOptionsView:getData:warning:' + message);
                }
            },
            deleteDuplicate: function (timerID) {
                var self = this;

                console.log('TimerOptionsView:deleteDuplicate:timerID:' + timerID);
                self.writeLog('TimerOptionsView:deleteDuplicate:timerID:' + timerID);

                self.timerDataSource.filter({field: "timerID", operator: "eq", value: timerID});
                self.timerDataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count > 0) {
                        var record = self.timerDataSource.at(0);
                        self.timerDataSource.remove(record);
                        this.sync();
                        self.writeLog('TimerOptionsView:deleteDuplicate:timerID:' + timerID + ' done');
                    } else {
                        self.writeLog('TimerOptionsView:deleteDuplicate:timerID:' + timerID + ' not found.');
                    }
                });

            },
            writeLog: function (text) {
                //console.log('TimerOptionsView:writeLog');

                var self = this;

                //self.timerLogDataSource.fetch(function () {

                    var offset = (new Date()).getTimezoneOffset();
                    var now = new Date(moment().clone().subtract((offset), 'minutes'));

                    var logText = "currentTimerID: " + self.currentTimerID + "\r\n";
                    logText += ":intTimerID: " + self.intTimerID + "\r\n";
                    logText += ":" + text;

                    self.timerLogDataSource.add({
                        logDate: now,
                        timerID: self.currentTimerID,
                        logText: logText
                    });

                    //console.log('TimerOptionsView:writeLog:started');
                    //$.when(this.sync()).done(function (e) {
                    //    //console.log('TimerOptionsView:writeLog:done');
                    //});
                //});
            },
            initializeServiceAndClient: function () {
                //console.log('TimerOptionsView:initializeServiceAndClient');
                var self = this;

                //self.timerStatus = App.model.get('timerStatus');
                //console.log('TimerOptionsView:initializeServiceAndClient:timerStatus:' + self.timerStatus);

                //console.log('TimerOptionsView:onTimerStatusChanged:serviceID:' + self.serviceID);
                self.serviceList = self.$("#serviceOptions").kendoDropDownList({
                    dataValueField: "serviceID",
                    dataTextField: "serviceItem",
                    dataSource: self.serviceRecords,
                    value: self.serviceID,
                    change: self.onServiceIdChanged
                });

                self.onServiceIdChanged(self.serviceList);

            },
            onClientIdChanged: function (e) {

                var self = this;

                var clientDDL = self.$("#timerClientListOptions").data("kendoDropDownList");
                var clientDataItem = null;
                if (clientDDL !== null && clientDDL !== undefined) {
                    clientDataItem = clientDDL.dataItem();
                }
                if (clientDataItem !== undefined && clientDataItem !== null) {
                    self.clientID = clientDataItem.clientID;

                    var companyName = clientDataItem.name;
                    self.companyID = clientDataItem.companyID;
                    self.taskTracking = clientDataItem.taskTracking;

                    // For LFD show extra fields
                    if (self.taskTracking === true) { //companyName.indexOf("LFD") > 0) {
                        self.$(".lincoln-fields").show();
                    } else {
                        self.$(".lincoln-fields").hide();
                    }
                }

                //console.log('TimerOptionsView:onClientIdChanged:clientD:' + self.clientID);

            },
            onServiceIdChanged: function (e) {

                var self = this;

                var serviceDDL = $("#serviceOptions").data("kendoDropDownList");
                var serviceDataItem = null;
                if (serviceDDL !== null && serviceDDL !== undefined) {
                    serviceDataItem = serviceDDL.dataItem();
                }
                if (serviceDataItem !== undefined && serviceDataItem !== null) {
                    self.serviceID = serviceDataItem.serviceID;
                    self.serviceItem = serviceDataItem.serviceItem;

                    self.recordTasksMtgs = serviceDataItem.recordTasksMtgs;
                    self.toStats = serviceDataItem.toStats;
                }

                this.myEmployeeId = App.model.get('myEmployeeId');

                if (this.myEmployeeId !== null && this.myEmployeeId !== undefined) {
                    this.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);
                }

                var serviceID = null;
                if (this.serviceID !== null) {
                    serviceID = parseInt(this.serviceID, 0);
                }
                //console.log('TimerOptionsView:onServiceIdChanged:serviceID:' + serviceID);

                if (e && e.sender) {
                    if (e.sender._selectedValue !== undefined && e.sender._selectedValue !== null) {
                        serviceID = parseInt(e.sender._selectedValue, 0);
                    }
                }
                //console.log('TimerOptionsView:onServiceIdChanged:serviceID:' + serviceID);
                //console.log('TimerOptionsView:onServiceIdChanged:myEmployeeId:' + this.myEmployeeId);

                var filter = {
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: [{field: "clientID", operator: "eq", value: 1793}]
                        },
                        {field: "status", operator: "eq", value: '4 - Active'}
                    ]
                };

                if (serviceID === 1001 || serviceID === 1013 || serviceID === null) {
                    // Scheduling and Corporate Compliance Training only show assigned clients
                    filter.filters[0].filters.push({field: "schedulerID", operator: "eq", value: this.myEmployeeId});
                } else if (serviceID === 1011) {
                    // Support Scheduling, show non-assigned clients only
                    filter.filters[0].filters.push({field: "schedulerID", operator: "neq", value: this.myEmployeeId});
                }
                else {
                    filter.filters[0].filters.push({field: "schedulerID", operator: "neq", value: -9999});
                }

                if (serviceID !== 1001 && serviceID !== 1011 && serviceID !== 1008 && serviceID !== 1014) {
                    self.hideCallsMeetings = true;
                    self.$(".support-scheduling-fields").hide();
                } else {
                    self.hideCallsMeetings = false;
                    self.$(".support-scheduling-fields").show();
                }

                self.clientNameRecords = App.model.get('clientNameRecords');

                var clientID = self.clientID; //App.model.get('currentTimerClientID');

                if (self.clientNameRecords.length === 0) {

                    var progressClientDS = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            batch: false,
                            serverSorting: true,
                            serverFiltering: true,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                    },
                                    complete: function (e) {
                                        console.log('TimeOptionsView:progressClientDS:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'TimeOptionsView:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientID",
                                    fields: {
                                        clientID: {editable: false, defaultValue: 0, type: "number"},
                                        schedulerID: {editable: false, defaultValue: 0, type: "number"},
                                        //schedulerName: {editable: true, type: "string", validation: {required: false}},
                                        companyID: {editable: false, defaultValue: 0, type: "number"},
                                        //clientName: {editable: true, type: "string", validation: {required: false}},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        clientCompany: {editable: true, type: "string", validation: {required: false}},
                                        taskTracking: {editable: true, type: "boolean"}
                                    }
                                }
                            },
                            requestStart: function () {
                                //console.log('TimerOptionsView:progressClientDS:requestStart');

                                kendo.ui.progress($("#loadingTimerWidget"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('TimerOptionsView:progressClientDS:requestEnd');
                                kendo.ui.progress($("#loadingTimerWidget"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('TimerOptionsView:progressClientDS:error');
                                kendo.ui.progress($("#loadingTimerWidget"), false);

                            },
                            sort: [
                                {field: "lastName", dir: "asc"},
                                {field: "firstName", dir: "asc"},
                                {field: "clientCompany", dir: "asc"}
                            ],
                            filter: [{field: "status", operator: "eq", value: "4 - Active"}]


                        });

                    //console.log('TimerOptionsView:onServiceIdChanged:filters:' + JSON.stringify(filter));

                    progressClientDS.fetch(function (data) {

                        self.clientNameRecords = this.data();

                        var modifiedRecords = [];
                        $.each(self.clientNameRecords, function (index, value) {

                            var record = value;
                            var newRecord = {
                                // From ProgressClient
                                clientID: record.clientID,
                                companyID: record.companyID,
                                name: record.lastName + ", " + record.firstName + ": " + record.clientCompany,
                                taskTracking: record.taskTracking
                            };

                            if (serviceID === 1001 || serviceID === 1013 || serviceID === null) {
                                // Scheduling and Corporate Compliance Training only show assigned clients
                                if (record.schedulerID === self.myEmployeeId) {
                                    modifiedRecords.push(newRecord);
                                }

                            } else if (serviceID === 1011) {
                                // Support Scheduling, show non-assigned clients only
                                if (record.schedulerID !== self.myEmployeeId) {
                                    modifiedRecords.push(newRecord);
                                }
                            }
                            else {
                                // show all
                                modifiedRecords.push(newRecord);
                            }

                        });

                        //var clientID = self.clientID;

                        //console.log('TimerOptionsView:onServiceIdChanged:clientID:' + clientID);

                        $("#timerClientListOptions").kendoDropDownList({
                            dataValueField: "clientID",
                            dataTextField: "name",
                            dataSource: modifiedRecords,
                            value: clientID,
                            change: self.onClientIdChanged
                        });

                        App.model.set('clientNameRecords', self.clientNameRecords);

                    });
                } else {

                    // get client list from cache

                    var modifiedRecords = [];
                    $.each(self.clientNameRecords, function (index, value) {

                        var record = value;
                        var newRecord = {
                            // From ProgressClient
                            clientID: record.clientID,
                            companyID: record.companyID,
                            name: record.lastName + ", " + record.firstName + ": " + record.clientCompany,
                            taskTracking: record.taskTracking
                        };

                        if (serviceID === 1001 || serviceID === 1013 || serviceID === null) {
                            // Scheduling and Corporate Compliance Training only show assigned clients
                            if (record.schedulerID === self.myEmployeeId) {
                                modifiedRecords.push(newRecord);
                            }

                        } else if (serviceID === 1011) {
                            // Support Scheduling, show non-assigned clients only
                            if (record.schedulerID !== self.myEmployeeId) {
                                modifiedRecords.push(newRecord);
                            }
                        }
                        else {
                            // show all
                            modifiedRecords.push(newRecord);
                        }

                    });

                    $("#timerClientListOptions").kendoDropDownList({
                        dataValueField: "clientID",
                        dataTextField: "name",
                        dataSource: modifiedRecords,
                        value: clientID,
                        change: self.onClientIdChanged
                    });
                }
            },
            onCloseClicked: function () {
                var self = this;
                //self.writeLog('TimerOptionsView:onCloseClicked');

                if (navigator.onLine) {
                    self.doesConnectionExist("close");

                } else {
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                }
            },
            onSaveClicked: function () {
                var self = this;
                //self.writeLog('TimerOptionsView:onSaveClicked');

                self.$('#SaveButton').addClass('disabled');
                if (navigator.onLine) {
                    self.doesConnectionExist("save");
                } else {
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                }
            },
            doesConnectionExist: function (type) {
                var self = this;
                var app = App;

                //self.writeLog('TimerOptionsView:doesConnectionExist');

                $.ajax({
                    type: "GET",
                    contentType: "application/json", //; charset=utf-8",
                    //url: App.config.DataServiceURL + "/rest/GetOpenTimers/" + today,
                    url: App.config.DataServiceURL + "/odata/PortalValue",
                    //dataType: "json",
                    success: function (result) {
                        if (type === "save") {
                            self.saveAction();
                        } else if (type === "close") {
                            if (self.entryType === "A") {
                                self.updateTimerRecord("open");
                                $('#stopButton').removeClass('disabled');
                            } else {
                                app.model.set('timerStatus', "Stopped");
                                $('#startButton').removeClass('disabled');
                                $('#manualButton').removeClass('disabled');
                            }

                            self.close();
                        }
                    },
                    error: function (result) {
                        alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                    }
                });
            },
            saveAction: function () {
                var self = this;
                //self.writeLog('TimerOptionsView:saveAction');

                this.saveStatus = self.$("#SaveButton").html();

                if (this.saveStatus !== "Finished") {
                    self.createAlert(this.$("#stopWarning"), "<strong>Important</strong> Please review your Timer Session Summary carefully. If any information is incorrect, you may edit this timer session. If all the information is correct, please click the finished button below.   You must click finished, or this Timer Session will remain open.");
                    self.$("#SaveButton").html("Finished");
                    if (this.entryType === "M") {
                        App.model.set('timerStatus', "Manual");
                        self.$('#SaveButton').removeClass('disabled');
                    }
                    this.updateTimerRecord("review");
                } else {

                    // Validate entries
                    var meetings = self.$("#meetings").val();
                    var tasks = self.$("#tasks").val();
                    var error = "";

                    if ($.isNumeric(tasks) !== true && !self.hideCallsMeetings) {
                        error = "Calls/Tasks value must be numeric.  Please populate and re-submit";
                        self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                        self.$('#SaveButton').removeClass('disabled');
                    } else if ($.isNumeric(meetings) !== true && !self.hideCallsMeetings) {
                        error = "Meetings value must be numeric.  Please populate and re-submit";
                        self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                        self.$('#SaveButton').removeClass('disabled');
                    } else if (this.entryType === "A") {
                        if (self.taskTracking === true && !self.hideCallsMeetings) { //self.companyID === 12
                            var tasksCalls = self.$("#tasksCalls").val();
                            var tasksVoiceMails = self.$("#tasksVoiceMails").val();
                            var tasksEmails = self.$("#tasksEmails").val();

                            if ($.isNumeric(tasksCalls) !== true) {
                                error = "Calls value must be numeric.  Please populate and re-submit";
                                self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                                $('#startButton').removeClass('disabled');
                                $('#manualButton').removeClass('disabled');
                                self.$('#SaveButton').removeClass('disabled');
                                return;
                            } else if ($.isNumeric(tasksVoiceMails) !== true) {
                                error = "Voice Mails value must be numeric.  Please populate and re-submit";
                                self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                                $('#startButton').removeClass('disabled');
                                $('#manualButton').removeClass('disabled');
                                self.$('#SaveButton').removeClass('disabled');
                                return;
                            } else if ($.isNumeric(tasksEmails) !== true) {
                                error = "Emails value must be numeric.  Please populate and re-submit";
                                self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                                $('#startButton').removeClass('disabled');
                                $('#manualButton').removeClass('disabled');
                                self.$('#SaveButton').removeClass('disabled');
                                return;
                            }
                        }
                        // When finish has already been chosen, this will close the timer record
                        this.updateTimerRecord("stop");
                    } else if (this.entryType === "M") {
                        var timeWorked = self.$("#timeWorked").val();
                        if ($.isNumeric(timeWorked) !== true) {
                            error = "Time Worked value must be numeric.  Please populate and re-submit";
                            self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                        } else {
                            self.$('#SaveButton').addClass('disabled');
                            this.createManualTimerRecord();
                        }
                    }
                }
            },
            createAlert: function (selector, html) {
                //console.log('TimerOptionsView:createAlert');

                var self = this;

                self.$(selector).html(html);
                self.$(selector).css("opacity", 1);
                self.$(selector).css("display", "block");
                var alert = self.$(selector).addClass("alert");

            },
            createManualTimerRecord: function () {
                var self = this;
                var app = App;

                //self.writeLog('TimerOptionsView:createManualTimerRecord');

                this.meetings = self.$("#meetings").val();
                this.tasks = self.$("#tasks").val();
                this.timerNote = self.$("#timerNote").val();
                //this.saveStatus = self.$("#SaveButton").html();

                var tasksCalls = null;
                var tasksVoiceMails = null;
                var tasksEmails = null;

                // Special fields for LFD (Lincoln)
                if (self.taskTracking === true && !self.hideCallsMeetings) { //this.companyID === 12
                    tasksCalls = self.$("#tasksCalls").val();
                    tasksVoiceMails = self.$("#tasksVoiceMails").val();
                    tasksEmails = self.$("#tasksEmails").val();

                    if ($.isNumeric(tasksCalls) !== true) {
                        error = "Calls value must be numeric.  Please populate and re-submit";
                        self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                        $('#startButton').removeClass('disabled');
                        $('#manualButton').removeClass('disabled');
                        return;
                    } else if ($.isNumeric(tasksVoiceMails) !== true) {
                        error = "Voice Mails value must be numeric.  Please populate and re-submit";
                        self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                        $('#startButton').removeClass('disabled');
                        $('#manualButton').removeClass('disabled');
                        return;
                    } else if ($.isNumeric(tasksEmails) !== true) {
                        error = "Emails value must be numeric.  Please populate and re-submit";
                        self.createAlert(this.$("#stopWarning"), "<strong>Warning.</strong> " + error + ".");
                        $('#startButton').removeClass('disabled');
                        $('#manualButton').removeClass('disabled');
                        return;
                    }
                }

                if ($.isNumeric(self.meetings) !== true) {
                    self.meetings = 0;
                }
                if ($.isNumeric(self.tasks) !== true) {
                    self.tasks = 0;
                }

                if (self.serviceID === null || self.serviceID === undefined) {
                    self.serviceID = self.options.serviceID;
                }
                if (self.clientID === null || self.clientID === undefined) {
                    self.clientID = self.options.clientID;
                }

                if (self.clientID === null || self.clientID === undefined) {
                    self.clientID = self.options.clientID;
                }

                var offset = (new Date()).getTimezoneOffset();
                var now = new Date(moment().clone().subtract((offset), 'minutes'));

                var workDate = self.$("#workDate").val();
                if (workDate !== null && workDate !== "") {
                    workDate = moment(new Date(workDate)).clone().format('YYYY-MM-DD');
                } else {
                    workDate = null;
                }

                var timeWorked = self.$("#timeWorked").val();

                self.timerDataSource.fetch(function () {

                    self.timerDataSource.add({
                        idTimeStamp: now,
                        entryType: "M",
                        employeeID: self.myEmployeeId,
                        clientID: self.clientID,
                        serviceID: self.serviceID,
                        workDate: workDate,
                        recordType: "NORM-STOP",
                        timerAction: "none",
                        timeWorked: timeWorked,
                        tasksCalls: tasksCalls,
                        tasksVoiceMails: tasksVoiceMails,
                        tasksEmails: tasksEmails,
                        tasks: self.tasks,
                        meetings: self.meetings,
                        timerNote: self.timerNote
                    });

                    $.when(this.sync()).done(function (e) {
                        //self.writeLog('TimerOptionsView:manual submission done');

                        //self.resetStatsDashboardCache();
                        //app.model.set('timerStatus', "Stopped");

                        // Check for existing Stats (tblProgressData) entry.
                        // There can be only one stats entry per client per day
                        // In order for the Stats views to calc properly.
                        self.populateProgressData();
                    });
                });


            },
            updateTimerRecord: function (timerAction) {

                // This function runs when timer is stopped
                var self = this;
                //var app = App;

                if (timerAction === "stop") {
                    self.$('#SaveButton').addClass('disabled');
                }

                if (self.timerDataSource.filter !== null && self.entryType === "A") {

                    //self.timerDataSource.filter({field: "timerID", operator: "eq", value: self.currentTimerID});
                    self.timerDataSource.fetch(function () {
                        var data = this.data();

                        //self.writeLog('TimerOptionsView:updateTimerRecord:getData:data:all records:' + JSON.stringify(data));
                        if (data.length > 0) {

                            self.meetings = self.$("#meetings").val();
                            self.tasks = self.$("#tasks").val();
                            self.timerNote = self.$("#timerNote").val();
                            //self.saveStatus = self.$("#SaveButton").html();

                            var tasksCalls = null;
                            var tasksVoiceMails = null;
                            var tasksEmails = null;

                            // Special fields for LFD (Lincoln)
                            if (self.taskTracking === true) { //this.companyID === 12
                                tasksCalls = self.$("#tasksCalls").val();
                                tasksVoiceMails = self.$("#tasksVoiceMails").val();
                                tasksEmails = self.$("#tasksEmails").val();
                            }

                            var timerRecord = data[0]; //self.timerDataSource.at(0);
                            var offset = (new Date()).getTimezoneOffset();

                            if (self.now === null || self.now === undefined)
                                self.now = new Date(moment().clone().subtract((offset), 'minutes'));

                            if (self.startTime === null || self.startTime === undefined)
                                self.startTime = new Date(moment(timerRecord.startTime).clone().subtract((offset), 'minutes'));

                            //if (self.stopTime === null || self.stopTime === undefined)
                            self.stopTime = new Date(moment(timerRecord.stopTime).clone().subtract((offset), 'minutes'));

                            if (moment(self.now).isBefore(self.startTime)) {

                                var message = 'TimerOptionsView:updateTimerRecord:start time was before end time.  Probably timer edited in different time zones.';
                                console.log(message);
                                //self.writeLog(message);
                            }

                            timerRecord.set("startTime", self.startTime);
                            timerRecord.set("idTimeStamp", self.startTime);

                            //self.writeLog('TimerOptionsView:updateTimerRecord:timerDataSource:timerAction:' + timerAction);

                            if (timerAction === "review") {
                                //timerRecord.set("timeWorkedText", self.timeWorkedText);
                                //timerRecord.set("timeWorked", self.timeWorked);
                                timerRecord.set("stopTime", self.now);
                                timerRecord.set("timerAction", 'review');

                                //self.writeLog('TimerOptionsView:updateTimerRecord:finish:' + self.currentTimerID + ":done");
                            } else if (timerAction === "stop") {

                                timerRecord.set("stopTime", self.stopTime);
                                timerRecord.set("timerAction", 'none');

                                timerRecord.set("serviceID", self.serviceID);
                                timerRecord.set("clientID", self.clientID);
                                timerRecord.set("meetings", self.meetings);
                                timerRecord.set("tasks", self.tasks);
                                timerRecord.set("tasksCalls", tasksCalls);
                                timerRecord.set("tasksEmails", tasksEmails);
                                timerRecord.set("tasksVoiceMails", tasksVoiceMails);
                                timerRecord.set("timerNote", self.timerNote);

                                if (timerRecord.recordType.indexOf("STOP") < 0) {
                                    timerRecord.set("recordType", timerRecord.recordType + '-STOP');
                                }

                            } else if (timerAction === "open") {
                                timerRecord.set("stopTime", new Date(0));
                                timerRecord.set("timerAction", 'stop');

                                timerRecord.set("timeWorkedText", "");
                                timerRecord.set("timeWorked", 0);
                                App.model.set('timerStatus', "Open");
                            }

                            $.when(self.timerDataSource.sync()).done(function (e) {
                                if (timerAction === "stop") {

                                    // if serviceID or clientID for interrupted record has changed, need to
                                    // go back and edit the completed initial timer record to accurately reflect the
                                    // latest choice for serviceID and clientID
                                    self.populateClientAndService(timerRecord.timerID);

                                    //self.populateProgressData();

                                } else if (timerAction === "review") {

                                    // This will make the "Finish" button available after review update has finished for "Submit"
                                    self.$('#SaveButton').removeClass('disabled');
                                }

                            });

                        }

                    });
                } else if (self.timerDataSource.filter === null && self.entryType === "A") {
                    var message = "Warning (2).  Link to the current timer file was lost and the current timer file might not have have been saved.  Refresh your browser to see if a timer file is still open.";
                    alert(message);
                    self.writeLog('TimerOptionsView:getData:warning:' + message);
                }
                //else {
                //    var message3 = "Warning (3).  Link to the current timer file was lost and the current timer file might not have have been saved.  Refresh your browser to see if a timer file is still open.";
                //    alert(message3);
                //    self.writeLog('TimerOptionsView:getData:warning:' + message3);
                //}
            },
            populateClientAndService: function (timerID) {

                var self = this;
                //self.writeLog('TimerOptionsView:populateClientAndService:intTimerID' + self.intTimerID);
                //self.writeLog('TimerOptionsView:populateClientAndService:timerID' + timerID);

                self.timerIntTimerIdDataSource.filter({field: "intTimerID", operator: "eq", value: self.intTimerID});
                self.timerIntTimerIdDataSource.fetch(function () {
                    var data = this.data();

                    var editedServiceOrClient = false;
                    // if serviceID or clientID for interrupted record has changed, need to
                    // go back and edit the completed initial timer record to accurately reflect the
                    // latest choice for serviceID and clientID

                    if (data.length > 1) {
                        //console.log('TimerOptionsView:populateClientAndService:number of records:' + data.length);
                        $.each(data, function (index) {
                            var record = data[index];
                            //console.log('TimerOptionsView:populateClientAndService:record:' + index);
                            //console.log('TimerOptionsView:populateClientAndService:serviceID:' + self.serviceID);
                            //console.log('TimerOptionsView:populateClientAndService:clientID:' + self.clientID);

                            // Don't edit the last record, the record just closed
                            if (record.timerID !== timerID && (record.serviceID !== self.serviceID || record.clientID !== self.clientID)) {
                                editedServiceOrClient = true;

                                var offset = (new Date()).getTimezoneOffset();
                                var startTime = new Date(moment(record.startTime).clone().subtract((offset), 'minutes'));
                                var stopTime = new Date(moment(record.stopTime).clone().subtract((offset), 'minutes'));

                                record.set("startTime", startTime);
                                record.set("idTimeStamp", startTime);
                                record.set("stopTime", stopTime);

                                record.set("serviceID", self.serviceID);
                                record.set("clientID", self.clientID);

                                // Sanity check changes
                                record.set("timerNote", record.timerNote + ".  Auto changed client/service ID.");
                                record.set("timerAction", 'none');
                                if (record.recordType.indexOf("STOP") < 0) {
                                    record.set("recordType", record.recordType + '-STOP');
                                }

                                //self.writeLog('TimerOptionsView:populateClientAndService:record:' + JSON.stringify(record));

                            }

                        });
                        if (editedServiceOrClient === true) {
                            $.when(this.sync()).done(function (e) {
                                self.populateProgressData();
                            });
                        } else {
                            self.populateProgressData();
                        }

                    } else {
                        self.populateProgressData();
                    }

                });
            },
            updateTimerRecordWithTimerId: function () {

                var self = this;
                //self.writeLog('TimerOptionsView:updateTimerRecordWithTimerId:Manual records only');

                if (self.currentTimerID !== null) {
                    self.currentTimerID = parseInt(self.currentTimerID, 0);
                    self.timerDataSource.filter({field: "timerID", operator: "eq", value: self.currentTimerID});
                    self.timerDataSource.fetch(function () {
                        var data = this.data();

                        if (data.length > 0) {

                            var timerRecord = self.timerDataSource.at(0);
                            timerRecord.set("intTimerID", self.currentTimerID);
                            self.timerDataSource.sync();
                        }
                    });
                } else {
                    //self.writeLog('TimerOptionsView:updateTimerRecordWithTimerID:currentTimerID is null');
                }
            },
            getServiceRecord: function (serviceID) {
                //console.log('TimerOptionsView:getServiceRecord:serviceID:' + serviceID);

                var self = this;

                var service = $.grep(self.serviceRecords, function (element, index) {
                    return element.serviceID == serviceID;
                });

                if (service.length > 0) {
                    return service[0];
                } else {
                    return null;
                }

            },
            populateProgressDataNew: function () {

                var self = this;
                console.log('TimerOptionsView:populateProgressData');

                var workStartDate = moment(self.startTime).clone().format('YYYY-MM-DD');
                var workCurrentDate = moment(self.$("#workDate").val()).clone().format('YYYY-MM-DD');

                var today = new Date(moment(self.$("#workDate").val()).clone());
                this.year = today.getFullYear();
                this.month = today.getMonth() + 1;
                this.day = today.getDate();

                this.restDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        transport: {
                            read: function (options) {
                                //self.dataSource.cancelled = true;
                                $.ajax({
                                    type: "GET",  //PATCH?
                                    contentType: "application/json", //; charset=utf-8",
                                    url: App.config.DataServiceURL + "/rest/GetCalcClientStats/" + self.clientID + "/" + workStartDate + "/" + workCurrentDate,
                                    dataType: "json",
                                    success: function (result) {
                                        console.log('TimerOptionsView:restDataSource:success');
                                        App.model.set('currentTimerServiceID', null);
                                        self.resetStatsDashboardCache();
                                        App.model.set('timerStatus', "Stopped");
                                        self.close();
                                    },
                                    error: function (result) {
                                        // notify the data source that the request failed
                                        console.log('TimerOptionsView:restDataSource:error');
                                    }
                                });
                            }
                        }
                    });

                self.restDataSource.fetch();

            },
            populateProgressData: function () {

                var self = this;
                var app = App;

                console.log('TimerOptionsView:populateProgressData');

                var timeColumn = "schedulerTime";
                var tasksColumn = "schedulerCalls";
                var meetingsColumn = "schedulerMeetings";

                // Choose columns to populate based on serviceID
                if (self.serviceID === 1001) { // Scheduling
                    // no change
                } else if (self.serviceID === 1011) { // Support Scheduling
                    timeColumn = "support1Time";
                    tasksColumn = "support1Calls";
                    meetingsColumn = "support1Meetings";
                } else if (self.serviceID === 1008) { // Comp Time
                    timeColumn = "compTime";
                    tasksColumn = "compCalls";
                    meetingsColumn = "compMeetings";
                } else if (self.serviceID === 1014) { // Referral Time
                    timeColumn = "referralTime";
                    tasksColumn = "referralCalls";
                    meetingsColumn = "referralMeetings";
                    
                } else { //Other
                    // no change
                }

                // First get any existing progress data records
                // workDate should be the date associated with the start time, not the end time (11/1/18)
                //var workDate = moment(self.startTime).clone().format('YYYY-MM-DD');
                var workDate = moment(self.$("#workDate").val()).clone().format('YYYY-MM-DD');
                var offset = (new Date()).getTimezoneOffset();

                var workDateStart = new Date(moment(workDate + "T00:00:00+0000").clone()); //.add((offset), 'minutes'));
                var workDateEnd = new Date(moment(workDate + "T23:59:59+0000").clone()); //.add((offset), 'minutes'));

                var now = new Date(moment().clone().subtract((offset), 'minutes'));

                this.progressDataSource = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressData";
                                },
                                complete: function (e) {
                                    console.log('TimeOptionsView:progressDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:progressDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('TimerOptionsView:update');
                                    return App.config.DataServiceURL + "/odata/ProgressData" + "(" + data.entryID + ")";
                                },
                                complete: function (e) {
                                    //console.log('TimerOptionsView:progressDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                },
                                error: function (e) {
                                    console.log('TimerOptionsView:progressDataSource:update:error:' + JSON.stringify(e.responseJSON));
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('TimerOptionsView:create:data:' + data);
                                    return App.config.DataServiceURL + "/odata/ProgressData";
                                },
                                complete: function (e) {
                                    console.log('TimeOptionsView:progressDataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:progressDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "entryID",
                                fields: {
                                    entryID: {editable: false, defaultValue: 0, type: "number"},
                                    workDate: {editable: true, type: "date", validation: {required: false}},
                                    timeStamp: {editable: true, type: "date", validation: {required: false}},
                                    schedulerTime: {type: "number"},
                                    support1Time: {type: "number"},
                                    adminTime: {type: "number"},
                                    schedulerCalls: {type: "number"},
                                    support1Calls: {type: "number"},
                                    schedulerMeetings: {type: "number"},
                                    support1Meetings: {type: "number"},
                                    compTime: {type: "number"},
                                    compCalls: {type: "number"},
                                    compMeetings: {type: "number"},
                                    referralTime: {type: "number"},
                                    referralCalls: {type: "number"},
                                    referralMeetings: {type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('  TimerOptionsView:progressDataSource:requestStart');

                            //kendo.ui.progress(self.$("#loadingSummaryStats"), true);
                        },
                        requestEnd: function () {

                            //console.log('   TimerOptionsView:progressDataSource:requestEnd');
                            //kendo.ui.progress(self.$("#loadingSummaryStats"), false);

                        },
                        error: function () {

                            //console.log('  TimerOptionsView:progressDataSource:error');
                            //kendo.ui.progress(self.$("#loadingSummaryStats"), false);

                        },

                        filter: [{field: "clientID", operator: "eq", value: self.clientID},
                            {field: "workDate", operator: "gte", value: workDateStart},
                            {field: "workDate", operator: "lte", value: workDateEnd}]
                    });

                // workDate associated with start day not necessarily current day
                //var today = new Date(moment(self.startTime).clone());
                var today = new Date(moment(self.$("#workDate").val()).clone());
                this.year = today.getFullYear();
                this.month = today.getMonth() + 1;
                this.day = today.getDate();

                this.timerClientTotalDataSource = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalTimerTotalClientTime";
                                },
                                complete: function (e) {
                                    console.log('TimeOptionsView:timerClientTotalDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeOptionsView:timerClientTotalDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "rowID",
                                fields: {
                                    rowID: {editable: false, defaultValue: 0, type: "number"},
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    workDate: {editable: true, type: "date", validation: {required: false}},
                                    totalTimeWorked: {editable: false, defaultValue: 0, type: "number"},
                                    totalTasks: {editable: false, defaultValue: 0, type: "number"},
                                    totalMeetings: {editable: false, defaultValue: 0, type: "number"},
                                    workYear: {editable: false, defaultValue: 0, type: "number"},
                                    workMonth: {editable: false, defaultValue: 0, type: "number"},
                                    workDay: {editable: false, defaultValue: 0, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   TimerOptionsView:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    TimerOptionsView:requestEnd');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), false);


                        },
                        filter: [
                            {field: "workYear", operator: "eq", value: self.year},
                            {field: "workMonth", operator: "eq", value: self.month},
                            {field: "workDay", operator: "eq", value: self.day},
                            {field: "clientID", operator: "eq", value: self.clientID}
                        ]
                    });

                this.progressDataSource.fetch(function () {

                    var progressData = this.data();

                    self.timerClientTotalDataSource.fetch(function () {

                            var timerClientTotalData = this.data();

                            // Existing timer
                            var sumTime = 0;
                            var sumTasks = 0;
                            var sumMeetings = 0;
                            var sumAdminTime = 0;
                            if (timerClientTotalData.length > 0) {
                                $.each(timerClientTotalData, function (index) {
                                    var record = timerClientTotalData[index];

                                    var totalTimeWorked = record.totalTimeWorked; //app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(record.totalTimeWorked);

                                    // Admin? WHERE	toStats = 1 AND recordTasksMtgs = 0 AND serviceID <> 1013
                                    //if (self.toStats === true && self.recordTasksMtgs === false && record.serviceID !== 1013 && record.serviceID !== 1001) {
                                    var serviceRecord = self.getServiceRecord(record.serviceID);
                                    //serviceRecord.toStats === false &&
                                    if (serviceRecord.recordTasksMtgs === false && record.serviceID !== 1013 && record.serviceID !== 1001) {
                                        sumAdminTime += totalTimeWorked;
                                    } else {
                                        if (record.serviceID === self.serviceID) {
                                            sumTime += totalTimeWorked;
                                            sumTasks += record.totalTasks;
                                            sumMeetings += record.totalMeetings;
                                        }
                                    }
                                });
                            }

                            sumTime = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTime);

                            if (progressData.length > 0) {

                                var progressDataRecord = progressData[0];
                                //Do we record tasks and meetings?
                                //Service ID 1013 is an exception (Corporate Compliance Training).
                                //It does not record stats, but gets counted toward schedulerTime.
                                //See default case above for columns.

                                var workDate = new Date(moment(progressDataRecord.workDate).clone().subtract((offset), 'minutes'));
                                var timeStamp = new Date(moment(progressDataRecord.timeStamp).clone().subtract((offset), 'minutes'));

                                progressDataRecord.set("workDate", workDate);
                                progressDataRecord.set("timeStamp", timeStamp);

                                if (self.recordTasksMtgs === true || self.serviceID === 1013 || self.serviceID === 1001) {
                                    progressDataRecord.set(timeColumn, sumTime);
                                    progressDataRecord.set(tasksColumn, sumTasks);
                                    progressDataRecord.set(meetingsColumn, sumMeetings);
                                }
                                else {
                                    progressDataRecord.set("adminTime", sumAdminTime);
                                }

                            } else {
                                // Create new progressData record
                                if (self.recordTasksMtgs === true || self.serviceID === 1013 || self.serviceID === 1001) {

                                    var newRecord = {
                                        clientID: self.clientID,
                                        timeStamp: now,
                                        workDate: workDateStart,
                                        schedulerID: 0,
                                        schedulerTime: 0,
                                        schedulerCalls: 0,
                                        schedulerMeetings: 0,
                                        support1ID: 0,
                                        support1Time: 0,
                                        support1Calls: 0,
                                        support1Meetings: 0,
                                        support2ID: 0,
                                        support2Time: 0,
                                        support2Calls: 0,
                                        support2Meetings: 0,
                                        compID: 0,
                                        compTime: 0,
                                        compCalls: 0,
                                        compMeetings: 0,
                                        referralTime: 0,
                                        referralCalls: 0,
                                        referralMeetings: 0,
                                        adminTime: 0
                                    };

                                    newRecord[timeColumn] = sumTime;
                                    newRecord[tasksColumn] = sumTasks;
                                    newRecord[meetingsColumn] = sumMeetings;

                                    self.progressDataSource.add(newRecord);

                                } else {

                                    self.progressDataSource.add({
                                        clientID: self.clientID,
                                        timeStamp: now,
                                        workDate: workDateStart,
                                        schedulerID: 0,
                                        schedulerTime: 0,
                                        schedulerCalls: 0,
                                        schedulerMeetings: 0,
                                        support1ID: 0,
                                        support1Time: 0,
                                        support1Calls: 0,
                                        support1Meetings: 0,
                                        support2ID: 0,
                                        support2Time: 0,
                                        support2Calls: 0,
                                        support2Meetings: 0,
                                        compID: 0,
                                        compTime: 0,
                                        compCalls: 0,
                                        compMeetings: 0,
                                        referralTime: 0,
                                        referralCalls: 0,
                                        referralMeetings: 0,
                                        adminTime: sumAdminTime
                                    });
                                }
                            }

                            $.when(self.progressDataSource.sync()).done(function (e) {
                                App.model.set('currentTimerServiceID', null);
                                self.resetStatsDashboardCache();
                                App.model.set('timerStatus', "Stopped");
                                self.close();
                            });
                        }
                    );
                });

            },
            resetStatsDashboardCache: function () {

                //console.log('TimerOptionsView:resetStatsDashboardCache');

                $.cookie('ESA:AppModel:mySumTotalTime', "");
                App.model.set('mySumTotalTime', null);

                $.cookie('ESA:AppModel:mySumTotalMeetings', "");
                App.model.set('mySumTotalMeetings', null);

                $.cookie('ESA:AppModel:mySumMeetingsTarget', "");
                App.model.set('mySumMeetingsTarget', null);

                $.cookie('ESA:AppModel:myMtgPercent', "");
                App.model.set('myMtgPercent', null);

                $.cookie('ESA:AppModel:teamSumTotalTime', "");
                App.model.set('teamSumTotalTime', null);

                $.cookie('ESA:AppModel:teamSumTotalMeetings', "");
                App.model.set('teamSumTotalMeetings', null);

                $.cookie('ESA:AppModel:teamSumMeetingsTarget', "");
                App.model.set('teamSumMeetingsTarget', null);

                $.cookie('ESA:AppModel:teamMtgPercent', "");
                App.model.set('teamMtgPercent', null);
            },
            onResize: function () {
//                console.log('TimerOptionsView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                console.log('TimerOptionsView:resize');


            },
            remove: function () {
                console.log('---------- TimerOptionsView:remove ----------');
                //this.writeLog('---------- TimerOptionsView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                //$('click').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });