define(['App', 'backbone', 'marionette', 'jquery', 'models/ScoreBoardDetailModel', 'hbs!templates/scoreBoardDetailCollection',
        'views/ScoreBoardDetailView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, ScoreBoardDetailModel, template,
              ScoreBoardDetailView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: ScoreBoardDetailView,
            initialize: function (options) {
                //console.log('ScoreBoardDetailCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.scoreBoardDetails = [];

                var self = this;
                var app = App;

                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;

                this.widget3Value =  App.model.get('widget3Value');

                this.getData();

            },
            onShow: function () {
                //console.log("-------- ScoreBoardDetailCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;
                var app = App;

                this.scoreBoardDataSource = new kendo.data.DataSource(
                    {
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalScoreBoard";
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:update');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoard" + "(" + data.year + ")";
                            }
                        },
                        create: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:create');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoard";
                            }
                            //dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:destroy');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoard" + "(" + data.year + ")";
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        //{
                        // "year":2016,
                        // "month":1,
                        // "added":13,
                        // "lost":2,
                        // "growth":"1.80"
                        //}
                        model: {
                            id: "year",
                            fields: {
                                year: {editable: false, defaultValue: 0, type: "number"},
                                month: {type: "number"},
                                added: {type: "number"},
                                lost: {type: "number"},
                                growth: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   AdminScoreboardView:requestStart');

                        kendo.ui.progress(self.$("#loadingAdmin"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    AdminScoreboardView:requestEnd');
                        kendo.ui.progress(self.$("#loadingAdmin"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   AdminScoreboardView:error');
                        kendo.ui.progress(self.$("#loadingAdmin"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   AdminScoreboardView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: { field: "year", operator: "eq", value: self.year},
                    sort: {field: "month", dir: "asc"}
                });

                this.scoreBoardDataSource.fetch(function () {

                    var data = this.data();
                    var scoreBoardDetailCount = data.length;

                    $.each(data, function (index, value) {

                        self.scoreBoardDetails.push({
                            scoreBoardDetailId: value.month + value.year,
                            month: value.month,
                            year: value.year,
                            added: value.added,
                            lost: value.lost,
                            growth: value.growth
                        });
                    });

                    // Populate scoreBoardDetail collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return model.get("scoreBoardDetailId");
                    };


                    self.totalAdded = 0;
                    self.totalLost = 0;
                    self.averageGrowth = 0;

                    $.each(self.scoreBoardDetails, function (index, value) {

                        //console.log('ScoreBoardDetailCollectionView:note:value:' + JSON.stringify(value));
                        var model = new ScoreBoardDetailModel();

                        model.set("scoreBoardDetailId", value.scoreBoardDetailId);
                        model.set("month", value.month);
                        model.set("year", value.year);
                        model.set("added", value.added);
                        model.set("lost", value.lost);
                        model.set("growth", value.growth);

                        self.collection.push(model);

                        self.totalAdded += value.added;
                        self.totalLost += value.lost;
                        self.averageGrowth += value.growth;
                    });

                    var model = new ScoreBoardDetailModel();

                    model.set("scoreBoardDetailId", 9999);
                    model.set("month", 13);
                    model.set("year", self.year);
                    model.set("added", self.totalAdded);
                    model.set("lost", self.totalLost);
                    model.set("growth", self.widget3Value);
                    //app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(self.averageGrowth/12)

                    self.collection.push(model);

                });

            },
            hideNoDataOverlay: function () {
                //console.log('ScoreBoardDetailCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#scoreBoardDetailWidgetContent').css('display', 'block');
            },
            displayNoDataOverlay: function () {
                //console.log('ScoreBoardDetailCollectionView:displayNoDataOverlay');

                // Hide the widget content
                //this.$('#scoreBoardDetailWidgetContent').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message" style="height: 120px;">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {
                //console.log('ScoreBoardDetailCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ScoreBoardDetailCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- ScoreBoardDetailCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
