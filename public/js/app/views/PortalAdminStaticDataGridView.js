define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalAdminStaticDataGrid',
        //'hbs!templates/editPortalAdminStaticDataPopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data','kendo/kendo.upload'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalAdminStaticDataGridView:initialize');

                _.bindAll(this);

                var self = this;

                self.categories = [
                    {text: 'ESA Forms', value: 9},
                    {text: 'Federal Rights and Postings', value: 10},
                    {text: 'State Rights and Postings', value: 11}
                ];

                // Arizona, California, Colorado, Florida, Georgia, Idaho, Indiana,
                // Kansas, Massachusetts, Michigan, Missouri, Nevada, New Jersey,
                // New York, North Carolina, Ohio, Pennsylvania, South Carolina,
                // Texas
                // Added Wisconsin NN 5/18/23
                self.subCategories = [
                    {text: 'N/A', value: -1},
                    {text: 'Arizona', value: 0},
                    {text: 'California', value: 1},
                    //{text: 'Colorado', value: 2},
                    {text: 'Florida', value: 3},
                    {text: 'Georgia', value: 4},
                    {text: 'Idaho', value: 5},
                    {text: 'Kansas', value: 6},
                    {text: 'Massachusetts', value: 7},
                    {text: 'Michigan', value: 8},
                    {text: 'Missouri', value: 9},
                    {text: 'Nevada', value: 10},
                    {text: 'New Jersey', value: 11},
                    {text: 'New York', value: 12},
                    {text: 'North Carolina', value: 13},
                    {text: 'Ohio', value: 14},
                    {text: 'Pennsylvania', value: 15},
                    {text: 'South Carolina', value: 16},
                    {text: 'Texas', value: 17},
                    {text: 'Wisconsin', value: 18}
                ];

                this.adminStaticGridFilter = App.model.get('adminStaticGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        autoSync: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                complete: function (e) {
                                    console.log('PortalAdminStaticDataGridView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminStaticDataGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalAdminStaticDataGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminStaticDataGridView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminStaticDataGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalAdminStaticDataGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminStaticDataGridView:dataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminStaticDataGridView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalAdminStaticDataGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminStaticDataGridView:dataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminStaticDataGridView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    title: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Document Name Here)"
                                    },
                                    text: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Document Description Here)"
                                    },
                                    text2: {editable: true, type: "string"},
                                    shortText: {editable: true, type: "string"},
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {editable: true, type: "string"},
                                    category: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:9
                                    },
                                    subCategory: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:-1
                                    },
                                    priority: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:1
                                    },
                                    showAsCompanyMessage: {editable: true, type: "boolean"},
                                    addUserImage: {editable: true, type: "boolean"},
                                    timeStamp: {
                                        editable: true,
                                        type: "date",
                                        defaultValue: function () {
                                            var offset = (new Date()).getTimezoneOffset();
                                            var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                            return now;
                                        }
                                    }
                                }
                            }
                        },
                        sort: [
                            {field: "ID", dir: "desc"}
                        ],
                        requestStart: function (e) {
                            //console.log('PortalAdminStaticDataGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#adminStaticDataLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalAdminStaticDataGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#adminStaticDataLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalAdminStaticDataGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalAdminStaticDataGridView:dataSource:error');
                            kendo.ui.progress(self.$("#adminStaticDataLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalAdminStaticDataGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#client').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onRender: function () {
                //console.log('PortalAdminStaticDataGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminStaticDataGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#adminStaticData").kendoGrid({

                    toolbar: ["create"],
                    editable: true,
                    selectable: "cell",
                    navigatable: true,
                    sortable: true,
                    extra: false,
                    filterable: {
                        mode: "row"
                    },
                    resizable: true,
                    reorderable: true,
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.ID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "category",
                            title: "Type",
                            width: 150,
                            template: function (e) {
                               return self.getCategoryText(e.category);
                            },
                            filterable: false,
                            sortable: true,
                            editor: function(container, options) {
                                var input = $("<input/>");
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoDropDownList({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: [
                                        {text: 'ESA Forms', value: 9},
                                        {text: 'Federal Rights and Postings', value: 10},
                                        {text: 'State Rights and Postings', value: 11}
                                    ]
                                });
                            }
                        },
                        {
                            field: "subCategory",
                            title: "State",
                            width: 165,
                            template: function (e) {
                                if (e.category !== 11) {
                                    e.subCategory = -1;
                                }
                                return self.getSubCategoryText(e.subCategory);
                            },
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.subCategoryFilter
                                }
                            },
                            editor: function(container, e) {
                                if (e.model.category === 11) {
                                    var input = $("<input/>");
                                    input.attr("name", e.field);
                                    input.appendTo(container);
                                    input.kendoDropDownList({
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        dataSource: [
                                            {text: 'N/A', value: -1},
                                            {text: 'Arizona', value: 0},
                                            {text: 'California', value: 1},
                                            //{text: 'Colorado', value: 2},
                                            {text: 'Florida', value: 3},
                                            {text: 'Georgia', value: 4},
                                            {text: 'Idaho', value: 5},
                                            {text: 'Kansas', value: 6},
                                            {text: 'Massachusetts', value: 7},
                                            {text: 'Michigan', value: 8},
                                            {text: 'Missouri', value: 9},
                                            {text: 'Nevada', value: 10},
                                            {text: 'New Jersey', value: 11},
                                            {text: 'New York', value: 12},
                                            {text: 'North Carolina', value: 13},
                                            {text: 'Ohio', value: 14},
                                            {text: 'Pennsylvania', value: 15},
                                            {text: 'South Carolina', value: 16},
                                            {text: 'Texas', value: 17},
                                            {text: 'Wisconsin', value: 18}
                                        ]
                                    });
                                }
                            }
                        },
                        {
                            field: "title",
                            title: "Title",
                            width: 150,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },
                        //{
                        //    field: "text",
                        //    title: "Text",
                        //    width: 260,
                        //    filterable: false,
                        //    editor: function(container, options) {
                        //        var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                        //        input.attr("name", options.field);
                        //        input.appendTo(container);
                        //        input.kendoMaskedTextBox();
                        //    }
                        //},
                        {
                            field: "fileName",
                            title: "File Name",
                            template: function (e) {
                                var template;
                                if (e.fileName === "" || e.fileName === null || e.fileName === undefined) {
                                    template = "<div style='text-align: center;cursor:pointer'><a>" + "Upload Document..." + "</a></div>";
                                } else {
                                    var fileName = e.fileName;
                                    if (fileName.length > 30) {
                                        fileName = fileName.substring(0,29) + " ...";
                                    }
                                    template = "<div style='text-align: center;cursor:pointer'><a>" + fileName + "</a></div>";
                                }
                                return template;
                            },
                            width: 300,
                            filterable: false,
                            editor: function(container, e) {
                                var input = $('<input name="fileName" id="fileName" type="file" aria-label="files" style="width: 300px"/>');
                                input.appendTo(container);

                                console.log('PortalAdminStaticDataGridView:def:onShow:e:' + JSON.stringify(e));

                                var fileName = e.model.fileName;
                                if (fileName.length > 15) {
                                    fileName = fileName.substring(0,14) + " ...";
                                }
                                var files = [{
                                    name: fileName,
                                    size: 600,
                                    extension: ""
                                }];


                                input.kendoUpload({
                                    success: self.onSuccess,
                                    remove: self.onRemove,
                                    async: {
                                        withCredentials: false,
                                        saveUrl: App.config.DataServiceURL + "/rest/files/AdminUpload/" + self.getCategoryText(e.model.category).replace(/ /g,""),
                                        removeUrl: App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/" + self.getCategoryText(e.model.category).replace(/ /g,"")
                                    },
                                    multiple: false,
                                    files: e.model.fileName !== "" ? files : []
                                });
                            }
                        },
                        {

                            field: "timeStamp",
                            title: "Time Stamp",
                            width: 100,
                            format: "{0:M/d/yyyy}",
                            filterable: false,
                            editable: false
                        }

                    ],
                    change: function (e) {
                        console.log('PortalAdminStaticDataGridView:def:onShow:onChange:');
                    },
                    saveChanges: function(e) {
                        console.log('PortalAdminStaticDataGridView:def:onShow:saveChanges:');
                        //if (!confirm("Are you sure you want to save all changes?")) {
                        //    e.preventDefault();
                        //}
                    },
                    beforeEdit: function(e) {
                        console.log('PortalAdminStaticDataGridView:def:onShow:beforeEdit:');
                        if (!e.model.isNew()) {
                            console.log('PortalAdminStaticDataGridView:def:onShow:beforeEdit:isNew');
                            //e.preventDefault();
                        }
                    },
                    save: function (e) {
                        e.model.dirty = true;
                        e.model.title = e.model.title.trim();
                        e.model.text = e.model.text.trim();
                        e.model.text2 = e.model.text2.trim();
                        e.model.externalLink = e.model.externalLink.trim();
                        e.model.shortText = e.model.shortText.trim();
                        //e.model.timeStamp.setHours(0);
                        console.log('PortalAdminStaticDataGridView:def:onShow:onSave:e.model:' + JSON.stringify(e.model));
                    },
                    edit: function (e) {
                        console.log('PortalAdminStaticDataGridView:onShow:edit');


                        self.fileName = e.model.fileName;
                        console.log('PortalAdminStaticDataGridView:onShow:edit:fileName:' + self.fileName);

                        console.log('PortalAdminStaticDataGridView:onShow:edit:category:' + e.model.category);
                        console.log('PortalAdminStaticDataGridView:onShow:edit:subCategory:' + e.model.subCategory);

                        self.model = e.model;

                    },
                    dataBound: function (e) {
                        console.log("PortalAdminStaticDataGridView:def:onShow:dataBound");

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;
                            if (filters.length === 0) {
                                filters.push({field: "category", operator: "gt", value: 8});
                            }
                            var adminStaticGridFilter = [];

                            $.each(filters, function (index, value) {
                                adminStaticGridFilter.push(value);
                            });

                            console.log("PortalAdminStaticDataGridView:def:onShow:dataBound:set adminStaticGridFilter:" + JSON.stringify(adminStaticGridFilter));
                            app.model.set('adminStaticGridFilter', adminStaticGridFilter);
                        }

                        console.log("PortalAdminStaticDataGridView:def:onShow:dataBound:re-sort by date");

                        $('.k-grid-add').unbind("click");
                        $('.k-grid-add').bind("click", function(){
                            console.log("PortalAdminStaticDataGridView:def:onShow:dataBound:Add");
                            var grid = self.$("#adminStaticData").data("kendoGrid");
                            grid.dataSource.sort({field: "ID", dir: "desc"});
                        });
                    }
                });

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getSubCategoryText: function (subCategory) {

                //console.log("PortalAdminStaticDataGridView:getSubCategoryText");
                var text = "";
                if (subCategory || subCategory === 0) {
                    $.each(this.subCategories, function (index, cat) {
                        if (cat.value === subCategory) {
                            text = cat.text;
                            return false;
                        }
                    });
                }
                return text;

            },
            getCategoryText: function (category) {

                //console.log("PortalAdminStaticDataGridView:getCategoryText");
                var text = "";
                if (category || category === 0) {
                    $.each(this.categories, function (index, cat) {
                        if (cat.value === category) {
                            text = cat.text;
                            return false;
                        }
                    });
                }
                return text;

            },
            onRemove:  function (e) {
                if (e && e.sender) {
                    console.log('PortalAdminStaticDataGridView:onRemove:');
                    e.sender.options.async.removeUrl = App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/" + this.category + "/" + this.fileName + "/";
                    this.model.fileName = "";
                    this.model.dirty = true;
                    var grid = $("#adminStaticData").data("kendoGrid");
                    grid.saveChanges();
                    console.log('PortalAdminStaticDataGridView:onRemove:removeUrl:' + e.sender.options.async.removeUrl);
                }
            },
            onSuccess:  function (e) {
                //if (e && e.response) {
                    console.log('PortalAdminStaticDataGridView:onSuccess:response:' + e.response);
                    console.log('PortalAdminStaticDataGridView:onSuccess:category:' + this.category);
                    console.log('PortalAdminStaticDataGridView:onSuccess:ID:' + this.model.ID);
                    if (e.response.indexOf("Exception") > -1) {
                    } else if (e.response.indexOf("Deleted file") > -1) {
                    } else {
                        this.fileName = e.response;
                        this.model.fileName = e.response;
                        this.model.dirty = true;
                        console.log('PortalAdminStaticDataGridView:onSuccess:fileName changed');
                        var grid = $("#adminStaticData").data("kendoGrid");
                        grid.saveChanges();
                    }
                    //else {
                    //    createAutoClosingAlert($("#startError"), "<strong>File Error</strong>" + e.response);
                    //}
                    console.log('PortalAdminStaticDataGridView:onSuccess:model:' + JSON.stringify(this.model));

                //}
            },
            createAutoClosingAlert:  function (selector, html)  {
                console.log('PortalAdminStaticDataGridView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 50000);
            },
            subCategoryFilter: function (container) {

                //console.log('PortalAdminStaticDataGridView:subCategoryFilter:' + JSON.stringify(container));
                container.element.kendoDropDownList({
                    dataValueField: "value",
                    dataTextField: "text",
                    valuePrimitive: true,
                    dataSource: this.subCategories,
                    optionLabel: "Select Value"
                });
            },
            getData: function () {
                //console.log('PortalAdminStaticDataGridView:getData');

                var grid = this.$("#adminStaticData").data("kendoGrid");

                //console.log('PortalAdminStaticDataGridView:getData:filter:' + JSON.stringify(state.filter));

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);

                    if (self.adminStaticGridFilter !== null && self.adminStaticGridFilter !== []) {
                        var filters = [];
                        if (self.adminStaticGridFilter.length === 0) {
                            filters.push({field: "category", operator: "gt", value: 8});
                        }
                        $.each(self.adminStaticGridFilter, function (index, value) {
                            filters.push(value);
                        });

                        //console.log("PortalAdminStaticDataGridView:grid:set datasource:filter" + JSON.stringify(filters));
                        grid.dataSource.filter(filters);
                    } else {
                        grid.dataSource.filter({field: "category", operator: "gt", value: 8});
                    }
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalAdminStaticDataGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#adminStaticData").parent().parent().height();
                this.$("#adminStaticData").height(parentHeight);

                parentHeight = parentHeight - 81;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalAdminStaticDataGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalAdminStaticDataGridView:resize:headerHeight:' + headerHeight);
                this.$("#adminStaticData").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalAdminStaticDataGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#adminStaticData").data('ui-tooltip'))
                    this.$("#adminStaticData").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });