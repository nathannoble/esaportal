define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalVendorGrid', 'hbs!templates/editPortalVendorPopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template, EditPortalVendorPopup) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalVendorGridView:initialize');

                _.bindAll(this);

                //this.editPopup = EditPortalVendorPopup;

                var self = this;
                var app = App;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalVendor",
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalVendorGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalVendor" + "(" + data.vendorID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalVendorGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalVendor";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalVendorGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalVendor" + "(" + data.vendorID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "vendorID",
                                fields: {

                                    //company = '#FORM.company#',
                                    //category = '#FORM.category#',
                                    //contactFirstName = '#FORM.contactFirstName#',
                                    //contactLastName = '#FORM.contactLastName#',
                                    //title = '#FORM.title#',
                                    //email = '#FORM.email#',

                                    //webAddress = '#FORM.webAddress#',
                                    //workPhone = '#FORM.workPhone#',
                                    //workStreet = '#FORM.workStreet#',
                                    //workCity = '#FORM.workCity#',
                                    //workState = '#FORM.workState#',
                                    //workZip = '#FORM.workZip#',
                                    //workFax = '#FORM.workFax#',
                                    //homePhone = '#FORM.homePhone#',
                                    //mobilePhone = '#FORM.mobilePhone#',
                                    //additonalInfo = '#FORM.additonalInfo#'

                                    vendorID: {editable: false, type: "number"},
                                    company: {editable: true, type: "string", validation: {required: false}},
                                    category: {editable: true, type: "string", validation: {required: false}},
                                    contactFirstName: {editable: true, type: "string", validation: {required: false}},
                                    contactLastName: {editable: true, type: "string", validation: {required: false}},
                                    title: {editable: true, type: "string", validation: {required: true}},
                                    email: {editable: true, type: "string", validation: {required: false}},
                                    webAddress: {editable: true, type: "string", validation: {required: false}},
                                    workPhone: {editable: true, type: "string", validation: {required: false}},
                                    workStreet: {editable: true, type: "string", validation: {required: false}},
                                    workCity: {editable: true, type: "string", validation: {required: false}},
                                    workState: {editable: true, type: "string", validation: {required: false}},
                                    workZip: {editable: true, type: "string", validation: {required: false}},
                                    workFax: {editable: true, type: "string", validation: {required: false}},
                                    homePhone: {editable: true, type: "string", validation: {required: false}},
                                    mobilePhone: {editable: true, type: "string", validation: {required: false}},
                                    additionalInfo: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: {field: "vendorID", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('PortalVendorGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#vendorLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalVendorGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#vendorLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalVendorGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalVendorGridView:dataSource:error');
                            kendo.ui.progress(self.$("#vendorLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalVendorGridView:displayNoDataOverlay');

                this.$el.css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No field selected</div></div></div>').appendTo(this.$el.parent());

            },
            onRender: function () {
                //console.log('PortalVendorGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalVendorGridView:onShow --------");
                var self = this;

                this.grid = this.$("#vendor").kendoGrid({
                    toolbar: ["create"],
                    //editable: "popup",
                    editable: {
                        mode: "popup",
                        window: {
                            title: "Edit Vendor"
                        },
                        template: function (e) {
                            var template = EditPortalVendorPopup();
                            return template;
                        }
                    },
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    groupable: true,
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction k-grid-edit' data-id='" + e.vendorID + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                                //fa-edit
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.vendorID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "vendorID",
                            title: "ID",
                            width: 100
                        }, {
                            field: "company",
                            title: "Company",
                            width: 150
                            //locked: true
                        },
                        {
                            field: "category",
                            title: "Category",
                            width: 150
                            //locked: true
                        },
                        {
                            field: "workCity",
                            title: "City",
                            width: 250
                            //locked: true
                        },
                        {
                            field: "workState",
                            title: "State",
                            width: 50
                            //locked: true
                        }
                    ],
                    change: function (e) {
                        //console.log('PortalVendorGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('PortalVendorGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('PortalVendorGridView:def:onShow:onEdit');

                        // Disable the vendorID editor
                        e.container.find("input[name='vendorID']").prop("disabled", true);

                        // Make edit/delete invisible
                        //e.container.find(".k-edit-label:first").hide();
                        //e.container.find(".k-edit-field:first").hide();
                        //e.container.find(".k-edit-label:nth-child(4)").hide();
                        //e.container.find(".k-edit-field:nth-child(4)").hide();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PortalVendorGridView:getData');

                var grid = this.$("#vendor").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalVendorGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#vendor").parent().parent().height();
                this.$("#vendor").height(parentHeight);

                parentHeight = parentHeight - 65; //117

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalVendorGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalVendorGridView:resize:headerHeight:' + headerHeight);
                this.$("#vendor").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalVendorGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });