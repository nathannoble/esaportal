define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressPlanGrid', 'hbs!templates/editProgressPlanPopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template, EditProgressPlanPopup) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('ProgressPlanGridView:initialize');

                _.bindAll(this);

                var self = this;
                //var app = App;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressPlan",
                                complete: function (e) {
                                    console.log('ProgressPlanGridView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressPlanGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('ProgressPlanGridView:update');
                                    return App.config.DataServiceURL + "/odata/ProgressPlan" + "(" + data.planID + ")";
                                },
                                complete: function (e) {
                                    console.log('ProgressPlanGridView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressPlanGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('ProgressPlanGridView:create');
                                    return App.config.DataServiceURL + "/odata/ProgressPlan";
                                },
                                complete: function (e) {
                                    console.log('ProgressPlanGridView:dataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressPlanGridView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('ProgressPlanGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/ProgressPlan" + "(" + data.planID + ")";
                                },
                                complete: function (e) {
                                    console.log('ProgressPlanGridView:dataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressPlanGridView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "planID",
                                fields: {
                                    planID: {editable: false, type: "number"},
                                    planType: {editable: true, type: "string", validation: {required: false}},
                                    planName: {editable: true, type: "string", validation: {required: false}},
                                    description: {editable: true, type: "string", validation: {required: true}},
                                    planPrice: {editable: true, type: "number", validation: {required: false}},
                                    rateSheetHours_4wk: {editable: true, type: "number"},
                                    schedulingHours_4wk: {editable: true, type: "number"},
                                    inactive: {
                                        editable: true,
                                        type: "boolean",
                                        nullable: true
                                    }
                                }
                            }
                        },
                        //pageSize: 50,
                        sort: {field: "planName", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('ProgressPlanGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#planLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('ProgressPlanGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#planLoading"), false);
                        },
                        change: function (e) {
                            //console.log('ProgressPlanGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('ProgressPlanGridView:dataSource:error');
                            kendo.ui.progress(self.$("#planLoading"), false);
                        }
                    });

            },
            onRender: function () {
                //console.log('ProgressPlanGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressPlanGridView:onShow --------");
                var self = this;

                // Panel title
                $("#planTitle").html("Plans");

                //console.log('ProgressPlanGridView:onShow:districtID:' + this.districtID);

                this.grid = this.$("#plan").kendoGrid({
                    toolbar: ["create"],
                    editable: {
                        mode: "popup",
                        window: {
                            title: "Edit Plan"
                        },
                        template: function (e) {
                            var template = EditProgressPlanPopup();
                            return template;
                        }
                    },
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    filterable: {
                        mode: "row"
                    },
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction k-grid-edit' data-id='" + e.planID + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                                //fa-edit
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        //{
                        //    width: 25,
                        //    template: function (e) {
                        //        var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.planID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                        //        return template;
                        //    },
                        //    filterable: false,
                        //    sortable: true
                        //},
                        {
                            field: "planID",
                            title: "ID",
                            width: 100,
                            filterable: false
                        }, {
                            field: "planType",
                            title: "Type",
                            width: 50,
                            filterable: false
                        },
                        {
                            field: "planName",
                            title: "Plan Name",
                            width: 250,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },
                        {
                            field: "description",
                            title: "Description",
                            width: 250,
                            filterable: false
                        },
                        {
                            field: "inactive",
                            title: "Plan Status",
                            template: function (e) {
                                if (e.inactive === null) {
                                    e.inactive = false;
                                }
                                if (e.inactive === false) {
                                    return "Active";
                                } else {
                                    return "Inactive";
                                }
                            },
                            width: 200,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.inactiveFilter
                                }
                            }
                        },
                        {
                            field: "planPrice",
                            title: "Pricing",
                            width: 125,
                            filterable: false
                        }, {
                            field: "rateSheetHours_4wk",
                            title: "Rate Sheet Hours",
                            width: 100,
                            filterable: false
                        }, {
                            field: "schedulingHours_4wk",
                            title: "Scheduling Hours",
                            width: 100,
                            filterable: false
                        }

                    ],
                    dataBound: function (e) {
                        //console.log("ProgressPlanGridView:def:onShow:dataBound");
                    },
                    change: function (e) {
                        //console.log('ProgressPlanGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('ProgressPlanGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('ProgressPlanGridView:def:onShow:onEdit');

                        // Disable the planID editor
                        e.container.find("input[name='planID']").prop("disabled", true);
                        //e.container.find("input[name='inactive']").prop("disabled", false);

                        // Make edit/delete invisible
                        //e.container.find(".k-edit-label:first").hide();
                        //e.container.find(".k-edit-field:first").hide();
                        ////e.container.find(".k-edit-label:nth-child(4)").hide();
                        //e.container.find(".k-edit-field:nth-child(4)").hide();

                        // Set the focus to the Username field
                        //e.container.find("input[name='Username']").focus();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            inactiveFilter: function (container) {
                var self = this;

                //console.log('ProgressPlanGridView:inactiveFilter:' + JSON.stringify(container));

                var dataSource = [
                    {value: true, text: "Inactive"},
                    {value: false, text: "Active"}
                ];

                //,{value: null, text: "All"}

                container.element.kendoDropDownList({
                    autoBind:false,
                    dataValueField: "value",
                    dataTextField: "text",
                    valuePrimitive: true,
                    dataSource: dataSource,
                    optionLabel: "Select Value"
                });

            },
            getData: function () {
                console.log('ProgressPlanGridView:getData');

                //var state = {};
                //state.page = 1;
                //state.pageSize = 40;

                var grid = this.$("#plan").data("kendoGrid");

                //console.log('ProgressPlanGridView:getData:filter:' + JSON.stringify(state.filter));

                var self = this;

                this.dataSource.fetch(function () {
                    //var json = JSON.stringify(data);

                    //var data = this.data();
                    //$.each(data, function (index, value) {
                    //    if (value.inactive === null) {
                    //        value.inactive = false;
                    //    }
                    //});
                    //var dataSource = new kendo.data.DataSource({
                    //    data: data
                    //});
                    //
                    //grid.setDataSource(dataSource);

                    grid.setDataSource(self.dataSource);
                    grid.dataSource.filter([{field: "inactive", operator: "eq", value: false}]);
                });

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressPlanGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#plan').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressPlanGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#plan').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressPlanGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#plan").parent().parent().height();
                this.$("#plan").height(parentHeight);

                parentHeight = parentHeight - 35;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressPlanGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressPlanGridView:resize:headerHeight:' + headerHeight);
                this.$("#plan").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressPlanGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#plan").data('ui-tooltip'))
                    this.$("#plan").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });