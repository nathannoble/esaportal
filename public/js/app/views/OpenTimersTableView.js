define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/openTimersTable', 'views/OpenTimersGridView'],
    function (App, Backbone, Marionette, $, template, OpenTimersGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                timeCardRegion: "#time-card-region"
            },
            initialize: function () {

                //console.log('OpenTimersTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('OpenTimersTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- OpenTimersTableView:onShow --------");

                var self = this;

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    this.timeCardRegion.show(new OpenTimersGridView());
                }


            },
            onSelectedDateFilterChanged: function () {
                this.timeCardRegion.reset();
                this.timeCardRegion.show(new OpenTimersGridView());
            },
            resize: function () {
                //console.log('OpenTimersTableView:resize');

                if (this.timeCardRegion.currentView)
                    this.timeCardRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------OpenTimersTableView:remove --------");

                //this.saveButtonClicked();

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });