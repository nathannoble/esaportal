define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/noteWidget', 'views/NoteCollectionView',
        'kendo/kendo.data', 'date'],
    //'kendo/kendo.binder'
    function (App, Backbone, Marionette, $, template, NoteCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                noteList: '#note-list'
            },
            events: {
                'click #EditNoteButton': 'editNoteButtonClicked'
            },
            initialize: function (options) {
                //console.log('NoteWidgetView:initialize');

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Client"};
                }

                this.viewModel = kendo.observable({
                    isChecked: false
                });
                this.userKey = parseInt(App.model.get('userKey'), 0);  //10268;

                this.clientID = parseInt(App.model.get('selectedClientId'), 0); //2189;
                this.employeeID = parseInt(App.model.get('selectedEmployeeId'), 0); //100;
                this.companyID = parseInt(App.model.get('selectedCompanyId'), 0); //4;

                this.firstName = "First";
                this.lastName = "Last";

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                //console.log('NoteWidgetView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('NoteWidgetView:onShow');

                // Setup the containers
                this.resize();

                var self = this;

                this.noteList.show(new NoteCollectionView({type: this.options.type}));
            },
            editNoteButtonClicked: function (e) {
                //console.log('NoteWidgetView:editNoteButtonClicked');

                var self = this;

                this.noteLabel = self.$('.editNoteLabel')[0].textContent;

                if (this.noteLabel === "Edit Note") {
                    this.noteID = parseInt(App.model.get('selectedNoteId'), 0);
                }

                self.editNote();

            },
            editNote: function () {
                //console.log('NoteWidgetView:editNote');
                var self = this;

                this.noteText = this.$('#editNoteText').val();

                var offset = (new Date()).getTimezoneOffset();
                var now = new Date(moment().clone().subtract((offset), 'minutes'));

                if (this.options.type === "Client") {
                    self.noteDataSourceClient = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            batch: false,
                            pageSize: 50,
                            serverPaging: true,
                            serverSorting: true,
                            serverFiltering: true,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressClientsNote";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceClient:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceClient:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSource:update');
                                        return App.config.DataServiceURL + "/odata/ProgressClientsNote" + "(" + data.clientNoteID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceClient:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceClient:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSourceClient:create');
                                        return App.config.DataServiceURL + "/odata/ProgressClientsNote";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceClient:create:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceClient:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSource:destroy');
                                        return App.config.DataServiceURL + "/odata/ProgressClientsNote" + "(" + data.clientNoteID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceClient:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceClient:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientNoteID",
                                    fields: {
                                        clientNoteID: {editable: false, defaultValue: 0, type: "number"},
                                        clientID: {type: "number"},
                                        userKey: {type: "number"},
                                        noteText: {editable: true, type: "string", validation: {required: false}},
                                        noteTimeStamp: {editable: true, type: "date", validation: {required: true}}
                                        // lastName: {editable: true, type: "string", validation: {required: false}},
                                        // firstName: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            sort: {field: "clientNoteID", dir: "desc"},
                            requestStart: function () {
                                //console.log('   NoteWidgetView:noteDataSource:requestStart');

                                kendo.ui.progress(self.$("#loadingNoteWidget"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    NoteWidgetView:noteDataSource:requestEnd');
                                kendo.ui.progress(self.$("#loadingNoteWidget"), false);
                                self.noteList.show(new NoteCollectionView({type: self.options.type}));

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   NoteWidgetView:noteDataSource:error');
                                kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                                self.displayNoDataOverlay();

                                //App.modal.show(new ErrorView());

                            }
                        });

                    if (self.noteLabel === "Edit Note") {

                        //Edit existing note record
                        self.noteDataSourceClient.filter({field: "clientNoteID", operator: "eq", value: self.noteID});
                        self.noteDataSourceClient.fetch(function () {

                            var data = this.data();

                            if (data.length > 0) {

                                var record = self.noteDataSourceClient.at(0);
                                record.set("noteText", self.noteText);
                                self.noteDataSourceClient.sync();
                            }
                        });

                    } else {

                        self.noteDataSourceClient.fetch(function () {

                            self.noteDataSourceClient.add({
                                //clientNoteID: {editable: false, defaultValue: 0, type: "number"},
                                noteText: self.noteText,
                                noteTimeStamp: now,
                                clientID: parseInt(self.clientID, 0),
                                // lastName: self.lastName,
                                // firstName: self.firstName,
                                userKey: self.userKey
                            });

                            self.noteDataSourceClient.sync();
                        });
                    }
                } else if (this.options.type === "Company") {
                    self.noteDataSourceCompany = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            batch: false,
                            pageSize: 50,
                            serverPaging: true,
                            serverSorting: true,
                            serverFiltering: true,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressCompaniesNote";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceCompany:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceCompany:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSource:update');
                                        return App.config.DataServiceURL + "/odata/ProgressCompaniesNote" + "(" + data.companyNoteID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceCompany:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceCompany:update::error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSourceCompany:create');
                                        return App.config.DataServiceURL + "/odata/ProgressCompaniesNote";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceCompany:create:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceCompany:rcreate:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSource:destroy');
                                        return App.config.DataServiceURL + "/odata/ProgressCompaniesNote" + "(" + data.companyNoteID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceCompany:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceCompany:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "companyNoteID",
                                    fields: {
                                        companyNoteID: {editable: false, defaultValue: 0, type: "number"},
                                        companyID: {type: "number"},
                                        userKey: {type: "number"},
                                        noteText: {editable: true, type: "string", validation: {required: false}},
                                        noteTimeStamp: {editable: true, type: "date", validation: {required: true}}
                                    }
                                }
                            },
                            sort: {field: "companyNoteID", dir: "desc"},
                            requestStart: function () {
                                //console.log('   NoteWidgetView:noteDataSource:requestStart');

                                kendo.ui.progress(self.$("#loadingNoteWidget"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    NoteWidgetView:noteDataSource:requestEnd');
                                kendo.ui.progress(self.$("#loadingNoteWidget"), false);
                                self.noteList.show(new NoteCollectionView({type: self.options.type}));

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   NoteWidgetView:noteDataSource:error');
                                kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                                self.displayNoDataOverlay();

                                //App.modal.show(new ErrorView());

                            }
                        });
                    if (self.noteLabel === "Edit Note") {

                        // Edit existing note record
                        self.noteDataSourceCompany.filter({field: "companyNoteID", operator: "eq", value: self.noteID});
                        self.noteDataSourceCompany.fetch(function () {

                            var data = this.data();

                            if (data.length > 0) {

                                var record = self.noteDataSourceCompany.at(0);
                                record.set("noteText", self.noteText);
                                self.noteDataSourceCompany.sync();
                            }
                        });

                    } else {
                        self.noteDataSourceCompany.fetch(function () {

                            self.noteDataSourceCompany.add({
                                noteText: self.noteText,
                                noteTimeStamp: now,
                                companyID: parseInt(self.companyID, 0),
                                userKey: self.userKey
                            });

                            self.noteDataSourceCompany.sync();
                        });
                    }
                } else if (this.options.type === "Employee") {

                    self.noteDataSourceEmployee = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            batch: false,
                            pageSize: 50,
                            serverPaging: true,
                            serverSorting: true,
                            serverFiltering: true,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressEmployeesNote";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceEmployee:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceEmployee:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSource:update');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployeesNote" + "(" + data.employeeNoteID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceEmployee:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceEmployee:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSourceEmployee:create');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployeesNote";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceEmployee:create:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceEmployee:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('NoteWidgetView:noteDataSource:destroy');
                                        return App.config.DataServiceURL + "/odata/ProgressEmployeesNote" + "(" + data.employeeNoteID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('NoteWidgetView:noteDataSourceEmployee:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'NoteWidgetView:noteDataSourceEmployee:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "employeeNoteID",
                                    fields: {
                                        employeeNoteID: {editable: false, defaultValue: 0, type: "number"},
                                        employeeID: {type: "number"},
                                        userKey: {type: "number"},
                                        noteText: {editable: true, type: "string", validation: {required: false}},
                                        noteTimeStamp: {editable: true, type: "date", validation: {required: true}}
                                    }
                                }
                            },
                            sort: {field: "employeeNoteID", dir: "desc"},
                            requestStart: function () {
                                //console.log('   NoteWidgetView:noteDataSource:requestStart:employee');

                                kendo.ui.progress(self.$("#loadingNoteWidget"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    NoteWidgetView:noteDataSource:requestEnd');
                                kendo.ui.progress(self.$("#loadingNoteWidget"), false);
                                self.noteList.show(new NoteCollectionView({type: self.options.type}));

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   NoteWidgetView:noteDataSource:error');
                                kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                                self.displayNoDataOverlay();

                                //App.modal.show(new ErrorView());

                            }
                        });

                    if (self.noteLabel === "Edit Note") {

                        //Edit existing note record
                        self.noteDataSourceEmployee.filter({field: "employeeNoteID", operator: "eq", value: self.noteID});
                        self.noteDataSourceEmployee.fetch(function () {

                            var data = this.data();

                            if (data.length > 0) {
                                var record = self.noteDataSourceEmployee.at(0);
                                record.set("noteText", self.noteText);
                                self.noteDataSourceEmployee.sync();
                            }
                        });

                    } else {

                        self.noteDataSourceEmployee.fetch(function () {

                            self.noteDataSourceEmployee.add({
                                noteText: self.noteText,
                                noteTimeStamp: now,
                                employeeID: parseInt(self.employeeID, 0),
                                userKey: self.userKey
                            });

                            self.noteDataSourceEmployee.sync();
                        });
                    }
                }

                //console.log('NoteWidgetView:editNote:date:utcdate:offset:' + now);
                self.$('#editNoteText').val("");
                self.$('.editNoteLabel').text("Add Note");
                self.$('#editNoteIcon').removeClass('fa-pencil-square-o');
                self.$('#editNoteIcon').addClass('fa-plus-circle');

            },
            displayNoDataOverlay: function () {
                //console.log('NoteWidgetView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('NoteWidgetView:resize');

            },
            remove: function () {
                //console.log('-------- NoteWidgetView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });