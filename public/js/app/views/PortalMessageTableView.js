define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalMessageTable', 'views/PortalMessageGridView'],
    function (App, Backbone, Marionette, $, template, PortalMessageGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                messageRegion: "#message-region"
            },
            initialize: function () {

                //console.log('PortalMessageTableView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('PortalMessageTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalMessageTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    this.messageRegion.show(new PortalMessageGridView());
                }

            },
            resize: function () {
                //console.log('PortalMessageTableView:resize');

                if (this.messageRegion.currentView)
                    this.messageRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalMessageTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });