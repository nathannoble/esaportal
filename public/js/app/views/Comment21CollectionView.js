define(['App', 'backbone', 'marionette', 'jquery', 'models/CommentModel', 'hbs!templates/comment21Collection',
        'views/Comment21View', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, CommentModel, template, Comment21View) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: Comment21View,
            initialize: function (options) {
                //console.log('Comment21CollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();

                var self = this;

                this.comments = [];

                if (this.options.comments) {
                    this.comments = this.options.comments;
                    this.addUserImage = this.options.addUserImage;
                }

                //console.log('Comment21CollectionView:initialize:comments: ' + JSON.stringify(this.comments));
                // Populate message and comment collections
                self.collection.reset();
                //self.collection.comparator = function (model) {
                //    return -(model.get("time_stamp"));
                //};

                self.collection.comparator = function(model) {
                        return -[model.get("comment_id")]; //, model.get("reply_comment_id")
                };

                if (self.comments.length === 0) {
                    // Hide comment area if no elements exist
                }

                $.each(self.comments, function (index,value) {

                    //console.log('Comment21CollectionView:comments:record:' + JSON.stringify(value));

                    if (value.replyCommentID === null) {
                        var model = new CommentModel();

                        model.set("comment_id", value.commentID);
                        model.set("time_stamp", value.timeStamp);
                        model.set("comment_text", value.commentText);
                        model.set("user_key", value.userKey);
                        model.set("last_name", value.lastName);
                        model.set("first_name", value.firstName);
                        model.set("message_id", value.messageID);
                        model.set("fileName", value.fileName);
                        model.set("addUserImage", self.addUserImage);

                        self.collection.push(model);
                    }
                });

                self.collection.sort();

                $.each(self.collection.models, function (index2,value2) {
                    $.each(self.comments, function (index,value) {

                        if (value.replyCommentID === value2.attributes.comment_id) {
                            // Need to get the data record and populate it
                            var model = new CommentModel();

                            model.set("comment_id", value.commentID);
                            model.set("time_stamp", value.timeStamp);
                            model.set("comment_text", value.commentText);
                            model.set("user_key", value.userKey);
                            model.set("last_name", value.lastName);
                            model.set("first_name", value.firstName);
                            model.set("message_id", value.messageID);
                            model.set("fileName", value.fileName);
                            model.set("reply_comment_id", value.replyCommentID);

                            self.collection.add(model,{at: index2+1});
                        }
                    });
                });
            },
            onShow: function () {
                console.log("-------- Comment21CollectionView:onShow --------");

                //console.log("Comment21CollectionView:onShow:collection:" + JSON.stringify(self.collection));

            },
            onResize: function () {
//                //console.log('Comment21CollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('Comment21CollectionView:resize');

            },
            remove: function () {
                //console.log('---------- Comment21CollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
