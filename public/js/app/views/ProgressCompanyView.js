define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressCompany', 'jquery.cookie',
        'kendo/kendo.data', 'kendo/kendo.combobox', 'kendo/kendo.datepicker'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #SaveButton': 'saveButtonClicked'
            },
            initialize: function (options) {
                //console.log('ProgressCompanyView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Edit Company"};
                }

                this.userId = App.model.get('userId');

                self.teamLeaderDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('ProgressCompanyView:teamLeaderDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressCompanyView:teamLeaderDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    status: {type: "string", validation: {required: false}},
                                    isTeamLeader: {editable: true, type: "boolean"},
                                    teamLeaderID: {editable: false, type: "number"},
                                    teamLeaderName: {editable: false, type: "string"}
                                }
                            }
                        },
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"}
                        ],
                        filter: {field: "isTeamLeader", operator: "eq", value: true},
                        serverFiltering: true,
                        serverSorting: true
                    });

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    if (this.options.type === "New Company") {
                        this.companyID = null;
                    } else {
                        this.companyID = parseInt(App.model.get('selectedCompanyId'), 0); //2189;
                    }
                    // Subscribe to browser events
                    $(window).on("resize", this.onResize);

                    this.companyDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressCompany";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressCompanyView:companyDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressCompanyView:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ProgressCompanyView:update');
                                        return App.config.DataServiceURL + "/odata/ProgressCompany" + "(" + data.companyID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressCompanyView:companyDataSource:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressCompanyView:companyDataSource:updateerror:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        console.log('ProgressCompanyView:create:data:' + JSON.stringify(data));
                                        return App.config.DataServiceURL + "/odata/ProgressCompany";
                                    },
                                    complete: function (e) {
                                        //console.log('ProgressCompanyView:create:complete');
                                        if (e.responseJSON !== undefined) {
                                            //console.log('ProgressCompanyView:create:complete:' + JSON.stringify(e.responseJSON));
                                            if (e.responseJSON.value[0]) {
                                                var clientCompany = e.responseJSON.value[0].clientCompany;
                                                var newCompanyID = e.responseJSON.value[0].companyID;
                                    
                                                console.log('ProgressCompanyView:create:complete:companyID:clientCompany:' + newCompanyID + ":"+  clientCompany);
                                    
                                                // A duplicate has been created
                                                if (clientCompany === '--- NONE ---') {
                                                    console.log('ProgressCompanyView:create:complete:duplicate');
                                    
                                                    //Delete duplicate
                                                    self.deleteDuplicate(newCompanyID);
                                                }
                                                app.model.set('selectedCompanyId', newCompanyID);
                                                app.router.navigate('companyProfile', {trigger: true});
                                            } else {
                                                var message = 'ProgressCompanyView:create:complete:no record';
                                                console.log(message);
                                            }
                                        } else {
                                            var message2 = 'ProgressCompanyView:create:complete:no record 2';
                                            console.log(message2);
                                        }
                                    },
                                    error: function (e) {
                                        var message = 'ProgressCompanyView:companyDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('ProgressCompanyView:destroy');
                                        return App.config.DataServiceURL + "/odata/ProgressCompany" + "(" + data.companyID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressCompanyView:companyDataSource:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressCompanyView:companyDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "companyID",
                                    fields: {
                                        companyID: {editable: false, type: "number"},
                                        branchID: {editable: true, type: "number"},
                                        clientCompany: {editable: true, type: "string", validation: {required: false}},
                                        startDate: {editable: true, type: "date", validation: {required: false}},
                                        stageOfSale: {editable: true, type: "string", validation: {required: false}},
                                        contactFirstName: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        contactLastName: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        contactPhone: {editable: true, type: "string", validation: {required: false}},
                                        contactEmail: {editable: true, type: "string", validation: {required: false}},
                                        contactAddress: {editable: true, type: "string", validation: {required: false}},
                                        contactCity: {editable: true, type: "string", validation: {required: false}},
                                        contactState: {editable: true, type: "string", validation: {required: false}},
                                        contactZip: {editable: true, type: "string", validation: {required: false}},
                                        systemCRM_Name: {editable: true, type: "string", validation: {required: false}},
                                        systemCRM_URL: {editable: true, type: "string", validation: {required: false}},
                                        systemEmail_Name: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        systemEmail_URL: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        companyProfile: {editable: true, type: "string", validation: {required: false}},
                                        keyPeople: {editable: true, type: "string", validation: {required: false}},
                                        industry: {editable: true, type: "string", validation: {required: false}},
                                        product: {editable: true, type: "string", validation: {required: false}},
                                        totalClientOpportunity: {editable: true, type: "number"},
                                        actualClients: {editable: true, type: "number"},
                                        expectedCloseDate: {
                                            editable: true,
                                            type: "date",
                                            validation: {required: false}
                                        },
                                        actualCloseDate: {editable: true, type: "date", validation: {required: false}},
                                        leadDate: {editable: true, type: "date", validation: {required: false}},
                                        taskTracking: {editable: true, type: "boolean"},
                                        calendarSystemNotes: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        emailSystemNotes: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        complianceGuidelines: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        billingDetails: {editable: true, type: "string", validation: {required: false}},
                                        cspType: {editable: true, type: "string", validation: {required: false}},
                                        ratesStructure: {editable: true, type: "string", validation: {required: false}},
                                        dbAccountType: {
                                            editable: true,
                                            type: "string",
                                            validation: {required: false}
                                        },
                                        esaExclusive: {editable: true, type: "boolean"},
                                        primaryAccountMgrID: {editable: true, type: "number"},
                                        corporateContract: {editable: true, type: "boolean"}
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                //console.log('   ProgressCompanyView:requestStart');

                                kendo.ui.progress(self.$("#loadingCompanyInfo"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressCompanyView:requestEnd');
                                kendo.ui.progress(self.$("#loadingCompanyInfo"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressCompanyView:error');
                                kendo.ui.progress(self.$("#loadingCompanyInfo"), false);

                                self.displayNoDataOverlay();

                                //App.modal.show(new ErrorView());

                            },
                            change: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressCompanyView:change');

                                var data = this.data();

                                if (data.length <= 0)
                                    self.displayNoDataOverlay();
                                else
                                    self.hideNoDataOverlay();
                            },
                            filter: [{field: "companyID", operator: "eq", value: self.companyID}]

                        });

                    this.clientDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            serverFiltering: true,
                            serverSorting: true,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressCompanyView:clientDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressCompanyView:clientDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientID",
                                    fields: {
                                        clientID: {editable: false, defaultValue: 0, type: "number"},
                                        lastName: {editable: true, type: "string", validation: {required: false}},
                                        firstName: {editable: true, type: "string", validation: {required: false}},
                                        companyID: {editable: false, defaultValue: 0, type: "number"},
                                        clientCompany: {editable: true, type: "string", validation: {required: false}}
                                    }
                                }
                            },
                            requestStart: function () {
                                //console.log('   ProgressClientActiveGridView:requestStart');

                                kendo.ui.progress(self.$("#clientLoading"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressClientActiveGridView:requestEnd');
                                kendo.ui.progress(self.$("#clientLoading"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressClientActiveGridView:error');
                                kendo.ui.progress(self.$("#clientLoading"), false);

                                self.displayNoDataOverlay();

                                //App.modal.show(new ErrorView());

                            },
                            change: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressClientActiveGridView:change');

                                var data = this.data();

                                if (data.length <= 0)
                                    self.displayNoDataOverlay();
                                else
                                    self.hideNoDataOverlay();
                            },
                            filter: [
                                {field: "status", operator: "eq", value: '4 - Active'},
                                {field: "companyID", operator: "eq", value: self.companyID}
                            ],
                            sort: [{field: "clientCompany", dir: "asc"}, {field: "lastName", dir: "asc"}]
                        });

                    this.getData();
                }
            },
            onRender: function () {
                //console.log('ProgressCompanyView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressCompanyView:onShow --------");
                var self = this;

                if (this.options.type === "New Company") {
                    self.$('#SaveButton').html('Add New Company');
                } else {
                    self.$('#SaveButton').html("Update Company");
                }

                self.$('#SaveButton').addClass('disabled');
            },
            getData: function () {

                var self = this;

                this.companyDataSource.fetch(function () {

                    var data = this.data();
                    var count = data.length;
                    var companyRecord;

                    //console.log('ProgressCompanyView:dataSource:fetch:count:' + count);
                    if (count === 0 || self.options.type === "New Company") {

                        // Create new record
                        companyRecord = {
                            contactFirstName: "First",
                            contactLastName: "Last",
                            contactEmail: "",
                            contactAddress: "",
                            contactCity: "",
                            contactState: "",
                            contactZip: "",
                            contactPhone: "",
                            clientCompany: "Company Name",
                            companyProfile: "",
                            keyPeople: "",

                            primaryAccountMgrID: 0,

                            systemCRM_Name: "",
                            systemCRM_URL: "",
                            systemEmail_Name: "",
                            systemEmail_URL: "",

                            calendarSystemNotes: "",
                            emailSystemNotes: "",
                            complianceGuidelines: "",
                            billingDetails: "",

                            esaExclusive: false,
                            corporateContract: false,
                            taskTracking: false,

                            totalClientOpportunity: 0,

                            industry: "",
                            stageOfSale: "",
                            product: "",
                            cspType:"",
                            ratesStructure: "",
                            dbAccountType: "",

                            startDate: null,
                            expectedCloseDate: null,
                            actualCloseDate: null,
                            leadDate: null
                        };

                        self.newRecord = companyRecord;

                        //console.log('ProgressCompanyView:dataSource:new company record:' + JSON.stringify(companyRecord));

                    } else {

                        companyRecord = data[0];
                        //console.log('ProgressCompanyView:dataSource:record:' + JSON.stringify(companyRecord));
                    }

                    // Dates
                    $("#leadDate").kendoDatePicker({
                        value: companyRecord.leadDate
                    });

                    $("#expectedCloseDate").kendoDatePicker({
                        value: companyRecord.expectedCloseDate
                    });

                    $("#actualCloseDate").kendoDatePicker({
                        value: companyRecord.actualCloseDate
                    });
                    $("#startDate").kendoDatePicker({
                        value: companyRecord.startDate
                    });

                    self.$(".k-textbox").kendoMaskedTextBox();
                    self.$(".k-numerictextbox").kendoNumericTextBox();

                    self.$("#contactFirstName").data("kendoMaskedTextBox").value(companyRecord.contactFirstName);
                    self.$("#contactLastName").data("kendoMaskedTextBox").value(companyRecord.contactLastName);
                    self.$("#contactAddress").data("kendoMaskedTextBox").value(companyRecord.contactAddress);
                    self.$("#contactCity").data("kendoMaskedTextBox").value(companyRecord.contactCity);
                    self.$("#contactState").data("kendoMaskedTextBox").value(companyRecord.contactState);
                    self.$("#contactZip").data("kendoMaskedTextBox").value(companyRecord.contactZip);
                    self.$("#contactPhone").data("kendoMaskedTextBox").value(companyRecord.contactPhone);
                    self.$("#contactEmail").data("kendoMaskedTextBox").value(companyRecord.contactEmail);

                    self.$("#companyID").data("kendoMaskedTextBox").value(companyRecord.companyID);

                    self.$("#clientCompany").data("kendoMaskedTextBox").value(companyRecord.clientCompany);
                    self.$("#companyProfile").data("kendoMaskedTextBox").value(companyRecord.companyProfile);
                    self.$("#keyPeople").data("kendoMaskedTextBox").value(companyRecord.keyPeople);

                    self.$("#systemCRM_Name").data("kendoMaskedTextBox").value(companyRecord.systemCRM_Name);
                    self.$("#systemCRM_URL").data("kendoMaskedTextBox").value(companyRecord.systemCRM_URL);

                    self.$("#systemEmail_Name").data("kendoMaskedTextBox").value(companyRecord.systemEmail_Name);
                    self.$("#systemEmail_URL").data("kendoMaskedTextBox").value(companyRecord.systemEmail_URL);

                    self.$("#totalClientOpportunity").data("kendoMaskedTextBox").value(companyRecord.totalClientOpportunity);

                    self.$("#calendarSystemNotes").data("kendoMaskedTextBox").value(companyRecord.calendarSystemNotes);
                    self.$("#emailSystemNotes").data("kendoMaskedTextBox").value(companyRecord.emailSystemNotes);
                    self.$("#complianceGuidelines").data("kendoMaskedTextBox").value(companyRecord.complianceGuidelines);
                    self.$("#billingDetails").data("kendoMaskedTextBox").value(companyRecord.billingDetails);

                    var yesNoList = [
                        {text: 'Yes', value: "true"},
                        {text: 'No', value: "false"}
                    ];

                    self.$("#taskTracking").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: companyRecord.taskTracking.toString()
                    });

                    if (companyRecord.esaExclusive === null) {
                        companyRecord.esaExclusive = false;
                    }

                    self.$("#esaExclusive").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: companyRecord.esaExclusive.toString()
                    });

                    if (companyRecord.corporateContract === null) {
                        companyRecord.corporateContract = false;
                    }

                    self.$("#corporateContract").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: companyRecord.corporateContract.toString()
                    });

                    if (self.teamLeaderRecords && self.teamLeaderRecords.length > 0) {

                        self.$("#primaryAccountMgrID").kendoDropDownList({
                            dataTextField: "teamLeaderName",
                            dataValueField: "teamLeaderID",
                            dataSource: self.teamLeaderRecords,
                            value: companyRecord.primaryAccountMgrID
                        });

                    } else {
                        if (companyRecord.primaryAccountMgrID === null) {
                            companyRecord.primaryAccountMgrID = 0;
                        }
                        self.teamLeaderDataSource.fetch(function () {

                            var tlRecords = this.data();
                            self.teamLeaderRecords = [];

                            $.each(tlRecords, function (index, value) {

                                var record = value;
                                var newRecord = {
                                    teamLeaderID: record.employeeID,
                                    teamLeaderName: record.lastName + ", " + record.firstName
                                };
                                self.teamLeaderRecords.push(newRecord);

                            });

                            var noneRecord = {
                                teamLeaderID: 0,
                                teamLeaderName: "None"
                            };

                            self.teamLeaderRecords.unshift(noneRecord);

                            self.$("#primaryAccountMgrID").kendoDropDownList({
                                dataTextField: "teamLeaderName",
                                dataValueField: "teamLeaderID",
                                dataSource: self.teamLeaderRecords,
                                value: companyRecord.primaryAccountMgrID
                                //change: self.onAccountMrgChanged
                            });
                        });
                    }

                    var cspTypes = [
                        {value: 'Short CSP'},
                        {value: 'Standard CSP'}
                    ];

                    self.$("#cspType").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: cspTypes,
                        value: companyRecord.cspType
                    });

                    var ratesStructures = [
                        {value: 'Corp Rates'},
                        {value: 'Standard Rates'},
                        {value: 'Other'}
                    ];

                    self.$("#ratesStructure").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: ratesStructures,
                        value: companyRecord.ratesStructure
                    });

                    var dbAccountTypes = [
                        {value: 'Dialer Account'},
                        {value: 'CRM Account'},
                        {value: 'Other'}
                    ];

                    self.$("#dbAccountType").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: dbAccountTypes,
                        value: companyRecord.dbAccountType
                    });

                    var branches = [
                        {text: 'ESA Scheduling Services', value: 1},
                        {text: 'OS Scheduling Services', value: 2}
                    ];

                    self.$("#branchID").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: branches,
                        value: companyRecord.branchID
                    });

                    var industries = [
                        {value: 'Financial Industry'},
                        {value: 'Insurance'},
                        {value: 'Pharmaceutical'},
                        {value: 'Real Estate'},
                        {value: 'Other'}
                    ];

                    self.$("#industry").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: industries,
                        value: companyRecord.industry
                    });

                    var products = [
                        {value: 'Other'},
                        {value: 'All Channels'},
                        {value: 'Alternative'},
                        {value: 'Annuities (Both)'},
                        {value: 'Annuities (Fixed)'},
                        {value: 'Annuities (Variable)'},
                        {value: 'Business Development'},
                        {value: 'Consulting'},
                        {value: 'Digital Wealth'},
                        {value: 'ETFs'},
                        {value: 'Hedge Fund'},
                        {value: 'Life Insurance'},
                        {value: 'Long Term Care'},
                        {value: 'Mutual Funds'},
                        {value: 'Recruiting'},
                        {value: 'REIT'},
                        {value: 'Retirement / 401K'},
                        {value: 'Reverse Mortgage'},
                        {value: 'RIA'},
                        {value: "SMA's"}
                    ];

                    var selectedProducts = [];
                    if (companyRecord.product !== null) {
                        selectedProducts = companyRecord.product.split(",");
                    }
                    self.$("#product").kendoMultiSelect({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: products,
                        value: selectedProducts
                    });

                    var stages = [
                        {value: '0 - Lost'},
                        {value: '1 - Prospect'},
                        {value: '2 - Qualified'},
                        {value: '3 - In Process'},
                        {value: '4 - Pending'},
                        {value: '5 - Closed (Approved)'},
                        {value: '6 - Closed (Contracted)'}
                    ];

                    self.$("#stageOfSale").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: stages,
                        value: companyRecord.stageOfSale
                    });

                    //console.log('ProgressCompanyView:before clientDataSource.fetch:companyId:' + self.companyID);
                    self.clientDataSource.fetch(function () {

                        //var activeClientCount = 0;
                        //console.log('ProgressCompanyView:clientDataSource.fetch:start');


                        if (this.data()) {
                            //$.each(data.items, function (index, value) {
                            //    activeClientCount += 1;
                            //});
                            self.$("#actualClients").data("kendoMaskedTextBox").value(this.data().length);
                            //console.log('ProgressCompanyView:clientDataSource.fetch:items found');
                        } else {
                            self.$("#actualClients").data("kendoMaskedTextBox").value(0);
                            //console.log('ProgressCompanyView:clientDataSource.fetch:items not found');
                        }

                        //console.log('ProgressCompanyView:clientDataSource.fetch:end');

                    });

                    console.log('ProgressCompanyView:after DataSource.fetch');

                    self.$('#SaveButton').removeClass('disabled');

                });

            },

            saveButtonClicked: function (e) {
                //console.log('ProgressCompanyView:saveButtonClicked');

                var self = this;
                var app = App;

                var record;

                var status = self.$("#status").val();

                self.startDate = self.$("#startDate").val();
                if (self.startDate !== null && self.startDate !== "") {
                    self.startDate = new Date(self.startDate).toISOString();
                } else {
                    self.startDate = null;
                }

                //console.log('ProgressCompanyView:saveButtonClicked:esaAnniversary:' + esaAnniversary);

                self.expectedCloseDate = self.$("#expectedCloseDate").val();
                if (self.expectedCloseDate !== null && self.expectedCloseDate !== "") {
                    self.expectedCloseDate = new Date(self.expectedCloseDate).toISOString();
                } else {
                    self.expectedCloseDate = null;
                }

                self.actualCloseDate = self.$("#actualCloseDate").val();
                if (self.actualCloseDate !== null && self.actualCloseDate !== "") {
                    self.actualCloseDate = new Date(self.actualCloseDate).toISOString();
                } else {
                    self.actualCloseDate = null;
                }

                self.leadDate = self.$("#leadDate").val();
                if (self.leadDate !== null && self.leadDate !== "") {
                    self.leadDate = new Date(self.leadDate).toISOString();
                } else {
                    self.leadDate = null;
                }

                var products = [];
                $("#product option:selected").each(function () {
                    products.push($(this).val());
                });

                if (this.options.type === "New Company") {

                    record = {
                        contactFirstName: self.$("#contactFirstName").val(),
                        contactLastName: self.$("#contactLastName").val(),
                        contactEmail: self.$("#contactEmail").val(),
                        contactAddress: self.$("#contactAddress").val(),
                        contactCity: self.$("#contactCity").val(),
                        contactState: self.$("#contactState").val(),
                        contactZip: self.$("#contactZip").val(),
                        contactPhone: self.$("#contactPhone").val(),
                        clientCompany: (self.$("#clientCompany").val()),

                        primaryAccountMgrID: (self.$("#primaryAccountMgrID").val()),
                        branchID: (self.$("#branchID").val()),

                        systemCRM_Name: self.$("#systemCRM_Name").val(),
                        systemCRM_URL: self.$("#systemCRM_URL").val(),
                        systemEmail_Name: self.$("#systemEmail_Name").val(),
                        systemEmail_URL: (self.$("#systemEmail_URL").val()),

                        calendarSystemNotes: self.$("#calendarSystemNotes").val(),
                        emailSystemNotes: self.$("#emailSystemNotes").val(),
                        complianceGuidelines: self.$("#complianceGuidelines").val(),
                        billingDetails: (self.$("#billingDetails").val()),

                        esaExclusive: self.$("#esaExclusive").val() === "true",
                        corporateContract: self.$("#corporateContract").val() === "true",
                        taskTracking: self.$("#taskTracking").val() === "true",

                        totalClientOpportunity: (self.$("#totalClientOpportunity").val()),

                        industry: self.$("#industry").val(),
                        stageOfSale: self.$("#stageOfSale").val(),
                        product: products.join(),
                        cspType: self.$("#cspType").val(),
                        ratesStructure: self.$("#ratesStructure").val(),
                        dbAccountType: self.$("#dbAccountType").val(),

                        startDate: self.startDate,
                        expectedCloseDate: self.expectedCloseDate,
                        actualCloseDate: self.actualCloseDate,
                        leadDate: self.leadDate,
                        companyProfile: self.$("#companyProfile").val(),
                        keyPeople: self.$("#keyPeople").val()
                    };

                    self.companyDataSource.add(record);
                    self.companyDataSource.sync();

                } else {
                    this.companyDataSource.fetch(function () {
                        var data = this.data();
                        var count = data.length;

                        //console.log('ProgressCompanyView:saveButtonClicked:product:' + self.$("#product").val());

                        if (count > 0) {

                            record = self.companyDataSource.at(0);

                            record.set("clientCompany", self.$("#clientCompany").val());
                            record.set("esaExclusive", (self.$("#esaExclusive").val() === "true"));
                            record.set("corporateContract", (self.$("#corporateContract").val() === "true"));
                            record.set("taskTracking", (self.$("#taskTracking").val() === "true"));

                            record.set("contactFirstName", self.$("#contactFirstName").val());
                            record.set("contactLastName", self.$("#contactLastName").val());
                            record.set("contactEmail", self.$("#contactEmail").val());
                            record.set("contactAddress", self.$("#contactAddress").val());
                            record.set("contactCity", self.$("#contactCity").val());
                            record.set("contactState", self.$("#contactState").val());
                            record.set("contactZip", self.$("#contactZip").val());
                            record.set("contactPhone", self.$("#contactPhone").val());
                            record.set("companyProfile", self.$("#companyProfile").val());

                            record.set("primaryAccountMgrID", self.$("#primaryAccountMgrID").val());
                            record.set("branchID", self.$("#branchID").val());

                            record.set("totalClientOpportunity", self.$("#totalClientOpportunity").val());

                            record.set("industry", self.$("#industry").val());
                            record.set("stageOfSale", self.$("#stageOfSale").val());

                            record.set("product", products.join());
                            record.set("cspType", self.$("#cspType").val());
                            record.set("ratesStructure", self.$("#ratesStructure").val());
                            record.set("dbAccountType", self.$("#dbAccountType").val());

                            record.set("startDate", self.$("#startDate").val());
                            record.set("expectedCloseDate", self.$("#expectedCloseDate").val());
                            record.set("actualCloseDate", self.$("#actualCloseDate").val());
                            record.set("leadDate", self.$("#leadDate").val());

                            record.set("systemCRM_Name", self.$("#systemCRM_Name").val());
                            record.set("systemCRM_URL", self.$("#systemCRM_URL").val());
                            record.set("systemEmail_Name", self.$("#systemEmail_Name").val());
                            record.set("systemEmail_URL", self.$("#systemEmail_URL").val());

                            record.set("calendarSystemNotes", self.$("#calendarSystemNotes").val());
                            record.set("emailSystemNotes", self.$("#emailSystemNotes").val());
                            record.set("complianceGuidelines", self.$("#complianceGuidelines").val());
                            record.set("billingDetails", self.$("#billingDetails").val());

                            record.set("keyPeople", self.$("#keyPeople").val());

                            console.log('ProgressCompanyView:companyDataSource:save:recordToSave:' + JSON.stringify(record));

                            $.when(this.sync()).done(function (e) {
                                //console.log('ProgressCompanyView:companyDataSource:add new sync done:' + record.companyID);
                                app.model.set('selectedCompanyId', record.companyID);
                                app.router.navigate('companyProfile', {trigger: true});
                            });
                        }
                    });
                }

            },

            createAutoClosingAlert: function (selector, html) {
                //console.log('HomeView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 5000);
            },

            displayNoDataOverlay: function () {
                //console.log('ProgressCompanyView:displayNoDataOverlay');

                // Display the no data overlay
                //if (this.$('.no-data-overlay-outer').length === 0)
                //    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressCompanyView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressCompanyView:resize');

            },
            remove: function () {
                //console.log("-------- ProgressCompanyView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });