define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/widget2', 'views/ScoreboardDetailsView', 'kendo/kendo.data'],
    function (App, Backbone, Marionette, $, template, ScoreboardDetailsView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #OpenDetails': 'onOpenDetailsClicked'
            },
            initialize: function (options) {
                //console.log('Widget2View:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                var app = App;

                if (App.model.get('selectedDateFilter') !== null) {
                    this.dateFilter = App.model.get('selectedDateFilter').substring(0, 10); // start date
                    this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;
                } else {
                    this.year = 2023;
                }

            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- Widget2View:onShow ----------');

                this.populateWidget();
            },
            onOpenDetailsClicked: function () {
//                console.log('Widget2View:onOpenDetailsClicked');

                App.modal.show(new ScoreboardDetailsView());
            },
            populateWidget: function () {
//                //console.log('Widget2View:createChart');

                var self = this;
                var app = App;

                // data call
                //SELECT    clients, target, ytd, remainGoal
                //FROM      tblPortalScoreBoardStats
                //WHERE    year = #this.year#


                //"year":2015,"clients":0,"target":"10.0","ytd":"0.2","remainGoal":"10.0"

                this.dataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalScoreBoardStat";
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "year",
                            fields: {
                                year: {editable: false, defaultValue: 0, type: "number"},
                                clients: {type: "number"},
                                ytd: {type: "number"},
                                target: {type: "number"},
                                remainGoal: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   Widget2View:requestStart');

                        kendo.ui.progress(self.$("#loadingWidget2"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    Widget2View:requestEnd');
                        kendo.ui.progress(self.$("#loadingWidget2"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   Widget2View:error');
                        kendo.ui.progress(self.$("#loadingWidget2"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   Widget2View:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: {field: "year", operator: "eq", value: self.year}
                });

                var widget2Value = app.model.get('widget2Value');

                //console.log('Widget2View:populateWidget:widget2Value:' + widget2Value);
                if (widget2Value === null) {
                    this.dataSource.fetch(function () {

                        var value = 0;
                        if (this.data().length > 0) {
                            var datarecord = this.data()[0];
                            value = datarecord.target;
                        }

                        value = app.Utilities.numberUtilities.getFormattedNumberWithOnePlaceRounding(value);

                        // Populate widget with data
                        self.$('#targetNum').html(value);
                        self.$('#yearLabel').html(self.year + " Target");

                        app.model.set('widget2Value', value);
                    });
                } else {
                    // Populate widget with data
                    self.$('#targetNum').html(widget2Value);
                    self.$('#yearLabel').html(self.year + " Target");
                }

            },
            displayNoDataOverlay: function () {
                //console.log('Widget2View:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('Widget2View:hideNodDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('.tile-content').css('display', 'block');
            },
            toggleMoreInfo: function () {
//                //console.log('Widget2View:toggleMoreInfo');

                if (this.$('.tile-hover-tip').hasClass('tile-hover-tip-displayed')) {
                    this.$('.tile-hover-tip').removeClass('tile-hover-tip-displayed');
                    this.$('.tile-more-info').removeClass('tile-more-info-rotated');
                }
                else {
                    this.$('.tile-hover-tip').addClass('tile-hover-tip-displayed');
                    this.$('.tile-more-info').addClass('tile-more-info-rotated');
                }
            },
            mouseEnter: function () {
//                //console.log('Widget2View:mouseEnter');

                self.$('#widget2Tile').find('.tile-icon').addClass('clr-selected');
            },
            mouseLeave: function () {
//                //console.log('Widget2View:mouseLeave');

                self.$('#widget2Tile').find('.tile-icon').removeClass('clr-selected');
            },
            onResize: function () {
//                //console.log('Widget2View:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('Widget2View:resize');

            },
            remove: function () {
                //console.log('---------- Widget2View:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off
                this.$('.tile-content').off();
                this.$('.tile-more-info').off();
                this.$('.tile-hover-tip').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });