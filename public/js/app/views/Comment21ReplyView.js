define(['App', 'backbone', 'marionette', 'jquery',
        'hbs!templates/comment21Reply',
        'kendo/kendo.data', 'kendo/kendo.combobox','kendo/kendo.upload'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #CloseButton': 'onCloseClicked',
                'click #SubmitButton': 'onSubmitClicked',
                'keydown': 'keyAction'
            },
            initialize: function (options) {
                console.log('Comment21ReplyView:initialize');

                var offset = (new Date()).getTimezoneOffset();
                this.now = new Date();
                this.now.setHours(23);

                this.userKey = parseInt(App.model.get('userKey'), 0);  //10268;

                this.fileName = null;

                this.myEmployeeId = App.model.get('myEmployeeId');

                this.options = options;

                if (this.options !== null && this.options !== undefined) {

                    this.commentID = this.options.commentID;
                    this.messageID = this.options.messageID;
                    this.name = this.options.name;
                    this.addUserImage = this.options.addUserImage;

                    console.log('Comment21ReplyView:init:commentID:messageID:addUserImage' + this.commentID + ":" + this.messageID + ":" + this.addUserImage);
                    this.getData();
                }

            },
            onRender: function () {
                console.log('Comment21ReplyView:onRender');

                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('---------- Comment21ReplyView:onShow ----------');

                var self = this;

                this.$('#title').html("Reply to " + this.name + "'s comment");

                $("#fileName").kendoUpload({
                    enabled: self.addUserImage,
                    success: self.onSuccess,
                    remove: self.onRemove,
                    async: {
                        //autoUpload: false,
                        withCredentials: false,
                        saveUrl: App.config.DataServiceURL + "/rest/files/AdminUpload/CommentImages",
                        removeUrl: App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/CommentImages"
                    },
                    multiple: false
                });
            },
            getData: function () {

                var self = this;

                this.messageCommentDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalMessagesComment";
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('Comment21ReplyView:update');
                                    return App.config.DataServiceURL + "/odata/PortalMessagesComment" + "(" + data.commentID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('Comment21ReplyView:create');
                                    return App.config.DataServiceURL + "/odata/PortalMessagesComment";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('Comment21ReplyView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalMessagesComment" + "(" + data.commentID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "commentID",
                                fields: {
                                    commentID: {editable: false, defaultValue: 0, type: "number"},
                                    commentText: {editable: true, type: "string", validation: {required: false}},
                                    fileName: {editable: true, type: "string", validation: {required: false}},
                                    timeStamp: {editable: true, type: "date", validation: {required: true}},
                                    messageID: {type: "number"},
                                    userKey: {type: "number"},
                                    replyCommentID: {editable: false, defaultValue: 0, type: "number"}
                                }
                            }
                        },
                        sort: [{field: "messageID", dir: "desc"},{field: "commentID", dir: "asc"}],
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            console.log('   Comment21ReplyView:messageCommentDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingComment21Reply"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            console.log('    Comment21ReplyView:messageCommentDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingComment21Reply"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            console.log('   Comment21ReplyView:messageCommentDataSource:error');
                            kendo.ui.progress(self.$("#loadingComment21Reply"), false);

                            //self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        }
                    });

            },
            keyAction: function (e) {
                //console.log('Comment21ReplyView:keyAction:' + e.which);
                if (e.which === 13) {
                    // enter has been pressed
                    this.createComment();
                }
            },
            onRemove:  function (e) {
                if (e && e.sender) {
                    console.log('Comment21ReplyView:onRemove:this.filename:' + this.filename);
                    e.sender.options.async.removeUrl = App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/CommentImages/" + this.fileName + "/";
                    this.fileName = null;
                    console.log('Comment21ReplyView:onRemove:removeUrl:' + e.sender.options.async.removeUrl);
                }
            },
            onSuccess:  function (e) {
                console.log('Comment21ReplyView:onSuccess:response:' + e.response);
                //console.log('Comment21ReplyView:onSuccess:ID:' + this.model.ID);
                if (e.response.indexOf("Exception") > -1) {
                } else if (e.response.indexOf("Deleted file") > -1) {
                } else {
                    window.fileName = e.response;
                    this.fileName = e.response;
                    console.log('Comment21ReplyView:onSuccess:fileName:' + e.response);
                }

            },
            onCloseClicked: function () {
                console.log('Comment21ReplyView:onCloseClicked');
                this.close();
            },
            onSubmitClicked: function () {
                console.log('Comment21ReplyView:onSubmitClicked');

                this.createComment();
            },
            createComment: function () {

                console.log('Comment21ReplyView:createComment');

                var self = this;
                var app = App;

                this.commentText = this.$('#newCommentText').val();

                if (self.fileName === "" ) {
                    self.fileName = null;
                }

                if (window.fileName !== undefined && !window.fileName.id) {
                    self.fileName = window.fileName;
                }

                if (this.commentText !== "") {

                    //var offset = (new Date()).getTimezoneOffset();
                    var now = new Date(); //moment().clone().subtract((offset), 'minutes'));
                    var today = moment(new Date()).clone().format('YYYY-MM-DD');

                    console.log('Comment21ReplyView:addComment:date:' + today + ":comment:" + this.commentText);

                    var commentText = this.commentText.trim();
                    if (commentText.length > 500) {
                        commentText = commentText.substring(0,499) + " ...";
                    }

                    self.messageCommentDataSource.add({
                        commentText: commentText,
                        timeStamp: now,
                        messageID: parseInt(self.messageID, 0),
                        userKey: parseInt(self.userKey, 0),
                        fileName: self.fileName,
                        replyCommentID: self.commentID
                    });

                    $.when(self.messageCommentDataSource.sync()).done(function (e) {

                        var random = Math.random();
                        console.log('Comment21ReplyView:addComment:saved:random:' + random);
                        app.model.set('messageCommentAddedId', random);

                        self.close();
                    });

                }
            },
            createAlert: function (selector, html) {
                //console.log('Comment21ReplyView:createAlert');

                var self = this;

                self.$(selector).html(html);
                self.$(selector).css("opacity", 1);
                self.$(selector).css("display", "block");
                var alert = self.$(selector).addClass("alert");

            },
            onResize: function () {
//                console.log('Comment21ReplyView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                console.log('Comment21ReplyView:resize');
            },
            remove: function () {
                console.log('---------- Comment21ReplyView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });