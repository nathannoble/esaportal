define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/ProgressClientReportGrid',
    'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('ProgressClientReportGridView:initialize');

                _.bindAll(this);

                this.options = options;

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Standard"};
                }

                if (this.options.type === "All") {
                    this.type = "All";
                }

                var self = this;
                var app = App;

                this.clientReportGridFilter = App.model.get('clientReportGridFilter');

                this.dateFilter = App.model.get('selectedDateFilter');

                this.startDate = new Date(app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter)); //"2016-10-01T00:00:00+0000"
                this.endDate = new Date(app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter)); //"2016-10-31T23:59:59+0000"

                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;
                this.startYear = app.Utilities.numberUtilities.getStartYearFromDateFilter(this.dateFilter); //2016;
                //this.month = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter); //11;
                this.startMonth = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter);
                this.endMonth = app.Utilities.numberUtilities.getEndMonthFromDateFilter(this.dateFilter);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.progressClientDS = new kendo.data.DataSource(
                    {
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                            },
                            complete: function (e) {
                                console.log('ProgressClientReportGridView:progressClientDS:read:complete');
                            },
                            error: function (e) {
                                var message = 'ProgressClientReportGridView:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                planHours: {type: "number"},
                                taskTargetLow: {type: "number"},
                                taskTargetHigh: {type: "number"},
                                mtgTargetLowDec: {type: "number"},
                                mtgTargetHigh: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   ProgressClientReportGridView:requestStart');

                        kendo.ui.progress(self.$("#clientDataLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    ProgressClientReportGridView:requestEnd');
                        //kendo.ui.progress(self.$("#clientDataLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientReportGridView:error');
                        kendo.ui.progress(self.$("#clientDataLoading"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientReportGridView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: [{field: "status", operator: "eq", value: '4 - Active'}]

                });

                this.progressClientDataDS = new kendo.data.DataSource(
                    {
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientData";
                            },
                            complete: function (e) {
                                console.log('ProgressClientReportGridView:progressClientDataDS:read:complete');
                            },
                            error: function (e) {
                                var message = 'ProgressClientReportGridView:progressClientDataDS:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "entryID",
                            fields: {
                                entryID: {editable: false, defaultValue: 0, type: "number"},
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                clientCompany: {editable: true, type: "string", validation: {required: false}},
                                schedulerName: {editable: true, type: "string", validation: {required: false}},
                                status: {editable: true, type: "string", validation: {required: false}},
                                workDate: {editable: true, type: "date", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: false}},
                                vYear: {type: "number"},
                                vMonth: {type: "number"},
                                sumTotalTime: {type: "number"},
                                sumTotalTasks: {type: "number"},
                                sumTotalMeetings: {type: "number"},
                                sumAdminTime: {type: "number"},
                                sumCompTime: {type: "number"},
                                sumCompCalls: {type: "number"},
                                sumCompMeetings: {type: "number"},
                                compMeetings: {type: "number"},
                                sumReferralTime: {type: "number"},
                                sumReferralCalls: {type: "number"},
                                sumReferralMeetings: {type: "number"},
                                referralMeetings: {type: "number"},
                                sumTotalTimeWithComp: {type: "number"},
                                sumTotalTasksWithComp: {type: "number"},
                                sumTotalMeetingsWithComp: {type: "number"},
                                sumTotalTimeWithReferral: {type: "number"},
                                sumTotalTasksWithReferral: {type: "number"},
                                sumTotalMeetingsWithReferral: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   ProgressClientReportGridView:requestStart');

                        //kendo.ui.progress(self.$("#clientDataLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    ProgressClientReportGridView:requestEnd');
                        kendo.ui.progress(self.$("#clientDataLoading"), false);

                    },
                    error: function (e) {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientReportGridView:error:' + JSON.stringify(e));
                        kendo.ui.progress(self.$("#clientDataLoading"), false);

                    },
                    filter: {
                        logic: "and",
                        filters: [
                            {field: "vYear", operator: "eq", value: self.year},
                            //{field: "vMonth", operator: "eq", value: self.month}
                            {field: "vMonth", operator: "gte", value: self.startMonth},
                            {field: "vMonth", operator: "lte", value: self.endMonth}
                        ]
                    }
                    //filter: [
                    //    {field: "workDate", operator: "gte", value: self.startDate},
                    //    {field: "workDate", operator: "lte", value: self.endDate}
                    //]
                });

                this.companyDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                complete: function (e) {
                                    console.log('ProgressClientReportGridView:companyDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientReportGridView:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyID",
                                fields: {
                                    companyID: {editable: false, type: "number"},
                                    clientCompany: {type: "string"}
                                }
                            }
                        },
                        sort: {field: "clientCompany", dir: "asc"},
                        serverSorting: true,
                        serverFiltering: true,
                        filter: [
                            {field: "clientCompany", operator: "neq", value: "--- NONE ---"}
                            //{field: "actualClients", operator: "gt", value: 0},
                            //{field: "actualClients", operator: "neq", value: null}
                        ]

                    });
            },
            onRender: function () {
                //console.log('ProgressClientReportGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientReportGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#clientData").kendoGrid(
                    {
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    toolbar: [
                        {
                            name: "excel"
                        }],
                    excel: {
                        fileName: "ClientReport.xlsx",
                        allPages: true
                    },
                    filterable: {
                        mode: "row"
                    },
                    columnResize: function (e) {
                        //self.resize();
                        window.setTimeout(self.resize, 10);
                    },
                    columns: [
                        {
                            title: "Client Company",
                            field: "clientCompany",
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                return template;
                            },
                            width: 250,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.companyFilter
                                }
                            },
                            sortable: true
                        },
                        {
                            field: "clientName",
                            title: "Client Name",
                            width: 200,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.lastName + ", " + e.firstName+ "</div>";
                                return template;
                            },
                            sortable: true,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },
                        {
                            field: "sumTotalTimeWithComp",
                            hidden: (self.type !== "All"),
                            template: function (e) {
                                var time;
                                //if (self.type === "All") {
                                    time = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumCompTime) + parseFloat(e.sumTotalTime));
                                    if (parseFloat(e.sumCompTime) > 0) {
                                        time = "<b>" + time + "</b>";
                                    }
                                //} else {
                                //    time = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalTime));
                                //}
                                var template = "<div style='text-align: left;'>" + time + "</div>";
                                return template;
                            },
                            title: "Total Time",
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "sumTotalTimeWithReferral",
                            hidden: (self.type !== "All"),
                            template: function (e) {
                                var time;
                                //if (self.type === "All") {
                                    time = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumReferralTime) + parseFloat(e.sumTotalTime));
                                    if (parseFloat(e.sumReferralTime) > 0) {
                                        time = "<b>" + time + "</b>";
                                    }
                                //} else {
                                //    time = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalTime));
                                //}
                                var template = "<div style='text-align: left;'>" + time + "</div>";
                                return template;
                            },
                            title: "Total Time with Ref",
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "sumTotalTime",
                            hidden: (self.type === "All"),
                            template: function (e) {
                                var time = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalTime));
                                var template = "<div style='text-align: left;'>" + time + "</div>";
                                return template;
                            },
                            title: "Total Time",
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "sumTotalTasks",
                            title: "Total Tasks",
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "avTasks",
                            hidden: (self.type === "All"),
                            title: "Average Tasks",
                            template: function (e) {
                                var taskAvg;
                                if (parseFloat(e.sumTotalTime) > 0)
                                    taskAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalTasks)/parseFloat(e.sumTotalTime));
                                else
                                    taskAvg = 0;
                                var template = "<div style='text-align: left;'>" + taskAvg + "</div>";
                                return template;
                            },
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "avTasksWithComp",
                            hidden: (self.type !== "All"),
                            title: "Average Tasks",
                            template: function (e) {
                                var taskAvg;
                                if (parseFloat(e.sumCompTime) + parseFloat(e.sumTotalTime) > 0)
                                    taskAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalTasks)/(parseFloat(e.sumCompTime) + parseFloat(e.sumTotalTime)));
                                else
                                    taskAvg = 0;
                                var template = "<div style='text-align: left;'>" + taskAvg + "</div>";
                                return template;
                            },
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "avTasksWithReferral",
                            hidden: (self.type !== "All"),
                            title: "Average Tasks with Ref",
                            template: function (e) {
                                var taskAvg;
                                if (parseFloat(e.sumReferralTime) + parseFloat(e.sumTotalTime) > 0)
                                    taskAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalTasks)/(parseFloat(e.sumReferralTime) + parseFloat(e.sumTotalTime)));
                                else
                                    taskAvg = 0;
                                var template = "<div style='text-align: left;'>" + taskAvg + "</div>";
                                return template;
                            },
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "sumTotalMeetings",
                            title: "Total Meetings",
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "avMeetings",
                            hidden: (self.type === "All"),
                            title: "Average Meetings",
                            template: function (e) {
                                var mtgAvg;
                                if (parseFloat(e.sumTotalTime) > 0)
                                    mtgAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalMeetings)/parseFloat(e.sumTotalTime));
                                else
                                    mtgAvg = 0;
                                var template = "<div style='text-align: left;'>" + mtgAvg + "</div>";
                                return template;
                            },
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "avMeetingsWithComp",
                            hidden: (self.type !== "All"),
                            title: "Average Meetings",
                            template: function (e) {
                                var mtgAvg;
                                if (parseFloat(e.sumCompTime) + parseFloat(e.sumTotalTime) > 0)
                                    mtgAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalMeetings)/(parseFloat(e.sumCompTime) + parseFloat(e.sumTotalTime)));
                                else
                                    mtgAvg = 0;

                                var template = "<div style='text-align: left;'>" + mtgAvg + "</div>";
                                return template;
                            },
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "avMeetingsWithReferral",
                            hidden: (self.type !== "All"),
                            title: "Average Meetings with Ref",
                            template: function (e) {
                                var mtgAvg;
                                if (parseFloat(e.sumReferralTime) + parseFloat(e.sumTotalTime) > 0)
                                    mtgAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(e.sumTotalMeetings)/(parseFloat(e.sumReferralTime) + parseFloat(e.sumTotalTime)));
                                else
                                    mtgAvg = 0;

                                var template = "<div style='text-align: left;'>" + mtgAvg + "</div>";
                                return template;
                            },
                            width: 120,
                            filterable: false
                        },
                        {
                            field: "schedulerName",
                            title: "Scheduler",
                            width: 250,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("ProgressClientReportGridView:def:onShow:dataBound");
                        self.$('.company-profile').on('click', self.viewCompanyProfile);

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;

                            var clientReportGridFilter = [];

                            $.each(filters, function (index, value) {
                                clientReportGridFilter.push(value);
                            });

                            //console.log("ProgressClientReportGridView:def:onShow:dataBound:set clientReportGridFilter:" + JSON.stringify(clientReportGridFilter));
                            app.model.set('clientReportGridFilter', clientReportGridFilter);
                        }

                    },
                    change: function (e) {
                        //console.log('ProgressClientReportGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('ProgressClientReportGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('ProgressClientReportGridView:def:onShow:onEdit');

                        // Disable the companyID editor
                        e.container.find("input[name='clientID']").prop("disabled", true);

                        // Set the focus to the Username field
                        //e.container.find("input[name='Username']").focus();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data

                if (this.year !== this.startYear) {
                    alert("The date filter crosses years (" + this.startYear + "-" + this.year + ").  Only data between the chosen months in the most recent year will displayed.");
                }

                this.getData();

            },
            getData: function () {
                //console.log('ProgressClientReportGridView:getData');

                var grid = this.$("#clientData").data("kendoGrid");

                var self = this;
                var app = App;

                this.progressClientDS.fetch(function (data) {

                    var progressClientRecords = this.data();
                    var clientRecords = [];

                    self.progressClientDataDS.fetch(function () {

                        var progressClientDataRecords = this.data();

                        $.each(progressClientRecords, function (index, value) {

                            var record = value;

                            var sumTotalMeetings = 0;
                            var sumTotalMeetingsWithComp = 0;
                            var sumTotalMeetingsWithReferral = 0;
                            //var sumMeetingsTarget = 0;
                            //var sumPlanHours = 0;

                            var sumTotalTasks= 0;
                            var sumTotalTasksWithComp= 0;
                            var sumTotalTasksWithReferral= 0;

                            var sumAdminTime= 0;
                            var sumTotalTime= 0;
                            var sumTotalTimeWithComp= 0;
                            var sumTotalTimeWithReferral= 0;
                            var sumRemainingTime= 0;
                            var sumCompTime= 0;
                            var sumReferralTime= 0;
                            
                            // var avMeetings = 0;
                            // var avMeetingsWithComp = 0;
                            // var avTasks = 0;
                            // var avTasksWithComp = 0;
                            // var avMeetingsWithReferral = 0;
                            // var avTasksWithReferral = 0;

                            // ProgressClientData records
                            $.each(progressClientDataRecords, function (index2, value2) {
                                var clientID = value2.clientID;
                                if (record.clientID === clientID) {
                                    //console.log('ProgressClientReportGridView:clientRecord:' + JSON.stringify(value));
                                    //console.log('ProgressClientReportGridView:clientDataRecord:' + JSON.stringify(value2));

                                    sumTotalMeetings += value2.sumTotalMeetingsWithComp; 
                                    sumTotalMeetings += value2.sumReferralMeetings; 

                                    sumTotalMeetingsWithComp += value2.sumTotalMeetingsWithComp;
                                    sumTotalMeetingsWithReferral += value2.sumTotalMeetingsWithReferral;

                                    sumAdminTime += value2.sumAdminTime;
                                    sumTotalTime+= value2.sumTotalTime;

                                    sumCompTime+= value2.sumCompTime;
                                    sumTotalTimeWithComp+= value2.sumTotalTimeWithComp;

                                    sumReferralTime+= value2.sumReferralTime;
                                    sumTotalTimeWithReferral+= value2.sumTotalTimeWithReferral;

                                    sumTotalTasks+= value2.sumTotalTasksWithComp; 
                                    sumTotalTasks+= value2.sumReferralCalls;

                                    sumTotalTasksWithComp+= value2.sumTotalTasksWithComp;
                                    sumTotalTasksWithReferral+= value2.sumTotalTasksWithReferral;
                                   
                                }
                            });

                            var meetingsTarget = record.mtgTargetLowDec * record.planHours;
                            var meetingsPercent = sumTotalMeetings / meetingsTarget;

                            if (record.planHours !== 0) {
                                sumRemainingTime = record.planHours - sumTotalTime;
                            } else {
                                sumRemainingTime = 0;
                            }

                            var taskAvg;
                            if (parseFloat(sumTotalTime) > 0)
                                taskAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(sumTotalTasks)/parseFloat(sumTotalTime));
                            else
                                taskAvg = 0;

                            var mtgAvg;
                            if (parseFloat(sumTotalTime) > 0)
                                mtgAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(sumTotalMeetings)/parseFloat(sumTotalTime));
                            else
                                mtgAvg = 0;

                            // Comp    
                            var taskAvgWithComp;
                            if (parseFloat(sumCompTime) + parseFloat(sumTotalTime) > 0)
                                taskAvgWithComp = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(sumTotalTasks)/(parseFloat(sumCompTime) + parseFloat(sumTotalTime)));
                            else
                                taskAvgWithComp = 0;

                            var mtgAvgWithComp;
                            if (parseFloat(sumCompTime) + parseFloat(sumTotalTime) > 0)
                                mtgAvgWithComp = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(sumTotalMeetings)/(parseFloat(sumCompTime) + parseFloat(sumTotalTime)));
                            else
                                mtgAvgWithComp = 0;

                            // Referral    
                            var taskAvgWithReferral;
                            if (parseFloat(sumReferralTime) + parseFloat(sumTotalTime) > 0)
                                taskAvgWithReferral = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(sumTotalTasks)/(parseFloat(sumReferralTime) + parseFloat(sumTotalTime)));
                            else
                                taskAvgWithReferral = 0;

                            var mtgAvgWithReferral;
                            if (parseFloat(sumReferralTime) + parseFloat(sumTotalTime) > 0)
                                mtgAvgWithReferral = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(parseFloat(sumTotalMeetings)/(parseFloat(sumReferralTime) + parseFloat(sumTotalTime)));
                            else
                                mtgAvgWithReferral = 0;


                            var newRecord = {

                                // From ProgressClient
                                clientID: record.clientID,
                                timeZoneName: record.timeZoneName,
                                companyID: record.companyID,
                                clientCompany: record.clientCompany,
                                lastName: record.lastName,
                                firstName: record.firstName,
                                planName: record.planName,
                                clientName: record.lastName + ", " + record.firstName,
                                planHours: record.planHours,
                                schedulerName: record.schedulerName,
                                hotList: record.hotList,
                                teamLeaderID: record.teamLeaderID,

                                // Calc'd from ProgressClient
                                tasksTarget:app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(record.planHours * record.taskTargetLow),
                                meetingsTarget:meetingsTarget,
                                sumTotalMeetings:sumTotalMeetings,

                                // Summed from ProgressClientData
                                sumAdminTime:app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumAdminTime),
                                sumTotalTime:app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalTime),
                                sumRemainingTime:app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumRemainingTime),
                                
                                sumCompTime:sumCompTime,
                                sumTotalTimeWithComp: app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalTimeWithComp),
                                
                                sumTotalTasks:sumTotalTasks,
                                sumTotalTasksWithComp:sumTotalTasksWithComp,

                                sumTotalMeetingsWithComp:sumTotalMeetingsWithComp,
                                meetingsPercent:app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(meetingsPercent),

                                avTasks: taskAvg,
                                avTasksWithComp: taskAvgWithComp,
                                
                                avMeetings: mtgAvg,
                                avMeetingsWithComp: mtgAvgWithComp,

                                sumReferralTime:sumReferralTime,
                                sumTotalTimeWithReferral: app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalTimeWithReferral),
                                sumTotalTasksWithReferral:sumTotalTasksWithReferral,
                                sumTotalMeetingsWithReferral:sumTotalMeetingsWithReferral,

                                avTasksWithReferral: taskAvgWithReferral,
                                avMeetingsWithReferral: mtgAvgWithReferral

                            };
                            //console.log('ProgressClientReportGridView:getData:clientRecord:' + JSON.stringify(newRecord));

                            clientRecords.push(newRecord);
                        });

                        var dataSource = new kendo.data.DataSource({
                            data: clientRecords,
                            sort: [{field: "clientCompany", dir: "asc"},{field: "lastName", dir: "asc"}]
                        });

                        grid.setDataSource(dataSource);
                        if (self.clientReportGridFilter !== null && self.clientReportGridFilter !== []) {
                            var filters = [];
                            $.each(self.clientReportGridFilter, function (index, value) {
                                filters.push(value);
                            });

                            //console.log("ProgressClientReportGridView:grid:set datasource:filters" + JSON.stringify(filters));
                            grid.dataSource.filter(filters);
                        } else {
                            var filter = {
                                logic: "and",
                                filters: [
                                    {field: "vYear", operator: "eq", value: self.year},
                                    {field: "vMonth", operator: "eq", value: self.month}
                                ]
                            };

                            //console.log("ProgressClientReportGridView:grid:set datasource:filter" + JSON.stringify(filter));
                            grid.dataSource.filter([]);
                        }

                        // Resize the grid
                        setTimeout(self.resize, 0);

                    });


                });

            },
            companyFilter: function (container) {
                var self = this;

                //console.log('ProgressClientReportGridView:companyFilter:' + JSON.stringify(container));
                self.companyDataSource.fetch(function (data) {

                    var dataSource = [];
                    var records = this.data();

                    $.each(records, function (index, value) {
                        dataSource.push({value: value.clientCompany, text: value.clientCompany});
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: dataSource,
                        optionLabel: "Select Value"
                        //change: self.onCompanyFilterChanged
                    });
                });

            },
            viewCompanyProfile: function (e) {
                //console.log('ProgressClientReportGridView:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientContactGridView:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId', companyId);


                App.router.navigate('companyProfile', {trigger: true});

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressClientReportGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#clientData').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressClientReportGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#clientData').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressClientReportGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#clientData").parent().parent().height();
                this.$("#clientData").height(parentHeight);

                parentHeight = parentHeight - 57; //117

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressClientReportGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressClientReportGridView:resize:headerHeight:' + headerHeight);
                this.$("#clientData").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressClientReportGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#clientData").data('ui-tooltip'))
                    this.$("#clientData").tooltip('destroy');

                this.clientReportGridFilter = [];

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });