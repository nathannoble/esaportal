define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalValueGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalValueGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalValue",
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalValueGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalValue" + "(" + data.valueID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalValueGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalValue";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalValueGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalValue" + "(" + data.valueID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "valueID",
                                fields: {
                                    valueID: {editable: false, type: "number"},
                                    valueText: {editable: true, type: "string"},
                                    valueTitle: {editable: true, type: "string"},
                                    lastDisplayed: {editable: true,type: "string"}
                                }
                            }
                        },
                        sort: {field: "lastDisplayed", dir: "desc"},
                        requestStart: function (e) {
                            //console.log('PortalValueGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#valueLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalValueGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#valueLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalValueGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalValueGridView:dataSource:error');
                            kendo.ui.progress(self.$("#valueLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalValueGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#client').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onRender: function () {
                //console.log('PortalValueGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalValueGridView:onShow --------");
                var self = this;

                this.grid = this.$("#value").kendoGrid({

                    toolbar: ["create"],
                    editable: "popup",  //inline, popup
                    sortable: true,
                    extra: false,
                    //selectable: "multiple",
                    resizable: true,
                    reorderable: true,
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction k-grid-edit' data-id='" + e.valueID + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                                //fa-edit
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.valueID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "valueID",
                            title: "ID",
                            width: 100
                        },
                        {
                            field: "valueTitle",
                            title: "Title",
                            width: 200
                        },{
                            field: "valueText",
                            title: "Text",
                            width: 400
                        },
                        {
                            field: "lastDisplayed",
                            title: "Last Displayed",
                            width: 100
                            //format: "{0:M/d/yyyy}"
                        }
                    ],
                    change: function (e) {
                        //console.log('PortalValueGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('PortalValueGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('PortalValueGridView:def:onShow:onEdit');

                        // Disable the valueID editor
                        e.container.find("input[name='valueID']").prop("disabled", true);

                        e.container.find("input[name='ID']").prop("disabled", true);

                        // Make edit/delete invisible
                        e.container.find(".k-edit-label:first").hide();
                        e.container.find(".k-edit-field:first").hide();
                        e.container.find(".k-edit-label:nth-child(4)").hide();
                        e.container.find(".k-edit-field:nth-child(4)").hide();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PortalValueGridView:getData');

                //var state = {};
                //state.page = 1;
                //state.pageSize = 40;

                var grid = this.$("#value").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalValueGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#value").parent().parent().height();
                this.$("#value").height(parentHeight);

                parentHeight = parentHeight - 65; //117

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalValueGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalValueGridView:resize:headerHeight:' + headerHeight);
                this.$("#value").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalValueGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#value").data('ui-tooltip'))
                    this.$("#value").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });