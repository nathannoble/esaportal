define(['App', 'backbone', 'marionette', 'jquery', 'models/NoteModel', 'hbs!templates/noteCollection',
        'views/NoteView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, NoteModel, template,
              NoteView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: NoteView,
            initialize: function (options) {
                //console.log('NoteCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.notes = [];

                var self = this;

                this.clientID = parseInt(App.model.get('selectedClientId'),0); //2189;
                this.employeeID =  parseInt(App.model.get('selectedEmployeeId'),0); //100;
                this.companyID = parseInt(App.model.get('selectedCompanyId'),0); //4;

                this.getData();

            },
            onShow: function () {
                //console.log("-------- NoteCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                if (this.options.type === "Client") {
                    this.noteDataSource = new kendo.data.DataSource(
                        {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalClientsNote";
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:update');
                                    return App.config.DataServiceURL + "/odata/PortalClientsNote" + "(" + data.clientNoteID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:create');
                                    return App.config.DataServiceURL + "/odata/PortalClientsNote";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalClientsNote" + "(" + data.clientNoteID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "clientNoteID",
                                fields: {
                                    clientNoteID: {editable: false, defaultValue: 0, type: "number"},
                                    clientID: {type: "number"},
                                    userKey: {type: "number"},
                                    noteText: {editable: true, type: "string", validation: {required: false}},
                                    noteTimeStamp: {editable: true, type: "date", validation: {required: true}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    firstName: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: {field: "clientNoteID", dir: "desc"},
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   NoteCollectionView:noteDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingNoteWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    NoteCollectionView:noteDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   NoteCollectionView:noteDataSource:error');
                            kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                            self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        filter: {field: "clientID", operator: "eq", value: self.clientID}

                    });

                    this.noteDataSource.fetch(function () {

                    var data = this.data();
                    var noteCount = data.length;
                    //console.log('NoteCollectionView:noteDataSource:noteCount:data:' + noteCount + ":" + JSON.stringify(data));

                    $.each(data, function (index, value) {

                        self.notes.push({
                            noteID: value.clientNoteID,
                            id:value.clientID,
                            userKey:value.userKey,
                            noteTimeStamp: value.noteTimeStamp,
                            noteText: value.noteText,
                            lastName: value.lastName,
                            firstName: value.firstName
                        });
                    });

                    // Populate note collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return model.get("noteID");
                    };

                    $.each(self.notes, function (index, value) {

                        //console.log('NoteCollectionView:note:value:' + JSON.stringify(value));
                        var model = new NoteModel();

                        model.set("noteID", value.noteID);
                        model.set("userKey", value.userKey);
                        model.set("id", value.id);
                        model.set("noteTimeStamp", value.noteTimeStamp);
                        model.set("noteText", value.noteText);
                        model.set("lastName", value.lastName);
                        model.set("firstName", value.firstName);

                        // if (self.options)
                        //     model.set("isEditable", self.options.isEditable);
                        // else
                        //     model.set("isEditable", false);

                        self.collection.push(model);
                    });

                });

                } else if (this.options.type === "Company") {

                    this.noteDataSource = new kendo.data.DataSource(
                        {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalCompaniesNote";
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:update');
                                    return App.config.DataServiceURL + "/odata/PortalCompaniesNote" + "(" + data.companyNoteID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:create');
                                    return App.config.DataServiceURL + "/odata/PortalCompaniesNote";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalCompaniesNote" + "(" + data.companyNoteID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyNoteID",
                                fields: {
                                    companyNoteID: {editable: false, defaultValue: 0, type: "number"},
                                    companyID: {type: "number"},
                                    userKey: {type: "number"},
                                    noteText: {editable: true, type: "string", validation: {required: false}},
                                    noteTimeStamp: {editable: true, type: "date", validation: {required: true}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    firstName: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: {field: "companyNoteID", dir: "desc"},
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   NoteCollectionView:noteDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingNoteWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    NoteCollectionView:noteDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   NoteCollectionView:noteDataSource:error');
                            kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                            self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        filter: {field: "companyID", operator: "eq", value: self.companyID}

                    });

                    this.noteDataSource.fetch(function () {

                        var data = this.data();
                        var noteCount = data.length;
                        //console.log('NoteCollectionView:noteDataSource:noteCount:data:' + noteCount + ":" + JSON.stringify(data));
                        //var state = {};
                        //var filters = [];

                        $.each(data, function (index, value) {

                            self.notes.push({
                                noteID: value.companyNoteID,
                                id:value.companyID,
                                userKey:value.userKey,
                                noteTimeStamp: value.noteTimeStamp,
                                noteText: value.noteText,
                                lastName: value.lastName,
                                firstName: value.firstName
                            });
                        });

                        // Populate note collection
                        self.collection.reset();
                        self.collection.comparator = function (model) {
                            return model.get("noteID");
                        };

                        $.each(self.notes, function (index, value) {

                            //console.log('NoteCollectionView:note:value:' + JSON.stringify(value));
                            var model = new NoteModel();

                            model.set("noteID", value.noteID);
                            model.set("userKey", value.userKey);
                            model.set("id", value.id);
                            model.set("noteTimeStamp", value.noteTimeStamp);
                            model.set("noteText", value.noteText);
                            model.set("lastName", value.lastName);
                            model.set("firstName", value.firstName);

                            // if (self.options)
                            //     model.set("isEditable", self.options.isEditable);
                            // else
                            //     model.set("isEditable", false);

                            self.collection.push(model);
                        });

                    });

                }  else if (this.options.type === "Employee") {

                    this.noteDataSource = new kendo.data.DataSource(
                        {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalEmployeesNote";
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:update');
                                    return App.config.DataServiceURL + "/odata/PortalEmployeesNote" + "(" + data.employeeNoteID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:create');
                                    return App.config.DataServiceURL + "/odata/PortalEmployeesNote";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('NoteCollectionView:noteDataSource:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalEmployeesNote" + "(" + data.employeeNoteID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeNoteID",
                                fields: {
                                    employeeNoteID: {editable: false, defaultValue: 0, type: "number"},
                                    employeeID: {type: "number"},
                                    userKey: {type: "number"},
                                    noteText: {editable: true, type: "string", validation: {required: false}},
                                    noteTimeStamp: {editable: true, type: "date", validation: {required: true}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    firstName: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: {field: "employeeNoteID", dir: "desc"},
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   NoteCollectionView:noteDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingNoteWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    NoteCollectionView:noteDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   NoteCollectionView:noteDataSource:error');
                            kendo.ui.progress(self.$("#loadingNoteWidget"), false);

                            self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        filter: {field: "employeeID", operator: "eq", value: self.employeeID}

                    });

                    this.noteDataSource.fetch(function () {

                        var data = this.data();
                        var noteCount = data.length;
                        //console.log('NoteCollectionView:noteDataSource:noteCount:data:' + noteCount + ":" + JSON.stringify(data));
                        //var state = {};
                        //var filters = [];

                        $.each(data, function (index, value) {

                            self.notes.push({
                                noteID: value.employeeNoteID,
                                id:value.employeeID,
                                userKey:value.userKey,
                                noteTimeStamp: value.noteTimeStamp,
                                noteText: value.noteText,
                                lastName: value.lastName,
                                firstName: value.firstName
                            });
                        });

                        // Populate note collection
                        self.collection.reset();
                        self.collection.comparator = function (model) {
                            return model.get("noteID");
                        };

                        $.each(self.notes, function (index, value) {

                            //console.log('NoteCollectionView:note:value:' + JSON.stringify(value));
                            var model = new NoteModel();

                            model.set("noteID", value.noteID);
                            model.set("userKey", value.userKey);
                            model.set("id", value.id);
                            model.set("noteTimeStamp", value.noteTimeStamp);
                            model.set("noteText", value.noteText);
                            model.set("lastName", value.lastName);
                            model.set("firstName", value.firstName);

                            // if (self.options)
                            //     model.set("isEditable", self.options.isEditable);
                            // else
                            //     model.set("isEditable", false);

                            self.collection.push(model);
                        });

                    });
                }

            },
            hideNoDataOverlay: function () {
                //console.log('NoteCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#noteWidgetContent').css('display', 'block');
            },
            displayNoDataOverlay: function () {
                //console.log('NoteCollectionView:displayNoDataOverlay');

                // Hide the widget content
                //this.$('#noteWidgetContent').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {
                //console.log('NoteCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('NoteCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- NoteCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
