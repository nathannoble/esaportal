define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalTipGrid', 'hbs!templates/editPortalTipPopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template, EditPortalTipPopup) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalTipGridView:initialize');

                _.bindAll(this);

                var self = this;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalTip",
                                complete: function (e) {
                                    console.log('PortalTipGridView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalTipGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalTipGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalTip" + "(" + data.tipID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalTipGridView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalTipGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalTipGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalTip";
                                },
                                complete: function (e) {
                                    console.log('PortalTipGridView:dataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalTipGridView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalTipGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalTip" + "(" + data.tipID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalTipGridView:dataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalTipGridView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "tipID",
                                fields: {
                                    tipID: {editable: false, type: "number"},
                                    tipText: {editable: true, type: "string", validation: {required: false}},
                                    lastDisplayed: {editable: true, type: "string", validation: {required: true}}
                                }
                            }
                        },
                        sort: {field: "lastDisplayed", dir: "desc"},
                        requestStart: function (e) {
                            //console.log('PortalTipGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#tipLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalTipGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#tipLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalTipGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalTipGridView:dataSource:error');
                            kendo.ui.progress(self.$("#tipLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalTipGridView:displayNoDataOverlay');

                this.$el.css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No field selected</div></div></div>').appendTo(this.$el.parent());

            },
            onRender: function () {
                //console.log('PortalTipGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalTipGridView:onShow --------");
                var self = this;

                // Panel title
                $("#tipTitle").html("Tips & Tricks");

                this.grid = this.$("#tip").kendoGrid({

                    toolbar: ["create"],
                    editable: "popup",  //inline, popup
                    sortable: true,
                    extra: false,
                    //selectable: "multiple",
                    resizable: true,
                    reorderable: true,
                    groupable: true,
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction k-grid-edit' data-id='" + e.tipID + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                                //fa-edit
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.tipID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "tipID",
                            title: "ID",
                            width: 75
                        }, {
                            field: "tipText",
                            title: "Text",
                            width: 500
                            //locked: true
                        },
                        {
                            field: "lastDisplayed",
                            title: "Last Displayed",
                            width: 100
                            //locked: true
                        }
                    ],
                    change: function (e) {
                        //console.log('PortalTipGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('PortalTipGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('PortalTipGridView:def:onShow:onEdit');

                        // Disable the tipID editor
                        e.container.find("input[name='tipID']").prop("disabled", true);

                        // Make edit/delete invisible
                        e.container.find(".k-edit-label:first").hide();
                        e.container.find(".k-edit-field:first").hide();
                        e.container.find(".k-edit-label:nth-child(4)").hide();
                        e.container.find(".k-edit-field:nth-child(4)").hide();

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PortalTipGridView:getData');

                var grid = this.$("#tip").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalTipGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#tip").parent().parent().height();
                this.$("#tip").height(parentHeight);

                parentHeight = parentHeight - 70; //117

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalTipGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalTipGridView:resize:headerHeight:' + headerHeight);
                this.$("#tip").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalTipGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });