define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/timeCard', 'views/PortalTimeCardDetailGridView',
        'jquery.cookie', 'kendo/kendo.data', 'kendo/kendo.combobox', 'kendo/kendo.datepicker', 'kendo/kendo.datetimepicker'],
    function (App, $, Backbone, Marionette, template, PortalTimeCardDetailGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #SaveButton': 'saveButtonClicked',
                'click #DeleteButton': 'deleteButtonClicked',
                'click #CancelButton': 'cancelButtonClicked'
            },
            regions: {
                timeCardRegion: "#mini-time-card-region"
            },
            initialize: function (options) {
                //console.log('TimeCardView:initialize');

                _.bindAll(this);

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "timerID"};
                }

                //this.deleteRecord = false;
                this.recordTasksMtgs = true;

                this.userType = parseInt(App.model.get('userType'), 0);
                //console.log('TimeCardView:initialize:userType:' + self.userType);

                this.clientIDOriginal = 0;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.myEmployeeId = App.model.get('myEmployeeId');
                    if (this.myEmployeeId !== null && this.myEmployeeId !== undefined) {
                        this.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);
                    }

                    // Subscribe to browser events
                    $(window).on("resize", this.onResize);
                }
            },
            onRender: function () {
                //console.log('TimeCardView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- TimeCardView:onShow --------");
                var self = this;

                self.$('#SaveButton').html("Update");

                this.timeCardRegion.show(new PortalTimeCardDetailGridView({type: 'oneTimerRecord'}));

                if (self.userType === 1 || self.userType === 2 ) {
                    self.$('#executiveButtons').addClass("hidden");
                } else {
                    self.$('#nonExecutiveButtons').addClass("hidden");
                }

                this.selectedTimerId = parseInt(App.model.get('selectedTimerId'), 0);

                //console.log('TimeCardView:initialize:selectedTimerId:' + this.selectedTimerId);

                if (this.options.type === "intTimerID") {
                    this.filter = {field: "intTimerID", operator: "eq", value: self.selectedTimerId};
                } else {
                    this.filter = {field: "timerID", operator: "eq", value: self.selectedTimerId};
                }

                this.getData();

            },
            getData: function () {

                var self = this;

                this.serviceDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalServiceItem";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:serviceDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:serviceataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "serviceID",
                                fields: {
                                    serviceID: {editable: true, type: "number"},
                                    ordering: {editable: false, type: "number"},
                                    serviceItem: {editable: true, type: "string", validation: {required: false}},
                                    recordTasksMtgs: {editable: true, type: "boolean"},
                                    toStats: {editable: true, type: "boolean"},
                                    timerStatus: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   Widget1View:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimeCardInfo"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    Widget1View:requestEnd');
                            //kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   Widget1View:error');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);

                            //App.modal.show(new ErrorView());

                        },
                        sort: {field: "ordering", dir: "asc"}
                    });

                this.progressClientDS = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        serverFiltering: true,
                        serverSorting: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:progressClientDS:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "clientID",
                                fields: {
                                    clientID: {editable: true, type: "number"},
                                    schedulerID: {editable: false, defaultValue: 0, type: "number"},
                                    //schedulerName: {editable: true, type: "string", validation: {required: false}},
                                    companyID: {editable: false, defaultValue: 0, type: "number"},
                                    //clientName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    taskTracking: {editable: true, type: "boolean"}
                                }
                            }
                        },
                        requestStart: function () {
                            //console.log('   ProgressClientDataGridView:requestStart');

                            kendo.ui.progress($("#loadingTimeCardInfo"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    ProgressClientDataGridView:requestEnd');
                            kendo.ui.progress($("#loadingTimeCardInfo"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ProgressClientDataGridView:error');
                            kendo.ui.progress($("#loadingTimeCardInfo"), false);

                        },
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"},
                            {field: "clientCompany", dir: "asc"}
                        ],
                        filter: {field: "status", operator: "eq", value: '4 - Active'}

                    });

                this.employeeDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('TimeCardView:employeeDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:employeeDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    title: {editable: true, type: "string", validation: {required: false}},
                                    userKey: {editable: true, type: "number"},
                                    teamLeaderID: {editable: true, type: "number"},
                                    timeZoneID: {editable: true, type: "number"},
                                    eeTimeZone: {editable: true, type: "number"},
                                    eeTimeZoneOffset: {editable: true, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   TimeCardView:requestStart');

                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    TimeCardView:requestEnd');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   TimeCardView:error');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);

                        }
                        //filter: [{field: "employeeID", operator: "eq", value: self.employeeID}]

                    });

                this.timerIntTimerIdDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalTimer",
                                complete: function (e) {
                                    console.log('TimeCardView:timerIntTimerIdDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timerIntTimerIdDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('TimeCardView:update');
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.timerID + ")";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:timerIntTimerIdDataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timerIntTimerIdDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json",
                                //contentType: "application/json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('TimeCardView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.intTimerID + ")";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:timerIntTimerIdDataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timerIntTimerIdDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    idTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    entryType: {editable: true, type: "string", validation: {required: false}},
                                    employeeID: {editable: true, type: "number"},
                                    clientID: {editable: true, type: "number"},
                                    serviceID: {editable: true, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: true},
                                        format: "{0:yyyy-MM-dd}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date",
                                        format: "{0:T}" //{0:s}
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date",
                                        format: "{0:T}"
                                    },
                                    timeWorked: {editable: true, type: "number"},
                                    timeWorkedText: {editable: true, type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    intTimerID: {editable: true, type: "number"},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}},
                                    editTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    editEmployeeID: {editable: true, type: "number"},
                                    tasksCalls: {editable: true, type: "number"},
                                    tasksEmails: {editable: true, type: "number"},
                                    tasksVoiceMails: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "timerID", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('TimeCardView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('TimeCardView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);
                        },
                        error: function (e) {
                            //console.log('TimeCardView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('TimeCardView:change');

                        },
                        serverFiltering: true,
                        serverSorting: true,
                        filter: {field: "intTimerID", operator: "eq", value: self.selectedTimerId}
                    });

                this.timerDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalTimer",
                                complete: function (e) {
                                    console.log('TimeCardView:timerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timeDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('TimeCardView:update');
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.timerID + ")";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:timerDataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timeDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json",
                                //contentType: "application/json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('TimeCardView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalTimer" + "(" + data.intTimerID + ")";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:timerDataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timeDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    idTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    entryType: {editable: true, type: "string", validation: {required: false}},
                                    employeeID: {editable: true, type: "number"},
                                    clientID: {editable: true, type: "number"},
                                    serviceID: {editable: true, type: "number"},
                                    workDate: {editable: true, type: "string", validation: {required: false}},
                                    startTime: {
                                        editable: true,
                                        type: "date",
                                        format: "{0:T}" //{0:s}
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date",
                                        format: "{0:T}"
                                    },
                                    timeWorked: {editable: true, type: "number"},
                                    timeWorkedText: {editable: true, type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    intTimerID: {editable: true, type: "number"},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}},
                                    editTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    editEmployeeID: {editable: true, type: "number"},
                                    tasksCalls: {editable: true, type: "number"},
                                    tasksEmails: {editable: true, type: "number"},
                                    tasksVoiceMails: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "timerID", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('TimeCardView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('TimeCardView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);
                        },
                        error: function (e) {
                            //console.log('TimeCardView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingTimeCardInfo"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('TimeCardView:change');

                        },
                        serverFiltering: true,
                        serverSorting: true,
                        filter: self.filter
                    });

                this.adminDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalAdmin";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:adminDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    return App.config.DataServiceURL + "/odata/PortalAdmin" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:adminDataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:adminDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, defaultValue: 0, type: "number"},
                                    payrollLockDate: {editable: true, type: "date", validation: {required: false}}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function (e) {
                            //console.log('TimeCardView:dataSource:request start:');
                            //kendo.ui.progress(self.$("#companyLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('TimeCardView:dataSource:request end:');
                            //kendo.ui.progress(self.$("#companyLoading"), false);
                        },
                        error: function (e) {
                            //console.log('TimeCardView:dataSource:error');
                            //kendo.ui.progress(self.$("#companyLoading"), false);
                        }
                        //filter: [{field: "status", operator: "eq", value: '4 - Active'}],
                        //sort: [{field: "lastName", dir: "asc"}]
                    });

                this.timerSummaryDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        //type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/rest/GetTimeCardDetailForOneIntTimerID/" + self.selectedTimerId;
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:timerSummaryDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timerSummaryDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, defaultValue: 0, type: "number"},
                                    intTimerID: {editable: false, defaultValue: 0, type: "number"},
                                    timeWorked: {type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function (e) {
                            //console.log('TimeCardView:dataSource:request start:');
                            //kendo.ui.progress(self.$("#companyLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('TimeCardView:dataSource:request end:');
                            //kendo.ui.progress(self.$("#companyLoading"), false);
                        },
                        error: function (e) {
                            //console.log('TimeCardView:dataSource:error');
                            //kendo.ui.progress(self.$("#companyLoading"), false);
                        }
                        //filter: [{field: "status", operator: "eq", value: '4 - Active'}],
                        //sort: [{field: "lastName", dir: "asc"}]

                    });

                this.adminDataSource.fetch(function () {

                    //console.log('TimeCardView:onShow:adminDataSource');

                    var records = this.data();

                    if (records.length > 0) {

                        var record = records[0];
                        self.payrollLockDate = record.payrollLockDate;
                        self.payrollLockDateFormatted = moment(record.payrollLockDate).clone().format('M/D/YYYY');
                    }

                    self.timerSummaryDataSource.fetch(function () {
                        var summaryData = this.data();
                        var summaryCount = summaryData.length;
                        var summaryRecord;

                        if (summaryCount > 0) {
                            summaryRecord = summaryData[0];
                        } else {
                            alert("No summary record exists for this time card id.");
                            return;
                        }

                        self.timerDataSource.fetch(function () {

                            var timerRecords = this.data();
                            var count = timerRecords.length;
                            var record;


                            //console.log('TimeCardView:dataSource:fetch:count:' + count);
                            if (count > 0) {

                                record = timerRecords[0];
                                //console.log('TimeCardView:dataSource:record:' + JSON.stringify(record));

                            } else {
                                alert("No timecard record exists for this time card id.");
                                return;
                            }

                            // Lock some fields (service, client, time) if this date is before the payroll lock date
                            self.$('#DeleteButton').removeClass('disabled');
                            if (moment(record.workDate).isBefore(self.payrollLockDate)) {
                                self.$("#service").prop('readonly', true);
                                self.$("#client").prop('readonly', true);
                                self.$("#timeWorked").prop('readonly', true);
                                self.$('#DeleteButton').addClass('disabled');
                            }

                            self.serviceDataSource.fetch(function () {

                                self.serviceRecords = this.data();

                                self.$("#service").kendoDropDownList({
                                    dataValueField: "serviceID",
                                    dataTextField: "serviceItem",
                                    dataSource: self.serviceRecords,
                                    value: record.serviceID
                                    //change: self.onServiceIdChanged
                                });

                            });

                            self.employeeDataSource.filter({
                                field: "employeeID",
                                operator: "eq",
                                value: record.employeeID
                            });
                            self.employeeDataSource.fetch(function () {

                                var data = this.data();

                                if (data.length > 0) {
                                    var empRecord = data[0];
                                    var employeeName = empRecord.lastName + ", " + empRecord.firstName;
                                    self.$("#employee").data("kendoMaskedTextBox").value(employeeName);
                                }

                            });

                            // Text boxes
                            self.$(".k-textbox").kendoMaskedTextBox();
                            self.$(".k-numerictextbox").kendoNumericTextBox();

                            self.clientNameRecords = App.model.get('clientNameRecords');

                            if (self.clientNameRecords.length === 0) {

                                self.progressClientDS.fetch(function (data) {

                                    self.clientNameRecords = this.data();

                                    //console.log('TimeCardView:onServiceIdChanged:data:' + JSON.stringify(self.clientNameRecords));

                                    var modifiedRecords = [];
                                    $.each(self.clientNameRecords, function (index, value) {

                                        var record = value;
                                        var newRecord = {
                                            // From ProgressClient
                                            clientID: record.clientID,
                                            companyID: record.companyID,
                                            name: record.lastName + ", " + record.firstName + ": " + record.clientCompany,
                                            taskTracking: record.taskTracking
                                        };
                                        modifiedRecords.push(newRecord);

                                    });

                                    $("#client").kendoDropDownList({
                                        dataValueField: "clientID",
                                        dataTextField: "name",
                                        dataSource: modifiedRecords,
                                        value: record.clientID
                                    });

                                    self.clientIDOriginal = parseInt(record.clientID, 0);
                                    App.model.set('clientNameRecords', self.clientNameRecords);

                                    var companyName = "";
                                    var taskTracking = false;
                                    var ddl = $("#client").data("kendoDropDownList");
                                    if (ddl !== undefined && ddl !== null) {
                                        companyName = ddl.dataItem().name;
                                        taskTracking = ddl.dataItem().taskTracking;
                                    }
                                    if (taskTracking === true) { //companyName.indexOf("LFD") > 0, this.companyID === 12

                                        self.$("#tasksCalls").data("kendoMaskedTextBox").value(summaryRecord.tasksCalls);
                                        self.$("#tasksVoiceMails").data("kendoMaskedTextBox").value(summaryRecord.tasksVoiceMails);
                                        self.$("#tasksEmails").data("kendoMaskedTextBox").value(summaryRecord.tasksEmails);

                                    } else {
                                        self.$("#tasksCallsLabels").hide();
                                        self.$("#tasksVoiceMailsLabels").hide();
                                        self.$("#tasksEmailsLabels").hide();

                                        self.$("#tasksCalls").hide();
                                        self.$("#tasksVoiceMails").hide();
                                        self.$("#tasksEmails").hide();
                                    }

                                });
                            } else {

                                var modifiedRecords = [];
                                $.each(self.clientNameRecords, function (index, value) {

                                    var record = value;
                                    var newRecord = {
                                        // From ProgressClient
                                        clientID: record.clientID,
                                        companyID: record.companyID,
                                        name: record.lastName + ", " + record.firstName + ": " + record.clientCompany,
                                        taskTracking: record.taskTracking
                                    };
                                    modifiedRecords.push(newRecord);

                                });

                                $("#client").kendoDropDownList({
                                    dataValueField: "clientID",
                                    dataTextField: "name",
                                    dataSource: modifiedRecords,
                                    value: record.clientID
                                });

                                self.clientIDOriginal = parseInt(record.clientID, 0);

                                var companyName = "";
                                var taskTracking = false;
                                var ddl = $("#client").data("kendoDropDownList");
                                if (ddl !== undefined && ddl !== null) {
                                    companyName = ddl.dataItem().name;
                                    taskTracking = ddl.dataItem().taskTracking;
                                }
                                if (taskTracking === true) { //companyName.indexOf("LFD") > 0, this.companyID === 12

                                    self.$("#tasksCalls").data("kendoMaskedTextBox").value(summaryRecord.tasksCalls);
                                    self.$("#tasksVoiceMails").data("kendoMaskedTextBox").value(summaryRecord.tasksVoiceMails);
                                    self.$("#tasksEmails").data("kendoMaskedTextBox").value(summaryRecord.tasksEmails);

                                } else {
                                    self.$("#tasksCallsLabels").hide();
                                    self.$("#tasksVoiceMailsLabels").hide();
                                    self.$("#tasksEmailsLabels").hide();

                                    self.$("#tasksCalls").hide();
                                    self.$("#tasksVoiceMails").hide();
                                    self.$("#tasksEmails").hide();
                                }

                            }

                            self.isTimerRecordOpen = false;
                            $.each(timerRecords, function (index, value) {
                                var record = value;
                                var timerAction = "";
                                if (record.timerAction !== null) {
                                    timerAction = record.timerAction;
                                }
                                if (timerAction.indexOf("none") < 0 || record.recordType.indexOf("NORM") >= 0 && record.recordType.indexOf("STOP") < 0) {
                                    self.isTimerRecordOpen = true;
                                    return;
                                }
                            });

                            if (self.isTimerRecordOpen) {
                                self.$('#SaveButton').html('Close Timer');
                            } else {
                                self.$('#SaveButton').html('Update');
                            }


                            var workDate = record.workDate;
                            var date = moment(workDate).clone().format('YYYY-MM-DD');

                            // Dates
                            $("#workDate").kendoDatePicker({
                                format: "yyyy-MM-dd",
                                value: date //record.workDate
                            });

                            self.workDateOriginal = self.$("#workDate").val();

                            self.$("#intTimerID").data("kendoMaskedTextBox").value(record.intTimerID);
                            self.$("#payrollLock").data("kendoMaskedTextBox").value(self.payrollLockDateFormatted);

                            if (record.startTime > 0) {
                                self.$("#startTime").data("kendoMaskedTextBox").value(record.startTime);
                            } else {
                                self.$("#startTime").data("kendoMaskedTextBox").value("--");
                            }

                            if (record.stopTime > 0) {
                                self.$("#stopTime").data("kendoMaskedTextBox").value(record.stopTime);
                            } else {
                                self.$("#stopTime").data("kendoMaskedTextBox").value("--");
                            }

                            self.$("#mgrNote").data("kendoMaskedTextBox").value(record.mgrNote);
                            self.$("#timerNote").data("kendoMaskedTextBox").value(record.timerNote);
                            self.$("#editTimeStamp").data("kendoMaskedTextBox").value(record.editTimeStamp);
                            self.$("#editEmployeeID").data("kendoMaskedTextBox").value(record.editEmployeeID);

                            self.intTimerIDOtherTimeWorked = summaryRecord.timeWorked - record.timeWorked;
                            self.intTimerIDOtherTasks = summaryRecord.tasks - record.tasks;
                            self.intTimerIDOtherMeetings = summaryRecord.meetings - record.meetings;
                            self.intTimerIDOtherTasksCalls = summaryRecord.tasksCalls - record.tasksCalls;
                            self.intTimerIDOtherTasksVoiceMails = summaryRecord.tasksVoiceMails - record.tasksVoiceMails;
                            self.intTimerIDOtherTasksEmails = summaryRecord.tasksEmails - record.tasksEmails;

                            self.$("#timeWorked").data("kendoMaskedTextBox").value(summaryRecord.timeWorked);
                            self.$("#tasks").data("kendoMaskedTextBox").value(summaryRecord.tasks);
                            self.$("#meetings").data("kendoMaskedTextBox").value(summaryRecord.meetings);

                        });
                    });
                });

            },
            writeLog: function (text) {
                console.log('TimeCardView:writeLog');

                var self = this;
                self.timerLogDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        batch: false,
                        pageSize: 10,
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalTimerLog";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:timerLogDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timerLogDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            create: {
                                url: function (data) {
                                    //console.log('TimeCardView timerLogDataSourceClient:create');
                                    return App.config.DataServiceURL + "/odata/PortalTimerLog";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "logID",
                                fields: {
                                    logID: {editable: false, defaultValue: 0, type: "number"},
                                    logDate: {editable: true, type: "date", validation: {required: false}},
                                    timerID: {editable: true, defaultValue: 0, type: "number"},
                                    logText: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        requestStart: function () {
                            //console.log('  TimeCardView:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   TimeCardView:requestEnd');
                            //kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        error: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            console.log('TimeCardView:writeLog:timerLogDataSource:error:e:' + JSON.stringify(e));
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                            //App.modal.show(new ErrorView());

                        }
                    });

                self.timerLogDataSource.fetch(function () {

                    var offset = (new Date()).getTimezoneOffset();
                    var now = new Date(moment().clone().subtract((offset), 'minutes'));

                    var logText = "selectedTimerId: " + self.selectedTimerId + "\r\n";
                    //logText += ":intTimerID: " + self.intTimerID + "\r\n";
                    logText += ":" + text;

                    self.timerLogDataSource.add({
                        logDate: now,
                        timerID: self.selectedTimerId,
                        logText: logText
                    });

                    console.log('TimeCardView:writeLog:started');
                    $.when(this.sync()).done(function (e) {
                        console.log('TimeCardView:writeLog:done');
                    });
                });
            },
            getServiceRecord: function (serviceID) {
                //console.log('TimeCardView:getServiceRecord:serviceID:' + serviceID);

                var self = this;

                var service = $.grep(self.serviceRecords, function (element, index) {
                    return element.serviceID == serviceID;
                });

                if (service.length > 0) {
                    return service[0];
                } else {
                    return null;
                }

            },
            cancelButtonClicked: function (e) {
                //console.log('TimeCardView:cancelButtonClicked');
                var self = this;

                self.$('.btn').addClass('disabled');

                if (self.isTimerRecordOpen === true) {
                    App.router.navigate('openTimers', {trigger: true});
                } else {
                    App.router.navigate('statsTimecardNoDateChange', {trigger: true});
                }

            },
            deleteButtonClicked: function (e) {
                //console.log('TimeCardView:deleteButtonClicked');

                var self = this;

                if (confirm("Are you sure you want to delete?")) {

                    self.$('.btn').addClass('disabled');

                    this.timerDataSource.fetch(function () {
                        var data = this.data();
                        var count = data.length;

                        if (count > 0) {

                            var record = self.timerDataSource.at(0);

                            self.timerDataSource.remove(record);

                            $.when(this.sync()).done(function (e) {

                                self.deleteRecord = true;
                                self.resetStatsDashboardCache();
                                self.populateProgressData("current");

                            });

                        }
                    });
                }

            },
            saveButtonClicked: function (e) {
                //console.log('TimeCardView:saveButtonClicked');

                var self = this;
                var app = App;

                self.$('.btn').addClass('disabled');

                var workDate = self.$("#workDate").val();
                if (workDate !== null && workDate !== "") {
                    workDate = new Date(workDate).toISOString();
                } else {
                    workDate = null;
                    alert("Work Date must be populated in order to save this time card record.");
                    self.$('.btn').removeClass('disabled');
                    return;
                }
                var workDateTest = self.$("#workDate").val();
                if (workDateTest !== moment(workDateTest).clone().format('YYYY-MM-DD')) {
                    alert("Work Date must have format: YYYY-MM-DD in order to save this time card record.");
                    self.$('.btn').removeClass('disabled');
                    return;
                }

                this.timerDataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count > 0) {

                        var record = self.timerDataSource.at(0);
                        var recordLast = self.timerDataSource.at(count - 1);

                        //self.isTimerRecordOpen = false;
                        //if (record.timerAction.indexOf("none") >= 0 || (record.recordType.indexOf("NORM") >= 0 && record.recordType.indexOf("STOP") < 0)) {
                        //    self.isTimerRecordOpen = true;
                        //}
                        var offset = (new Date()).getTimezoneOffset();
                        var now = new Date(moment().clone().subtract((offset), 'minutes'));

                        var startTime = new Date(moment(record.startTime).clone().subtract((offset), 'minutes'));
                        var stopTime = new Date(moment(record.stopTime).clone().subtract((offset), 'minutes'));

                        record.set("startTime", startTime);
                        record.set("stopTime", stopTime);

                        record.set("idTimeStamp", now);
                        record.set("workDate", self.$("#workDate").val());

                        var timeWorkedDiff = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(self.$("#timeWorked").val() - self.intTimerIDOtherTimeWorked);
                        record.set("timeWorked", timeWorkedDiff);

                        // Update timeWorkedText also to sync with new timeWorked value
                        var hours = parseInt(timeWorkedDiff,0);
                        var minutes = Math.abs(parseInt((timeWorkedDiff - (hours))*60,0));
                        //var seconds = parseInt(((timeWorkedDiff - hours)*60 - minutes)*60,0);


                        console.log('TimeCardView:saveButtonClicked:timeWorkedDiff:hours:minutes:' + timeWorkedDiff + ":" + hours + ":" + minutes);
                        // If less than 1 minute, make it 1 minute or 1/100th hour
                        var timeWorkedText = "0:1";
                        if (Math.abs(hours*60) + Math.abs(minutes) >= 1) {
                            timeWorkedText = (hours + ":" + minutes).toString();
                        }
                        record.set("timeWorkedText", timeWorkedText);

                        var tasksDiff = self.$("#tasks").val() - self.intTimerIDOtherTasks;
                        record.set("tasks", tasksDiff);

                        var meetingsDiff = self.$("#meetings").val() - self.intTimerIDOtherMeetings;
                        record.set("meetings", meetingsDiff);

                        var tasksCallsDiff = self.$("#tasksCalls").val() - self.intTimerIDOtherTasksCalls;
                        record.set("tasksCalls", tasksCallsDiff);

                        var tasksEmailsDiff = self.$("#tasksEmails").val() - self.intTimerIDOtherTasksEmails;
                        record.set("tasksEmails", tasksEmailsDiff);

                        var tasksVoiceMailsDiff = self.$("#tasksVoiceMails").val() - self.intTimerIDOtherTasksVoiceMails;
                        record.set("tasksVoiceMails", tasksVoiceMailsDiff);

                        record.set("mgrNote", self.$("#mgrNote").val());
                        record.set("timerNote", self.$("#timerNote").val());

                        record.set("editTimeStamp", now);
                        record.set("editEmployeeID", self.myEmployeeId);

                        if (self.isTimerRecordOpen === true) {

                            var startTimeLast = new Date(moment(recordLast.startTime).clone().subtract((offset), 'minutes'));
                            var stopTimeLast = new Date(moment(recordLast.stopTime).clone().subtract((offset), 'minutes'));
                            var idTimeStamp = new Date(moment(recordLast.idTimeStamp).clone().subtract((offset), 'minutes'));

                            recordLast.set("startTime", startTimeLast);
                            recordLast.set("stopTime", stopTimeLast);
                            recordLast.set("idTimeStamp", idTimeStamp);

                            recordLast.set("timerAction", "none");
                            if (recordLast.recordType.indexOf("-STOP") < 0) {
                                //recordLast.recordType = recordLast.recordType + "-STOP";
                                recordLast.set("recordType", recordLast.recordType + "-STOP");
                            }
                        }

                        //console.log('TimeCardView:timerDataSource:save:recordToSave:' + JSON.stringify(record));
                        $.when(this.sync()).done(function (e) {
                            self.timerIntTimerIdDataSource.fetch(function () {
                                var data2 = this.data();
                                var count2 = data2.length;

                                // make sure client/service id is updated for all records with this intTimerID
                                //console.log('TimeCardView:saveButtonClicked:intTimerID records length:' + count2);

                                $.each(data2, function (index) {
                                    var record2 = self.timerIntTimerIdDataSource.at(index);
                                    //console.log('TimeCardView:saveButtonClicked:record index:' + index);

                                    //console.log('TimeCardView:saveButtonClicked:record2:' + JSON.stringify(record2));

                                    var startTime2 = new Date(moment(record2.startTime).clone().subtract((offset), 'minutes'));
                                    var stopTime2 = new Date(moment(record2.stopTime).clone().subtract((offset), 'minutes'));
                                    var idTimeStamp2 = new Date(moment(record2.idTimeStamp).clone().subtract((offset), 'minutes'));

                                    record2.set("startTime", startTime2);
                                    record2.set("stopTime", stopTime2);
                                    record2.set("idTimeStamp", idTimeStamp2);

                                    record2.set("serviceID", parseInt(self.$("#service").val(), 0));
                                    record2.set("clientID", parseInt(self.$("#client").val(), 0));
                                    record2.set("workDate", self.$("#workDate").val()); //record2.workDate);

                                    // Make sure other records remain what they were
                                    record2.set("workDate", record2.workDate);
                                    record2.set("timeWorked", record2.timeWorked);
                                    record2.set("timeWorkedText", record2.timeWorkedText);
                                    record2.set("tasks", record2.tasks);
                                    record2.set("meetings", record2.meetings);
                                    record2.set("tasksCalls", record2.tasksCalls);
                                    record2.set("tasksEmails", record2.tasksEmails);
                                    record2.set("tasksVoiceMails", record2.tasksVoiceMails);
                                    record2.set("mgrNote", record2.mgrNote);
                                    record2.set("timerNote", record2.timerNote);

                                });

                                $.when(this.sync()).done(function (e) {
                                    console.log('TimeCardView:populateProgressData:date: ' + self.$("#workDate").val());
                                    self.populateProgressData("current");
                                });
                            });
                        });
                    }
                });
            },
            populateProgressData: function (arg) {
                //console.log('TimeCardView:populateProgressData');
                var self = this;
                var app = App;

                var serviceDDL = $("#service").data("kendoDropDownList");
                var serviceDataItem = serviceDDL.dataItem();
                if (serviceDataItem !== undefined && serviceDataItem !== null) {
                    self.recordTasksMtgs = serviceDataItem.recordTasksMtgs;
                    self.toStats = serviceDataItem.toStats;
                }

                // Choose columns to populate based on serviceID
                self.serviceID = parseInt(self.$("#service").val(), 0);
                self.clientID = parseInt(self.$("#client").val(), 0);

                // First get any existing progress data records
                var offset = (new Date()).getTimezoneOffset();
                var workDate;
                if (arg === "current") {
                    workDate = new Date(moment(self.$("#workDate").val()).clone()); //.add((offset), 'minutes')
                } else {
                    workDate = new Date(moment(arg).clone());
                }
                var now = new Date(moment().clone().subtract((offset), 'minutes'));

                var workDateDay;
                if (arg === "current") {
                    workDateDay = self.$("#workDate").val();
                } else {
                    workDateDay = arg;
                }
                var workDayStart = new Date(moment(workDateDay + "T00:00:00+0000").clone()); //.add((offset), 'minutes'));
                var workDayEnd = new Date(moment(workDateDay + "T23:59:59+0000").clone()); //.add((offset), 'minutes'));

                var theseClients = [];
                theseClients.push(self.clientID);

                if (self.clientID !== self.clientIDOriginal) {
                    theseClients.push(self.clientIDOriginal);
                }

                self.progressDataSource = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressData";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:progressDataSourcee:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:progressDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('TimeCardView:update');
                                    return App.config.DataServiceURL + "/odata/ProgressData" + "(" + data.entryID + ")";
                                },
                                complete: function (e) {
                                    //console.log('TimeCardView:progressDataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                },
                                error: function (e) {
                                    //console.log('TimeCardView:progressDataSource:update:error:' + JSON.stringify(e.responseJSON));
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('TimeCardView:create:data:' + data);
                                    return App.config.DataServiceURL + "/odata/ProgressData";
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "entryID",
                                fields: {
                                    entryID: {editable: false, defaultValue: 0, type: "number"},
                                    workDate: {editable: true, type: "date", validation: {required: false}},
                                    timeStamp: {editable: true, type: "date", validation: {required: false}},
                                    schedulerTime: {type: "number"},
                                    support1Time: {type: "number"},
                                    adminTime: {type: "number"},
                                    schedulerCalls: {type: "number"},
                                    support1Calls: {type: "number"},
                                    schedulerMeetings: {type: "number"},
                                    support1Meetings: {type: "number"},
                                    compTime: {type: "number"},
                                    compCalls: {type: "number"},
                                    compMeetings: {type: "number"},
                                    referralTime: {type: "number"},
                                    referralCalls: {type: "number"},
                                    referralMeetings: {type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   TimeCardView:progressDataSource:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimeCard"), true);
                        },
                        requestEnd: function () {

                            //console.log('    TimeCardView:progressDataSource:requestEnd');
                            //kendo.ui.progress(self.$("#loadingTimeCard"), false);

                        },
                        error: function () {

                            //console.log('   TimeCardView:progressDataSource:error');
                            //kendo.ui.progress(self.$("#loadingTimeCard"), false);

                        },
                        filter: [
                            {
                                logic: "or",
                                filters: [
                                    {field: "clientID", operator: "eq", value: self.clientID},
                                    {field: "clientID", operator: "eq", value: self.clientIDOriginal}
                                ]
                            },
                            {field: "workDate", operator: "gte", value: workDayStart},
                            {field: "workDate", operator: "lte", value: workDayEnd}

                        ]
                        //filter: [
                        //    //{field: "clientID", operator: "eq", value: self.clientID},
                        //    {field: "workDate", operator: "gte", value: workDayStart},
                        //    {field: "workDate", operator: "lte", value: workDayEnd}
                        //]
                    });

                self.year = workDate.getFullYear();
                self.month = workDate.getMonth() + 1;
                self.day = workDate.getDate();

                self.timerClientTotalDataSource = new kendo.data.DataSource(
                    {
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/PortalTimerTotalClientTime";
                                },
                                complete: function (e) {
                                    console.log('TimeCardView:timerClientTotalDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'TimeCardView:timerClientTotalDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "rowID",
                                fields: {
                                    rowID: {editable: false, defaultValue: 0, type: "number"},
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    workDate: {editable: true, type: "date", validation: {required: false}},
                                    totalTimeWorked: {editable: false, defaultValue: 0, type: "number"},
                                    totalTasks: {editable: false, defaultValue: 0, type: "number"},
                                    totalMeetings: {editable: false, defaultValue: 0, type: "number"},
                                    workYear: {editable: false, defaultValue: 0, type: "number"},
                                    workMonth: {editable: false, defaultValue: 0, type: "number"},
                                    workDay: {editable: false, defaultValue: 0, type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   TimeCardView:requestStart');

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    TimeCardView:requestEnd');
                            kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //kendo.ui.progress(self.$("#loadingTimerWidget"), false);

                        },
                        filter: [
                            {
                                logic: "or",
                                filters: [
                                    {field: "clientID", operator: "eq", value: self.clientID},
                                    {field: "clientID", operator: "eq", value: self.clientIDOriginal}
                                ]
                            },
                            {field: "workYear", operator: "eq", value: self.year},
                            {field: "workMonth", operator: "eq", value: self.month},
                            {field: "workDay", operator: "eq", value: self.day}
                        ]
                        //filter: [
                        //    {field: "clientID", operator: "eq", value: clientID},
                        //    {field: "workYear", operator: "eq", value: self.year},
                        //    {field: "workMonth", operator: "eq", value: self.month},
                        //    {field: "workDay", operator: "eq", value: self.day}
                        //]
                    });

                self.timerClientTotalDataSource.fetch(function () {

                    var timerClientTotalData = this.data();
                    self.progressDataSource.fetch(function () {

                        var progressData = this.data();

                        // Loop through 1 (or 2 if client changed) clients
                        $.each(theseClients, function (clientIndex, clientValue) {

                            var clientID = clientValue;

                            console.log('TimeCardView:calculateProgressDate:clientID:' + clientID);

                            // Existing timer records
                            var timerClientDateRecords = $.grep(timerClientTotalData, function (element, index) {
                                return (element.clientID === clientID);
                            });

                            // Existing timer

                            var sumSchedulerTime = 0;
                            var sumSchedulerCalls = 0;
                            var sumSchedulerMeetings = 0;

                            var sumSupport1Time = 0;
                            var sumSupport1Calls = 0;
                            var sumSupport1Meetings = 0;

                            var sumCompTime = 0;
                            var sumCompCalls = 0;
                            var sumCompMeetings = 0;

                            var sumReferralTime = 0;
                            var sumReferralCalls = 0;
                            var sumReferralMeetings = 0;

                            var sumAdminTime = 0;

                            if (timerClientDateRecords.length > 0) {
                                $.each(timerClientDateRecords, function (index, value) {
                                    var record = value;

                                    var totalTimeWorked = record.totalTimeWorked;

                                    var serviceRecord = self.getServiceRecord(record.serviceID);
                                    //serviceRecord.toStats === false &&
                                    if (serviceRecord.recordTasksMtgs === false && record.serviceID !== 1013 && record.serviceID !== 1001) {
                                        sumAdminTime += totalTimeWorked;
                                        //console.log('TimeCardView:populateProgressData:adminTime:' + sumAdminTime);
                                    } else {

                                        if (record.serviceID === 1011) {  //Support Scheduling
                                            sumSupport1Time += totalTimeWorked;
                                            sumSupport1Calls += record.totalTasks;
                                            sumSupport1Meetings += record.totalMeetings;
                                        } else if (record.serviceID === 1008) { // Comp Time
                                            sumCompTime += totalTimeWorked;
                                            sumCompCalls += record.totalTasks;
                                            sumCompMeetings += record.totalMeetings;
                                        } else if (record.serviceID === 1014) { // Referral Time
                                            sumReferralTime += totalTimeWorked;
                                            sumReferralCalls += record.totalTasks;
                                            sumReferralMeetings += record.totalMeetings;
                                        } else { // Scheduling (1001) and other
                                            sumSchedulerTime += totalTimeWorked;
                                            sumSchedulerCalls += record.totalTasks;
                                            sumSchedulerMeetings += record.totalMeetings;
                                        }
                                    }
                                });

                            }

                            sumSchedulerTime = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumSchedulerTime));
                            sumSupport1Time = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumSupport1Time));
                            sumCompTime = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumCompTime));
                            sumReferralTime = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumReferralTime));
                            sumAdminTime = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumAdminTime));

                            var progressRecords = $.grep(progressData, function (element, index) {
                                return element.clientID === clientID;
                            });

                            console.log('TimeCardView:populateProgressData:progressRecord:' + JSON.stringify(progressRecords));
                            console.log('TimeCardView:populateProgressData:current clientID:' + clientID);

                            if (progressRecords.length > 0) {

                                $.each(progressData, function (index, value) {
                                    var progressDataRecord = value;

                                    if (progressDataRecord.clientID === clientID) {
                                        var workDate = new Date(moment(progressDataRecord.workDate).clone().subtract((offset), 'minutes'));
                                        var timeStamp = new Date(moment(progressDataRecord.timeStamp).clone().subtract((offset), 'minutes'));

                                        progressDataRecord.set("workDate", workDate);
                                        progressDataRecord.set("timeStamp", timeStamp);

                                        progressDataRecord.set("schedulerTime", sumSchedulerTime);
                                        progressDataRecord.set("schedulerCalls", sumSchedulerCalls);
                                        progressDataRecord.set("schedulerMeetings", sumSchedulerMeetings);

                                        progressDataRecord.set("support1Time", sumSupport1Time);
                                        progressDataRecord.set("support1Calls", sumSupport1Calls);
                                        progressDataRecord.set("support1Meetings", sumSupport1Meetings);

                                        progressDataRecord.set("compTime", sumCompTime);
                                        progressDataRecord.set("compCalls", sumCompCalls);
                                        progressDataRecord.set("compMeetings", sumCompMeetings);

                                        progressDataRecord.set("referralTime", sumReferralTime);
                                        progressDataRecord.set("referralCalls", sumReferralCalls);
                                        progressDataRecord.set("referralMeetings", sumReferralMeetings);

                                        progressDataRecord.set("adminTime", sumAdminTime);
                                        //console.log('TimeCardView:populateProgressData:progressDataRecord:' + JSON.stringify(progressDataRecord));
                                    }
                                });

                            } else {
                                // Create new progressData record

                                var newRecord = {
                                    clientID: self.clientID,
                                    timeStamp: now,
                                    workDate: workDayStart,
                                    schedulerID: 0,
                                    schedulerTime: 0,
                                    schedulerCalls: 0,
                                    schedulerMeetings: 0,
                                    support1ID: 0,
                                    support1Time: 0,
                                    support1Calls: 0,
                                    support1Meetings: 0,
                                    support2ID: 0,
                                    support2Time: 0,
                                    support2Calls: 0,
                                    support2Meetings: 0,
                                    compID: 0,
                                    compTime: 0,
                                    compCalls: 0,
                                    compMeetings: 0,
                                    referralTime: 0,
                                    referralCalls: 0,
                                    referralMeetings: 0,
                                    adminTime: 0
                                };

                                newRecord.schedulerTime = sumSchedulerTime;
                                newRecord.schedulerCalls = sumSchedulerCalls;
                                newRecord.schedulerMeetings = sumSchedulerMeetings;

                                newRecord.support1Time = sumSupport1Time;
                                newRecord.support1Calls = sumSupport1Calls;
                                newRecord.support1Meetings = sumSupport1Meetings;

                                newRecord.compTime = sumCompTime;
                                newRecord.compCalls = sumCompCalls;
                                newRecord.compMeetings = sumCompMeetings;

                                newRecord.referralTime = sumReferralTime;
                                newRecord.referralCalls = sumReferralCalls;
                                newRecord.referralMeetings = sumReferralMeetings;

                                newRecord.adminTime = sumAdminTime;

                                self.progressDataSource.add(newRecord);

                            }

                        });

                        $.when(this.sync()).done(function (e) {

                            if (self.workDateOriginal !== self.$("#workDate").val() && self.workDateOriginal !== null) {

                                // If the time card workDate has changed, need to adjust the stats for the current date and the previous date
                                console.log('TimeCardView:populateProgressData:previous date: ' + self.workDateOriginal);
                                self.populateProgressData(self.workDateOriginal);
                                self.workDateOriginal = null;

                            } else {
                                self.resetStatsDashboardCache();

                                App.model.set('timerStatus', "Nothing");

                                var timeStamp = (new Date()).valueOf();
                                console.log('PortalTimeCardDetailGridView:updatedOpenTimer:timeStamp:' + timeStamp);
                                App.model.set('updatedOpenTimer', timeStamp);

                                if (self.isTimerRecordOpen === true) {
                                    App.router.navigate('openTimers', {trigger: true});
                                } else {
                                    App.router.navigate('statsTimecardNoDateChange', {trigger: true});
                                }
                            }
                        });
                    });
                });
            },
            resetStatsDashboardCache: function () {

                //console.log('PortalTimeCardDetailGridView:resetStatsDashboardCache');

                $.cookie('ESA:AppModel:mySumTotalTime', "");
                App.model.set('mySumTotalTime', null);

                $.cookie('ESA:AppModel:mySumTotalMeetings', "");
                App.model.set('mySumTotalMeetings', null);

                $.cookie('ESA:AppModel:mySumMeetingsTarget', "");
                App.model.set('mySumMeetingsTarget', null);

                $.cookie('ESA:AppModel:myMtgPercent', "");
                App.model.set('myMtgPercent', null);

                $.cookie('ESA:AppModel:teamSumTotalTime', "");
                App.model.set('teamSumTotalTime', null);

                $.cookie('ESA:AppModel:teamSumTotalMeetings', "");
                App.model.set('teamSumTotalMeetings', null);

                $.cookie('ESA:AppModel:teamSumMeetingsTarget', "");
                App.model.set('teamSumMeetingsTarget', null);

                $.cookie('ESA:AppModel:teamMtgPercent', "");
                App.model.set('teamMtgPercent', null);
            },
            displayNoDataOverlay: function () {
                //console.log('TimeCardView:displayNoDataOverlay');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('TimeCardView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('TimeCardView:resize');

            },
            remove: function () {
                //console.log("-------- TimeCardView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);


                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });