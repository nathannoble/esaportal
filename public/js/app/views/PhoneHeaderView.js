define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/phoneHeader'],
    function(App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend( {
            events:{
               
            },
            template: template,
            initialize: function(options) {
                //console.log('PhoneHeaderView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);
                
            },
            onShow: function(){
                //console.log('PhoneHeaderView:onShow');
                

            },
            
            onResize: function () {
                //console.log('PhoneHeaderView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PhoneHeaderView:resize');
                
            },
            remove: function () {
                //console.log('---------- PhonePhoneHeaderView:remove ----------');

                // Turn off events
                $(window).off("resize", this.onResize);
                $(document).off("webkitfullscreenchange mozfullscreenchange fullscreenchange", this.onFullScreenChange);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }

        });
    });