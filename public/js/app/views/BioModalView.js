define(['App', 'backbone', 'marionette', 'jquery',
        'hbs!templates/bioModal','kendo/kendo.data'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.ItemView.extend({
            template:template,
            events: {
                'click #CloseButton': 'onCloseClicked'
            },
            initialize: function(options){
                console.log('BioModalView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

            },
            onRender: function(){
                console.log('BioModalView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('---------- BioModalView:onShow ----------');

                // Setup the containers
                this.resize();

                this.$('#name').html(this.options.shortText);
                this.$('#title').html(this.options.title);
                //this.$('#bioImage').attr('src', "../../" + App.config.PortalName + "/EmployeeBios/" + this.fileName );
                this.$('#bioImage').attr('src', App.config.PortalFiles + "/EmployeeBios/" + this.options.fileName );
                this.$('#text').html(this.options.text);
                this.$('#text2').html(this.options.text2);

            },
            onCloseClicked: function(){
                console.log('BioModalView:onCloseClicked');

                var self = this;
                this.close();
            },
            onResize: function(){
                console.log('BioModalView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
//                console.log('BioModalView:resize');

            },
            remove: function(){
                console.log('---------- BioModalView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                $('click').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });