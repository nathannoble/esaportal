define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalUserGrid', 'hbs!templates/editPortalUserPopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template, EditPortalUserPopup) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalUserGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.userGridFilter = App.model.get('userGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalUserTable",
                                complete: function (e) {
                                    console.log('PortalUserGridView:dataSource:update:complete');
                                 },
                                error: function (e) {
                                    var message = 'PortalUserGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalUserGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalUserGridView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalUserGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalUserGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalUserTable";
                                },
                                complete: function (e) {
                                    console.log('PortalUserGridView:dataSource:create:complete');
                                 },
                                error: function (e) {
                                    var message = 'PortalUserGridView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalUserGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalUserGridView:dataSource:destroy:complete');
                                 },
                                error: function (e) {
                                    var message = 'PortalUserGridView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "userKey",
                                fields: {
                                    userKey: {editable: false, type: "number"},
                                    userID: {
                                        editable: true,
                                        type: "string",
                                        validation: {required: true},
                                        defaultValue: function () {
                                            return app.Utilities.numberUtilities.createGUID();
                                        }
                                    },
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    userLogin: {editable: true, type: "string", validation: {required: false}},
                                    userPassword: {editable: true, type: "string", validation: {required: false}},
                                    lastLogin: {editable: false, type: "date", validation: {required: false}},
                                    lastLogout: {editable: false, type: "date", validation: {required: false}},
                                    loginCount: {editable: false, type: "number"},
                                    userType: {
                                        type: "number",
                                        validation: {required: true},
                                        values: [
                                            {text: 'Pre-Hire', value: 0},
                                            {text: 'Employee', value: 1},
                                            {text: 'Manager', value: 2},
                                            {text: 'Executive', value: 3}
                                        ],
                                        defaultValue: 1
                                    },
                                    inactive: {
                                        editable: true,
                                        type: "boolean"
                                    },
                                    lastPasswordChange: {editable: false, type: "date"}
                                }
                            }
                        },
                        sort: [
                            {field: "lastName", dir: "asc"},
                            {field: "firstName", dir: "asc"}
                        ],
                        requestStart: function (e) {
                            //console.log('PortalUserGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#userLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalUserGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#userLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalUserGridView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalUserGridView:dataSource:error');
                            kendo.ui.progress(self.$("#userLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalUserGridView:displayNoDataOverlay');

                this.$el.css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No field selected</div></div></div>').appendTo(this.$el.parent());

            },
            onRender: function () {
                //console.log('PortalUserGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalUserGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#user").kendoGrid({

                    editable: {
                        mode: "popup",
                        window: {
                            title: "Edit User"
                        },
                        template: function (e) {
                            var template = EditPortalUserPopup();
                            return template;
                        }
                    },
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    filterable: {
                        mode: "row"
                    },
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Edit' style='text-align: center'><a class='btn-interaction k-grid-edit' data-id='" + e.userKey + "'><i class='font-white-brdr fa fa-pencil-square-o' style='color: black'></i></a></div>";
                                //fa-edit
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        //{
                        //    width: 25,
                        //    template: function (e) {
                        //        var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.companyID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                        //        return template;
                        //    },
                        //    filterable: false,
                        //    sortable: true
                        //},
                        {
                            field: "userKey",
                            title: "User Key",
                            width: 75,
                            filterable: false
                        },
                        //{
                        //    field: "userID",
                        //    title: "User ID",
                        //    width: 150
                        //},
                        {
                            field: "lastName",
                            title: "Last Name",
                            width: 175,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },
                        {
                            field: "firstName",
                            title: "First Name",
                            width: 175,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },
                        {
                            field: "userLogin",
                            title: "User Login",
                            width: 100,
                            filterable: false
                        },
                        {
                            field: "userPassword",
                            title: "User Password",
                            width: 100,
                            filterable: false,
                            template: function (e) {
                                if ('●'.repeat) {
                                    return e.userPassword === null ? ' ' : '●'.repeat(e.userPassword.length);
                                } else {
                                    return '●●●●●●';
                                }
                            }
                        },
                        {
                            field: "loginCount",
                            title: "Login Count",
                            width: 75,
                            filterable: false
                        }, {
                            field: "lastLogin",
                            title: "Last Login",
                            width: 130,
                            format: "{0:M/d/yyyy h:mm:ss tt}",
                            filterable: false
                        },
                        {
                            field: "lastPasswordChange",
                            title: "Password Changed",
                            width: 130,
                            format: "{0:M/d/yyyy h:mm:ss tt}",
                            filterable: false
                        },
                        //{
                        //    field: "lastLogout",
                        //    title: "Last Logout",
                        //    width: 130,
                        //    format: "{0:M/d/yyyy h:mm:ss tt}"
                        //},
                        {
                            field: "userType",
                            title: "User Type",
                            width: 75,
                            filterable: false
                        },
                        {
                            field: "inactive",
                            title: "Inactive",
                            template: function (e) {
                                if (e.inactive === false) {
                                    return "False";
                                } else {
                                    return "True";
                                }
                            },
                            width: 175,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.inactiveFilter
                                }
                            }
                        }
                    ],
                    change: function (e) {
                        console.log('PortalUserGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        console.log('PortalUserGridView:def:onShow:onSave');

                        if ($("input[name = 'userPassword']").val() === "") {
                            e.model.userPassword = "";
                            e.model.dirty = true;
                        }

                        //if (e.container.find("input[name='inactive']").val() === "true") {
                        //
                        //} else {
                        //
                        //}
                    },
                    edit: function (e) {

                        //console.log('PortalUserGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        e.container.find("input[name='userKey']").prop("disabled", true);
                        e.container.find("input[name='lastLogin']").prop("disabled", true);
                        e.container.find("input[name='loginCount']").prop("disabled", true);
                        e.container.find("input[name='dataUrl']").attr("value",App.config.DataServiceURL);
                        //e.container.find("input[name='userPassword']").prop("disabled", true);

                        // Make edit/delete invisible
                        //e.container.find(".k-edit-label:first").hide();
                        //e.container.find(".k-edit-field:first").hide();

                        //e.container.find("input[name='email']").val("disabled");


                    },
                    dataBound: function (e) {
                        //console.log("PortalUserGridView:def:onShow:dataBound");

                        //self.$('.user-profile').on('click', self.viewEmployeeProfile);

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;

                            var userGridFilter = [];

                            $.each(filters, function (index, value) {
                                userGridFilter.push(value);
                            });

                            //console.log("PortalUserGridView:def:onShow:dataBound:set userGridFilter:" + JSON.stringify(userGridFilter));
                            app.model.set('userGridFilter', userGridFilter);
                        }

                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PortalUserGridView:getData');

                var grid = this.$("#user").data("kendoGrid");

                //console.log('PortalUserGridView:getData:filter:' + JSON.stringify(state.filter));

                var self = this;

                this.dataSource.fetch(function (data) {

                    grid.setDataSource(self.dataSource);
                    //grid.dataSource.filter({field: "inactive", operator: "eq", value: false});
                    if (self.userGridFilter !== null && self.userGridFilter !== []) {
                        var filters = [];
                        $.each(self.userGridFilter, function (index, value) {
                            filters.push(value);
                        });

                        //console.log("PortalUserGridView:grid:set datasource:filter" + JSON.stringify(filters));
                        grid.dataSource.filter(filters);
                    } else {
                        grid.dataSource.filter({field: "inactive", operator: "eq", value: 0});
                    }

                });

            },
            inactiveFilter: function (container) {
                var self = this;

                //console.log('PortalUserGridView:inactiveFilter:' + JSON.stringify(container));

                var dataSource = [
                    {value: true, text: "True"},
                    {value: false, text: "False"}
                ];

                container.element.kendoDropDownList({
                    //autoBind:false,
                    dataValueField: "value",
                    dataTextField: "text",
                    valuePrimitive: true,
                    dataSource: dataSource,
                    optionLabel: "Select Value"
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalUserGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#user").parent().parent().height();
                this.$("#user").height(parentHeight);

                parentHeight = parentHeight - 52; //65

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalUserGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalUserGridView:resize:headerHeight:' + headerHeight);
                this.$("#user").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalUserGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });