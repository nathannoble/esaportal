define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/avModal','kendo/kendo.data'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.ItemView.extend({
            template:template,
            events: {
                'click #closeButton': 'onCloseClicked'
            },
            initialize: function(options){
                console.log('AVModalView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', self.onResize);

                this.options = options;
                this.playing = false;

                this.userKey = parseInt(App.model.get('userKey'), 0);
                this.portalAdminDataID = options.ID;

                console.log('AVModalView:initialize:this.portalAdminDataID:' + this.portalAdminDataID);

                this.esaMediaUserDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ESAMediaUser",
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('AVModalView:update');
                                    return App.config.DataServiceURL + "/odata/ESAMediaUser" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('AVModalView:esaMediaUserDataSource:update:complete');
                                 },
                                error: function (e) {
                                    var message = 'AVModalView:esaMediaUserDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    console.log('AVModalView:create:data:' + JSON.stringify(data));
                                    return App.config.DataServiceURL + "/odata/ESAMediaUser";
                                },
                                complete: function (e) {
                                    console.log('AVModalView:esaMediaUserDataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'AVModalView:esaMediaUserDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"

                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('AVModalView:destroy');
                                    return App.config.DataServiceURL + "/odata/ESAMediaUser" + "(" + data.ID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    userKey: {editable: true, type: "number"},
                                    portalAdminDataID: {editable: true, type: "number"},
                                    startTime: {
                                        editable: true,
                                        type: "date",
                                        format: "{0:s}"
                                    },
                                    stopTime: {editable: true, type: "date", validation: {required: false}},
                                    totalTime: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "ID", dir: "desc"},
                        requestStart: function (e) {
                            //console.log('AVModalView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingWidget"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('AVModalView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#loadingWidget"), false);
                        },
                        error: function (e) {
                            console.log('AVModalView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingWidget"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('AVModalView:change');

                        }
                    });

            },
            onRender: function(){
                console.log('AVModalView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                console.log('---------- AVModalView:onShow ----------');

                var self = this;

                // Setup the containers
                this.resize();

                this.$('#timeStamp').html(this.options.shortText);
                this.$('#title').html(this.options.title);
                this.$('#text').html(this.options.text);
                var avFile = encodeURIComponent(this.options.avFile);
                var isAudio = false;

                // Audio
                if (avFile.indexOf("mp3") > -1) {
                    this.$('#videoFile').hide();
                    this.$('#audioFile').show();
                    this.$('#audioFile').attr('type', "audio/mpeg");
                    this.$('#audioFile').attr('src', App.config.PortalFiles + "/AVFiles/" + avFile);
                    isAudio = true;

                } else if (avFile.indexOf("wav") > -1) {
                    this.$('#videoFile').hide();
                    this.$('#audioFile').show();
                    this.$('#audioFile').attr('type', "audio/wav");
                    this.$('#audioFile').attr('src', App.config.PortalFiles + "/AVFiles/" + avFile);
                    isAudio = true;

                // Video
                } else if (avFile.indexOf("mp4") > -1) {
                    this.$('#audioFile').hide();
                    this.$('#videoFile').show();
                    this.$('#videoFileSource').attr('type', "video/mp4");
                    this.$('#videoFileSource').attr('src', App.config.PortalFiles + "/AVFiles/" + avFile);
                } else if (avFile.indexOf("ogg") > -1) {
                    this.$('#audioFile').hide();
                    this.$('#videoFile').show();
                    this.$('#videoFileSource').attr('type', "video/ogg");
                    this.$('#videoFileSource').attr('src', App.config.PortalFiles + "/AVFiles/" + avFile);
                } else if (avFile.indexOf("webm") > -1) {
                    this.$('#audioFile').hide();
                    this.$('#videoFile').show();
                    this.$('#videoFileSource').attr('type', "video/webm");
                    this.$('#videoFileSource').attr('src', App.config.PortalFiles + "/AVFiles/" + avFile);
                }

                if (isAudio) {

                    this.aud = document.getElementById("audioFile");
                    this.aud.onplay = function() {
                        self.onAVPlay();
                    };
                    this.aud.onpause = function() {
                        self.onAVPause();
                    };
                    this.aud.onended = function() {
                        self.onAVStop();
                    };
                    this.aud.onabort = function() {
                        self.onAVStop();
                    };

                } else {
                    this.vid = document.getElementById("videoFile");
                    this.vid.onplay = function() {
                        self.onAVPlay();
                    };
                    this.vid.onpause = function() {
                        self.onAVPause();
                    };
                    this.vid.onended = function() {
                        self.onAVStop();
                    };
                    this.vid.onabort = function() {
                        self.onAVStop();
                    };
                }

            },
            onAVPlay: function() {
                console.log('AVModalView:onAVPlay');
                if (navigator.onLine) {
                    this.doesConnectionExist("start");
                } else {
                   alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                }
            },
            onAVStop: function() {
                console.log('AVModalView:onAVStop');
                if (navigator.onLine) {
                    this.doesConnectionExist("stop");
                } else {
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                }
            },
            onAVPause: function() {
                console.log('AVModalView:onAVPause');
                if (navigator.onLine) {
                    this.doesConnectionExist("pause");
                } else {
                    alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                }
            },
            doesConnectionExist: function (type) {
                console.log('AVModalView:doesConnectionExist');
                var self = this;
                $.ajax({
                    type: "GET",
                    contentType: "application/json", //; charset=utf-8",
                    url: App.config.DataServiceURL + "/odata/PortalValue",
                    //dataType: "json",
                    success: function (result) {
                        //console.log('AVModalView:doesConnectionExist:result:' + JSON.stringify(result));
                        console.log('AVModalView:doesConnectionExist:result:yes');

                        self.updateESAMediaUserRecord(type);
                    },
                    error: function (result) {
                        alert("Internet connection is currently unavailable (navigator offline).  Please check your connection try again in a few minutes.");
                    }
                });
            },
            updateESAMediaUserRecord: function (type) {

                var self = this;
                var app = App;

                if (type === "start") {
                    if (self.playing === false) {
                        console.log('AVModalView:updateESAMediaUserRecord:updateESAMediaUserRecord:start');
                        self.addESAMediaUserRecord(type);
                    }
                } else {

                    self.esaMediaUserDataSource.filter([
                        {field: "userKey", operator: "eq", value: self.userKey},
                        {field: "portalAdminDataID", operator: "eq", value: self.portalAdminDataID}]
                    );

                    self.esaMediaUserDataSource.fetch(function () {
                        var data = this.data();

                        if (data.length > 0) {

                            console.log('AVModalView:updateESAMediaUserRecord:type:' + type);

                            var record = self.esaMediaUserDataSource.at(0);
                            var offset = (new Date()).getTimezoneOffset();
                            var now = new Date(moment().clone().subtract((offset), 'minutes'));

                            var startTime = new Date(moment(record.startTime).clone().subtract((offset), 'minutes'));
                            var stopTime = new Date(moment(record.stopTime).clone().subtract((offset), 'minutes'));

                            if (moment(now).isBefore(startTime)) {
                                var message = 'AVModalView:updateESAMediaUserRecord:now is before start time:now:startTime:' + now + ":" + startTime;
                                console.log(message);
                            }

                            var stop = moment(now).clone();
                            var start = moment(startTime).clone();

                            var seconds = parseFloat(stop.diff(start, 'seconds'));
                            var minutes = parseFloat(stop.diff(start, 'minutes'));
                            var hours = parseFloat(stop.diff(start, 'hours'));

                            //var totalTimeText = "";
                            var pastTotalTime = 0; //record.totalTime;
                            var currentTime = 0;

                            // Round up minutes
                            if ((minutes + 1) * 60 - seconds >= 30) {
                                minutes += 1;
                            }

                            // If less than 1 minute, make it 1 minute or 1/100th hour
                            if (minutes < 1) {
                                currentTime = 0.01;
                                //totalTimeText = "0:1";
                            } else {
                                currentTime = parseFloat(app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(seconds / 3600));
                                //totalTimeText = parseInt(hours, 0).toString() + ":" + (parseInt(minutes, 0) - parseInt(hours, 0) * 60).toString();
                            }

                            if (type === "pause") {
                                console.log('AVModalView:updateESAMediaUserRecord:updateESAMediaUserRecord:pause');
                            } else if (type === "stop") {
                                console.log('AVModalView:updateESAMediaUserRecord:updateESAMediaUserRecord:stop');
                            } else {
                                console.log('AVModalView:updateESAMediaUserRecord:updateESAMediaUserRecord:type:' + type);
                            }
                            if (self.playing === true) {
                                record.set("startTime", startTime);
                                record.set("stopTime", now);
                                record.set("totalTime", pastTotalTime + currentTime);
                                self.playing = false;
                                self.esaMediaUserDataSource.sync();
                            }
                        } else {
                            self.addESAMediaUserRecord(type);
                        }

                    });
                }
            },
            addESAMediaUserRecord: function (type) {
                console.log('AVModalView:addESAMediaUserRecord:add:type:' + type);

                var self = this;

                //var today = moment(new Date()).clone().format('YYYY-MM-DD'); //"2016-11-01";
                var offset = (new Date()).getTimezoneOffset();
                var now = new Date(moment().clone().subtract((offset), 'minutes'));

                //self.startTime = moment().clone();

                // Add new record
                //self.esaMediaUserDataSource.fetch(function () {

                if (self.playing === false) {
                    self.playing = true;
                    self.esaMediaUserDataSource.add({
                        userKey: self.userKey,
                        portalAdminDataID: self.portalAdminDataID,
                        startTime: now,
                        stopTime: new Date(0),
                        totalTime: 0
                    });

                    self.esaMediaUserDataSource.sync();
                }

                //});

            },
            onCloseClicked: function(){
                console.log('AVModalView:onCloseClicked');

                //var self = this;

                if (this.playing) {
                    this.onAVStop();
                }
                this.remove();
                this.close();

            },
            onResize: function(){
                console.log('AVModalView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
                // console.log('AVModalView:resize');
            },
            remove: function(){
                console.log('---------- AVModalView:remove ----------');

                // Turn off events
                $(window).off('resize', self.onResize);

                this.$('#audioFile').attr('src', '');
                this.$('#videoFile').attr('src', '');

                $('click').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });