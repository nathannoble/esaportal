define(['App', 'backbone', 'marionette', 'jquery', 'models/PhoneModel', 'hbs!templates/phoneCollection',
        'views/PhoneView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, PhoneModel, template,
              PhoneView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: PhoneView,
            initialize: function (options) {
                //console.log('PhoneCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.phones = [];

                var self = this;

                this.clientID = parseInt(App.model.get('selectedClientId'), 0); //2189;
                this.employeeID = parseInt(App.model.get('selectedEmployeeId'), 0); //100;
                this.companyID = parseInt(App.model.get('selectedCompanyId'), 0); //4;

                this.getData();

            },
            onShow: function () {
                //console.log("-------- PhoneCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                this.dataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        serverFiltering: true,
                        serverSorting: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('PhoneCollectionView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PhoneCollectionView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PhoneCollectionView:update');
                                    return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                                },
                                complete: function (e) {
                                    console.log('PhoneCollectionView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PhoneCollectionView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PhoneCollectionView:create');
                                    return App.config.DataServiceURL + "/odata/ProgressEmployee";
                                },
                                complete: function (e) {
                                    console.log('PhoneCollectionView:dataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'PhoneCollectionView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PhoneCollectionView:destroy');
                                    return App.config.DataServiceURL + "/odata/ProgressEmployee" + "(" + data.employeeID + ")";
                                },
                                complete: function (e) {
                                    console.log('PhoneCollectionView:dataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'PhoneCollectionView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    emailAddress: {editable: true, type: "string", validation: {required: true}},
                                    esaLocalPhone: {editable: true, type: "string", validation: {required: false}},
                                    esaExtension: {type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: {field: "firstName", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('PhoneCollectionView:dataSource:request start:');
                            kendo.ui.progress(self.$("#phoneCollectionLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PhoneCollectionView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#phoneCollectionLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PhoneCollectionView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PhoneCollectionView:dataSource:error');
                            kendo.ui.progress(self.$("#phoneCollectionLoading"), false);
                        },
                        filter: {field: "status", operator: "eq", value: '1 - Active'}
                    });

                this.dataSource.fetch(function () {

                    var data = this.data();
                    var phoneCount = data.length;

                    $.each(data, function (index, value) {

                        self.phones.push({
                            employeeID: value.employeeID,
                            esaExtension: value.esaExtension,
                            lastName: value.lastName,
                            firstName: value.firstName
                        });
                    });

                    // Populate phone collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return model.get("phoneID");
                    };

                    $.each(self.phones, function (index, value) {

                        //console.log('PhoneCollectionView:note:value:' + JSON.stringify(value));
                        var model = new PhoneModel();

                        model.set("employeeID", value.employeeID);
                        model.set("esaExtension", value.esaExtension);
                        model.set("lastName", value.lastName);
                        model.set("firstName", value.firstName);

                        self.collection.push(model);
                    });

                });

            },
            hideNoDataOverlay: function () {
                //console.log('PhoneCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#phoneWidgetContent').css('display', 'block');
            },
            displayNoDataOverlay: function () {
                //console.log('PhoneCollectionView:displayNoDataOverlay');

                // Hide the widget content
                //this.$('#phoneWidgetContent').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {
                //console.log('PhoneCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PhoneCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- PhoneCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
