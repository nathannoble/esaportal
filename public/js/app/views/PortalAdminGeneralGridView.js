define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalAdminGeneralGrid', 'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalAdminGeneralGridView:initialize');

                _.bindAll(this);
                
                // Subscribe to browser events
                $(window).on("resize", this.onResize);
            },
            onRender: function () {
                //console.log('PortalAdminGeneralGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminGeneralGridView:onShow --------");
                var self = this;

               
                this.grid = this.$("#adminGeneral").kendoGrid({

                    //toolbar: ["create"],
                    //editable: "popup",  //inline, popup
                    sortable: true,
                    extra: false,
                    //selectable: "multiple",
                    resizable: true,
                    //reorderable: true,
                    //filterable: true,
                    //scrollable: false,
                    //selectable: "row",
                    //pageable: true,
                    //groupable: true,
                    columns: [
                       {
                            field: "link",
                            title: "Link", width: 200,
                            template: function (e) {
                                var template = "<div style='text-align: left'>" + e.link + "</b></div>";
                                return template;
                            }
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("PortalAdminGeneralGridView:holiday:onShow:dataBound");
                    },
                    change: function (e) {
                        //console.log('PortalAdminGeneralGridView:holiday:onShow:onChange');
                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PortalAdminGeneralGridView:getData');

                var adminLinks = [
                    {link: '<a href="#detailsAdmin"><i style="width: 15px;text-align:center" class="fa fa-gear"></i>&nbsp;<span>Dashboard Details Admin</span></a>'},
                    {link: '<a href="#adminAlertsData"><i style="width: 15px;text-align:center" class="fa fa-exclamation"></i>&nbsp;<span>Company Alerts Admin</span></a>'},
                    {link: '<a href="#adminMessagesData"><i style="width: 15px;text-align:center" class="fa fa-flag"></i>&nbsp;<span>Company Messages Admin</span></a>'},
                    {link: '<a href="#adminChatData"><i style="width: 15px;text-align:center" class="fa fa-comments-o"></i>&nbsp;<span>Company Chat Admin</span></a>'},
                    {link: '<a href="#adminESAMediaData"><i style="width: 15px;text-align:center" class="fa fa-video-camera"></i>&nbsp;<span>ESA Media Admin</span></a>'},
                    //{link: '<a href="#adminSocialMediaData"><i class="fa fa-thumbs-o-up"></i>&nbsp;<span>Social Media Admin</span></a>'},
                    {link: '<a href="#adminIndustryNewsData"><i style="width: 15px;text-align:center"class="fa fa-newspaper-o"></i>&nbsp;<span>Industry News Admin</span></a>'},
                    {link: '<a href="#adminStaticData"><i style="width: 15px;text-align:center" class="fa fa-sticky-note-o"></i>&nbsp;<span>HR Forms & Docs Admin</span></a>'},
                    {link: '<a href="#adminBioData"><i style="width: 15px;text-align:center" class="fa fa-users"></i>&nbsp;<span>ESA Team Admin</span></a>'},
                    {link: '<a href="#empPodcastData"><i style="width: 15px;text-align:center" class="fa fa-microphone"></i>&nbsp;<span>Employee Podcast Report</span></a>'}
                ];

                var configuration = {
                    sortable: true,
                    extra: false,
                    resizable: true,
                    columns: [{
                        field: "link",
                        title: "Link",
                        template: function (e) {
                            var template = "<div style='text-align: left'>" + e.link + "</div>";
                            return template;
                        },
                        width: 200
                    }],
                    dataSource: {
                        //sort: {field: "link", dir: "asc"},
                        data: adminLinks,
                        schema: {
                            model:{
                                fields:[{"link": { type: "string" }}]
                            }
                        }
                    }
                };

                this.$("#adminGeneral").kendoGrid(configuration).data("kendoGrid");

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalAdminGeneralGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#adminGeneral").parent().parent().height();
                this.$("#adminGeneral").height(parentHeight);

                parentHeight = parentHeight - 15;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalAdminGeneralGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalAdminGeneralGridView:resize:headerHeight:' + headerHeight);
                this.$("#adminGeneral").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalAdminGeneralGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#adminGeneral").data('ui-tooltip'))
                    this.$("#adminGeneral").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });