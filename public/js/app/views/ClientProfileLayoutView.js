define(['App', 'jquery', 'hbs!templates/clientProfileLayout', 'backbone', 'marionette', 'underscore',
        'views/ContactInfoView','views/FileWidgetView','views/SummaryStatsView','views/NoteWidgetView', 'views/HistoryWidgetView'],
    function (App, $, template, Backbone, Marionette, _, ContactInfoView, FileWidgetView, SummaryStatsView, NoteWidgetView,HistoryWidgetView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template:template,
            regions:{
                tile1: "#tile-client-contact-info",
                tile2: "#tile-client-files",
                tile3: "#tile-client-summary",
                tile4: "#tile-client-notes",
                tile5: "#tile-scheduler-history"
            },
            initialize: function(){
                //console.log('ClientProfileLayoutView:initalize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);

            },
            onRender: function(){
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                //console.log('ClientProfileLayoutView:onShow');

                this.userId = App.model.get('userId');
                this.userType = parseInt(App.model.get('userType'), 0);

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                   this.resetLayout();
                }

            },
            resetLayout: function () {
                //console.log('ClientProfileLayoutView:resetLayout');

                this.tile1.reset();
                this.tile2.reset();
                this.tile3.reset();
                this.tile4.reset();
                this.tile5.reset();

                this.tile1.show(new ContactInfoView({type: 'Client'}));
                this.tile2.show(new FileWidgetView({type: 'Client'}));
                this.tile3.show(new SummaryStatsView({type: 'Client'}));
                this.tile4.show(new NoteWidgetView({type: 'Client'}));
                if (this.userType === 3)  {
                    this.$('#scheduler-history').show();
                    this.tile5.show(new HistoryWidgetView({type: 'Client'}));
                } else {
                    this.$('#scheduler-history').hide();
                }

            },
            onSelectedDateFilterChanged: function () {
                this.resetLayout();
            },
            transitionIn: function(){
                //console.log('ClientProfileLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function(){
                //console.log('ClientProfileLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
                //console.log('ClientProfileLayoutView:resize');
            },
            remove: function(){
                //console.log('---------- ClientProfileLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });