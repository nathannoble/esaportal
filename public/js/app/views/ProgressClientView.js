define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressClient', 'jquery.cookie',
    'kendo/kendo.data', 'kendo/kendo.combobox', 'kendo/kendo.datepicker'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #SaveButton': 'saveButtonClicked'
            },
            initialize: function (options) {
                //console.log('ProgressClientView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = { type: "Edit" };
                }

                this.userId = App.model.get('userId');
               
                this.addToAddedFlag = false;
                this.addToLostFlag = false;

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', { trigger: true });
                } else {

                    if (this.options.type === "New Client") {
                        this.clientID = null;
                    } else {
                        this.clientID = parseInt(App.model.get('selectedClientId'), 0); //2189;
                    }
                    // Subscribe to browser events
                    $(window).on("resize", this.onResize);

                    this.progressClientDS = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/ProgressClient";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressClientView:progressClientDS:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientID",
                                    fields: {
                                        clientID: { editable: false, defaultValue: 0, type: "number" },
                                        schedulerID: { editable: false, defaultValue: 0, type: "number" },
                                        schedulerName: { editable: true, type: "string", validation: { required: false } },
                                        companyID: { editable: false, defaultValue: 0, type: "number" },
                                        clientCompany: { editable: true, type: "string", validation: { required: false } },
                                        status: { editable: true, type: "string", validation: { required: false } },
                                        planHours: { type: "number" },
                                        schedulingHours: { type: "number" },
                                        taskTargetLow: { type: "number" },
                                        taskTargetHigh: { type: "number" },
                                        mtgTargetLowDec: { type: "number" },
                                        mtgTargetHigh: { type: "number" },
                                        planStartDate: { editable: true, type: "date", validation: { required: false } },
                                        planEndDate: { editable: true, type: "date", validation: { required: false } }
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                //console.log('   ProgressClientView:requestStart');

                                kendo.ui.progress(self.$("#loadingClientInfo"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressClientView:requestEnd');
                                kendo.ui.progress(self.$("#loadingClientInfo"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressClientView:error');
                                kendo.ui.progress(self.$("#loadingClientInfo"), false);

                                self.displayNoDataOverlay();

                                //App.modal.show(new ErrorView());

                            },
                            change: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressClientView:change');

                                var data = this.data();

                                if (data.length <= 0)
                                    self.displayNoDataOverlay();
                                else
                                    self.hideNoDataOverlay();
                            },
                            sort: [{ field: "planStartDate", dir: "desc" }],
                            filter: [{ field: "clientID", operator: "eq", value: self.clientID }]

                        });
                    this.clientDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressClientTable",
                                    complete: function (e) {
                                        console.log('ProgressClientView:clientDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:clientDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ProgressClientView:update');
                                        return App.config.DataServiceURL + "/odata/ProgressClientTable" + "(" + data.clientID + ")";
                                        //return "http://localhost:64859/odata/ProgressClientTable" + "(" + data.clientID + ")";

                                    },
                                    complete: function (e) {
                                        console.log('ProgressClientView:dataSource:update:complete:' + JSON.stringify(e.responseJSON));
                                        if (e.responseJSON.value[0]) {
                                            var clientRecord = e.responseJSON.value[0];
                                            console.log('ProgressClientView:dataSource:update:complete:record:' + JSON.stringify(clientRecord));
                                            self.updateClientPlanTable(clientRecord);
                                        }

                                    },
                                    error: function (e) {
                                        console.log('ProgressClientView:dataSource:update:error:' + JSON.stringify(e.responseJSON));
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('ProgressClientView:create');
                                        return App.config.DataServiceURL + "/odata/ProgressClientTable";
                                        //return "http://localhost:64859/odata/ProgressClientTable";
                                    },
                                    complete: function (e) {

                                        if (e.responseJSON.value[0]) {
                                            var newID = e.responseJSON.value[0].clientID;
                                            console.log('ProgressClientView:create:complete:' + newID);
                                            app.model.set('selectedClientId', newID);
                                            var clientRecord = e.responseJSON.value[0];
                                            //console.log('ProgressClientView:clientDataSource:create:complete:record:' + JSON.stringify(clientRecord));
                                            self.createClientPlanTableRecord(clientRecord);
                                        }

                                    },
                                    error: function (e) {
                                        console.log('ProgressClientView:dataSource:create:error:' + JSON.stringify(e.responseJSON));
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('ProgressClientView:destroy');
                                        return App.config.DataServiceURL + "/odata/ProgressClientTable" + "(" + data.clientID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressClientView:clientDataSource:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:clientDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientID",
                                    fields: {
                                        clientID: { editable: false, defaultValue: 0, type: "number" },
                                        schedulerID: { defaultValue: 0, type: "number" },
                                        status: { editable: true, type: "string", validation: { required: false } },
                                        companyID: { defaultValue: 0, type: "number" },
                                        taskTargetLow: { type: "number" },
                                        taskTargetHigh: { type: "number" },
                                        mtgTargetLowDec: { type: "number" },
                                        mtgTargetHigh: { type: "number" },
                                        firstName: { editable: true, type: "string", validation: { required: false } },
                                        lastName: { editable: true, type: "string", validation: { required: false } },
                                        customerName: { editable: true, type: "string", validation: { required: false } },
                                        companyClient: { editable: true, type: "boolean" },
                                        clientSuccessPlan: { editable: true, type: "boolean" },
                                        useAsReference: { editable: true, type: "boolean" },
                                        emailAddress: { editable: true, type: "string", validation: { required: false } },
                                        address: { editable: true, type: "string", validation: { required: false } },
                                        city: { editable: true, type: "string", validation: { required: false } },
                                        state: { editable: true, type: "string", validation: { required: false } },
                                        zipCode: { editable: true, type: "string", validation: { required: false } },
                                        mobilePhone: { editable: true, type: "string", validation: { required: false } },
                                        homePhone: { editable: true, type: "string", validation: { required: false } },
                                        sbCount: { editable: true, type: "boolean" },
                                        clientTimeZoneID: { type: "number" },
                                        industry: { editable: true, type: "string", validation: { required: false } },
                                        divisionID: { type: "number" },
                                        division: { editable: true, type: "string", validation: { required: false } },
                                        title: { editable: true, type: "string", validation: { required: false } },
                                        lfdAuthorization: { editable: true, type: "string", validation: { required: false } },
                                        dsuNumber: { editable: true, type: "string", validation: { required: false } },
                                        product: { editable: true, type: "string", validation: { required: false } },
                                        distributionChannel: {
                                            editable: true,
                                            type: "string",
                                            validation: { required: false }
                                        },
                                        states: { editable: true, type: "string", validation: { required: false } },
                                        lengthInTerritory: { editable: true, type: "string", validation: { required: false } },
                                        workPhone: { editable: true, type: "string", validation: { required: false } },
                                        workExtension: { editable: true, type: "string", validation: { required: false } },
                                        assistantInternal: { editable: true, type: "string", validation: { required: false } },
                                        assistantPhone: { editable: true, type: "string", validation: { required: false } },
                                        assistantExtension: { editable: true, type: "string", validation: { required: false } },
                                        assistantEmail: { editable: true, type: "string", validation: { required: false } },
                                        typeOfService: { editable: true, type: "string", validation: { required: false } },
                                        referredBy: { editable: true, type: "string", validation: { required: false } },
                                        planID: { type: "number" },
                                        adminSchedulerID: { type: "number" },
                                        realTimeAccess: { editable: true, type: "boolean" },
                                        realTimeUser: { editable: true, type: "string", validation: { required: false } },
                                        realTimePassword: { editable: true, type: "string", validation: { required: false } },
                                        esaAnniversary: { editable: true, type: "date", validation: { required: true } },
                                        termDate: { editable: true, type: "date", validation: { required: true } },
                                        schedulingStartDate: { editable: true, type: "date", validation: { required: false } },
                                        lastCheckInDate: { editable: true, type: "date", validation: { required: false } },
                                        surveySentDate: { editable: true, type: "date", validation: { required: false } },
                                        additionalInformation: {
                                            editable: true,
                                            type: "string",
                                            validation: { required: false }
                                        },
                                        sysCrmUserName: { editable: true, type: "string", validation: { required: false } },
                                        sysCrmPassword: { editable: true, type: "string", validation: { required: false } },
                                        sysCrmNotes: { editable: true, type: "string", validation: { required: false } },
                                        sysEmailUserName: { editable: true, type: "string", validation: { required: false } },
                                        sysEmailPassword: { editable: true, type: "string", validation: { required: false } },
                                        sysEmailNotes: { editable: true, type: "string", validation: { required: false } },
                                        sysCalendar: { editable: true, type: "string", validation: { required: false } },
                                        sysCalendarUserName: {
                                            editable: true,
                                            type: "string",
                                            validation: { required: false }
                                        },
                                        sysCalendarPassword: {
                                            editable: true,
                                            type: "string",
                                            validation: { required: false }
                                        },
                                        sysCalendarNotes: { editable: true, type: "string", validation: { required: false } },
                                        dbAccountType: { editable: true, type: "string", validation: { required: false } },
                                        checkInStatus: { editable: true, type: "string", validation: { required: false } },
                                        returningClient: { editable: true, type: "boolean" },
                                        schedulerChangeDate: { editable: true, type: "date", validation: { required: false } },
                                        dataCallDate: { editable: true, type: "date", validation: { required: false } },
                                        dbBuildCompleted: { editable: true, type: "date", validation: { required: false } },
                                        mapCreated: { editable: true, type: "date", validation: { required: false } },
                                        mapApproved: { editable: true, type: "date", validation: { required: false } },
                                        mapLastUpdated: { editable: true, type: "date", validation: { required: false } },
                                        linkedIn: { editable: true, type: "string", validation: { required: false } },
                                        cspTarget: { editable: true, type: "string", validation: { required: false } },
                                        clientAppPassword: { editable: true, type: "string", validation: { required: false } },
                                        companyEmailMFA: { editable: true, type: "string", validation: { required: false } },
                                        tier: { editable: true, type: "string", validation: { required: false } },
                                        wholesalerRank: { editable: true, type: "string", validation: { required: false } },
                                        platform: { editable: true, type: "string", validation: { required: false } },
                                        distSegment: { editable: true, type: "string", validation: { required: false } },
                                        secondaryRank: { editable: true, type: "string", validation: { required: false } },
                                        specialDBTags: { editable: true, type: "string", validation: { required: false } },
                                        focusFirms: { editable: true, type: "string", validation: { required: false } }, 
                                        maptiveID: { editable: true, type: "string", validation: { required: false } },
                                        clientTerrMap: { editable: true, type: "string", validation: { required: false } }, 
                                        schedDirectives: { editable: true, type: "string", validation: { required: false } }, 
                                        zoneMapNotes: { editable: true, type: "string", validation: { required: false } }, 
                                        adminDept: { editable: true, type: "string", validation: { required: false } }, 
                                        specialInstForDB: { editable: true, type: "string", validation: { required: false } },
                                        mtgTypes: { editable: true, type: "string", validation: { required: false } },
                                        dialerAcctTypes: { editable: true, type: "string", validation: { required: false } },
                                        existingZoning: { editable: true, type: "string", validation: { required: false } },
                                        travelRotation: { editable: true, type: "string", validation: { required: false } }
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                //console.log('   ProgressClientView:requestStart');

                                kendo.ui.progress(self.$("#loadingClientInfo"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressClientView:requestEnd');
                                kendo.ui.progress(self.$("#loadingClientInfo"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressClientView:error');
                                kendo.ui.progress(self.$("#loadingClientInfo"), false);

                                self.displayNoDataOverlay();

                                //App.modal.show(new ErrorView());

                            },
                            change: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                            },
                            filter: [{ field: "clientID", operator: "eq", value: self.clientID }]

                        });

                    this.clientPlanDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ClientPlan",
                                    complete: function (e) {
                                        console.log('ProgressClientView:clientPlanDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:clientPlanDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ProgressClientView:update');
                                        return App.config.DataServiceURL + "/odata/ClientPlan" + "(" + data.clientPlanID + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressClientView:clientPlanDataSource:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:clientPlanDataSource:updateerror:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('ProgressClientView:create');
                                        return App.config.DataServiceURL + "/odata/ClientPlan";
                                    },

                                    complete: function (e) {

                                        if (e.responseJSON.value[0]) {
                                            var newID = e.responseJSON.value[0].clientPlanID;
                                            //console.log('ProgressClientView:create:complete:' + newID);
                                            //App.model.set('selectedClientId', newID);
                                        }
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:clientPlanDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "clientPlanID",
                                    fields: {
                                        clientPlanID: { editable: false, defaultValue: 0, type: "number" },
                                        clientID: { editable: false, defaultValue: 0, type: "number" },
                                        planID: { type: "number" },
                                        planType: { editable: true, type: "string", validation: { required: false } },
                                        planName: { editable: true, type: "string", validation: { required: false } },
                                        description: { editable: true, type: "string", validation: { required: true } },
                                        planPrice: { editable: true, type: "number" },
                                        rateSheetHours_4wk: { editable: true, type: "number" },
                                        schedulingHours_4wk: { editable: true, type: "number" },
                                        schedulingHours_1wk: { editable: true, type: "number" },
                                        schedulingHours_day: { editable: true, type: "number" },
                                        startDate: { editable: true, type: "date", validation: { required: true } },
                                        endDate: { editable: true, type: "date", validation: { required: true } }
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                //console.log('   ProgressClientView:requestStart');

                                kendo.ui.progress(self.$("#loadingClientInfo"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressClientView:requestEnd');
                                kendo.ui.progress(self.$("#loadingClientInfo"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressClientView:error');
                                kendo.ui.progress(self.$("#loadingClientInfo"), false);

                                self.displayNoDataOverlay();

                                //App.modal.show(new ErrorView());

                            }
                            //filter: [{field: "clientID", operator: "eq", value: self.clientID}]

                        });

                    this.companyDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                    complete: function (e) {
                                        console.log('ProgressClientView:companyDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "companyID",
                                    fields: {
                                        companyID: { editable: false, type: "number" },
                                        clientCompany: { editable: true, type: "string", validation: { required: false } }
                                    }
                                }
                            },
                            //pageSize: 50,
                            sort: { field: "clientCompany", dir: "asc" },
                            serverFiltering: true,
                            serverSorting: true

                        });

                    this.planDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressPlan",
                                    complete: function (e) {
                                        console.log('ProgressClientView:planDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:planDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                // "planID":23,
                                // "planType":"S",
                                // "planName":"2012 Preferred Premium 5",
                                // "description":"Scheduling Service - Preferred",
                                // "planPrice":"700.0000",
                                // "rateSheetHours_4wk":20.0,
                                // "schedulingHours_4wk":20.0,
                                // "schedulingHours_1wk":4.5,
                                // "schedulingHours_day":0.9

                                model: {
                                    id: "planID",
                                    fields: {
                                        planID: { editable: false, type: "number" },
                                        planType: { editable: true, type: "string", validation: { required: false } },
                                        planName: { editable: true, type: "string", validation: { required: false } },
                                        description: { editable: true, type: "string", validation: { required: true } },
                                        planPrice: { editable: true, type: "number" },
                                        rateSheetHours_4wk: { editable: true, type: "number" },
                                        schedulingHours_4wk: { editable: true, type: "number" },
                                        schedulingHours_1wk: { editable: true, type: "number" },
                                        schedulingHours_day: { editable: true, type: "number" },
                                        inactive: {
                                            editable: true,
                                            type: "boolean"
                                        }
                                    }
                                }
                            },
                            //pageSize: 50,
                            sort: { field: "planName", dir: "asc" },
                            serverFiltering: true,
                            serverSorting: true,
                            filter: { field: "inactive", operator: "neq", value: true }
                        });

                    this.schedulerDataSource = new kendo.data.DataSource(
                        {
                            autoBind: false,
                            type: "odata",
                            transport: {
                                read: {
                                    url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                    complete: function (e) {
                                        console.log('ProgressClientView:schedulerDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:schedulerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "employeeID",
                                    fields: {
                                        employeeID: { editable: false, type: "number" },
                                        firstName: { editable: true, type: "string", validation: { required: false } },
                                        lastName: { editable: true, type: "string", validation: { required: false } },
                                        status: { type: "string", validation: { required: false } },
                                        isTeamLeader: { editable: true, type: "boolean" }
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            sort: [
                                { field: "lastName", dir: "asc" },
                                { field: "firstName", dir: "asc" }
                            ],
                            filter: { field: "status", operator: "eq", value: '1 - Active' }
                        });

                    this.scoreBoardDataSource = new kendo.data.DataSource(
                        {
                            type: "odata",
                            transport: {
                                read: {
                                    url: function () {
                                        return App.config.DataServiceURL + "/odata/PortalScoreBoard";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressClientView:scoreBoardDataSource:read:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:scoreBoardDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    },
                                    dataType: "json"
                                },
                                update: {
                                    url: function (data) {
                                        //console.log('ProgressClientView:update');
                                        return App.config.DataServiceURL + "/odata/PortalScoreBoard" + "(" + data.year + data.month + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressClientView:scoreBoardDataSource:update:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:scoreBoardDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                },
                                create: {
                                    url: function (data) {
                                        //console.log('ProgressClientView:create');
                                        return App.config.DataServiceURL + "/odata/PortalScoreBoard";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressClientView:scoreBoardDataSource:create:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:scoreBoardDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                    //dataType: "json"
                                },
                                destroy: {
                                    url: function (data) {
                                        //console.log('ProgressClientView:destroy');
                                        return App.config.DataServiceURL + "/odata/PortalScoreBoard" + "(" + data.year + data.month + ")";
                                    },
                                    complete: function (e) {
                                        console.log('ProgressClientView:scoreBoardDataSource:destroy:complete');
                                    },
                                    error: function (e) {
                                        var message = 'ProgressClientView:scoreBoardDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                        console.log(message);
                                    }
                                }
                            },
                            schema: {
                                data: "value",
                                total: function (data) {
                                    return data['odata.count'];
                                },
                                model: {
                                    id: "year",
                                    fields: {
                                        year: { editable: false, defaultValue: 0, type: "number" },
                                        month: { type: "number" },
                                        added: { type: "number" },
                                        lost: { type: "number" },
                                        growth: { type: "number" }
                                    }
                                }
                            },
                            serverFiltering: true,
                            serverSorting: true,
                            requestStart: function () {
                                //console.log('   ProgressClientView:requestStart');

                                kendo.ui.progress(self.$("#loadingClientInfo"), true);
                            },
                            requestEnd: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('    ProgressClientView:requestEnd');
                                kendo.ui.progress(self.$("#loadingClientInfo"), false);

                            },
                            error: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;

                                //console.log('   ProgressClientView:error');
                                kendo.ui.progress(self.$("#loadingClientInfo"), false);


                            },
                            change: function () {

                                // Do nothing if the request has been cancelled
                                if (this.cancelled === true)
                                    return;
                            },
                            sort: { field: "month", dir: "asc" }
                        });

                    this.getData();
                }
            },
            onRender: function () {
                //console.log('ProgressClientView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientView:onShow --------");
                var self = this;

                if (this.options.type === "New Client") {
                    self.$('#SaveButton').html('Add New Client');
                } else {
                    self.$('#SaveButton').html("Update Client");
                }

                self.$('#SaveButton').addClass('disabled');
            },
            getData: function () {

                var self = this;

                this.clientDataSource.fetch(function () {

                    var data = this.data();
                    var count = data.length;
                    var clientRecord;

                    //console.log('ProgressClientView:dataSource:fetch:count:' + count);
                    if (count === 0 || self.options.type === "New Client") {

                        // Create new record
                        clientRecord = {
                            // clientID: data.clientID,
                            firstName: "",
                            lastName: "",
                            // customerName:data.customerName,
                            companyClient: false,
                            clientSuccessPlan: false,
                            useAsReference: false,
                            // emailAddress: data.emailAddress,
                            // address: data.address,
                            // city: data.city,
                            // state: data.state,
                            // zipCode: data.zipCode,
                            // mobilePhone: data.mobilePhone,
                            // homePhone: data.homePhone,
                            sbCount: true,
                            // clientTimeZoneID: data.clientTimeZoneID,
                            // companyID: data.companyID,
                            // industry: data.industry,
                            // division: data.division,
                            title: "Title",
                            // lfdAuthorization: data.lfdAuthorization,
                            // dsuNumber: data.dsuNumber,
                            product: "",
                            distributionChannel: "",
                            // states: data.states,
                            // lengthInTerritory: data.lengthInTerritory,
                            // workPhone: data.workPhone,
                            // workExtension: data.workExtension,
                            // assistantInternal: data.assistantInternal,
                            // assistantPhone: data.assistantPhone,
                            // assistantExtension: data.assistantExtension,
                            // assistantEmail: data.assistantEmail,
                            // typeOfService: data.typeOfService,
                            // referredBy: data.referredBy,
                            // planID: data.planID,
                            status: '3 - Onboarding',
                            schedulerID: 0,
                            // adminSchedulerID: data.adminSchedulerID,
                            // taskTargetLow:data.taskTargetLow,
                            // mtgTargetLowDec:data.mtgTargetLowDec,
                            //realTimeAccess: false,
                            // realTimeUser:data.realTimeUser,
                            // realTimePassword:data.realTimePassword,
                            esaAnniversary: null,//new Date().toISOString(),
                            termDate: null,
                            schedulingStartDate: null,
                            lastCheckInDate: null,
                            surveySentDate: null,
                            dataCallDate: null,
                            dbBuildCompleted: null,
                            mapCreated: null,
                            mapApproved: null,
                            mapLastUpdated: null,
                            // additionalInfo:data.additionalInfo,
                            // sysCrmUserName:data.sysCrmUserName,
                            // sysCrmPassword:data.sysCrmPassword,
                            // sysCrmNotes:data.sysCrmNotes,
                            // sysEmailUserName:data.sysEmailUserName,
                            // sysEmailPassword:data.sysEmailPassword,
                            // sysEmailNotes:data.sysEmailNotes,
                            // sysCalendar:data.sysCalendar,
                            // sysCalendarUserName:data.sysCalendarUserName,
                            // sysCalendarPassword:data.sysCalendarPassword,
                            // sysCalendarNotes:data.sysCalendarNotes,
                            mtgTypes: "",
                            dialerAcctTypes: "",
                            existingZoning: "--- NONE ---",
                            travelRotation: "--- NONE ---"
                        };

                        self.newRecord = clientRecord;

                        //console.log('ProgressClientView:dataSource:new client record:' + JSON.stringify(clientRecord));


                    } else {

                        clientRecord = data[0];
                        console.log('ProgressClientView:dataSource:record:' + JSON.stringify(clientRecord));
                    }

                    self.schedulerChangeDate = clientRecord.schedulerChangeDate;

                    self.productOtherText = clientRecord.productOtherText;

                    // Dates
                    $("#esaAnniversary").kendoDatePicker({
                        value: clientRecord.esaAnniversary
                    });
                    $("#termDate").kendoDatePicker({
                        value: clientRecord.termDate
                    });

                    $("#schedulingStartDate").kendoDatePicker({
                        value: clientRecord.schedulingStartDate
                    });
                    $("#lastCheckInDate").kendoDatePicker({
                        value: clientRecord.lastCheckInDate
                    });
                    $("#surveySentDate").kendoDatePicker({
                        value: clientRecord.surveySentDate
                    });

                    $("#dataCallDate").kendoDatePicker({
                        value: clientRecord.dataCallDate
                    });
                    $("#dbBuildCompleted").kendoDatePicker({
                        value: clientRecord.dbBuildCompleted
                    });
                    $("#mapCreated").kendoDatePicker({
                        value: clientRecord.mapCreated
                    });
                    $("#mapApproved").kendoDatePicker({
                        value: clientRecord.mapApproved
                    });
                    $("#mapLastUpdated").kendoDatePicker({
                        value: clientRecord.mapLastUpdated
                    });
                    
                    // Contact Info
                    // self.$('input[data-role=dropdownlist]').kendoDropDownList();
                    self.$(".k-textbox").kendoMaskedTextBox();
                    self.$(".k-numerictextbox").kendoNumericTextBox();

                    self.$("#firstName").data("kendoMaskedTextBox").value(clientRecord.firstName);
                    self.$("#lastName").data("kendoMaskedTextBox").value(clientRecord.lastName);
                    self.$("#customerName").data("kendoMaskedTextBox").value(clientRecord.customerName);
                    self.$("#clientID").data("kendoMaskedTextBox").value(clientRecord.clientID);
                    self.$("#emailAddress").data("kendoMaskedTextBox").value(clientRecord.emailAddress);
                    self.$("#address").data("kendoMaskedTextBox").value(clientRecord.address);
                    self.$("#city").data("kendoMaskedTextBox").value(clientRecord.city);
                    self.$("#state").data("kendoMaskedTextBox").value(clientRecord.state);
                    self.$("#zipCode").data("kendoMaskedTextBox").value(clientRecord.zipCode);
                    self.$("#mobilePhone").data("kendoMaskedTextBox").value(clientRecord.mobilePhone);
                    self.$("#homePhone").data("kendoMaskedTextBox").value(clientRecord.homePhone);

                    // Business Info
                    self.$("#division").data("kendoMaskedTextBox").value(clientRecord.division);
                    self.$("#title").data("kendoMaskedTextBox").value(clientRecord.title);
                    self.$("#dsuNumber").data("kendoMaskedTextBox").value(clientRecord.dsuNumber);
                    self.$("#states").data("kendoMaskedTextBox").value(clientRecord.states);
                    self.$("#workPhone").data("kendoMaskedTextBox").value(clientRecord.workPhone);
                    self.$("#workExtension").data("kendoMaskedTextBox").value(clientRecord.workExtension);
                    self.$("#assistantInternal").data("kendoMaskedTextBox").value(clientRecord.assistantInternal);
                    self.$("#assistantPhone").data("kendoMaskedTextBox").value(clientRecord.assistantPhone);
                    self.$("#assistantExtension").data("kendoMaskedTextBox").value(clientRecord.assistantExtension);
                    self.$("#assistantEmail").data("kendoMaskedTextBox").value(clientRecord.assistantEmail);

                    // Service Info
                    self.$("#referredBy").data("kendoMaskedTextBox").value(clientRecord.referredBy);
                    self.$("#personalEmail").data("kendoMaskedTextBox").value(clientRecord.realTimeUser);
                    self.$("#personalPhone").data("kendoMaskedTextBox").value(clientRecord.realTimePassword);
                    self.$("#additionalInformation").data("kendoMaskedTextBox").value(clientRecord.additionalInformation);
                    self.$("#sysCrmUserName").data("kendoMaskedTextBox").value(clientRecord.sysCrmUserName);
                    self.$("#sysCrmPassword").data("kendoMaskedTextBox").value(clientRecord.sysCrmPassword);
                    self.$("#sysCrmNotes").data("kendoMaskedTextBox").value(clientRecord.sysCrmNotes);
                    self.$("#sysEmailUserName").data("kendoMaskedTextBox").value(clientRecord.sysEmailUserName);
                    self.$("#sysEmailPassword").data("kendoMaskedTextBox").value(clientRecord.sysEmailPassword);
                    self.$("#sysEmailNotes").data("kendoMaskedTextBox").value(clientRecord.sysEmailNotes);
                    self.$("#sysCalendar").data("kendoMaskedTextBox").value(clientRecord.sysCalendar);
                    self.$("#sysCalendarUserName").data("kendoMaskedTextBox").value(clientRecord.sysCalendarUserName);
                    self.$("#sysCalendarPassword").data("kendoMaskedTextBox").value(clientRecord.sysCalendarPassword);
                    self.$("#sysCalendarNotes").data("kendoMaskedTextBox").value(clientRecord.sysCalendarNotes);

                     // Client Data
                    self.$("#linkedIn").data("kendoMaskedTextBox").value(clientRecord.linkedIn);
                    self.$("#cspTarget").data("kendoMaskedTextBox").value(clientRecord.cspTarget);
                    self.$("#clientAppPassword").data("kendoMaskedTextBox").value(clientRecord.clientAppPassword);
                    self.$("#companyEmailMFA").data("kendoMaskedTextBox").value(clientRecord.companyEmailMFA);
                    self.$("#tier").data("kendoMaskedTextBox").value(clientRecord.tier);
                    self.$("#wholesalerRank").data("kendoMaskedTextBox").value(clientRecord.wholesalerRank);
                    self.$("#platform").data("kendoMaskedTextBox").value(clientRecord.platform);
                    self.$("#distSegment").data("kendoMaskedTextBox").value(clientRecord.distSegment);
                    self.$("#secondaryRank").data("kendoMaskedTextBox").value(clientRecord.secondaryRank);
                    self.$("#specialDBTags").data("kendoMaskedTextBox").value(clientRecord.specialDBTags);
                    self.$("#focusFirms").data("kendoMaskedTextBox").value(clientRecord.focusFirms);
                    self.$("#maptiveID").data("kendoMaskedTextBox").value(clientRecord.maptiveID);
                    self.$("#clientTerrMap").data("kendoMaskedTextBox").value(clientRecord.clientTerrMap);
                    //self.$("#accountManager").data("kendoMaskedTextBox").value(clientRecord.accountManager);   

                    self.$("#schedDirectives").data("kendoMaskedTextBox").value(clientRecord.schedDirectives);
                    self.$("#zoneMapNotes").data("kendoMaskedTextBox").value(clientRecord.zoneMapNotes);
                    self.$("#adminDept").data("kendoMaskedTextBox").value(clientRecord.adminDept);
                    self.$("#specialInstForDB").data("kendoMaskedTextBox").value(clientRecord.specialInstForDB);

                    var yesNoList = [
                        { text: 'Yes', value: "true" },
                        { text: 'No', value: "false" }
                    ];

                    self.$("#companyClient").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: clientRecord.companyClient.toString()
                    });

                    if (clientRecord.clientSuccessPlan === null) {
                        clientRecord.clientSuccessPlan = false;
                    }

                    self.$("#clientSuccessPlan").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: clientRecord.clientSuccessPlan.toString()
                    });

                    if (clientRecord.useAsReference === null) {
                        clientRecord.useAsReference = false;
                    }
                    self.$("#useAsReference").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: clientRecord.useAsReference.toString()
                    });

                    if (clientRecord.sbCount === null) {
                        clientRecord.sbCount = false;
                    }

                    self.$("#sbCountList").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: clientRecord.sbCount.toString()
                    });

                    var timeZones = [
                        { text: 'US/Pacific', value: 1 },
                        { text: 'US/Mountain', value: 2 },
                        { text: 'US/Central', value: 3 },
                        { text: 'US/Eastern', value: 4 },
                        { text: 'US/Atlantic', value: 4 }
                    ];

                    self.$("#clientTimeZoneID").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: timeZones,
                        value: clientRecord.clientTimeZoneID
                    });

                    var industries = [
                        { value: '--- NONE ---' },
                        { value: '1 - Financial Advisor' },
                        { value: '2 - Financial Services' },
                        { value: '3 - Financial Wholesaler' },
                        { value: '4 - Insurance' },
                        { value: '5 - Mass Media' },
                        { value: '6 - Mortgage' },
                        { value: '7 - Pharmaceutical' },
                        { value: '8 - Real Estate' },
                        { value: '9 - Other' }
                    ];

                    self.$("#industry").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: industries,
                        value: clientRecord.industry
                    });

                    var yesNoNA = [
                        { value: 'N/A' },
                        { value: 'Yes' },
                        { value: 'No' }
                    ];
                    self.$("#lfdAuthorization").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: yesNoNA,
                        value: clientRecord.lfdAuthorization
                    });

                    var products = [
                        { value: 'Other' },
                        { value: 'All Channels' },
                        { value: 'Alternative' },
                        { value: 'Annuities (Both)' },
                        { value: 'Annuities (Fixed)' },
                        { value: 'Annuities (Variable)' },
                        { value: 'Business Development' },
                        { value: 'Consulting' },
                        { value: 'Digital Wealth' },
                        { value: 'ETFs' },
                        { value: 'Hedge Fund' },
                        { value: 'Life Insurance' },
                        { value: 'Long Term Care' },
                        { value: 'Mutual Funds' },
                        { value: 'Recruiting' },
                        { value: 'REIT' },
                        { value: 'Retirement / 401K' },
                        { value: 'Reverse Mortgage' },
                        { value: 'RIA' },
                        { value: "SMA's" }
                    ];

                    var selectedProducts = clientRecord.product.split(",");
                    self.$("#product").kendoMultiSelect({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: products,
                        value: selectedProducts,
                        change: self.onProductChanged
                        //autoWidth: true
                    });

                    if (selectedProducts.indexOf("Other") > -1) {
                        self.$("#productOtherText").show();
                        self.$("#productOtherText").data("kendoMaskedTextBox").value(clientRecord.productOtherText);
                        self.$("#productOtherTextLabel").show();
                    } else {
                        self.$("#productOtherText").hide();
                        self.$("#productOtherTextLabel").hide();
                    }

                    var distributionChannels = [
                        //{value: '--- NONE ---'},
                        { value: 'Banks' },
                        { value: 'B2B' },
                        { value: 'B2C' },
                        { value: 'Independents' },
                        { value: 'Other' },
                        { value: 'Regional' },
                        { value: 'Wirehouses' }
                    ];

                    var selectedChannels = clientRecord.distributionChannel.split(",");
                    self.$("#distributionChannel").kendoMultiSelect({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: distributionChannels,
                        value: selectedChannels
                    });

                    var lengthsInTerritory = [
                        { value: '--- NONE ---' },
                        { value: '0-1 Year' },
                        { value: '1 Year' },
                        { value: '2 Years' },
                        { value: '3-5 Years' },
                        { value: '5 Years Plus' }
                    ];

                    self.$("#lengthInTerritory").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: lengthsInTerritory,
                        value: clientRecord.lengthInTerritory
                    });

                    var typesOfService = [
                        { value: '1 - Scheduling' },
                        { value: '2 - Admin' },
                        { value: '3 - Sched + Admin' },
                        { value: '4 - Activity Net' },
                        { value: '5 - Activity Net + Sched' }
                    ];

                    self.$("#typeOfService").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: typesOfService,
                        value: clientRecord.typeOfService
                    });

                    var statuses = [
                        //{value: '0 - Active'},
                        { value: '1 - Prospect (Cold)' },
                        { value: '2 - Pending' },
                        { value: '3 - Onboarding' },
                        { value: '4 - Active' },
                        { value: '5 - On Hold' },
                        { value: '6 - Termed' }
                    ];

                    self.$("#status").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: statuses,
                        value: clientRecord.status,
                        change: self.onStatusChanged
                    });

                    self.originalStatus = clientRecord.status;

                    var tasksLow = [
                        { text: '10-15', value: 10 },
                        { text: '15-20', value: 15 },
                        { text: '20-25', value: 20 },
                        { text: '25-30', value: 25 },
                        { text: '30-35', value: 30 }
                    ];

                    self.$("#taskTargetLow").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: tasksLow,
                        value: clientRecord.taskTargetLow
                    });

                    var mtgTargetsLow = [
                        { text: '0', value: 0 },
                        { text: '0.5', value: 0.5 },
                        { text: '1-3', value: 1 },
                        { text: '2-4', value: 2 },
                        { text: '3-5', value: 3 },
                        { text: '4-6', value: 4 },
                        { text: '5-7', value: 5 }
                    ];

                    self.$("#mtgTargetLowDec").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: mtgTargetsLow,
                        value: clientRecord.mtgTargetLowDec
                    });

                    var checkInStatuses = [
                        { text: '--- NONE ---', value: null },
                        { text: 'Satisfied', value: 'Satisfied' },
                        { text: 'Unsatisfied', value: 'Unsatisfied' }
                    ];

                    self.$("#checkInStatus").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: checkInStatuses,
                        value: clientRecord.checkInStatus
                    });

                    if (clientRecord.returningClient === null || clientRecord.returningClient === undefined) {
                        clientRecord.returningClient = false;
                    }
                    self.$("#returningClient").kendoDropDownList({
                        dataValueField: "value",
                        dataTextField: "text",
                        dataSource: yesNoList,
                        value: clientRecord.returningClient.toString()
                    });

                    // New fields as of 10/24
                    var mtgTypess = [
                        { value: 'In-Person' },
                        { value: 'Zoom/Teams Meetings' },
                        { value: 'Conference Calls' },
                        { value: 'Office Meetings' },
                        { value: 'Office Lunches' },
                        { value: 'Office Breakfast' },
                        { value: 'Walk-Thru' },
                        { value: 'Lunches' },
                        { value: 'Happy Hour' }
                    ];

                    if (clientRecord.mtgTypes === null || clientRecord.mtgTypes === undefined) {
                        clientRecord.mtgTypes = "";
                    }

                    var selectedMtgTypes = clientRecord.mtgTypes.split(",");
                    self.$("#mtgTypes").kendoMultiSelect({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: mtgTypess,
                        value: selectedMtgTypes
                    });

                    var dialerAcctTypess = [
                        { value: 'Emailing - ESA' },
                        { value: 'Emailing - Client Domain' },
                        { value: 'Non-Emailing' },
                        { value: 'Client CRM/Note Entry' },
                        { value: 'Dialer Scheduling' },
                        { value: 'Networked' },
                        { value: 'Leadstream' },
                        { value: 'Campaign' },
                        { value: 'Dialer Linked to Client CRM' },
                        { value: 'Client CRM Scheduling Only' }
                    ];

                    if (clientRecord.dialerAcctTypes === null || clientRecord.dialerAcctTypes === undefined) {
                        clientRecord.dialerAcctTypes = "";
                    }

                    var selectedDialerAcctTypes = clientRecord.dialerAcctTypes.split(",");
                    self.$("#dialerAcctTypes").kendoMultiSelect({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: dialerAcctTypess,
                        value: selectedDialerAcctTypes
                    });

                    if (clientRecord.existingZonings === null || clientRecord.mtgTypes === existingZonings) {
                        clientRecord.existingZonings = '--- NONE ---';
                    }

                    var existingZonings = [
                       { value: '--- NONE ---' },
                       { value: 'Yes - Does Not Utilize' },
                       { value: 'Yes - Keep Existing Zones' },
                       { value: 'No' }
                    ];

                    self.$("#existingZoning").kendoDropDownList({
                        dataTextField: "value",
                        dataValueField: "value",
                        dataSource: existingZonings,
                        value: clientRecord.existingZoning
                    });

                    if (clientRecord.travelRotations === null || clientRecord.mtgTypes === travelRotations) {
                        clientRecord.travelRotations = '--- NONE ---';
                    }

                    var travelRotations = [
                        { value: '--- NONE ---' },
                        { value: 'Yes' },
                        { value: 'No - Client Will Provide' },
                        { value: 'No - ESA To Provide' }
                     ];
 
                     self.$("#travelRotation").kendoDropDownList({
                         dataTextField: "value",
                         dataValueField: "value",
                         dataSource: travelRotations,
                         value: clientRecord.travelRotation
                     });

                    self.companyDataSource.fetch(function () {

                        var companyData = this.data();

                        self.$("#companyID").kendoDropDownList({
                            dataTextField: "clientCompany",
                            dataValueField: "companyID",
                            dataSource: self.companyDataSource,
                            value: clientRecord.companyID
                        });

                        var companyRecords = $.grep(companyData, function (element, index) {
                            return element.companyID == clientRecord.companyID;
                        });

                        self.$("#dbAccountType").data("kendoMaskedTextBox").value(companyRecords.length > 0 ? companyRecords[0].dbAccountType : "None");


                        self.planDataSource.fetch(function () {
                            self.planList = self.$("#planID").kendoDropDownList({
                                dataTextField: "planName",
                                dataValueField: "planID",
                                dataSource: self.planDataSource,
                                value: clientRecord.planID,
                                change: self.onPlanChanged
                            });
                            self.$("#planID").css("width", "200px");
                            console.log('ProgressClientView:planDataSource:fetch:count:' + count);

                            self.progressClientDS.fetch(function () {
                                var data = this.data();
                                var count = data.length;

                                if (count > 0) {

                                    self.planHours = data[0].planHours;
                                    console.log('ProgressClientView:current Plan Hours:' + self.planHours);
                                }
                            });

                            self.schedulerDataSource.fetch(function () {

                                var records = this.data();
                                var modifiedRecords = [{
                                    employeeID: 0,
                                    name: '--- NONE ---'
                                }];

                                $.each(records, function (index, value) {
                                    
                                    var record = value;

                                    if (record.employeeID == clientRecord.schedulerID) {
                                        self.teamLeaderID = record.teamLeaderID;
                                        console.log('ProgressClientView:teamLeaderID:' + self.teamLeaderID);
                                    }

                                    var newRecord = {
                                        // From ProgressClient
                                        employeeID: record.employeeID,
                                        name: record.firstName + " " + record.lastName
                                    };
                                    modifiedRecords.push(newRecord);

                                });

                                $.each(records, function (index, value) {
                                    
                                    var record = value;

                                    if (record.employeeID == self.teamLeaderID) {
                                        self.teamLeaderFullName = record.firstName + " " + record.lastName;
                                        console.log('ProgressClientView:teamLeaderFullName:' + self.teamLeaderFullName);
                                        self.$("#accountManager").data("kendoMaskedTextBox").value(self.teamLeaderFullName);
                                    }

                                });

                                self.$("#schedulerID").kendoDropDownList({
                                    dataTextField: "name",
                                    dataValueField: "employeeID",
                                    dataSource: modifiedRecords,
                                    value: clientRecord.schedulerID,
                                    change: self.onSchedulerChanged

                                });

                                self.$("#adminSchedulerID").kendoDropDownList({
                                    dataTextField: "name",
                                    dataValueField: "employeeID",
                                    dataSource: modifiedRecords,
                                    value: clientRecord.adminSchedulerID
                                });

                                self.$('#SaveButton').removeClass('disabled');

                                
                            });
                        });
                    });

                });

            },
            saveButtonClicked: function (e) {
                console.log('ProgressClientView:saveButtonClicked');

                var self = this;
                var app = App;

                var record;

                var status = self.$("#status").val();

                self.esaAnniversary = self.$("#esaAnniversary").val();
                if (self.esaAnniversary !== null && self.esaAnniversary !== "") {
                    self.esaAnniversary = new Date(self.esaAnniversary).toISOString();
                } else {
                    self.esaAnniversary = null;
                }

                //console.log('ProgressClientView:saveButtonClicked:esaAnniversary:' + esaAnniversary);

                self.termDate = self.$("#termDate").val();
                if (self.termDate !== null && self.termDate !== "") {
                    self.termDate = new Date(self.termDate).toISOString();
                } else {
                    self.termDate = null;
                }

                self.schedulingStartDate = self.$("#schedulingStartDate").val();
                if (self.schedulingStartDate !== null && self.schedulingStartDate !== "") {
                    self.schedulingStartDate = new Date(self.schedulingStartDate).toISOString();
                } else {
                    self.schedulingStartDate = null;
                }

                self.lastCheckInDate = self.$("#lastCheckInDate").val();
                if (self.lastCheckInDate !== null && self.lastCheckInDate !== "") {
                    self.lastCheckInDate = new Date(self.lastCheckInDate).toISOString();
                } else {
                    self.lastCheckInDate = null;
                }

                self.surveySentDate = self.$("#surveySentDate").val();
                if (self.surveySentDate !== null && self.surveySentDate !== "") {
                    self.surveySentDate = new Date(self.surveySentDate).toISOString();
                } else {
                    self.surveySentDate = null;
                }

                self.dataCallDate = self.$("#dataCallDate").val();
                if (self.dataCallDate !== null && self.dataCallDate !== "") {
                    self.dataCallDate = new Date(self.dataCallDate).toISOString();
                } else {
                    self.dataCallDate = null;
                }

                self.dbBuildCompleted = self.$("#dbBuildCompleted").val();
                if (self.dbBuildCompleted !== null && self.dbBuildCompleted !== "") {
                    self.dbBuildCompleted = new Date(self.dbBuildCompleted).toISOString();
                } else {
                    self.dbBuildCompleted = null;
                }

                self.mapCreated = self.$("#mapCreated").val();
                if (self.mapCreated !== null && self.mapCreated !== "") {
                    self.mapCreated = new Date(self.mapCreated).toISOString();
                } else {
                    self.mapCreated = null;
                }

                self.mapApproved = self.$("#mapApproved").val();
                if (self.mapApproved !== null && self.mapApproved !== "") {
                    self.mapApproved = new Date(self.mapApproved).toISOString();
                } else {
                    self.mapApproved = null;
                }

                self.mapLastUpdated = self.$("#mapLastUpdated").val();
                if (self.mapLastUpdated !== null && self.mapLastUpdated !== "") {
                    self.mapLastUpdated = new Date(self.mapLastUpdated).toISOString();
                } else {
                    self.mapLastUpdated = null;
                }

                var plan = self.$("#planID").val();
                var error = null;

                // Validate entries
                if (this.options.type === "New Client") {



                    if (plan === null || plan === "0" || plan === undefined) {
                        error = "You must choose a plan (on the Service Info tab) in order to create a new client.";
                        self.createAutoClosingAlert(this.$("#clientWarning"), "<strong>Warning.</strong> " + error);
                        return;
                    }
                }

                //var taskTargetLow = parseInt(self.$("#taskTargetLow").val(),0);
                var planHours = parseInt(self.planDataSource.get(plan) ? self.planDataSource.get(plan).schedulingHours_4wk : 0, 0);
                var mtgTargetLowDec = parseFloat(self.$("#mtgTargetLowDec").val());

                if (mtgTargetLowDec === 0.5) {
                    if (planHours % 2 !== 0) {
                        error = "Client plan hours = " + planHours + " . If the meeting target is 0.5 you must have a plan hours value that is divisible by 2.";
                        self.createAutoClosingAlert(this.$("#clientWarning"), "<strong>Warning.</strong> " + error);
                        return;
                    }
                }

                //console.log('ProgressClientView:saveButtonClicked:termDate:' + termDate);
                var message;
                var date;
                self.month = 1;
                //self.year = 2018;
                if (status !== self.originalStatus) {
                    if (status == "4 - Active" && self.originalStatus === "3 - Onboarding") {
                        // New employee
                        if (self.esaAnniversary === null) {
                            // If employee is being activated and anniversary date not set, ensure they set it.
                            message = "You must choose an anniversary date in order to activate (4 - Active) a client. ";
                            self.createAutoClosingAlert(this.$("#clientWarning"), "<strong>Warning.</strong> " + message);
                            return;
                        } else {
                            date = moment(new Date(self.esaAnniversary)).clone().format('DD/MM/YYYY');
                            self.month = moment(new Date(self.esaAnniversary)).clone().format('M');
                            message = "Based on the client's ESA Anniversary date of " + moment(new Date(self.esaAnniversary)).clone().format('M/D/YYYY') + ", this will add 1 to the Scoreboard for ADDED for the month: " + self.month;
                            if (confirm(message) === false) {
                                return;
                            } else {
                                self.addToAddedFlag = true;
                            }
                        }
                    } else if ((status == "6 - Termed" && self.originalStatus === "4 - Active")) {
                        // termed employee
                        if (self.termDate === null) {
                            // If employee is being activated and anniversary date not set, ensure they set it.
                            message = "You must choose an term date in order to terminate (6 - Termed) a client. ";
                            self.createAutoClosingAlert(this.$("#clientWarning"), "<strong>Warning.</strong> " + message);
                            return;
                        } else {
                            date = moment(new Date(self.termDate)).clone().format('M/D/YYYY');
                            self.month = moment(new Date(self.termDate)).clone().format('M');
                            message = "Based on the client's Term date of " + date + ", this will add 1 to the Scoreboard for LOST for the month: " + self.month;
                            if (confirm(message) === false) {
                                return;
                            } else {
                                self.addToLostFlag = true;
                            }
                        }
                    }
                }

                var products = [];
                $("#product option:selected").each(function () {
                    products.push($(this).val());
                });

                var channels = [];
                $("#distributionChannel option:selected").each(function () {
                    channels.push($(this).val());
                });
                
                var mtgTypess = [];
                $("#mtgTypes option:selected").each(function () {
                    mtgTypess.push($(this).val());
                });

                var dialerAcctTypess = [];
                $("#dialerAcctTypes option:selected").each(function () {
                    dialerAcctTypess.push($(this).val());
                });
        

                if (this.options.type === "New Client") {

                    // Validate entries
                    plan = self.$("#planID").val();

                    record = clientRecord = {
                        // clientID: data.clientID,
                        firstName: self.$("#firstName").val(),
                        lastName: self.$("#lastName").val(),
                        customerName: self.$("#customerName").val(),
                        companyClient: (self.$("#companyClient").val()),
                        clientSuccessPlan: (self.$("#clientSuccessPlan").val()),
                        useAsReference: (self.$("#useAsReference").val()),
                        emailAddress: self.$("#emailAddress").val(),
                        address: self.$("#address").val(),
                        city: self.$("#city").val(),
                        state: self.$("#state").val(),
                        zipCode: self.$("#zipCode").val(),
                        mobilePhone: self.$("#mobilePhone").val(),
                        homePhone: self.$("#homePhone").val(),
                        sbCount: (self.$("#sbCountList").val()), 
                        clientTimeZoneID: self.$("#clientTimeZoneID").val(),
                        companyID: self.$("#companyID").val(),
                        industry: self.$("#industry").val(),
                        division: self.$("#division").val(),
                        title: self.$("#title").val(),
                        lfdAuthorization: self.$("#lfdAuthorization").val(),
                        dsuNumber: self.$("#dsuNumber").val(),
                        product: products.join(), 
                        productOtherText: self.$("#productOtherText").val(),
                        distributionChannel: channels.join(),
                        states: self.$("#states").val(),
                        lengthInTerritory: self.$("#lengthInTerritory").val(),
                        workPhone: self.$("#workPhone").val(),
                        workExtension: self.$("#workExtension").val(),
                        assistantInternal: self.$("#assistantInternal").val(),
                        assistantPhone: self.$("#assistantPhone").val(),
                        assistantExtension: self.$("#assistantExtension").val(),
                        assistantEmail: self.$("#assistantEmail").val(),
                        typeOfService: self.$("#typeOfService").val(),
                        referredBy: self.$("#referredBy").val(),
                        planID: self.$("#planID").val(),
                        status: self.$("#status").val(),
                        schedulerID: self.$("#schedulerID").val(),
                        schedulerChangeDate: null,
                        adminSchedulerID: self.$("#adminSchedulerID").val(),
                        taskTargetLow: self.$("#taskTargetLow").val(),
                        mtgTargetLowDec: parseFloat(self.$("#mtgTargetLowDec").val()),
                        realTimeAccess: false, 
                        realTimeUser: self.$("#personalEmail").val(),
                        realTimePassword: self.$("#personalPhone").val(),
                        esaAnniversary: self.esaAnniversary,
                        termDate: self.termDate,
                        schedulingStartDate: self.schedulingStartDate,
                        lastCheckInDate: self.lastCheckInDate,
                        surveySentDate: self.surveySentDate,
                        dataCallDate: self.dataCallDate,
                        dbBuildCompleted: self.dbBuildCompleted,
                        mapCreated: self.mapCreated,
                        mapApproved: self.mapApproved,
                        mapLastUpdated: self.mapLastUpdated,
                        additionalInformation: self.$("#additionalInformation").val(),
                        sysCrmUserName: self.$("#sysCrmUserName").val(),
                        sysCrmPassword: self.$("#sysCrmPassword").val(),
                        sysCrmNotes: self.$("#sysCrmNotes").val(),
                        sysEmailUserName: self.$("#sysEmailUserName").val(),
                        sysEmailPassword: self.$("#sysEmailPassword").val(),
                        sysEmailNotes: self.$("#sysEmailNotes").val(),
                        sysCalendar: self.$("#sysCalendar").val(),
                        sysCalendarUserName: self.$("#sysCalendarUserName").val(),
                        sysCalendarPassword: self.$("#sysCalendarPassword").val(),
                        sysCalendarNotes: self.$("#sysCalendarNotes").val(),
                        checkInStatus: self.$("#checkInStatus").val(),
                        returningClient: self.$("#returningClient").val(),
                        // New fields as of 10/24
                        linkedIn: self.$("#linkedIn").val(),
                        cspTarget: self.$("#cspTarget").val(),
                        clientAppPassword: self.$("#clientAppPassword").val(),
                        companyEmailMFA: self.$("#companyEmailMFA").val(),
                        tier: self.$("#tier").val(),
                        wholesalerRank: self.$("#wholesalerRank").val(),
                        platform: self.$("#platform").val(),
                        distSegment: self.$("#distSegment").val(),
                        secondaryRank: self.$("#secondaryRank").val(),
                        specialDBTags: self.$("#specialDBTags").val(),
                        focusFirms: self.$("#focusFirms").val(),
                        maptiveID: self.$("#maptiveID").val(),
                        clientTerrMap: self.$("#clientTerrMap").val(),
                        mtgTypes: mtgTypess.join(),
                        dialerAcctTypes: dialerAcctTypess.join(),
                        existingZoning: self.$("#existingZoning").val(),
                        travelRotation: self.$("#travelRotation").val(),
                        schedDirectives: self.$("#schedDirectives").val(),
                        zoneMapNotes: self.$("#zoneMapNotes").val(),
                        adminDept: self.$("#adminDept").val(),
                        specialInstForDB: self.$("#specialInstForDB").val()
                    };

                    self.clientDataSource.add(record);
                    self.clientDataSource.sync();

                } else {
                    this.clientDataSource.fetch(function () {
                        var data = this.data();
                        var count = data.length;

                        //console.log('ProgressClientView:saveButtonClicked:product:' + self.$("#product").val());

                        if (count > 0) {

                            record = self.clientDataSource.at(0);
                            var schedulerID = parseInt(self.$("#schedulerID").val(), 0);
                            
                            if (isNaN(schedulerID)) {
                                schedulerID = 0;
                            }
                            
                            record.set("schedulerID", schedulerID);
                            // record.set("schedulerName", self.$("#schedulerName").val());
                            record.set("companyID", self.$("#companyID").val());
                            record.set("companyClient", (self.$("#companyClient").val() === "true"));
                            record.set("clientSuccessPlan", (self.$("#clientSuccessPlan").val() === "true"));
                            record.set("useAsReference", (self.$("#useAsReference").val() === "true"));
                            record.set("taskTargetLow", self.$("#taskTargetLow").val());
                            record.set("taskTargetHigh", self.$("#taskTargetHigh").val());
                            record.set("mtgTargetLowDec", parseFloat(self.$("#mtgTargetLowDec").val()));
                            record.set("mtgTargetHigh", self.$("#mtgTargetHigh").val());
                            record.set("firstName", self.$("#firstName").val());
                            record.set("lastName", self.$("#lastName").val());
                            record.set("customerName", self.$("#customerName").val());
                            record.set("emailAddress", self.$("#emailAddress").val());
                            record.set("address", self.$("#address").val());
                            record.set("city", self.$("#city").val());
                            record.set("state", self.$("#state").val());
                            record.set("zipCode", self.$("#zipCode").val());
                            record.set("mobilePhone", self.$("#mobilePhone").val());
                            record.set("homePhone", self.$("#homePhone").val());
                            record.set("sbCount", (self.$("#sbCountList").val() === "true"));
                            record.set("clientTimeZoneID", self.$("#clientTimeZoneID").val());
                            record.set("industry", self.$("#industry").val());
                            record.set("divisionID", self.$("#divisionID").val());
                            record.set("division", self.$("#division").val());
                            record.set("title", self.$("#title").val());
                            record.set("lfdAuthorization", self.$("#lfdAuthorization").val());
                            record.set("dsuNumber", self.$("#dsuNumber").val());
                            record.set("product", products.join()); //self.$("#product").val());
                            record.set("productOtherText", self.$("#productOtherText").val());
                            record.set("distributionChannel", channels.join());
                            record.set("states", self.$("#states").val());
                            record.set("lengthInTerritory", self.$("#lengthInTerritory").val());
                            record.set("workPhone", self.$("#workPhone").val());
                            record.set("workExtension", self.$("#orkExtension").val());
                            record.set("assistantInternal", self.$("#assistantInternal").val());
                            record.set("assistantPhone", self.$("#assistantPhone").val());
                            record.set("assistantExtension", self.$("#assistantExtension").val());
                            record.set("assistantEmail", self.$("#assistantEmail").val());
                            record.set("typeOfService", self.$("#typeOfService").val());
                            record.set("referredBy", self.$("#referredBy").val());
                            record.set("planID", self.$("#planID").val());

                            var status = self.$("#status").val();
                            record.set("status", status);

                            record.set("adminSchedulerID", self.$("#adminSchedulerID").val());
                            record.set("realTimeUser", self.$("#personalEmail").val());
                            record.set("realTimePassword", self.$("#personalPhone").val());
                            record.set("esaAnniversary", self.$("#esaAnniversary").val());
                            record.set("termDate", self.$("#termDate").val());
                            record.set("schedulingStartDate", self.$("#schedulingStartDate").val());
                            record.set("lastCheckInDate", self.$("#lastCheckInDate").val());
                            record.set("surveySentDate", self.$("#surveySentDate").val());
                            record.set("dataCallDate", self.$("#dataCallDate").val());
                            record.set("dbBuildCompleted", self.$("#dbBuildCompleted").val());
                            record.set("mapCreated", self.$("#mapCreated").val());
                            record.set("mapApproved", self.$("#mapApproved").val());
                            record.set("mapLastUpdated", self.$("#mapLastUpdated").val());
                            record.set("additionalInformation", self.$("#additionalInformation").val());
                            record.set("sysCrmUserName", self.$("#sysCrmUserName").val());
                            record.set("sysCrmPassword", self.$("#sysCrmPassword").val());
                            record.set("sysCrmNotes", self.$("#sysCrmNotes").val());
                            record.set("sysEmailUserName", self.$("#sysEmailUserName").val());
                            record.set("sysEmailPassword", self.$("#sysEmailPassword").val());
                            record.set("sysEmailNotes", self.$("#sysEmailNotes").val());
                            record.set("sysCalendar", self.$("#sysCalendar").val());
                            record.set("sysCalendarUserName", self.$("#sysCalendarUserName").val());
                            record.set("sysCalendarPassword", self.$("#sysCalendarPassword").val());
                            record.set("sysCalendarNotes", self.$("#sysCalendarNotes").val());
                            record.set("checkInStatus", self.$("#checkInStatus").val());
                            record.set("returningClient", (self.$("#returningClient").val() === "true"));
                            record.set("clientSuccessPlan", (self.$("#clientSuccessPlan").val() === "true"));

                            if (self.schedulerDateChanged && status === "4 - Active") {
                                record.set("schedulerChangeDate", self.schedulerChangeDate);
                            }

                            // New fields (10/24)
                            record.set("linkedIn", self.$("#linkedIn").val());
                            record.set("cspTarget", self.$("#cspTarget").val());
                            record.set("clientAppPassword", self.$("#clientAppPassword").val());
                            record.set("companyEmailMFA", self.$("#companyEmailMFA").val());
                            record.set("tier", self.$("#tier").val());
                            record.set("wholesalerRank", self.$("#wholesalerRank").val());
                            record.set("platform", self.$("#platform").val());
                            record.set("distSegment", self.$("#distSegment").val());
                            record.set("secondaryRank", self.$("#secondaryRank").val());
                            record.set("specialDBTags", self.$("#specialDBTags").val());
                            record.set("focusFirms", self.$("#focusFirms").val());
                            record.set("maptiveID", self.$("#maptiveID").val());
                            record.set("clientTerrMap", self.$("#clientTerrMap").val());
                            record.set("mtgTypes", mtgTypess.join());
                            record.set("dialerAcctTypes", dialerAcctTypess.join());
                            record.set("existingZoning", self.$("#existingZoning").val());
                            record.set("travelRotation", self.$("#travelRotation").val());
                            record.set("schedDirectives", self.$("#schedDirectives").val());
                            record.set("zoneMapNotes", self.$("#zoneMapNotes").val());
                            record.set("adminDept", self.$("#adminDept").val());
                            record.set("specialInstForDB", self.$("#specialInstForDB").val());

                            console.log('ProgressClientView:clientDataSource:save:recordToSave:' + JSON.stringify(record));

                            $.when(this.sync()).done(function (e) {

                                if (self.schedulerDateChanged && status === "4 - Active") {
                                    self.createSchedulerHistoryTableRecord(self,app,record);
                                } else {
                                    //console.log('ProgressClientView:clientDataSource:add new sync done:' + record.clientID);
                                    app.model.set('selectedClientId', record.clientID);
                                    app.model.set('widget1Value', null);
                                    $.cookie('ESA:AppModel:widget1Value', "");

                                    if (self.addToAddedFlag === true) {
                                        $.when(self.addToAdded()).done(function (e) {
                                            app.router.navigate('clientProfile', { trigger: true });
                                        });
                                    } else if (self.addToLostFlag === true) {
                                        $.when(self.addToLost()).done(function (e) {
                                            app.router.navigate('clientProfile', { trigger: true });
                                        });
                                    } else {
                                        app.router.navigate('clientProfile', { trigger: true });
                                    }
                                }
                            });
                        }
                    });
                }

            },
            addToLost: function () {
                //console.log('ProgressClientView:addToLost');

                var self = this;

                var termDate = new Date(self.termDate);
                var year = termDate.getFullYear();
                var month = termDate.getMonth() + 1;

                self.scoreBoardDataSource.filter([
                    { field: "year", operator: "eq", value: year },
                    { field: "month", operator: "eq", value: month }
                ]);

                self.scoreBoardDataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count > 0) {
                        record = self.scoreBoardDataSource.at(0);
                        var lost = record.lost;
                        lost = lost + 1;
                        record.set("lost", lost);
                        self.scoreBoardDataSource.sync();
                        //console.log('ProgressClientView:addToLost:done');
                    }
                });
            },
            addToAdded: function () {
                //console.log('ProgressClientView:addToAdded');

                var self = this;
                var date = new Date(self.esaAnniversary);
                var year = date.getFullYear();
                var month = date.getMonth() + 1;

                self.scoreBoardDataSource.filter([
                    { field: "year", operator: "eq", value: year },
                    { field: "month", operator: "eq", value: month }
                ]);

                self.scoreBoardDataSource.fetch(function () {
                    var data = this.data();
                    var count = data.length;

                    if (count > 0) {
                        record = self.scoreBoardDataSource.at(0);
                        var added = record.added;
                        added = added + 1;
                        record.set("added", added);
                        self.scoreBoardDataSource.sync();
                        //console.log('ProgressClientView:addToAdded:done');
                    }

                    //if (this.options.type !== "New Client") {
                    //    App.router.navigate('clientProfile', {trigger: true});
                    //}
                });
            },
            createAutoClosingAlert: function (selector, html) {
                //console.log('HomeView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 5000);
            },
            createSchedulerHistoryTableRecord: function (self, app, clientRecord) {

                console.log('ProgressClientView:createSchedulerHistoryTableRecord');
                var historyDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ClientSchedulerHistoryTable";
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('ProgressClientView:historyDataSource:update');
                                    return App.config.DataServiceURL + "/odata/ClientSchedulerHistoryTable" + "(" + data.schedulerHistoryID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('ProgressClientView:historyDataSource:create');
                                    return App.config.DataServiceURL + "/odata/ClientSchedulerHistoryTable";
                                },
                                complete: function (e) {
                                    if (e.responseJSON.value[0]) {
                                        var newID = e.responseJSON.value[0].schedulerHistoryID;
                                        console.log('ProgressClientView:historyDataSource:create:complete:' + newID);
                                        //App.model.set('selectedClientId', newID);
                                    }
                                },
                                error: function (e) {
                                    var message = 'ProgressClientView:historyDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('ProgressClientView:historyDataSource:destroy');
                                    return App.config.DataServiceURL + "/odata/ClientSchedulerHistoryTable" + "(" + data.schedulerHistoryID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "schedulerHistoryID",
                                fields: {
                                    schedulerHistoryID: { editable: false, defaultValue: 0, type: "number" },
                                    clientID: { type: "number" },
                                    employeeID: { type: "number" },
                                    schedulerChangeTimeStamp: { editable: true, type: "date", validation: { required: true } }
                                }
                            }
                        },
                        sort: { field: "schedulerHistoryID", dir: "desc" },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   ProgressClientView:historyDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingHistoryWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    ProgressClientView:historyDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingHistoryWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            console.log('   ProgressClientView:historyDataSource:error');
                            kendo.ui.progress(self.$("#loadingHistoryWidget"), false);

                            //self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        filter: { field: "clientID", operator: "eq", value: clientRecord.clientID }

                    });

                historyDataSource.fetch(function () {
                    var newRecord = {
                        clientID: clientRecord.clientID,
                        schedulerChangeTimeStamp: clientRecord.schedulerChangeDate,
                        employeeID: clientRecord.schedulerID
                    };

                    historyDataSource.add(newRecord);

                    $.when(historyDataSource.sync()).done(function (e) {
                        app.model.set('selectedClientId', clientRecord.clientID);
                        app.model.set('widget1Value', null);
                        $.cookie('ESA:AppModel:widget1Value', "");

                        if (self.addToAddedFlag === true) {
                            $.when(self.addToAdded()).done(function (e) {
                                app.router.navigate('clientProfile', { trigger: true });
                            });
                        } else if (self.addToLostFlag === true) {
                            $.when(self.addToLost()).done(function (e) {
                                app.router.navigate('clientProfile', { trigger: true });
                            });
                        } else {
                            app.router.navigate('clientProfile', { trigger: true });
                        }
                    });
                });
            },
            createClientPlanTableRecord: function (clientRecord) {

                //console.log('ProgressClientView:createClientPlanTableRecord');
                var self = this;
                var app = App;

                self.clientPlanDataSource.filter([
                    { field: "clientID", operator: "eq", value: clientRecord.clientID },
                    { field: "endDate", operator: "eq", value: null }]
                );


                this.clientPlanDataSource.fetch(function () {
                    var offset = (new Date()).getTimezoneOffset();
                    //var now = new Date(moment().clone().subtract((offset), 'minutes'));

                    if (clientRecord.planID !== 0) { //  && clientRecord.planID !== 285 (285 is the "None" plan)
                        var newRecord = {
                            clientID: clientRecord.clientID,
                            planID: clientRecord.planID,
                            planType: self.selectedPlan ? self.selectedPlan.planType : 'X',
                            planName: self.selectedPlan ? self.selectedPlan.planName : '--- NONE ---',
                            description: self.selectedPlan ? self.selectedPlan.description : 'DO NOT DELETE',
                            planPrice: self.selectedPlan ? self.selectedPlan.planPrice : 0,
                            rateSheetHours_4wk: self.selectedPlan ? self.selectedPlan.rateSheetHours_4wk : 0,
                            schedulingHours_4wk: self.selectedPlan ? self.selectedPlan.schedulingHours_4wk : 0,
                            schedulingHours_1wk: self.selectedPlan ? self.selectedPlan.schedulingHours_1wk : 0,
                            schedulingHours_day: self.selectedPlan ? self.selectedPlan.schedulingHours_day : 0,
                            startDate: new Date(0),
                            endDate: null
                        };

                        self.clientPlanDataSource.add(newRecord);
                        self.clientPlanDataSource.sync();
                    }

                    //console.log('ProgressClientView:createClientPlanTableRecord:navigate to client profile');
                    if (self.addToAddedFlag === true) {
                        $.when(self.addToAdded()).done(function (e) {
                            app.router.navigate('clientProfile', { trigger: true });
                        });
                    } else if (self.addToLostFlag === true) {
                        $.when(self.addToLost()).done(function (e) {
                            app.router.navigate('clientProfile', { trigger: true });
                        });
                    } else {
                        app.router.navigate('clientProfile', { trigger: true });
                    }

                });

            },
            updateClientPlanTable: function (clientRecord) {

                //console.log('ProgressClientView:updateClientPlanTable');
                var self = this;
                var app = App;

                self.clientPlanDataSource.filter([
                    { field: "clientID", operator: "eq", value: clientRecord.clientID },
                    { field: "endDate", operator: "eq", value: null }]
                );
                this.clientPlanDataSource.fetch(function () {

                    var data = this.data();
                    var count = data.length;

                    if (count > 0) {
                        var clientPlanRecord = self.clientPlanDataSource.at(0);

                        // if plan changed
                        if (clientRecord.planID !== clientPlanRecord.planID) {

                            // edit existing clientPlan record endDate = today
                            var offset = (new Date()).getTimezoneOffset();
                            var now = new Date(moment().clone().subtract((offset), 'minutes'));
                            var startDate = new Date(moment(clientPlanRecord.startDate).clone().subtract((offset), 'minutes'));
                            clientPlanRecord.set("startDate", startDate);
                            clientPlanRecord.set("endDate", now);
                            //this.sync();
                            //this.clientPlanDataSource.sync();

                            //{
                            // "clientPlanID":2963,
                            // "clientID":621,
                            // "planID":85,
                            // "planType":"S",
                            // "planName":"2009-(AMPF) Hourly Scheduling Rate",
                            // "description":"0",
                            // "planPrice":"33.0000",
                            // "rateSheetHours_4wk":null,
                            // "schedulingHours_4wk":0.0,
                            // "schedulingHours_1wk":0.0,
                            // "schedulingHours_day":0.0,
                            // "startDate":"1970-01-01T00:00:00",
                            // "endDate":null
                            //}

                            // write new clientPlan record to tblClientPlan with new planID, startDate = now and empty endDate
                            if (self.selectedPlan !== null && self.selectedPlan !== undefined && clientRecord.planID !== 0) {

                                var newRecord = {
                                    clientID: clientPlanRecord.clientID,
                                    planID: clientRecord.planID,
                                    planType: self.selectedPlan.planType,
                                    planName: self.selectedPlan.planName,
                                    description: self.selectedPlan.description,
                                    planPrice: self.selectedPlan.planPrice,
                                    rateSheetHours_4wk: self.selectedPlan.rateSheetHours_4wk,
                                    schedulingHours_4wk: self.selectedPlan.schedulingHours_4wk,
                                    schedulingHours_1wk: self.selectedPlan.schedulingHours_1wk,
                                    schedulingHours_day: self.selectedPlan.schedulingHours_day,
                                    startDate: now,
                                    endDate: null
                                };

                                self.clientPlanDataSource.add(newRecord);
                                self.clientPlanDataSource.sync();
                            }

                        }
                    }

                });

            },
            // onMtgTypeChanged: function (e) {

            //     var selectedMtgTypes = [];

            //     $("#mtgTypes option:selected").each(function () {
            //         selectedMtgTypes.push($(this).val());
            //     });

            //     console.log('ProgressClientView:onMtgTypeChanged:selectedMtgTypes:' + this.selectedMtgTypes);
            // },
            onProductChanged: function (e) {

                var selectedProducts = [];

                $("#product option:selected").each(function () {
                    selectedProducts.push($(this).val());
                });

                if (selectedProducts.join().indexOf("Other") > -1) {
                    self.$("#productOtherText").show();
                    self.$("#productOtherText").data("kendoMaskedTextBox").value(this.productOtherText);
                    self.$("#productOtherTextLabel").show();
                } else {
                    self.$("#productOtherText").hide();
                    self.$("#productOtherTextLabel").hide();
                }

                console.log('ProgressClientView:onProductChanged:selectedProduct:productOtherText' + this.selectedProduct + ":" + this.productOtherText);
            },
            onStatusChanged: function (e) {
                this.selectedStatus = e.sender._selectedValue;
                //console.log('ProgressClientView:onStatusChanged:selectedStatus:' + this.selectedStatus + ':originalStatus:' + this.originalStatus);
            },
            onPlanChanged: function (e) {
                this.selectedPlan = this.planDataSource.data()[e.sender.selectedIndex];
                //console.log('ProgressClientView:onPlanChanged:planRecord:' + JSON.stringify(this.selectedPlan));
            },
            onSchedulerChanged: function (e) {
                this.schedulerChangeDate = new Date();
                this.schedulerDateChanged = true;
                console.log('ProgressClientView:onSchedulerChanged:schedulerChangeDate:' + JSON.stringify(this.schedulerChangeDate));

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressClientView:displayNoDataOverlay');

                // Display the no data overlay
                //if (this.$('.no-data-overlay-outer').length === 0)
                //    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressClientView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressClientView:resize');

            },
            remove: function () {
                //console.log("-------- ProgressClientView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });