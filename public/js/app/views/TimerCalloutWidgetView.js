define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/timerCalloutWidget', 'kendo/kendo.data', 'kendo/kendo.combobox'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('TimerCalloutWidgetView:initialize:options:' + options);

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                this.$(".showTimer").hide();
                this.timer = null;
                this.seconds = 0;
                this.minutes = 0;
                this.hours = 0;
                this.timerStatus = App.model.get('timerStatus');
                this.currentTimerID = App.model.get('currentTimerID');
                this.intTimerID = App.model.get('intTimerID');

                this.userType = parseInt(App.model.get('userType'), 0);

                this.today = moment().clone().format('YYYY-MM-DD');

                this.timerDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalTimer",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            //aggregate: [
                            //    { field: "timeWorked", aggregate: "sum" }
                            //],
                            model: {
                                id: "timerID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    idTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    entryType: {editable: true, type: "string", validation: {required: false}},
                                    employeeID: {editable: true, type: "number"},
                                    clientID: {editable: true, type: "number"},
                                    serviceID: {editable: true, type: "number"},
                                    workDate: {editable: true, type: "string", validation: {required: false}},
                                    startTime: {
                                        editable: true,
                                        type: "date",
                                        format: "{0:s}"
                                    },
                                    stopTime: {editable: true, type: "date", validation: {required: false}},
                                    timeWorked: {editable: true, type: "number"},
                                    timeWorkedText: {editable: true, type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    intTimerID: {editable: true, type: "number"},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    editTimeStamp: {editable: true, type: "date", validation: {required: false}},
                                    editEmployeeID: {editable: true, type: "number"},
                                    tasksCalls: {editable: true, type: "number"},
                                    tasksEmails: {editable: true, type: "number"},
                                    tasksVoiceMails: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: {field: "timerID", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('TimerCalloutWidgetView:dataSource:request start:');
                            //kendo.ui.progress(self.$("#loadingCalloutWidget"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('TimerCalloutWidgetView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#loadingCalloutWidget"), false);
                        },
                        error: function (e) {
                            //console.log('TimerCalloutWidgetView:dataSource:error');
                            //kendo.ui.progress(self.$("#loadingCalloutWidget"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('TimerCalloutWidgetView:change');

                        },
                        serverFiltering: true,
                        serverSorting: true
                    });
            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('---------- TimerCalloutWidgetView:onShow ----------');

                this.populateWidget();
            },
            populateWidget: function () {
                console.log('TimerCalloutWidgetView:populateWidget');

                var self = this;
                var app = App;

                self.timerStatus = App.model.get('timerStatus');
                self.$('#closeAlert').hide();

                self.timerAlert = true;

                // For now this is the timer alert
                console.log('TimerCalloutWidgetView:populateWidget:timerStatus:' + self.timerStatus);

                var today = moment(new Date()).format('dddd, MMMM DD YYYY');

                var startTime = moment(App.model.get('timerStartTime'));
                var startTimeShow = startTime.format("h:mma");
                var interruptTime = App.model.get('timerInterruptTime');
                //console.log('TimerCalloutWidgetView:populateWidget:interruptTime:' + interruptTime);
                var interruptTimeM = moment(interruptTime);
                var interruptTimeShow = interruptTimeM.format("h:mma");

                if (self.timerStatus === "INTERRUPTED" || self.timerStatus === "Finish") {
                    self.$('.alert').removeClass('alert-danger');
                    self.$('.alert').addClass('alert-warning');
                } else {
                    self.$('.alert').addClass('alert-danger');
                    self.$('.alert').removeClass('alert-warning');
                }
                self.$('.alert').addClass('alert-dismissable');

                self.$('#alertHeader').html(self.options.type);
                self.$('#alertText').html(self.options.message);
                self.$('#alertDate').html("<b>Start Time:</b> " + today);

                if (startTime !== null && self.timerStatus !== "Stopped") {
                    self.$(".showTimer").show();
                    self.$('#alertStartTime').html('&nbsp' + startTimeShow);

                    if (interruptTime !== null) {
                        self.$('#alertInterruptTime').html("<b>Interrupt Time: </b>" + interruptTimeShow);
                    }
                    self.displayCounter(startTime, interruptTime);

                    // Change dashboard length
                    //console.log('TimerCalloutWidgetView:populateWidget:-140 dashboard');
                    $('.esa-section').css('height', '100%').css('height', '-=140px');
                    $('.esa-pp-section').css('height', '100%').css('height', '-=116px');
                    $('.esa-views-section').css('height', '100%').css('height', '-=116px');
                    $('.esa-tables-section').css('height', '100%').css('height', '-=123px');
                    $('.esa-stats-timecard-section').css('height', '100%').css('height', '-=136px');
                } else {
                    //console.log('TimerCalloutWidgetView:populateWidget:-65 dashboard');
                    $('.esa-section').css('height', '100%').css('height', '-=65px');
                    $('.esa-pp-section').css('height', '100%').css('height', '-=65px');
                    $('.esa-views-section').css('height', '100%').css('height', '-=31px');
                    $('.esa-tables-section').css('height', '100%').css('height', '-=35px');
                    $('.esa-stats-timecard-section').css('height', '100%').css('height', '-=50px');
                }


                if (self.options.borderRadius !== undefined) {
                    self.$('.alert').css('border-radius', self.options.borderRadius);
                }


            },
            displayCounter: function (startTime, interruptTime) {

                console.log('TimerCalloutWidgetView:displayCounter');
                var self = this;
                var now = moment();

                var timeWorked = 0;

                self.timerDataSource.filter(
                    {field: "intTimerID", operator: "eq", value: self.intTimerID}
                );
                self.timerDataSource.fetch(function () {

                    var data = this.data();

                    self.hours = 0;
                    self.minutes = 0;
                    self.seconds = 0;

                    if (data.length > 0 && interruptTime === null) {
                        startTime = data[data.length - 1].startTime;
                        var timerAction = data[data.length - 1].timerAction;
                        var then = moment(startTime).clone();
                        if (timerAction !== "finish") {

                            self.seconds = parseFloat(now.diff(then, 'seconds')) % 60;
                            self.minutes = parseFloat(now.diff(then, 'minutes'));

                            var hoursToAdd = 0;
                            var minutesLeft = 0;
                            if (parseFloat(now.diff(then, 'minutes')) % 60 > 0) {
                                hoursToAdd++;
                                minutesLeft = parseFloat(now.diff(then, 'minutes')) % 60;
                                self.minutes = minutesLeft;
                            }

                            self.hours = parseFloat(now.diff(then, 'hours')); // + hoursToAdd ;

                        }
                    }

                    $.each(data, function (index, value) {
                        timeWorked += value.timeWorked;
                    });

                    //console.log('TimerCalloutWidgetView:displayCounter:withInterrupt:timeWorked(hours):' + timeWorked);
                    //console.log('TimerCalloutWidgetView:displayCounter:withInterrupt:hours:' + self.hours);
                    //console.log('TimerCalloutWidgetView:displayCounter:withInterrupt:minutes:' + self.minutes);

                    if (timeWorked - self.hours > 0) {
                        self.hours += Math.floor(timeWorked);
                        self.minutes += Math.floor((timeWorked - self.hours) * 60);
                        //self.seconds += Math.floor(self.minutes) * 60);
                    }

                    self.timer = setTimeout(self.addTime, 1000); //1000
                });
                //});
            },
            addTime: function () {

                //console.log('TimerCalloutWidgetView:addTime');

                var self = this;
                var refreshTimer = false;

                this.seconds++;
                //this.seconds = this.seconds + 5;
                if (this.seconds >= 60) {

                    refreshTimer = true;
                    if (self.timerStatus !== "Stopped") {
                        self.populateWidget();
                    }

                    this.seconds = 0;
                    this.minutes++;
                    if (this.minutes >= 60) {
                        this.minutes = 0;
                        this.hours++;
                    }
                }

                var totalTimeWorked = App.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(this.hours + this.minutes / 60 + this.seconds / 3600);
                this.$('#alertTimer').html("<b>Time Worked: </b> " + totalTimeWorked + "  (" + (this.hours ? (this.hours > 9 ? this.hours : "0" + this.hours) : "00") + ":" + (this.minutes ? (this.minutes > 9 ? this.minutes : "0" + this.minutes) : "00") + ":" + (this.seconds > 9 ? this.seconds : "0" + this.seconds) + " minutes)");

                if (this.timerStatus !== "INTERRUPTED" && this.timerStatus !== "Finish") {
                    this.timer = setTimeout(function () {
                        if (refreshTimer === true) {
                            self.seconds++;
                            //self.seconds = self.seconds + 5;
                            self.$('#alertTimer').html("<b>Time Worked: </b> " + totalTimeWorked + "  (" + (self.hours ? (self.hours > 9 ? self.hours : "0" + self.hours) : "00") + ":" + (self.minutes ? (self.minutes > 9 ? self.minutes : "0" + self.minutes) : "00") + ":" + (self.seconds > 9 ? self.seconds : "0" + self.seconds) + " minutes)");
                        } else {
                            self.addTime();
                        }
                    }, 1000);


                }

            },
            displayNoDataOverlay: function () {
                //console.log('TimerCalloutWidgetView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {
//                //console.log('TimerCalloutWidgetView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('TimerCalloutWidgetView:resize');

            },
            remove: function () {
                //console.log('---------- TimerCalloutWidgetView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // When closing timer alert remove extra space created for it in dashboard section area
                if (this.timerAlert === true) {
                    $('.esa-section').css('height', '100%').css('height', '-=65px');
                    $('.esa-pp-section').css('height', '100%').css('height', '-=65px');
                    $('.esa-views-section').css('height', '100%').css('height', '-=31px');
                    $('.esa-tables-section').css('height', '100%').css('height', '-=35px');
                    $('.esa-stats-timecard-section').css('height', '100%').css('height', '-=50px');
                }

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });