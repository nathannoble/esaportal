define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalAdminMessagesDataTable', 'views/PortalAdminMessagesDataGridView'],
    function (App, Backbone, Marionette, $, template, PortalAdminMessagesDataGridView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                adminDataRegion: "#admin-data-region"
            },
            initialize: function () {

                //console.log('PortalAdminMessagesDataTableView:initialize');

                _.bindAll(this);

                App.modal.currentView = null;

            },
            onRender: function () {

                //console.log('PortalAdminMessagesDataTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminMessagesDataTableView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.adminDataRegion.show(new PortalAdminMessagesDataGridView());
                }



            },
            validateData: function () {
                var self = this;

                if (App.modal.currentView === null || App.modal.currentView === undefined) {

                }
            },
            resize: function () {
                //console.log('PortalAdminMessagesDataTableView:resize');

                if (this.adminDataRegion.currentView)
                    this.adminDataRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalAdminMessagesDataTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });