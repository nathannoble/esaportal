define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/myTimeCardDetailGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {

                _.bindAll(this);

                var self = this;
                var app = App;

                this.userType = parseInt(App.model.get('userType'), 0);
                this.myEmployeeId = App.model.get('myEmployeeId');
                //console.log('MyTimeCardDetailGridView:initialize:myEmployeeId:' + this.myEmployeeId);

                this.dateFilter = App.model.get('selectedDateFilter');
                var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                this.startDate = moment(startDate,'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                this.endDate = moment(endDate,'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.restDataSource = new kendo.data.DataSource(
                    {
                    autoBind: false,
                    transport: {
                        type: "odata",
                        read: {
                            url: function () {
                                var url = App.config.DataServiceURL + "/rest/GetMyTimeCardDetail/" + self.startDate + "/" + self.endDate + "/" + self.myEmployeeId;
                                return url;
                            },
                            complete: function (e) {
                                console.log('MyTimeCardDetailGridView:restDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'MyTimeCardDetailGridView:restDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            type: "GET",
                            dataType: "json"
                        }
                        //autoBind: false,
                    },
                    schema: {
                        //data: "value",
                        //total: function (data) {
                        //    return data['odata.count'];
                        //},
                        model: {
                            id: "employeeID",
                            fields: {

                                intTimerID: {editable: false, type: "number"},
                                companyID: {editable: false, type: "number"},
                                clientID: {editable: false, type: "number"},
                                employeeID: {editable: false, type: "number"},
                                serviceID: {editable: false, type: "number"},
                                workDate: {
                                    editable: true,
                                    type: "date"
                                    //format: "{0:yyyy-MM-ddTHH}",
                                    //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                },
                                startTime: {
                                    editable: true,
                                    type: "date"
                                    //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                },
                                stopTime: {
                                    editable: true,
                                    type: "date"
                                    //format: "{0:yyyy-MM-ddTHH:mm:ss}"
                                },
                                clientCompany: {editable: true, type: "string", validation: {required: false}},
                                clientName: {editable: true, type: "string", validation: {required: false}},
                                customerName: {editable: true, type: "string", validation: {required: true}},
                                employeeName: {editable: true, type: "string", validation: {required: false}},
                                serviceItem: {type: "string", validation: {required: false}},
                                tasks: {editable: true, type: "number"},
                                meetings: {editable: true, type: "number"},
                                timeWorked: {editable: true, type: "number"},
                                timerNote: {editable: true, type: "string", validation: {required: false}},
                                mgrNote: {editable: true, type: "string", validation: {required: false}},
                                timerAction: {editable: true, type: "string", validation: {required: false}},
                                recordType: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    //group: { field: "clientName" },
                    sort: [
                        {field: "clientName", dir: "asc"},
                        {field: "workDate", dir: "asc"}
                    ],
                    //filter: {field: "lastName", operator: "neq", value: ""},
                    requestStart: function (e) {
                        //console.log('MyTimeCardDetailGridView:dataSource:request start:');
                        kendo.ui.progress(self.$("#timeCardDetailLoading"), true);
                    },
                    requestEnd: function (e) {
                        //console.log('MyTimeCardDetailGridView:dataSource:request end:');
                        //var data = this.data();
                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                    },
                    change: function (e) {
                        //console.log('MyTimeCardDetailGridView:dataSource:change:');

                        var data = this.data();
                        if (data.length > 0) {

                        } else {
                            self.displayNoDataOverlay();
                        }
                    },
                    error: function (e) {
                        //console.log('MyTimeCardDetailGridView:dataSource:error');
                        kendo.ui.progress(self.$("#timeCardDetailLoading"), false);
                    }
                });

            },
            getData: function () {

                var self = this;

                    //console.log('MyTimeCardDetailGridView:getData:employeeID:' + self.myEmployeeId);

                    self.restDataSource.fetch(function (data) {
                        //console.log('MyTimeCardDetailGridView:getData:data 1st record:' + JSON.stringify(this.data[0]));
                        var grid = self.$("#timeCardDetail").data("kendoGrid");
                        if (grid !== undefined && grid !== null) {
                            grid.setDataSource(self.restDataSource);
                        }

                        // Resize the grid
                        setTimeout(self.resize, 0);

                    });

            },
            onRender: function () {
                //console.log('MyTimeCardDetailGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- MyTimeCardDetailGridView:onShow --------");
                var self = this;

                this.grid = this.$("#timeCardDetail").kendoGrid({
                    //dataSource: self.dataSource,
                    //toolbar: ["create"],
                    height: 500,
                    editable: "popup",  //inline, popup
                    sortable: true,
                    //extra: false,
                    //selectable: "multiple",
                    resizable: true,
                    reorderable: true,
                    groupable: true,
                    columnResize: function (e) {
                        //self.resize();
                        window.setTimeout(self.resize, 10);
                    },
                    columns: [
                    {
                        title: "Client Company",
                        field: "clientCompany",
                        template: function (e) {
                            //var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                            //return template;
                            var template = "";
                            if (self.userType > 1) {
                                template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                            } else {
                                template = "<div style='text-align: left'><span>" + e.clientCompany + "</span></div>";
                            }
                            return template;
                        },
                        width: 120
                    },
                    {
                        title: "Client Name",
                        field: "clientName",
                        width: 120,
                        template: function (e) {
                            //var template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" + e.clientName + "</a></div>";
                            //return template;
                            var template = "";
                            if (self.userType > 1) {
                                template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" +  e.clientName + "</a></div>";
                            } else {
                                template = "<div style='text-align: left'><span>" + e.clientName + "</span></div>";
                            }
                            return template;
                        },
                        filterable: false,
                        sortable: true
                        //locked: true
                    },
                    {
                        field: "serviceItem",
                        title: "Service",
                        width: 100
                        //locked: true
                    }, {
                        field: "workDate",
                        title: "Date",
                        width: 80,
                        format: "{0:M/d/yyyy}"
                        //locked: true
                    }, {
                        field: "startTime",
                        title: "Start Time",
                        width: 80,
                        format: "{0:h:mm:ss tt}"
                    }, {
                        field: "stopTime",
                        title: "Stop Time",
                        width: 80,
                        format: "{0:h:mm:ss tt}"
                    }, {
                        field: "timeWorked",
                        title: "Time Worked",
                        width: 80
                    },
                    //{
                    //    field: "tasks",
                    //    title: "Tasks",
                    //    width: 80
                    //}, {
                    //    field: "meetings",
                    //    title: "Meetings",
                    //    width: 80
                    //},
                    //{
                    //    field: "timerNote",
                    //    title: "Note",
                    //    width: 100
                    //},
                    {
                        field: "mgrNote",
                        title: "Manager Note",
                        width: 120
                    }
                    //{
                    //    field: "timerAction",
                    //    title: "Action",
                    //    width: 80
                    //}, {
                    //    field: "recordType",
                    //    title: "Type",
                    //    width: 100
                    //}
                    ],
                    dataBound: function (e) {
                        //console.log("MyTimeCardDetailGridView:onShow:grid:dataBound");
                        self.$('.client-profile').on('click', self.viewClientProfile);
                        self.$('.company-profile').on('click', self.viewCompanyProfile);
                    },
                    change: function (e) {
                        //console.log('MyTimeCardDetailGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('MyTimeCardDetailGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('MyTimeCardDetailGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        //e.container.find("input[name='employeeID']").prop("disabled", true);

                    }
                }); //.data("kendoGrid");

                if (this.myEmployeeId !== null && this.myEmployeeId !== undefined) {
                    this.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);

                    this.getData();
                }

            },
            viewCompanyProfile: function (e) {
                //console.log('MyTimeCardDetailGridView:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('MyTimeCardDetailGridView:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId', companyId);

                App.router.navigate('companyProfile', {trigger: true});

            },
            viewClientProfile: function (e) {
                //console.log('MyTimeCardDetailGridView:viewClientProfile:e:' + e.target);

                var clientId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    clientId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    clientId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('MyTimeCardDetailGridView:viewClientProfile:e:' + clientId);
                App.model.set('selectedClientId', clientId);


                App.router.navigate('clientProfile', {trigger: true});

            },
            displayNoDataOverlay: function () {
                //console.log('MyTimeCardDetailGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#timeCardDetail').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('MyTimeCardDetailGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#timeCardDetail').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    //this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('MyTimeCardDetailGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#timeCardDetail").parent().parent().height();
                this.$("#timeCardDetail").height(parentHeight);

                parentHeight = parentHeight - 45;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                console.log('MyTimeCardDetailGridView:resize:parentHeight:' + parentHeight);
                this.$("#timeCardDetail").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- MyTimeCardDetailGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#timeCardDetail").data('ui-tooltip'))
                    this.$("#timeCardDetail").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });