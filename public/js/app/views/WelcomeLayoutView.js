define(['App', 'jquery', 'hbs!templates/welcomeLayout', 'backbone', 'marionette', 'underscore', 
        'views/SubHeaderView', 'views/WelcomeBioView','views/WelcomeNewsView',
        'views/WelcomeSocialMediaView','views/WelcomeESAMediaView','views/WelcomeDocsView',
        'views/ChatWidgetView',
        'views/CompanyMessagesCollectionView',
        'views/AlertsCollectionView'],
    function (App, $, template, Backbone, Marionette, _, SubHeaderView, WelcomeBioView,WelcomeNewsView,
              WelcomeSocialMediaView,WelcomeESAMediaView,WelcomeDocsView,
              ChatWidgetView, CompanyMessagesCollectionView, AlertsCollectionView
             ) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeader: "#sub-header",
                welcomeBio: "#welcome-bio",
                welcomeNews: "#welcome-news",
                welcomeSocialMedia: "#welcome-social-media",
                welcomeESAMedia: "#welcome-esa-media",
                welcomeDocs: "#welcome-docs",
                welcomeChat: "#welcome-chat",
                welcomeCompanyMessages: "#company-messages",
                welcomeAlerts: "#company-alerts"
            },
            initialize: function () {
                //console.log('WelcomeLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);
                
            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('WelcomeLayoutView:onShow');

                this.userId = App.model.get('userId');
                this.myEmployeeId = App.model.get('myEmployeeId');

                if (this.userId === null) {

                    this.subHeader.reset();
                    this.welcomeBio.reset();
                    this.welcomeNews.reset();
                    this.welcomeSocialMedia.reset();
                    this.welcomeESAMedia.reset();
                    this.welcomeDocs.reset();
                    this.welcomeChat.reset();
                    this.welcomeCompanyMessages.reset();
                    this.welcomeAlerts.reset();

                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    console.log('WelcomeLayoutView:onShow:userId:' + this.userId);
                    if (this.myEmployeeId !== null) {
                        console.log('WelcomeLayoutView:onShow:myEmployeeId:' + this.myEmployeeId);
                        this.resetLayout();
                    }
                }
            },
            resetLayout: function () {
                console.log('WelcomeLayoutView:resetLayout');

                var self = this;

                self.subHeader.reset();

                self.subHeader.show(new SubHeaderView({
                    majorTitle: "Welcome",
                    minorTitle: "",
                    page: "Welcome",
                    showDateFilter: false,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));
                
                self.welcomeBio.show(new WelcomeBioView());
                self.welcomeNews.show(new WelcomeNewsView());
                self.welcomeSocialMedia.show(new WelcomeSocialMediaView());
                self.welcomeESAMedia.show(new WelcomeESAMediaView());
                self.welcomeDocs.show(new WelcomeDocsView());
                self.welcomeChat.show(new ChatWidgetView());
                self.welcomeCompanyMessages.show(new CompanyMessagesCollectionView({category: 0}));
                self.welcomeAlerts.show(new AlertsCollectionView({category: 4}));

            },
            onMyEmployeeIdChanged: function () {

                console.log('WelcomeLayoutView:onMyEmployeeIdChanged');
                this.resetLayout();

            },
            
            transitionIn: function () {
                //console.log('WelcomeLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('WelcomeLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('WelcomeLayoutView:resize');
                var self = this;
            },
            remove: function () {
                console.log('---------- WelcomeLayoutView:remove ----------');

                var random = Math.random();
                App.model.set('welcomePageClosed', random);

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });