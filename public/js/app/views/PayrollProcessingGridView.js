define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/payrollProcessingGrid',
        'jquery.cookie', 'kendo/kendo.all.min', 'kendo/kendo.grid', 'kendo/kendo.data'],  //'kendo.all.min'
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('PayrollProcessingGridView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.dateFilter = App.model.get('selectedDateFilter');
                var startDate = app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter);
                var endDate = app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter);

                this.startDate = moment(startDate,'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-01";
                this.endDate = moment(endDate,'MM/DD/YYYY').clone().format('YYYY-MM-DD'); //"2016-11-15";

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                //console.log('PayrollProcessingGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PayrollProcessingGridView:onShow --------");
                var self = this;

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('PayrollProcessingGridView:getData');

                var app = App;
                var self = this;

                this.dataSource = new kendo.data.DataSource(
                    {
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetTimeCardDetail_HHMM/" + self.startDate + "/" + self.endDate + "/employeeID";
                                    return url;
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {
                            model: {
                                id: "employeeID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    intTimerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    employeeID: {editable: false, type: "number"},
                                    serviceID: {editable: false, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    clientName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: true}},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: [
                            {field: "workDate", dir: "asc"},
                            {field: "employeeID", dir: "asc"},
                            {field: "customerName", dir: "asc"}
                        ],
                        requestStart: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:error');
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), false);
                        }
                    });

                self.dataSource.fetch(function () {
                    console.log('PayrollProcessingGridView:getData:data 1st record:' + JSON.stringify(this.data[0]));

                    kendo.ui.progress(self.$("#payrollProcessingLoading"), true);

                    var data = this.data();
                    var dataCount = data.length;
                    var lastEmployeeID = 0;
                    var lastWorkDate = null;
                    var rptRecord = null;

                    var gridColumns = [{
                        field: "employeeName",
                        title: "Employee",
                        width: 150,
                        footerTemplate: "Daily Totals"
                    }];

                    var fields =  [{employeeName: { type: "string" }}];
                    var aggfields = [];
                    var modifiedRecords = [];

                    if (dataCount > 0) {

                        // Add time up for each employee for each day in query
                        $.each(data, function (index, value) {
                            var record = value;

                            var thisEmployeeID = record.employeeID;
                            var thisWorkDate = moment(new Date(record.workDate)).clone().format('YYYY-MM-DD');
                            var thisWorkDateField = "date_" + moment(new Date(record.workDate)).clone().format('MM_DD');

                            var weekday = moment.weekdaysShort(new Date(record.workDate).getDay());
                            var thisWorkDateTitle = weekday + " " + moment(new Date(record.workDate)).clone().format('M/D');

                            if (thisWorkDate !== lastWorkDate) {
                                if (JSON.stringify(gridColumns).indexOf(thisWorkDateField) === -1) {
                                    fields.push({
                                        thisWorkDateField: { type: "number" }
                                    });
                                    aggfields.push({
                                        field: thisWorkDateField,
                                        aggregate: "sum"
                                    });
                                    gridColumns.push({
                                        field: thisWorkDateField,
                                        title: thisWorkDateTitle,
                                        //aggregate: ["sum"],
                                        //aggregates: ["sum"],
                                        footerTemplate: "#= kendo.toString(sum,'n2') #",
                                        //footerTemplate: "#=App.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sum)#",
                                        //footerTemplate: function (e) {
                                        //    console.log('PayrollProcessingGridView:getData:footerTemplate:e' + JSON.stringify(e));
                                        //    return app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e[index].sum);
                                        //},
                                        format: "{0:n2}",
                                        //type: "number",
                                        width: 50,
                                        template: function (e) {
                                            //console.log('PayrollProcessingGridView:getData:template:e' + e);
                                            var value = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e[thisWorkDateField]);
                                            var template = "<div style='text-align: left'>" + value + "</div>";
                                            if (e[thisWorkDateField] === null || e[thisWorkDateField] === undefined) {
                                                template = "<div style='text-align: left'>0</div>";
                                            }
                                            return template;
                                        }
                                    });
                                }
                                lastWorkDate = thisWorkDate;
                            }

                            if (thisEmployeeID !== lastEmployeeID) {

                                if (rptRecord !== null && rptRecord.added === false) {
                                    rptRecord.added = true;
                                    modifiedRecords.push(rptRecord);
                                }
                                lastEmployeeID = thisEmployeeID;

                                if (thisEmployeeID > lastEmployeeID) {

                                    // New record
                                    rptRecord = {
                                        employeeID: record.employeeID,
                                        employeeName: record.employeeName,
                                        added: false
                                    };

                                } else {

                                    // Find existing record
                                    var employees = $.grep(modifiedRecords, function (element, index) {
                                        return element.employeeID == thisEmployeeID;
                                    });

                                    if (employees.length > 0) {
                                        rptRecord = employees[0];
                                    } else {
                                        // New record
                                        rptRecord = {
                                            employeeID: record.employeeID,
                                            employeeName: record.employeeName,
                                            added: false
                                        };
                                    }
                                }

                                rptRecord[thisWorkDateField] = record.timeWorked;

                            } else {
                                if (rptRecord[thisWorkDateField] === undefined) {
                                    rptRecord[thisWorkDateField] = record.timeWorked;
                                } else {
                                    rptRecord[thisWorkDateField] += record.timeWorked;
                                }
                            }

                            if (index === data.length - 1) {
                                if (rptRecord !== null && rptRecord.added === false) {
                                    rptRecord.added = true;
                                    modifiedRecords.push(rptRecord);
                                }
                            }

                        });

                    }

                    // Add summary to each employee row
                    $.each(modifiedRecords, function (index, value) {
                        var record = value;

                        var array = $.map(record, function(value, index) {
                            return [value];
                        });

                        var sum = 0;
                        $.each(array, function (index, value) {
                            if (index > 2)
                                sum+= parseFloat(value);
                        });

                        record.HoursSum = sum;
                    });

                    fields.push({"HoursSum": { type: "number" }});
                    aggfields.push({field: "HoursSum", aggregate: "sum"});
                    gridColumns.push({
                        field: "HoursSum",
                        title: "Total",
                        footerTemplate: "#= kendo.toString(sum,'n2') #",
                        format: "{0:n2}",
                        width: 50
                    });

                    var aggregates = [];
                    $.each(gridColumns, function (index, value) {
                        if (index > 0) {
                            aggregates.push({field: value.field, aggregate: "sum"});
                            //value.footerTemplate = "Sum: #=sum#";
                        }
                    });

                    var configuration = {
                        //autoBind: false,
                        columnResize: function (e) {
                            //self.resize();
                            window.setTimeout(self.resize, 10);
                        },
                        toolbar: [
                            {
                                name: "excel"
                                //template: function (e) {
                                //    var template = '<a class="k-button k-button-icontext k-grid-excel" href="#"><span class="k-icon k-i-excel"></span>Export to Excel</a>';
                                //    return template;
                                //}
                            }, {
                                template: function (e) {
                                    var template = '<a class="k-button k-button-icontext k-grid-exportToIIF"><span class="k-icon k-i-excel"></span>Export to QuickBooks</a>';
                                    return template;
                                }
                            },{
                                template: function (e) {
                                    var template = '<a class="k-button k-button-icontext k-grid-exportToExcelDecimal"><span class="k-icon k-i-excel"></span>Export to Excel (Decimal)</a>';
                                    return template;
                                }
                            }],
                        excel: {
                            fileName: "PayrollProcessingReport.xlsx",
                            allPages: true
                        },
                        //excelExport: function (e) {
                        //    self.excelExport(e);
                        //},
                        serverAggregates: true,
                        sortable: true,
                        extra: false,
                        resizable: true,
                        reorderable: true,
                        columns: gridColumns,
                        dataSource: {
                            sort: {field: "employeeName", dir: "asc"},
                            data: modifiedRecords,
                            schema: {
                                model:{
                                    fields:fields
                                }
                            },
                            aggregate: aggfields
                        },
                        aggregate: aggregates
                    };

                    self.grid = self.$("#payrollProcessing").kendoGrid(configuration).data("kendoGrid");
                    //self.grid.dataSource.sort({field: "employeeName", dir: "asc"});
                    //self.grid.dataSource.data(modifiedRecords);
                    //self.grid.columns = gridColumns;

                    self.$(".k-grid-exportToIIF").click(function (e) {
                        console.log('PayrollProcessingGridView:exportToIIF:1');
                        self.exportToIIF(e);
                    });

                    self.$(".k-grid-exportToExcelDecimal").click(function (e) {
                        console.log('PayrollProcessingGridView:exportToExcelDecimal:1');
                        self.exportToExcelDecimal(e);
                    });

                    kendo.ui.progress(self.$("#payrollProcessingLoading"), false);

                });

            },
            getDataDecimal: function () {
                console.log('PayrollProcessingGridView:getDataDecimal');

                var app = App;
                var self = this;

                var dataSourceDecimal = new kendo.data.DataSource(
                    {
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetTimeCardDetail/" + self.startDate + "/" + self.endDate + "/employeeID";
                                    return url;
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {
                            model: {
                                id: "employeeID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    intTimerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    employeeID: {editable: false, type: "number"},
                                    serviceID: {editable: false, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    clientName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: true}},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: [
                            {field: "workDate", dir: "asc"},
                            {field: "employeeID", dir: "asc"},
                            {field: "customerName", dir: "asc"}
                        ],
                        requestStart: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:error');
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), false);
                        }
                    });

                dataSourceDecimal.fetch(function ()
                {
                    console.log('PayrollProcessingGridView:getDataDecimal:data 1st record:' + JSON.stringify(this.data[0]));

                    kendo.ui.progress(self.$("#payrollProcessingLoading"), true);

                    var data = this.data();
                    var dataCount = data.length;
                    var lastEmployeeID = 0;
                    var lastWorkDate = null;
                    var rptRecord = null;

                    var gridColumns = [{
                        field: "employeeName",
                        title: "Employee",
                        width: 150,
                        footerTemplate: "Daily Totals"
                    }];

                    var fields =  [{employeeName: { type: "string" }}];
                    var aggfields = [];
                    var modifiedRecords = [];

                    if (dataCount > 0) {

                        // Add time up for each employee for each day in query
                        $.each(data, function (index, value) {
                            var record = value;

                            var thisEmployeeID = record.employeeID;
                            var thisWorkDate = moment(new Date(record.workDate)).clone().format('YYYY-MM-DD');
                            var thisWorkDateField = "date_" + moment(new Date(record.workDate)).clone().format('MM_DD');

                            var weekday = moment.weekdaysShort(new Date(record.workDate).getDay());
                            var thisWorkDateTitle = weekday + " " + moment(new Date(record.workDate)).clone().format('M/D');

                            if (thisWorkDate !== lastWorkDate) {
                                if (JSON.stringify(gridColumns).indexOf(thisWorkDateField) === -1) {
                                    fields.push({
                                        thisWorkDateField: { type: "number" }
                                    });
                                    aggfields.push({
                                        field: thisWorkDateField,
                                        aggregate: "sum"
                                    });
                                    gridColumns.push({
                                        field: thisWorkDateField,
                                        title: thisWorkDateTitle,
                                        //aggregate: ["sum"],
                                        //aggregates: ["sum"],
                                        footerTemplate: "#= kendo.toString(sum,'n2') #",
                                        //footerTemplate: "#=App.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sum)#",
                                        //footerTemplate: function (e) {
                                        //    console.log('PayrollProcessingGridView:getData:footerTemplate:e' + JSON.stringify(e));
                                        //    return app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e[index].sum);
                                        //},
                                        format: "{0:n2}",
                                        //type: "number",
                                        width: 50,
                                        template: function (e) {
                                            //console.log('PayrollProcessingGridView:getData:template:e' + e);
                                            var value = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e[thisWorkDateField]);
                                            var template = "<div style='text-align: left'>" + value + "</div>";
                                            if (e[thisWorkDateField] === null || e[thisWorkDateField] === undefined) {
                                                template = "<div style='text-align: left'>0</div>";
                                            }
                                            return template;
                                        }
                                    });
                                }
                                lastWorkDate = thisWorkDate;
                            }

                            if (thisEmployeeID !== lastEmployeeID) {

                                if (rptRecord !== null && rptRecord.added === false) {
                                    rptRecord.added = true;
                                    modifiedRecords.push(rptRecord);
                                }
                                lastEmployeeID = thisEmployeeID;

                                if (thisEmployeeID > lastEmployeeID) {

                                    // New record
                                    rptRecord = {
                                        employeeID: record.employeeID,
                                        employeeName: record.employeeName,
                                        added: false
                                    };

                                } else {

                                    // Find existing record
                                    var employees = $.grep(modifiedRecords, function (element, index) {
                                        return element.employeeID == thisEmployeeID;
                                    });

                                    if (employees.length > 0) {
                                        rptRecord = employees[0];
                                    } else {
                                        // New record
                                        rptRecord = {
                                            employeeID: record.employeeID,
                                            employeeName: record.employeeName,
                                            added: false
                                        };
                                    }
                                }

                                rptRecord[thisWorkDateField] = record.timeWorked;

                            } else {
                                if (rptRecord[thisWorkDateField] === undefined) {
                                    rptRecord[thisWorkDateField] = record.timeWorked;
                                } else {
                                    rptRecord[thisWorkDateField] += record.timeWorked;
                                }
                            }

                            if (index === data.length - 1) {
                                if (rptRecord !== null && rptRecord.added === false) {
                                    rptRecord.added = true;
                                    modifiedRecords.push(rptRecord);
                                }
                            }

                        });

                    }

                    // Add summary to each employee row
                    $.each(modifiedRecords, function (index, value) {
                        var record = value;

                        var array = $.map(record, function(value, index) {
                            return [value];
                        });

                        var sum = 0;
                        $.each(array, function (index, value) {
                            if (index > 2)
                                sum+= parseFloat(value);
                        });

                        record.HoursSum = sum;
                    });

                    fields.push({"HoursSum": { type: "number" }});
                    aggfields.push({field: "HoursSum", aggregate: "sum"});
                    gridColumns.push({
                        field: "HoursSum",
                        title: "Total",
                        footerTemplate: "#= kendo.toString(sum,'n2') #",
                        format: "{0:n2}",
                        width: 50
                    });

                    var aggregates = [];
                    $.each(gridColumns, function (index, value) {
                        if (index > 0) {
                            aggregates.push({field: value.field, aggregate: "sum"});
                            //value.footerTemplate = "Sum: #=sum#";
                        }
                    });

                    var configuration = {
                        //autoBind: false,
                        columnResize: function (e) {
                            //self.resize();
                            window.setTimeout(self.resize, 10);
                        },
                        //toolbar: [
                        //    {
                        //        name: "excel"
                        //    }
                        //],
                        excel: {
                            fileName: "PayrollProcessingReportDecimal.xlsx",
                            allPages: true
                        },
                        serverAggregates: true,
                        sortable: true,
                        extra: false,
                        resizable: true,
                        reorderable: true,
                        columns: gridColumns,
                        dataSource: {
                            sort: {field: "employeeName", dir: "asc"},
                            data: modifiedRecords,
                            schema: {
                                model:{
                                    fields:fields
                                }
                            },
                            aggregate: aggfields
                        },
                        aggregate: aggregates
                    };

                    var gridDecimal = self.$("#payrollProcessingDecimal").kendoGrid(configuration).data("kendoGrid");
                    gridDecimal.saveAsExcel();
                    kendo.ui.progress(self.$("#payrollProcessingLoading"), false);

                });

            },
            exportToExcelDecimal: function (e) {
                console.log('PayrollProcessingGridView:exportToExcelDecimal:2');
                var self = this;
                self.getDataDecimal();

            },
            exportToIIF: function (e) {
                console.log('PayrollProcessingGridView:exportToIIF:2');
                var self = this;
                var app = App;

                var dataSource = new kendo.data.DataSource(
                    {
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetTimeCardDetail_HHMM/" + self.startDate + "/" + self.endDate + "/employeeName";
                                    return url;
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {
                            model: {
                                id: "employeeID",
                                fields: {
                                    timerID: {editable: false, type: "number"},
                                    intTimerID: {editable: false, type: "number"},
                                    companyID: {editable: false, type: "number"},
                                    clientID: {editable: false, type: "number"},
                                    employeeID: {editable: false, type: "number"},
                                    serviceID: {editable: false, type: "number"},
                                    workDate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    startTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    stopTime: {
                                        editable: true,
                                        type: "date"
                                    },
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    clientName: {editable: true, type: "string", validation: {required: false}},
                                    customerName: {editable: true, type: "string", validation: {required: true}},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    serviceItem: {type: "string", validation: {required: false}},
                                    tasks: {editable: true, type: "number"},
                                    meetings: {editable: true, type: "number"},
                                    timeWorked: {editable: true, type: "number"},
                                    timerNote: {editable: true, type: "string", validation: {required: false}},
                                    mgrNote: {editable: true, type: "string", validation: {required: false}},
                                    timerAction: {editable: true, type: "string", validation: {required: false}},
                                    recordType: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        //serverFiltering: true,
                        //serverSorting: true
                        requestStart: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), false);
                        },
                        error: function (e) {
                            //console.log('PortalTimeCardDetailGridView:dataSource:error');
                            kendo.ui.progress(self.$("#payrollProcessingLoading"), false);
                        }
                    });

                //dataSource.sort(
                //    {field: "workDate", dir: "asc"},
                //    {field: "employeeID", dir: "asc"},
                //    {field: "clientID", dir: "asc"},
                //    {field: "customerName", dir: "asc"},
                //    {field: "serviceItem", dir: "asc"}
                //);
                dataSource.fetch(function () {

                    var a = document.createElement("a");
                    var csvContent =  "data:text/csv;charset=utf-8,";
                    if (window.navigator.msSaveOrOpenBlob) {
                        csvContent = "";
                    }

                    var data = this.data();
                    //var dataCount = data.length;
                    var today = moment().clone().format('MM/DD/YYYY');

                    //!TIMERHDR	VER	REL	COMPANYNAME	IMPORTEDBEFORE	FROMTIMER	COMPANYCREATETIME
                    //TIMERHDR	8	0	EXECUTIVE SCHEDULING ASSOCIATES, INC.	N	N	1510262132
                    //!HDR	PROD	VER	REL	IIFVER	DATE
                    //HDR	QuickBooks Desktop Pro	Version 6.0D	Release R5P	1	2/15/2019
                    //!TIMEACT	DATE	EMP	JOB	ITEM	DURATION	PITEM	NOTE	BILLINGSTATUS

                    //var csvContent = "data:text/csv;charset=utf-8,";
                    csvContent += "!TIMERHDR	VER	REL	COMPANYNAME	IMPORTEDBEFORE	FROMTIMER	COMPANYCREATETIME" + "\n";
                    csvContent += "TIMERHDR	8	0	EXECUTIVE SCHEDULING ASSOCIATES, INC.	N	N	1510262132" + "\n";
                    csvContent += "!HDR	PROD	VER	REL	IIFVER	DATE" + "\n";
                    csvContent += "HDR	QuickBooks Desktop Pro	Version 6.0D	Release R5P	1	" + today + "\n"; //5/5/2017
                    csvContent += "!TIMEACT	DATE	EMP	JOB	ITEM	DURATION	PITEM	NOTE	BILLINGSTATUS" + "\n";

                    var lastEmployeeName = null;
                    var lastCustomerName = null;
                    var lastServiceItem = null;
                    var sumTimeWorked = 0;

                    var row;

                    $.each(data, function (index, value) {

                        var thisEmployeeName = value.employeeName;
                        var thisCustomerName = value.customerName;
                        var thisServiceItem = value.serviceItem;

                        if (index === 0) {
                            lastEmployeeName = value.employeeName;
                            lastCustomerName = value.customerName;
                            lastServiceItem = value.serviceItem;
                        }

                        var hrs;
                        var min;

                        if (thisEmployeeName !== lastEmployeeName || thisCustomerName !== lastCustomerName ||
                            thisServiceItem !== lastServiceItem || index === data.length - 1) {

                            hrs = parseInt(Number(sumTimeWorked), 0);
                            min = Math.round((Number(sumTimeWorked)-hrs) * 60);
                            if(hrs < 10)
                            {
                                hrs = "0" + hrs;
                            }
                            if(min < 10)
                            {
                                min = "0" + min;
                            }

                            row = "TIMEACT";
                            row += String.fromCharCode(9) + moment(value.workDate).clone().format('MM/DD/YYYY');
                            row += String.fromCharCode(9) + lastEmployeeName;
                            row += String.fromCharCode(9) + lastCustomerName;
                            row += String.fromCharCode(9) + lastServiceItem;
                            row += String.fromCharCode(9) + hrs+':'+min;
                            //row += String.fromCharCode(9) + app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTimeWorked);
                            row += String.fromCharCode(9) + "Hourly Wage";
                            row += String.fromCharCode(9) + "no notes"; //?
                            row += String.fromCharCode(9) + "1"; //value.idCount; //?
                            //row += String.fromCharCode(9) + value.workDate + ":" + value.employeeID + ":" + value.clientID;
                            //row += String.fromCharCode(9) + index + " of " + data.length;

                            csvContent += row + "\n";

                            sumTimeWorked = 0;
                        }

                        lastEmployeeName = value.employeeName;
                        lastCustomerName = value.customerName;
                        lastServiceItem = value.serviceItem;
                        sumTimeWorked += value.timeWorked;

                        if (index === data.length - 1) {

                            hrs = parseInt(Number(sumTimeWorked), 0);
                            min = Math.round((Number(sumTimeWorked)-hrs) * 60);

                            if(hrs < 10)
                            {
                                hrs = "0" + hrs;
                            }
                            if(min < 10)
                            {
                                min = "0" + min;
                            }

                            row = "TIMEACT";
                            row += String.fromCharCode(9) + moment(value.workDate).clone().format('MM/DD/YYYY');
                            row += String.fromCharCode(9) + lastEmployeeName;
                            row += String.fromCharCode(9) + lastCustomerName;
                            row += String.fromCharCode(9) + lastServiceItem;
                            row += String.fromCharCode(9) + hrs+':'+min;
                            //row += String.fromCharCode(9) + app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTimeWorked);
                            row += String.fromCharCode(9) + "Hourly Wage";
                            row += String.fromCharCode(9) + "no notes"; //?
                            row += String.fromCharCode(9) + "1"; //value.idCount; //?
                            //row += String.fromCharCode(9) + value.workDate + ":" + value.employeeID + ":" + value.clientID;
                            //row += String.fromCharCode(9) + index + " of " + data.length;

                            csvContent += row;
                        }
                    });

                    if (window.navigator.msSaveOrOpenBlob) {
                        var blob = new Blob([csvContent]);
                        window.navigator.msSaveOrOpenBlob(blob, "PayrollProcessing_" + (new Date()).getTime() + ".iif");
                    } else {
                        var encodedUri = encodeURI(csvContent);
                        a.href = encodedUri;
                        a.download = "PayrollProcessing_" + (new Date()).getTime() + ".iif";
                        document.body.appendChild(a);
                        a.click();
                    }
                });

            },
            //excelExport: function (e) {
            //    console.log('PayrollProcessingGridView:excelExport');
            //},
            displayNoDataOverlay: function () {
                //console.log('PayrollProcessingGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#payrollProcessing').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('PayrollProcessingGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#payrollProcessing').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PayrollProcessingGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#payrollProcessing").parent().parent().height();
                this.$("#payrollProcessing").height(parentHeight);

                parentHeight = parentHeight - 65; //117

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PayrollProcessingGridView:resize:parentHeight:' + parentHeight);
                //console.log('PayrollProcessingGridView:resize:headerHeight:' + headerHeight);
                this.$("#payrollProcessingn").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PayrollProcessingGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        })
            ;
    })
;