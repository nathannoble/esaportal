define(['App', 'jquery', 'hbs!templates/MessagesFullLayout', 'backbone', 'marionette',
        'views/MessagesCollectionView', 'views/SubHeaderView'],
    function (App, $, template, Backbone, Marionette, MessagesCollectionView,SubHeaderView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                messages: '#messages'
            },
            initialize: function () {
                //console.log('MessagesFullLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.userKey = parseInt(App.model.get('userKey'), 0);
                this.userDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalUserTable",
                                complete: function (e) {
                                    console.log('MessagesFullLayoutView:userDataSource:read:complete');
                                 },
                                error: function (e) {
                                    var message = 'MessagesFullLayoutView:userDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('MessageLayoutView:update');
                                    return App.config.DataServiceURL + "/odata/PortalUserTable" + "(" + data.userKey + ")";
                                },
                                complete: function (e) {
                                    console.log('MessagesFullLayoutView:userDataSource:update:complete');
                                 },
                                error: function (e) {
                                    var message = 'MessagesFullLayoutView:userDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "userKey",
                                fields: {
                                    userKey: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    userLogin: {editable: true, type: "string", validation: {required: false}},
                                    lastCompanyMessagesView: {editable: true, type: "date", validation: {required: false}}
                                }
                            }
                        },
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        sort: {field: "userKey", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('MessageLayoutView:dataSource:request start:');
                            //kendo.ui.progress(self.$("#loadingEmployeeInfo"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('MessageLayoutView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#loadingEmployeeInfo"), false);
                        },
                        error: function (e) {
                            //console.log('MessageLayoutView:dataSource:error');
                            //kendo.ui.progress(self.$("#loadingEmployeeInfo"), false);
                        }

                    });

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('MessagesFullLayoutView:onShow');
                var self = this;
                var app = App;

                this.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Company Messages",
                    minorTitle: "",
                    page: "Company Messages",
                    showDateFilter: false,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));

                this.messages.show(new MessagesCollectionView({
                    category: 0,
                    messagesfull:true
                }));

                self.userDataSource.filter({field: "userKey", operator: "eq", value: self.userKey});
                self.userDataSource.fetch(function () {

                    var userData = this.data();

                    if (userData.length > 0) {
                        console.log('MessagesFullLayoutView:onShow:Update lastCompanyMessagesView date and trigger change to that app model value');
                        var offset = (new Date()).getTimezoneOffset();
                        var thisMorning = new Date(moment().clone().subtract((offset), 'minutes'));
                        thisMorning.setHours(0);
                        userData[0].set("lastCompanyMessagesView", thisMorning);

                        $.when(this.sync()).done(function (e) {
                            app.model.set('lastCompanyMessagesView', thisMorning);
                        });
                    } else {
                        console.log('MessagesFullLayoutView:onShow:Update lastCompanyMessagesView date and trigger change to that app model value');
                    }

                });

            },
            transitionIn: function () {
                console.log('MessagesFullLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('MessagesFullLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('MessagesFullLayoutView:resize');
                var self = this;
            },
            remove: function () {
                //console.log('---------- MessagesFullLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });