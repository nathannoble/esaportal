define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/progressClientContactTable',
        'views/ProgressClientContactGridView', 'views/ProgressClientPasswordGridView',
        'views/ProgressClientAssignmentsGridView', 'views/ProgressClientReportGridView',
        'views/PortalTimerTaskBreakdownGridView', 'views/ProgressClientActiveGridView',
        'views/SubHeaderView', 'kendo/kendo.combobox'],
    function (App, Backbone, Marionette, $, template, ProgressClientContactGridView, ProgressClientPasswordGridView,
              ProgressClientAssignmentsGridView, ProgressClientReportGridView, PortalTimerTaskBreakdownGridView, ProgressClientActiveGridView,
              SubHeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                clientRegion: "#client-region"
            },
            initialize: function () {

                //console.log('ProgressClientContactTableView:initialize');

                _.bindAll(this);

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedCompanyIdChanged, this.onSelectedCompanyIdChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedClientReportChanged, this.onSelectedClientReportChanged);

            },
            onRender: function () {

                //console.log('ProgressClientContactTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientContactTableView:onShow --------");

                this.userId = App.model.get('userId');
                this.userType = App.model.get('userType');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    if (this.userType === 2) {
                        this.hideClients = App.model.get('hideClients');
                        if (this.hideClients) {
                            console.log('EmployeeProfileLayoutView:onShow:you do not have permissions to see client reports.');
                            App.model.set('callbackErrorMessage',4);
                            //alert('You do not have permissions to see client reports.');
                            //history.back();
                        }
                    }

                    this.onSelectedClientReportChanged();
                }

            },
            onSelectedDateFilterChanged: function () {
                //console.log('ProgressClientContactTableVie:onSelectedDateFilterChanged');

                this.onSelectedClientReportChanged();
            },
            onSelectedCompanyIdChanged: function () {
                //console.log('ProgressClientContactTableView:onSelectedCompanyIdChangede');

                var option = App.model.get('selectedClientReport');

                if (option === "Task Breakdown Reports") {
                    this.clientRegion.reset();
                    this.clientRegion.show(new PortalTimerTaskBreakdownGridView());
                }
            },
            onSelectedClientReportChanged: function (e) {
                //console.log('ProgressClientContactTableView:onSelectedClientReportChanged');
                var self = this;

                var option = App.model.get('selectedClientReport'); //this.$("#clientOptions").val();

                this.clientRegion.reset();

                if (option === "Task Breakdown Reports") {

                    this.subHeaderRegion.show(new SubHeaderView({
                        majorTitle: "Client Reports",
                        minorTitle: '',
                        page: "Views",
                        showDateFilter: true,
                        showStatusFilter: false,
                        showEmpStatusFilter: false,
                        showClientReportFilter: true,
                        showViewsFilter: false,
                        showCompanyFilter: true,
                        showClientFilter: false,
                        showTeamLeaderFilter:false,
                        showPayrollProcessingDate:false
                    }));
                    this.clientRegion.show(new PortalTimerTaskBreakdownGridView());
                } else {

                    this.subHeaderRegion.show(new SubHeaderView({
                        majorTitle: "Client Reports",
                        minorTitle: '',
                        page: "Views",
                        showDateFilter: true,
                        showStatusFilter: false,
                        showEmpStatusFilter: false,
                        showClientReportFilter: true,
                        showViewsFilter: false,
                        showCompanyFilter: false,
                        showClientFilter: false,
                        showTeamLeaderFilter:false,
                        showPayrollProcessingDate:false
                    }));

                    if (option === "Client Contact Info") {
                        this.clientRegion.show(new ProgressClientContactGridView());
                    } else if (option === "Client Passwords") {
                        this.clientRegion.show(new ProgressClientPasswordGridView());
                    } else if (option === "Client Report") {
                        this.clientRegion.show(new ProgressClientReportGridView({type: 'Standard'}));
                    } else if (option === "Client Report with All") {
                        this.clientRegion.show(new ProgressClientReportGridView({type: 'All'}));
                    } else if (option === "Client Assignments") {
                        this.clientRegion.show(new ProgressClientAssignmentsGridView());
                    } else if (option === "Active Client List") {
                        this.clientRegion.show(new ProgressClientActiveGridView());
                    } else {
                        this.clientRegion.show(new ProgressClientContactGridView());
                    }

                }
            },
            resize: function () {
                //console.log('ProgressClientContactTableView:resize');

                if (this.clientRegion.currentView)
                    this.clientRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------ProgressClientContactTableView:remove --------");

                //this.saveButtonClicked();

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });