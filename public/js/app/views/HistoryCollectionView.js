define(['App', 'backbone', 'marionette', 'jquery', 'models/HistoryModel', 'hbs!templates/historyCollection',
        'views/HistoryView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, HistoryModel, template,
              HistoryView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: HistoryView,
            initialize: function (options) {
                //console.log('HistoryCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.histories = [];

                var self = this;

                this.clientID = parseInt(App.model.get('selectedClientId'),0); //2189;

                this.getData();

            },
            onShow: function () {
                //console.log("-------- HistoryCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;
                    this.historyDataSource = new kendo.data.DataSource(
                        {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ClientSchedulerHistory";
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('HistoryCollectionView:historyDataSource:update');
                                    return App.config.DataServiceURL + "/odata/ClientSchedulerHistory" + "(" + data.schedulerHistoryID + ")";
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('HistoryCollectionView:historyDataSource:create');
                                    return App.config.DataServiceURL + "/odata/ClientSchedulerHistory";
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('HistoryCollectionView:historyDataSource:destroy');
                                    return App.config.DataServiceURL + "/odata/ClientSchedulerHistory" + "(" + data.schedulerHistoryID + ")";
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "schedulerHistoryID",
                                fields: {
                                    schedulerHistoryID: {editable: false, defaultValue: 0, type: "number"},
                                    clientID: {type: "number"},
                                    userKey: {type: "number"},
                                    schedulerChangeTimeStamp: {editable: true, type: "date", validation: {required: true}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    firstName: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: {field: "schedulerHistoryID", dir: "desc"},
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   HistoryCollectionView:historyDataSource:requestStart');

                            kendo.ui.progress(self.$("#loadingHistoryWidget"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    HistoryCollectionView:historyDataSource:requestEnd');
                            kendo.ui.progress(self.$("#loadingHistoryWidget"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   HistoryCollectionView:historyDataSource:error');
                            kendo.ui.progress(self.$("#loadingHistoryWidget"), false);

                            self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        filter: {field: "clientID", operator: "eq", value: self.clientID}

                    });

                    this.historyDataSource.fetch(function () {

                    var data = this.data();
                    var historyCount = data.length;
                    //console.log('HistoryCollectionView:historyDataSource:historyCount:data:' + historyCount + ":" + JSON.stringify(data));

                    $.each(data, function (index, value) {

                        self.histories.push({
                            schedulerHistoryID: value.schedulerHistoryID,
                            id:value.clientID,
                            userKey:value.userKey,
                            schedulerChangeTimeStamp: value.schedulerChangeTimeStamp,
                            lastName: value.lastName,
                            firstName: value.firstName
                        });
                    });

                    // Populate collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return model.get("schedulerHistoryID");
                    };

                    $.each(self.histories, function (index, value) {

                        //console.log('HistoryCollectionView:history:value:' + JSON.stringify(value));
                        var model = new HistoryModel();

                        model.set("schedulerHistoryID", value.schedulerHistoryID);
                        model.set("userKey", value.userKey);
                        model.set("id", value.id);
                        model.set("schedulerChangeTimeStamp", value.schedulerChangeTimeStamp);
                        model.set("lastName", value.lastName);
                        model.set("firstName", value.firstName);
                      
                        self.collection.push(model);
                    });
                });

            },
            hideNoDataOverlay: function () {
                //console.log('HistoryCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#historyWidgetContent').css('display', 'block');
            },
            displayNoDataOverlay: function () {
                //console.log('HistoryCollectionView:displayNoDataOverlay');

                // Hide the widget content
                //this.$('#historyWidgetContent').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {
                //console.log('HistoryCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('HistoryCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- HistoryCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
