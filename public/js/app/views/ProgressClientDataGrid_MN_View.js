define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/ProgressClientDataGrid_MN',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('ProgressClientDataGrid_MN_View:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;

                this.options = options;
                if (this.options === null || this.options === undefined) {
                    this.options = {type: "All Clients"};
                }


                this.userType = parseInt(App.model.get('userType'), 0);
                //this.isExecutive = false;
                //if (self.userType === 3) {
                //    this.isExecutive = true;
                //}
                this.selectedTeamLeaderId = App.model.get('selectedTeamLeaderId');
                this.myTeamLeaderId = parseInt(App.model.get('myTeamLeaderId'), 0);

                if (this.selectedTeamLeaderId === null) {
                    this.selectedTeamLeaderId = this.myTeamLeaderId;
                } else {
                    this.selectedTeamLeaderId = parseInt(App.model.get('selectedTeamLeaderId'), 0);
                }

                //console.log('ProgressClientDataGrid_MN_View:initialize:selectedTeamLeaderId:' + this.selectedTeamLeaderId);

                this.dateFilter = App.model.get('selectedDateFilter');

                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;
                this.startYear = app.Utilities.numberUtilities.getStartYearFromDateFilter(this.dateFilter); //2016;
                //this.month = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter); //11;
                this.startMonth = app.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter);
                this.endMonth = app.Utilities.numberUtilities.getEndMonthFromDateFilter(this.dateFilter);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                var filters = [];

                this.statusFilter = App.model.get('selectedStatusFilter');
                if (this.statusFilter === null || this.statusFilter === undefined || this.options.type === "My Clients") {
                    this.statusFilter = "4 - Active";
                }

                if (this.statusFilter !== "All Status") {
                    filters.push({field: "status", operator: "eq", value: self.statusFilter});
                }

                if (self.selectedTeamLeaderId !== 0 && this.options.type !== "My Clients") {
                    filters.push({field: "teamLeaderID", operator: "eq", value: self.selectedTeamLeaderId});
                }
                this.myClients = false;
                if (this.options.type === "My Clients") {
                    this.myClients = true;
                    this.myEmployeeId = App.model.get('myEmployeeId');
                    if (this.myEmployeeId !== null) {
                        this.myEmployeeId = parseInt(App.model.get('myEmployeeId'), 0);
                        filters.push({field: "schedulerID", operator: "eq", value: this.myEmployeeId});
                    }
                }

                if (!this.myClients) {
                    this.clientDataGridFilter = App.model.get('clientDataGridFilter');
                } else {
                    this.clientDataGridFilter = [];
                }

                // find the right plan that corresponds to this date range
                //console.log('ProgressClientDataGrid_MN_View:initialize:datafilter:' + this.dateFilter);
                this.startDate = new Date(app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter)); //"2016-10-01T00:00:00+0000"
                this.endDate = new Date(app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter)); //"2016-10-31T23:59:59+0000"
                this.endDate = this.endDate.addHours(23);
                this.endDate = this.endDate.addMinutes(59);

                var planDateFilter = {
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: [
                                {field: "planEndDate", operator: "eq", value: null},
                                {field: "planEndDate", operator: "gt", value: this.endDate}
                            ]
                        },
                        {field: "planStartDate", operator: "lt", value: this.endDate}
                    ]
                };


                filters.push(planDateFilter);

                this.progressClientDS = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressClient";
                                },
                                complete: function (e) {
                                    console.log('ProgressClientDataGrid_MN_View:progressClientDS:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientDataGrid_MN_View:progressClientDS:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "clientID",
                                fields: {
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    schedulerID: {editable: false, defaultValue: 0, type: "number"},
                                    schedulerName: {editable: true, type: "string", validation: {required: false}},
                                    companyID: {editable: false, defaultValue: 0, type: "number"},
                                    clientCompany: {editable: true, type: "string", validation: {required: false}},
                                    status: {editable: true, type: "string", validation: {required: false}},
                                    planHours: {type: "number"},
                                    schedulingHours: {type: "number"},
                                    taskTargetLow: {type: "number"},
                                    taskTargetHigh: {type: "number"},
                                    mtgTargetLowDec: {type: "number"},
                                    mtgTargetHigh: {type: "number"},
                                    planStartDate: {editable: true, type: "date", validation: {required: false}},
                                    planEndDate: {editable: true, type: "date", validation: {required: false}},
                                    esaAnniversary: {editable: true, type: "date", validation: {required: false}}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   ProgressClientDataGrid_MN_View:requestStart');

                            kendo.ui.progress(self.$("#clientDataLoading"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    ProgressClientDataGrid_MN_View:requestEnd');
                            //kendo.ui.progress(self.$("#clientDataLoading"), false);

                        },
                        error: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ProgressClientDataGrid_MN_View:error');
                            kendo.ui.progress(self.$("#clientDataLoading"), false);

                            self.displayNoDataOverlay();

                            //App.modal.show(new ErrorView());

                        },
                        change: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ProgressClientDataGrid_MN_View:change');

                            var data = this.data();

                            if (data.length <= 0)
                                self.displayNoDataOverlay();
                            else
                                self.hideNoDataOverlay();
                        },
                        filter: filters
                        //filter: [{field: "status", operator: "eq", value: '4 - Active'}]

                    });

                this.progressClientDataDS = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: function () {
                                    return App.config.DataServiceURL + "/odata/ProgressClientData";
                                },
                                complete: function (e) {
                                    console.log('ProgressClientDataGrid_MN_View:progressClientDataDS:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientDataGrid_MN_View:progressClientDataDS:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "entryID",
                                fields: {
                                    entryID: {editable: false, defaultValue: 0, type: "number"},
                                    clientID: {editable: false, defaultValue: 0, type: "number"},
                                    status: {editable: true, type: "string", validation: {required: false}},
                                    workDate: {editable: true, type: "date", validation: {required: false}},
                                    timeStamp: {editable: true, type: "date", validation: {required: false}},
                                    vYear: {type: "number"},
                                    vMonth: {type: "number"},
                                    sumTotalTime: {type: "number"},
                                    sumTotalTasks: {type: "number"},
                                    sumTotalMeetings: {type: "number"},
                                    sumAdminTime: {type: "number"},
                                    sumCompTime: {type: "number"},
                                    sumCompCalls: {type: "number"},
                                    sumCompMeetings: {type: "number"},
                                    compMeetings: {type: "number"},
                                    sumTotalTimeWithComp: {type: "number"},
                                    sumTotalTasksWithComp: {type: "number"},
                                    sumTotalMeetingsWithComp: {type: "number"}
                                }
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        requestStart: function () {
                            //console.log('   ProgressClientDataGrid_MN_View:requestStart');

                            //kendo.ui.progress(self.$("#clientDataLoading"), true);
                        },
                        requestEnd: function () {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('    ProgressClientDataGrid_MN_View:requestEnd');
                            kendo.ui.progress(self.$("#clientDataLoading"), false);

                        },
                        error: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('   ProgressClientDataGrid_MN_View:error:' + JSON.stringify(e));
                            kendo.ui.progress(self.$("#clientDataLoading"), false);

                        },
                        filter: {
                            logic: "and",
                            filters: [
                                {field: "vYear", operator: "eq", value: self.year},
                                //{field: "vMonth", operator: "eq", value: self.month}
                                {field: "vMonth", operator: "gte", value: self.startMonth},
                                {field: "vMonth", operator: "lte", value: self.endMonth}
                            ]
                        }
                    });

                this.companyDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                complete: function (e) {
                                    console.log('ProgressClientDataGrid_MN_View:companyrDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientDataGrid_MN_View:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyID",
                                fields: {
                                    companyID: {editable: false, type: "number"},
                                    clientCompany: {type: "string"}
                                }
                            }
                        },
                        sort: {field: "clientCompany", dir: "asc"},
                        serverSorting: true,
                        serverFiltering: true,
                        filter: [
                            {field: "clientCompany", operator: "neq", value: "--- NONE ---"}
                            //{field: "actualClients", operator: "gt", value: 0},
                            //{field: "actualClients", operator: "neq", value: null}
                        ]

                    });

                this.planDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressPlan",
                                complete: function (e) {
                                    console.log('ProgressClientDataGrid_MN_View:planDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientDataGrid_MN_View:planDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "planID",
                                fields: {
                                    planID: {editable: false, type: "number"},
                                    planType: {editable: true, type: "string", validation: {required: false}},
                                    planName: {editable: true, type: "string", validation: {required: false}},
                                    description: {editable: true, type: "string", validation: {required: true}},
                                    inactive: {
                                        editable: true,
                                        type: "boolean"
                                    }
                                }
                            }
                        },
                        sort: {field: "planName", dir: "asc"},
                        serverFiltering: true,
                        serverSorting: true,
                        filter: {field: "inactive", operator: "neq", value: true}
                    });


                var schedulerFilter = [
                    {field: "status", operator: "eq", value: '1 - Active'}
                ];
                if (self.selectedTeamLeaderId !== 0) {
                    schedulerFilter = [
                        {field: "status", operator: "eq", value: '1 - Active'},
                        {field: "teamLeaderID", operator: "eq", value: self.selectedTeamLeaderId}
                    ];
                }
                this.schedulerDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressEmployee",
                                complete: function (e) {
                                    console.log('ProgressClientDataGrid_MN_View:schedulerDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientDataGrid_MN_View:schedulerDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "employeeID",
                                fields: {
                                    employeeID: {editable: false, type: "number"},
                                    firstName: {editable: true, type: "string", validation: {required: false}},
                                    lastName: {editable: true, type: "string", validation: {required: false}},
                                    status: {type: "string", validation: {required: false}},
                                    isTeamLeader: {editable: true, type: "boolean"}
                                }
                            }
                        },
                        sort: [
                            {field: "firstName", dir: "asc"},
                            {field: "lastName", dir: "asc"}
                        ],
                        filter: schedulerFilter,
                        serverSorting: true,
                        serverFiltering: true
                    });
            },
            onRender: function () {
                //console.log('ProgressClientDataGrid_MN_View:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientDataGrid_MN_View:onShow --------");

                // Load up the relevant data
                if (!(this.options.type === "My Clients" && this.myEmployeeId === null)) {

                    if (this.year !== this.startYear) {
                        alert("The date filter crosses years (" + this.startYear + "-" + this.year + ").  Only data between the chosen months in the most recent year will displayed.");
                    }
                    this.getData();
                } else {
                    //console.log('ProgressClientDataGrid_MN_View:no data');
                }

            },
            getData: function () {
                //console.log('ProgressClientDataGrid_MN_View:getData');

                var self = this;
                var app = App;

                if (self.startMonth !== null && self.endMonth !== null) {
                    this.progressClientDS.fetch(function (data) {

                        var progressClientRecords = this.data();
                        var clientRecords = [];

                        //if (self.options.type === "My Clients") {
                        //    self.progressClientDataDS.filter().filters[3].filters.push({field: "clientID", operator: "eq", value: value.clientID});
                        //}

                        // Note - because of the way progressClientData returns info (by year/month) the Views will always have the current month's
                        // data regardless of the date range
                        self.progressClientDataDS.fetch(function () {
                            console.log('ProgressClientDataGrid_MN_View:getData:self.progressClientDataDS.fetch');
                            var progressClientDataRecords = this.data();

                            $.each(progressClientRecords, function (index, value) {

                                //var record = value;

                                var sumTotalMeetings = 0;
                                var sumTotalMeetingsWithComp = 0;
                                //var sumMeetingsTarget = 0;
                                //var sumPlanHours = 0;

                                var sumAdminTime = 0;
                                var sumTotalTime = 0;
                                var sumRemainingTime = 0;
                                var sumCompTime = 0;
                                var sumTotalTasks = 0;
                                var sumTotalTasksWithComp = 0;


                                // ProgressClientData records
                                $.each(progressClientDataRecords, function (index2, value2) {
                                    //var record2 = value2;
                                    var clientID = value2.clientID;
                                    if (value.clientID === clientID) {
                                        //console.log('ProgressClientDataGrid_MN_View:clientRecord:' + JSON.stringify(value));
                                        //console.log('ProgressClientDataGrid_MN_View:clientDataRecord:' + JSON.stringify(value2));

                                        sumTotalMeetings += value2.sumTotalMeetingsWithComp; //value2.sumTotalMeetings +
                                        sumTotalMeetingsWithComp += value2.sumTotalMeetingsWithComp;

                                        sumTotalTasks += value2.sumTotalTasksWithComp; //value2.sumTotalTasks +
                                        sumTotalTasksWithComp += value2.sumTotalTasksWithComp;

                                        //recordTasksMtgs = false for everything but 1001, 1008 (Comp), 1011 (Support Sched)
                                        //if (serviceRecord.recordTasksMtgs === false && value2.serviceID !== 1013 && value2.serviceID !== 1001) {
                                        //if (value2.serviceID !== 1013 && value2.serviceID !== 1001 && value2.serviceID !== 1008 && value2.serviceID !== 1011) {
                                        sumAdminTime += value2.sumAdminTime;
                                        sumTotalTime += value2.sumTotalTime ;
                                        sumCompTime += value2.sumCompTime;
                                    }
                                });

                                // If scheduling hours is 0, it's an hourly plan. Planned hours should then be actual hours (NN - 8/29/18)
                                var planHours = value.planHours;
                                if (value.schedulingHours === 0) {
                                    planHours = sumTotalTime; // - sumAdminTime;
                                }

                                var meetingsTarget = value.mtgTargetLowDec * planHours;
                                var taskTarget = value.taskTargetLow * planHours;

                                var meetingsPercent = sumTotalMeetings / meetingsTarget;

                                if (planHours !== 0) {
                                    sumRemainingTime = planHours - sumTotalTime;
                                } else {
                                    sumRemainingTime = 0;
                                }

                                var newRecord = {

                                    // From ProgressClient
                                    clientID: value.clientID,
                                    timeZoneName: value.timeZoneName,
                                    companyID: value.companyID,
                                    clientCompany: value.clientCompany,
                                    status: value.status,
                                    lastName: value.lastName,
                                    firstName: value.firstName,
                                    clientFullName: value.lastName + ", " + value.firstName,
                                    planName: value.planName,
                                    planHours: planHours,
                                    schedulerID: value.schedulerID,
                                    schedulerName: value.schedulerName,
                                    hotList: value.hotList,
                                    teamLeaderID: value.teamLeaderID,

                                    // Calc'd from ProgressClient
                                    tasksTarget: taskTarget, //app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(value.planHours * value.taskTargetLow),
                                    meetingsTarget: meetingsTarget,
                                    sumTotalMeetings: sumTotalMeetings,

                                    // Summed from ProgressClientData
                                    sumAdminTime: sumAdminTime, //app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumAdminTime),
                                    sumTotalTime: sumTotalTime, //app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumTotalTime),
                                    sumRemainingTime: sumRemainingTime, //app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sumRemainingTime),
                                    sumCompTime: sumCompTime,

                                    sumTotalTasks: sumTotalTasks,
                                    sumTotalTasksWithComp: sumTotalTasksWithComp,

                                    sumTotalMeetingsWithComp: sumTotalMeetingsWithComp,
                                    meetingsPercent: meetingsPercent, //app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(meetingsPercent),

                                    //rest of ProgressClientTable info
                                    customerName: value.customerName,
                                    companyClient: value.companyClient,
                                    clientSuccessPlan: value.clientSuccessPlan,
                                    emailAddress: value.emailAddress,
                                    street: value.street,
                                    city: value.city,
                                    state: value.state,
                                    zipCode: value.zipCode,
                                    mobilePhone: value.mobilePhone,
                                    homePhone: value.homePhone,
                                    sbCount: value.sbCount,
                                    clientTimeZoneID: value.clientTimeZoneID,
                                    industry: value.industry,
                                    division: value.division,
                                    title: value.title,
                                    lfdAuthorization: value.lfdAuthorization,
                                    dsuNumber: value.dsuNumber,
                                    product: value.product,
                                    distributionChannel: value.distributionChannel,
                                    states: value.states,
                                    lengthInTerritory: value.lengthInTerritory,
                                    workPhone: value.workPhone,
                                    workExtension: value.workExtension,
                                    assistantInternal: value.assistantInternal,
                                    assistantPhone: value.assistantPhone,
                                    assistantExtension: value.assistantExtension,
                                    assistantEmail: value.assistantEmail,
                                    typeOfService: value.typeOfService,
                                    referredBy: value.referredBy,
                                    planID: value.planID,
                                    adminSchedulerID: value.adminSchedulerID,
                                    taskTargetLow: value.taskTargetLow,
                                    mtgTargetLowDec: value.mtgTargetLowDec,
                                    realTimeAccess: value.realTimeAccess,
                                    realTimeUser: value.realTimeUser,
                                    realTimePassword: value.realTimePassword,
                                    esaAnniversary: value.esaAnniversary,
                                    termDate: value.termDate,
                                    additionalInfo: value.additionalInfo,
                                    sysCrmUserName: value.sysCrmUserName,
                                    sysCrmPassword: value.sysCrmPassword,
                                    sysCrmNotes: value.sysCrmNotes,
                                    sysEmailUserName: value.sysEmailUserName,
                                    sysEmailPassword: value.sysEmailPassword,
                                    sysEmailNotes: value.sysEmailNotes,
                                    sysCalendar: value.sysCalendar,
                                    sysCalendarUserName: value.sysCalendarUserName,
                                    sysCalendarPassword: value.sysCalendarPassword,
                                    sysCalendarNotes: value.sysCalendarNotes

                                };
                                //console.log('ProgressClientDataGrid_MN_View:getData:clientRecord:' + JSON.stringify(newRecord));

                                clientRecords.push(newRecord);
                            });

                            var fields = [];
                            fields.push({"clientID": {type: "number"}});
                            fields.push({"clientCompany": {type: "string"}});
                            fields.push({"clientFullName": {type: "string"}});
                            //fields.push({"firstName": {type: "string"}});
                            fields.push({"planHours": {type: "number"}});

                            var aggfields = [];
                            aggfields.push({field: "planHours", aggregate: "sum"});
                            aggfields.push({field: "clientFullName", aggregate: "count"});

                            var configuration = {

                                selectable: "row",
                                sortable: true,
                                extra: false,
                                resizable: true,
                                reorderable: true,
                                filterable: {
                                    mode: "row"
                                },
                                serverAggregates: true,
                                columnResize: function (e) {
                                    //self.resize();
                                    window.setTimeout(self.resize, 10);
                                },
                                dataBound: function (e) {
                                    //console.log("ProgressClientDataGrid_MN_View:def:onShow:dataBound");
                                    self.$('.client-profile').on('click', self.viewClientProfile);
                                    self.$('.my-stats').on('click', self.viewMyStats);
                                    self.$('.company-profile').on('click', self.viewCompanyProfile);
                                    //self.$('.employee-profile').on('click', self.viewEmployeeProfile);
                                    self.$('.k-grid-create-client').on('click', self.addNewClient);

                                    // save current filter
                                    var filters = null;
                                    if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                                        filters = e.sender.dataSource.filter().filters;

                                        var clientDataGridFilter = [];

                                        $.each(filters, function (index, value) {
                                            clientDataGridFilter.push(value);
                                        });

                                        //console.log("ProgressClientDataGrid_MN_View:def:onShow:dataBound:set clientDataGridFilter:" + JSON.stringify(clientDataGridFilter));
                                        if (!self.myClients) {
                                            app.model.set('clientDataGridFilter', clientDataGridFilter);
                                        }
                                    } else {
                                        if (!self.myClients) {
                                            app.model.set('clientDataGridFilter', []);
                                        }

                                    }

                                },
                                change: function (e) {
                                    //console.log('ProgressClientDataGrid_MN_View:def:onShow:onChange');

                                },
                                save: function (e) {
                                    //console.log('ProgressClientDataGrid_MN_View:def:onShow:onSave');
                                },
                                edit: function (e) {
                                    //console.log('ProgressClientDataGrid_MN_View:def:onShow:onEdit');
                                },
                                dataSource: {
                                    data: clientRecords,
                                    sort: [
                                        {field: "clientCompany", dir: "asc"}
                                        //{field: "lastName", dir: "asc"}
                                    ],
                                    schema: {
                                        model: {
                                            fields: fields
                                        }
                                    }
                                    //aggregate: aggfields
                                }
                                //aggregate: aggfields
                            };

                            configuration.toolbar = [{name: "excel"}];
                            configuration.excel = {
                                fileName: "ManagementView.xlsx",
                                allPages: true
                            };
                            configuration.columns =  [

                                    {
                                        title: "Client Full Name",
                                        field: "clientFullName",
                                        width: 175,
                                        template: function (e) {
                                            var template = "";
                                            if (self.userType > 1) {
                                                template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" + e.clientFullName + "</a></div>";
                                            } else {
                                                template = "<div style='text-align: left'><span>" + e.clientFullName + "</span></div>";
                                            }
                                            return template;

                                        },
                                        filterable: {
                                            cell: {
                                                showOperators: false,
                                                operator: "contains",
                                                suggestionOperator: "contains"
                                            }
                                        },
                                        sortable: {
                                            initialDirection: "desc"
                                        }
                                        //footerTemplate: "Total Count: #= count #"
                                    },
                                    {
                                        title: "Client Company",
                                        field: "clientCompany",
                                        template: function (e) {
                                            var template = "";
                                            if (self.userType > 1) {
                                                template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                            } else {
                                                template = "<div style='text-align: left'><span>" + e.clientCompany + "</span></div>";
                                            }
                                            return template;
                                        },
                                        width: 200,
                                        filterable: {
                                            cell: {
                                                showOperators: false,
                                                template: self.companyFilter
                                            }
                                        },
                                        sortable: true
                                    },
                                    {
                                        field: "planName",
                                        title: "Plan Name",
                                        width: 200,
                                        //filterable: !self.myClients,
                                        filterable: {
                                            cell: {
                                                inputWidth: 150,
                                                showOperators: false,
                                                template: self.planFilter
                                            }
                                        }
                                    },
                                    {
                                        field: "mobilePhone",
                                        title: "Mobile Phone",
                                        filterable: false,
                                        width: 130
                                    },
                                    {
                                        field: "emailAddress",
                                        title: "Email",
                                        width: 250,
                                        filterable: false
                                    },
                                    {
                                        field: "assistantPhone",
                                        title: "Assistant Phone",
                                        width: 130,
                                        filterable: false
                                    }, {
                                        field: "assistantEmail",
                                        title: "Assistant Email",
                                        width: 250,
                                        filterable: false
                                    },
                                    {
                                        field: "esaAnniversary",
                                        title: "ESA Ann",
                                        width: 100,
                                        format: "{0:M/d/yyyy}",
                                        filterable: false
                                    },
                                    {
                                        field: "product",
                                        title: "Product",
                                        width: 200,
                                        filterable: {
                                            cell: {
                                                operator: "contains",
                                                suggestionOperator: "contains",
                                                showOperators: false,
                                                template: self.productFilter
                                            }
                                        }

                                    },
                                    {
                                        field: "states",
                                        title: "States",
                                        width: 75,
                                        filterable: false
                                    },
                                    {
                                        field: "distributionChannel",
                                        title: "Channel",
                                        width: 200,
                                        filterable: {
                                            cell: {
                                                operator: "contains",
                                                suggestionOperator: "contains",
                                                showOperators: false,
                                                template: self.dcFilter
                                            }
                                        }
                                    }


                                ];

                            var grid = self.$("#clientData").kendoGrid(configuration).data("kendoGrid");
                            if (self.clientDataGridFilter !== null && self.clientDataGridFilter !== []) { //} && !self.myClients) {
                                var filters = [];
                                $.each(self.clientDataGridFilter, function (index, value) {
                                    filters.push(value);
                                });

                                //console.log("ProgressClientDataGrid_MN_View:grid:set datasource:filter" + JSON.stringify(filters));
                                grid.dataSource.filter(filters);
                            } else {
                                //grid.dataSource.filter({field: "status", operator: "eq", value: '4 - Active'});
                            }

                            // Resize the grid
                            setTimeout(self.resize, 0);
                        });
                    });
                }

            },
            calculateMeetingPercentage: function (total, target) {

                //console.log('ProgressClientContactGridView:calcMeetingPercentage');

                if (target === 0) {
                    return "N/A";
                } else if (total === 0) {
                    return "0%";
                } else {
                    return App.Utilities.numberUtilities.getFormattedNumberWithRounding(total / target * 100) + "%";
                }

            },
            productFilter: function (container) {
                //console.log('ProgressClientDataGrid_MN_View:productFilter:' + JSON.stringify(container));
                var products = [
                    {value: 'Other'},
                    {value: 'All Channels'},
                    {value: 'Alternative'},
                    {value: 'Annuities (Both)'},
                    {value: 'Annuities (Fixed)'},
                    {value: 'Annuities (Variable)'},
                    {value: 'Business Development'},
                    {value: 'Consulting'},
                    {value: 'Digital Wealth'},
                    {value: 'ETFs'},
                    {value: 'Hedge Fund'},
                    {value: 'Life Insurance'},
                    {value: 'Long Term Care'},
                    {value: 'Mutual Funds'},
                    {value: 'Recruiting'},
                    {value: 'REIT'},
                    {value: 'Retirement / 401K'},
                    {value: 'Reverse Mortgage'},
                    {value: 'RIA'},
                    {value: "SMA's"}
                ];

                container.element.kendoDropDownList({
                    dataValueField: "value",
                    dataTextField: "value",
                    valuePrimitive: true,
                    dataSource:products,
                    optionLabel: "Select Value"
                });

            },
            dcFilter: function (container) {
                //console.log('ProgressClientDataGrid_MN_View:dc:' + JSON.stringify(container));
                var distributionChannels = [
                    {value: 'Banks'},
                    {value: 'B2B'},
                    {value: 'B2C'},
                    {value: 'Independents'},
                    {value: 'Other'},
                    {value: 'Regional'},
                    {value: 'Wirehouses'}
                ];


                container.element.kendoDropDownList({
                    dataValueField: "value",
                    dataTextField: "value",
                    valuePrimitive: true,
                    dataSource:distributionChannels,
                    optionLabel: "Select Value"
                });

            },
            companyFilter: function (container) {
                var self = this;

                //console.log('ProgressClientDataGrid_MN_View:companyFilter:' + JSON.stringify(container));
                self.companyDataSource.fetch(function (data) {

                    var dataSource = [];
                    var records = this.data();

                    $.each(records, function (index, value) {
                        dataSource.push({value: value.clientCompany, text: value.clientCompany});
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: dataSource,
                        optionLabel: "Select Value"
                        //change: self.onCompanyFilterChanged
                    });

                    // Resize the grid
                    //setTimeout(self.resize, 0);
                });

            },
            planFilter: function (container) {
                var self = this;

                //console.log('ProgressClientDataGrid_MN_View:planFilter:' + JSON.stringify(container));
                self.planDataSource.fetch(function (data) {

                    var dataSource = [];
                    var records = this.data();

                    $.each(records, function (index, value) {
                        dataSource.push({value: value.planName, text: value.planName});
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: dataSource,
                        optionLabel: "Select Value"
                    });
                });

            },
            viewCompanyProfile: function (e) {
                //console.log('ProgressClientDataGrid_MN_View:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientDataGrid_MN_View:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId', companyId);


                App.router.navigate('companyProfile', {trigger: true});

            },

            viewClientProfile: function (e) {
                //console.log('ProgressClientDataGrid_MN_View:viewClientProfile:e:' + e.target);

                var clientId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    clientId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    clientId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientDataGrid_MN_View:viewClientProfile:e:' + clientId);
                App.model.set('selectedClientId', clientId);

                App.router.navigate('clientProfile', {trigger: true});

            },

            displayNoDataOverlay: function () {
                //console.log('ProgressClientDataGrid_MN_View:displayNoDataOverlay');

                // Hide the grid
                this.$('#clientData').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressClientDataGrid_MN_View:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#clientData').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressClientDataGrid_MN_View:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#clientData").parent().parent().height();
                //console.log('ProgressClientDataGrid_MN_View:resize:parentHeight:' + parentHeight);

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();

                if (!this.myClients) {
                    this.$("#clientData").height(parentHeight);
                    parentHeight = parentHeight - 75;
                } else {
                    this.$("#clientData").height(parentHeight);
                    parentHeight = parentHeight - 75;
                    if (parentHeight - headerHeight < 49) {
                        parentHeight = headerHeight + 49;
                        this.$("#clientData").height(this.$("#clientData").height() + 7);
                    }
                }

                //console.log('ProgressClientDataGrid_MN_View:resize:parentHeight:' + parentHeight);

                this.$("#clientData").find(".k-grid-content").height(parentHeight - headerHeight);


            },
            remove: function () {
                //console.log("-------- ProgressClientDataGrid_MN_View:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#clientData").data('ui-tooltip'))
                    this.$("#clientData").tooltip('destroy');

                this.clientDataGridFilter = [];

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });