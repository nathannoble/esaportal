define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/scoreBoardDetail','models/ScoreBoardDetailModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            initialize: function () {
                //console.log('ScoreBoardDetailView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('ScoreBoardDetailView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- ScoreBoardDetailView:onShow --------');
                
                this.scoreBoardDetailId = this.model.get('scoreBoardDetailId');
                this.month = parseInt(this.model.get('month'),0);
                this.year = this.model.get('year');
                this.added = this.model.get('added');
                this.lost = this.model.get('lost');
                this.growth = this.model.get('growth');

                var months = ["January","February","March","April","May","June","July","August","September","October","November","December", "Total"];
                this.monthText = months[this.month - 1];


                var today = new Date();
                var todayYear = today.getFullYear();
                var todayMonth = today.getMonth() + 1;

                if (this.month === 13) {
                    this.$('#monthYear').html("<b>" + this.monthText + " " + this.year + "</b>");
                    this.$('#added').html("<b>" + this.added+ "</b>");
                    this.$('#lost').html("<b>" + this.lost+ "</b>");
                    this.$('#growth').html("<b>" + this.growth+ "</b>");
                } else {
                    this.$('#monthYear').html(this.monthText + " " + this.year);
                    this.$('#added').html(this.added);
                    this.$('#lost').html(this.lost);
                    this.$('#growth').html(this.growth);
                }

                //console.log('ScoreBoardDetailView:onShow:this.year:this.month:' + this.year + ":" + this.month);
                //console.log('ScoreBoardDetailView:onShow:todayYear:todayMonth:' + todayYear + ":" + todayMonth);

                if (todayYear <= this.year && todayMonth < this.month){
                    if (this.added === 0) {
                        this.$('#added').html("-");
                    }
                    if (this.lost === 0) {
                        this.$('#lost').html("-");
                    }
                    if (this.growth === 0) {
                        this.$('#growth').html("-");
                    }
                }

           },

            onResize: function () {
//                //console.log('ScoreBoardDetailView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('ScoreBoardDetailView:resize');

            },
            remove: function () {
                //console.log('-------- ScoreBoardDetailView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#scoreBoardDetailRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
