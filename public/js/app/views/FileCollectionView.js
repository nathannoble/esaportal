define(['App', 'backbone', 'marionette', 'jquery', 'models/FileModel', 'hbs!templates/fileCollection',
        'views/FileView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, FileModel, template,
              FileView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: FileView,
            initialize: function (options) {

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                if (this.options === null || this.options === undefined) {
                    this.options = {type: "Client"};
                }

                if (this.options.type === "Client") {
                    this.id = parseInt(App.model.get('selectedClientId'),0); //2189;
                } else if (this.options.type === "Company") {
                    this.id = parseInt(App.model.get('selectedCompanyId'),0); //4;
                } else if (this.options.type === "Employee") {
                    this.id = parseInt(App.model.get('selectedEmployeeId'),0); //100;
                }

                //console.log('FileCollectionView:initialize:type:' + this.options.type);

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.files = [];

                var self = this;

                this.getFiles();

            },
            onShow: function () {
                //console.log("-------- FileCollectionView:onShow --------");

            },
            getFiles: function () {

                var self = this;

                $.getJSON( App.config.DataServiceURL + "/rest/files/GetFileInfo/" + self.options.type + "/" + self.id, function(data) {

                    // console.log('FileCollectionView:getFiles:data' + JSON.stringify(data));
                    // {
                    // "FileName":"2189_enc-test.txt",
                    // "FilePath":"C:\\inetpub\\wwwroot\\esasolutions\\portal\\filesClient",
                    // "FileNameWithPath":"C:\\inetpub\\wwwroot\\esasolutions\\portal\\filesClient\\2189_enc-test.txt",
                    // "FileDate":"2/7/2017",
                    // "FileSize":28.0,
                    // "errorMessage":""
                    // }


                    $.each(data, function (index, value) {

                        self.files.push({
                            fileID:value.FileName,
                            fileNameWithPath:value.FileNameWithPath,
                            fileName: value.FileName,
                            fileDate:value.FileDate,
                            type: self.options.type
                        });
                    });

                    self.files.sort(function(a,b){
                        var c = new Date(a.fileDate);
                        var d = new Date(b.fileDate);
                        return d-c;
                    });

                    // Populate file collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        //return model.get("fileID");
                        return (model.get("fileDate"));
                    };

                    $.each(self.files, function (index, value) {

                        //console.log('FileCollectionView:file:value:' + JSON.stringify(value));
                        var model = new FileModel();

                        model.set("fileDate", value.fileDate);
                        model.set("fileID", value.fileID);
                        model.set("fileName", value.fileName);
                        model.set("fileNameWithPath", value.fileNameWithPath);
                        model.set("type", self.options.type);

                        self.collection.push(model);
                    });

                    //self.collection.sort();
                });
            },
            hideNoDataOverlay: function () {
                //console.log('FileCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#fileWidgetContent').css('display', 'block');
            },
            onResize: function () {
                //console.log('FileCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('FileCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- FileCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
