define( ['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/footer'],
    function(App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            //regions:{
            //    breadcrumbRegion:"#breadcrumb-region"
            //},
            template: template,
            onShow: function(){
                //console.log('FooterView:onShow');

                //this.breadcrumbRegion.show(new BreadcrumbView());
            }
        });
    });