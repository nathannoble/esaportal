define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/welcomeDocs'],
    function (App, Backbone, Marionette, _, $, template ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            initialize: function () {
                //console.log('WelcomeDocsView:initialize');

                _.bindAll(this);

            },

            onRender: function () {
//                //console.log('WelcomeDocsView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- WelcomeDocsView:onShow --------');

            },
            onResize: function () {
//                //console.log('WelcomeDocsView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('WelcomeDocsView:resize');

            },
            remove: function () {
                //console.log('-------- WelcomeDocsView:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
