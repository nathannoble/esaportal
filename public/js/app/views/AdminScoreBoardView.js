define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/adminScoreBoard',
        'jquery.cookie', 'kendo/kendo.data', 'kendo/kendo.maskedtextbox', 'kendo/kendo.numerictextbox'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            events: {
                'click #SaveButton': 'saveButtonClicked'
            },
            initialize: function (options) {

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;

            },
            onRender: function () {
                //console.log('AdminScoreboardView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- AdminScoreboardView:onShow --------");

                //this.listenTo(App.vent, App.Events.ModelEvents.SelectedFieldIdsChanged, this.onSelectedFieldIdsChanged);
                //this.displayNoDataOverlay();

                this.getData();
            },
            getData: function () {

                var self = this;

                this.scoreBoardStatDataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalScoreBoardStat";
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:update');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoardStat" + "(" + data.year + ")";
                            }
                        },
                        create: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:create');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoardStat";
                            }
                            //dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:destroy');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoardStat" + "(" + data.year + ")";
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        //{
                        // "year":2015,
                        // "clients":0,
                        // "target":"10.0",
                        // "ytd":"0.2",
                        // "remainGoal":"10.0"
                        //}
                        model: {
                            id: "year",
                            fields: {
                                year: {editable: false, defaultValue: 0, type: "number"},
                                clients: {type: "number"},
                                ytd: {type: "number"},
                                target: {type: "number"},
                                remainGoal: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   AdminScoreboardView:requestStart');

                        kendo.ui.progress(self.$("#loadingAdmin"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    AdminScoreboardView:requestEnd');
                        kendo.ui.progress(self.$("#loadingAdmin"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   AdminScoreboardView:error');
                        kendo.ui.progress(self.$("#loadingAdmin"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   AdminScoreboardView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: { field: "year", operator: "eq", value: self.year}
                });

                this.scoreBoardDataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalScoreBoard";
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:update');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoard" + "(" + data.year + ")";
                            }
                        },
                        create: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:create');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoard";
                            }
                            //dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                //console.log('AdminScoreBoardView:destroy');
                                return App.config.DataServiceURL + "/odata/PortalScoreBoard" + "(" + data.year + ")";
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "year",
                            fields: {
                                year: {editable: false, defaultValue: 0, type: "number"},
                                month: {type: "number"},
                                added: {type: "number"},
                                lost: {type: "number"},
                                growth: {type: "number"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   AdminScoreboardView:requestStart');

                        kendo.ui.progress(self.$("#loadingAdmin"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    AdminScoreboardView:requestEnd');
                        kendo.ui.progress(self.$("#loadingAdmin"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   AdminScoreboardView:error');
                        kendo.ui.progress(self.$("#loadingAdmin"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   AdminScoreboardView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: { field: "year", operator: "eq", value: self.year},
                    sort: {field: "month", dir: "asc"}
                });

                this.scoreBoardStatDataSource.fetch(function () {

                    var data = this.data();
                    var count = data.length;

                    if (count > 0) {

                        var record2 = self.scoreBoardStatDataSource.at(0);

                        self.$("#target").kendoNumericTextBox({
                            //format: "# \\%"
                        });
                        var target = self.$("#target").data("kendoNumericTextBox");
                        target.value(record2.target);

                        self.$("#ytd").kendoNumericTextBox({
                            //format: "# \\%"
                        });
                        var ytd = self.$("#ytd").data("kendoNumericTextBox");
                        ytd.value(record2.ytd);

                        self.$("#remainGoal").kendoNumericTextBox({
                            //format: "# \\%"
                        });
                        var remainGoal = self.$("#remainGoal").data("kendoNumericTextBox");
                        remainGoal.value(record2.remainGoal);
                    }

                    self.scoreBoardDataSource.fetch(function () {
                        $.each(this.data(), function (index, value) {
                            var record = value;

                            self.$("#added" + (index+1)).kendoNumericTextBox({});
                            var added = self.$("#added" + (index+1)).data("kendoNumericTextBox");
                            added.value(record.added);

                            self.$("#lost" + (index+1)).kendoNumericTextBox({});
                            var lost = self.$("#lost" + (index+1)).data("kendoNumericTextBox");
                            lost.value(record.lost);

                            self.$("#growth" + (index+1)).kendoNumericTextBox({});
                            var growth = self.$("#growth" + (index+1)).data("kendoNumericTextBox");
                            growth.value(record.growth);
                        });
                    });
                });

            },
            saveButtonClicked: function (e) {
                //console.log('AdminScoreboardView:saveButtonClicked');

                var self = this;

                // Set new values for PortalScoreBoard
                $.each(this.scoreBoardDataSource.data(), function (index, value) {
                    var record = value;

                    var added = self.$("#added" + (index+1)).val();
                    var lost = self.$("#lost" + (index+1)).val();
                    var growth = self.$("#growth" + (index+1)).val();

                    // Set record values
                    record.set("added", added);
                    record.set("lost", lost);
                    record.set("growth", growth);

                    //console.log('AdminScoreboardView:dataSource:recordToSave:' + JSON.stringify(record));

                });

                self.scoreBoardDataSource.sync();

                var target = this.$("#target").val();
                var ytd = this.$("#ytd").val();
                var remainGoal = this.$("#remainGoal").val();

                // Set new values for PortalScoreBoardStats
                var record2 = self.scoreBoardStatDataSource.at(0);
                record2.set("target", target);
                record2.set("ytd", ytd);
                record2.set("remainGoal", remainGoal);

                self.scoreBoardStatDataSource.sync();

                App.model.set('widget2Value',null);
                $.cookie('ESA:AppModel:widget2Value', "");

                App.model.set('widget3Value',null);
                $.cookie('ESA:AppModel:widget3Value', "");

                App.model.set('widget4Value',null);
                $.cookie('ESA:AppModel:widget4Value', "");

            },
            displayNoDataOverlay: function () {
                //console.log('AdminScoreboardView:displayNoDataOverlay');

                this.$el.css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No field selected</div></div></div>').appendTo(this.$el.parent());

            },
            hideNoDataOverlay: function () {
                //console.log('AdminScoreboardView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$el.css('display', 'block');
                $('.no-data-overlay-outer').remove();

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('AdminScoreboardView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

            },
            remove: function () {
                //console.log("-------- AdminScoreboardView:remove --------");

                if (this.fieldsTempDataSource !== null && this.fieldsTempDataSource !== undefined) {
                    this.saveValues();
                    //this.saveButtonClicked();
                }

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });