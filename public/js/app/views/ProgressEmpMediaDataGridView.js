define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressEmpMediaDataGrid',
        'jquery.cookie', 'kendo/kendo.all.min', 'kendo/kendo.grid', 'kendo/kendo.data'], //kendo.all.min
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('ProgressEmpMediaDataGridView:initialize');

                _.bindAll(this);

                var app = App;

                this.dateFilter = App.model.get('selectedDateFilter');
                this.startDate = new Date(app.Utilities.numberUtilities.getStartDateFromDateFilter(this.dateFilter)); //"2016-10-01T00:00:00+0000"
                this.endDate = new Date(app.Utilities.numberUtilities.getEndDateFromDateFilter(this.dateFilter)); //"2016-10-31T23:59:59+0000"

                this.filter = [
                    {field: "status", operator: "eq", value: "1 - Active"},
                    {field: "timeStamp", operator: "gte", value: this.startDate},
                    {field: "timeStamp", operator: "lte", value: this.endDate}
                ];

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                //console.log('ProgressEmpMediaDataGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressEmpMediaDataGridView:onShow --------");
                var self = this;

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('ProgressEmpMediaDataGridView:getData');

                var app = App;
                var self = this;

                this.dataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        serverSorting: true,
                        serverFiltering: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ESAMediaUserStat",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "rowID",
                                fields: {
                                    rowID: {editable: false, type: "number"},
                                    userKey: {editable: true, type: "number"},
                                    employeeID: {editable: true, type: "number"},
                                    userLogin: {editable: false, type: "string"},
                                    firstName: {editable: false, type: "string"},
                                    lastName: {editable: false, type: "string"},
                                    portalAdminDataID: {editable: true, type: "number"},
                                    startTime: {editable: true, type: "date"},
                                    stopTime: {editable: true, type: "date"},
                                    totalTime: {editable: true, type: "number"},
                                    title: {editable: true, type: "string", validation: {required: false}},
                                    avFile: {editable: true, type: "string", validation: {required: false}},
                                    timeStamp: {editable: true, type: "date"},
                                    status: {editable: true, type: "string", validation: {required: false}}
                                }
                            }
                        },
                        sort: {field: "userKey", dir: "asc"},
                        requestStart: function (e) {
                            //console.log('AVModalView:dataSource:request start:');
                            kendo.ui.progress(self.$("#esaMediaDataLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('AVModalView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#esaMediaDataLoading"), false);
                        },
                        error: function (e) {
                            console.log('AVModalView:dataSource:error');
                            kendo.ui.progress(self.$("#esaMediaDataLoading"), false);
                        },
                        change: function (e) {

                            // Do nothing if the request has been cancelled
                            if (this.cancelled === true)
                                return;

                            //console.log('AVModalView:change');

                        },
                        filter:self.filter
                    });

                self.dataSource.fetch(function () {
                    console.log('ProgressEmpMediaDataGridView:getData:data 1st record:' + JSON.stringify(this.data()[0]));

                    kendo.ui.progress(self.$("#esaMediaDataLoading"), true);

                    var data = this.data();
                    var dataCount = data.length;
                    var lastUserKey = 0;
                    var lastESAMediaID = null;
                    var rptRecord = null;

                    var gridColumns = [{
                        title: "Employee Name",
                        field: "empName",
                        width: 100,
                        template: function (e) {
                            var template = "<div style='text-align: left'>" + e.empName + "</div>";
                            return template;
                        }
                    }];

                    var fields = [{empName: {type: "string"}}];
                    var aggfields = [];
                    var modifiedRecords = [];

                    if (dataCount > 0) {

                        // Add time up for each employee for each day in query
                        $.each(data, function (index, value) {
                            var record = value;

                            if (record.totalTime === null) {
                                record.totalTime = 0;
                            }
                            var userKey = record.userKey;

                            var thisESAMediaID = record.portalAdminDataID;
                            var thisESAMediaIDField = "ID_" + record.portalAdminDataID;

                            var thisESAMediaStartTimeField = "ListenDateTime_" + record.portalAdminDataID;
                            var thisESAMediaStartTimeTitle = "Date Listened";

                            if (record.title !== null) {
                                var thisESAMediaIDTitle = record.title; //.substring(0, 30);

                                if (thisESAMediaID !== lastESAMediaID) {
                                    if (JSON.stringify(gridColumns).indexOf(thisESAMediaIDField) === -1) {
                                        fields.push({
                                            thisESAMediaIDField: {type: "number"}
                                        });
                                        aggfields.push({
                                            field: thisESAMediaIDField,
                                            aggregate: "average"
                                        });
                                        gridColumns.push({
                                            field: thisESAMediaIDField,
                                            title: thisESAMediaIDTitle,
                                            //footerTemplate: "#= kendo.toString(average,'n2') #",
                                            footerTemplate: function (e) {
                                                var value = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e[thisESAMediaIDField] ? e[thisESAMediaIDField].average: e.average);
                                                var template = "<div style='text-align: left'>" + value + "</div>";
                                                //if (e.average === null || e.average === undefined) {
                                                //    template = "<div style='text-align: left'>0</div>";
                                                //}

                                                var hrs = parseInt(Number(e[thisESAMediaIDField] ? e[thisESAMediaIDField].average: e.average), 0);
                                                var min = Math.round((Number(e[thisESAMediaIDField] ? e[thisESAMediaIDField].average: e.average)-hrs) * 60);

                                                if(hrs < 10)
                                                {
                                                    hrs = "0" + hrs;
                                                }
                                                if(min < 10)
                                                {
                                                    min = "0" + min;
                                                }

                                                var valueHHMM = hrs+':'+min;

                                                //console.log('ProgressEmpMediaDataGridView:getData:template:value:valueHHMM:' + value + ":" + valueHHMM);

                                                var templateHHMM = "Avg: " + valueHHMM; //"<div style='text-align: left'>Avg: " + valueHHMM + "</div>";
                                                if ((e[thisESAMediaIDField] ? e[thisESAMediaIDField].average: e.average) === null || (e[thisESAMediaIDField] ? e[thisESAMediaIDField].average: e.average) === undefined) {
                                                    templateHHMM = "Avg: 00:00"; //"<div style='text-align: left'>Avg: 00:00</div>";
                                                }
                                                return templateHHMM;
                                            },
                                            //format: "{0:n2}",
                                            width: 75,
                                            template: function (e) {
                                                var value = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e[thisESAMediaIDField]);
                                                var template = "<div style='text-align: left'>" + value + "</div>";
                                                if (e[thisESAMediaIDField] === null || e[thisESAMediaIDField] === undefined) {
                                                    template = "<div style='text-align: left'>0</div>";
                                                }

                                                var hrs = parseInt(Number(e[thisESAMediaIDField]), 0);
                                                var min = Math.round((Number(e[thisESAMediaIDField])-hrs) * 60);

                                                if(hrs < 10)
                                                {
                                                    hrs = "0" + hrs;
                                                }
                                                if(min < 10)
                                                {
                                                    min = "0" + min;
                                                }

                                                var valueHHMM = hrs+':'+min;

                                                console.log('ProgressEmpMediaDataGridView:getData:template:value:valueHHMM:' + value + ":" + valueHHMM);

                                                var templateHHMM = "<div style='text-align: left'>" + valueHHMM + "</div>";
                                                if (e[thisESAMediaIDField] === null || e[thisESAMediaIDField] === undefined) {
                                                    templateHHMM = "<div style='text-align: left'>00:00</div>";
                                                }
                                                return templateHHMM;
                                            }
                                        });

                                        fields.push({
                                            thisESAMediaStartTimeField: {type: "date"}
                                        });
                                        gridColumns.push({
                                            field: thisESAMediaStartTimeField,
                                            title: thisESAMediaStartTimeTitle,
                                            format: "{0:M/d/yy h:mm tt}",
                                            width: 75
                                        });
                                    }
                                    lastESAMediaID = thisESAMediaID;
                                }

                                if (userKey !== lastUserKey) {

                                    if (rptRecord !== null && rptRecord.added === false) {
                                        rptRecord.added = true;
                                        modifiedRecords.push(rptRecord);
                                    }
                                    lastUserKey = userKey;

                                    if (userKey > lastUserKey) {

                                        // New record
                                        rptRecord = {
                                            userKey: record.userKey,
                                            employeeID: record.employeeID,
                                            empName: record.lastName + ", " + record.firstName,
                                            lastName: record.lastName,
                                            firstName: record.firstName,
                                            added: false
                                        };

                                    } else {

                                        // Find existing record
                                        var employees = $.grep(modifiedRecords, function (element, index) {
                                            return element.userKey == userKey;
                                        });

                                        if (employees.length > 0) {
                                            rptRecord = employees[0];
                                        } else {
                                            // New record
                                            rptRecord = {
                                                userKey: record.userKey,
                                                employeeID: record.employeeID,
                                                empName: record.lastName + ", " + record.firstName,
                                                lastName: record.lastName,
                                                firstName: record.firstName,
                                                added: false
                                            };
                                        }
                                    }

                                    rptRecord[thisESAMediaIDField] = record.totalTime;
                                    rptRecord[thisESAMediaStartTimeField] = record.startTime;

                                } else {
                                    if (rptRecord[thisESAMediaIDField] === undefined || rptRecord[thisESAMediaIDField] === 0) {
                                        rptRecord[thisESAMediaIDField] = record.totalTime;
                                        rptRecord[thisESAMediaStartTimeField] = record.startTime;
                                    } else {
                                        rptRecord[thisESAMediaIDField] += record.totalTime;
                                        rptRecord[thisESAMediaStartTimeField] = record.startTime;
                                    }
                                }

                                if (index === data.length - 1) {
                                    if (rptRecord !== null && rptRecord.added === false) {
                                        rptRecord.added = true;
                                        modifiedRecords.push(rptRecord);
                                    }
                                }
                            }

                        });

                    }

                    // Add summary to each employee row
                    $.each(modifiedRecords, function (index, value) {
                        var record = value;

                        var array = $.map(record, function (value, index) {
                            return [value];
                        });

                        var sum = 0;
                        //var count = 0;
                        $.each(array, function (index, value) {
                            if ((index > 5) && (index % 2 === 0))
                                sum += parseFloat(value);
                        });

                        record.TimeSum = sum;
                        //record.TimeAvg = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(sum/count);
                    });

                    fields.push({"TimeSum": {type: "number"}});
                    aggfields.push({field: "TimeSum", aggregate: "sum"});
                    gridColumns.push({
                        field: "TimeSum",
                        title: "Total",
                        //footerTemplate: "#= kendo.toString(sum,'n2') #",
                        footerTemplate: function (e){
                            var value = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e.data ? e.data.sum : (e.TimeSum ? e.TimeSum.sum : 0));
                            var template = "<div style='text-align: left'>" + value + "</div>";
                            //if (e.data.sum === null || e.data.sum === undefined) {
                            //    template = "<div style='text-align: left'>0</div>";
                            //}

                            var hrs = parseInt(Number(e.data ? e.data.sum : (e.TimeSum ? e.TimeSum.sum : 0)), 0);
                            var min = Math.round((Number(e.data ? e.data.sum : (e.TimeSum ? e.TimeSum.sum : 0))-hrs) * 60);

                            if (isNaN(hrs) || isNaN(min)) {
                                if (e.data && e.data.TimeSum) {
                                    hrs = parseInt(Number(e.data.TimeSum.sum),0);
                                    min = Math.round((Number((e.data.TimeSum.sum-hrs))) * 60);
                                }
                            }

                            if(hrs < 10)
                            {
                                hrs = "0" + hrs;
                            }
                            if(min < 10)
                            {
                                min = "0" + min;
                            }

                            var valueHHMM = hrs+':'+min;

                            //console.log('ProgressEmpMediaDataGridView:getData:template:value:valueHHMM:' + value + ":" + valueHHMM);

                            if (isNaN(hrs)|| isNaN(min)) {

                                valueHHMM = "00:00";
                            }
                            var templateHHMM = "Total: " + valueHHMM;

                            //var templateHHMM = "<div style='text-align: left'>Total: " + valueHHMM + "</div>";
                            //if (e.TimeSum.sum === null || e.TimeSum.sum === undefined) {
                            //    templateHHMM = "<div style='text-align: left'>Total: 00:00</div>";
                            //}
                            return templateHHMM;
                        } ,
                        //format: "{0:n2}",
                        template: function (e) {
                            var value = app.Utilities.numberUtilities.getFormattedNumberWithTwoPlaceRounding(e.TimeSum);
                            var template = "<div style='text-align: left'>" + value + "</div>";
                            if (e.TimeSum === null || e.TimeSum === undefined) {
                                template = "<div style='text-align: left'>0</div>";
                            }

                            var hrs = parseInt(Number(e.TimeSum), 0);
                            var min = Math.round((Number(e.TimeSum)-hrs) * 60);

                            if(hrs < 10)
                            {
                                hrs = "0" + hrs;
                            }
                            if(min < 10)
                            {
                                min = "0" + min;
                            }

                            var valueHHMM = hrs+':'+min;

                            //console.log('ProgressEmpMediaDataGridView:getData:emplate:value:valueHHMM:' + value + ":" + valueHHMM);

                            var templateHHMM = valueHHMM;//<div style='text-align: left'>" + valueHHMM + "</div>";
                            if (e.TimeSum === null || e.TimeSum === undefined) {
                                templateHHMM = "00:00"; //"<div style='text-align: left'>00:00</div>";
                            }
                            return templateHHMM;
                        },
                        width: 50
                    });

                    //fields.push({"TimeAvg": {type: "number"}});
                    //aggfields.push({field: "TimeAvg", aggregate: "average"});
                    //gridColumns.push({
                    //    field: "TimeAvg",
                    //    title: "Average",
                    //    //footerTemplate: "#= kendo.toString(sum,'n2') #",
                    //    footerTemplate: "#= kendo.toString(average,'n2') #",
                    //    format: "{0:n2}",
                    //    width: 50
                    //});

                    var aggregates = [];
                    $.each(gridColumns, function (index, value) {
                        if (index > 0) {
                            aggregates.push({field: value.field, aggregate: "sum"});
                            //aggregates.push({field: value.field, aggregate: "average"});
                            //value.footerTemplate = "Sum: #=sum#";
                        }
                    });

                    var configuration = {
                        //autoBind: false,
                        columnResize: function (e) {
                            //self.resize();
                            window.setTimeout(self.resize, 10);
                        },
                        toolbar: [{
                                name: "excel"
                            }],
                        excel: {
                            fileName: "EmployeePodcastReport.xlsx",
                            allPages: true
                        },
                        serverAggregates: true,
                        sortable: true,
                        extra: false,
                        resizable: true,
                        reorderable: true,
                        columns: gridColumns,
                        dataSource: {
                            sort: {field: "userKey", dir: "asc"},
                            data: modifiedRecords,
                            schema: {
                                model: {
                                    fields: fields
                                }
                            },
                            aggregate: aggfields
                        },
                        aggregate: aggregates
                    };

                    self.grid = self.$("#esaMediaData").kendoGrid(configuration).data("kendoGrid");

                    //self.grid.thead.kendoTooltip({
                    //    filter: "th",
                    //    content: function (e) {
                    //        console.log('ProgressEmpMediaDataGridView:grid:header:e.target:' + JSON.stringify(e.target));
                    //        var target = e.target;
                    //        if($(target).index() == 1) {
                    //            return ("SomeTooltipText")
                    //        }
                    //        else {return ("AnotherTooltipText")}
                    //    }
                    //});

                    kendo.ui.progress(self.$("#esaMediaDataLoading"), false);

                });

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressEmpMediaDataGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#esaMediaData').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressEmpMediaDataGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#esaMediaData').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressEmpMediaDataGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#esaMediaData").parent().parent().height();
                this.$("#esaMediaData").height(parentHeight);

                parentHeight = parentHeight - 65; //117

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressEmpMediaDataGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressEmpMediaDataGridView:resize:headerHeight:' + headerHeight);
                this.$("#esaMediaDatan").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressEmpMediaDataGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        })
            ;
    })
;