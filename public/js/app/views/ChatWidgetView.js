define(['App', 'jquery', 'hbs!templates/chatWidget', 'backbone', 'marionette',
        'views/Comment21CollectionView'],
    function (App, $, template, Backbone, Marionette, Comment21CollectionView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                comments: '#comment-21-list'
            },
            events: {
                'click #NewCommentButton': 'newCommentButtonClicked'
            },
            initialize: function () {
                //console.log('ChatWidgetView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                var offset = (new Date()).getTimezoneOffset();
                this.now = new Date();
                this.now.setHours(23);

                this.userKey = parseInt(App.model.get('userKey'), 0);  //10268;

                this.messages = [];

                this.fileName = null;

                this.myEmployeeId = App.model.get('myEmployeeId');

                this.getData();
                this.listenTo(App.vent, App.Events.ModelEvents.MessageCommentDeletedIdChanged, this.onMessageCommentDeletedIdChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.MessageCommentAddedIdChanged, this.onMessageCommentAddedIdChanged);

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                console.log('ChatWidgetView:onShow');
                //this.resize();

            },
            onRemove:  function (e) {
                if (e && e.sender) {
                    console.log('ChatWidgetView:onRemove:this.filename:' + this.filename);
                    e.sender.options.async.removeUrl = App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/CommentImages/" + this.fileName + "/";
                    this.fileName = null;
                    console.log('ChatWidgetView:onRemove:removeUrl:' + e.sender.options.async.removeUrl);
                }
            },
            onSuccess:  function (e) {
                console.log('ChatWidgetView:onSuccess:response:' + e.response);//console.log('ChatWidgetView:onSuccess:category:' + this.category);
                //console.log('ChatWidgetView:onSuccess:ID:' + this.model.ID);
                if (e.response.indexOf("Exception") > -1) {
                } else if (e.response.indexOf("Deleted file") > -1) {
                } else {
                    this.fileName = e.response;
                    console.log('ChatWidgetView:onSuccess:fileName changed');
                }

            },
            getData: function () {

                var self = this;

                this.messageCommentDataSource = new kendo.data.DataSource(
                    {
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalMessagesComment";
                            },
                            complete: function (e) {
                                console.log('ChatWidgetView:messageCommentDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'ChatWidgetView:messageCommentDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                //console.log('ChatWidgetView:update');
                                return App.config.DataServiceURL + "/odata/PortalMessagesComment" + "(" + data.commentID + ")";
                            },
                            complete: function (e) {
                                console.log('ChatWidgetView:messageCommentDataSource:update:complete');
                            },
                            error: function (e) {
                                var message = 'ChatWidgetView:messageCommentDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                        },
                        create: {
                            url: function (data) {
                                //console.log('ChatWidgetView:create');
                                return App.config.DataServiceURL + "/odata/PortalMessagesComment";
                            },
                            complete: function (e) {
                                console.log('ChatWidgetView:messageCommentDataSource:create:complete');
                            },
                            error: function (e) {
                                var message = 'ChatWidgetView:messageCommentDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                            //dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                //console.log('ChatWidgetView:destroy');
                                return App.config.DataServiceURL + "/odata/PortalMessagesComment" + "(" + data.commentID + ")";
                            },
                            complete: function (e) {
                                console.log('ChatWidgetView:messageCommentDataSource:destroy:complete');
                            },
                            error: function (e) {
                                var message = 'ChatWidgetView:messageCommentDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "commentID",
                            fields: {
                                commentID: {editable: false, defaultValue: 0, type: "number"},
                                commentText: {editable: true, type: "string", validation: {required: false}},
                                fileName: {editable: true, type: "string", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: true}},
                                messageID: {type: "number"},
                                userKey: {type: "number"}
                            }
                        }
                    },
                    sort: [{field: "messageID", dir: "desc"},{field: "commentID", dir: "asc"}],
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        console.log('   ChatWidgetView:messageCommentDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingChatWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        console.log('    ChatWidgetView:messageCommentDataSource:requestEnd');
                        kendo.ui.progress(self.$("#loadingChatWidget"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        console.log('   ChatWidgetView:messageCommentDataSource:error');
                        kendo.ui.progress(self.$("#loadingChatWidget"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    }
                });

                this.commentDataSource = new kendo.data.DataSource(
                    {
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalComment";
                            },
                            complete: function (e) {
                                console.log('ChatWidgetView:commentDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'ChatWidgetView:commentDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "commentID",
                            fields: {
                                commentID: {editable: false, defaultValue: 0, type: "number"},
                                commentText: {editable: true, type: "string", validation: {required: false}},
                                fileName: {editable: true, type: "string", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: true}},
                                messageID: {type: "number"},
                                userKey: {type: "number"},
                                lastName: {editable: true, type: "string", validation: {required: false}},
                                firstName: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    sort: [{field: "messageID", dir: "desc"},{field: "commentID", dir: "asc"}],
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   ChatWidgetView:commentDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingChatWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    ChatWidgetView:commentDataSource:requestEnd');
                        kendo.ui.progress(self.$("#loadingChatWidget"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ChatWidgetView:commentDataSource:error');
                        kendo.ui.progress(self.$("#loadingChatWidget"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: self.onCommentDataSourceChange
                });

                this.chatDataSource = new kendo.data.DataSource(
                    {
                        autoBind: false,
                        batch: false,
                        pageSize: 10,
                        serverPaging: true,
                        serverSorting: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                complete: function (e) {
                                    console.log('ChatWidgetView:chatDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ChatWidgetView:chatDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('ChatWidgetView:update');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('ChatWidgetView:chatDataSource:updatecomplete');
                                },
                                error: function (e) {
                                    var message = 'ChatWidgetView:chatDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('ChatWidgetView:create');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('ChatWidgetView:chatDataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'ChatWidgetView:chatDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('ChatWidgetView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('ChatWidgetView:chatDataSource:delete:complete');
                                },
                                error: function (e) {
                                    var message = 'ChatWidgetView:chatDataSource:delete:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    text: {editable: true, type: "string"},
                                    shortText: {editable: true, type: "string"},
                                    text2: {editable: true, type: "string"},
                                    title: {editable: true, type: "string"},
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {editable: true, type: "string"},
                                    category: {editable: true, type: "number"},
                                    subCategory: {editable: true, type: "number"},
                                    priority: {editable: true, type: "number"},
                                    showAsCompanyMessage:{editable: true, type: "boolean"},
                                    addUserImage:{editable: true, type: "boolean"},
                                    timeStamp: {editable: true,type: "date"}
                                }
                            }
                        },
                        serverFiltering: true,
                        requestStart: function (e) {
                            //console.log('ChatWidgetView:dataSource:request start:');
                            kendo.ui.progress(self.$("#loadingChatWidget"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('ChatWidgetView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#loadingChatWidget"), false);
                        },
                        change: function (e) {
                            //console.log('ChatWidgetView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('ChatWidgetView:dataSource:error');
                            kendo.ui.progress(self.$("#loadingChatWidget"), false);
                        },
                        sort: {field: "timeStamp", dir: "desc"},
                        filter: [
                            {field: "category", operator: "eq", value: 5},
                            {field: "timeStamp", operator: "lte", value: self.now}
                        ]
                    });

                this.chatDataSource.fetch(function () {

                    var data = this.data();

                    self.state = {};
                    var filters = [];

                    console.log('ChatWidgetView:chatDataSource:fetch:data.length:' + data.length);

                    $.each(data, function (index, value) {

                        self.messages.push({
                            messageID: value.ID,
                            messageDate: value.timeStamp,
                            messageText: value.text,
                            comments:[]
                        });

                        //console.log('ChatWidgetView:chatDataSource:data record:' + JSON.stringify(value));
                        filters.push({field: "messageID", operator: "eq", value: value.ID});

                        //self.$('#text').html(value.text);

                        self.addUserImage = value.addUserImage;
                        self.$('#title').html(value.title);
                        self.$('#text').html(value.text);
                        self.$('#timeStamp').html("Date posted: " + moment(value.timeStamp).clone().format('MM/DD/YYYY'));

                        var url =App.config.PortalFiles + "/CompanyChat/" + value.fileName;
                        $.ajax({
                            url: url,
                            type: "GET",
                            crossDomain: true,
                            success: function (response) {
                                console.log('ChatWidgetView:user image found:done');
                                //self.$('#image').attr('src', "../../" + App.config.PortalName + "/CompanyChat/" + value.fileName );
                                self.$('#image').attr('src', App.config.PortalFiles + "/CompanyChat/" + value.fileName );
                            },
                            error: function (xhr, status) {
                                console.log('ChatWidgetView:user image not found:fail');
                            }
                        });
                        
                        $("#files").kendoUpload({
                            enabled: self.addUserImage,
                            success: self.onSuccess,
                            remove: self.onRemove,
                            async: {
                                //autoUpload: false,
                                withCredentials: false,
                                saveUrl: App.config.DataServiceURL + "/rest/files/AdminUpload/CommentImages",
                                removeUrl: App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/CommentImages"
                            },
                            multiple: false
                        });

                        return false;
                    });

                    self.state.filter = {
                        logic: "or",
                        filters: filters
                    };

                    // Only query for comments if there is at least one message
                    if (data.length > 0) {
                        $("#welcome-chat").show();
                        self.commentDataSource.query(self.state);
                    } else {
                        $("#welcome-chat").hide();
                    }

                    //window.setTimeout(self.resize, 10);
                });

            },
            onCommentDataSourceChange: function () {

                var self = this;

                var data = this.commentDataSource.data();

                var comments = [];

                //console.log('ChatWidgetView:onCommentDataSourceChange:data:' + JSON.stringify(data));

                // comment records
                $.each(data, function (index, value) {
                    comments.push(value);
                });

                if (this.comments) {
                    this.comments.reset();
                    this.comments.show(new Comment21CollectionView({
                        comments: comments,
                        addUserImage: self.addUserImage
                    }));
                    window.setTimeout(this.resize, 10);
                }

            },
            onMessageCommentDeletedIdChanged: function (e) {
                console.log('ChatWidgetView:onMessageCommentDeletedIdChanged:e:' + e);

                this.refreshCommentList();

            },
            onMessageCommentAddedIdChanged: function (e) {
                console.log('ChatWidgetView:onMessageCommentAddedIdChanged:e:' + e);

                this.refreshCommentList();

            },
            refreshCommentList: function (e) {
                console.log('ChatWidgetView:refreshCommentList');

                var self = this;

                self.$('#newCommentText').val("");
                self.fileName = null;

                self.commentDataSource.filter(self.state.filter);
                self.commentDataSource.fetch(function () {
                    var data = this.data();

                    var comments = [];

                    //console.log('ChatWidgetView:onCommentDataSourceChange:data:' + JSON.stringify(data));

                    // comment records
                    $.each(data, function (index, value) {
                        comments.push(value);
                    });

                    if (this.comments) {
                        this.comments.reset();
                        this.comments.show(new Comment21CollectionView({comments: comments}));
                    }

                    $(".k-upload-files.k-reset").find("li").remove();

                    window.setTimeout(self.resize, 10);

                });
            },
            newCommentButtonClicked: function (e) {
                console.log('ChatWidgetView:newCommentButtonClicked');

                this.createComment();

            },
            createComment: function () {

                console.log('ChatWidgetView:createComment');

                var self = this;

                this.commentText = this.$('#newCommentText').val();

                if (self.fileName === "" ) {
                    self.fileName = null;
                }

                if (this.commentText !== "") {

                    //var offset = (new Date()).getTimezoneOffset();
                    var now = new Date(); //moment().clone().subtract((offset), 'minutes'));
                    var today = moment(new Date()).clone().format('YYYY-MM-DD');

                    console.log('ChatWidgetView:addComment:date:' + today + ":comment:" + this.commentText);

                    var commentText = this.commentText.trim();
                    if (commentText.length > 500) {
                        commentText = commentText.substring(0,499) + " ...";
                    }

                    self.messageCommentDataSource.add({
                        //commentID: {editable: false, defaultValue: 0, type: "number"},
                        commentText: commentText,
                        timeStamp: now,
                        messageID: parseInt(self.messages[0].messageID, 0),
                        userKey: parseInt(self.userKey, 0),
                        fileName: self.fileName
                        //lastName: self.lastName,
                        //firstName: self.firstName
                    });

                    $.when(self.messageCommentDataSource.sync()).done(function (e) {

                        console.log('ChatWidgetView:addComment:saved');

                        self.refreshCommentList();
                    });

                }
            },
            transitionIn: function () {
                console.log('ChatWidgetView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                console.log('ChatWidgetView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                console.log('ChatWidgetView:resize');

                var height = $('#chat-col1').height();
                console.log('ChatWidgetView:resize:height of col 1:' + height);
                $('#chat-col2').height(height);
            },
            remove: function () {
                console.log('---------- ChatWidgetView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });