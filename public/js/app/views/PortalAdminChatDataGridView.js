define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalAdminChatDataGrid',
        //'hbs!templates/editPortalAdminChatDataPopup',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data','kendo/kendo.upload'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('PortalAdminChatDataGridView:initialize');

                _.bindAll(this);

                var self = this;
                self.categories = [
                    {text: 'Company Message', value: 0},
                    {text: 'ESA Media', value: 1},
                    {text: 'Industry News', value: 2},
                    {text: 'Social Media', value: 3},
                    {text: 'Alert Message', value: 4},
                    {text: 'Company Chat', value: 5}
                ];

                //self.priorities = [
                //    {text: '1', value: 1},
                //    {text: '2', value: 2},
                //    {text: '3', value: 3},
                //    {text: '4', value: 4},
                //    {text: '5', value: 5}
                //];

                this.adminChatGridFilter = App.model.get('adminChatGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        autoSync: true,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalAdminData",
                                complete: function (e) {
                                    console.log('PortalAdminChatDataGridView:dataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminChatDataGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            },
                            update: {
                                url: function (data) {
                                    //console.log('PortalAdminChatDataGridView:update');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminChatDataGridView:dataSource:update:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminChatDataGridView:dataSource:update:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            },
                            create: {
                                url: function (data) {
                                    //console.log('PortalAdminChatDataGridView:create');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminChatDataGridView:dataSource:create:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminChatDataGridView:dataSource:create:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                                //dataType: "json"
                            },
                            destroy: {
                                url: function (data) {
                                    //console.log('PortalAdminChatDataGridView:destroy');
                                    return App.config.DataServiceURL + "/odata/PortalAdminData" + "(" + data.ID + ")";
                                },
                                complete: function (e) {
                                    console.log('PortalAdminChatDataGridView:dataSource:destroy:complete');
                                },
                                error: function (e) {
                                    var message = 'PortalAdminChatDataGridView:dataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                }
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ID: {editable: false, type: "number"},
                                    title: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Title Here)"
                                    },
                                    text: {
                                        editable: true,
                                        type: "string",
                                        defaultValue:"(Enter Prompt Here)"
                                    },
                                    text2: {editable: true, type: "string"},
                                    shortText: {editable: true, type: "string"},
                                    fileName: {editable: true, type: "string"},
                                    externalLink: {editable: true, type: "string"},
                                    category: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:5
                                    },
                                    subCategory: {editable: true, type: "number"},
                                    priority: {
                                        editable: true,
                                        type: "number",
                                        defaultValue:1
                                    },
                                    showAsCompanyMessage: {editable: true, type: "boolean"},
                                    addUserImage: {editable: true, type: "boolean"},
                                    timeStamp: {
                                        editable: true,
                                        type: "date",
                                        defaultValue: function () {
                                            var offset = (new Date()).getTimezoneOffset();
                                            var now = new Date(moment().clone().subtract((offset), 'minutes'));
                                            return now;
                                        }
                                    }
                                }
                            }
                        },
                        sort: [{field: "timeStamp", dir: "desc"} ],
                        requestStart: function (e) {
                            //console.log('PortalAdminChatDataGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#adminChatDataLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalAdminChatDataGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#adminChatDataLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalAdminChatDataGridView:dataSource:change:');

                            var data = this.data();
                                if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalAdminChatDataGridView:dataSource:error');
                            kendo.ui.progress(self.$("#adminChatDataLoading"), false);
                        }
                    });

            },
            displayNoDataOverlay: function () {
                //console.log('PortalAdminChatDataGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#adminChatData').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onRender: function () {
                //console.log('PortalAdminChatDataGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalAdminChatDataGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#adminChatData").kendoGrid({

                    toolbar: ["create"],
                    editable: true,
                    selectable: "cell",
                    navigatable: true,
                    sortable: true,
                    extra: false,
                    filterable: {
                        mode: "row"
                    },
                    resizable: true,
                    reorderable: true,
                    columns: [
                        {
                            width: 25,
                            template: function (e) {
                                var template = "<div title='Delete' style='text-align: center'><a class='btn-interaction k-grid-delete' data-id='" + e.ID + "'><i class='font-white-brdr fa fa-times' style='color: black'></i></a></div>";
                                return template;
                            },
                            filterable: false,
                            sortable: true
                        },
                        {
                            field: "title",
                            title: "Title",
                            width: 150,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },{
                            field: "text",
                            title: "Prompt",
                            width: 310,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $('<textarea type="text" class="k-input k-textbox" style="height: 100px" ></textarea>');
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoMaskedTextBox();
                            }
                        },
                        {
                            field: "fileName",
                            title: "Image File",
                            template: function (e) {
                                var template;
                                if (e.fileName === "" || e.fileName === null || e.fileName === undefined) {
                                    template = "<div style='text-align: center;cursor:pointer'><a>" + "Upload Image..." + "</a></div>";
                                } else {
                                    var fileName = e.fileName;
                                    if (fileName.length > 30) {
                                        fileName = fileName.substring(0,29) + " ...";
                                    }
                                    template = "<div style='text-align: center;cursor:pointer'><a>" + fileName + "</a></div>";
                                }
                                return template;
                            },
                            width: 300,
                            filterable: false,
                            editor: function(container, e) {
                                var input = $('<input name="fileName" id="fileName" type="file" aria-label="files" style="width: 300px"/>');
                                input.appendTo(container);

                                console.log('PortalAdminChatDataGridView:def:onShow:e:' + JSON.stringify(e));

                                var fileName = e.model.fileName;
                                if (fileName.length > 15) {
                                    fileName = fileName.substring(0,14) + " ...";
                                }
                                var files = [{
                                    name: fileName,
                                    size: 600,
                                    extension: ""
                                }];

                                input.kendoUpload({
                                    success: self.onSuccess,
                                    remove: self.onRemove,
                                    async: {
                                        withCredentials: false,
                                        saveUrl: App.config.DataServiceURL + "/rest/files/AdminUpload/CompanyChat",
                                        removeUrl: App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/CompanyChat"
                                    },
                                    multiple: false,
                                    files: e.model.fileName !== "" ? files : []
                                });
                            }
                        },
                        {
                            field: "addUserImage",
                            title: "Allow User Images?",
                            template: function (e) {
                                if (e.addUserImage === false) {
                                    template = "<div style='text-align: center'>No</div>";
                                    return template;
                                } else {
                                    template = "<div style='text-align: center'>Yes</div>";
                                    return template;
                                }
                            },
                            width: 110,
                            filterable: false,
                            editor: function(container, options) {
                                var input = $("<input/>");
                                input.attr("name", options.field);
                                input.appendTo(container);
                                input.kendoDropDownList({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: [
                                        {text: 'Yes', value: 'true'},
                                        {text: 'No', value: 'false'}
                                    ]
                                });
                            }
                        },
                        {
                            field: "timeStamp",
                            title: "Time Stamp",
                            width: 100,
                            format: "{0:M/d/yyyy}",
                            filterable: false
                        }
                    ],
                    change: function (e) {
                        console.log('PortalAdminChatDataGridView:def:onShow:onChange:e.model:' + JSON.stringify(e.model));
                    },
                    save: function (e) {
                        e.model.dirty = true;
                        e.model.title = e.model.title.trim();
                        e.model.text = e.model.text.trim();
                        e.model.text2 = e.model.text2.trim();
                        e.model.externalLink = e.model.externalLink.trim();
                        e.model.shortText = e.model.shortText.trim();
                        //e.model.timeStamp.setHours(0);
                        console.log('PortalAdminChatDataGridView:def:onShow:onSave:e.model:text:' + e.model.text);
                    },
                    edit: function (e) {
                        console.log('PortalAdminChatDataGridView:onShow:edit');

                        // Disable the ID editor
                        //e.container.find("input[name='ID']").prop("disabled", true);

                        e.model.category = 5;

                        // For new records set priority
                        if (e.model.ID === 0) {
                            //$('#idGroup').hide();
                            //$('.k-window-title').html('Add Company Chat Record');
                            e.model.priority = 1;
                        }
                        self.fileName = e.model.fileName;
                        console.log('PortalAdminChatDataGridView:onShow:edit:fileName:' + self.fileName);

                        self.category = self.categories[e.model.category].text.replace(" ","");
                        console.log('PortalAdminChatDataGridView:onShow:edit:category:' + self.category);

                        self.model = e.model;

                    },
                    dataBound: function (e) {
                        console.log("PortalAdminChatDataGridView:def:onShow:dataBound");

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;
                            if (filters.length === 0) {
                                filters.push({field: "category", operator: "eq", value: 5 });
                            }
                            var adminChatGridFilter = [];

                            $.each(filters, function (index, value) {
                                adminChatGridFilter.push(value);
                            });

                            console.log("PortalAdminChatDataGridView:def:onShow:dataBound:set adminChatGridFilter:" + JSON.stringify(adminChatGridFilter));
                            app.model.set('adminChatGridFilter', adminChatGridFilter);
                        }

                        console.log("PortalAdminChatDataGridView:def:onShow:dataBound:re-sort by date");

                        $('.k-grid-add').unbind("click");
                        $('.k-grid-add').bind("click", function(){
                            console.log("PortalAdminChatDataGridView:def:onShow:dataBound:Add");
                            var grid = self.$("#adminChatData").data("kendoGrid");
                            grid.dataSource.sort({field: "timeStamp", dir: "desc"});
                        });
                    }
                });

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            onRemove:  function (e) {
                if (e && e.sender) {
                    console.log('PortalAdminChatDataGridView:onRemove:');
                    e.sender.options.async.removeUrl = App.config.DataServiceURL + "/rest/files/PostAdminDeleteFile/" + this.category + "/" + this.fileName + "/";
                    this.model.fileName = "";
                    this.model.dirty = true;
                    var grid = $("#adminChatData").data("kendoGrid");
                    grid.saveChanges();
                    console.log('PortalAdminChatDataGridView:onRemove:removeUrl:' + e.sender.options.async.removeUrl);
                }
            },
            onSuccess:  function (e) {
                //if (e && e.response) {
                    console.log('PortalAdminChatDataGridView:onSuccess:response:' + e.response);
                    console.log('PortalAdminChatDataGridView:onSuccess:category:' + this.category);
                    console.log('PortalAdminChatDataGridView:onSuccess:ID:' + this.model.ID);
                    if (e.response.indexOf("Exception") > -1) {
                    } else if (e.response.indexOf("Deleted file") > -1) {
                    } else {
                        this.fileName = e.response;
                        this.model.fileName = e.response;
                        this.model.dirty = true;
                        console.log('PortalAdminChatDataGridView:onSuccess:fileName changed');
                        var grid = $("#adminChatData").data("kendoGrid");
                        grid.saveChanges();
                    }
                    //else {
                    //    createAutoClosingAlert($("#startError"), "<strong>File Error</strong>" + e.response);
                    //}
                    console.log('PortalAdminChatDataGridView:onSuccess:model:' + JSON.stringify(this.model));

                //}
            },
            createAutoClosingAlert:  function (selector, html)  {
                console.log('PortalAdminChatDataGridView:createAutoClosingAlert');

                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");

                window.setTimeout(function () {
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, 50000);
            },
            priorityFilter: function (container) {

                //console.log('PortalAdminChatDataGridView:categoryFilter:' + JSON.stringify(container));

                container.element.kendoDropDownList({
                    dataValueField: "value",
                    dataTextField: "text",
                    valuePrimitive: true,
                    dataSource: this.priorities,
                    optionLabel: "Select Value"
                });

            },
            getData: function () {
                //console.log('PortalAdminChatDataGridView:getData');

                var grid = this.$("#adminChatData").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);

                    if (self.adminChatGridFilter !== null && self.adminChatGridFilter !== [] && self.adminChatGridFilter !== undefined) {
                        var filters = [];
                        if (self.adminChatGridFilter.length === 0) {
                            filters.push({field: "category", operator: "eq", value: 5});
                        }
                        $.each(self.adminChatGridFilter, function (index, value) {
                            filters.push(value);
                        });

                        //console.log("PortalAdminChatDataGridView:grid:set datasource:filter" + JSON.stringify(filters));
                        grid.dataSource.filter(filters);
                    } else {
                        grid.dataSource.filter({field: "category", operator: "eq", value: 5});
                    }
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalAdminChatDataGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#adminChatData").parent().parent().height();
                this.$("#adminChatData").height(parentHeight);

                parentHeight = parentHeight - 38;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                this.$("#adminChatData").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalAdminChatDataGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#adminChatData").data('ui-tooltip'))
                    this.$("#adminChatData").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });