define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/portalCalendarHoliday',
        'views/PortalHolidayGridView', 'views/ProgressWorkingDayGridView', 'views/SubHeaderView'],
    function (App, Backbone, Marionette, $, template, PortalHolidayGridView,
              ProgressWorkingDayGridView, SubHeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                holidayRegion: "#holiday-region",
                workingDayRegion: "#working-day-region",
                subHeaderRegion: "#sub-header"
            },
            initialize: function () {

                console.log('PortalCalendarHolidayView:initialize');

                _.bindAll(this);

                var self = this;
                var app = App;
                App.modal.currentView = null;

                this.dateFilter = App.model.get('selectedDateFilter');

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);


            },
            onRender: function () {

                //console.log('PortalCalendarHolidayView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalCalendarHolidayView:onShow --------");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    this.subHeaderRegion.show(new SubHeaderView({
                        majorTitle: "Calendar and Holidays",
                        minorTitle: "",
                        page: "Calendar and Holidays",
                        showDateFilter: true,
                        showStatusFilter: false,
                        showEmpStatusFilter: false,
                        showClientReportFilter: false,
                        showViewsFilter: false,
                        showCompanyFilter: false,
                        showClientFilter: false,
                        showTeamLeaderFilter: false,
                        showPayrollProcessingDate: false
                    }));
                    this.holidayRegion.show(new PortalHolidayGridView());
                    this.workingDayRegion.show(new ProgressWorkingDayGridView());
                }

            },
            validateData: function () {
                var self = this;

                if (App.modal.currentView === null || App.modal.currentView === undefined) {

                }
            },
            saveButtonClicked: function (e) {
                //console.log('PortalCalendarHolidayView:saveButtonClicked');

                var self = this;

            },
            onSelectedDateFilterChanged: function () {
                console.log('PortalCalendarHolidayView:onSelectedDateFilterChanged');

                this.dateFilter = App.model.get('selectedDateFilter');

                if (this.dateFilter !== null && this.dateFilter !== undefined) {
                    this.newYear = App.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter);

                    // These only change if year changed
                    if (this.newYear !== this.previousYear) {
                        console.log('PortalCalendarHolidayView:onSelectedDateFilterChanged:Year Changed');

                        this.holidayRegion.reset();
                        this.workingDayRegion.reset();

                        this.holidayRegion.show(new PortalHolidayGridView());
                        this.workingDayRegion.show(new ProgressWorkingDayGridView());
                    }
                }

                this.previousYear = this.newYear;

            },
            resize: function () {
                //console.log('PortalCalendarHolidayView:resize');

                if (this.holidayRegion.currentView)
                    this.holidayRegion.currentView.resize();

                if (this.workingDayRegion.currentView)
                    this.workingDayRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PortalCalendarHolidayView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });