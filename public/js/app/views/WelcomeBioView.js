define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/welcomeBio','views/BioModalView'],
    function (App, Backbone, Marionette, _, $, template, BioModalView ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #readMore': 'readMoreClicked'
            },
            initialize: function () {
                //console.log('WelcomeBioView:initialize');

                _.bindAll(this);

                this.bios = [];

            },
            getData: function () {

                var self = this;

                this.adminDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalAdminData";
                            },
                            complete: function (e) {
                                console.log('WelcomeBioView:adminDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'WelcomeBioView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {editable: false, type: "number"},
                                text: {editable: true, type: "string"},
                                shortText: {editable: true, type: "string"},
                                text2: {editable: true, type: "string"},
                                title: {editable: true, type: "string"},
                                fileName: {editable: true, type: "string"},
                                externalLink: {editable: true, type: "string"},
                                category: {editable: true, type: "number"},
                                subCategory: {editable: true, type: "number"},
                                priority: {editable: true, type: "number"},
                                showAsCompanyMessage:{editable: true, type: "boolean"},
                                addUserImage:{editable: true, type: "boolean"},
                                timeStamp: {editable: true,type: "date"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   WelcomeBioView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    WelcomeBioView:messageDataSource:requestEnd');
                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   WelcomeBioView:messageDataSource:error');
                        kendo.ui.progress(self.$("#welcomeBioViewLoading"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   WelcomeBioView:messageDataSource:change');

                        var data = this.data();
                        
                    },
                    
                    filter: [
                        {field: "showAsCompanyMessage", operator: "eq", value:true},
                        {field: "category", operator: "eq", value: 8}
                    ]
                });

                this.adminDataSource.fetch(function () {

                    var data = this.data();
                    $.each(data, function (index, value) {

                        self.bios.push({
                            ID:value.ID,
                            fileName:value.fileName,
                            title:value.title,
                            shortText:value.shortText,
                            text:value.text,
                            text2:value.text2,
                            subCategory: value.subCategory
                        });

                    });

                    //self.bios.sort(function(a,b){
                    //    var c = a.priority;
                    //    var d = b.priority;
                    //    return d-c;
                    //});

                    //console.log('WelcomeBioView:getData:bios:' + JSON.stringify(self.bios));

                    if (self.bios.length > 0) {
                        self.ID = self.bios[0].ID;
                        self.fileName = self.bios[0].fileName;
                        self.title = self.bios[0].title.trim();
                        self.shortText = self.bios[0].shortText.trim();
                        self.text = self.bios[0].text.trim();
                        var funfact = self.bios[0].text2.trim();
                        //if (funfact.length > 75) {
                        //    funfact = funfact.substring(0,74) + " ...";
                        //}
                        self.text2 = funfact;
                        self.subCategory = self.bios[0].subCategory;

                        self.$('#name').html("Meet " + self.shortText);
                        self.$('#title').html(self.title);
                        self.$('#text2').html('<a id="readMore" href="#" data-toggle="modal" data-target="#modal-default">Read More</a>');
                        //self.$('#bioImage').attr('src', "../../" + App.config.PortalName + "/EmployeeBios/" + self.fileName );
                        self.$('#bioImage').attr('src', App.config.PortalFiles + "/EmployeeBios/" + self.fileName);
                    }
                });
            },

            onRender: function () {
//                //console.log('WelcomeBioView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- WelcomeBioView:onShow --------');

                this.getData();

            },
            readMoreClicked: function (e) {

                console.log('WelcomeBioView:readMoreClicked');

                var options = {
                    ID: this.ID,
                    fileName: this.fileName,
                    title: this.title,
                    shortText:this.shortText,
                    text:this.text,
                    text2:this.text2,
                    subCategory:this.subCategory
                };

                App.modal.show(new BioModalView(options));

            },
            onResize: function () {
//                //console.log('WelcomeBioView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            createAutoClosingAlert: function (selector, html, msDuration) {
                // show the alert
                $(selector).html(html);
                $(selector).css("opacity", 1);
                $(selector).css("display", "block");
                var alert = $(selector).addClass("alert");
                var dz = $("#dragDropHandler");

                //this.isUploadingBio = false;
                //App.model.set('uploadingSpreadCSV', false);

                //wait five seconds and close the alert
                window.setTimeout(function () {
                    dz.slideDown(500);
                    alert.fadeTo(500, 0).slideUp(500, function () {
                        alert.html("");
                        alert.removeClass("alert");
                    });
                }, msDuration);
            },
            resize: function () {
//                //console.log('WelcomeBioView:resize');

            },
            remove: function () {
                //console.log('-------- WelcomeBioView:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
