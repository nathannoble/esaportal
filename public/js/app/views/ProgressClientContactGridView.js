define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/progressClientContactGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('ProgressClientContactGridView:initialize');

                _.bindAll(this);

                var self = this;
                //var app = App;

                this.clientContactGridFilter = App.model.get('clientContactGridFilter');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource({
                    //autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/ProgressClientOrig";
                            },
                            complete: function (e) {
                                console.log('ProgressClientContactGridView:dataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'ProgressClientContactGridView:dataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "clientID",
                            fields: {
                                clientID: {editable: false, defaultValue: 0, type: "number"},
                                lastName: {editable: true, type: "string", validation: {required: false}},
                                firstName: {editable: true, type: "string", validation: {required: false}},
                                companyID: {editable: false, defaultValue: 0, type: "number"},
                                clientCompany: {editable: true, type: "string", validation: {required: false}},
                                mobilePhone: {editable: true, type: "string", validation: {required: false}},
                                emailAddress: {editable: true, type: "string", validation: {required: false}},
                                assistantInternal: {editable: true, type: "string", validation: {required: false}},
                                assistantPhone: {editable: true, type: "string", validation: {required: false}},
                                assistantEmail: {editable: true, type: "string", validation: {required: false}},
                                esaAnniversary: {editable: true, type: "date", validation: {required: true}}
                            }
                        }
                    },
                    //serverFiltering: true,
                    //serverSorting: true,
                    requestStart: function () {
                        //console.log('   ProgressClientContactGridView:requestStart');

                        kendo.ui.progress(self.$("#clientLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    ProgressClientContactGridView:requestEnd');
                        kendo.ui.progress(self.$("#clientLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientContactGridView:error');
                        kendo.ui.progress(self.$("#clientLoading"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   ProgressClientContactGridView:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    //filter: [{field: "status", operator: "eq", value: '4 - Active'}],
                    sort: [
                        {field: "clientCompany", dir: "asc"}, 
                        {field: "lastName", dir: "asc"},
                        {field: "firstName", dir: "asc"}
                    ]
                });

                this.companyDataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/ProgressCompany",
                                complete: function (e) {
                                    console.log('ProgressClientContactGridView:companyDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'ProgressClientContactGridView:companyDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "companyID",
                                fields: {
                                    companyID: {editable: false, type: "number"},
                                    clientCompany: {type: "string"}
                                }
                            }
                        },
                        sort: {field: "clientCompany", dir: "asc"},
                        serverSorting: true,
                        serverFiltering: true,
                        filter: [
                            {field: "clientCompany", operator: "neq", value: "--- NONE ---"}
                            //{field: "actualClients", operator: "gt", value: 0},
                            //{field: "actualClients", operator: "neq", value: null}
                        ]

                    });

            },
            onRender: function () {
                //console.log('ProgressClientContactGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientContactGridView:onShow --------");
                var self = this;
                var app = App;

                this.grid = this.$("#client").kendoGrid({
                    sortable: true,
                    extra: false,
                    resizable: true,
                    reorderable: true,
                    toolbar: [
                        {
                            name: "excel"
                        }],
                    excel: {
                        fileName: "ClientContacts.xlsx",
                        allPages: true
                    },
                    filterable: {
                        mode: "row"
                    },
                    columns: [
                        {
                            title: "Client Name",
                            field: "clientName",
                            width: 175,
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='client-profile btn-interaction' data-id='" + e.clientID + "' href='#clientProfile'>" + e.lastName + ", " + e.firstName + "</a></div>";
                                return template;
                            },
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            },
                            sortable: true
                            //locked: true
                        },
                        {
                            title: "Client Company",
                            field: "clientCompany",
                            template: function (e) {
                                var template = "<div style='text-align: left'><a class='company-profile btn-interaction' data-id='" + e.companyID + "' href='#companyProfile'>" + e.clientCompany + "</a></div>";
                                return template;
                            },
                            width: 200,
                            filterable: {
                                cell: {
                                    showOperators: false,
                                    template: self.companyFilter
                                }
                            },
                            sortable: true
                        },
                        {
                            field: "planName",
                            title: "Plan Name",
                            width: 250,
                            filterable: false
                        },
                        {
                            field: "mobilePhone",
                            title: "Mobile Phone",
                            filterable: false,
                            width: 130
                        },
                        {
                            field: "emailAddress",
                            title: "Email",
                            width: 250,
                            filterable: false
                        }, {
                            field: "assistantInternal",
                            title: "Assistant Internal",
                            width: 150,
                            filterable: false
                        },
                        {
                            field: "assistantPhone",
                            title: "Assistant Phone",
                            width: 130,
                            filterable: false
                        }, {
                            field: "assistantEmail",
                            title: "Assistant Email",
                            width: 250,
                            filterable: false
                        },
                        {
                            field: "esaAnniversary",
                            title: "ESA Ann",
                            width: 100,
                            format: "{0:M/d/yyyy}",
                            filterable: false
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("ProgressClientContactGridView:def:onShow:dataBound");
                        self.$('.client-profile').on('click', self.viewClientProfile);
                        self.$('.company-profile').on('click', self.viewCompanyProfile);

                        // save current filter
                        var filters = null;
                        if (e.sender.dataSource.filter() !== undefined && e.sender.dataSource.filter() !== null && e.sender.dataSource.filter() !== []) {
                            filters = e.sender.dataSource.filter().filters;

                            var clientContactGridFilter = [];

                            $.each(filters, function (index, value) {
                                clientContactGridFilter.push(value);
                            });

                            //console.log("ProgressClientContactGridView:def:onShow:dataBound:set clientContactGridFilter:" + JSON.stringify(clientContactGridFilter));
                            app.model.set('clientContactGridFilter', clientContactGridFilter);
                        }


                    },
                    change: function (e) {
                        //console.log('ProgressClientContactGridView:def:onShow:onChange');

                    },
                    save: function (e) {
                        //console.log('ProgressClientContactGridView:def:onShow:onSave');
                    },
                    edit: function (e) {
                        //console.log('ProgressClientContactGridView:def:onShow:onEdit');

                        // Disable the employeeID editor
                        e.container.find("input[name='clientID']").prop("disabled", true);


                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                //console.log('ProgressClientContactGridView:getData');

                var grid = this.$("#client").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);
                    if (self.clientContactGridFilter !== null && self.clientContactGridFilter !== []) {
                        var filters = [];
                        $.each(self.clientContactGridFilter, function (index, value) {
                            filters.push(value);
                        });

                        //console.log("ProgressClientContactGridView:grid:set datasource:filter" + JSON.stringify(filters));
                        grid.dataSource.filter(filters);
                    } else {
                        grid.dataSource.filter({field: "status", operator: "eq", value: '4 - Active'});
                    }

                    self.resize();

                });

            },
            companyFilter: function (container) {
                var self = this;

                //console.log('ProgressClientContactGridView:companyFilter:' + JSON.stringify(container));
                self.companyDataSource.fetch(function (data) {

                    var dataSource = [];
                    var records = this.data();

                    $.each(records, function (index, value) {
                        dataSource.push({value: value.clientCompany, text: value.clientCompany});
                    });

                    container.element.kendoDropDownList({
                        //autoBind:false,
                        dataValueField: "value",
                        dataTextField: "text",
                        valuePrimitive: true,
                        dataSource: dataSource,
                        optionLabel: "Select Value"
                        //change: self.onCompanyFilterChanged
                    });
                });
            },
            viewCompanyProfile: function (e) {
                //console.log('ProgressClientContactGridView:viewCompanyProfile:e:' + e.target);

                var companyId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    companyId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (e.target.attributes[2]) {
                    companyId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientContactGridView:viewCompanyProfile:e:' + companyId);
                App.model.set('selectedCompanyId', companyId);


                App.router.navigate('companyProfile', {trigger: true});

            },
            viewClientProfile: function (e) {
                //console.log('ProgressClientContactGridView:viewClientProfile:e:' + e.target);

                var clientId = 0;
                if (!isNaN(e.currentTarget.attributes[1].value)) {
                    clientId = parseInt(e.currentTarget.attributes[1].value, 0);
                } else if (!isNaN(e.target.attributes[2].value)) {
                    clientId = parseInt(e.target.attributes[2].value, 0);
                }

                //console.log('ProgressClientContactGridView:viewClientProfile:e:' + clientId);
                App.model.set('selectedClientId', clientId);


                App.router.navigate('clientProfile', {trigger: true});

            },
            displayNoDataOverlay: function () {
                //console.log('ProgressClientContactGridView:displayNoDataOverlay');

                // Hide the grid
                this.$('#client').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('ProgressClientContactGridView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('#client').css('display', 'block');
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('ProgressClientContactGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#client").parent().parent().height();
                this.$("#client").height(parentHeight);

                parentHeight = parentHeight - 57;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('ProgressClientContactGridView:resize:parentHeight:' + parentHeight);
                //console.log('ProgressClientContactGridView:resize:headerHeight:' + headerHeight);
                this.$("#client").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- ProgressClientContactGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#client").data('ui-tooltip'))
                    this.$("#client").tooltip('destroy');

                this.clientContactGridFilter = [];

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });