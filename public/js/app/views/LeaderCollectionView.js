define(['App', 'backbone', 'marionette', 'jquery', 'models/LeaderModel', 'hbs!templates/leaderCollection',
        'views/LeaderView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, LeaderModel, template,
              LeaderView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: LeaderView,
            initialize: function (options) {
                //console.log('LeaderCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.leaders = [];

                var self = this;

                var today = new Date();
                this.year = today.getFullYear();
                this.month = today.getMonth() + 1;
                this.day = today.getDate();

                this.getData();

            },
            onShow: function () {
                //console.log("-------- LeaderCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                //Top 5 Month query

                //<cfquery name="getTop5_Month" datasource="#APPLICATION.dataSource#" maxrows="20">
                //    select top (20) employeeID, employeeName, avg(mtgsPerHour) AS mtgsPerHour
                //from vPortalTop5_Month_2
                //where workYear = #this.year# and workMonth = #this.month#
                //<!---and mtgTargetLowDec >= 3--->
                //<!---and employeeID NOT IN (#employeeBlackList#)--->
                //and status LIKE '1%'
                //group by employeeID, employeeName
                //order by mtgsPerHour DESC
                //</cfquery>
                //
                //<!---This query of queries eliminates the divide by zero error encountered on some dates--->
                //<cfquery name="getTop5_Month_2" dbtype="query">
                //    SELECT employeeID, employeeName, mtgsPerHour
                //FROM		getTop5_Month
                //<!---WHERE mtgsPerHour >= #this.minMPH#--->
                //</cfquery>

                //Top 5 Day Query

                //<cfquery name="getTop5_Day" datasource="#APPLICATION.dataSource#">
                //    select top (5) employeeName, avg(mtgsPerHour) AS mtgsPerHour
                //from vPortalTop5_Day_2
                //where workYear = #this.year# and workMonth = #this.month# and workDay = #this.day#
                //<!---and mtgTargetLowDec >= 3--->
                //<!---and employeeID NOT IN (#employeeBlackList#)--->
                //and status LIKE '1%'
                //group by employeeName
                //order by mtgsPerHour DESC
                //</cfquery>
                //
                //<!---This query of queries eliminates the divide by zero error encountered on some dates--->
                //<cfquery name="getTop5_Day_2" dbtype="query">
                //    SELECT employeeName, mtgsPerHour
                //FROM		getTop5_Day
                //<!---WHERE mtgsPerHour >= #this.minMPH#--->
                //</cfquery>

                if (this.options.type === "This Month") {

                    this.restDataSource = new kendo.data.DataSource({
                        autoBind:false,
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetTop5Month/" + self.year + "/" + self.month;
                                    return url;
                                },
                                complete: function (e) {
                                    console.log('LeaderCollectionView:restDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'LeaderCollectionView:restDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {
                            model: {
                                id: "employeeID",
                                fields: {

                                    employeeID: {editable: false, type: "number"},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    mtgsPerHour: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: [{field: "mtgsPerHour", dir: "desc"}],
                        requestStart: function (e) {
                            //console.log('LeaderCollectionView:dataSource:request start:');
                            kendo.ui.progress(self.$("#leaderCollectionLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('LeaderCollectionView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#openTimerLoading"), false);
                        },
                        change: function (e) {
                            //console.log('LeaderCollectionView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('LeaderCollectionView:dataSource:error');
                            kendo.ui.progress(self.$("#leaderCollectionLoading"), false);
                        }
                    });

                } else {

                    this.restDataSource = new kendo.data.DataSource({
                        autoBind:false,
                        transport: {
                            type: "odata",
                            read: {
                                url: function () {
                                    var url = App.config.DataServiceURL + "/rest/GetTop5Day/" + self.year + "/" + self.month + "/" + self.day;
                                    return url;
                                },
                                complete: function (e) {
                                    console.log('LeaderCollectionView:restDataSource:read:complete');
                                },
                                error: function (e) {
                                    var message = 'LeaderCollectionView:restDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                    console.log(message);
                                },
                                type: "GET",
                                dataType: "json"
                            }
                        },
                        schema: {

                            model: {
                                id: "employeeID",
                                fields: {

                                    employeeID: {editable: false, type: "number"},
                                    employeeName: {editable: true, type: "string", validation: {required: false}},
                                    mtgsPerHour: {editable: true, type: "number"}
                                }
                            }
                        },
                        sort: [{field: "mtgsPerHour", dir: "desc"}],
                        requestStart: function (e) {
                            //console.log('LeaderCollectionView:dataSource:request start:');
                            kendo.ui.progress(self.$("#leaderCollectionLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('LeaderCollectionView:dataSource:request end:');
                            //var data = this.data();
                            //kendo.ui.progress(self.$("#openTimerLoading"), false);
                        },
                        change: function (e) {
                            //console.log('LeaderCollectionView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('LeaderCollectionView:dataSource:error');
                            kendo.ui.progress(self.$("#leaderCollectionLoading"), false);
                        }
                    });
                }

                this.restDataSource.fetch(function () {

                    var data = this.data();
                    var count = data.length;

                    $.each(data, function (index, value) {

                        self.leaders.push({
                            employeeID: value.employeeID,
                            employeeName: value.employeeName,
                            mtgsPerHour: value.mtgsPerHour
                        });

                        var leaderCount = self.leaders.length;

                        if (leaderCount === 5) {
                            return false;
                        }
                    });

                    // Populate leader collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return model.get("employeeID");
                    };

                    $.each(self.leaders, function (index, value) {

                        //console.log('LeaderCollectionView:note:value:' + JSON.stringify(value));
                        var model = new LeaderModel();

                        model.set("employeeID", value.employeeID);
                        model.set("employeeName", value.employeeName);
                        model.set("mtgsPerHour", value.mtgsPerHour);

                        self.collection.push(model);
                    });

                });

            },
            hideNoDataOverlay: function () {
                //console.log('LeaderCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#phoneWidgetContent').css('display', 'block');
            },
            displayNoDataOverlay: function () {
                //console.log('LeaderCollectionView:displayNoDataOverlay');

                // Hide the widget content
                //this.$('#phoneWidgetContent').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer" style="padding-top:0px !important"><div class="no-data-overlay-inner"><div class="no-data-overlay-message"  style="height: 120px;">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {
                //console.log('LeaderCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('LeaderCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- LeaderCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
