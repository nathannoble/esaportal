define(['App', 'backbone', 'marionette', 'jquery',
    'hbs!templates/progressClientDataTable',
    'views/ProgressClientDataGridView',
    'views/ProgressClientDataGrid_MA_View',
    'views/ProgressClientDataGrid_SA_View',
    'views/ProgressClientDataGrid_MK_View',
    'views/ProgressClientDataGrid_MN_View',
    'views/ProgressClientDataGrid_OA_View',
    'views/ProgressClientDataGrid_ALL_View',
    'views/ProgressClientDataGrid_EOM_View',
    'views/SubHeaderView'],
    function (App, Backbone, Marionette, $, template,
              ProgressClientDataGridView,
              ProgressClientDataGrid_MA_View,
              ProgressClientDataGrid_SA_View,
              ProgressClientDataGrid_MK_View,
              ProgressClientDataGrid_MN_View,
              ProgressClientDataGrid_OA_View,
              ProgressClientDataGrid_ALL_View,
              ProgressClientDataGrid_EOM_View,
              SubHeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                clientDataRegion: "#client-data-region"
            },
            initialize: function () {

                //console.log('ProgressClientDataTableView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                App.modal.currentView = null;

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedStatusFilterChanged, this.onSelectedStatusFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedTeamLeaderIdChanged, this.onSelectedTeamLeaderIdChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedViewChanged, this.onSelectedViewChanged);

            },
            onRender: function () {

                //console.log('ProgressClientDataTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressClientDataTableView:onShow --------");

                this.userId = App.model.get('userId');
                //this.userType = App.model.get('userType');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.onSelectedViewChanged();
                }

            },
            onSelectedTeamLeaderIdChanged: function () {
                console.log('ProgressClientDataTableView:onSelectedTeamLeaderIdChanged');

                this.clientDataRegion.reset();

                var option = App.model.get('selectedView');
                if (option === "Default") {
                    this.clientDataRegion.show(new ProgressClientDataGridView());
                } else if (option === "Client Mailing Address") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_MA_View());
                } else if (option === "Sales") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_SA_View());
                } else if (option === "Marketing") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_MK_View());
                } else if (option === "EOM Report") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_EOM_View());
                } else if (option === "Management") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_MN_View());
                } else if (option === "Onboarding") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_OA_View());
                } else if (option === "All Client Fields") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_ALL_View());
                } else {
                    this.clientDataRegion.show(new ProgressClientDataGridView());
                }
            },
            onSelectedStatusFilterChanged: function () {
                console.log('ProgressClientDataTableView:onSelectedStatusFilterChanged');

                this.onSelectedTeamLeaderIdChanged();

            },
            onSelectedDateFilterChanged: function () {
                console.log('ProgressClientDataTableView:onSelectedDateFilterChanged');

                this.onSelectedTeamLeaderIdChanged();
            },
            onSelectedViewChanged: function (e) {
                console.log('ProgressClientDataTableView:onSelectedViewChanged');

                this.clientDataRegion.reset();

                var option = App.model.get('selectedView');

                this.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Views",
                    minorTitle: '', //Client data
                    page: "Views", //option,
                    showDateFilter: true,
                    showStatusFilter: true,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: true,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: true,
                    showPayrollProcessingDate: false
                }));

                if (option === "Default") {
                    this.clientDataRegion.show(new ProgressClientDataGridView());
                } else if (option === "Client Mailing Address") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_MA_View());
                } else if (option === "Sales") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_SA_View());
                } else if (option === "Marketing") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_MK_View());
                } else if (option === "EOM Report") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_EOM_View());
                } else if (option === "Management") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_MN_View());
                } else if (option === "Onboarding") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_OA_View());
                } else if (option === "All Client Fields") {
                    this.clientDataRegion.show(new ProgressClientDataGrid_ALL_View());
                } else {
                    this.clientDataRegion.show(new ProgressClientDataGridView());
                }
            },

            resize: function () {
                //console.log('ProgressClientDataTableView:resize');

                if (this.clientDataRegion.currentView)
                    this.clientDataRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------ProgressClientDataTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });