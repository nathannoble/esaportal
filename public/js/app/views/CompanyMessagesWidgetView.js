define(['App', 'jquery', 'hbs!templates/companyMessagesWidget', 'backbone', 'marionette',
        'views/CompanyMessagesCollectionView'],
    function (App, $, template, Backbone, Marionette, CompanyMessagesCollectionView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                companyMessages: '#company-messages'
            },
            initialize: function () {
                //console.log('CompanyMessagesWidgetView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('CompanyMessagesWidgetView:onShow');

                this.companyMessages.show(new CompanyMessagesCollectionView({category: 0}));

            },
            transitionIn: function () {
                console.log('CompanyMessagesWidgetView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('CompanyMessagesWidgetView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('CompanyMessagesWidgetView:resize');
                var self = this;
            },
            remove: function () {
                //console.log('---------- CompanyMessagesWidgetView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });