define(['App', 'jquery', 'hbs!templates/companyProfileLayout', 'backbone', 'marionette', 'underscore', 'options/ViewOptions',
        'views/ContactInfoView','views/FileWidgetView','views/SummaryStatsView','views/NoteWidgetView'],
    function (App, $, template, Backbone, Marionette, _, ViewOptions, ContactInfoView, FileWidgetView, SummaryStatsView, NoteWidgetView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template:template,
            regions:{
                tile1: "#tile-company-contact-info",
                tile2: "#tile-company-files",
                tile4: "#tile-company-notes"

            },
            initialize: function(){
                //console.log('CompanyProfileLayoutView:initalize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);
            },
            onRender: function(){
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){
                //console.log('CompanyProfileLayoutView:onShow');

                //this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);

                this.userId = App.model.get('userId');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    this.resetView();
                }

            },
            resetView: function () {
                //console.log('CompanyProfileLayoutView:resetDashboard');

                this.tile1.reset();
                this.tile2.reset();
                this.tile4.reset();

                this.tile1.show(new ContactInfoView({type: 'Company'}));
                this.tile2.show(new FileWidgetView({type: 'Company'}));
                this.tile4.show(new NoteWidgetView({type: 'Company'}));

            },
            onSelectedDateFilterChanged: function () {
                this.resetView();
            },
            transitionIn: function(){
                //console.log('CompanyProfileLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function(){
                //console.log('CompanyProfileLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
                //console.log('CompanyProfileLayoutView:resize');
            },
            remove: function(){
                //console.log('---------- CompanyProfileLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });