define(['App', 'jquery', 'hbs!templates/esaTileLayout', 'backbone', 'marionette', 'underscore', 'options/ViewOptions',
        'views/Widget1View', 'views/Widget2View', 'views/Widget3View', 'views/Widget4View', 'views/BarChartView',
        'views/DashboardSummaryWidgetView', 'views/CalloutWidgetView',
        //'views/MessageWidgetView',
        'views/TimerWidgetView', 'views/ProgressClientDataGridView', 'views/PhoneDirectoryWidgetView',
        'views/LeaderBoardWidgetView', 'views/SubHeaderView', 'views/ScoreboardDetailsView'],
    function (App, $, template, Backbone, Marionette, _, ViewOptions,
              Widget1View, Widget2View, Widget3View, Widget4View, BarChartView,
              DashboardSummaryWidgetView, CalloutWidgetView,
              //MessageWidgetView,
              TimerWidgetView, ProgressClientDataGridView, PhoneDirectoryWidgetView,
              LeaderBoardWidgetView, SubHeaderView, ScoreboardDetailsView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #OpenDetails': 'onOpenDetailsClicked',
                'click #viewNotificationsButtonCollapse': 'onViewNotificationsButtonCollapseClicked',
                'click #viewNotificationsButtonRemove': 'onViewNotificationsButtonRemoveClicked',
                'click #viewCompanyStatsButtonCollapse': 'onViewCompanyStatsButtonCollapseClicked',
                'click #viewTeamPersonalStatsButtonCollapse': 'onViewTeamPersonalStatsButtonCollapseClicked',
                // 'click #viewTeamMeetingPercButtonCollapse': 'onViewTeamMeetingPercButtonCollapseClicked',
                'click #viewMyClientsButtonCollapse': 'onViewMyClientsButtonCollapseClicked',
                'click #toggleCompTime': 'onToggleCompTimeClicked'
            },
            regions: {
                subHeaderRegion: "#sub-header",
                timerAlertRegion: "#timer-alert",
                alertTips: "#alert-tips",
                //alertValue: "#alert-value",
                //alertMessage: "#alert-message",
                //alertOpenTimer: "#alert-open-timer",
                tile1: "#tile-1",
                tile2: "#tile-2",
                tile3: "#tile-3",
                tile4: "#tile-4",
                tile5: "#tile-5",
                tile6: "#tile-6",
                //tile7: "#tile-7",
                tile8: "#tile-8",
                tile9: "#tile-9",
                tile10: "#tile-10",
                tile11: "#tile-11",
                tile12: "#tile-12",
                tile13: "#tile-13"
            },
            initialize: function () {
                //console.log('TileLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                // set date range
                var start = moment().startOf('month');
                var end = moment().endOf('month');
                this.dateFilter = start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY');
                App.model.set('selectedDateFilter', this.dateFilter);

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.ShowCompTimeChanged, this.onShowCompTimeChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.TimerStatusChanged, this.onTimerStatusChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedStatusFilterChanged, this.onSelectedStatusFilterChanged);

                this.showCompTime = App.model.get('showCompTime');
                this.tileViewNotifications = App.model.get('tileViewNotifications');
                this.tileViewCompanyStats = App.model.get('tileViewCompanyStats');
                this.tileViewTeamPersonalStats = App.model.get('tileViewTeamPersonalStats');
                this.tileViewTeamMeetingPerc = App.model.get('tileViewTeamMeetingPerc');
                this.tileViewMyClients = App.model.get('tileViewMyClients');

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('TileLayoutView:onShow');

                this.userId = App.model.get('userId');
                this.myEmployeeId = App.model.get('myEmployeeId');

                if (this.userId === null) {

                    this.subHeaderRegion.reset();

                    this.alertTips.reset();
                    //this.alertValue.reset();
                    //this.alertMessage.reset();
                    //this.alertOpenTimer.reset();

                    this.tile1.reset();
                    // this.tile2.reset();
                    // this.tile3.reset();
                    // this.tile4.reset();
                    //this.tile5.reset();
                    this.tile6.reset();
                    //this.tile7.reset();
                    //this.tile8.reset();
                    this.tile9.reset();
                    this.tile10.reset();
                    this.tile11.reset();
                    this.tile12.reset();
                    this.tile13.reset();

                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    console.log('ESATileLayoutView:onShow:userId:' + this.userId);
                    if (this.myEmployeeId !== null) {
                        console.log('ESATileLayoutView:onShow:myEmployeeId:' + this.myEmployeeId);
                        this.resetLayout();
                    }
                }
            },
            resetLayout: function () {
                console.log('ESATileLayoutView:resetLayout');

                var self = this;

                self.subHeaderRegion.reset();

                self.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Dashboard",
                    minorTitle: "", //'Control Panel',
                    page: "Dashboard",
                    showDateFilter: true,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));

                // View Notifications tile
                if (this.tileViewNotifications === null) {
                    this.alertTips.reset();
                    //this.alertValue.reset();
                    //this.alertMessage.reset();
                    //this.alertOpenTimer.reset();

                    this.alertTips.show(new CalloutWidgetView({type: 'Tips'}));
                    //this.alertValue.show(new CalloutWidgetView({type: 'Value'}));
                    //this.alertMessage.show(new CalloutWidgetView({type: 'Message'}));
                    //this.alertOpenTimer.show(new CalloutWidgetView({type: 'Open Timer'}));
                } else if (this.tileViewNotifications === "collapsed") {
                    // Change to "+"
                    self.$('#viewNotificationsIconCollapse').removeClass('fa-minus');
                    self.$('#viewNotificationsIconCollapse').addClass('fa-plus');
                    self.$('.view-notifications-box').addClass('collapsed-box');
                } else {
                    // removed
                    this.$('#viewNotificationsTile').remove();
                }

                //this.tile7.reset();
                this.tile10.reset();
                this.tile11.reset();

                // Company Stats tile
                if (this.tileViewCompanyStats === null) {
                    this.tile1.reset();
                    // this.tile2.reset();
                    // this.tile3.reset();
                    // this.tile4.reset();

                    this.tile1.show(new Widget1View());
                    // this.tile2.show(new Widget2View());
                    // this.tile3.show(new Widget3View());
                    // this.tile4.show(new Widget4View());
                } else {
                    // Change to "+"
                    self.$('#viewCompanyStatsIconCollapse').removeClass('fa-minus');
                    self.$('#viewCompanyStatsIconCollapse').addClass('fa-plus');
                    self.$('.view-company-stats-box').addClass('collapsed-box');
                }

                // Team Personal Stats tile
                if (this.tileViewTeamPersonalStats === null) {

                    this.tile6.reset();
                    //this.tile8.reset();
                    this.tile12.reset();
                    this.tile13.reset();

                    this.tile6.show(new DashboardSummaryWidgetView({type: 'My Stats'}));
                    //this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));
                    this.tile12.show(new LeaderBoardWidgetView({type: 'Today'}));
                    this.tile13.show(new LeaderBoardWidgetView({type: 'This Month'}));
                } else {
                    // Change to "+"
                    self.$('#viewTeamPersonalStatsIconCollapse').removeClass('fa-minus');
                    self.$('#viewTeamPersonalStatsIconCollapse').addClass('fa-plus');
                    self.$('.view-team-personal-stats-box').addClass('collapsed-box');
                }

                // Team Meeting Percentage tile
                // if (this.tileViewTeamMeetingPerc === null) {
                //     this.tile5.reset();
                //     this.tile5.show(new BarChartView({type: 'Team Meetings'}));
                // } else {
                //     // Change to "+"
                //     self.$('#viewTeamMeetingPercIconCollapse').removeClass('fa-minus');
                //     self.$('#viewTeamMeetingPercIconCollapse').addClass('fa-plus');
                //     self.$('.view-team-meeting-perc-box').addClass('collapsed-box');
                // }

                // Message Board tile
                //this.tile7.show(new MessageWidgetView());

                // Phone Directory tile
                this.tile11.show(new PhoneDirectoryWidgetView());

                // My Clients tile
                if (this.tileViewMyClients === null) {
                    this.tile9.reset();
                    this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));
                } else {
                    // Change to "+"
                    self.$('#viewMyClientsIconCollapse').removeClass('fa-minus');
                    self.$('#viewMyClientsIconCollapse').addClass('fa-plus');
                    self.$('.view-my-clients-box').addClass('collapsed-box');
                }

                // Timer Widget
                this.tile10.show(new TimerWidgetView());


            },
            onShowCompTimeChanged: function () {
                console.log('ESATileLayoutView:onShowCompTimeChanged');

                //this.tile6.reset();
                //this.tile8.reset();
                //this.tile5.reset();

                //this.tile5.show(new BarChartView({type: 'Team Meetings'}));
                //this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));
            },
            onToggleCompTimeClicked: function (e) {
                console.log('ESATileLayoutView:onToggleCompTimeClicked:' + e);

                var app = App;

                if (e.target.checked === true) {
                    $('.toggleCheckBox').prop('checked', true);
                    $('.toggle').prop('title', "Show Comp Time");
                    app.model.set('showCompTime',true);
                } else {
                    $('.toggleCheckBox').prop('checked', false);
                    $('.toggle').prop('title', "Don't Show Comp Time");
                    app.model.set('showCompTime',false);
                }
            },
            onViewNotificationsButtonCollapseClicked: function (e) {
                //console.log('ESATileLayoutView:onViewNotificationsButtonCollapseClicked');

                var self = this;
                var tileViewNotifications = null;

                if (e.target.classList.toString().indexOf('plus') > -1 || e.target.innerHTML.indexOf('plus') > -1) {

                    // Change to "-"
                    //self.$('#viewNotificationsIconCollapse').removeClass('fa-plus');
                    //self.$('#viewNotificationsIconCollapse').addClass('fa-minus');
                    //self.$('.view-notifications-box').removeClass('collapsed-box');
                    this.alertTips.reset();
                    //this.alertValue.reset();
                    //this.alertMessage.reset();
                    //this.alertOpenTimer.reset();

                    this.alertTips.show(new CalloutWidgetView({type: 'Tips'}));
                    //this.alertValue.show(new CalloutWidgetView({type: 'Value'}));
                    //this.alertMessage.show(new CalloutWidgetView({type: 'Message'}));
                    //this.alertOpenTimer.show(new CalloutWidgetView({type: 'Open Timer'}));

                    // expanded
                    App.model.set('tileViewNotifications', tileViewNotifications);
                } else {

                    // Change to "+"
                    //self.$('#viewNotificationsIconCollapse').removeClass('fa-minus');
                    //self.$('#viewNotificationsIconCollapse').addClass('fa-plus');
                    //self.$('.view-notifications-box').addClass('collapsed-box');

                    // collapsed
                    tileViewNotifications = "collapsed";
                    App.model.set('tileViewNotifications', tileViewNotifications);
                }

                this.tileViewNotifications = tileViewNotifications;
            },
            onViewNotificationsButtonRemoveClicked: function (e) {
                console.log('ESATileLayoutView:onViewNotificationsButtonRemoveClicked');

                App.model.set('tileViewNotifications', "removed");
                this.tileViewNotifications = App.model.set('tileViewNotifications');

            },
            onViewCompanyStatsButtonCollapseClicked: function (e) {
                //console.log('ESATileLayoutView:onViewCompanyStatsButtonCollapseClicked');
                var self = this;
                var tileViewCompanyStats = null;
                if (e.target.classList.toString().indexOf('plus') > -1 || e.target.innerHTML.indexOf('plus') > -1) {

                    // Change to "-"
                    //self.$('#viewCompanyStatsIconCollapse').removeClass('fa-plus');
                    //self.$('#viewCompanyStatsIconCollapse').addClass('fa-minus');
                    //self.$('.view-company-stats-box').removeClass('collapsed-box');

                    // Views
                    this.tile1.reset();
                    // this.tile2.reset();
                    // this.tile3.reset();
                    // this.tile4.reset();

                    this.tile1.show(new Widget1View());
                    // this.tile2.show(new Widget2View());
                    // this.tile3.show(new Widget3View());
                    // this.tile4.show(new Widget4View());
                    // expanded
                    App.model.set('tileViewCompanyStats', tileViewCompanyStats);
                } else {

                    // Change to "+"
                    //self.$('#viewCompanyStatsIconCollapse').removeClass('fa-minus');
                    //self.$('#viewCompanyStatsIconCollapse').addClass('fa-plus');
                    //self.$('.view-company-stats-box').addClass('collapsed-box');

                    // collapsed
                    tileViewCompanyStats = "collapsed";
                    App.model.set('tileViewCompanyStats', tileViewCompanyStats);
                }

                this.tileViewCompanyStats = tileViewCompanyStats;
            },
            onViewTeamPersonalStatsButtonCollapseClicked: function (e) {
                //console.log('ESATileLayoutView:onViewTeamPersonalStatsButtonCollapseClicked');

                var tileViewTeamPersonalStats = null;
                if (e.target.classList.toString().indexOf('plus') > -1 || e.target.innerHTML.indexOf('plus') > -1) {

                    // Change to "-"
                    //self.$('#viewTeamPersonalStatsIconCollapse').removeClass('fa-plus');
                    //self.$('#viewTeamPersonalStatsIconCollapse').addClass('fa-minus');
                    //self.$('.view-team-personal-stats-box').removeClass('collapsed-box');

                    // views
                    this.tile6.reset();
                    //this.tile8.reset();
                    this.tile12.reset();
                    this.tile13.reset();

                    this.tile6.show(new DashboardSummaryWidgetView({type: 'My Stats'}));
                    //this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));
                    this.tile12.show(new LeaderBoardWidgetView({type: 'Today'}));
                    this.tile13.show(new LeaderBoardWidgetView({type: 'This Month'}));

                    // expanded
                    App.model.set('tileViewTeamPersonalStats', tileViewTeamPersonalStats);
                } else {

                    // Change to "+"
                    //self.$('#viewTeamPersonalStatsIconCollapse').removeClass('fa-minus');
                    //self.$('#viewTeamPersonalStatsIconCollapse').addClass('fa-plus');
                    //self.$('.view-team-personal-stats-box').addClass('collapsed-box');

                    // collapsed
                    tileViewTeamPersonalStats = "collapsed";
                    App.model.set('tileViewTeamPersonalStats', tileViewTeamPersonalStats);
                }

                this.tileViewTeamPersonalStats = tileViewTeamPersonalStats;
            },
            onViewTeamMeetingPercButtonCollapseClicked: function (e) {
                console.log('ESATileLayoutView:onViewTeamMeetingPercButtonCollapseClicked');

                var tileViewTeamMeetingPerc = null;
                if (e.target.classList.toString().indexOf('plus') > -1 || e.target.innerHTML.indexOf('plus') > -1) {

                    // Change to "-"
                    //self.$('#viewTeamMeetingPercIconCollapse').removeClass('fa-plus');
                    //self.$('#viewTeamMeetingPercIconCollapse').addClass('fa-minus');
                    //self.$('.view-team-meeting-perc-box').removeClass('collapsed-box');

                    // view
                    this.tile5.reset();
                    this.tile5.show(new BarChartView({type: 'Team Meetings'}));

                    // expanded
                    App.model.set('tileViewTeamMeetingPerc', tileViewTeamMeetingPerc);
                } else {

                    // Change to "+"
                    //self.$('#viewTeamMeetingPercIconCollapse').removeClass('fa-minus');
                    //self.$('#viewTeamMeetingPercIconCollapse').addClass('fa-plus');
                    //self.$('.view-team-meeting-perc-box').addClass('collapsed-box');

                    // collapsed
                    tileViewTeamMeetingPerc = "collapsed";
                    App.model.set('tileViewTeamMeetingPerc', tileViewTeamMeetingPerc);
                }

                this.tileViewTeamMeetingPerc = tileViewTeamMeetingPerc;
            },
            onViewMyClientsButtonCollapseClicked: function (e) {
                //console.log('ESATileLayoutView:onViewMyClientsButtonCollapseClicked:e.target.classList.toString():' + e.target.classList.toString());
                //console.log('ESATileLayoutView:onViewMyClientsButtonCollapseClicked:e.target.innerHTML:' + e.target.innerHTML);

                var tileViewMyClients = null;
                if (e.target.classList.toString().indexOf('plus') > -1 || e.target.innerHTML.indexOf('plus') > -1) {

                    // Change to "-"
                    //self.$('#viewMyClientsIconCollapse').removeClass('fa-plus');
                    //self.$('#viewMyClientsIconCollapse').addClass('fa-minus');
                    //self.$('.view-my-clients-box').removeClass('collapsed-box');

                    // view
                    this.tile9.reset();
                    this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));

                    // expanded
                    App.model.set('tileViewMyClients', tileViewMyClients);
                } else {

                    // Change to "+"
                    //self.$('#viewMyClientsIconCollapse').removeClass('fa-minus');
                    //self.$('#viewMyClientsIconCollapse').addClass('fa-plus');
                    //self.$('.view-my-clients-box').addClass('collapsed-box');

                    // collapsed
                    tileViewMyClients = "collapsed";
                    App.model.set('tileViewMyClients', tileViewMyClients);
                }

                this.tileViewMyClients = tileViewMyClients;
            },
            onSelectedStatusFilterChanged: function () {
                if (this.tileViewMyClients === null) {
                    this.tile9.reset();
                    this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));
                }
            },
            onSelectedDateFilterChanged: function () {
                console.log('ESATileLayoutView:onSelectedDateFilterChanged');

                this.dateFilter = App.model.get('selectedDateFilter');

                if (this.dateFilter !== null && this.dateFilter !== undefined) {
                    this.newYear = App.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter);
                    this.newMonth = App.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter);

                    if (this.tileViewMyClients === null) {
                        this.tile9.reset();
                        this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));
                    }

                    // These only change if year changed
                    if (this.newYear !== this.previousYear) {
                        console.log('ESATileLayoutView:onSelectedDateFilterChanged:Year Changed');

                        if (this.tileViewCompanyStats === null) {
                            this.tile1.reset();
                            this.tile2.reset();
                            this.tile3.reset();
                            this.tile4.reset();
                            this.tile1.show(new Widget1View());
                            this.tile2.show(new Widget2View());
                            this.tile3.show(new Widget3View());
                            this.tile4.show(new Widget4View());
                        }
                    }

                    // These only change if month/year changed
                    if (this.newYear !== this.previousYear || this.newMonth != this.previousMonth) {
                        console.log('ESATileLayoutView:onSelectedDateFilterChanged:Month and Year Changed');

                        // if (this.tileViewTeamMeetingPerc === null) {
                        //     this.tile5.reset();
                        //     this.tile5.show(new BarChartView({type: 'Team Meetings'}));
                        // }

                        if (this.tileViewTeamPersonalStats === null) {
                            this.tile6.reset();
                            //this.tile8.reset();
                            this.tile6.show(new DashboardSummaryWidgetView({type: 'My Stats'}));
                            //this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));
                        }
                    }

                    this.previousMonth = this.newMonth;
                    this.previousYear = this.newYear;
                }


            },
            onMyEmployeeIdChanged: function () {

                console.log('ESATileLayoutView:onMyEmployeeIdChanged');
                this.resetLayout();

            },
            onTimerStatusChanged: function () {

                console.log('ESATileLayoutView:onTimerStatusChanged');
                this.timerStatus = App.model.get('timerStatus');

                console.log('TileLayoutView:onTimerStatusChanged:' + this.timerStatus);

                if (this.timerStatus !== "Manual") {
                    //&& this.previousTimerStatus !== "" && this.previousTimerStatus !== undefined && this.previousTimerStatus !== null) {

                    this.tile10.reset();
                    this.tile10.show(new TimerWidgetView());

                    if (this.timerStatus !== "Nothing" && this.previousTimerStatus !== "Nothing") {
                        if (this.tileViewMyClients === null) {
                            this.tile9.reset();
                            this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));
                        }

                        if (this.tileViewTeamPersonalStats === null) {
                            this.tile12.reset();
                            this.tile12.show(new LeaderBoardWidgetView({type: 'Today'}));

                            this.tile13.reset();
                            this.tile13.show(new LeaderBoardWidgetView({type: 'This Month'}));
                        }
                    }
                }

                if (this.timerStatus !== "Manual" && this.timerStatus !== "Open" && this.timerStatus !== "INTERRUPTED" && this.timerStatus !== "Nothing" && this.previousTimerStatus !== "Nothing") {

                    console.log('ESATileLayoutView:onTimerStatusChanged:timerStatus:previousTimerStatus:' + this.timerStatus + ":" + this.previousTimerStatus);
                    if (this.tileViewTeamPersonalStats === null) {
                        this.tile6.reset();
                        this.tile6.show(new DashboardSummaryWidgetView({type: 'My Stats'}));

                        //this.tile8.reset();
                        //this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));
                    }

                }

                this.previousTimerStatus = this.timerStatus;

            },
            onOpenDetailsClicked: function () {
//                console.log('TileLayoutView:onOpenDetailsClicked');

                App.modal.show(new ScoreboardDetailsView());
            },
            transitionIn: function () {
                //console.log('TileLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('TileLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('TileLayoutView:resize');
                var self = this;
            },
            remove: function () {
                //console.log('---------- TileLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });