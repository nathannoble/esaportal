define(['App', 'backbone', 'marionette', 'underscore', 'jquery', 'hbs!templates/esaMedia',
        'models/AdminDataModel', 'views/AVModalView'],
    function (App, Backbone, Marionette, _, $, template,AdminDataModel,AVModalView ) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #listen': 'listenClicked'
            },
            initialize: function () {
                //console.log('ESAMedia:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);

            },
            onRender: function () {
//                //console.log('ESAMedia:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('-------- ESAMedia:onShow --------');

                this.ID = this.model.get('ID');
                this.fileName = this.model.get('fileName');
                this.title = this.model.get('title');
                this.shortText = this.model.get('shortText');
                this.text = this.model.get('text');
                this.avFile = this.model.get('avFile');
                this.priority = this.model.get('priority');
                this.timeStamp = this.model.get('timeStamp');

                this.category = this.model.get('category');

                this.$('#text').html(this.text);
                //this.$('#text2').html(this.text2);
                this.$('#title').html(this.title);
                this.$('#timeStamp').html("Date posted: " + moment(this.timeStamp).clone().format('MM/DD/YYYY'));

                this.$('#image').attr('src', App.config.PortalFiles + "/ESAMedia/" + this.fileName );
                if (this.avFile.indexOf("mpeg") > -1 || this.avFile.indexOf("mp3") > -1) {
                    this.$('#avCaption').html('<i class="fa fa-microphone"></i>&nbsp;&nbsp; Listen to Audio');
                } else {
                    this.$('#avCaption').html('<i class="fa fa-video-camera"></i>&nbsp;&nbsp; Watch Video');
                }
            },
            listenClicked: function (e) {

                console.log('ESAMedia:listenClicked');

                var options = {
                    ID: this.ID,
                    timeStamp: this.timeStamp,
                    avFile: this.avFile,
                    fileName: this.fileName,
                    title: this.title,
                    shortText:this.shortText,
                    text:this.text,
                    subCategory:this.subCategory
                };

                // Open Modal dialog
                App.modal.show(new AVModalView(options));

            },

            onResize: function () {
//                //console.log('ESAMedia:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('ESAMedia:resize');

            },
            remove: function () {
                //console.log('-------- ESAMedia:remove --------');

                // Turn off events
                //$(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
