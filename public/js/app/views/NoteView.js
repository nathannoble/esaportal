define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/note', 'models/NoteModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #editNote': 'onEditNoteClicked'
            },
            initialize: function () {
                //console.log('NoteView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('NoteView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- NoteView:onShow --------');

                this.noteID = this.model.get('noteID');
                this.noteTimeStamp = new Date(this.model.get('noteTimeStamp')).toDateString();
                this.noteText = this.model.get('noteText');
                this.userName = this.model.get('firstName') + " " + this.model.get('lastName');

                this.$('#noteText').html(this.noteText);
                this.$('#noteUserName').html(this.userName);
                this.$('#noteTimeStamp').html(this.noteTimeStamp);
            },
            onEditNoteClicked: function () {
                //console.log('NoteView:onEditNoteClicked:noteID:' + this.noteID);


                App.model.set('selectedNoteId', this.noteID);

                $('#editNoteText').val(this.noteText);
                $('.editNoteLabel').text("Edit Note");
                $('#editNoteIcon').removeClass('fa-plus-circle');
                $('#editNoteIcon').addClass('fa-pencil-square-o');
            },
            onResize: function () {
               //console.log('NoteView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('NoteView:resize');

            },
            remove: function () {
                //console.log('-------- NoteView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#noteRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
