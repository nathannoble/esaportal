define(['App', 'jquery', 'hbs!templates/teamLayout', 'backbone', 'marionette',
        'views/BioCollectionView','views/SubHeaderView'],
    function (App, $, template, Backbone, Marionette, BioCollectionView,SubHeaderView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                corporateList: '#corporate-list',
                executiveList: '#executive-list',
                seniorMgtList: '#senior-mgt-list',
                accountMgtList: '#account-mgt-list',
                asstAccountMgtList: '#asst-account-mgt-list',
                empList: '#emp-list'
            },
            initialize: function () {
                //console.log('TeamLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('TeamLayoutView:onShow');

                this.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Team",
                    minorTitle: "",
                    page: "Team",
                    showDateFilter: false,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));

                this.corporateList.show(new BioCollectionView({subCategory: 0}));
                this.executiveList.show(new BioCollectionView({subCategory: 1}));
                this.seniorMgtList.show(new BioCollectionView({subCategory: 2}));
                this.accountMgtList.show(new BioCollectionView({subCategory: 3}));
                this.asstAccountMgtList.show(new BioCollectionView({subCategory: 4}));
                this.empList.show(new BioCollectionView({subCategory: 5}));

            },
            transitionIn: function () {
                console.log('TeamLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('TeamLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('TeamLayoutView:resize');
                var self = this;
            },
            remove: function () {
                //console.log('---------- TeamLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });