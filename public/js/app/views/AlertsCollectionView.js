define(['App', 'backbone', 'marionette', 'jquery', 'models/AdminDataModel', 'hbs!templates/alertsCollection',
        'views/AlertsView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, AdminDataModel, template, AlertsView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: AlertsView,
            initialize: function (options) {

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                //console.log('AlertsCollectionView:initialize:type:' + this.options.type);

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.messages = [];

                this.getData();

            },
            onRender: function () {
                //console.log('AlertsCollectionView:onRender');

                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- AlertsCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                var filter = [
                    {field: "category", operator: "eq", value: self.options.category},
                    {field: "showAsCompanyMessage", operator: "eq", value: true}
                ];

                this.adminDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalAdminData";
                            },
                            complete: function (e) {
                                console.log('AlertsCollectionView:adminDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'AlertsCollectionView:adminDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "ID",
                            fields: {
                                ID: {editable: false, type: "number"},
                                text: {editable: true, type: "string"},
                                shortText: {editable: true, type: "string"},
                                text2: {editable: true, type: "string"},
                                title: {editable: true, type: "string"},
                                fileName: {editable: true, type: "string"},
                                externalLink: {editable: true, type: "string"},
                                category: {editable: true, type: "number"},
                                subCategory: {editable: true, type: "number"},
                                priority: {editable: true, type: "number"},
                                showAsCompanyMessage:{editable: true, type: "boolean"},
                                addUserImage:{editable: true, type: "boolean"},
                                timeStamp: {editable: true,type: "date"}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   AlertsCollectionView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#companyMessagesCollectionViewLoading"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    AlertsCollectionView:messageDataSource:requestEnd');
                        //kendo.ui.progress(self.$("#companyMessagesCollectionViewLoading"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   AlertsCollectionView:messageDataSource:error');
                        kendo.ui.progress(self.$("#companyMessagesCollectionViewLoading"), false);

                        //self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   AlertsCollectionView:messageDataSource:change');

                        //var data = this.data();

                        //if (data.length <= 0)
                        //    self.displayNoDataOverlay();
                        //else
                        //    self.hideNoDataOverlay();
                    },
                    sort: {field: "priority", dir: "asc"}, //{field: "timeStamp", dir: "desc"},
                    filter: filter
                });

                this.adminDataSource.fetch(function () {

                    var data = this.data();
                    $.each(data, function (index, value) {

                        self.messages.push({
                            ID:value.ID,
                            fileName:value.fileName,
                            title:value.title,
                            shortText:value.shortText,
                            text:value.text,
                            text2:value.text2,
                            category: value.category,
                            subCategory: value.subCategory,
                            externalLink: value.externalLink,
                            priority: value.priority,
                            timeStamp:value.timeStamp,
                            isFirst: index === 0 ? true : false
                        });
                    });

                    self.messages.sort(function(a,b){
                        var c = a.priority;
                        var d =b.priority;
                        return c-d;
                    });

                    // Populate collection
                    self.collection.reset();
                    self.collection.comparator = function (model) {
                        return (model.get("priority"));
                    };

                    var oneAdded = false;
                    var twoAdded = false;

                    $.each(self.messages, function (index, value) {

                        //console.log('AlertsCollectionView:file:value:' + JSON.stringify(value));
                        var model = new AdminDataModel();

                        model.set("ID", value.ID);
                        model.set("fileName", value.fileName);
                        model.set("title", value.title);
                        model.set("shortText", value.shortText);
                        model.set("text", value.text);
                        model.set("text2", value.text2);
                        model.set("category", value.category);
                        //model.set("subCategory", value.subCategory);
                        model.set("externalLink", value.externalLink);
                        model.set("priority", value.priority);
                        model.set("timeStamp", value.timeStamp);
                        if (index === 0) {
                            model.set("isFirst", true);
                        } else {
                            model.set("isFirst", false);
                        }

                        // Add up to 1 level 1 alert
                        if (self.collection.length <= 1 && value.priority === 1 && !oneAdded) {
                            self.collection.unshift(model);
                            oneAdded = true;
                        }

                        // Add up to 2 level 2 alert
                        if (self.collection.length <= 1 && value.priority === 2 && !twoAdded) {
                            self.collection.push(model);
                            twoAdded = true;
                        }

                    });

                    self.collection.sort();

                    //console.log('AlertsCollectionView:collection:' + JSON.stringify(self.collection));
                });
            },
            hideNoDataOverlay: function () {
                //console.log('AlertsCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                //this.$('#fileWidgetContent').css('display', 'block');
            },
            onResize: function () {
                //console.log('AlertsCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('AlertsCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- AlertsCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
