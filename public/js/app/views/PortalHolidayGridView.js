define(['App', 'jquery', 'backbone', 'marionette', 'hbs!templates/portalHolidayGrid',
        'jquery.cookie', 'kendo/kendo.grid', 'kendo/kendo.data'],
    function (App, $, Backbone, Marionette, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('PortalHolidayGridView:initialize');

                _.bindAll(this);

                //this.editPopup = EditPortalHolidayPopup;

                var self = this;
                var app = App;

                //this.userId = App.model.get('userId');
                this.weekdays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
                this.dateFilter = App.model.get('selectedDateFilter');
                this.year = app.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter); //2016;

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.dataSource = new kendo.data.DataSource(
                    {
                        //autoBind: false,
                        type: "odata",
                        transport: {
                            read: {
                                url: App.config.DataServiceURL + "/odata/PortalHoliday",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: "value",
                            total: function (data) {
                                return data['odata.count'];
                            },
                            model: {
                                id: "caldate",
                                //{
                                // "caldate":"2010-01-01T00:00:00",
                                // "holidayName":"New Year's Day                ",
                                // "vMonthNum":1,
                                // "vMonthName":"January",
                                // "vYear":2010,
                                // "workingday":"N",
                                // "nyseHoliday":"Y"
                                //}
                                fields: {
                                    caldate: {
                                        editable: true,
                                        type: "date"
                                        //format: "{0:yyyy-MM-ddTHH:mm:ss}",
                                        //parseFormats: "{0:yyyy-MM-ddTHH:mm:ss}"
                                    },
                                    holidayName: {editable: true, type: "string", validation: {required: false}},
                                    vMonthName: {editable: true, type: "string", validation: {required: false}},
                                    vYear: {editable: true, type: "string", validation: {required: true}},
                                    vMonthNum: {editable: true, type: "number"},
                                    workingday: {editable: true, type: "boolean"},
                                    nyseholiday: {editable: true, type: "boolean"}
                                }
                            }
                        },
                        sort: {field: "vMonthNum", dir: "asc"},
                        filter: { field: "vYear", operator: "eq", value: self.year},
                        requestStart: function (e) {
                            //console.log('PortalHolidayGridView:dataSource:request start:');
                            kendo.ui.progress(self.$("#holidayLoading"), true);
                        },
                        requestEnd: function (e) {
                            //console.log('PortalHolidayGridView:dataSource:request end:');
                            //var data = this.data();
                            kendo.ui.progress(self.$("#holidayLoading"), false);
                        },
                        change: function (e) {
                            //console.log('PortalHolidayGridView:dataSource:change:');

                            var data = this.data();
                            if (data.length > 0) {

                            } else {
                                //self.displayNoDataOverlay();
                            }
                        },
                        error: function (e) {
                            //console.log('PortalHolidayGridView:dataSource:error');
                            kendo.ui.progress(self.$("#holidayLoading"), false);
                        }
                    });

            },
            onRender: function () {
                //console.log('PortalHolidayGridView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- PortalHolidayGridView:onShow --------");
                var self = this;

                // Panel title
                //$("#holidayTitle").html("Employees");

                //console.log('PortalHolidayGridView:onShow:districtID:' + this.districtID);
                //if (this.employeeID === null) {
                //    // Go back to home page
                //    window.location.href = "";
                //}

                this.grid = this.$("#holiday").kendoGrid({

                    //toolbar: ["create"],
                    //editable: "popup",  //inline, popup
                    sortable: true,
                    extra: false,
                    //selectable: "multiple",
                    resizable: true,
                    //reorderable: true,
                    //filterable: true,
                    //scrollable: false,
                    //selectable: "row",
                    //pageable: true,
                    //groupable: true,
                    columns: [
                        {
                            field: "holidayName",
                            title: "Holiday",
                            width: 150
                        },{
                            field: "caldate", title: "Date", width: 200,
                            template: function (e) {
                                var date = new Date(e.caldate);
                                var dayOfMonth = date.getDate();//  Returns the day of the month (from 1-31)
                                var dayOfWeek = date.getDay(); //  Returns the day of the week (from 0-6)
                                var year = date.getFullYear();
                                //Friday, January 1, 2016
                                var template = "<div style='text-align: left'>" + self.weekdays[dayOfWeek] + ", " + e.vMonthName + " " + dayOfMonth  + ", " + year + "</b></div>";
                                return template;
                            }
                        }
                    ],
                    dataBound: function (e) {
                        //console.log("PortalHolidayGridView:holiday:onShow:dataBound");
                    },
                    change: function (e) {
                        //console.log('PortalHolidayGridView:holiday:onShow:onChange');
                    }
                }); //.data("kendoGrid");

                // Load up the relevant data
                this.getData();

                // Resize the grid
                setTimeout(this.resize, 0);

            },
            getData: function () {
                console.log('PortalHolidayGridView:getData');

                var grid = this.$("#holiday").data("kendoGrid");

                var self = this;

                this.dataSource.fetch(function (data) {
                    //var json = JSON.stringify(data);

                    grid.setDataSource(self.dataSource);
                });

            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('PortalHolidayGridView:resize');

                // When the container is resized the grid needs to be resized vertically
                // and rendered

                var parentHeight = this.$("#holiday").parent().parent().height();
                this.$("#holiday").height(parentHeight);

                parentHeight = parentHeight - 15;

                // Account for header i.e. column resizing to double lines
                var headerHeight = this.$(".k-header").height();
                //console.log('PortalHolidayGridView:resize:parentHeight:' + parentHeight);
                //console.log('PortalHolidayGridView:resize:headerHeight:' + headerHeight);
                this.$("#holiday").find(".k-grid-content").height(parentHeight - headerHeight);
            },
            remove: function () {
                //console.log("-------- PortalHolidayGridView:remove --------");

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Clear tooltips
                if (this.$("#holiday").data('ui-tooltip'))
                    this.$("#holiday").tooltip('destroy');

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });