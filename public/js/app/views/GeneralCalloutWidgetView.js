define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/generalCalloutWidget','views/HeaderView'],
    function (App, Backbone, Marionette, $, template,HeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                console.log('GeneralCalloutWidgetView:initialize:options:' + options);

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log('---------- GeneralCalloutWidgetView:onShow ----------');

                this.populateWidget();
            },
            populateWidget: function () {
                console.log('GeneralCalloutWidgetView:populateWidget');

                var self = this;

                self.$("#calloutWidget").addClass("alert");
                self.$("#calloutWidget").addClass('alert-danger');
                self.$('#alertHeader').html(self.options.type);
                self.$('#alertText').html(self.options.message);

                if (self.options.borderRadius !== undefined) {
                    self.$('.alert').css('border-radius', self.options.borderRadius);
                }

                window.setTimeout(function () {
                    self.$("#calloutWidget").removeClass("alert").fadeTo(500, 0).slideUp(500, function () {
                        self.$('#alertHeader').html("");
                        self.$('#alertText').html("");
                        self.$("#calloutWidget").removeClass('alert-danger');
                        self.$("#callback-alert").css("height","0px");
                    });

                }, 5000);

            },
            onResize: function () {
//                //console.log('GeneralCalloutWidgetView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('GeneralCalloutWidgetView:resize');

            },
            remove: function () {
                //console.log('---------- GeneralCalloutWidgetView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove extra space created for it in dashboard section area
                $('.esa-section').css('height', '100%').css('height', '-=65px');
                $('.esa-pp-section').css('height', '100%').css('height', '-=65px');
                $('.esa-views-section').css('height', '100%').css('height', '-=31px');
                $('.esa-tables-section').css('height', '100%').css('height', '-=35px');
                $('.esa-stats-timecard-section').css('height', '100%').css('height', '-=50px');


                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });