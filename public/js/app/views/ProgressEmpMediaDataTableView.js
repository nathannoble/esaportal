define(['App', 'backbone', 'marionette', 'jquery',
    'hbs!templates/progressEmpMediaDataTable',
    'views/ProgressEmpMediaDataGridView',
    'views/SubHeaderView'],
    function (App, Backbone, Marionette, $, template,
              ProgressEmpMediaDataGridView,
              SubHeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                empMediaDataRegion: "#emp-media-data-region"
            },
            initialize: function () {

                //console.log('ProgressEmpMediaDataTableView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                App.modal.currentView = null;

                  this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);

            },
            onRender: function () {

                //console.log('ProgressEmpMediaDataTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log("-------- ProgressEmpMediaDataTableView:onShow --------");

                this.userId = App.model.get('userId');
                //this.userType = App.model.get('userType');

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    this.onSelectedDateFilterChanged();
                }

            },
            onSelectedDateFilterChanged: function () {
                console.log('ProgressEmpMediaDataTableView:onSelectedDateFilterChanged');

                this.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Employee Podcast Report",
                    minorTitle: '',
                    page: "Employee Podcast Report",
                    showDateFilter: true,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));
                this.empMediaDataRegion.show(new ProgressEmpMediaDataGridView());

            },

            resize: function () {
                //console.log('ProgressEmpMediaDataTableView:resize');

                if (this.clientDataRegion.currentView)
                    this.clientDataRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------ProgressEmpMediaDataTableView:remove --------");

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });