define(['App', 'jquery', 'hbs!templates/511TileLayout', 'backbone', 'marionette', 'underscore', 'options/ViewOptions',
        'views/Widget1View', 'views/Widget2View', 'views/Widget3View', 'views/Widget4View', 'views/BarChartView',
        'views/DashboardSummaryWidgetView', 'views/CalloutWidgetView', 'views/MessageWidgetView',
        'views/TimerWidgetView', 'views/ProgressClientDataGridView', 'views/PhoneDirectoryWidgetView',
        'views/LeaderBoardWidgetView', 'views/SubHeaderView', 'views/ScoreboardDetailsView'],
    function (App, $, template, Backbone, Marionette, _, ViewOptions,
              Widget1View, Widget2View, Widget3View, Widget4View, BarChartView,
              DashboardSummaryWidgetView, CalloutWidgetView, MessageWidgetView,
              TimerWidgetView, ProgressClientDataGridView, PhoneDirectoryWidgetView,
              LeaderBoardWidgetView, SubHeaderView, ScoreboardDetailsView) {

        return Backbone.Marionette.Layout.extend({
            template: template,
            events: {
                'click #OpenDetails': 'onOpenDetailsClicked'
                //'keydown': 'keyAction'
            },
            regions: {
                subHeaderRegion: "#sub-header",
                timerAlertRegion: "#timer-alert",
                alertTips: "#alert-tips",
                alertValue: "#alert-value",
                alertMessage: "#alert-message",
                alertOpenTimer: "#alert-open-timer",
                tile1: "#tile-1",
                tile2: "#tile-2",
                tile3: "#tile-3",
                tile4: "#tile-4",
                tile5: "#tile-5",
                tile6: "#tile-6",
                tile7: "#tile-7",
                tile8: "#tile-8",
                tile9: "#tile-9",
                tile10: "#tile-10",
                tile11: "#tile-11",
                tile12: "#tile-12",
                tile13: "#tile-13"
            },
            initialize: function () {
                //console.log('TileLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                // set date range
                var start = moment().startOf('month');
                var end = moment().endOf('month');
                this.dateFilter = start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY');
                App.model.set('selectedDateFilter', this.dateFilter);

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);
                //this.listenTo(App.vent, App.Events.ModelEvents.MyEmployeeIdChanged, this.onMyEmployeeIdChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.TimerStatusChanged, this.onTimerStatusChanged);
                this.listenTo(App.vent, App.Events.ModelEvents.SelectedStatusFilterChanged, this.onSelectedStatusFilterChanged);

            },
            onRender: function () {
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {

                //console.log('TileLayoutView:onShow');

                this.userId = App.model.get('userId');
                this.myEmployeeId = App.model.get('myEmployeeId');

                if (this.userId === null) {

                    this.subHeaderRegion.reset();

                    this.alertTips.reset();
                    this.alertValue.reset();
                    this.alertMessage.reset();
                    this.alertOpenTimer.reset();

                    this.tile1.reset();
                    this.tile2.reset();
                    this.tile3.reset();
                    this.tile4.reset();
                    //this.tile5.reset();
                    this.tile6.reset();
                    this.tile7.reset();
                    this.tile8.reset();
                    this.tile9.reset();
                    this.tile10.reset();
                    this.tile11.reset();
                    this.tile12.reset();
                    this.tile13.reset();

                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    console.log('511TileLayoutView:onShow:userId:' + this.userId);
                    if (this.myEmployeeId !== null) {
                        console.log('511TileLayoutView:onShow:myEmployeeId:' + this.myEmployeeId);
                        this.resetLayout();
                    }
                }
            },
            resetLayout: function () {
                console.log('511TileLayoutView:resetLayout');

                var self = this;

                self.subHeaderRegion.reset();

                self.subHeaderRegion.show(new SubHeaderView({
                    majorTitle: "Dashboard",
                    minorTitle: "", //'Control Panel',
                    page: "Dashboard",
                    showDateFilter: true,
                    showStatusFilter: false,
                    showEmpStatusFilter: false,
                    showClientReportFilter: false,
                    showViewsFilter: false,
                    showCompanyFilter: false,
                    showClientFilter: false,
                    showTeamLeaderFilter: false,
                    showPayrollProcessingDate: false
                }));

                this.alertTips.reset();
                this.alertValue.reset();
                this.alertMessage.reset();
                this.alertOpenTimer.reset();

                this.tile1.reset();
                this.tile2.reset();
                this.tile3.reset();
                this.tile4.reset();
                //this.tile5.reset();
                this.tile6.reset();
                this.tile7.reset();
                this.tile8.reset();
                this.tile9.reset();
                this.tile10.reset();
                this.tile11.reset();
                this.tile12.reset();
                this.tile13.reset();

                this.alertTips.show(new CalloutWidgetView({type: 'Tips'}));
                this.alertValue.show(new CalloutWidgetView({type: 'Value'}));
                this.alertMessage.show(new CalloutWidgetView({type: 'Message'}));
                this.alertOpenTimer.show(new CalloutWidgetView({type: 'Open Timer'}));

                this.tile1.show(new Widget1View());
                this.tile2.show(new Widget2View());
                this.tile3.show(new Widget3View());
                this.tile4.show(new Widget4View());
                //this.tile5.show(new BarChartView({type: 'Team Meetings'}));
                this.tile6.show(new DashboardSummaryWidgetView({type: 'My Stats'}));
                this.tile7.show(new MessageWidgetView());
                this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));
                this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));
                this.tile10.show(new TimerWidgetView());
                this.tile11.show(new PhoneDirectoryWidgetView());
                this.tile12.show(new LeaderBoardWidgetView({type: 'Today'}));
                this.tile13.show(new LeaderBoardWidgetView({type: 'This Month'}));

            },
            onSelectedStatusFilterChanged: function () {
                this.tile9.reset();
                this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));
            },
            onSelectedDateFilterChanged: function () {

                console.log('511TileLayoutView:onSelectedDateFilterChanged');

                this.dateFilter = App.model.get('selectedDateFilter');

                this.newYear = App.Utilities.numberUtilities.getYearFromDateFilter(this.dateFilter);
                this.newMonth = App.Utilities.numberUtilities.getMonthFromDateFilter(this.dateFilter);

                this.tile9.reset();
                this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));

                // These only change if year changed
                if (this.newYear !== this.previousYear) {
                    console.log('511TileLayoutView:onSelectedDateFilterChanged:Year Changed');
                    this.tile1.reset();
                    this.tile2.reset();
                    this.tile3.reset();
                    this.tile4.reset();
                    this.tile1.show(new Widget1View());
                    this.tile2.show(new Widget2View());
                    this.tile3.show(new Widget3View());
                    this.tile4.show(new Widget4View());
                }

                // These only change if month/year changed
                if (this.newYear !== this.previousYear || this.newMonth != this.previousMonth) {
                    console.log('511TileLayoutView:onSelectedDateFilterChanged:Month and Year Changed');
                    //this.tile5.reset();
                    this.tile6.reset();
                    this.tile8.reset();
                    //this.tile5.show(new BarChartView({type: 'Team Meetings'}));
                    this.tile6.show(new DashboardSummaryWidgetView({type: 'My Stats'}));
                    this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));
                }

                this.previousMonth = this.newMonth;
                this.previousYear = this.newYear;

            },
            onMyEmployeeIdChanged: function () {

                console.log('511TileLayoutView:onMyEmployeeIdChanged');
                this.resetLayout();

            },
            onTimerStatusChanged: function () {

                console.log('511TileLayoutView:onTimerStatusChanged');
                this.timerStatus = App.model.get('timerStatus');

                console.log('TileLayoutView:onTimerStatusChanged:' + this.timerStatus);

                if (this.timerStatus !== "Manual") {
                    //&& this.previousTimerStatus !== "" && this.previousTimerStatus !== undefined && this.previousTimerStatus !== null) {

                    this.tile10.reset();
                    this.tile10.show(new TimerWidgetView());

                    if ( this.timerStatus !== "Nothing" && this.previousTimerStatus !== "Nothing") {
                        this.tile9.reset();
                        this.tile9.show(new ProgressClientDataGridView({type: 'My Clients'}));

                        this.tile12.reset();
                        this.tile12.show(new LeaderBoardWidgetView({type: 'Today'}));

                        this.tile13.reset();
                        this.tile13.show(new LeaderBoardWidgetView({type: 'This Month'}));
                    }
                }

                if (this.timerStatus !== "Manual" && this.timerStatus !== "Open" && this.timerStatus !== "INTERRUPTED" && this.timerStatus !== "Nothing" && this.previousTimerStatus !== "Nothing") {

                    console.log('511TileLayoutView:onTimerStatusChanged:timerStatus:previousTimerStatus:' + this.timerStatus + ":" + this.previousTimerStatus);
                    this.tile6.reset();
                    this.tile6.show(new DashboardSummaryWidgetView({type: 'My Stats'}));

                    this.tile8.reset();
                    this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));

                }

                this.previousTimerStatus = this.timerStatus;

            },
            onOpenDetailsClicked: function () {
//                console.log('TileLayoutView:onOpenDetailsClicked');

                App.modal.show(new ScoreboardDetailsView());
            },
            transitionIn: function () {
                //console.log('TileLayoutView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function () {
                //console.log('TileLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('TileLayoutView:resize');
                var self = this;
            },
            remove: function () {
                //console.log('---------- TileLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove attached event handlers
                this.$el.off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });