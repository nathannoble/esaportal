define(['App', 'jquery', 'hbs!templates/esaTileLayoutTest', 'backbone', 'marionette', 'underscore', 'options/ViewOptions',
        'views/Widget1View','views/Widget2View','views/Widget3View',
        'views/Widget4View','views/StatisticsChartView','views/BarChartView',
        'views/DashboardSummaryWidgetView','views/CalloutWidgetView','views/MessageWidgetView'],
    function (App, $, template, Backbone, Marionette, _, ViewOptions,
              Widget1View,Widget2View,Widget3View,Widget4View,StatisticsChartView,BarChartView,
              DashboardSummaryWidgetView,CalloutWidgetView,MessageWidgetView) {

        return Backbone.Marionette.Layout.extend({
            template:template,
            regions:{
                tileDanger: "#tile-danger",
                tileTips: "#tile-tips",
                tileValue: "#tile-value",
                tile1: "#tile-1",
                tile2: "#tile-2",
                tile3: "#tile-3",
                tile4: "#tile-4",
                tile5: "#tile-5",
                tile6: "#tile-6",
                tile7: "#tile-7",
                tile8: "#tile-8",
                tile9: "#tile-9",
                tile10: "#tile-10"
            },
            initialize: function(){
                console.log('TileLayoutView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

                this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);

            },
            onRender: function(){
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function(){

                var self = this;
                this.userId = 'bbee446a-1415-4dc8-897a-a3235146bc62';
                App.model.set('userId','bbee446a-1415-4dc8-897a-a3235146bc62');
                App.model.set('userKey',10011);

                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {
                    console.log('ESATileLayoutTestView:onShow:userId:' + this.userId);
                    this.resetLayout();
                }
            },
            resetLayout: function () {
                console.log('ESATileLayoutTestView:resetDashboard');

                //this.tileDanger.reset();
                //this.tileTips.reset();
                //this.tileValue.reset();
                //this.tile1.reset();
                //this.tile2.reset();
                //this.tile3.reset();
                //this.tile4.reset();
                //this.tile5.reset();
                //this.tile6.reset();
                //this.tile7.reset();
                //this.tile8.reset();

                //if (true) {
                //    this.tileDanger.show(new CalloutWidgetView({
                //        type: 'Danger',
                //        message: 'Timer File is currently open.'
                //    }));
                //}
                //this.tileTips.show(new CalloutWidgetView({type: 'Tips'}));
                //this.tileValue.show(new CalloutWidgetView({type: 'Value'}));
                this.tile1.show(new Widget1View());
                //this.tile2.show(new Widget2View());
                this.tile3.show(new Widget3View());
                //this.tile4.show(new Widget4View());
                this.tile5.show(new BarChartView({type: 'Team Meetings'}));
                //this.tile6.show(new DashboardSummaryWidgetView({type: 'My Stats'}));
                //this.tile7.show(new MessageWidgetView());
                //this.tile8.show(new DashboardSummaryWidgetView({type: 'My Team Stats'}));

                $('#main-region').css('margin-left', '0px');
                $('#main-region').css('background-color', 'white');
            },
            onSelectedDateFilterChanged: function () {
                this.resetLayout();
            },
            transitionIn: function(){
                console.log('ESATileLayoutTestView:transitionIn');
                this.$el.fadeIn();
            },
            onResize: function(){
                //console.log('TileLayoutView:windowResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if(this.winWidth!=winNewWidth || this.winHeight!=winNewHeight)
                {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function(){
                console.log('TileLayoutView:resize');
            },
            remove: function(){
                console.log('---------- TileLayoutView:remove ----------');

                // Turn off window events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });