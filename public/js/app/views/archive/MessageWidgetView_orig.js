define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/messageWidget', 'kendo/kendo.data'],
    function (App, Backbone, Marionette, $, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend({
            template: template,
            initialize: function (options) {
                //console.log('MessageWidgetView:initialize');

                _.bindAll(this);

                // Subscribe to browser events
                $(window).on('resize', this.onResize);

                this.options = options;

                var date = new Date();
                this.today = "2016-11-03";
                //this.today = date.toISOString();

            },
            onRender: function () {
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('---------- MessageWidgetView:onShow ----------');

                // UIUX - Events beyond css and view classes
                var self = this;

                //this.$('#widgetTitle').html(this.options.type);

                this.populateWidget();
            },
            populateWidget: function () {
//                //console.log('MessageWidgetView:populateWidget');

                var self = this;
                var app = App;

                // data calls
                this.commentDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalComment";
                            },
                            complete: function (e) {
                                console.log('MessageWidgetView:commentDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'MessageWidgetView:commentDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                //console.log('PortalTipGridView:update');
                                return App.config.DataServiceURL + "/odata/PortalComment" + "(" + data.commentID + ")";
                            },
                            complete: function (e) {
                                console.log('MessageWidgetView:commentDataSource:update:complete');
                            },
                            error: function (e) {
                                var message = 'MessageWidgetView:commentDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                            
                        },
                        create: {
                            url: function (data) {
                                //console.log('PortalTipGridView:create');
                                return App.config.DataServiceURL + "/odata/PortalComment";
                            },
                            complete: function (e) {
                                console.log('MessageWidgetView:commentDataSource:create:complete');
                            },
                            error: function (e) {
                                var message = 'MessageWidgetView:commentDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                            //dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                //console.log('PortalTipGridView:destroy');
                                return App.config.DataServiceURL + "/odata/PortalComment" + "(" + data.commentID + ")";
                            },
                            complete: function (e) {
                                console.log('MessageWidgetView:commentDataSource:destroy:complete');
                            },
                            error: function (e) {
                                var message = 'MessageWidgetView:commentDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "commentID",
                            //{
                            // "commentID":1018,
                            // "commentText":"Steve is awesome. I pay him for his thoughts.",
                            // "timeStamp":"2011-05-18T13:41:26.103",
                            // "messageID":1006,
                            // "userKey":10002,
                            // "lastName":"Santala",
                            // "firstName":"Mitch"
                            //}
                            fields: {
                                commentID: {editable: false, defaultValue: 0, type: "number"},
                                commentText: {editable: true, type: "string", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: true}},
                                messageID: {type: "number"},
                                userKey: {type: "number"},
                                lastName: {editable: true, type: "string", validation: {required: false}},
                                firstName: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   MessageWidgetView:commentDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingMessageWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    MessageWidgetView:commentDataSource:requestEnd');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessageWidgetView:commentDataSource:error');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: self.onCommentDataSourceChange
                });

                this.messageDataSource = new kendo.data.DataSource({
                    autoBind:false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalMessage";
                            },
                            complete: function (e) {
                                console.log('MessageWidgetView:messageDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'MessageWidgetView:messageDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "messageID",
                            //{
                            // "messageID":1008,
                            // "messageDate":"2011-10-01",
                            // "messageText":"Thanks for helping make ESA a great place to work!"
                            //}
                            fields: {
                                messageID: {editable: false, defaultValue: 0, type: "number"},
                                messageText: {editable: true, type: "string", validation: {required: false}},
                                messageDate: {editable: true, type: "date", validation: {required: true}}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   MessageWidgetView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingMessageWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    MessageWidgetView:messageDataSource:requestEnd');
                        //kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessageWidgetView:messageDataSource:error');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessageWidgetView:messageDataSource:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: { field: "messageDate", operator: "eq", value: self.today }
                });

                this.messageDataSource.fetch(function () {

                    var data = this.data();
                    //console.log('MessageWidgetView:messageDataSource:data:' + JSON.stringify(data));
                    //var messageIDs = [];
                    var state = {};
                    var filters = [];

                    $.each(data, function (index) {
                        var record = data[index];
                        //messageIDs.push(record.messageID);
                        //console.log('MessageWidgetView:messageDataSource:data record:' + JSON.stringify(record));
                        filters.push({field: "messageID", operator: "eq", value: record.messageID});
                    });

                    state.filter = {
                        logic: "or",
                        filters: filters
                    };

                    // Only query for comments if there is at least one message
                    if (data.length > 0) {
                        self.commentDataSource.query(state);
                    }
                });

            },
            onCommentDataSourceChange: function () {
                var data = this.commentDataSource.data();

                //console.log('MessageWidgetView:onCommentDataSourceChange:data:' + JSON.stringify(data));
                $.each(data, function (index) {
                    var record = data[index];
                });

            },
            displayNoDataOverlay: function () {
                //console.log('MessageWidgetView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            hideNoDataOverlay: function () {
                //console.log('MessageWidgetView:hideNodDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the grid
                this.$('.tile-content').css('display', 'block');
            },
            onResize: function () {
//                //console.log('MessageWidgetView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }
                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('MessageWidgetView:resize');

            },
            remove: function () {
                //console.log('---------- MessageWidgetView:remove ----------');

                // Turn off events
                $(window).off('resize', this.onResize);

                // Remove attached event handlers
//                this.$el.off();                             // turns all events off
                this.$('.tile-content').off();
                this.$('.tile-more-info').off();
                this.$('.tile-hover-tip').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });