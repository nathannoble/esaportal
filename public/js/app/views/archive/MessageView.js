define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/message',
        'views/CommentCollectionView','models/MessageModel'],
    function (App, Backbone, Marionette, $, template,CommentCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                commentList: '#comment-list'
            },
            initialize: function () {
                //console.log('MessageView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('MessageView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- MessageView:onShow --------');

                //this.model.set("message_id", value.messageID);
                //this.model.set("message_date", value.messageDate);
                //this.model.set("message_text", value.messageText);

                this.messageID = this.model.get('message_id');
                this.messageDate = new Date(this.model.get('message_date')).toDateString();
                this.messageText = this.model.get('message_text');
                this.comments = this.model.get('comments');

                this.$('#message').html(this.messageText);
                this.$('#messageTimeStamp').html(this.messageDate);
                //console.log('   MessageView:onShow:CommentCollection:comments:' + JSON.stringify(this.comments));
                this.commentList.show(new CommentCollectionView({comments:this.comments}));
            },

            onResize: function () {
//                //console.log('MessageView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('MessageView:resize');

            },
            remove: function () {
                //console.log('-------- MessageView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#messageRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
