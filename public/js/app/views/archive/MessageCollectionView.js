define(['App', 'backbone', 'marionette', 'jquery', 'models/MessageModel', 'hbs!templates/messageCollection',
        'views/MessageView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, MessageModel, template,
              MessageView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: MessageView,
            initialize: function (options) {
                //console.log('MessageCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();
                this.messages = [];

                this.showMessagesUntilThisDate = moment().clone().subtract(14, 'days').format('YYYY-MM-DD'); // 15 days ago at the moment
                this.today = moment().clone().format('YYYY-MM-DD');
                this.getData();

            },

            onShow: function () {
                //console.log("-------- MessageCollectionView:onShow --------");

            },
            getData: function () {

                var self = this;

                this.commentDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalComment";
                            },
                            complete: function (e) {
                                console.log('MessageCollectionView:commentDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'MessageCollectionView:commentDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                console.log('MessageCollectionView:commentDataSource:update');
                                return App.config.DataServiceURL + "/odata/PortalComment" + "(" + data.commentID + ")";
                            },
                            complete: function (e) {
                                console.log('MessageCollectionView:commentDataSource:update:complete');
                            },
                            error: function (e) {
                                var message = 'MessageCollectionView:commentDataSource:update:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                        },
                        create: {
                            url: function (data) {
                                //console.log('MessageCollectionView:create');
                                return App.config.DataServiceURL + "/odata/PortalComment";
                            },
                            complete: function (e) {
                                console.log('MessageCollectionView:commentDataSource:create:complete');
                            },
                            error: function (e) {
                                var message = 'MessageCollectionView:commentDataSource:create:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                            //dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                //console.log('MessageCollectionView:destroy');
                                return App.config.DataServiceURL + "/odata/PortalComment" + "(" + data.commentID + ")";
                            },
                            complete: function (e) {
                                console.log('MessageCollectionView:commentDataSource:destroy:complete');
                            },
                            error: function (e) {
                                var message = 'MessageCollectionView:commentDataSource:destroy:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "commentID",
                            //{
                            // "commentID":1018,
                            // "commentText":"Steve is awesome. I pay him for his thoughts.",
                            // "timeStamp":"2011-05-18T13:41:26.103",
                            // "messageID":1006,
                            // "userKey":10002,
                            // "lastName":"Santala",
                            // "firstName":"Mitch"
                            //}
                            fields: {
                                commentID: {editable: false, defaultValue: 0, type: "number"},
                                commentText: {editable: true, type: "string", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: true}},
                                messageID: {type: "number"},
                                userKey: {type: "number"},
                                lastName: {editable: true, type: "string", validation: {required: false}},
                                firstName: {editable: true, type: "string", validation: {required: false}}
                            }
                        }
                    },
                    sort: [{field: "messageID", dir: "desc"},{field: "commentID", dir: "asc"}],
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   MessageCollectionView:commentDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingMessageWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    MessageCollectionView:commentDataSource:requestEnd');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessageCollectionView:commentDataSource:error');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: self.onCommentDataSourceChange
                });

                this.messageDataSource = new kendo.data.DataSource({
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalMessage";
                            },
                            complete: function (e) {
                                console.log('MessageCollectionView:messageDataSource:read:complete');
                            },
                            error: function (e) {
                                var message = 'MessageCollectionView:messageDataSource:read:error:' + JSON.stringify(e.responseJSON);
                                console.log(message);
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "messageID",
                            //{
                            // "messageID":1008,
                            // "messageDate":"2011-10-01",
                            // "messageText":"Thanks for helping make ESA a great place to work!"
                            //}
                            fields: {
                                messageID: {editable: false, defaultValue: 0, type: "number"},
                                messageText: {editable: true, type: "string", validation: {required: false}},
                                messageDate: {editable: true, type: "date", validation: {required: true}}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   MessageCollectionView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingMessageWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    MessageCollectionView:messageDataSource:requestEnd');
                        //kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessageCollectionView:messageDataSource:error');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessageCollectionView:messageDataSource:change');

                        var data = this.data();

                        if (data.length <= 0)
                            self.displayNoDataOverlay();
                        else
                            self.hideNoDataOverlay();
                    },
                    filter: [
                        {field: "messageDate", operator: "gte", value: self.showMessagesUntilThisDate},
                        {field: "messageDate", operator: "lte", value: self.today}
                    ],
                    sort: {field: "messageDate", dir: "desc"}
                });

                this.messageDataSource.fetch(function () {

                    var data = this.data();
                    var messageCount = data.length;
                    //console.log('MessageCollectionView:messageDataSource:messageCount:data' + messageCount + ":" + JSON.stringify(data));
                    //var messageIDs = [];
                    var state = {};
                    var filters = [];

                    $.each(data, function (index, value) {
                        //messageIDs.push(value.messageID);

                        self.messages.push({
                            messageID: value.messageID,
                            messageDate: value.messageDate,
                            messageText: value.messageText,
                            comments:[]
                        });

                        //console.log('MessageCollectionView:messageDataSource:data record:' + JSON.stringify(value));
                        filters.push({field: "messageID", operator: "eq", value: value.messageID});
                    });

                    state.filter = {
                        logic: "or",
                        filters: filters
                    };

                    // Only query for comments if there is at least one message
                    if (data.length > 0) {
                        self.commentDataSource.query(state);
                    }
                });

            },
            onCommentDataSourceChange: function () {

                var self = this;
                var data = this.commentDataSource.data();

                //console.log('MessageCollectionView:onCommentDataSourceChange:data:' + JSON.stringify(data));

                // message records
                $.each(self.messages, function (index2, value2){

                    // add comments to appropriate message model object
                    //console.log('MessageCollectionView:commentDataSource:data record:' + JSON.stringify(value));

                    // comment records
                    $.each(data, function (index, value) {

                        // if comment and message have same messageID add comment to message comment collection
                        if (value.messageID === value2.messageID){
                            value2.comments.push(value);
                        }
                    });
                });

                // Populate message and comment collections
                self.collection.reset();
                self.collection.comparator = function (model) {
                    return model.get("message_id");
                };

                $.each(this.messages, function (index, value) {
                    var model = new MessageModel();
                    //{
                    // "messageID":1008,
                    // "messageDate":"2011-10-01",
                    // "messageText":"Thanks for helping make ESA a great place to work!"
                    //}
                    model.set("message_id", value.messageID);
                    model.set("message_date", value.messageDate);
                    model.set("message_text", value.messageText);
                    model.set("comments", value.comments);

                    if (self.options)
                        model.set("isEditable", self.options.isEditable);
                    else
                        model.set("isEditable", false);

                    self.collection.push(model);
                });


            },
            hideNoDataOverlay: function () {
                //console.log('MessageCollectionView:hideNoDataOverlay');

                // Remove the no data overlay
                this.$('.no-data-overlay-outer').remove();

                // Display the widget content
                this.$('#messageWidgetContent').css('display', 'block');
            },
            displayNoDataOverlay: function () {
                //console.log('MessageCollectionView:displayNoDataOverlay');

                // Hide the widget content
                this.$('#messageWidgetContent').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer" style="height:485px;"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {
//                //console.log('MessageCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('MessageCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- MessageCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
