define(['App', 'backbone', 'marionette', 'jquery', 'models/CommentModel', 'hbs!templates/commentCollection',
        'views/CommentView', 'kendo/kendo.data', 'kendo/kendo.binder'],
    function (App, Backbone, Marionette, $, CommentModel, template,
              CommentView) {
        return Backbone.Marionette.CompositeView.extend({
            template: template,
            itemView: CommentView,
            initialize: function (options) {
                //console.log('CommentCollectionView:initialize');

                _.bindAll(this);

                // Store input configuration parameters
                this.options = options;

                // Initialize the collection
                this.collection = new Backbone.Collection();

                var self = this;

                this.comments = [];

                if (this.options.comments) {
                    this.comments = this.options.comments;
                }

                //console.log('CommentCollectionView:initialize:comments: ' + JSON.stringify(this.comments));
                // Populate message and comment collections
                self.collection.reset();
                self.collection.comparator = function (model) {
                    return -(model.get("time_stamp"));
                };

                if (self.comments.length === 0) {
                    // Hide comment area if no elements exist
                }

                $.each(self.comments, function (index,value) {

                    //console.log('CommentCollectionView:comments:record:' + JSON.stringify(value));

                    // Need to get the data record and populate it
                    var model = new CommentModel();
                    //{
                    // "commentID":1018,
                    // "commentText":"Steve is awesome. I pay him for his thoughts.",
                    // "timeStamp":"2011-05-18T13:41:26.103",
                    // "messageID":1006,
                    // "userKey":10002,
                    // "lastName":"Santala",
                    // "firstName":"Mitch"
                    //}

                    model.set("comment_id", value.commentID);
                    model.set("time_stamp", value.timeStamp);
                    model.set("comment_text", value.commentText);
                    model.set("user_key", value.userKey);
                    model.set("last_name", value.lastName);
                    model.set("first_name", value.firstName);
                    model.set("message_id", value.messageID);

                    self.collection.push(model);
                });

                self.collection.sort();


            },
            onShow: function () {
                //console.log("-------- CommentCollectionView:onShow --------");

                var self = this;

                //console.log("CommentCollectionView:onShow:collection:" + JSON.stringify(self.collection));

            },
            onResize: function () {
//                //console.log('CommentCollectionView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('CommentCollectionView:resize');

            },
            remove: function () {
                //console.log('---------- CommentCollectionView:remove ----------');

                // Turn off events
//                $(window).off('resize', this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
