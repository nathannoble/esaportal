define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/comment', 'models/CommentModel'],
    function (App, Backbone, Marionette, $, template) {
        return Backbone.Marionette.ItemView.extend({
            template: template,
            //events: {
            //    'click #commentRow': 'rowClicked'
            //},
            initialize: function () {
                //console.log('CommentView:initialize');

                //this.selected = false;

                _.bindAll(this);

                // Subscribe to browser events
//                $(window).on('resize', this.onResize);
            },
            onRender: function () {
//                //console.log('CommentView:onRender');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('-------- CommentView:onShow --------');

                //{
                // "commentID":1018,
                // "commentText":"Steve is awesome. I pay him for his thoughts.",
                // "timeStamp":"2011-05-18T13:41:26.103",
                // "messageID":1006,
                // "userKey":10002,
                // "lastName":"Santala",
                // "firstName":"Mitch"
                //}

                this.messageID = this.model.get('message_id');

                //var nowUTC = new Date(now.getUTCFullYear(),now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                //var currentTime = nowUTC.getTime();
                //var offset = now.getTimezoneOffset()/60;

                var date = new Date(this.model.get('time_stamp'));
                var time =   moment(date).format('h:mm a'); //  date.getHours() + ":" + date.getMinutes();

                this.commentID = this.model.get('comment_id');
                this.commentText = this.model.get('comment_text');
                this.timeStamp = time;
                this.userKey = this.model.get('user_key');
                this.lastName = this.model.get('last_name');
                this.firstName = this.model.get('first_name');

                this.$('#comment').html(this.commentText);
                this.$('#commentUserName').html(this.firstName + " " + this.lastName);
                this.$('#commentTimeStamp').html(this.timeStamp);

            },

            onResize: function () {
//                //console.log('CommentView:onResize');

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth !== winNewWidth || this.winHeight !== winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
//                //console.log('CommentView:resize');

            },
            remove: function () {
                //console.log('-------- CommentView:remove --------');

                // Turn off events
//                $(window).off("resize", this.onResize);
                this.$('#commentRow').off();

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });
