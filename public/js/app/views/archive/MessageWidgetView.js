define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/messageWidget', 'views/MessageCollectionView',
        'kendo/kendo.data', 'date'],
    //'kendo/kendo.binder'
    function (App, Backbone, Marionette, $, template, MessageCollectionView) {
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                messageList: '#message-list'
            },
            events: {
                'click #NewCommentButton': 'newCommentButtonClicked',
                'wheel': 'onMouseWheel',
                'click #viewMessageBoardButtonCollapse': 'onViewMessageBoardButtonCollapseClicked'
            },
            initialize: function () {
                //console.log('MessageWidgetView:initialize');

                var self = this;
                var app = App;

                this.viewModel = kendo.observable({
                    isChecked: false
                });

                //this.permitNum = App.model.get('permitNum');
                self.userKey = App.model.get('userKey');  //10268
                this.showMessagesUntilThisDate = moment().subtract(1, 'month').format('YYYY-MM-DD'); // 15 days ago at the moment

                this.messageID = 1513;

                this.tileViewMessageBoard = App.model.get('tileViewMessageBoard');

                // Subscribe to browser events
                $(window).on("resize", this.onResize);

            },
            onRender: function () {
                //console.log('MessageWidgetView:initialize');

                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                //console.log('MessageWidgetView:onShow');

                // Setup the containers
                this.resize();

                var self = this;

                if (this.tileViewMessageBoard === null) {
                    this.messageList.show(new MessageCollectionView());
                } else {
                    // Change to "+"
                    self.$('#viewMessageBoardIconCollapse').removeClass('fa-minus');
                    self.$('#viewMessageBoardIconCollapse').addClass('fa-plus');
                    self.$('.view-message-board-box').addClass('collapsed-box');
                }
            },
            onViewMessageBoardButtonCollapseClicked: function (e) {
                console.log('MessageWidgetView:onViewMessageBoardButtonCollapseClicked');
                var self = this;
                var tileViewMessageBoard = null;
                if (e.target.classList.toString().indexOf('plus') > -1 || e.target.innerHTML.indexOf('plus') > -1) {

                    // Change to "-"
                    //self.$('#viewMessageBoardIconCollapse').removeClass('fa-plus');
                    //self.$('#viewMessageBoardIconCollapse').addClass('fa-minus');
                    //self.$('.view-message-board-box').removeClass('collapsed-box');

                    // view
                    this.messageList.reset();
                    this.messageList.show(new MessageCollectionView());

                    // expanded
                    App.model.set('tileViewMessageBoard', tileViewMessageBoard);
                } else {

                    // Change to "+"
                    //self.$('#viewMessageBoardIconCollapse').removeClass('fa-minus');
                    //self.$('#viewMessageBoardIconCollapse').addClass('fa-plus');
                    //self.$('.view-message-board-stats-box').addClass('collapsed-box');

                    // collapsed
                    tileViewMessageBoard = "collapsed";
                    App.model.set('tileViewMessageBoard', tileViewMessageBoard);
                }

                this.tileViewMessageBoard = tileViewMessageBoard;
            },

            onMouseWheel: function (e) {
                //console.log('MessageWidgetView:onMouseWheel');
                e.preventDefault();
                var event = e.originalEvent;
                var d = event.wheelDelta || -event.detail;
                var change = ( d < 0 ? 1 : -1 ) * 32;
                $('html, body #dashboard-section').stop().animate({
                    scrollTop: "+=" + change
                }, 1);
            },
            newCommentButtonClicked: function (e) {
                //console.log('MessageWidgetView:newCommentButtonClicked');

                var self = this;

                this.createComment();

            },
            createComment: function () {

                var self = this;

                this.messageDataSource = new kendo.data.DataSource(
                    {
                    autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalMessage";
                            },
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "messageID",
                            fields: {
                                messageID: {editable: false, defaultValue: 0, type: "number"},
                                messageText: {editable: true, type: "string", validation: {required: false}},
                                messageDate: {editable: true, type: "date", validation: {required: true}}
                            }
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   MessageCollectionView:messageDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingMessageWidget"), true);
                    },
                    requestEnd: function () {

                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessageCollectionView:messageDataSource:error');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                        self.displayNoDataOverlay();

                        //App.modal.show(new ErrorView());

                    },
                    change: function () {

                    },
                    sort: {field: "messageID", dir: "desc"}
                });

                this.messagesCommentDataSource = new kendo.data.DataSource({
                    //autoBind: false,
                    type: "odata",
                    transport: {
                        read: {
                            url: function () {
                                return App.config.DataServiceURL + "/odata/PortalMessagesComment";
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                //console.log('MessageWidgetView:commentDataSource:update');
                                return App.config.DataServiceURL + "/odata/PortalMessagesComment" + "(" + data.commentID + ")";
                            }
                        },
                        create: {
                            url: function (data) {
                                //console.log('MessageWidgetView:commentDataSource:create');
                                return App.config.DataServiceURL + "/odata/PortalMessagesComment";
                            }
                            //dataType: "json"
                        },
                        destroy: {
                            url: function (data) {
                                //console.log('MessageWidgetView:commentDataSource:destroy');
                                return App.config.DataServiceURL + "/odata/PortalMessagesComment" + "(" + data.commentID + ")";
                            }
                        }
                    },
                    schema: {
                        data: "value",
                        total: function (data) {
                            return data['odata.count'];
                        },
                        model: {
                            id: "commentID",
                            //{
                            // "commentID":1018,
                            // "commentText":"Steve is awesome. I pay him for his thoughts.",
                            // "timeStamp":"2011-05-18T13:41:26.103",
                            // "messageID":1006,
                            // "userKey":10002,
                            //}
                            fields: {
                                commentID: {editable: false, defaultValue: 0, type: "number"},
                                messageID: {editable: false, defaultValue: 0, type: "number"},
                                commentText: {editable: true, type: "string", validation: {required: false}},
                                timeStamp: {editable: true, type: "date", validation: {required: true}},
                                userKey: {type: "number"}
                            }
                        }
                    },
                    sort: [{field: "messageID", dir: "desc"}, {field: "commentID", dir: "asc"}],
                    //sort: {field: "commentID", dir: "desc"},
                    serverFiltering: true,
                    serverSorting: true,
                    requestStart: function () {
                        //console.log('   MessageWidgetView:commentDataSource:requestStart');

                        kendo.ui.progress(self.$("#loadingMessageWidget"), true);
                    },
                    requestEnd: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('    MessageWidgetView:commentDataSource:requestEnd');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);
                        self.messageList.show(new MessageCollectionView());

                    },
                    error: function () {

                        // Do nothing if the request has been cancelled
                        if (this.cancelled === true)
                            return;

                        //console.log('   MessageWidgetView:commentDataSource:error');
                        kendo.ui.progress(self.$("#loadingMessageWidget"), false);

                        self.displayNoDataOverlay();

                    }
                });

                this.commentText = this.$('#newCommentText').val();

                if (this.commentText !== "") {

                    //var now = new Date();
                    var offset = (new Date()).getTimezoneOffset();
                    var now = new Date(moment().clone().subtract((offset), 'minutes'));
                    var today = moment(new Date()).clone().format('YYYY-MM-DD'); //subtract((offset), 'minutes')

                    console.log('MessageWidgetView:addComment:date:' + today + ":comment:" + this.commentText);
                    this.messageDataSource.filter({field: "messageDate", operator: "eq", value: today});
                    this.messageDataSource.fetch(function () {

                        var data = this.data();
                        var noMessage = true;
                        if (data.length > 0) {
                            $.each(this.data(), function (index, value) {
                                var message = value;
                                //var message = this.data()[0];
                                self.messageID = message.messageID;

                                var messageDate = moment(new Date(message.messageDate)).clone().format('YYYY-MM-DD');

                                console.log('MessageWidgetView:addComment:date:' + today + ":messageDate:" + messageDate);

                                if (today === messageDate) {

                                    self.messagesCommentDataSource.add({
                                        //commentID: {editable: false, defaultValue: 0, type: "number"},
                                        commentText: self.commentText,
                                        timeStamp: now,
                                        messageID: parseInt(self.messageID, 0),
                                        userKey: parseInt(self.userKey, 0)
                                        //lastName: self.lastName,
                                        //firstName: self.firstName
                                    });

                                    self.messagesCommentDataSource.sync();
                                    self.$('#newCommentText').val("");
                                    noMessage = false;
                                    return;
                                }
                            });

                            if (noMessage === true) {
                                alert('There is no message today, so your comment could not be added.');
                                kendo.ui.progress(self.$("#loadingMessageWidget"), false);
                            }
                        } else {
                            alert('There is no message today, so your comment could not be added.');
                            kendo.ui.progress(self.$("#loadingMessageWidget"), false);
                        }

                    });
                }

            },
            displayNoDataOverlay: function () {
                //console.log('MessageWidgetView:displayNoDataOverlay');

                // Hide the grid
                this.$('.tile-content').css('display', 'none');

                // Display the no data overlay
                if (this.$('.no-data-overlay-outer').length === 0)
                    $('<div class="no-data-overlay-outer"><div class="no-data-overlay-inner"><div class="no-data-overlay-message">No data available</div></div></div>').appendTo(this.$el);
            },
            onResize: function () {

                // Get height and width
                var winNewWidth = $(window).width();
                var winNewHeight = $(window).height();

                // Compare the new height and width with old one
                if (this.winWidth != winNewWidth || this.winHeight != winNewHeight) {
                    window.clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = window.setTimeout(this.resize, 10);
                }

                //Update the width and height
                this.winWidth = winNewWidth;
                this.winHeight = winNewHeight;
            },
            resize: function () {
                //console.log('MessageWidgetView:resize');

            },
            remove: function () {
                //console.log('-------- MessageWidgetView:remove --------');

                // Turn off events
                $(window).off("resize", this.onResize);

                // Remove view events setup in events configuration
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });