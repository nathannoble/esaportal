define(['App', 'backbone', 'marionette', 'jquery', 'hbs!templates/payrollProcessingTable',
    'views/PayrollProcessingGridView','views/SubHeaderView'],
    function (App, Backbone, Marionette, $, template, PayrollProcessingGridView,SubHeaderView) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend({
            template: template,
            regions: {
                subHeaderRegion: "#sub-header",
                payrollProcessingRegion: "#payroll-processing-region"
            },
            initialize: function () {

                //console.log('PayrollProcessingTableView:initialize');

                _.bindAll(this);

            },
            onRender: function () {

                //console.log('PayrollProcessingTableView:onRender');
                // get rid of that pesky wrapping-div
                // assumes 1 child element.
                this.$el = this.$el.children();
                this.setElement(this.$el);
            },
            onShow: function () {
                console.log("PayrollProcessingTableView:onShow");

                var self = this;

                this.userId = App.model.get('userId');
                if (this.userId === null) {
                    // Go back to home page
                    App.router.navigate('index', {trigger: true});
                } else {

                    // set default time card date range
                    this.day = (new Date()).getDate();

                    // Last half of last month is default
                    var start = moment().subtract(1, 'month').startOf('month').add(15,'days');
                    var end = moment().subtract(1, 'month').endOf('month');
                    if (this.day > 15) {

                        // First half of this month is default
                        start = moment().startOf('month');
                        end = moment().startOf('month').add(15, 'days');
                    }

                    this.dateFilter = start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY');
                    App.model.set('selectedDateFilter', this.dateFilter);

                    this.listenTo(App.vent, App.Events.ModelEvents.SelectedDateFilterChanged, this.onSelectedDateFilterChanged);

                    this.subHeaderRegion.show(new SubHeaderView({
                        majorTitle: "Payroll Processing",
                        minorTitle: "",
                        page: "Payroll Processing",
                        showDateFilter: true,
                        showStatusFilter: false,
                        showEmpStatusFilter: false,
                        showClientReportFilter: false,
                        showViewsFilter: false,
                        showCompanyFilter: false,
                        showClientFilter: false,
                        showTeamLeaderFilter:false,
                        showPayrollProcessingDate:true
                    }));

                    this.payrollProcessingRegion.show(new PayrollProcessingGridView());
                }

            },
            onSelectedDateFilterChanged: function () {
                console.log('PayrollProcessingTableView:onSelectedDateFilterChanged');
                this.payrollProcessingRegion.reset();
                this.payrollProcessingRegion.show(new PayrollProcessingGridView());
            },
            resize: function () {
                //console.log('PayrollProcessingTableView:resize');

                if (this.payrollProcessingRegion.currentView)
                    this.payrollProcessingRegion.currentView.resize();

            },
            remove: function () {
                //console.log("--------PayrollProcessingTableView:remove --------");

                //this.saveButtonClicked();

                var self = this;

                // Remove browser events
                $(window).off("resize", this.onResize);

                // Remove all the event handlers
                this.undelegateEvents();

                Backbone.View.prototype.remove.apply(this, arguments);
            }
        });
    });