//http://mrbool.com/backbone-js-router/28001
define(['backbone', 'marionette'], function(Backbone, Marionette) {
    return Backbone.Marionette.AppRouter.extend({
        //"index" must be a method in AppRouter's controller
        appRoutes: {
            "": "welcome",
            "welcome": "welcome",
            "dashboard": "dashboard",
            "index": "index",
            "home": "home",
            "checkEmail": "checkEmail",
            "setPassword": "setPassword",
            "setPassword?*queryString": "setPassword",
            //"test": "test",
            "views": "views",
            'clientProfile': "clientProfile",
            'companyProfile': "companyProfile",
            'employeeProfile': "employeeProfile",
            'userProfile': "userProfile",
            "myTimecard": "myTimecard",
            "openTimers": "openTimers",
            "clientReports": "clientReports",
            'plans': "plans",
            'employees': "employees",
            "statsTimecard": "statsTimecard",
            'statsTimecardNoDateChange': "statsTimecardNoDateChange",
            "companies": "companies",
            'calendar': "calendar",
            'myStats': "myStats",
            'users': "users",
            "vendors": "vendors",
            "messages": "messages",
            "messagesfull": "messagesfull",
            'tipsTricks': "tipsTricks",
            //'values': "values",
            'detailsAdmin': "detailsAdmin",
            'taskBreakdown': "taskBreakdown",
            'editClient': "editClient",
            'editCompany': "editCompany",
            'addNewClient': "addNewClient",
            'addNewCompany': "addNewCompany",
            'editEmployee': "editEmployee",
            'addNewEmployee': "addNewEmployee",
            'editTimecard': "editTimecard",
            'editTimecardIntTimerID': "editTimecardIntTimerID",
            'payrollProcessing': "payrollProcessing",
            'team': "team",
            'docs': "docs",
            'news': "news",
            'esamedia': "esamedia",
            //'socialmedia': "socialmedia",
            'adminIndustryNewsData': "adminIndustryNewsData",
            'adminAlertsData': "adminAlertsData",
            'adminMessagesData': "adminMessagesData",
            'adminChatData': "adminChatData",
            //'adminSocialMediaData': "adminSocialMediaData",
            'adminESAMediaData': "adminESAMediaData",
            'adminStaticData': "adminStaticData",
            'adminBioData': "adminBioData",
            'admin': "admin",
            'empPodcastData': "empPodcastData"
        },
        // GA - To track our application routes
        initialize: function() {
            this.bind('route', this._pageView);
        },
        _pageView: function() {
            var path = Backbone.history.getFragment();
            //ga('send', 'pageview', {page: "phone-/" + path});
        }
    });
});