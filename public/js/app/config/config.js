require.config({
    baseUrl:"./js/app",
    // 3rd party script alias names (Easier to type "jquery" than "libs/jquery, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths:{
        // Core Libraries
        "jquery":"../libs/jquery-2.1.4.min",
        //"jquery":"../libs/jquery-3.7.1.min",
        //"jquery-migrate":"../libs/jquery-migrate-3.5.2.min",
        //"jquery":"../libs/jquery-2.2.4.min",
        "jqueryui":"../libs/jquery-ui-1.11.4.min",
        //"jquerymobile":"../libs/jquery.mobile-1.4.4.min",

        "underscore":"../libs/lodash",
        "backbone":"../libs/backbone-min",
        "marionette":"../libs/backbone.marionette",
        "handlebars":"../libs/handlebars",
        "hbs":"../libs/hbs",
        "i18nprecompile":"../libs/i18nprecompile",
        "json2":"../libs/json2",
        //"jasmine": "../libs/jasmine",
        //"jasmine-html": "../libs/jasmine-html",
        //"ext": "../libs/ext-all",
       
        // Kendo build from 2014
        "kendo": '../libs/kendo',

        // Kendo build from 2018
        // "kendo.all.min": '../libs/kendo/kendo.all.min',
        // "kendo": '../libs/kendo/kendo.all.min',

        // "kendo/kendo.data": '../libs/kendo/kendo.data.min',
        // "kendo/kendo.grid": '../libs/kendo/kendo.grid.min',
        // "kendo/kendo.upload": '../libs/kendo/kendo.upload.min',
        // "kendo/kendo.numerictextbox": '../libs/kendo/kendo.numerictextbox.min',
        // "kendo/kendo.maskedtextbox": '../libs/kendo/kendo.maskedtextbox.min',
        // "kendo/kendo.combobox": '../libs/kendo/kendo.combobox.min',
        // "kendo/kendo.datepicker": '../libs/kendo/kendo.datepicker.min',
        // "kendo/kendo.datetimepicker": '../libs/kendo/kendo.datetimepicker.min',
        // "kendo/kendo.binder": '../libs/kendo/kendo.binder.min',
       
        //"jszip": '../libs/kendo/jszip',

        //"kendo.dropdowntreeview": "../libs/plugins/kendo.dropdowntreeview",

        // Plugins
        "backbone.validateAll":"../libs/plugins/Backbone.validateAll",
        "bootstrap":"../libs/plugins/bootstrap-3.3.4.min",
        //"text":"../libs/plugins/text",
        //"jasminejquery": "../libs/plugins/jasmine-jquery",
        //"jquery.iosslider":"../libs/plugins/jquery.iosslider",
        "jquery.cookie":"../libs/plugins/jquery.cookie",
        //"imagesLoaded":  "../libs/plugins/imagesloaded.pkgd.min",
        "jquery.easing": "../libs/plugins/jquery.easing-1.3",
        //"jquery.easypiechart": "../libs/plugins/jquery.easypiechart",
        //"jquery.circliful": "../libs/plugins/jquery.circliful.min",
        //"jquery.scrollTo": "../libs/plugins/jquery.scrollTo",
        //"jquery.slimScroll": "../libs/plugins/slimScroll/jquery.slimscroll.min",
        //
        //"jquery.layout":"../libs/plugins/jquery.layout-latest.min",
        //"jquery.tmpl":"../libs/plugins/jquery.tmpl",
        //"jquery.ui.grid":"../libs/plugins/jquery.ui.grid",
        //"jquery.i18n.properties":"../libs/plugins/jquery.i18n.properties-min-1.0.9",
        //"jquery.contextMenu":"../libs/plugins/jquery.contextMenu",
        "date":"../libs/plugins/date", // TODO: Cleanup
        //"canvg":"../libs/plugins/canvg",
        //"html2canvas": "../libs/html2canvas",
        "jquery.dateFormat": "../libs/plugins/jquery.dateFormat",

        //Added for ESA dashboard/charts
        //"fastclick": "../libs/plugins/fastclick/fastclick.min",
        "chartjs": "../libs/plugins/chartjs/Chart.bundle.min",
        "chartjs_2.3.0": "../libs/plugins/chartjs/Chart_2.3.0.bundle.min",
        "chartjs-plugin-datalabels": "../libs/plugins/chartjs/chartjs-plugin-datalabels",

        "adminLTE": "../libs/plugins/AdminLTE/app-2.3.0.min",

        "moment": "../libs/plugins/daterangepicker/moment",
        "daterangepicker": "../libs/plugins/daterangepicker/daterangepicker",
        //"moment-timezone": "../libs/plugins/moment-timezone.min"
        "xlsx": "../libs/plugins/xlsx.full.min"
    },
    // Sets the configuration for your third party scripts that are not AMD compatible
    shim:{
        // Twitter Bootstrap jQuery plugins
        "bootstrap":["jquery"],

        // jQueryUI
        "jqueryui":["jquery"],

        // jQuery mobile
        "jquerymobile":["jqueryui"],

        // JQuery Plugins
        //"jquery.cookie":["jquery"],
        //"jquery.iosslider":["jquery"],
        //
        //"jquery.easing":["jquery"],
        //"jquery.easypiechart":["jquery","jquery.easing"],
        //"jquery.circliful": ["jquery"],
        //"jquery.scrollTo": ["jquery"],

//        "jquery.XDomainRequest":["jquery"],
//
//        "imagesLoaded":{
//            "deps":["jquery"],
//            "exports": "imagesLoaded"
//        },

        // Backbone
        "backbone":{
            // Depends on underscore/lodash and jQuery
            "deps":["underscore", "jquery"],
            // Exports the global window.Backbone object
            "exports":"Backbone"
        },
        // Marionette
        "marionette":{
            "deps":["underscore", "backbone", "jquery"],
            "exports":"Marionette"
        },
        // Handlebars
        "handlebars":{
            "exports":"Handlebars"
        },
        // Backbone.validateAll plugin that depends on Backbone
        "backbone.validateAll":["backbone"],

        // Kendo
        "kendo/kendo.core": {
            deps: ["jquery"]
        }
        //"kendo.dropdowntreeview":{
        //    "deps": ["kendo/kendo.treeview", "kendo/kendo.dropdownlist", "jquery"],
        //    "exports": "kendo.dropdowntreeview"
        //},
        
        //"jasmine": {
        //    // Exports the global 'window.jasmine' object
        //    "exports": "jasmine"
        //},
        //
        //"jasmine-html": {
        //    "deps": ["jasmine"],
        //    "exports": "jasmine"
        //}

//        "lazyLoader":{
//            "exports": "lazyLoader"
//        }

    },
    // hbs config - must duplicate in Gruntfile.js Require build
    hbs: {
        templateExtension: "html",
        helperDirectory: "templates/helpers/",
        i18nDirectory: "templates/i18n/",

        compileOptions: {}        // options object which is passed to Handlebars compiler
    }
});