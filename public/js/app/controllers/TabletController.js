define(['App', 'backbone', 'marionette', 'views/HeaderView', 'views/AsideView', 'views/FooterView', 'views/HomeView','views/PasswordView',
        'views/ClientProfileLayoutView', 'views/CompanyProfileLayoutView', 'views/EmployeeProfileLayoutView',
        'views/ProgressPlanTableView', 'views/PortalEmployeeTableView', 'views/ProgressCompanyTableView',
        'views/ESATileLayoutView', 'views/ESAStatsLayoutView', 'views/PortalCalendarHolidayView',
        'views/PortalUserTableView', 'views/PortalVendorTableView', 'views/PortalMessageTableView',
        'views/PortalTipTableView', 'views/PortalValueTableView', 'views/PortalAdminIndustryNewsDataTableView',
        'views/PortalAdminMessagesDataTableView', 'views/PortalAdminChatDataTableView',
        'views/PortalAdminAlertsDataTableView', 'views/PortalAdminSocialMediaDataTableView', 'views/PortalAdminESAMediaDataTableView',
        'views/PortalAdminStaticDataTableView','views/PortalAdminBioDataTableView',
        'views/PortalTimeCardDetailTableView', 'views/MyTimeCardDetailLayoutView', 'views/AdminScoreBoardView', 'views/ProgressClientDataTableView',
        'views/ProgressClientContactTableView', 'views/PortalTimerTaskBreakdownGridView', 'views/ProgressClientView','views/ProgressCompanyView',
        'views/ProgressEmployeeView','views/OpenTimersTableView','views/TimeCardView', 'views/PayrollProcessingTableView',
        'views/TeamLayoutView','views/DocsLayoutView', 'views/IndustryNewsLayoutView',
        'views/ESAMediaLayoutView','views/SocialMediaLayoutView', 'views/MessagesLayoutView','views/MessagesFullLayoutView',
        'views/WelcomeLayoutView','views/PortalAdminGeneralView'
    ],
    function (App, Backbone, Marionette, HeaderView, AsideView, FooterView, HomeView,PasswordView,
              ClientProfileLayoutView, CompanyProfileLayoutView, EmployeeProfileLayoutView, ProgressPlanTableView, PortalEmployeeTableView,
              ProgressCompanyTableView, ESATileLayoutView, ESAStatsLayoutView, PortalCalendarHolidayView,
              PortalUserTableView, PortalVendorTableView, PortalMessageTableView, PortalTipTableView, PortalValueTableView,
              PortalAdminIndustryNewsDataTableView,
              PortalAdminMessagesDataTableView, PortalAdminChatDataTableView,
              PortalAdminAlertsDataTableView, PortalAdminSocialMediaDataTableView,PortalAdminESAMediaDataTableView,
              PortalAdminStaticDataTableView, PortalAdminBioDataTableView,
              PortalTimeCardDetailTableView, MyTimeCardDetailLayoutView, AdminScoreBoardView, ProgressClientDataTableView,
              ProgressClientContactTableView, PortalTimerTaskBreakdownGridView, ProgressClientView, ProgressCompanyView, ProgressEmployeeView,
              OpenTimersTableView,TimeCardView,PayrollProcessingTableView,
              TeamLayoutView,DocsLayoutView,IndustryNewsLayoutView,
              ESAMediaLayoutView,SocialMediaLayoutView,MessagesLayoutView,MessagesFullLayoutView,
              WelcomeLayoutView,PortalAdminGeneralView
    ) {
        return Backbone.Marionette.Controller.extend({
            initialize: function (options) {

                $("body").css('overflow', '');
                $("#esa").removeAttr('style');

                this.userType = App.model.get('userType');

                if (App.mobile) {
                    $("#loading-error-region").empty();
                    document.ontouchmove = function (e) {
                        return true;
                    };
                }

                // Setup the header and footer
                $("#loading-error-region").empty();
                App.headerRegion.show(new HeaderView());
                App.asideRegion.show(new AsideView());
                App.footerRegion.show(new FooterView());


            },
            removeLayoutClasses: function () {
                $("#main-region").removeClass("content-wrapper");
                // $("#main-region").removeClass("container-fluid");
                // $("#main-region").removeClass("container-fluid");
                // $("#main-region").removeClass("container-full-map");
                // $("#main-region").removeClass("container-padded-home");
                // $("#main-region").removeClass("container-fluid-tight");
                // $("#main-region").removeClass("container-risk-layout");
                $("#main-region").height(0);
            },
            resetMarkupContainers: function () {

                $("body").css('overflow-x', '');
                $("body").css('overflow-y', '');
                $("body").css('overflow', '');
                $("html").removeAttr('style');
                $("#esa").removeAttr('style');

                $("html,body").scrollTop(0);
            },
            //gets mapped to in AppRouter's appRoutes
            index: function () {
                console.log('TabletController:index');

                this.compareVersions();

                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();
                App.asideRegion.reset();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                $("body").css('overflow-y', 'auto');
                $("body").css('overflow-x', 'hidden');
                $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                $("#esa").css('background-color', '#FFFFFF');

                // Reset the layout
                this.removeLayoutClasses();

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new HomeView());
                }

            },
            compareVersions: function () {

                // Compare JS App (this app) version with .NET API version.
                console.log('TabletController:compareVersions:AppVersion:' + App.config.AppVersion);

                this.userType = App.model.get('userType');

                var apiVersion = 0;
                var app = App;

                $.ajax({
                    type: 'GET',
                    contentType: "application/json", //; charset=utf-8",
                    url: App.config.DataServiceURL + "/rest/GetVersion/",
                    dataType: 'json',
                    success: function (response) {
                        apiVersion = parseInt(response,0);
                        console.log("TabletController:compareVersions:apiVersion:" + apiVersion);

                        $.ajax({
                            type: 'GET',
                            url: 'config.json',
                            dataType: 'json',
                            success: function (data) {
                                app.config.appVersion = data.AppVersion;
                                // If the API version is higher, force refresh
                                if (apiVersion > app.config.AppVersion) {
                                    console.log("TabletController:compareVersions:update");
                                    window.location.reload(true);
                                }
                            },
                            error: function(){
                                console.log("TabletController:compareVersions:config.json access error");
                            },
                            data: {},
                            async: false
                        });

                    },
                    error: function (err) {
                        console.log("TabletController:compareVersions:error:" + err);
                    }
                });

            },
            checkEmail: function () {
                console.log('TabletController:checkEmail');

                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();
                App.asideRegion.reset();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                $("body").css('overflow-y', 'auto');
                $("body").css('overflow-x', 'hidden');
                $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                $("#esa").css('background-color', '#FFFFFF');

                // Reset the layout
                this.removeLayoutClasses();

                App.mainRegion.show(new HomeView({type: "checkEmail"})); //HomeView());

            },
            setPassword: function () {
                console.log('TabletController:setPassword');

                this.compareVersions();

                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();
                App.asideRegion.reset();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                $("body").css('overflow-y', 'auto');
                $("body").css('overflow-x', 'hidden');
                $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                $("#esa").css('background-color', '#FFFFFF');

                // Reset the layout
                this.removeLayoutClasses();

                App.mainRegion.show(new PasswordView());

            },
            home: function () {
                console.log('TabletController:home');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                $("#loading-error-region").empty();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                App.mainRegion.reset();
                App.headerRegion.reset();
                App.asideRegion.reset();
                App.footerRegion.reset();

                App.headerRegion.show(new HeaderView());
                App.asideRegion.show(new AsideView());
                App.footerRegion.show(new FooterView());

                //App.mainRegion.show(new ESATileLayoutView());

            },
            dashboard: function () {
                console.log('TabletController:dashboard');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                $("#loading-error-region").empty();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                //App.headerRegion.show(new HeaderView());
                //App.mainRegion.reset();
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESATileLayoutView());
                }

            },
            welcome: function () {
                console.log('TabletController:welcome');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                $("#loading-error-region").empty();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new WelcomeLayoutView());
                }

            },
            test: function () {
                console.log('TabletController:test');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                //$("#loading-error-region").empty();
                //App.headerRegion.show(new HeaderView());
                //App.asideRegion.show(new AsideView());

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");
                //App.asideRegion.reset();
                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();

                App.headerRegion.show(new HeaderView());
                //App.asideRegion.show(new AsideView());
                //App.mainRegion.show(new ESATileLayoutTestView());

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            views: function () {
                console.log('TabletController:views');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            team: function () {
                console.log('TabletController:team');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TeamLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            docs: function () {
                console.log('TabletController:docs');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                App.mainRegion.show(new DocsLayoutView());

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            news: function () {
                console.log('TabletController:news');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new IndustryNewsLayoutView());
                }
            },
            esamedia: function () {
                console.log('TabletController:esamedia');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESAMediaLayoutView());
                }
            },
            socialmedia: function () {
                console.log('TabletController:socialmedia');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new SocialMediaLayoutView());
                }
            },
            messagesfull: function () {
                console.log('TabletController:messagesfull');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MessagesFullLayoutView());
                }
            },
            messages: function () {
                console.log('TabletController:messages');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Messages');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MessagesLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            clientProfile: function () {
                console.log('TabletController:clientProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ClientProfileLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            companyProfile: function () {
                console.log('TabletController:companyProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new CompanyProfileLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            employeeProfile: function () {
                console.log('TabletController:employeeProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new EmployeeProfileLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            userProfile: function () {
                console.log('TabletController:userProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new EmployeeProfileLayoutView({type: "User"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            myTimecard: function () {
                console.log('TabletController:myTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MyTimeCardDetailLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            openTimers: function () {
                console.log('TabletController:openTimers');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new OpenTimersTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            clientReports: function () {
                console.log('TabletController:clientReports');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientContactTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            plans: function () {
                console.log('TabletController:plans');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressPlanTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            employees: function () {
                console.log('TabletController:employees');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalEmployeeTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            statsTimecard: function () {
                console.log('TabletController:statsTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimeCardDetailTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            statsTimecardNoDateChange: function (options) {
                console.log('TabletController:statsTimecardNoDateChange');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimeCardDetailTableView({ignoreDateChange: true}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            companies: function () {
                console.log('TabletController:companies');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressCompanyTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            calendar: function () {
                console.log('TabletController:calendar');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalCalendarHolidayView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            myStats: function () {
                console.log('TabletController:myStats');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESAStatsLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            users: function () {
                console.log('TabletController:users');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalUserTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            vendors: function () {
                console.log('TabletController:vendors');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalVendorTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            tipsTricks: function () {
                console.log('TabletController:tipsTricks');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTipTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminMessagesData: function () {
                console.log('TabletController:adminMessagesData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminMessagesDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminChatData: function () {
                console.log('TabletController:adminChatData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminChatDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminAlertsData: function () {
                console.log('TabletController:adminAlertsData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminAlertsDataTableView());
                }
            },
            adminESAMediaData: function () {
                console.log('TabletController:adminESAMediaData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminESAMediaDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminSocialMediaData: function () {
                console.log('TabletController:adminSocialMediaData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminSocialMediaDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminIndustryNewsData: function () {
                console.log('TabletController:adminIndustryNewsData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminIndustryNewsDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminStaticData: function () {
                console.log('TabletController:adminStaticData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminStaticDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminBioData: function () {
                console.log('TabletController:adminBioData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminBioDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            admin: function () {
                console.log('TabletController:admin');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminGeneralView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            detailsAdmin: function () {
                console.log('TabletController:detailsAdmin');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new AdminScoreBoardView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            taskBreakdown: function () {
                console.log('TabletController:taskBreakdown');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimerTaskBreakdownGridView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editCompany: function () {
                console.log('TabletController:editCompany');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressCompanyView({type: "Edit Company"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            addNewCompany: function () {
                console.log('TabletController:addNewCompany');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressCompanyView({type: "New Company"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editClient: function () {
                console.log('TabletController:editClient');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientView({type: "Edit Client"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            addNewClient: function () {
                console.log('TabletController:addNewClient');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientView({type: "New Client"}));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editEmployee: function () {
                console.log('TabletController:editEmployee');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmployeeView({type: "Edit Employee"}));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            addNewEmployee: function () {
                console.log('TabletController:addNewEmployee');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmployeeView({type: "New Employee"}));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editTimecard: function () {
                console.log('TabletController:editTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TimeCardView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editTimecardIntTimerID: function () {
                console.log('TabletController:editTimecardIntTimerID');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TimeCardView({
                        type: "intTimerID"
                    }));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            payrollProcessing: function () {
                console.log('TabletController:payrollProcessing');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PayrollProcessingTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            empPodcastData : function () {
                console.log('TabletController:empPodcastData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmpMediaDataTableView());
                }

            },
            updateNavBar: function (route) {

                //console.log('TabletController:updateNavBar:route:' + route);
                if (route === '')
                    route = 'index';

                if (route.indexOf('?') > -1) {
                    route = route.slice(0, route.indexOf('?'));
                }
                $el = $('#nav-' + route);

                // If current route is highlighted, we're done.

                if (route != 'esa_app') {
                    if ($el.hasClass('current_page_item')) {
                        return;
                    } else {
                        // Unhighlight active tab.
                        $('#main-menu li.current_page_item').removeClass('current_page_item');
                        // Highlight active page tab.
                        $el.addClass('current_page_item');
                    }
                }
                else {
                    if ($el.hasClass('current_sub_page_item')) {
                        return;
                    } else {
                        // Unhighlight active tab.
                        $('#main-menu li.current_sub_page_item').removeClass('current_sub_page_item');
                        $('#main-menu li.current_page_item').removeClass('current_page_item');

                        // Highlight active page tab.
                        $el.addClass('current_sub_page_item');
                    }
                }

                // Highlight the menu if applicable...
                //console.log(route);
                //route === 'dashboard' || route === 'projectfilters' ||
                if (route === 'esa_app') {
                    if ($('#nav-programmenu').hasClass('current_page_item'))
                        return;

                    $('#nav-programmenu').addClass('current_page_item');
                    $('#nav-programmenu-text').addClass('current_page_item_text');

                }
                else {
                    $('#nav-programmenu').removeClass('current_page_item');
                }
            },
            parseQueryString: function (queryString) {
                var params = {};
                if (queryString) {
                    _.each(
                        _.map(decodeURI(queryString).split(/&/g), function (el, i) {
                            var aux = el.split('='), o = {};
                            if (aux.length >= 1) {
                                var val;
                                if (aux.length == 2)
                                    val = aux[1];
                                o[aux[0]] = val;
                            }
                            return o;
                        }),
                        function (o) {
                            _.extend(params, o);
                        }
                    );
                }
                return params;
            },
            startApp: function (e) {

                var self = this;

                console.log('TabletController:userId:' + this.userId);

                var thisView = this;
            }
        });
    });