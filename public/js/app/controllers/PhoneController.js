define(['App', 'backbone', 'marionette', 'views/HeaderView', 'views/AsideView', 'views/FooterView', 'views/HomeView','views/PasswordView',
        'views/ClientProfileLayoutView', 'views/CompanyProfileLayoutView', 'views/EmployeeProfileLayoutView',
        'views/ProgressPlanTableView', 'views/PortalEmployeeTableView', 'views/ProgressCompanyTableView',
        'views/ESATileLayoutView', 'views/ESAStatsLayoutView', 'views/PortalCalendarHolidayView',
        'views/PortalUserTableView', 'views/PortalVendorTableView', 'views/PortalMessageTableView',
        'views/PortalTipTableView', 'views/PortalValueTableView', 'views/PortalAdminIndustryNewsDataTableView',
        'views/PortalAdminMessagesDataTableView', 'views/PortalAdminChatDataTableView',
        'views/PortalAdminAlertsDataTableView', 'views/PortalAdminSocialMediaDataTableView', 'views/PortalAdminESAMediaDataTableView',
        'views/PortalAdminStaticDataTableView','views/PortalAdminBioDataTableView',
        'views/PortalTimeCardDetailTableView', 'views/MyTimeCardDetailLayoutView', 'views/AdminScoreBoardView', 'views/ProgressClientDataTableView',
        'views/ProgressClientContactTableView', 'views/PortalTimerTaskBreakdownGridView', 'views/ProgressClientView', 'views/ProgressCompanyView',
        'views/ProgressEmployeeView','views/OpenTimersTableView','views/TimeCardView', 'views/PayrollProcessingTableView',
        'views/TeamLayoutView','views/DocsLayoutView', 'views/IndustryNewsLayoutView',
        'views/ESAMediaLayoutView','views/SocialMediaLayoutView', 'views/MessagesLayoutView','views/MessagesFullLayoutView',
        'views/WelcomeLayoutView','views/PortalAdminGeneralView'
    ],
    function (App, Backbone, Marionette, HeaderView, AsideView, FooterView, HomeView,PasswordView,
              ClientProfileLayoutView, CompanyProfileLayoutView, EmployeeProfileLayoutView, ProgressPlanTableView, PortalEmployeeTableView,
              ProgressCompanyTableView, ESATileLayoutView, ESAStatsLayoutView, PortalCalendarHolidayView,
              PortalUserTableView, PortalVendorTableView, PortalMessageTableView, PortalTipTableView, PortalValueTableView,
              PortalAdminIndustryNewsDataTableView,
              PortalAdminMessagesDataTableView, PortalAdminChatDataTableView,
              PortalAdminAlertsDataTableView, PortalAdminSocialMediaDataTableView,PortalAdminESAMediaDataTableView,
              PortalAdminStaticDataTableView, PortalAdminBioDataTableView,
              PortalTimeCardDetailTableView, MyTimeCardDetailLayoutView, AdminScoreBoardView, ProgressClientDataTableView,
              ProgressClientContactTableView, PortalTimerTaskBreakdownGridView, ProgressClientView, ProgressEmployeeView,
              OpenTimersTableView,TimeCardView,PayrollProcessingTableView,
              TeamLayoutView,DocsLayoutView,IndustryNewsLayoutView,
              ESAMediaLayoutView,SocialMediaLayoutView,MessagesLayoutView,MessagesFullLayoutView,
              WelcomeLayoutView,PortalAdminGeneralView
    ) {
        return Backbone.Marionette.Controller.extend({
            initialize:function (options) {

                $("body").css('overflow', '');
                $("#esa").removeAttr('style');

                this.userType = App.model.get('userType');

                if (App.mobile) {
                    $("#loading-error-region").empty();
                    document.ontouchmove = function (e) {
                        return true;
                    };
                }

                // Setup the header and footer
                $("#loading-error-region").empty();
                App.headerRegion.show(new HeaderView());
                App.asideRegion.show(new AsideView());
                App.footerRegion.show(new FooterView());

            },
            removeLayoutClasses: function () {
                $("#main-region").removeClass("content-wrapper");
                // $("#main-region").removeClass("container-fluid");
                // $("#main-region").removeClass("container-fluid");
                // $("#main-region").removeClass("container-full-map");
                // $("#main-region").removeClass("container-padded-home");
                // $("#main-region").removeClass("container-fluid-tight");
                // $("#main-region").removeClass("container-risk-layout");
                $("#main-region").height(0);
            },
            //gets mapped to in AppRouter's appRoutes
            index:function () {
                console.log('PhoneController:index');

                this.compareVersions();

                // Start Crumb
                //App.model.startBreadcrumb('Home');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                App.asideRegion.reset();
                App.headerRegion.reset();
                App.footerRegion.reset();

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new HomeView());
                }
                App.mainRegion.show(new HomeView());

            },
            compareVersions: function () {

                // Compare JS App (this app) version with .NET API version.
                console.log('PhoneController:compareVersions:AppVersion:' + App.config.AppVersion);

                this.userType = App.model.get('userType');

                var apiVersion = 0;
                var app = App;

                $.ajax({
                    type: 'GET',
                    contentType: "application/json", //; charset=utf-8",
                    url: App.config.DataServiceURL + "/rest/GetVersion/",
                    dataType: 'json',
                    success: function (response) {
                        apiVersion = parseInt(response,0);
                        console.log("PhoneController:compareVersions:apiVersion:" + apiVersion);

                        $.ajax({
                            type: 'GET',
                            url: 'config.json',
                            dataType: 'json',
                            success: function (data) {
                                app.config.appVersion = data.AppVersion;
                                // If the API version is higher, force refresh
                                if (apiVersion > app.config.AppVersion) {
                                    console.log("PhoneController:compareVersions:update");
                                    window.location.reload(true);
                                }
                            },
                            error: function(){
                                console.log("PhoneController:compareVersions:config.json access error");
                            },
                            data: {},
                            async: false
                        });

                    },
                    error: function (err) {
                        console.log("PhoneController:compareVersions:error:" + err);
                    }
                });

            },
            checkEmail: function () {
                console.log('PhoneController:checkEmail');

                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();
                App.asideRegion.reset();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                $("body").css('overflow-y', 'auto');
                $("body").css('overflow-x', 'hidden');
                $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                $("#esa").css('background-color', '#FFFFFF');

                // Reset the layout
                this.removeLayoutClasses();

                App.mainRegion.show(new HomeView({type: "checkEmail"})); //HomeView());

            },
            setPassword: function () {
                console.log('PhoneController:setPassword');

                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();
                App.asideRegion.reset();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                $("body").css('overflow-y', 'auto');
                $("body").css('overflow-x', 'hidden');
                $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                $("#esa").css('background-color', '#FFFFFF');

                // Reset the layout
                this.removeLayoutClasses();

                App.mainRegion.show(new PasswordView());

            },
            resetMarkupContainers: function () {

                $("body").css('overflow-x', '');
                $("body").css('overflow-y', '');
                $("body").css('overflow', '');
                $("html").removeAttr('style');
                $("#esa").removeAttr('style');

                $("html,body").scrollTop(0);
            },
            home: function () {
                console.log('PhoneController:home');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                $("#loading-error-region").empty();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                App.asideRegion.reset();
                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();

                App.headerRegion.show(new HeaderView());
                App.asideRegion.show(new AsideView());
                App.footerRegion.show(new FooterView());

                //App.mainRegion.show(new ESATileLayoutView());

            },
            dashboard: function () {
                console.log('PhoneController:dashboard');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                $("#loading-error-region").empty();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESATileLayoutView());
                }

            },
            welcome: function () {
                console.log('PhoneController:welcome');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new WelcomeLayoutView());
                }

            },
            views: function () {
                console.log('PhoneController:views');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                
                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientDataTableView());
                }
                
            },
            team: function () {
                console.log('PhoneController:team');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TeamLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            docs: function () {
                console.log('PhoneController:docs');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                App.mainRegion.show(new DocsLayoutView());

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            news: function () {
                console.log('PhoneController:news');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new IndustryNewsLayoutView());
                }
            },
            esamedia: function () {
                console.log('PhoneController:esamedia');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESAMediaLayoutView());
                }
            },
            socialmedia: function () {
                console.log('PhoneController:socialmedia');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new SocialMediaLayoutView());
                }
            },
            messagesfull: function () {
                console.log('PhoneController:messagesfull');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MessagesFullLayoutView());
                }
            },
            messages: function () {
                console.log('PhoneController:messages');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MessagesLayoutView());
                }
            },
            clientProfile: function () {
                console.log('PhoneController:clientProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Client Profile');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ClientProfileLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            companyProfile: function () {
                console.log('PhoneController:companyProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Company Profile');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new CompanyProfileLayoutView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            employeeProfile: function () {
                console.log('PhoneController:employeeProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Employee Profile');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new EmployeeProfileLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            userProfile: function () {
                console.log('PhoneController:userProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('User Profile');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new EmployeeProfileLayoutView({type: "User"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            myTimecard: function () {
                console.log('PhoneController:myTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('My Time Card');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MyTimeCardDetailLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            openTimers: function () {
                console.log('PhoneController:openTimers');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new OpenTimersTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            clientReports: function () {
                console.log('PhoneController:clientReports');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Client Contact Info');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientContactTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            plans: function () {
                console.log('PhoneController:plans');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Plans');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressPlanTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            employees: function () {
                console.log('PhoneController:employees');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Employees');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);


                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalEmployeeTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            statsTimecard: function () {
                console.log('PhoneController:statsTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimeCardDetailTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            statsTimecardNoDateChange: function (options) {
                console.log('PhoneController:statsTimecardNoDateChange');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimeCardDetailTableView({ignoreDateChange: true}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            companies: function () {
                console.log('PhoneController:companies');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Companies');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressCompanyTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            calendar: function () {
                console.log('PhoneController:calendar');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Calendar/Holidays');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalCalendarHolidayView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            myStats: function () {
                console.log('PhoneController:myStats');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('My Stats');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESAStatsLayoutView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            users: function () {
                console.log('PhoneController:users');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Users');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalUserTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            vendors: function () {
                console.log('PhoneController:vendors');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Vendors');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalVendorTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            tipsTricks: function () {
                console.log('PhoneController:tipsTricks');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Tips & Tricks');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTipTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },

            adminAlertsData: function () {
                console.log('PhoneController:adminAlertsData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminAlertsDataTableView());
                }
            },
            adminMessagesData: function () {
                console.log('PhoneController:adminMessagesData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminMessagesDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminChatData: function () {
                console.log('PhoneController:adminChatData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                App.mainRegion.show(new PortalAdminChatDataTableView());

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminESAMediaData: function () {
                console.log('PhoneController:adminESAMediaData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminESAMediaDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminSocialMediaData: function () {
                console.log('PhoneController:adminSocialMediaData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminSocialMediaDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminIndustryNewsData: function () {
                console.log('PhoneController:adminIndustryNewsData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminIndustryNewsDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminStaticData: function () {
                console.log('PhoneController:adminStaticData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminStaticDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminBioData: function () {
                console.log('PhoneController:adminBioData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminBioDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            admin: function () {
                console.log('PhoneController:admin');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminGeneralView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            detailsAdmin: function () {
                console.log('PhoneController:detailsAdmin');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new AdminScoreBoardView());
                }
            },
            taskBreakdown: function () {
                console.log('PhoneController:taskBreakdown');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Task Breakdown Reports');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimerTaskBreakdownGridView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editCompany: function () {
                console.log('PhoneController:editCompany');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressCompanyView({type: "Edit Company"}));
                }

            },
            addNewCompany: function () {
                console.log('PhoneController:addNewCompany');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressCompanyView({type: "New Company"}));
                }

            },
            editClient: function () {
                console.log('PhoneController:editClient');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Edit Client');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientView({type: "Edit Client"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            addNewClient: function () {
                console.log('PhoneController:addNewClient');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Add New Client');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientView({type: "New Client"}));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editEmployee: function () {
                console.log('PhoneController:editEmployee');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Edit Employee');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmployeeView({type: "Edit Employee"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            addNewEmployee: function () {
                console.log('PhoneController:addNewEmployee');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Add New Employee');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmployeeView({type: "New Employee"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editTimecard: function () {
                console.log('PhoneController:editTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TimeCardView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editTimecardIntTimerID: function () {
                console.log('PhoneController:editTimecardIntTimerID');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TimeCardView({
                        type: "intTimerID"
                    }));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            payrollProcessing: function () {
                console.log('PhoneController:payrollProcessing');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PayrollProcessingTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            empPodcastData : function () {
                console.log('PhoneController:empPodcastData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmpMediaDataTableView());
                }

            },
            updateNavBar: function (route) {

                if (route === '')
                    route = 'index';

                if (route.indexOf('?') > -1) {
                    route = route.slice(0, route.indexOf('?'));
                }
                $el = $('#nav-' + route);

                // If current route is highlighted, we're done.

                if (route != 'esa_app') {
                    if ($el.hasClass('current_page_item')) {
                        return;
                    } else {
                        // Unhighlight active tab.
                        $('#main-menu li.current_page_item').removeClass('current_page_item');
                        // Highlight active page tab.
                        $el.addClass('current_page_item');
                    }
                }
                else {
                    if ($el.hasClass('current_sub_page_item')) {
                        return;
                    } else {
                        // Unhighlight active tab.
                        $('#main-menu li.current_sub_page_item').removeClass('current_sub_page_item');
                        $('#main-menu li.current_page_item').removeClass('current_page_item');

                        // Highlight active page tab.
                        $el.addClass('current_sub_page_item');
                    }
                }

                // Highlight the menu if applicable...
                //console.log(route);
                //route === 'dashboard' || route === 'projectfilters' ||
                if (route === 'esa_app') {
                    if ($('#nav-programmenu').hasClass('current_page_item'))
                        return;

                    $('#nav-programmenu').addClass('current_page_item');
                    $('#nav-programmenu-text').addClass('current_page_item_text');

                }
                else {
                    $('#nav-programmenu').removeClass('current_page_item');
                }
            },
            parseQueryString: function (queryString) {
                var params = {};
                if (queryString) {
                    _.each(
                        _.map(decodeURI(queryString).split(/&/g), function (el, i) {
                            var aux = el.split('='), o = {};
                            if (aux.length >= 1) {
                                var val;
                                if (aux.length == 2)
                                    val = aux[1];
                                o[aux[0]] = val;
                            }
                            return o;
                        }),
                        function (o) {
                            _.extend(params, o);
                        }
                    );
                }
                return params;
            },
            startApp: function (e) {

                var self = this;

                console.log('PhoneController:userId:' + this.userId);

                var thisView = this;
            }
        });
    });