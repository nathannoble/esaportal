define(['App', 'backbone', 'marionette', 'views/HeaderView', 'views/AsideView', 'views/FooterView', 'views/HomeView','views/PasswordView',
        'views/ClientProfileLayoutView', 'views/CompanyProfileLayoutView', 'views/EmployeeProfileLayoutView',
        'views/ProgressPlanTableView', 'views/PortalEmployeeTableView', 'views/ProgressCompanyTableView',
        'views/ESATileLayoutView', 'views/ESAStatsLayoutView', 'views/PortalCalendarHolidayView',
        'views/PortalUserTableView', 'views/PortalVendorTableView', 'views/PortalMessageTableView',
        'views/PortalTipTableView', 'views/PortalValueTableView', 'views/PortalAdminIndustryNewsDataTableView',
        'views/PortalAdminMessagesDataTableView', 'views/PortalAdminChatDataTableView',
        'views/PortalAdminAlertsDataTableView', 'views/PortalAdminSocialMediaDataTableView', 'views/PortalAdminESAMediaDataTableView',
        'views/PortalAdminStaticDataTableView','views/PortalAdminBioDataTableView',
        'views/PortalTimeCardDetailTableView', 'views/MyTimeCardDetailLayoutView', 'views/AdminScoreBoardView', 'views/ProgressClientDataTableView',
        'views/ProgressClientContactTableView', 'views/PortalTimerTaskBreakdownGridView', 'views/ProgressClientView','views/ProgressCompanyView',
        'views/ProgressEmployeeView','views/OpenTimersTableView','views/TimeCardView', 'views/PayrollProcessingTableView',
        'views/TeamLayoutView','views/DocsLayoutView', 'views/IndustryNewsLayoutView',
        'views/ESAMediaLayoutView','views/SocialMediaLayoutView', 'views/MessagesLayoutView','views/MessagesFullLayoutView',
        'views/WelcomeLayoutView','views/PortalAdminGeneralView','views/ProgressEmpMediaDataTableView'
    ],
    function (App, Backbone, Marionette, HeaderView, AsideView, FooterView, HomeView,PasswordView,
              ClientProfileLayoutView, CompanyProfileLayoutView, EmployeeProfileLayoutView, ProgressPlanTableView, PortalEmployeeTableView,
              ProgressCompanyTableView, ESATileLayoutView, ESAStatsLayoutView, PortalCalendarHolidayView,
              PortalUserTableView, PortalVendorTableView, PortalMessageTableView, PortalTipTableView, PortalValueTableView,
              PortalAdminIndustryNewsDataTableView,
              PortalAdminMessagesDataTableView, PortalAdminChatDataTableView,
              PortalAdminAlertsDataTableView, PortalAdminSocialMediaDataTableView,PortalAdminESAMediaDataTableView,
              PortalAdminStaticDataTableView, PortalAdminBioDataTableView,
              PortalTimeCardDetailTableView, MyTimeCardDetailLayoutView, AdminScoreBoardView, ProgressClientDataTableView,
              ProgressClientContactTableView, PortalTimerTaskBreakdownGridView, ProgressClientView, ProgressCompanyView, ProgressEmployeeView,
              OpenTimersTableView,TimeCardView,PayrollProcessingTableView,
              TeamLayoutView,DocsLayoutView,IndustryNewsLayoutView,
              ESAMediaLayoutView,SocialMediaLayoutView,MessagesLayoutView,MessagesFullLayoutView,WelcomeLayoutView,
              PortalAdminGeneralView,ProgressEmpMediaDataTableView
    ) {
        return Backbone.Marionette.Controller.extend({
            initialize: function (options) {

                $("body").css('overflow', '');
                $("#esa").removeAttr('style');
                this.userType = App.model.get('userType');

                if (App.mobile) {
                    $("#loading-error-region").empty();
                    document.ontouchmove = function (e) {
                        return true;
                    };
                }

                // Setup the header and footer
                $("#loading-error-region").empty();
                App.headerRegion.show(new HeaderView());
                App.asideRegion.show(new AsideView());
                App.footerRegion.show(new FooterView());


            },
            removeLayoutClasses: function () {
                $("#main-region").removeClass("content-wrapper");
                // $("#main-region").removeClass("container-fluid");
                // $("#main-region").removeClass("container-fluid");
                // $("#main-region").removeClass("container-full-map");
                // $("#main-region").removeClass("container-padded-home");
                // $("#main-region").removeClass("container-fluid-tight");
                // $("#main-region").removeClass("container-risk-layout");
                $("#main-region").height(0);
            },
            resetMarkupContainers: function () {

                $("body").css('overflow-x', '');
                $("body").css('overflow-y', '');
                $("body").css('overflow', '');
                $("html").removeAttr('style');
                $("#esa").removeAttr('style');

                $("html,body").scrollTop(0);
            },
            //gets mapped to in AppRouter's appRoutes
            index: function () {
                console.log('DesktopController:index');

                this.compareVersions();

                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();
                App.asideRegion.reset();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                $("body").css('overflow-y', 'auto');
                $("body").css('overflow-x', 'hidden');
                $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                $("#esa").css('background-color', '#FFFFFF');

                // Reset the layout
                this.removeLayoutClasses();

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new HomeView());
                }

            },
            compareVersions: function () {

                // Compare JS App (this app) version with .NET API version.
                console.log('DesktopController:compareVersions:AppVersion:' + App.config.AppVersion);

                this.userType = App.model.get('userType');

                var apiVersion = 0;
                var app = App;

                $.ajax({
                    type: 'GET',
                    contentType: "application/json", //; charset=utf-8",
                    url: App.config.DataServiceURL + "/rest/GetVersion/",
                    dataType: 'json',
                    success: function (response) {
                        apiVersion = parseInt(response,0);
                        console.log("DesktopController:compareVersions:apiVersion:" + apiVersion);

                        $.ajax({
                            type: 'GET',
                            url: 'config.json',
                            dataType: 'json',
                            success: function (data) {
                                app.config.appVersion = data.AppVersion;
                                // If the API version is higher, force refresh
                                if (apiVersion > app.config.AppVersion) {
                                    console.log("DesktopController:compareVersions:update");
                                    window.location.reload(true);
                                }
                            },
                            error: function(){
                                console.log("DesktopController:compareVersions:config.json access error");
                            },
                            data: {},
                            async: false
                        });

                    },
                    error: function (err) {
                        console.log("DesktopController:compareVersions:error:" + err);
                    }
                });

            },
            checkEmail: function () {
                console.log('DesktopController:checkEmail');

                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();
                App.asideRegion.reset();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                $("body").css('overflow-y', 'auto');
                $("body").css('overflow-x', 'hidden');
                $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                $("#esa").css('background-color', '#FFFFFF');

                // Reset the layout
                this.removeLayoutClasses();

                App.mainRegion.show(new HomeView({type: "checkEmail"})); //HomeView());

            },
            setPassword: function () {
                console.log('DesktopController:setPassword');

                this.compareVersions();

                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();
                App.asideRegion.reset();

                // Update containers for scrolling and background
                this.resetMarkupContainers();
                $("body").css('overflow-y', 'auto');
                $("body").css('overflow-x', 'hidden');
                $("html").css('background-color', '#FFFFFF'); // This ensures the background for overflow elements are white as well
                $("#esa").css('background-color', '#FFFFFF');

                // Reset the layout
                this.removeLayoutClasses();

                App.mainRegion.show(new PasswordView());

            },
            home: function () {
                console.log('DesktopController:home');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                $("#loading-error-region").empty();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                App.mainRegion.reset();
                App.headerRegion.reset();
                App.asideRegion.reset();
                App.footerRegion.reset();

                App.headerRegion.show(new HeaderView());
                App.asideRegion.show(new AsideView());
                App.footerRegion.show(new FooterView());

                //App.mainRegion.show(new ESATileLayoutView());

            },
            dashboard: function () {
                console.log('DesktopController:dashboard');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                $("#loading-error-region").empty();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                //App.headerRegion.show(new HeaderView());
                //App.mainRegion.reset();
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESATileLayoutView());
                }

            },
            welcome: function () {
                console.log('DesktopController:welcome');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                $("#loading-error-region").empty();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new WelcomeLayoutView());
                }

            },
            test: function () {
                console.log('DesktopController:test');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Setup the header and footer
                //$("#loading-error-region").empty();
                //App.headerRegion.show(new HeaderView());
                //App.asideRegion.show(new AsideView());

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");
                //App.asideRegion.reset();
                App.headerRegion.reset();
                App.footerRegion.reset();
                App.mainRegion.reset();

                App.headerRegion.show(new HeaderView());
                //App.asideRegion.show(new AsideView());
                //App.mainRegion.show(new ESATileLayoutTestView());

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            views: function () {
                console.log('DesktopController:views');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            team: function () {
                console.log('DesktopController:team');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TeamLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            docs: function () {
                console.log('DesktopController:docs');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                App.mainRegion.show(new DocsLayoutView());

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            news: function () {
                console.log('DesktopController:news');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new IndustryNewsLayoutView());
                }
            },
            esamedia: function () {
                console.log('DesktopController:esamedia');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESAMediaLayoutView());
                }
            },
            socialmedia: function () {
                console.log('DesktopController:socialmedia');

                this.compareVersions();

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new SocialMediaLayoutView());
                }
            },
            messagesfull: function () {
                console.log('DesktopController:messagesfull');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MessagesFullLayoutView());
                }
            },
            messages: function () {
                console.log('DesktopController:messages');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Start Breadcrumb
                //App.model.startBreadcrumb('ESA');
                //App.model.pushBreadcrumb('Messages');
                //this.footerView.updateBreadcrumb();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MessagesLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            clientProfile: function () {
                console.log('DesktopController:clientProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ClientProfileLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            companyProfile: function () {
                console.log('DesktopController:companyProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new CompanyProfileLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            employeeProfile: function () {
                console.log('DesktopController:employeeProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new EmployeeProfileLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            userProfile: function () {
                console.log('DesktopController:userProfile');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new EmployeeProfileLayoutView({type: "User"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            myTimecard: function () {
                console.log('DesktopController:myTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new MyTimeCardDetailLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            openTimers: function () {
                console.log('DesktopController:openTimers');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new OpenTimersTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            clientReports: function () {
                console.log('DesktopController:clientReports');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientContactTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            plans: function () {
                console.log('DesktopController:plans');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                // Setup the main container
                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressPlanTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            employees: function () {
                console.log('DesktopController:employees');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalEmployeeTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            statsTimecard: function () {
                console.log('DesktopController:statsTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimeCardDetailTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            statsTimecardNoDateChange: function (options) {
                console.log('DesktopController:statsTimecardNoDateChange');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimeCardDetailTableView({ignoreDateChange: true}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            companies: function () {
                console.log('DesktopController:companies');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                App.mainRegion.show(new ProgressCompanyTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            calendar: function () {
                console.log('DesktopController:calendar');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalCalendarHolidayView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            myStats: function () {
                console.log('DesktopController:myStats');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ESAStatsLayoutView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            users: function () {
                console.log('DesktopController:users');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalUserTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            vendors: function () {
                console.log('DesktopController:vendors');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalVendorTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            tipsTricks: function () {
                console.log('DesktopController:tipsTricks');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTipTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminMessagesData: function () {
                console.log('DesktopController:adminMessagesData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminMessagesDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminChatData: function () {
                console.log('DesktopController:adminChatData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminChatDataTableView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminAlertsData: function () {
                console.log('DesktopController:adminAlertsData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminAlertsDataTableView());
                }
            },
            adminESAMediaData: function () {
                console.log('DesktopController:adminESAMediaData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminESAMediaDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminSocialMediaData: function () {
                console.log('DesktopController:adminSocialMediaData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminSocialMediaDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminIndustryNewsData: function () {
                console.log('DesktopController:adminIndustryNewsData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminIndustryNewsDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminStaticData: function () {
                console.log('DesktopController:adminStaticData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminStaticDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            adminBioData: function () {
                console.log('DesktopController:adminBioData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminBioDataTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            admin: function () {
                console.log('DesktopController:admin');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalAdminGeneralView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            detailsAdmin: function () {
                console.log('DesktopController:detailsAdmin');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new AdminScoreBoardView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            taskBreakdown: function () {
                console.log('DesktopController:taskBreakdown');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PortalTimerTaskBreakdownGridView());
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editCompany: function () {
                console.log('DesktopController:editCompany');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressCompanyView({type: "Edit Company"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            addNewCompany: function () {
                console.log('DesktopController:addNewCompany');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressCompanyView({type: "New Company"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editClient: function () {
                console.log('DesktopController:editClient');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientView({type: "Edit Client"}));
                }
                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            addNewClient: function () {
                console.log('DesktopController:addNewClient');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressClientView({type: "New Client"}));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editEmployee: function () {
                console.log('DesktopController:editEmployee');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmployeeView({type: "Edit Employee"}));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            addNewEmployee: function () {
                console.log('DesktopController:addNewEmployee');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmployeeView({type: "New Employee"}));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editTimecard: function () {
                console.log('DesktopController:editTimecard');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TimeCardView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            editTimecardIntTimerID: function () {
                console.log('DesktopController:editTimecardIntTimerID');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");

                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new TimeCardView({
                        type: "intTimerID"
                    }));
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            payrollProcessing: function () {
                console.log('DesktopController:payrollProcessing');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                // Update the nav bar
                //this.updateNavBar(Backbone.history.fragment);

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new PayrollProcessingTableView());
                }

                // Store this route/previous route for some conditional breadcrumb items
                //this.previousRoute = Backbone.history.fragment;
            },
            empPodcastData : function () {
                console.log('DesktopController:empPodcastData');

                // Update containers for scrolling and background
                this.resetMarkupContainers();

                // Reset the layout
                this.removeLayoutClasses();

                $("#main-region").addClass("content-wrapper");
                if (this.userType === 0) {
                    App.mainRegion.show(new DocsLayoutView());
                } else {
                    App.mainRegion.show(new ProgressEmpMediaDataTableView());
                }

            },
            updateNavBar: function (route) {

                //console.log('DesktopController:updateNavBar:route:' + route);
                if (route === '')
                    route = 'index';

                if (route.indexOf('?') > -1) {
                    route = route.slice(0, route.indexOf('?'));
                }
                $el = $('#nav-' + route);

                // If current route is highlighted, we're done.

                if (route != 'esa_app') {
                    if ($el.hasClass('current_page_item')) {
                        return;
                    } else {
                        // Unhighlight active tab.
                        $('#main-menu li.current_page_item').removeClass('current_page_item');
                        // Highlight active page tab.
                        $el.addClass('current_page_item');
                    }
                }
                else {
                    if ($el.hasClass('current_sub_page_item')) {
                        return;
                    } else {
                        // Unhighlight active tab.
                        $('#main-menu li.current_sub_page_item').removeClass('current_sub_page_item');
                        $('#main-menu li.current_page_item').removeClass('current_page_item');

                        // Highlight active page tab.
                        $el.addClass('current_sub_page_item');
                    }
                }

                // Highlight the menu if applicable...
                //console.log(route);
                //route === 'dashboard' || route === 'projectfilters' ||
                if (route === 'esa_app') {
                    if ($('#nav-programmenu').hasClass('current_page_item'))
                        return;

                    $('#nav-programmenu').addClass('current_page_item');
                    $('#nav-programmenu-text').addClass('current_page_item_text');

                }
                else {
                    $('#nav-programmenu').removeClass('current_page_item');
                }
            },
            parseQueryString: function (queryString) {
                var params = {};
                if (queryString) {
                    _.each(
                        _.map(decodeURI(queryString).split(/&/g), function (el, i) {
                            var aux = el.split('='), o = {};
                            if (aux.length >= 1) {
                                var val;
                                if (aux.length == 2)
                                    val = aux[1];
                                o[aux[0]] = val;
                            }
                            return o;
                        }),
                        function (o) {
                            _.extend(params, o);
                        }
                    );
                }
                return params;
            },
            startApp: function (e) {

                var self = this;

                console.log('DesktopController:userId:' + this.userId);

                var thisView = this;
            }
        });
    });