module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
//        removelogging: {
//            dist: {
//                src: "public/js/app/init/DesktopInit.min.js"
//            }
//        },

        requirejs: {
            phoneJS: {
                options: {
                    baseUrl: "public/js/app",
                    wrap: true,
                    // Don't use almond if your project needs to load modules dynamically
                    name: "../libs/almond",
                    preserveLicenseComments: false,
                    optimize: "uglify",
                    optimizeCss: "standard",
                    mainConfigFile: "public/js/app/config/config.js",
                    include: ["init/PhoneInit"],
                    out: "public/js/app/init/PhoneInit.min.js",

                    /*********
                     * https://github.com/SlexAxton/require-handlebars-plugin
                     */
                    pragmasOnSave: {
                        //removes Handlebars.Parser code (used to compile template strings) set
                        //it to `false` if you need to parse template strings even after build
                        excludeHbsParser : true,
                        // kills the entire plugin set once it's built.
                        excludeHbs: true,
                        // removes i18n precompiler, handlebars and json2
                        excludeAfterBuild: true
                    },

                    locale: "en_us",

                    // options object which is passed to Handlebars compiler
                    hbs : {
                        templateExtension: "html",
                        helperDirectory: "templates/helpers/",
                        i18nDirectory: "templates/i18n/",

                        compileOptions: {}
                    }
                }
            },
            phoneCSS: {
                options: {
                    optimizeCss: "standard",
                    cssIn: "./public/css/phone.css",
                    out: "./public/css/phone.min.css"
                }
            },
            tabletJS: {
                options: {
                    baseUrl: "public/js/app",
                    wrap: true,
                    // Don't use almond if your project needs to load modules dynamically
                    name: "../libs/almond",
                    preserveLicenseComments: false,
                    optimize: "uglify",
                    optimizeCss: "standard",
                    mainConfigFile: "public/js/app/config/config.js",
                    include: ["init/TabletInit"],
                    out: "public/js/app/init/TabletInit.min.js",

                    /*********
                     * https://github.com/SlexAxton/require-handlebars-plugin
                     */
                    pragmasOnSave: {
                        //removes Handlebars.Parser code (used to compile template strings) set
                        //it to `false` if you need to parse template strings even after build
                        excludeHbsParser : true,
                        // kills the entire plugin set once it's built.
                        excludeHbs: true,
                        // removes i18n precompiler, handlebars and json2
                        excludeAfterBuild: true
                    },

                    locale: "en_us",

                    // options object which is passed to Handlebars compiler
                    hbs : {
                        templateExtension: "html",
                        helperDirectory: "templates/helpers/",
                        i18nDirectory: "templates/i18n/",

                        compileOptions: {}
                    }
                }
            },
            tabletCSS: {
                options: {
                    optimizeCss: "standard",
                    cssIn: "./public/css/tablet.css",
                    out: "./public/css/tablet.min.css"
                }
            },
            desktopJS: {
                options: {
                    uglify2: {
                        compress: {
                            drop_console: true
                        }
                    },
                    baseUrl: "public/js/app",
                    wrap: true,
                    // Cannot use almond since it does not currently appear to support requireJS's config-map
                    name: "../libs/almond",
//                    onModuleBundleComplete: amdcleanLogic,
                    preserveLicenseComments: false,
                    optimize: "uglify",
                    mainConfigFile: "public/js/app/config/config.js",
                    include: ["init/DesktopInit"],
                    out: "public/js/app/init/DesktopInit.min.js"

                }
            },
            desktopCSS: {
                options: {
                    // Refer to desktop.css for additional instructions
                    optimizeCss: "standard",
                    cssIn: "./public/css/desktop.css",
                    out: "./public/css/desktop.min.css"
                }
            },
            desktopCSSIE: {
                options: {
                    // Refer to desktop.css for additional instructions
                    optimizeCss: "standard",
                    cssIn: "./public/css/desktop_optimize.css",
                    out: "./public/css/desktop.ie.min.css"
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'public/js/app/**/*.js', '!public/js/app/**/*min.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: false,
                    module: true,
                    document: true
                }
            }
        }
//        plato: {
//            your_task: {
//                options : {
//                    exclude: /\.min\.js$/    // excludes source files finishing with ".min.js"
//                },
//                files: {
//                    'public/reports': ['public/js/app/**/*.js']
//                }
//            }
//        }
    });

    // TODO: Admin Build
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.registerTask('test', ['jshint']);
    //grunt.registerTask('build', ['requirejs:desktopJS']);
    grunt.registerTask('build', ['requirejs:desktopCSS','requirejs:tabletJS','requirejs:phoneJS','requirejs:desktopCSSIE', 'requirejs:tabletCSS', 'requirejs:phoneCSS','requirejs:desktopJS']);
    grunt.registerTask('default', ['test', 'build']);
};
